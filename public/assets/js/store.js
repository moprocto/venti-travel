jQuery(document).ready(function ($) {
   var tripSlider = document.getElementById('day-slider');
   var budgetSlider = document.getElementById('budget-slider');

   
   var windowWidth = $(window).width();
   if(windowWidth < 768){
      $("#accordionExample").hide();
      $('.collapse').collapse();
   }

   var $grid = $('.grid').isotope({
      getSortData: {
         country: '[data-country]',
          price: '[data-price] parseInt',
          length: '[data-length] parseInt',
          departure: '[data-departure] parseInt'
      },
      itemSelector: '.grid-item'
   });

   noUiSlider.create(tripSlider, {
       start: [1, 21],
       connect: true,
       step: 1,
       range: {'min': 1,'max': 21}
   });

   $('.js-example-basic-multiple').select2({
       placeholder: "SELECT TAGS",
       maximumSelectionLength: 4,
       closeOnSelect: false,
       allowClear: true
   });
   

   var filters = "*";
   var tripRangeElement = document.getElementById('trip-range');
   var budgetRangeElement = document.getElementById('budget-range');

   tripSlider.noUiSlider.on('update', function (values, handle) {
      filters = filterCategories([0,0], tripSlider.noUiSlider.get(true));
      $grid.isotope({ filter: filters });

      $("#results-count").html($grid.data('isotope').filteredItems.length);
      tripRangeElement.innerHTML = Math.round(values[0],0) + " Day(s) - " + Math.round(values[1],0) + " Day(s)";
   });

   $('.js-example-basic-multiple').on('select2:select', function(e) {
         filters = filterCategories([0,0], tripSlider.noUiSlider.get(true));
         $grid.isotope({ filter: filters});
         $("#results-count").html($grid.data('isotope').filteredItems.length);
   });

   $("body").on("change", ".js-example-basic-multiple", function(){
      filters = filterCategories([0,0], tripSlider.noUiSlider.get(true));
         $grid.isotope({ filter: filters});
         $("#results-count").html($grid.data('isotope').filteredItems.length);
   });

   $("#month-select").on("change", function(){
      filters = filterCategories([0,0], tripSlider.noUiSlider.get(true));
      $grid.isotope({ filter: filters});
      $("#results-count").html($grid.data('isotope').filteredItems.length);
   });

   $("#countries").on("change", function(){
      filters = filterCategories([0,0], tripSlider.noUiSlider.get(true));
      $grid.isotope({ filter: filters});
      $("#results-count").html($grid.data('isotope').filteredItems.length);
   });

   $("#strongChemistry").on("click", function(){
      filters = filterCategories([0,0], tripSlider.noUiSlider.get(true));
      $grid.isotope({ filter: filters});
      $("#results-count").html($grid.data('isotope').filteredItems.length);
   });

   $("span.month").each(function(){
      var departure = $(this).attr("data-departure")
      departure = new moment(departure).format("MMMM");
      $(this).html(departure.toUpperCase())
   });


   $("#sort").on("change", function(){

      //sortAscending: false
      var sortBy = $(this).val();
      var asc = false;

      

      if(sortBy.includes("ASC")){
         asc = true;
      }

      if(sortBy.includes("price")){
         if(sortBy.includes("ASC")){
            asc = true;
         }
         $grid.isotope({
           sortBy: 'price',
           sortAscending: asc,
           sortDescending: !asc
         });
      }
      if(sortBy.includes("length")){
         if(sortBy.includes("ASC")){
            asc = true;
         }
         $grid.isotope({
           sortBy: 'length',
           sortAscending: asc,
           sortDescending: !asc
         });
      }
      if(sortBy.includes("departure")){
         if(sortBy.includes("ASC")){
            asc = true;
         }
         $grid.isotope({
           sortBy: 'departure',
           sortAscending: asc,
           sortDescending: !asc
         });
      }
      
   });
});

function filterCategories(pricing, days){
   
   categoriesArray = [];

   var tags = $(".js-example-basic-multiple").val();
   

   $(tags).each(function(e,v){
      categoriesArray.push(v);
   });

   categoriesArray.push($("#countries").val());

   if($("#strongChemistry").prop("checked")){
      categoriesArray.push($("#strongChemistry").val());
   }

   var filters = categoriesArray.join("");

   console.log(filters);

   var daysRange = getRange(days[1], days[0], 1);
   budgetRange = getRange(pricing[1], pricing[0], 50);

   daysRange = daysRange.map(function(val) {
     return val;
   }).join('days, .') + "days";

   //daysRange = '\, ' + daysRange;

  // budgetRange = ',' + budgetRange.join(", budget");

  var filterList = [];

   $.map(daysRange.split(","), function(elem){
      $.map(filters.split(","), function(e){
         var departure = $("#month-select").val();
         if(departure != ""){
            departure = "." + departure;
         }

         if(elem.charAt(0) != "."){
            elem = "." + elem;
         }

         filter = (e + elem);
         filter = filter.replace(" ","");
         filter = filter.replace("..",".")
         filter = filter + departure;
         
         filterList.push(filter);
      });
   });
   
   filterList = filterList.join(",");
   return filterList;
}

function getRange(end, start, step){
   const len = Math.floor((end - start) / step) + 1
   return Array(len).fill().map((_, idx) => start + (idx * step))
}

function countriesSelect(){
   var countries = [];
   $(".grid-item").each(function(){
      var trip = $(this);
      countries.push(trip.attr("data-country"));
   });

   countries = countries.filter(function (value, index, array) { 
     return array.indexOf(value) === index;
   });

   for(i = 0; i < countries.length; i++){
      $("#countries").append("<option value='." + countries[i].replace(/\s+/g,"") +"'>" + countries[i].toUpperCase() + "</option>");
   }
}
