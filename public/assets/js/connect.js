jQuery(document).ready(function ($) {
         var tripSlider = document.getElementById('day-slider');
         var budgetSlider = document.getElementById('budget-slider');
         var $grid = $('.grid').isotope({
           percentPosition: true,
           getSortData: {
               gender: '[data-gender]'
           },
           itemSelector: '.grid-item',
           masonry: {
              columnWidth: '.grid-sizer',
              gutter: '.gutter-sizer'
            }
         });
         /*
         noUiSlider.create(tripSlider, {
             start: [1, 21],
             connect: true,
             step: 1,
             range: {'min': 1,'max': 21}
         });
         */
         
         /*
         

         noUiSlider.create(budgetSlider, {
             start: [50, 4000],
             connect: true,
             step:50,
             range: {'min': 50,'max': 4000}
         });

         

         */
var windowWidth = $(window).width();
   if(windowWidth < 768){
      
      $('.collapse').collapse();
      $("#accordionExample").show();
      $(".accordion-body").show();
   }
         var filters = "*";
         var tripRangeElement = document.getElementById('trip-range');
         var budgetRangeElement = document.getElementById('budget-range');

         /*

         tripSlider.noUiSlider.on('update', function (values, handle) {
            filters = filterCategories([0,0], tripSlider.noUiSlider.get(true));
            $grid.isotope({ filter: filters });

            $("#results-count").html($grid.data('isotope').filteredItems.length);
            tripRangeElement.innerHTML = Math.round(values[0],0) + " Day(s) - " + Math.round(values[1],0) + " Day(s)";
         });

         */

         /*

         budgetSlider.noUiSlider.on('update', function (values, handle) {
            filter = filterCategories(budgetSlider.noUiSlider.get(true), tripSlider.noUiSlider.get(true));
            $grid.isotope({ filter: filters  });
            budgetRangeElement.innerHTML = "$" + Math.round(values[0],0) + " - " + "$" + Math.round(values[1],);
         });
         
         */
         $('.js-example-basic-multiple').select2({
             placeholder: "Select Languages",
             maximumSelectionLength: 3,
             closeOnSelect: false,
             allowClear: true
         });

         $('.js-example-basic-multiple-interests').select2({
             placeholder: "Select Interests",
             maximumSelectionLength: 4,
             closeOnSelect: false,
             allowClear: true
         });

         
         

         

         $(".categories").click(function(){
            filters = filterCategories([0,0], tripSlider.noUiSlider.get(true));
            $grid.isotope({ filter: filters});
            $("#results-count").html($grid.data('isotope').filteredItems.length);
         });

         $("#month-select").on("change", function(){
            filters = filterCategories([0,0], tripSlider.noUiSlider.get(true));
            $grid.isotope({ filter: filters});
            $("#results-count").html($grid.data('isotope').filteredItems.length);
         });

         $("span.month").each(function(){
            var departure = $(this).attr("data-departure")
            departure = new moment(departure).format("MMMM");
            $(this).html(departure.toUpperCase())
         });

         $(".filter").on("change", function(){
            var compatibility = $("#compatibility").val(); 
            var gender = $("#gender").val(); 
            var smoking = $("#smoking").val();
            var drinking = $("#drinking").val();
            var invites = $("#invites").val();
            var religion = $("#religion").val();
            var languages = $("#languages").val().join(".");
            var interests = $("#interests").val().join(".");
            if(languages == "."){
               languages = "";
            }
            if(interests == "."){
               interests = "";
            }

            var filters = drinking + smoking + interests + gender + invites + religion + languages + (compatibility ?? "");

            $grid.isotope({ filter: filters});

         })

         $("#sort").on("change", function(){

            //sortAscending: false
            var sortBy = $(this).val();
            var asc = false;

            if(sortBy.includes("ASC")){
               asc = true;
            }

            if(sortBy.includes("price")){
               if(sortBy.includes("ASC")){
                  asc = true;
               }
               $grid.isotope({
                 sortBy: 'price',
                 sortAscending: asc,
                 sortDescending: !asc
               });
            }
            if(sortBy.includes("length")){
               if(sortBy.includes("ASC")){
                  asc = true;
               }
               $grid.isotope({
                 sortBy: 'length',
                 sortAscending: asc,
                 sortDescending: !asc
               });
            }
            if(sortBy.includes("departure")){
               if(sortBy.includes("ASC")){
                  asc = true;
               }
               $grid.isotope({
                 sortBy: 'departure',
                 sortAscending: asc,
                 sortDescending: !asc
               });
            }

         });

      });

      function filterCategories(pricing, days){
         //console.log(pricing);
         
         categoriesArray = [];

         $(".categories:checked").each(function(e,v){
            categoriesArray.push(v.value);
         });

         var filters = categoriesArray.join("");

         

         var daysRange = getRange(days[1], days[0], 1);
         budgetRange = getRange(pricing[1], pricing[0], 50);

         daysRange = daysRange.map(function(val) {
           return val;
         }).join('days, .') + "days";

         //daysRange = '\, ' + daysRange;

        // budgetRange = ',' + budgetRange.join(", budget");

        var filterList = [];

         $.map(daysRange.split(","), function(elem){
            $.map(filters.split(","), function(e){
               var departure = $("#month-select").val();
               if(departure != ""){
                  departure = "." + departure;
               }

               if(elem.charAt(0) != "."){
                  elem = "." + elem;
               }
               filter = (e + elem);
               filter = filter.replace(" ","");
               filter = filter.replace("..",".")
               filter = filter + departure;
               
               filterList.push(filter);
            });
         });
         
         filterList = filterList.join(",");
         return filterList;
      }

      function getRange(end, start, step){
         const len = Math.floor((end - start) / step) + 1
         return Array(len).fill().map((_, idx) => start + (idx * step))
      }