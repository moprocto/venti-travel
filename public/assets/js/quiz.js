$("#example-basic").steps({
    headerTag: "h3",
    bodyTag: "section",
    autoFocus: true,
    onFinished: function(event, currentIndex){
    	console.log("finished");
    	var peaceful = 0;
    		var adventure = 0;
    		var spending = 0;
    		var classic = 0;
    		var taboo = 0;
    	$(".choice.active").each(function(){
    		var radio = $(this).find(".selection");
    		var cat = radio.attr("data-cat");
    		var value = parseInt(radio.val());
    		

    		switch (cat){
        		case "peaceful":
        			peaceful += value;
        			break;
        		case "adventure":
        			adventure += value;
        			break;
        		case "spending":
        			spending += value;
        			break;
        		case "classic":
        			classic += value;
        			break;
        		case "taboo":
        			taboo += value;
        			break;
        	}

    	});



    	peaceful = peaceful/2;
    	adventure = adventure/4;
    	spending = spending/3;
    	taboo = taboo/3;
    	classic = parseInt(classic)/3;

    	

    	var results = [
    		{	
    			"category": "peaceful",
    			"value": peaceful
    		},
    		{	
    			"category": "adventure",
    			"value": adventure
    		},
    		{	
    			"category": "spending",
    			"value": spending
    		},
    		{	
    			"category": "classic",
    			"value": classic
    		},
    		{	
    			"category": "taboo",
    			"value": taboo
    		}
    	];



    	results = results.sort((a, b) => b.value - a.value);

    	

    	// store the result

    	$.ajax({
               url: "/quiz/save",
               type: 'post',
               processData: true,
               dataType: 'json',
               data:{
                  cat1: results[0]["category"],
                  cat2: results[1]["category"],
                  peaceful: peaceful,
                  taboo: taboo,
                  adventure: adventure,
                  classic: classic,
                  spending: spending
               },
               success: function (data) {
                window.location.replace("/quiz/result?personality=" + data);
               }
        });

    	// direct the user to the completion page

    	
    }
});

	    

	    $('li.choice').click(function(){
	    	$(this).find(".selection").prop("checked", true);
	    	$(this).addClass("active");
	    	setTimeout(updateActives,100);
	    });

	    function updateActives(){
	    	$('li.choice').each(function(){
	    		if(!$(this).hasClass("active")){
	    			//$(this).find(".selection").removeAttr("checked");
	    		}
	    	});

	    	$('.selection').each(function(){
	    		if (!$(this).is(":checked")) {
	    			$(this).parent().parent().removeClass("active");
	    		}
	    	});
	    	

	    	setTimeout(function(){
 
				$('.actions a[href="#next"]').trigger('click');
			}, 400);
 
	    	
	    }
