class SlideStories {
  constructor(id) {
    this.slide = document.querySelector(`[data-slide="${id}"]`);
    this.active = 0;
    this.init();
  }

  activeSlide(index) {
    this.active = index;
    this.items.forEach((item) => item.classList.remove('active'));
    this.items[index].classList.add('active');
    this.thumbItems.forEach((item) => item.classList.remove('active'));
    this.thumbItems[index].classList.add('active');
    //this.autoSlide();
  }

  prev() {
    if (this.active > 0) {
      this.activeSlide(this.active - 1);
    } else {
      this.activeSlide(this.items.length - 1);
    }
  }

  next() {
    if (this.active < this.items.length - 1) {
      this.activeSlide(this.active + 1);
    } else {
      this.activeSlide(0);
    }
  }

  addNavigation() {
    const nextBtn = this.slide.querySelector('.slide-next');
    const prevBtn = this.slide.querySelector('.slide-prev');
    nextBtn.addEventListener('click', this.next);
    prevBtn.addEventListener('click', this.prev);
  }

  addThumbItems() {
    this.items.forEach(() => (this.thumb.innerHTML += `<span></span>`));
    this.thumbItems = Array.from(this.thumb.children);
  }

  autoSlide() {
    clearTimeout(this.timeout);
    this.timeout = setTimeout(this.next, 50000);
  }

  init() {
    this.next = this.next.bind(this);
    this.prev = this.prev.bind(this);
    this.items = this.slide.querySelectorAll('.slide-items > *');
    this.thumb = this.slide.querySelector('.slide-thumb');
    this.addThumbItems();
    this.activeSlide(0);
    this.addNavigation();
  }
}

jQuery(document).ready(function ($) {

  var windowWidth = $(window).width();
   if(windowWidth < 768){
      $("#accordionExample").hide();
      setTimeout(function(){
        $("#accordionExample").show();
        $('.collapse').collapse("hide");
      }, 500);
   } else{
    $('.collapse').collapse("show");
   }

  var $grid = $('.grid').isotope({
    percentPosition: true,
    itemSelector: '.grid-item',
    layoutMode: "fitRows",
    masonry: {
      columnWidth: 100,
    }
  });


  $(".filter").on("change", function(){
      var category = $("#category").val();
      var country = $("#countries").val();

      var filters = category + country;

      $grid.isotope({ filter: filters});

   });

  countriesSelect();

  
});



function filterCategories(pricing, days){
         //console.log(pricing);
         
         categoriesArray = [];

         $(".categories:checked").each(function(e,v){
            categoriesArray.push(v.value);
         });

         var filters = categoriesArray.join("");

         

         var daysRange = getRange(days[1], days[0], 1);
         budgetRange = getRange(pricing[1], pricing[0], 50);

         daysRange = daysRange.map(function(val) {
           return val;
         }).join('days, .') + "days";

         //daysRange = '\, ' + daysRange;

        // budgetRange = ',' + budgetRange.join(", budget");

        var filterList = [];

         $.map(daysRange.split(","), function(elem){
            $.map(filters.split(","), function(e){
               var departure = $("#month-select").val();
               if(departure != ""){
                  departure = "." + departure;
               }

               if(elem.charAt(0) != "."){
                  elem = "." + elem;
               }
               filter = (e + elem);
               filter = filter.replace(" ","");
               filter = filter.replace("..",".")
               filter = filter + departure;
               
               filterList.push(filter);
            });
         });
         
         filterList = filterList.join(",");
         return filterList;
      }
function countriesSelect(){
   var countries = [];
   $(".grid-item").each(function(){
      var trip = $(this);
      countries.push(trip.attr("data-country"));
   });

   countries = countries.filter(function (value, index, array) { 
     return array.indexOf(value) === index;
   });

   for(i = 0; i < countries.length; i++){
      $("#countries").append("<option value='." + countries[i].replace(/\s+/g,"") +"'>" + countries[i].toUpperCase() + "</option>");
   }
}