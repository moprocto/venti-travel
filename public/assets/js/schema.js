var products = {
	"0": {
		"label": 'Personal Credit Card',
		"icon": 'fas fa-credit-card',
		'description': "Any credit card product meant for personal use",
		'data': { // the data this info will send to step 2
			"0": {
				"label": "Balance",
				"type": "float",
				"icon": "fas fa-credit-card"
			},
			"1":{
				"label": "Transactions",
				"type": "array",
				"icon": "fas fa-rectangle-list"
			}
		}
	},
	"1": {
		"label": 'Personal Debit Card',
		"icon": 'fas fa-credit-card',
		'description': "Any credit card product meant for personal use",
		'data': { // the data this info will send to step 2
			"0": {
				"label": "Balance",
				"type": "float",
				"icon": "fas fa-credit-card"
			},
			"1":{
				"label": "Transactions",
				"type": "array",
				"icon": "fas fa-rectangle-list"
			}
		}
	},
	"2": {
		"label": 'Personal Checking',
		"icon": 'fas fa-money-check-dollar',
		'description': "Any checking account meant for personal use. You can specify the product ID via the playground settings for this tab.",
		'data': { // the data this info will send to step 2
			"0": {
				"label": "Balance",
				"type": "float",
				"icon": "fas fa-money-check-dollar"
			},
			"1":{
				"label": "Transactions",
				"type": "array",
				"icon": "fa-regular fa-rectangle-list"
			}
		}
	},
	"3": {
		"label": 'Personal Savings',
		"icon": 'fas fa-money-check-dollar',
		'description': "Any savings account meant for personal use, includes high-yield savings. You can specify the product ID via the playground settings for this tab.",
		'data': { // the data this info will send to step 2
			"0": {
				"label": "Balance",
				"type": "float",
				"icon": "fas fa-money-check-dollar"
			},
			"1":{
				"label": "Transactions",
				"type": "array",
				"icon": "fa-regular fa-rectangle-list"
			}
		}
	},
	"4": {
		"label": 'Business Credit Card',
		"icon": 'fas fa-credit-card',
		'description': "Any credit card product tied to a business account.",
		'data': { // the data this info will send to step 2
			"0": {
				"label": "Balance",
				"type": "float",
				"icon": "fas fa-credit-card"
			},
			"1":{
				"label": "Transactions",
				"type": "array",
				"icon": "fas fa-rectangle-list"
			}
		}
	},
	"5": {
		"label": 'Business Debit Card',
		"icon": 'fas fa-credit-card',
		'description': "Any debit card product tied to a business account.",
		'data': { // the data this info will send to step 2
			"0": {
				"label": "Balance",
				"type": "float",
				"icon": "fas fa-credit-card"
			},
			"1":{
				"label": "Transactions",
				"type": "array",
				"icon": "fas fa-rectangle-list"
			}
		}
	},
	"6": {
		"label": 'Business Checking',
		"icon": 'fas fa-money-check-dollar',
		'description': "Any checking account tied to a business account. You can specify the product ID via the playground settings for this tab.",
		'data': { // the data this info will send to step 2
			"0": {
				"label": "Balance",
				"type": "float",
				"icon": "fas fa-money-check-dollar"
			},
			"1":{
				"label": "Transactions",
				"type": "array",
				"icon": "fa-regular fa-rectangle-list"
			}
		}
	},
	"7": {
		"label": 'Business Savings',
		"icon": 'fas fa-money-check-dollar',
		'description': "Any savings account tied to a business account. You can specify the product ID via the playground settings for this tab.",
		'data': { // the data this info will send to step 2
			"0": {
				"label": "Balance",
				"type": "float",
				"icon": "fas fa-money-check-dollar"
			},
			"1":{
				"label": "Transactions",
				"type": "array",
				"icon": "fa-regular fa-rectangle-list"
			}
		}
	},
	"8": {
		"label": 'Credit Score',
		"icon": 'fas fa-gauge-high',
		'description': "A credit score tied to an individual account holder",
		'data': { // the data this info will send to step 2
			"0": {
				"label": "Latest Credit Score",
				"type": "integer",
				"icon": 'fas fa-gauge-high',
			},
			"1": {
				"label": "Last 10 Credit Scores",
				"type": "array",
				"icon": 'fas fa-gauge',
			}
		}
	},
	"9": {
		"label": 'Mortgage',
		"icon": 'fas fa-home',
		'description': "Any home loan product. You can specify the product ID via the playground settings for this tab.",
		'data': { // the data this info will send to step 2
			"0": {
				"label": "Balance",
				"type": "float",
				"icon": "fas fa-money-check-dollar"
			},
			"1":{
				"label": "Transactions",
				"type": "array",
				"icon": "fa-regular fa-rectangle-list"
			}
		}
	},
	"10": {
		"label": 'Auto Loan',
		"icon": 'fas fa-car',
		'description': "Any auto loan product. You can specify the product ID via the playground settings for this tab.",
		'data': { // the data this info will send to step 2
			"0": {
				"label": "Balance",
				"type": "float",
				"icon": "fas fa-money-check-dollar"
			},
			"1":{
				"label": "Transactions",
				"type": "array",
				"icon": "fa-regular fa-rectangle-list"
			}
		}
	},
	"11": {
		"label": 'Student Loan',
		"icon": 'fas fa-graduation-cap',
		'description': "Any student loan product. You can specify the product ID via the playground settings for this tab.",
		'data': { // the data this info will send to step 2
			"0": {
				"label": "Balance",
				"type": "float",
				"icon": "fas fa-money-check-dollar"
			},
			"1":{
				"label": "Transactions",
				"type": "array",
				"icon": "fa-regular fa-rectangle-list"
			}
		}
	},
	"12": {
		"label": 'Signature Loan',
		"icon": 'fas fa-file-signature',
		'description': "Any unsecured loan product. You can specify the product ID via the playground settings for this tab.",
		'data': { // the data this info will send to step 2
			"0": {
				"label": "Balance",
				"type": "float",
				"icon": "fas fa-money-check-dollar"
			},
			"1":{
				"label": "Transactions",
				"type": "array",
				"icon": "fa-regular fa-rectangle-list"
			}
		}
	},
	"13": {
		"label": 'Personal Loan',
		"icon": 'fas fa-file-contract',
		'description': "Any unsecured loan product. You can specify the product ID via the playground settings for this tab.",
		'data': { // the data this info will send to step 2
			"0": {
				"label": "Balance",
				"type": "float",
				"icon": "fas fa-money-check-dollar"
			},
			"1":{
				"label": "Transactions",
				"type": "array",
				"icon": "fa-regular fa-rectangle-list"
			}
		}
	},
	"14": {
		"label": 'Home Equity Loan (HELOC)',
		"icon": 'fas fa-file-contract',
		'description': "Any secured loan against a property. You can specify the product ID via the playground settings for this tab.",
		'data': { // the data this info will send to step 2
			"0": {
				"label": "Balance",
				"type": "float",
				"icon": "fas fa-money-check-dollar"
			},
			"1":{
				"label": "Transactions",
				"type": "array",
				"icon": "fa-regular fa-rectangle-list"
			}
		}
	},
	"15": {
		"label": 'Retirement Account (IRA)',
		"icon": 'fas fa-piggy-bank',
		'description': "Any investment account meant for retirement. You can specify the product ID via the playground settings for this tab.",
		'data': { // the data this info will send to step 2
			"0": {
				"label": "Balance",
				"type": "float",
				"icon": "fas fa-money-check-dollar"
			},
			"1":{
				"label": "Transactions",
				"type": "array",
				"icon": "fa-regular fa-rectangle-list"
			}
		}
	},
	"16": {
		"label": 'Certificate of Deposit (CD)',
		"icon": 'fas fa-money-bill-trend-up',
		'description': "Any certificate of deposit account. You can specify the product ID via the playground settings for this tab.",
		'data': { // the data this info will send to step 2
			"0": {
				"label": "Balance",
				"type": "float",
				"icon": "fas fa-money-check-dollar"
			},
			"1":{
				"label": "Transactions",
				"type": "array",
				"icon": "fa-regular fa-rectangle-list"
			}
		}
	},
	"17": {
		"label": 'Money Market',
		"icon": 'fas fa-money-bill-trend-up',
		'description': "Any money market account. You can specify the product ID via the playground settings for this tab.",
		'data': { // the data this info will send to step 2
			"0": {
				"label": "Balance",
				"type": "float",
				"icon": "fas fa-money-check-dollar"
			},
			"1":{
				"label": "Transactions",
				"type": "array",
				"icon": "fa-regular fa-rectangle-list"
			}
		}
	},
	"18": {
		"label": 'Referral',
		"icon": 'fas fa-people-arrows',
		'description': "A new member joins that was referred by an existing member. You can specify additional criteria via the playground settings for this tab.",
		'data': { // the data this info will send to step 2
			"0": {
				"label": "Member Profile",
				"type": "array",
				"icon": 'fas fa-people-arrows',
			},
			"1": {
				"label": "Referred Member Profile",
				"type": "array",
				"icon": 'fas fa-user',
			}
		}
	},
	"19": {
		"label": 'QR Code',
		"icon": 'fas fa-qrcode',
		'description': "A QR code that you've created gets scanned by a member with a rewards account. You can specify additional criteria via the playground settings for this tab.",
		'data': {
			"0": {
				"label": "Member Profile",
				"type": "array",
				"icon": 'fas fa-user',
			}
		}
	},
	"20": {
		"label": 'Webhook',
		"icon": 'fas fa-server',
		'description': "A URL managed by Venti receives encrypted data from an external platform you manage, such as an ecommerce store, learning management system, CRM, etc. You can specify additional criteria via the playground settings for this tab.",
		'data': {
			"0": {
				"label": "Member Profile",
				"type": "array",
				"icon": 'fas fa-user',
			}
		}
	}
}

var rewards = {
	 "0": {
	 	"label" : "Fixed Amount",
	 	"html" : "number"
	 },
	 "1": {
	 	"label" : "Percent of Received Value",
	 	"input" : "number"
	 },
	 "2": {
	 	"label" : "AI Determined Range*",
	 	"input" : "range"
	 }
}

var rewardsInput = {
	 "0": {
	 	"label" : "Fixed Amount",
	 	"html" : '<hr><label class="form-label text-bold text-small">Fixed Amount</label><div class="input-group"><span class="btn btn-white" style="pointer-events: none; border:1px solid #ced4da"><i class="fa fa-hashtag" style="padding-top:8px"></i></span><input type="number" class="form-control"></div>'
	 },
	 "1": {
	 	"label" : "Percent of Received Value",
	 	"html" : '<hr><label class="form-label text-bold text-small">% of Received Value</label><div class="input-group"><span class="btn btn-white" style="pointer-events: none; border:1px solid #ced4da"><i class="fa fa-percent" style="padding-top:8px"></i></span><input type="number" class="form-control"></div><hr><label class="text-small text-bold text-uppercase">Use Value</label><select class="form-control select2"><option>Current Available Balance</option><option>Avg. Daily Balance (30 Days)</option><option>Sum of Transactions (30 Days)</option></select>'
	 },
	 "2": {
	 	"label" : "AI Determined Range*",
	 	"html" : '<hr><label class="form-label text-bold text-small">AI Determined Range</label><div class="input-group"><input type="number" class="form-control"><span class="btn btn-white" style="pointer-events: none; border:1px solid #ced4da"><i class="fa fa-arrows-left-right" style="padding-top:8px"></i></span><input type="number" class="form-control"></div>'
	 }
}

var constraintsInput = {

}



var triggers = {
	"0": {
		"newAccount" : "Opens New Account",
		"purchase" : "Makes Any Purchase",
		"payment" : "Makes Bill Payment",
		"volume" : "Spend Volume",
		"frequency" : "Spend Frequency",
		"location": "Spend Location",
		"transfer" : "Balance Transfer",
		"minimumBalance": "Minimum Balance"
	},
	"1": {
		"newAccount" : "Opens New Account",
		"purchase" : "Makes Any Purchase",
		"payment" : "Makes Bill Payment",
		"volume" : "Spend Volume",
		"frequency" : "Spend Frequency",
		"location": "Spend Location"
	},
	"2": {
		"newAccount" : "Opens New Account",
		"directDeposit" : "Direct Deposit",
		"depositVolume": "Deposit Volume",
		"minimumBalance": "Minimum Balance"
	},
	"3": {
		"newAccount" : "Opens New Account",
		"directDeposit" : "Direct Deposit",
		"depositVolume": "Deposit Volume",
		"minimumBalance": "Minimum Balance"
	},
	"2": {
		"newAccount" : "Opens New Account",
		"directDeposit" : "Direct Deposit",
		"depositVolume": "Deposit Volume",
		"minimumBalance": "Minimum Balance"
	},
	"3": {
		"newAccount" : "Opens New Account",
		"directDeposit" : "Direct Deposit",
		"depositVolume": "Deposit Volume",
		"minimumBalance": "Minimum Balance"
	},
	"8": {
		"newAccount" : "Permissions Enabled",
		"creditScoreIncreased" : "Credit Score Increased",
		"creditScoreDecreased" : "Credit Score Decreased",
	},
	"9": {
		"newAccount" : "Opens New Account",
		"directDeposit" : "Direct Deposit",
		"depositVolume": "Deposit Volume",
		"minimumBalance": "Minimum Balance"
	},
	"10": {
		"newAccount" : "Opens New Account",
		"payment" : "Makes Bill Payment",
		"transfer" : "Balance Transfer",
		"payoff" : "Pays Off Balance",
	},
	"11": {
		"newAccount" : "Opens New Account",
		"payment" : "Makes Bill Payment",
		"transfer" : "Balance Transfer",
		"payoff" : "Pays Off Balance",
	},
	"12": {
		"newAccount" : "Opens New Account",
		"payment" : "Makes Bill Payment",
		"transfer" : "Balance Transfer",
		"payoff" : "Pays Off Balance",
	},
	"13": {
		"newAccount" : "Opens New Account",
		"payment" : "Makes Bill Payment",
		"transfer" : "Balance Transfer",
		"payoff" : "Pays Off Balance",
	},
	"14": {
		"newAccount" : "Opens New Account",
		"purchase" : "Makes Any Purchase",
		"payment" : "Makes Bill Payment",
		"volume" : "Spend Volume",
		"frequency" : "Spend Frequency",
		"transfer" : "Balance Transfer",
		"payoff" : "Pays Off Balance",
	},
	"15": {
		"newAccount" : "Opens New Account",
		"balance" : "Minimum Balance",
		"balanceTranser": "Balance Transfer",
		"recurringDeposit" : "Completes Recurring Deposit",
	},
	"16": {
		"newAccount" : "Opens New Account",
		"balance" : "Minimum Balance",
		"balanceTranser": "Balance Transfer",
		"recurringDeposit" : "Recurring Deposit",
	},
	"17": {
		"newAccount" : "Opens New Account",
		"balance" : "Minimum Balance",
		"balanceTranser": "Balance Transfer",
		"recurringDeposit" : "Recurring Deposit",
	},
	"18": {
		"referralComplete": "Referral Complete",
		"referralCompleteConstraints": "Referral Complete With Profile Review"
	},
	"19": {
		"scanComplete" : "Scan Complete"
	},
	"20": {
		"webookReceived" : "Webhook Received"
	}
}

var triggerSubactions = {
	"newAccount": {
    "0": {
      "label": "Date Range",
      "type": "date",
      "dataLabel": "dateRange"
    }
	},
  "cardPurchase": {
    "0": {
      "label": "Date Range",
      "type": "date",
      "dataLabel": "dateRange"
  	},
	  "1": {
	      "label": "Merchant Code",
	      "type": "string",
	      "dataLabel": "merchantCode"
	  },
	  "2": {
	      "label": "Transaction Amount",
	      "type": "integer",
	      "dataLabel": "transactionAmount"
	  }
	},
  "payment": {
    "0": {
      "label": "Transaction Amount",
      "type": "integer",
      "dataLabel": "transactionAmount"
    },
    "1": {
      "label": "Percent Balance",
      "type": "integer",
      "dataLabel": "percentBalance"
    },
    "2": {
      "label": "Date Range",
      "type": "date",
      "dataLabel": "dateRange"
    }
  },
  "volume": {
    "0": {
      "label": "Transaction Amount",
      "type": "integer",
      "dataLabel": "transactionAmount"
    },
    "1": {
      "label": "Date Range",
      "type": "date",
      "dataLabel": "dateRange"
    }
  },
  "frequency": {
    "0": {
      "label": "Number of Payments",
      "type": "integer",
      "dataLabel": "numberPayments"
    },
    "1": {
      "label": "Date Range",
      "type": "date",
      "dataLabel": "dateRange"
    }
  },
  "location": {
    "0": {
      "label": "Zip Code",
      "type": "integer",
      "dataLabel": "zipCode"
    },
    "1": {
      "label": "Was Online",
      "type": "boolean",
      "dataLabel": "wasOnline"
    }
  },
  "transfer": {
    "0": {
      "label": "Transaction Amount",
      "type": "integer",
      "dataLabel": "transactionAmount"
    },
    "1": {
      "label": "Percent Balance",
      "type": "integer",
      "dataLabel": "percentBalance"
    },
    "2": {
      "label": "Percent Account Limit",
      "type": "integer",
      "dataLabel": "percentBalance"
    },
    "3": {
      "label": "Date Range",
      "type": "date",
      "dataLabel": "dateRange"
    }
  },
  "minimumBalance": {
    "0": {
      "label": "Minimum Balance",
      "type": "integer",
      "dataLabel": "minimumBalance"
    }
  },
  "minimumBalance": {
    "0": {
      "label": "Minimum Balance",
      "type": "integer",
      "dataLabel": "minimumBalance"
    }
  }
}

var constraintsInput = {
"0": {
      "label": "Number of Payments",
      "type": "integer",
      "dataLabel": "numberPayments"
    },
    "1": {
      "label": "Date Range",
      "type": "date",
      "dataLabel": "dateRange"
    }

}

function formatOption(option) {
  if (!option.id) { return option.text; }
  var $option = $(
    '<span class="mx-drop-align"><i class="' + option.icon + '"></i> ' + option.text + '</span>'
  );
  return $option;
}

var selectOptions = [];

for (var key in products) {
	selectOptions.push({
	  id: key,
	  text: products[key].label,
	  icon: products[key].icon
	});
}

var rewardOptions = [];

for (var key in rewards) {
	rewardOptions.push({
	  id: key,
	  text: rewards[key].label,
	  icon: rewards[key].icon
	});
}

function updateTriggerOptions(selectedProductId) {
	var triggerOptions = triggers[selectedProductId] || {}; // Get triggers for selected product, or an empty object if not found
	var newOptions = [];
	for (var key in triggerOptions) {
	  newOptions.push({
	    id: key,
	    text: triggerOptions[key]
	  });
	}

	// Initialize (or re-initialize) the second dropdown
	$('#triggerSelect').empty().select2({
		templateResult: formatOption,
		templateSelection: formatOption,
	  	data: newOptions
	});
}

function updateDataAvailableList(selectedProductId) {
	var dataOptions = products[selectedProductId].data || {}; // Get triggers for selected product, or an empty object if not found
	var newOptions = [];
	$("#stepOneData").html("");

	for(i = 0; i < Object.keys(dataOptions).length; i++){

		$("#stepOneData").append("<li><i class='" + dataOptions[i].icon + "'></i> " + dataOptions[i].label + "</li>")
	}
}

function updateRewardsInput(selectedRewardType){
	var dataOptions = rewardsInput[selectedRewardType].html || "";
	$("#rewardInput").html(dataOptions);
}

function updateContraintsInput(selectedConstraintsType){
	var dataOptions = rewardsInput[selectedRewardType].html || "";
	$("#constraintInput").html(dataOptions);
}

  // Initialize second dropdown with default options (product 0)
  setTimeout(function(){
  	  updateTriggerOptions(0);
  }, 100)


  // Event listener for changes in the first dropdown
  $('#productSelect').change('select2:select', function(e) {

    //var selectedProductId = e.params.data.id;
    
  });