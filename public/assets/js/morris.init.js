! function(r) {
    "use strict";

    function e() {}
    e.prototype.createStackedChart = function(e, a, r, t, o, i) {
        Morris.Bar({
            element: e,
            data: a,
            xkey: r,
            ykeys: t,
            stacked: !0,
            labels: o,
            hideHover: "auto",
            dataLabels: !1,
            resize: !0,
            gridLineColor: "rgba(65, 80, 95, 0.07)",
            barColors: i
        })
    }, e.prototype.createAreaChart = function(e, a, r, t, o, i, l, s, b) {
        Morris.Area({
            element: e,
            pointSize: a,
            lineWidth: r,
            data: t,
            xkey: o,
            dataLabels: !1,
            ykeys: i,
            labels: l,
            fillOpacity: s,
            hideHover: "auto",
            resize: !0,
            gridLineColor: "rgba(65, 80, 95, 0.07)",
            lineColors: b
        })
    }, e.prototype.createLineChart = function(e, a, r, t, o, i, l, s, b) {
        Morris.Line({
            element: e,
            data: a,
            dataLabels: !1,
            xkey: r,
            ykeys: t,
            labels: o,
            fillOpacity: i,
            pointFillColors: l,
            pointStrokeColors: s,
            behaveLikeLine: !0,
            gridLineColor: "rgba(65, 80, 95, 0.07)",
            hideHover: "auto",
            lineWidth: "3px",
            pointSize: 0,
            preUnits: "$",
            resize: !0,
            lineColors: b
        })
    }, e.prototype.createBarChart = function(e, a, r, t, o, i) {
        Morris.Bar({
            element: e,
            data: a,
            dataLabels: !1,
            xkey: r,
            ykeys: t,
            labels: o,
            hideHover: "auto",
            resize: !0,
            gridLineColor: "rgba(65, 80, 95, 0.07)",
            barSizeRatio: .4,
            xLabelAngle: 35,
            barColors: i
        })
    }, e.prototype.createAreaChartDotted = function(e, a, r, t, o, i, l, s, b, c) {
        Morris.Area({
            element: e,
            pointSize: 3,
            lineWidth: 1,
            data: t,
            dataLabels: !1,
            xkey: o,
            ykeys: i,
            labels: l,
            hideHover: "auto",
            pointFillColors: s,
            pointStrokeColors: b,
            resize: !0,
            smooth: !1,
            behaveLikeLine: !0,
            fillOpacity: .4,
            gridLineColor: "rgba(65, 80, 95, 0.07)",
            lineColors: c
        })
    }, e.prototype.createDonutChart = function(e, a, r) {
        Morris.Donut({
            element: e,
            data: a,
            barSize: .2,
            resize: !0,
            colors: r,
            backgroundColor: "transparent"
        })
    }, e.prototype.init = function() {
        var e = ["#fff","#0b8ffb", "#04e397"],
            a = r("#morris-bar-stacked").data("colors");
        a && (e = a.split(",")), this.createStackedChart("morris-bar-stacked", [{
            y: "Jan.",
            a: 5,
            b: 100
        }, {
            y: "Feb.",
            a: 7,
            b: 120
        }, {
            y: "Mar.",
            a: 20,
            b: 140
        }, {
            y: "Apr.",
            a: 30,
            b: 200
        }, {
            y: "May",
            a: 33,
            b: 260
        }, {
            y: "Jun.",
            a: 40,
            b: 500
        }, {
            y: "Jul.",
            a: 30,
            b: 400
        }, {
            y: "Aug.",
            a: 50,
            b: 300
        }, {
            y: "Sep.",
            a: 50,
            b: 40
        }, {
            y: "Oct.",
            a: 75,
            b: 65
        }, {
            y: "Nov.",
            a: 100,
            b: 90
        }, {
            y: "Dec.",
            a: 80,
            b: 65
        }], "y", ["a", "b"], ["Points","Cash Balance"], e);
        
        e = ["#0b8ffb", "#04e397"];

        (a = r("#morris-donut-example").data("colors")) && (e = a.split(",")), this.createDonutChart("morris-donut-example", [{
            label: "Ethereum",
            value: 12
        }, {
            label: "Bitcoin",
            value: 30
        }, {
            label: "Litecoin",
            value: 20
        }], e)
    }, r.MorrisCharts = new e, r.MorrisCharts.Constructor = e
}(window.jQuery),
function() {
    "use strict";
    window.jQuery.MorrisCharts.init()
}();