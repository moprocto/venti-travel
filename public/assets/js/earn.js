jQuery(document).ready(function ($) {
    runCalc();

    $(".calculate").on("change", function(){
      runCalc();
    });

		function runCalc(){
			var poolSize = $("#pool-size").val();
			var poolAmount = $("#pool-amount").val();
			var poolGroup = $("#pool-group").val();
			var poolRate = $("#pool-rate").val();

			var poolPrize = poolAmount * poolSize * poolGroup * 10 + ((poolRate*0.5) * poolGroup * poolAmount);


			var VentiFee = poolPrize * 0.75;

			$("#pool-payout").text(VentiFee.toLocaleString("en-US",{minimumFractionDigits: 2, maximumFractionDigits: 2}));

			//$("#pool-payout").text((calculatePayout(poolAmount, poolSize, poolRate) - VentiFee - stripeFeePerTransaction - stripeFeeProcessingPercentage).toLocaleString("en-US",{minimumFractionDigits: 2, maximumFractionDigits: 2}));
		}

		function calculatePayout(poolAmount, poolSize, poolRate){
			var monthly = poolSize * poolAmount;
			return parseInt(monthly + poolRate);
		}	

		$("#submitApp").click(function(){
			//$(this).addClass("disabled");
			//$(this).attr("disabled", "disabled");
			//$(this).html("<span class='text-center'><i class='fa fa-refresh loading'></i> Submit Application</span>");
		})

		var myCanvas = document.createElement('canvas');
		$("#emitter").append(myCanvas);

		/*

		var myConfetti = confetti.create(myCanvas, {
		resize: true
		});

		myConfetti({
		particleCount: 100,
			spread: 70
		});
		*/

		var screenshotImagesTimeline = ["/assets/img/mockups/screenshots/list-mode-start-iphone-13.png","/assets/img/mockups/screenshots/list-mode-mid-iphone-13.png","/assets/img/mockups/screenshots/list-mode-gallery-iphone-13.png"];

      var screenshotImagesStory = ["/assets/img/mockups/screenshots/story-mode-start-iphone-13.png","/assets/img/mockups/screenshots/story-mode-mid-iphone-13.png","/assets/img/mockups/screenshots/story-mode-gallery-iphone-13.png"];

      const sleep = ms => new Promise(resolve => setTimeout(resolve, ms));

      function startRotationOne(rotatorMode){	
      	(async () => {
      		for (let i = 0; i < screenshotImagesTimeline.length; i++) {
            	$("#screenshot-timeline-view").attr("src", screenshotImagesTimeline[i]);
		    	await sleep(4000);
		  	}
		  startRotationOne();
		})();
      }

      function startRotationTwo(rotatorMode){	
      	(async () => {
      		for (let i = 0; i < screenshotImagesStory.length; i++) {
            	$("#screenshot-story-view").attr("src", screenshotImagesStory[i]);
		    	await sleep(4000);
			}
		  startRotationTwo();
		})();
      }

      startRotationOne();
      startRotationTwo();

 });