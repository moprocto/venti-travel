Apex.grid = {
	padding: {
		right: 0,
		left: 0
	}
}, Apex.dataLabels = {
	enabled: !1
};


colors = ["#f1556c", "#2e2e2e", "#f1556c", "#f1556c", "#f1556c"];

(dataColors = $("#apex-line-1").data("colors")) && (colors = dataColors.split(","));
options = {
	chart: {
		height: 380,
		type: "line",
		shadow: {
			enabled: !1,
			color: "#bbb",
			top: 3,
			left: 2,
			blur: 3,
			opacity: 1
		}
	},
	stroke: {
		width: 5,
		curve: "smooth"
	},
	series: [
	{
		name: "Total",
		data: [90, 40, 60, 80, 120, 160, 2, 260, 320, 380, 440, 500]
	},
	{
		name: "Cash",
		data: [90, 47, 54, 71, 111, 141, 0, 248, 308, 373, 395, 450]
	}
	],
	xaxis: {
		type: "datetime",
		categories: [
			"1/1/2023", 
			"2/1/2023", 
			"3/1/2023", 
			"4/1/2023", 
			"5/1/2023", 
			"6/1/2023",
			"7/1/2023",
			"8/1/2023",
			"9/1/2023",
			"10/1/2023",
			"11/1/2023",
			"12/1/2023"
		]
	},
	title: {
		text: "Avg. Balance in USD",
		align: "center",
		style: {
			fontSize: "14px",
			color: "#666"
		}
	},
	fill: {
		type: "gradient",
		gradient: {
			shade: "dark",
			gradientToColors: colors,
			shadeIntensity: 1,
			type: "horizontal",
			opacityFrom: 1,
			opacityTo: 1,
			stops: [0, 100, 100, 100]
		}
	},
	markers: {
		size: 4,
		opacity: .9,
		colors: ["#2e2e2e"],
		strokeColor: "#fff",
		strokeWidth: 2,
		style: "inverted",
		hover: {
			size: 7
		}
	},
	yaxis: {
		min: 0,
		max: 510,
		title: {
			text: "Average Balance in Dollars"
		}
	},
	grid: {
		row: {
			colors: ["transparent", "transparent"],
			opacity: 0
		},
		borderColor: "#ffffff"
	},
	responsive: [{
		breakpoint: 600,
	}]
};
(chart = new ApexCharts(document.querySelector("#apex-line-1"), options)).render();


colors = ["#feae1b", "#6658dd"];
(dataColors = $("#apex-mixed-1").data("colors")) && (colors = dataColors.split(","));
options = {
    chart: {
        height: 380,
        type: "line"
    },
    stroke: {
        width: 2,
        curve: "smooth"
    },
    series: [{
        name: "Balance",
        type: "area",
        colors: "#feae1b,#6658dd",
        data: [2, 3.5, 6, 10, 8, 12, 14, 18, 22, 26, 30]
    }],
    colors: colors,
    fill: {
        type: "gradient",
        opacity: [.35, 1]
    },
    labels: ["Jan.", "Feb.", "Mar.", "April", "May", "June", "July", "Aug.", "Sept.", "Oct.", "Nov.","Dec."],
    markers: {
        size: 0
    },
    yaxis: [{
        title: {
            text: "Points Balance"
        }
    }],
    tooltip: {
        shared: !0,
        intersect: !1,
        y: {
            formatter: function(e) {
                return void 0 !== e ? e.toFixed(0) + " points" : e
            }
        }
    },
    legend: {
        offsetY: 7
    }
};
(chart = new ApexCharts(document.querySelector("#apex-mixed-1"), options)).render();



