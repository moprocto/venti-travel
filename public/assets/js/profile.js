function calcSummary(){
	var dob = $("#dob").val();
	var myGender = $("#gender").val();

	var size = $("#sizePref").val();
	var age = $("#agePref").val();
	var gender = $("#genderPref").val();
	var spending = $("#spendingPref").val();
	var smoking = $("#smokePref").val();
	var drinking = $("#drinkPref").val();
	var defaultText = "We will priorize matching you ";

	var dob =  moment(dob).diff(moment(), 'years') * -1;
	

	switch(size) {
	  case "small":
	    size = "of no more than four"
	    break;
	  case "medium":
	  	size = "of no more than 10"
	    // code block
	    break;
	  case "large":
	  	size = "of no more than 20"
	  	break;
	  default:
	}

	switch(age) {
	  case "small":
	    age = " between the ages of " + calcMinAge(dob, 4, true) + " and " + calcMinAge(dob, 4, false);
	    break;
	  case "medium":
	  	age = " between the ages of " + calcMinAge(dob, 8, true) + " and " + calcMinAge(dob, 8, false);
	    // code block
	    break;
	  case "large":
	  	age = " between the ages of " + calcMinAge(dob, 16, true) + " and " + calcMinAge(dob, 16, false);
	  	break;
	  default:
	  	age = "none"
	}

	$("#summary").text(defaultText);

	if(size != "none"){
		defaultText += "into groups " + size;
	} else {
		defaultText += " with any size group "
	}

	if(gender == "same"){
		defaultText += " " + calcGender(myGender, gender);
	}

	if(age != "none" && $("#dob").val() != ""){
		defaultText += age;
	}

	if(size != "none" || gender != "none" || age != "none"){
		defaultText += " that fit your above criteria.";
	}

	

	

	$("#summary").text(defaultText);
}

function calcGender(mine, theirs){
	if(theirs == "none"){
		return "people";
	}

	if(mine != "none" && theirs == "same"){
		return mine;
	}
	return "";
}

function calcMinAge(dob, range, bound){
	var lowerBound = parseInt(dob - range);
	var upperBound = parseInt(dob + range);

	if(bound  && lowerBound < 18){
		// is lower range
		return 18;
	}
	if(!bound && upperBound < 18){
		return 18;
	}

	if(bound){
		return lowerBound;
	}
	return upperBound;
}