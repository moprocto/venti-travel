$(".value-button").click(function(){
				// calc number of travelers
				var adults = parseInt($("#adultNumber").val());
				var children = parseInt($("#childrenNumber").val());
				var infants = parseInt($("#infantsNumber").val());

				var passengers = parseInt(adults + children + infants);
				if(children != 0 || infants != 0){
					$("#passengers").text(passengers + " Travelers");
				} else {
					$("#passengers").text(passengers + " Adults");
				}
				
		    });

		    $('#flightSearch').submit(function(e){
			  //e.preventDefault();
			});

			initFlightTo();
			initFlightFrom();

			$("#flipLocations").click(function(){
				var flightFrom = $('#flight-from').select2('data')[0];
				var flightTo = $('#flight-to').select2('data')[0];

				$("#flight-from").select2('destroy');
				clearOptions("flight-from");
				$('#flight-from').append('<option value="1">' + flightTo.text + '</option>')
				$("#flight-from").select2();

				initFlightFrom();

				$("#flight-to").select2('destroy');
				clearOptions("flight-to");
				$('#flight-to').append('<option value="1">' + flightFrom.text + '</option>')
				$("#flight-to").select2();

				initFlightTo();
			});

			$('#search').click(function(){
				var flightFrom = $('#flight-from').select2('data')[0];
				var flightTo = $('#flight-to').select2('data')[0];
				var flightType = $('.flight-type[type=radio]:checked').val();
				var flightClass = $('#flight-class').val();
				var flightStartDate = $('.datepicker').val().split(" - ")[0];
				var flightReturnDate = $('.datepicker').val().split(" - ")[1];


				var adults = $("#adultNumber").val();
				var children = $("#childrenNumber").val();
				var infants = $("#infantsNumber").val();

			  	var fieldCheck = checkMandatoryFields(flightFrom, flightTo, flightType, flightClass, flightStartDate, flightReturnDate, adults, children, infants);

			  	if(fieldCheck === true){
			  		window.location.href = "/flights/search" +
			  		"?from=" + flightFrom.text + 
			  		"&to=" + flightTo.text + 
			  		"&type=" + flightType + 
			  		"&class=" + flightClass + 
			  		"&start=" + flightStartDate +  
			  		"&end=" + flightReturnDate + 
			  		"&adults=" + adults + 
			  		"&children=" + children + 
			  		"&infants=" + infants
			  	}
			});

			

			function checkMandatoryFields(flightFrom, flightTo, flightType, flightClass, flightStartDate, flightReturnDate, adults, children, infants){
				console.log(flightFrom);

				if(isBlank(flightFrom.text) || flightFrom.text == "Select Origin"){
					swal({
	                      icon: 'error',
	                      title: 'Missing required fields',
	                      text: 'Please add where you are flying from',
		            });

		            return false;
				}

				if(isBlank(flightTo.text) || flightTo.text == "Select Destination"){
					swal({
	                      icon: 'error',
	                      title: 'Missing required fields',
	                      text: 'Please add where you are flying to',
		            });

		            return false;
				}

				if(isBlank(flightType)){
					swal({
	                      icon: 'error',
	                      title: 'Missing required fields',
	                      text: 'You need to select your flight type',
		            });

		            return false;
				}

				if(isBlank(flightClass)){
					swal({
	                      icon: 'error',
	                      title: 'Missing required fields',
	                      text: 'You need to select your seat class',
		            });

		            return false;
				}



				if(adults == 0 && children == 0 && infants == 0){
					swal({
	                      icon: 'error',
	                      title: 'Missing required fields',
	                      text: 'You need to input at least one traveler',
		            });
		            return false;
				}
				if(infants >= 1 && adults == 0){
					swal({
	                      icon: 'error',
	                      title: 'Missing required fields',
	                      text: 'You need to input at least one adult',
		            });
		            return false;
				}

				return true;
			}

			function isBlank(value){
				if(value == "" || value == undefined || value == null){
					return true;
				}
				return false;
			}

			function clearOptions(elementId) {
			   var courseSelect = document.querySelector("#" + elementId);

			   while (courseSelect.childNodes[0] != null) {
			       courseSelect.childNodes[0].remove();
			   }
			}

			var dayDif = 0;

			$(".flight-type[type=radio]").click(function(){
				var flightStartDate = $('.datepicker').val().split(" - ")[0];
				var flightReturnDate = $('.datepicker').val().split(" - ")[1];
				if(!isBlank(flightReturnDate)){
					dayDif = moment(flightReturnDate).diff(moment(flightStartDate), 'days', false);

				}
				
				if($(this).val() == "one-way"){
					$('.datepicker').daterangepicker({
					    locale: {
					          format: 'MM/DD/YYYY',
					    },
					    singleDatePicker: true,
					    showDropdowns: true,
					    startDate: flightStartDate,
					    drops: "up",
						opens:"left",
						minDate: moment().format("MM/DD/YYYY"),
						maxDate: moment().add(10, 'M').format("MM/DD/YYYY")
					});
				} else {

					$('.datepicker').daterangepicker({
					    locale: {
					          format: 'MM/DD/YYYY',
					    },
					    showDropdowns: true,
					    startDate: flightStartDate,
					    endDate: (dayDif > 0) ? moment(flightStartDate).add(dayDif, 'days').format("MM/DD/YYYY") : flightReturnDate,
					    drops: "up",
						opens:"left",
						minDate: moment().format("MM/DD/YYYY"),
						maxDate: moment().add(10, 'M').format("MM/DD/YYYY")
					});
				}
			});

		$('input[name="dates"]').daterangepicker({
			drops: "up",
			opens:"left",
			minDate: moment().format("MM/DD/YYYY"),
			maxDate: moment().add(10, 'M').format("MM/DD/YYYY")
		});

		function initFlightFrom(){
			return $("#flight-from").select2({
			    placeholder: "Select Origin",
			    minimumInputLength: 3,
			    language: {
				    'inputTooShort': function () {
				        return '';
				    }
				},
			    delay:250,
			    data: function (term) {
		            return {
		                term: term
		            };
		        },
				ajax: {
				    url: '/flights/data/airports',
				    dataType: 'json',
			        processResults: function (data) {

			            return {

			                results: $.map(data, function (item) {

			                	var item = JSON.parse(item);


			                	return item.map((airport, i) => {

			                		return {
				                        text: airport.text,
				                        id: airport.id,
				                    }
			                	});
			                })
			            };
			        }
				}
			});
		}

		function initFlightTo(){
			return $("#flight-to").select2({
		    placeholder: "Select Destination",
		    delay:250,
		    minimumInputLength: 3,
		    data: function (term) {
	            return {
	                term: term
	            };
	        },
			ajax: {
			    url: '/flights/data/airports',
			    dataType: 'json',
		        processResults: function (data) {
		            return {
		                results: $.map(data, function (item) {
		                	var item = JSON.parse(item);

		                	return item.map((airport, i) => {

		                		return {
			                        text: airport.text,
			                        id: airport.id
			                    }
		                	});
		                })
		            };
		        }
			}
		});
		}

		function increaseAdultCount() {
		  var value = parseInt(document.getElementById('adultNumber').value, 10);
		  value = isNaN(value) ? 0 : value;
		  value++;
		  document.getElementById('adultNumber').value = value;
		}

		function decreaseAdultCount() {
		  var value = parseInt(document.getElementById('adultNumber').value, 10);
		  value = isNaN(value) ? 0 : value;
		  value < 1 ? value = 1 : '';
		  value--;
		  document.getElementById('adultNumber').value = value;
		}

		function increaseChildrenCount() {
		  var value = parseInt(document.getElementById('childrenNumber').value, 10);
		  value = isNaN(value) ? 0 : value;
		  value++;
		  document.getElementById('childrenNumber').value = value;
		}

		function decreaseChildrenCount() {
		  var value = parseInt(document.getElementById('childrenNumber').value, 10);
		  value = isNaN(value) ? 0 : value;
		  value < 1 ? value = 1 : '';
		  value--;
		  document.getElementById('childrenNumber').value = value;
		}

		function increaseInfantsCount() {
		  var value = parseInt(document.getElementById('infantsNumber').value, 10);
		  value = isNaN(value) ? 0 : value;
		  value++;
		  document.getElementById('infantsNumber').value = value;
		}

		function decreaseInfantsCount() {
		  var value = parseInt(document.getElementById('infantsNumber').value, 10);
		  value = isNaN(value) ? 0 : value;
		  value < 1 ? value = 1 : '';
		  value--;
		  document.getElementById('infantsNumber').value = value;
		}

		$(document).on('select2:open', function(e) {
  			document.querySelector(`[aria-controls="select2-${e.target.id}-results"]`).focus();
		});

function getDiscountPercent(productClass, msrp){
    if (productClass === 'airfare') {
	    	var newPrice = msrp - 250;
	        if (msrp > 5000) {
	            // 5 percent off business and first class flights
	            return (newPrice / msrp);
	        } else if (msrp < 5000 && msrp >= 2000) {
	            // 10 percent off premium economy
	            return (newPrice / msrp);
	        } else if (msrp < 2000 && msrp >= 500) {
	            // 15 percent off economy
	            return (newPrice / msrp);
	        } else if (msrp < 500 && msrp >= 250) {
	            // 30 percent off domestic economy
	            return (newPrice / msrp);
	        } else {
	            // Handle any other msrp ranges if needed
	            return 0; // No discount applied
	        }
	    } else {
	        // Handle other product classes if needed
	        return newPrice; // Default return value when not 'airfare'
	    }
}