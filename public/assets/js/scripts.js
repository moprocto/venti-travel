jQuery(document).ready(function ($) {
	$("#hamburger-menu").click(function(){
	  $("body").toggleClass("mobile-nav-open");
	  $(".hamburger-menu-list").toggleClass("active");
	});
});

function checkIfEmpty(str){
	if ((str?.trim()?.length || 0) > 0) {
		return true;
	}
	return false;
}