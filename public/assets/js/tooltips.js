$(document).ready(function() {

	// Initialize Tippy.js for the top tooltip
	tippy.setDefaultProps({
        theme: 'gradient-no-arrow',
        hideOnClick: true, // Prevent hiding when clicking inside the tooltip
        interactive: false, // Allow interaction with tooltip content
    });

    tippy('.loan-1', {
        content: "<i class='fa fa-bolt'></i> New Account",
        placement: 'left-end',
        multiple: true,
        theme: 'gradient-no-arrow-1',
        duration: [100,200],
        distance:5
    });

    // Initialize Tippy.js for the bottom tooltip
    tippy('.loan-1', {
        content: "<i class='fa fa-bolt'></i> Payment",
        placement: 'top-start',
        multiple: true,
        theme: 'gradient-no-arrow-1',
        duration: [500,100],
        distance:5
    });

    // Initialize Tippy.js for the bottom tooltip
    tippy('.loan-2', {
        content: "<i class='fa fa-bolt'></i> Refinance",
        placement: 'bottom',
        multiple: true,
        theme: 'gradient-no-arrow-1',
        duration: [500,100],
        distance:5
    });

    tippy('.loan-3', {
        content: "<i class='fa fa-bolt'></i> Transact",
        placement: 'right',
        multiple: true,
        theme: 'gradient-no-arrow-1',
        duration: [500,100],
        distance:5
    });

    tippy('.deposit-1', {
        content: "<i class='fa fa-bolt'></i> Transact",
        placement: 'left',
        multiple: true,
        theme: 'gradient-no-arrow-1',
        duration: [500,100],
        distance:5
    });

    tippy('.deposit-1', {
        content: "<i class='fa fa-bolt'></i> Balance",
        placement: 'top',
        multiple: true,
        theme: 'gradient-no-arrow-1',
        duration: [500,100],
        distance:5
    });

    tippy('.deposit-1', {
        content: "<i class='fa fa-bolt'></i> Volume",
        placement: 'right',
        multiple: true,
        theme: 'gradient-no-arrow-1',
        duration: [500,100],
        distance:5
    });

    tippy('.activity-1', {
    	content: "<i class='fa fa-bolt'></i> Completion",
        placement: 'top',
        multiple: true,
        theme: 'gradient-no-arrow-1',
        duration: [500,100],
        distance:5
    });

    tippy('.activity-2', {
    	content: "<i class='fa fa-bolt'></i> Trend",
        placement: 'right',
        multiple: true,
        theme: 'gradient-no-arrow-1',
        duration: [500,100],
        distance:5
    });

    tippy('.activity-2', {
    	content: "<i class='fa fa-bolt'></i> Trend",
        placement: 'right',
        multiple: true,
        theme: 'gradient-no-arrow-1',
        duration: [500,100],
        distance:5
    });

    tippy('.activity-3', {
    	content: "<i class='fa fa-bolt'></i> Date",
        placement: 'top',
        multiple: true,
        theme: 'gradient-no-arrow-1',
        duration: [500,100],
        distance:5
    });
});