jQuery(document).ready(function ($) {
	

	var container = document.getElementById('build-wrap');
		var fields = [
		{
			label: '🍔 Restaurant', 
			attrs: {type: 'restaurant'}
		},
		{
			label: '⏰ Activity',
			attrs: {type: 'activity'}
		},
		{
			label: '🛏 Accomodation',
			attrs: {type: 'accomodation'}
		},
		{
			label: '📍 City',
			attrs: {type: 'city'}
		},
		
	  ];
	  var templates = {
	    restaurant: function(fieldData) {
	      return {
	        field: '<span id="' + fieldData.name + '">',
	        onRender: function() {
	        	var randomID = Math.floor(Math.random() * 100000000000);
	          $(document.getElementById(fieldData.name)).html("<div class='form-group indent'><label class='form-label'>Name:</label><input type='text' class='form-control' placeholder='Name of Restaurant' name='restaurant[]'></div><div class='form-group'><label class='form-label description'>Description:</label><textarea class='form-control description' rows='1' id='d" + randomID +"'></textarea></div><div class='form-group'><label class='form-label description'>Photos (Max 4):</label><div class='image-upload-gallery'><input type='file' class='imageuploadify' id='p" + randomID +"' accept='.xlsx,.xls,image/*,.doc,audio/*,.docx,video/*,.ppt,.pptx,.txt,.pdf' multiple></div></div>");
	        }
	      };
	    },
	    activity: function(fieldData) {
	      return {
	        field: '<span id="' + fieldData.name + '">',
	        onRender: function() {
	        	var randomID = Math.floor(Math.random() * 100000000000);
	          $(document.getElementById(fieldData.name)).html("<div class='form-group indent'><label class='form-label'>Name:</label><input type='text' class='form-control' placeholder='Name of Activity'></div><div class='form-group'><label class='form-label description'>Description:</label><textarea class='form-control description' rows='1' id='d" + randomID +"'></textarea></div><div class='form-group'><label class='form-label description'>Photos (Max 4):</label><div class='image-upload-gallery'><input type='file' class='imageuploadify' id='p" + randomID +"' accept='.xlsx,.xls,image/*,.doc,audio/*,.docx,video/*,.ppt,.pptx,.txt,.pdf' multiple></div></div>");
	        }
	      };
	    },
	    accomodation: function(fieldData) {
	      return {
	        field: '<span id="' + fieldData.name + '">',
	        onRender: function() {
	        	var randomID = Math.floor(Math.random() * 100000000000);
	          $(document.getElementById(fieldData.name)).html("<div class='form-group indent'><label class='form-label'>Name:</label><input type='text' class='form-control'></div><div class='form-group'><label class='form-label description'>Description:</label><textarea class='form-control description' rows='1' id='d" + randomID +"'></textarea></div><div class='form-group'><label class='form-label description'>Photos (Max 4):</label><div class='image-upload-gallery'><input type='file' class='imageuploadify' id='p" + randomID +"' accept='.xlsx,.xls,image/*,.doc,audio/*,.docx,video/*,.ppt,.pptx,.txt,.pdf' multiple></div></div>");
	        }
	      };
	    },
	    city: function(fieldData) {
	      return {
	        field: '<span id="' + fieldData.name + '">',
	        onRender: function() {
	        	console.log(fieldData);
	        	var randomID = Math.floor(Math.random() * 100000000000);
	          $(document.getElementById(fieldData.name)).html("<div class='form-group indent'><label class='form-label'>Name:</label><input type='text' class='form-control' placeholder='Name of City'></div><div class='form-group'><label class='form-label description'>Description:</label><textarea class='form-control description' rows='1' id='d" + randomID +"'></textarea></div><div class='form-group'><label class='form-label description'>Photos (Max 4):</label><div class='image-upload-gallery'><input type='file' class='imageuploadify' id='p" + randomID +"' accept='.xlsx,.xls,image/*,.doc,audio/*,.docx,video/*,.ppt,.pptx,.txt,.pdf' multiple></div></div>");
	        }
	      };
	    }
	  };

	  var renderOpts = {
		  controlConfig: {
		    'textarea.tinymce': {
		      paste_data_images: false
		    }
		  }
		};

	  	var formBuilder = $(container).formBuilder({
		    templates,
		    fields,
		    renderOpts,
		    render: true,
		    scrollToFieldOnAdd: false,
		    onAddField: function(fieldId) {
			    setTimeout(function() { 
			        initMCE();
			    }, 1000);
			},
	  	});

	  	function initMCE(){
	  		$("textarea.description").each(function(){
	  			var targetDescriptionID = $(this).attr("id");
	  			var gallerDescriptionID = $(this).parent().parent().find(".imageuploadify").attr("id");
	  			tinymce.init({
			      selector: '#' + targetDescriptionID,
			      height: 150,
			      menubar: false,
			      plugins: [
			        'link'
			      ],
			      toolbar: 'undo redo | formatselect | ' +
			      'bold italic link | ' +
			      'removeformat',
			      content_style: 'body { font-family:Helvetica,Arial,sans-serif; font-size:14px }'
			    });
			    console.log('#' + gallerDescriptionID);

			    if($(this).parent().parent().find(".image-upload-gallery").find(".well").length == 0){
			    	$('#' + gallerDescriptionID).imageuploadify();
			    }
	  		});
	  		
	  		
	  	}

	  	document.getElementById('setDays').addEventListener('change', function() {
	  		
	  		var numDays = $(this).val();
	  		var formData = new Array();
	  		for(i = 1; i <= numDays; i++){
	  			var randomID = Math.floor(Math.random() * 100000000000);
	  			formData.push('{"type": "header","subtype": "h2","label": "Day ' + i + '","access": false},{"type": "city","required": false,"label": "📍 City","name": "city-' + randomID +'-0","access": false}');
	  		}
		    formBuilder.actions.setData("[" + formData.toString() + "]");
		    //console.log();
		});


	  	$('.js-example-basic-multiple').select2({
        	placeholder: "Select tags",
        	maximumSelectionLength: 4,
        	closeOnSelect: false,
        	allowClear: true
    	});

	  tinymce.init({
	      selector: 'textarea#purchaseNote',
	      height: 200,
	      menubar: false,
	      plugins: [
	        'link'
	      ],
	      toolbar: 'undo redo | formatselect | ' +
	      'bold italic link | ' +
	      'removeformat',
	      content_style: 'body { font-family:Helvetica,Arial,sans-serif; font-size:14px }'
	    });

	  

	    tinymce.init({
	      selector: 'textarea#personalizationNote',
	      height: 200,
	      menubar: false,
	      plugins: [
	        'link',
	      ],
	      toolbar: 'undo redo | formatselect | ' +
	      'bold italic link | ' +
	      'removeformat',
	      content_style: 'body { font-family:Helvetica,Arial,sans-serif; font-size:14px }'
	    });
});