<?php

namespace App\Services\Mailchimp;

use Illuminate\Support\Facades\Facade;

use Newsletter;

class MailchimpFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'Mailchimp';
    }

    public static function getSubscribers(){
        dd(Newsletter::getMembers());
    }
}