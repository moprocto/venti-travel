<?php

namespace app\Services\Plaid;

use Illuminate\Support\ServiceProvider;

class PlaidServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->singleton('Plaid', function($app) {
            return new Plaid();
        });
    }
}