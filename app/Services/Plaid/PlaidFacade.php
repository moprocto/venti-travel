<?php

namespace App\Services\Plaid;

use TomorrowIdeas\Plaid\Plaid;
use TomorrowIdeas\Plaid\Entities\User as PlaidUser;
use TomorrowIdeas\Plaid\PlaidRequestException as PlaidRequestException;
use Session;

class PlaidFacade
{
    protected $plaid;

    public function __construct()
    {
        $this->plaid = new Plaid(
            env("PLAID_" . strtoupper(env("APP_ENV")) . "_CLIENT"), 
            env("PLAID_" . strtoupper(env("APP_ENV")) . "_KEY"), 
            env("PLAID_" . strtoupper(env("APP_ENV")) . "_ENV"),
        );
    }

    public function createLinkToken()
    {
        $user = Session::get('user');
        $claims = $user->customClaims;
        $cell = $claims["phoneNumber"];

        $tokenUser = new PlaidUser($user->uid, cleanName($user->displayName), $cell, null, $user->email); 

        $client = new \TomorrowIdeas\Plaid\Plaid(
            env("PLAID_" . strtoupper(env("APP_ENV")) . "_CLIENT"), 
            env("PLAID_" . strtoupper(env("APP_ENV")) . "_KEY"), 
            env("PLAID_" . strtoupper(env("APP_ENV")) . "_ENV"),
        );

        try{
            $token = $this->plaid->tokens->create(
                "venti",
                "en",
                ['US'],  // Country codes
                $tokenUser,
                ['auth', 'transactions']
            );

            return $token;
        }
        catch(PlaidRequestException $e){
            dd($e);
        }  
    }

    public function exchangePublicToken($publicToken)
    {
        return $this->plaid->items->exchangeToken($publicToken);
    }

    public function getItem($accessToken)
    {
        return $this->plaid->items->get($accessToken);
    }

    public function getAccountInfo($accessToken)
    {
        return $this->plaid->accounts->list($accessToken);
    }

    public function getAuth($accessToken){
        return $this->plaid->auth->list($accessToken);
    }

    public function getBalance($accessToken)
    {
        return $this->plaid->accounts->getBalance($accessToken);
    }

    public function getBankName($institutionId)
    {
        $institutionResponse = $this->plaid->institutions->get($institutionId, ['US']);
        return $institutionResponse->institution;
    }

    public function getTransactions($accessToken, $startDate, $endDate, $accountIds){
        try{
            return $this->plaid->transactions->list(
                $accessToken,
                $startDate,
                $endDate,
                [], // optional: count (number of transactions to return)
                [] // optional: offset (transactions offset)
            );
        }
        catch(PlaidRequestException $e){
            //
        }
    }

    public function getAccountBalance($accessToken, $accountId)
    {

        // Fetch balance for the provided account ID
        $balanceResponse = $this->plaid->accounts->getBalance($accessToken, ["account_ids" => [$accountId]]);

        return $balanceResponse;
    }

    public function removeItem($accessToken){
        return $this->plaid->items->remove($accessToken);
    }
}