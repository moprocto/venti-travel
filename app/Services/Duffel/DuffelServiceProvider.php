<?php

namespace App\Services\Duffel;

use Illuminate\Support\ServiceProvider;

class DuffelServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->singleton('Duffel', function($app) {
            return new MailGun();
        });
    }
}