<?php

namespace App\Services\Duffel;

use Illuminate\Support\Facades\Facade;
use Illuminate\Support\Facades\Http;

class DuffelFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'Duffel';
    }

    public static function getSubscribers(){

    }

    public function auth(){
        // grab an authorization token from the API provider
    }

    public static function createLink(){

        $headers = [
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
            'Duffel-Version' => 'v1',
            "Authorization" => "Bearer " . env('DUFFEL_API_KEY'),
            "Accept-Encoding" => "gzip"
        ];

        $body = [
            "traveller_currency" => "USD",
            "success_url" => "https://venti.co/booking/flights/success",
            "should_hide_traveller_currency_selector" => "true",
            "secondary_color" => "#000000",
            "reference" => "user_123", // the firebase uid
            "primary_color" => "#000000",
            "markup_rate" => "0.1",
            "markup_currency" => "USD",
            "markup_amount" => "10.00",
            "logo_url" => "https://venti.co/assets/img/venti-trademark-logo-black.png",
            "failure_url" => "https://venti.co/booking/failure",
            "checkout_display_text" => "Thank you for booking with us.",
            "abandonment_url" => "https://venti.co/booking/flights/timeout"
        ];

        $url = env("DUFFEL_API_URL") . '/links/sessions';

        $response = Http::withHeaders($headers)->post($url, $body);
        
        $result = json_decode($response->body(),true);

        return dd($result);
    }
}