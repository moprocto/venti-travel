<?php

namespace App\Services\Stripe;

use Illuminate\Support\Facades\Facade;

$public = base_path();
require_once($public . '/vendor/stripe/stripe-php/init.php');

\Stripe\Stripe::setApiKey(env('STRIPE_SECRET_' . strtoupper(env('APP_ENV'))));
use \Stripe\Customer as Customer;
use \Stripe\StripeClient as Client;
use Stripe\PaymentMethod as Payment;

use DB;
use Auth;
use Email;


class StripeFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'Stripe';
    }

    /*
        fees:   2.9% + .30 for non AMEX
                3.5% for amex
    */

    public static function checkCustomerExists($email)
    {
      try {
        $customer = Customer::all(["email" => $email]);
        if(sizeof($customer->data) == 0){
            return false;
        }
        return $customer->data[0];
      } catch (\Stripe\Exception\InvalidRequestException $e) {
        return false;
      }
    }

    public static function createCustomer($displayName, $email, $phoneNumber){
        $customer = Customer::create(
        array(
            "name" => cleanName($displayName),
            "description" => cleanName($displayName),
            "phone" => $phoneNumber,
            "email" => $email
        ));

        return $customer;
    }

    public static function saveCard($customerId, $paymentMethodId){
        $error = "";
        try {

            $paymentMethod = \Stripe\PaymentMethod::retrieve($paymentMethodId);
            $paymentMethod->attach(['customer' => $customerId]);

            return true;
        }
        catch (\Stripe\Exception\CardException $e) {
            $error = "We cannot add this payment method to your account. " . $e->getMessage();
        }

        return $error;
    }

    public static function deleteCustomer($customerId){
        $customer = \Stripe\Customer::retrieve($customerId);
        $customer->delete();
    }

    public static function createPaymentIntent($amountDue, $paymentMethodId, $customerId, $description, $metadata = null){
        try{
            $paymentIntent = \Stripe\PaymentIntent::create([
                'amount' => $amountDue,
                'currency' => "USD",
                'payment_method' => $paymentMethodId,
                'confirm' => true, // Automatically confirm the Payment Intent
                'customer' => $customerId,
                'automatic_payment_methods[enabled]' => true,
                'automatic_payment_methods[allow_redirects]' => "never",
                'statement_descriptor' => $description,
                'metadata' => $metadata
            ]);

            return $paymentIntent;
        }
        catch(\Stripe\Exception\StripeException $e){
            return $e->getMessage();
        }
        catch(\Stripe\Exception\CardException $e){
            return $e->getMessage();
        }
    }

    public static function chargePaymentIntent($paymentIntentId){
        try {
            $paymentIntent = \Stripe\PaymentIntent::retrieve($paymentIntentId);
            $paymentIntent->confirm();
        }
        catch(\Stripe\Exception\StripeException $e){
            dd($e);
        }
    }

    public static function refundPayment($chargeID){
        if(!is_string($chargeID)){
            try {
                $refund = \Stripe\Refund::create([
                    'charge' => $chargeID->latest_charge
                ]);

                return $refund;
            }
            catch(\Stripe\Exception\StripeException $e){
                return false;
            }
        }
        else {
            return false;
        }
    }


    public function deleteCard(){

    }

    public static function getBalance(){
        foreach (\Stripe\Balance::retrieve()->available as $key => $value) {
            $balance[$key] = $value;
        }
        return $balance[0]->amount / 100;
    }

    public static function retrieveCard($customer, $cardID){
        $customer = $this->retrieveCustomer($customer);
        return $customer->sources->retrieve($cardID);
    }


    public static function retrieveCustomer($customer){
        return \Stripe\Customer::retrieve($customer);
    }



    public static function captureCharge($chargeID){

        $ch = \Stripe\Charge::retrieve("$chargeID");

        $ch->capture();

        DB::table('payments')->where('token', $chargeID)->update([
            'captured' => 1
        ]);

        return $ch;

    }
}