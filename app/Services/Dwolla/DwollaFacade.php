<?php

namespace App\Services\Dwolla;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Facade;
use Illuminate\Support\Facades\Http;
use DwollaSwagger\Configuration as Configuration;
use DwollaSwagger\ApiClient as apiClient;
use DwollaSwagger\TokensApi as TokensApi;
use DwollaSwagger\CustomersApi as CustomersApi;
use DwollaSwagger\ApiException as DwollaException;
use DwollaSwagger\FundingsourcesApi as FundingSourcesApi;
use DwollaSwagger\AccountsApi as AccountsApi;
use DwollaSwagger\TransfersApi as TransfersApi;
use DwollaSwagger\WebhooksubscriptionsApi as WebhooksubscriptionsApi;
use DwollaSwagger\WebhooksApi as WebhooksApi;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException as GuzzleException;

use Session;

Configuration::$username = env("DWOLLA_KEY_" . strtoupper(env("APP_ENV")));
Configuration::$password = env("DWOLLA_SECRET_" . strtoupper(env("APP_ENV")));

class DwollaFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'Dwolla';
    }

    public function auth(){
        //
        # For Sandbox

        $apiClient = new ApiClient(env("DWOLLA_URL_" . strtoupper(env("APP_ENV"))));

        $tokensApi = new TokensApi($apiClient);
        $appToken = $tokensApi->token();

        Configuration::$access_token = $appToken->access_token;

        return $appToken->access_token;

        // store the token into a session, so we don't have to keep getting new tokens
    }

    public function getCustomer($user, $auth = null, $db = null){
        // get the customer ID from firebase

        if(is_null($auth)){
            $firestore = app('firebase.firestore');
            $db = $firestore->database();
            $auth = app('firebase.auth');
        }

        $customer = $db->collection("Customers");
        $customer = $customer->where("uid","=",$user->uid)->documents();
        $customerData = null;

        foreach($customer as $cus){
            $customerData = $cus->data();
        }

        if(isset($customerData)){
            return $customerData;
        }

        return false;
    }

    public function createCustomer($user, $customerData, $customerType){

        $appToken = Configuration::$access_token;

        if(empty($appToken)){
            $appToken = $this->auth();
        }

        $phoneNumber = str_replace("+1","",$user->customClaims["phoneNumber"]) ?? false;

        try{
            $apiClient = new ApiClient(env("DWOLLA_URL_" . strtoupper(env("APP_ENV"))));
            $customersApi = new CustomersApi($apiClient);

            $customer = $customersApi->create([
              'firstName' => getFirstName($user->displayName),
              'lastName' => getLastName($user->displayName),
              'email' => $user->email,
              'ipAddress' => \Request::ip(),// ip
              'phone' => $phoneNumber,// phone number 10 digits
              'type' => $customerType,
              'correlationId' => $user->uid,
              'postalCode' => $customerData->zipcode,
              'address1' =>  $customerData->address1,
              'address2' => $customerData->address2,
              'city' => $customerData->city,
              'state' => $customerData->state,
              'ssn' => $customerData->ssn,
              'dateOfBirth' => $customerData->dob
            ]);

            return $customer;

            // successful response https://api-sandbox.dwolla.com/customers/135d9d2e-9c28-4cba-bb62-ac62d90e10a7
        }
        catch (DwollaException $e){
            // something went wrong with the connection
            // customer exists already
            dd($e);
            return false;
        }
        
    }

    public function readCustomer($customerUrl){
        $apiClient = new ApiClient(env("DWOLLA_URL_" . strtoupper(env("APP_ENV"))));
        $appToken = Configuration::$access_token;

        if(empty($appToken)){
            $appToken = $this->auth();
        }

        $customersApi = new CustomersApi($apiClient);
        $customer = $customersApi->getCustomer($customerUrl);

        return $customer;
    }

    public function updateCustomer($user, $customerData, $customerUrl){
        $appToken = Configuration::$access_token;

        if(empty($appToken)){
            $appToken = $this->auth();
        }

        $phoneNumber = str_replace("+1","",$user->customClaims["phoneNumber"]) ?? false;

        try{
            $apiClient = new ApiClient(env("DWOLLA_URL_" . strtoupper(env("APP_ENV"))));
            $customersApi = new CustomersApi($apiClient);

            $customer = $customersApi->updateCustomer([
              'firstName' => getFirstName($user->displayName),
              'lastName' => getLastName($user->displayName),
              'email' => $user->email,
              'ipAddress' => \Request::ip(),// ip
              'phone' => $phoneNumber,// phone number 10 digits
              'type' => 'personal',
              'correlationId' => $user->uid,
              'postalCode' => $customerData->zipcode,
              'address1' =>  $customerData->address1,
              'address2' => $customerData->address2,
              'city' => $customerData->city,
              'state' => $customerData->state,
              'ssn' => $customerData->ssn,
              'dateOfBirth' => $customerData->dob
            ], $customerUrl);

            return $customer;

            // successful response https://api-sandbox.dwolla.com/customers/135d9d2e-9c28-4cba-bb62-ac62d90e10a7
        }
        catch (DwollaException $e){
            // something went wrong with the connection
            // customer exists already

            return false;
        }
    }

    public function updateCustomerName($name,$lname, $customerUrl){
        $appToken = Configuration::$access_token;

        if(empty($appToken)){
            $appToken = $this->auth();
        }

        try{
            $apiClient = new ApiClient(env("DWOLLA_URL_" . strtoupper(env("APP_ENV"))));
            $customersApi = new CustomersApi($apiClient);

            $customer = $customersApi->updateCustomer([
              'firstName' => $name,
              'lastName' => $lname
            ], $customerUrl);

            return $customer;

            // successful response https://api-sandbox.dwolla.com/customers/135d9d2e-9c28-4cba-bb62-ac62d90e10a7
        }
        catch (DwollaException $e){
            // something went wrong with the connection
            // customer exists already
            dd($e);

            return false;
        }
    }

    public function uploadCustomerDocument($customerID, $documentType, $file){
        $apiClient = new ApiClient(env("DWOLLA_URL_" . strtoupper(env("APP_ENV"))));
        $appToken = Configuration::$access_token;

        if(empty($appToken)){
            $appToken = $this->auth();
        }

        $client = new \GuzzleHttp\Client(['base_uri' => env('DWOLLA_URL_' . strtoupper(env('APP_ENV')) )]);

        try{
            $response =  $client->post("/customers/$customerID/documents", [
                 'headers' => [
                     'Accept' => 'application/vnd.dwolla.v1.hal+json',
                     'Authorization' => 'Bearer ' . $appToken,
                 ],
                 'multipart' => [
                     [
                         'name' => 'documentType',
                         'contents' => $documentType
                     ],
                     [
                         'Content-type' => 'multipart/form-data',
                         'name'     => 'file',
                         'contents' => $file->get()
                     ]
                 ]
             ]);

        $response = $response->getHeader('location');

        if(array_key_exists(0, $response)){
            // document uploaded successfully :)
            return $response[0];
        }
        }
        catch(GuzzleException $e){
            return false;
        }

        

        return false;
    }

    public function readAccounts($customerUrl){
        $apiClient = new ApiClient(env("DWOLLA_URL_" . strtoupper(env("APP_ENV"))));
        $accountsApi = new AccountsApi($apiClient);
        $account = $accountsApi->id($customerUrl);

        return $account;
    }

    public function fundingSources($customerUrl){
        $apiClient = new ApiClient(env("DWOLLA_URL_" . strtoupper(env("APP_ENV"))));
        $appToken = Configuration::$access_token;

        if(empty($appToken)){
            $appToken = $this->auth();
        }

        $fsApi = new FundingsourcesApi($apiClient);

        $fundingSources = $fsApi->getCustomerFundingSources($customerUrl);
        $fundingSources = $fundingSources->_embedded->{"funding-sources"} ?? [];
        $sources = [];

        foreach($fundingSources as $fundingSource){
            array_push($sources, $fundingSource);
        }

        return $sources;
    }

    public function createFundingSource($customer, $customerData){
        $apiClient = new ApiClient(env("DWOLLA_URL_" . strtoupper(env("APP_ENV"))));
        $appToken = Configuration::$access_token;

        if(empty($appToken)){
            $appToken = $this->auth();
        }

        $fsApi = new FundingsourcesApi($apiClient);

        try{
            $fundingSource = $fsApi->createCustomerFundingSource([
              "routingNumber" => $customerData->routingNumber,
              "accountNumber" => $customerData->accountNumber,
              "bankAccountType" => strtolower($customerData->bankAccountType),
              "name" => $customerData->name
            ], $customer["customerID"]);

            return $fundingSource;
        }
        catch(DwollaException $e){
            return false;
        }

        return false;
    }

    public function removeFundingSource($fundingSourceUrl){

        $apiClient = new ApiClient(env("DWOLLA_URL_" . strtoupper(env("APP_ENV"))));
        $appToken = Configuration::$access_token;

        if(empty($appToken)){
            $appToken = $this->auth();
        }

        try{
            $fsApi = new FundingsourcesApi($apiClient);
            $fsApi->softDelete(["removed" => true], $fundingSourceUrl);

            return true;
        }
        catch(DwollaException $e){
            return false;
        }

        return false;
    }

    public function verifyFundingSource($fundingSourceUrl, $firstDeposit, $secondDeposit){

        $apiClient = new ApiClient(env("DWOLLA_URL_" . strtoupper(env("APP_ENV"))));
        $appToken = Configuration::$access_token;

        if(empty($appToken)){
            $appToken = $this->auth();
        }

        $fsApi = new FundingsourcesApi($apiClient);

        try{
            $verification = $fsApi->microDeposits([
              'amount1' => [
                'value' => $firstDeposit,
                'currency' => 'USD'
              ],
              'amount2' => [
                'value' => $secondDeposit,
                'currency' => 'USD'
              ]],
              $fundingSourceUrl
            );

            return true;
        }
        catch(DwollaException $e){
            return false;
        }   

        return false;
    }

    public function getFundingSource($fundingSourceUrl){
        $apiClient = new ApiClient(env("DWOLLA_URL_" . strtoupper(env("APP_ENV"))));
        $appToken = Configuration::$access_token;

        if(empty($appToken)){
            $appToken = $this->auth();
        }

        try{
            $fsApi = new FundingsourcesApi($apiClient);
            $source = $fsApi->id($fundingSourceUrl);

            return $source;
        }
        catch(DwollaException $e){
            return false;
        }

        return false;
    }

    public function getFundingSourceBalance($fundingSourceUrl){
        $apiClient = new ApiClient(env("DWOLLA_URL_" . strtoupper(env("APP_ENV"))));
        $appToken = Configuration::$access_token;

        if(empty($appToken)){
            $appToken = $this->auth();
        }

        try{
            $fsApi = new FundingsourcesApi($apiClient);
            $balance = $fsApi->getBalance($fundingSourceUrl);

            return $balance;
        }
        catch(DwollaException $e){
            return false;
        }

        return false;
    }

    public function getWallet($customerUrl){
        $fundingSources = $this->fundingSources($customerUrl);

        foreach($fundingSources as $fundingSource){
            if($fundingSource->type == "balance"){
                return $fundingSource;
            }
        }

        return false;
    }

    public function initiateMicrodeposits($fundingSourceUrl){
        $apiClient = new ApiClient(env("DWOLLA_URL_" . strtoupper(env("APP_ENV"))));
        $appToken = Configuration::$access_token;
        $fsApi = new FundingsourcesApi($apiClient);

        $deposit = $fsApi->microDeposits(null, $fundingSourceUrl);

        if(!is_null($deposit)){
            return true;
        }

        return false;
    }

    public function depositToBoardingPass($user, $walletUrl, $bankUrl, $transactionID, $amount, $speed, $note){
        $apiClient = new ApiClient(env("DWOLLA_URL_" . strtoupper(env("APP_ENV"))));
        $appToken = Configuration::$access_token;

        if(empty($appToken)){
            $appToken = $this->auth();
        }

        $transfersApi = new TransfersApi($apiClient);

        try{
            $transfer = $transfersApi->create([
              "_links" => [
                "source" => [
                  "href" => $bankUrl
                ],
                "destination" => [
                  "href" => $walletUrl
                ]
              ],
              "amount" => [
                "currency" => "USD",
                "value" => $amount
              ],
              "clearing" => [
                "source" => $speed
              ],
              "correlationId" => $user->uid
            ]);

            return $transfer;
        }
        catch(DwollaException $e){

            return false;
        }
    }

    public function withdrawToBank($user, $walletUrl, $bankUrl, $transactionID, $amount, $speed, $note){
        $apiClient = new ApiClient(env("DWOLLA_URL_" . strtoupper(env("APP_ENV"))));
        $appToken = Configuration::$access_token;

        if(empty($appToken)){
            $appToken = $this->auth();
        }

        $transfersApi = new TransfersApi($apiClient);

        try{
            $transfer = $transfersApi->create([
              "_links" => [
                "source" => [
                  "href" => $walletUrl
                ],
                "destination" => [
                  "href" => $bankUrl
                ]
              ],
              "amount" => [
                "currency" => "USD",
                "value" => $amount
              ],
              "clearing" => [
                "source" => $speed
              ],
              "correlationId" => $user->uid
            ]);

            return $transfer;
        }
        catch(DwollaException $e){

            return false;
        }
    }

    public function createWebhookSubscription(){
        // once per environment
        $apiClient = new ApiClient(env("DWOLLA_URL_" . strtoupper(env("APP_ENV"))));

        $appToken = Configuration::$access_token;

        if(empty($appToken)){
            $appToken = $this->auth();
        }

        $webhookApi = new WebhooksubscriptionsApi($apiClient);

        $subscription = $webhookApi->create(array (
          "url" => env('APP_URL') . "/webhooks/dwolla",
          "secret" => env('DWOLLA_WEBHOOK_KEY_' . strtoupper(env('APP_ENV'))),
        ));

        dd($subscription); // store url in env file per application
    }

    public function retrieveWebhookSubscription($url){
        $apiClient = new ApiClient(env("DWOLLA_URL_" . strtoupper(env("APP_ENV"))));

        $appToken = Configuration::$access_token;

        if(empty($appToken)){
            $appToken = $this->auth();
        }

        $webhookApi = new WebhooksubscriptionsApi($apiClient);
        $retrieved = $webhookApi->id($url);
        dd($retrieved);
    }

    public function listWebhookSubscription($url){
        $apiClient = new ApiClient(env("DWOLLA_URL_" . strtoupper(env("APP_ENV"))));

        $appToken = Configuration::$access_token;

        if(empty($appToken)){
            $appToken = $this->auth();
        }
        //dd($url);

        $webhookApi = new WebhooksubscriptionsApi($apiClient);
        $retrieved = $webhookApi->_list();
        dd($retrieved);
    }

    public function deleteWebhookSubscription(){
        $apiClient = new ApiClient(env("DWOLLA_URL_" . strtoupper(env("APP_ENV"))));

        $appToken = Configuration::$access_token;

        if(empty($appToken)){
            $appToken = $this->auth();
        }

        $webhookApi = new WebhooksubscriptionsApi($apiClient);
        $webhookApi->deleteById('https://api-sandbox.dwolla.com/webhook-subscriptions/9f27900e-5ea3-4bb6-bbba-1df9275673c9');
    }

    public function transferFromUserWalletToVentiWallet($user, $customerWallet, $fee, $type){
        // when a user deposits or withdraws funds
        // when a user makes a purchase

        $apiClient = new ApiClient(env("DWOLLA_URL_" . strtoupper(env("APP_ENV"))));
        $appToken = Configuration::$access_token;

        if(empty($appToken)){
            $appToken = $this->auth();
        }

        $transfersApi = new TransfersApi($apiClient);

        try{
            $transfer = $transfersApi->create([
              "_links" => [
                "source" => [
                  "href" => $customerWallet
                ],
                "destination" => [
                  "href" => env("DWOLLA_URL_" . strtoupper(env("APP_ENV"))) . "/funding-sources/" . env("DWOLLA_WALLET_" . strtoupper(env("APP_ENV"))) 
                ]
              ],
              "amount" => [
                "currency" => "USD",
                "value" => $fee
              ],
              "clearing" => [
                "destination" => "next-available"
              ],
              "correlationId" => $user->uid
            ]);

            return $transfer;
        }
        catch(DwollaException $e){
            dd($e);
            return false;
        }
    }

    public function transferFromVentiWalletToUserWallet($user, $customerWallet, $fee, $type){
        // when a user deposits or withdraws funds
        // when a user makes a purchase

        $apiClient = new ApiClient(env("DWOLLA_URL_" . strtoupper(env("APP_ENV"))));
        $appToken = Configuration::$access_token;

        if(empty($appToken)){
            $appToken = $this->auth();
        }

        $transfersApi = new TransfersApi($apiClient);

        try{
            $transfer = $transfersApi->create([
              "_links" => [
                "source" => [
                  "href" => env("DWOLLA_URL_" . strtoupper(env("APP_ENV"))) . "/funding-sources/" . env("DWOLLA_WALLET_" . strtoupper(env("APP_ENV")))
                ],
                "destination" => [
                  "href" => $customerWallet
                ]
              ],
              "amount" => [
                "currency" => "USD",
                "value" => $fee
              ],
              "clearing" => [
                "destination" => "next-available"
              ],
              "correlationId" => $user->uid
            ]);

            return $transfer;
        }
        catch(DwollaException $e){
            dd($e);
            return false;
        }
    }

    public function getCancel($transactionID){
        // when a user deposits or withdraws funds
        // when a user makes a purchase
        $url = env("DWOLLA_URL_" . strtoupper(env("APP_ENV")));

        $transferUrl = $url . "/transfers/" . $transactionID;

        $apiClient = new ApiClient($url);
        $appToken = Configuration::$access_token;

        if(empty($appToken)){
            $appToken = $this->auth();
        }

        $transfersApi = new TransfersApi($apiClient);

        try{
            $transfer = $transfersApi->byId($transferUrl);
            if($transfer){
                $cancelLink = $transfer->_links["cancel"] ?? false;
                if($cancelLink){
                    return $cancelLink->href;
                }
                return false;
            }
            return false;
        }
        catch(DwollaException $e){
            return false;
        }

        return false;
    }

    public function cancelTransaction($transactionID){
        $user = Session::get('user');

        if($user !== null){

            $url = env("DWOLLA_URL_" . strtoupper(env("APP_ENV")));

            $transferUrl = $url . "/transfers/" . $transactionID;

            $apiClient = new ApiClient($url);

            $appToken = Configuration::$access_token;

            if(empty($appToken)){
                $appToken = $this->auth();
            }

            $transfersApi = new TransfersApi($apiClient);

            try{
                $transfer = $transfersApi->update([
                  'status' => 'cancelled'
                ], $transferUrl);

                if($transfer->status == "cancelled"){
                    return true;
                }

                return false;
            }
            catch(DwollaException $e){
                return false;
            }

            return false;
        } 

        // no user is active. spam?

        return false;

    }
}