<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use GuzzleHttp\Pool;
use GuzzleHttp\Client;
use Newsletter;
use Kreait\Firebase\Exception\FirebaseException;
use GuzzleHttp\Psr7\Request as HTTP_Request;
use Guzzle\Http\Exception\ClientErrorResponseException;
use Mail;
use Dwolla;
use Carbon\Carbon;
use Exception;

class SyncInterest extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:interest';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This will calculate the ending accrued interest for a user based on the ending balance of yesterday';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $auth = app('firebase.auth');
        $firestore = app('firebase.firestore');
        $db = $firestore->database();

        // we are only going to run this function on verified customers

        $customers = $db->collection("Customers");
        $customers = $customers->where("status","=", "verified")->documents();

        $dwolla = new Dwolla;
        $now = \Carbon\Carbon::now('UTC')->timestamp; // UTC
        $date = \Carbon\Carbon::now('UTC')->format("Y-m-d H:i:s");
        $lastWeek = strtotime("last week");

        if(env("APP_ENV") == "local"){
            $lastWeek = strtotime("yesterday");
        }
        $z = 0;

        foreach($customers as $customer){
            // get the status and subscription of the user

            $subscription = "buddy";

            try{
                $user = $auth->getUser($customer["uid"]);
                if($user->disabled == true){
                    echo $customer["uid"] . " user disabled \n";
                    continue;
                }
                $subscription = $user->customClaims["subscription"] ?? "buddy";


            }
            catch(FirebaseException $e){
                // not going to waste resources on deleted or disabled accounts
                //continue;
            }

            if($subscription == "buddy"){
                // buddy pass does not receive points
                echo $user->uid . " is buddy pass \n";
                continue;
            }


            // get their wallet
            $LastWeeksBalanceSum = $LastWeeksPointsSum = 0;
            $snapshots = $db->collection("BalanceSnapshots");
            $snapshots = $snapshots
            ->where("uid","=", $customer["uid"])
            ->where("timestamp","<=",$now)
            ->where("timestamp",">=",$lastWeek)
            ->orderBy('timestamp','DESC')
            ->documents();

            $numSnapshots = 0;

            // at this step, we need to s

            $counter = 0;

            foreach($snapshots as $snapshot){
                $snapshot = $snapshot->data();

                if($snapshot["compoundable"] == true){

                    $snapshotBalance = (float) $snapshot["balance"];

                    if($snapshotBalance > 7500){
                        // we are capping interest to no more than $7500
                        $snapshotBalance = 7500;
                    }

                    // we need to place restraints 
                    $LastWeeksBalanceSum += $snapshotBalance;
                    $LastWeeksPointsSum += (float) ($snapshot["points"] ?? 0);
                    $snapshotDoc = $db->collection("BalanceSnapshots")->document($snapshot["snapshotID"]);
                    $snapshotDoc->update([
                        ["path" => "reconciled","value" => true]
                    ]);

                    $numSnapshots++;

                }
                $counter++;
            }

            

            // proceed

            if($numSnapshots == 0){
                $numSnapshots = 1;
            }

            // get the avg balance held over 7 days

            $LastWeeksBalanceSum = (float) $LastWeeksBalanceSum/$numSnapshots;
            $LastWeeksPointsSum = (float) $LastWeeksPointsSum/$numSnapshots; 

            // $LastWeeksBalanceSum is now equal to the average balance held over the last 30 days.

            $transactionCol = $db->collection("Transactions");
            
            if($LastWeeksBalanceSum == 0){
                // no point in wasting storage space if the balance of the user was zero
                // the balance cannot be zero to earn interest, regardless of points balance. This prevents cheating.
                echo "Found zero balance $LastWeeksBalanceSum with $counter snapshots for " . $user->displayName . "\n";
                continue;
            }

            // we add the acumulated interest from last month to the cash balance held from last month and the points balance

            $LastWeeksBalanceSum += (float) $LastWeeksPointsSum;

            // calculate the APR to reward

            $apr = (float) env("DWOLLA_APR_" . strtoupper(env("APP_ENV")) . "_" . strtoupper($subscription));

            $interest = (
                (($LastWeeksBalanceSum * $apr)/52)/100
            );

            $transactionID = generateRandomString(12);

            //dd([$customer["uid"],$LastWeeksBalanceSum, $interest, $apr]);

            $transactionInsert = $transactionCol->document($transactionID)->set([
                "transactionID" => $transactionID,
                "uid" => $customer["uid"],
                "type" => "points-deposit",
                "timestamp" => $now, // UTC
                "date" => $date,
                "amount" => (float) $interest,
                "speed" => "next-available",
                "fee" => 0,
                "status" => "complete",
                "from" => "Venti",
                "to" => "My Boarding Pass",
                "total" => (float) $interest,
                "note" => "Earned Interest",
                "transferID" => null,
                "transactionUrl" => null,
                "env" => env('APP_ENV'),
                "APR" => env("DWOLLA_APR_" . strtoupper(env("APP_ENV")))
            ]);

            // now that the statement is reconciled, send email

            // statement emails are sent in separate command
            $z++;
        }

        echo "$z customers evaluated <br>\n\n";

        echo "Completed at: " . \Carbon\Carbon::now('UTC')->timezone("America/New_York")->format("Y-m-d H:i:s");
    }
}

