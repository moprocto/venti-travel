<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use GuzzleHttp\Pool;
use GuzzleHttp\Client;
use Newsletter;
use Kreait\Firebase\Exception\FirebaseException;
use GuzzleHttp\Psr7\Request as HTTP_Request;
use Guzzle\Http\Exception\ClientErrorResponseException;
use Illuminate\Support\Facades\Http;
use Mail;
use Dwolla;
use Carbon\Carbon;
use Exception;

class SyncMailing extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:mailing';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Will sync email octopus with current values from firebase each night.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $auth = app('firebase.auth');
        $usersList = [];
        $users = $auth->listUsers($defaultMaxResults = 3000, $defaultBatchSize = 3000);
        $subscribed = $unsubscribed = 0;
        $new = 0;
        $removed = 0;
        $skips = [];
        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $dwolla = new Dwolla;

        
        foreach($users as $user){
            $user->createdAt = strtotime($user->metadata->createdAt->format("Y-m-d"));
            array_push($usersList, $user);
            // check if the user is already in the octopus list
            
        }

        // sort by most recent sign ups

        usort($usersList, function($a, $b) {
            return $b->createdAt <=> $a->createdAt;
        });

        for($i = 0; $i < 100; $i++){
            $user = $usersList[$i];
            $memberID = md5($user->email);

            if($user->email == null){
                continue;
            }
                // the customer record does not exist in firebase

                $url = "https://emailoctopus.com/api/1.6/lists/3b3c3c3a-3b19-11ee-9258-cf345e7859db/contacts/$memberID";

                $result = Http::get($url, [
                    'api_key' => env('EMAIL_OCTOPUS_API_KEY')
                ]);

                $result = json_decode($result->body());
                $id = $result->id ?? false;
                $first_name = getFirstName($user->displayName);
                $last_name = getLastName($user->displayName);
                $smsVerified = $user->customClaims["smsVerified"] ?? false;
                $appPaid = $user->customClaims["appPaid"] ?? false;


                if($appPaid){
                    $tags = [
                        "onVenti" => true,
                        "tag_blogs" => true,
                        "tag_surveys" => true,
                        "tag_promos" => true,
                        "tag_product" => true,
                        "appPaid" => true
                    ];
                } else {
                     $tags = [
                        "onVenti" => true,
                        "tag_blogs" => true,
                        "tag_surveys" => true,
                        "tag_promos" => true,
                        "tag_product" => true,
                        "appNotPaid" => true
                    ];
                }

                if($smsVerified){
                    $tags["smsVerified"] = true;
                }

                

                if($id){
                    // they are not in email octopus
                    echo "Updating " . $user->email . "\n";

                    if($result->status == "UNSUBSCRIBED"){
                        continue;
                    }

                    $result = Http::put("https://emailoctopus.com/api/1.6/lists/3b3c3c3a-3b19-11ee-9258-cf345e7859db/contacts/$memberID", [
                        'api_key' => env('EMAIL_OCTOPUS_API_KEY'),
                        'email_address' => $user->email,
                        "tags" => $tags,
                        "fields" =>[
                            "EmailAddress" => $user->email,
                            "FirstName" => $first_name,
                            "LastName" => $last_name
                        ],
                        "status" => "SUBSCRIBED"
                    ]);

                    $result = $result->body();

                    //echo $result;



                } else {
                    echo "Adding " . $user->email . "\n";

                    $tagsList = [];

                    foreach($tags as $key => $value){
                        if($tags[$key] == true){
                            array_push($tagsList, $key);
                        }
                    }

                    $result = Http::post("https://emailoctopus.com/api/1.6/lists/3b3c3c3a-3b19-11ee-9258-cf345e7859db/contacts", [
                        'api_key' => env('EMAIL_OCTOPUS_API_KEY'),
                        'email_address' => $user->email,
                        "tags" => ["onVenti"],
                        "fields" =>[
                            "EmailAddress" => $user->email,
                            "FirstName" => $first_name,
                            "LastName" => $last_name
                        ],
                        "status" => "SUBSCRIBED"
                    ]);

                     $result = json_decode($result->body());

                    print_r($result);
                }
            }
        
    }
}

