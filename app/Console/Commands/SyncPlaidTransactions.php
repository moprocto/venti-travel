<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use GuzzleHttp\Pool;
use GuzzleHttp\Client;
use Newsletter;
use Kreait\Firebase\Exception\FirebaseException;
use GuzzleHttp\Psr7\Request as HTTP_Request;
use Guzzle\Http\Exception\ClientErrorResponseException;
use Illuminate\Support\Facades\Http;
use Carbon\Carbon as Carbon;
use Mail;
use Plaid;


class SyncPlaidBalances extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:plaid_transactions';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This will take a snapshot of all bank transactions associated with a user plaid-connected account';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $auth = app('firebase.auth');
        $firestore = app('firebase.firestore');
        $db = $firestore->database();

        // we are only going to run this function on verified customers
        // this will reconcile customer accounts with their wallet url and balance each night
        // this will also record their balance into a separate table for interest calculation

        $plaidAccounts = $db->collection("Plaids")->documents();

        foreach($plaidAccounts as $bank){
            $bank = $bank->data();

            $result = Http::withHeaders([
                    'x-api-key' => 'h3z5uLO9xJxK7Ae6Flq8uYqPIqaPAHuOEuoW4v4r601R9Ez7',
                    'uid' => $user->uid
            ])->post(env("VENTI_API_URL") . "pro/bank/transactions",[
                    "vid" => $bank["vid"],
                    "account_id" => $bank["account_id"],
                    "access_token" => $bank["access_token"]
            ]);

            $transactions = json_decode($result->body());

            // by this point, we've refreshed the database with new transaction data
        }

        echo "Completed transaction refresh at: " . \Carbon\Carbon::now('UTC')->timezone("America/New_York")->format("Y-m-d H:i:s");
    }
}
