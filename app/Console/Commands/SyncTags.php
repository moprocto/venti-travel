<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use GuzzleHttp\Pool;
use GuzzleHttp\Client;
use Newsletter;
use Kreait\Firebase\Exception\FirebaseException;
use GuzzleHttp\Psr7\Request as HTTP_Request;
use Guzzle\Http\Exception\ClientErrorResponseException;
use Illuminate\Support\Facades\Http;
use Mail;
use Dwolla;
use Carbon\Carbon;
use Exception;

class SyncTags extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:tags';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Will update emailoctopus tags based on activity on Venti';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $auth = app('firebase.auth');
        $usersList = [];
        $subscribed = $unsubscribed = 0;
        $new = 0;
        $removed = 0;
        $skips = [];
        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $dwolla = new Dwolla;

        $customerDocs = $db->collection('Customers')->documents();
        $customers = [];

        foreach($customerDocs as $customer){
            $customer = $customer->data();
            $customer["fundingSources"] = [];

            $fundingSourcesDoc = $db->collection('Sources')->documents();
            $fundingSources = [];

            foreach($fundingSourcesDoc as $fundingSource){
                $fundingSource = $fundingSource->data();

                if($fundingSource["uid"] == $customer["uid"]){
                    array_push($customer["fundingSources"],  $fundingSource);
                }
            }

            // we need to assign our marketing tags to the customer

            $zeroBalance = $lowBalance = $boardingpass = $buddypass = false;
            $hasFundingSource = true;
            
            $sourcePendingVerification = false;
            $latestBalance = $customer["latestBalance"] ?? null;

            if((float) $latestBalance == 0){
                $zeroBalance = true;
            }
            if((float) $latestBalance < 450){
                $lowBalance = true;
            }

            if(sizeof($customer["fundingSources"]) == 0){
                $hasFundingSource = false;
            }

            if($customer["status"] == "verified"){
                $boardingpass = true;
            }
            if($customer["status"] == "unverified"){
                $buddypass = true;
            }

            $customer["zeroBalance"] = $zeroBalance;
            $customer["lowBalance"] = $lowBalance;
            $customer["hasFundingSource"] = $hasFundingSource;

            try{
                $user = $auth->getUser($customer["uid"]);    
            }
            catch(FirebaseException $e){
                continue;
            }

            $memberID = md5($user->email);

            $url = "https://emailoctopus.com/api/1.6/lists/3b3c3c3a-3b19-11ee-9258-cf345e7859db/contacts/$memberID";

            $result = Http::get($url, [
                'api_key' => env('EMAIL_OCTOPUS_API_KEY')
            ]);

            $result = json_decode($result->body());
            
            $id = $result->id ?? false;

            $first_name = getFirstName($user->displayName);

            if($id){
                if($result->status == "UNSUBSCRIBED"){
                    continue;
                }

                // only update the status of subscribed members or new people
            }

            $url = "https://emailoctopus.com/api/1.6/lists/3b3c3c3a-3b19-11ee-9258-cf345e7859db/contacts/$memberID";

            $points = $this->getPoints($user, $db);

            $subscription = $user->customClaims["subscription"] ?? "buddy";

            $smsVerified = $user->customClaims["smsVerified"] ?? false;
            $appPaid = $user->customClaims["appPaid"] ?? false;

            $entries = getSweepstakeEntries($subscription, $customer["latestBalance"] ?? 0, $points);

            $result = Http::put($url, [
                'api_key' => env('EMAIL_OCTOPUS_API_KEY'),
                "fields" => [
                    'Entries' => $entries,
                    'FirstName' => $first_name,
                ],
                "tags" => [
                    "boardingpass" => $boardingpass,
                    "buddypass" => $buddypass, 
                    "onVenti" => true,
                    "zeroBalance" => $zeroBalance,
                    "lowBalance" => $lowBalance,
                    "hasFundingSource" => $hasFundingSource,
                    "smsVerified" => $smsVerified,
                    "appPaid" => $appPaid,
                    "appNotPaid" => $appPaid ? false : true
                ]
            ]);
        }   
    }

    public function getPoints($user, $db){
        $transactions = [];

        $transactionDocs = $db->collection("Transactions");
         $transactionDocs = $transactionDocs
                ->where("uid","=",$user->uid)
                ->where("env","=",env("APP_ENV"))
                ->documents();
            // I need a separate firebase instance for sandboxing

            foreach($transactionDocs as $transactionDoc){
                array_push($transactions,  $transactionDoc->data());
            }

        return getPointsBalance($transactions);
    }
}

