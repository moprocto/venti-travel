<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use GuzzleHttp\Pool;
use GuzzleHttp\Client;
use Newsletter;
use Kreait\Firebase\Exception\FirebaseException;
use GuzzleHttp\Psr7\Request as HTTP_Request;
use Guzzle\Http\Exception\ClientErrorResponseException;
use Illuminate\Support\Facades\Http;
use Carbon\Carbon as Carbon;
use Mail;
use Plaid;


class SyncPlaidBalances extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:plaid_balances';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This will take a snapshot of accounts connected via plaid';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $auth = app('firebase.auth');
        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $numSnapshots = 0;

        // we are only going to run this function on verified customers
        // this will reconcile customer accounts with their wallet url and balance each night
        // this will also record their balance into a separate table for interest calculation

        $plaidAccounts = $db->collection("Plaids")->documents();

        $plaid = new Plaid;

        $timestamp = Carbon::now('UTC')->timestamp; // UTU

        // /bank/subaccounts/balanace

        foreach($plaidAccounts as $plaidAccount){
            // request the balance info from Plaid
            $plaidAccount = $plaidAccount->data();
            $accessToken = $plaidAccount["access_token"];

            $user = $auth->getUser($plaidAccount["uid"]);

            if($user->disabled == true){
                continue;
            }

            // check if account is active

            $subaccounts = $db->collection("PlaidSubaccounts")
                            ->where("access_token", "=",$accessToken)
                            ->where("subtype","=","savings")
                            ->documents();

            foreach($subaccounts as $subaccount){
                $subaccount = $subaccount->data();

                // get the current balance for this account

                $result = Http::withHeaders([
                    'x-api-key' => 'h3z5uLO9xJxK7Ae6Flq8uYqPIqaPAHuOEuoW4v4r601R9Ez7',
                    'uid' => $user->uid
                ])
                ->post(env("VENTI_API_URL") . "pro/bank/subaccounts/balance",[
                    "vid" => $subaccount["vid"]
                ]);

                $balances = json_decode($result->body(),true);

                // 
                if($balances){
                    $db->collection('PlaidSubaccounts')->document($subaccount["vid"])->update([
                        ["path" => "balances","value" => $balances],
                    ]);
                }
                $compoundable = true;
                $availableBalance = $balances["available"];
                $currentBalance = $balances["current"];

                if($availableBalance == 0){
                    $compoundable = false;
                }
                
                $points = $this->getPoints($user, $db);
                $snapshotID = generateRandomString(20);

                $snapshot = $db->collection("BalanceSnapshots")->document($snapshotID)->set([
                    "uid" => $user->uid,
                    "balance" => $availableBalance,
                    "timestamp" => $timestamp,
                    "walletID" => $subaccount["account_id"],
                    "snapshotID" => $snapshotID,
                    "reconciled" => false,
                    "points" => $points,
                    "env" => env("APP_ENV"),
                    "APR" => env("PLAID_APR_" . strtoupper(env("APP_ENV"))),
                    "compoundable" => $compoundable,
                    "plaid" => true
                ]);

                $numSnapshots++;
            }
        }

        echo "Completed $numSnapshots snapshots at: " . \Carbon\Carbon::now('UTC')->timezone("America/New_York")->format("Y-m-d H:i:s");
    }

    public function getPoints($user, $db){
        $transactions = [];

        $transactionDocs = $db->collection("Transactions");
         $transactionDocs = $transactionDocs
                ->where("uid","=",$user->uid)
                ->where("env","=",env("APP_ENV"))
                ->documents();
            // I need a separate firebase instance for sandboxing

            foreach($transactionDocs as $transactionDoc){
                array_push($transactions,  $transactionDoc->data());
            }

        return getPointsBalance($transactions);
    }
}
