<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use GuzzleHttp\Pool;
use GuzzleHttp\Client;
use Newsletter;
use Kreait\Firebase\Exception\FirebaseException;
use GuzzleHttp\Psr7\Request as HTTP_Request;
use Guzzle\Http\Exception\ClientErrorResponseException;

use Mail;
use Dwolla;
use Carbon\Carbon;


class SyncWallets extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:wallets';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This will calculate the recommendations of potential groups for every user';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $auth = app('firebase.auth');
        $firestore = app('firebase.firestore');
        $db = $firestore->database();

        // we are only going to run this function on verified customers
        // this will reconcile customer accounts with their wallet url and balance each night
        // this will also record their balance into a separate table for interest calculation

        $customers = $db->collection("Customers");
        $customers = $customers->where("status","=", "verified")->documents();

        $dwolla = new Dwolla;
        $timestamp = \Carbon\Carbon::now('UTC')->timestamp; // UTU

        foreach($customers as $customer){
            // get their wallet
            $customer = $customer->data();

            if(!array_key_exists("wallet", $customer)){
                // no wallet url for this person.
                echo $customer["uid"] . "\n\n";
                continue;
            }

            try{
                $user = $auth->getUser($customer["uid"]);
            }
            catch(FirebaseException $e){
                // user account doesn't exist or is suspended
                
                continue;
            }

            //$walletDetails = $dwolla->getFundingSourceBalance($customer["wallet"]);

            $customerID = explode("customers/",$customer["customerID"])[1];

            $customerDoc = $db->collection("Customers")->document($customerID);

            $customerDoc->update([
                    ["path" => "latestBalance","value" => 0],
                    ["path" => "latestBalanceTimestamp","value" => $timestamp]
            ]);

            $compoundable = false; // true;


            /*
            if((float) $walletDetails->balance->value == 0){
                // skip wasting resources on zero balance accounts
                $compoundable = false;
            }

            

            */

            $walletID = explode("funding-sources/",$customer["wallet"])[1];

            $snapshotID = generateRandomString(20);

            // time to get the points balance

            $points = $this->getPoints($user, $db);

            $snapshot = $db->collection("BalanceSnapshots")->document($snapshotID)->set([
                "uid" => $customer["uid"],
                "balance" => 0,//$walletDetails->balance->value,
                "timestamp" => $timestamp,
                "wallet" => $customer["wallet"],
                "walletID" => $walletID,
                "snapshotID" => $snapshotID,
                "reconciled" => false,
                "points" => $points,
                "env" => env("APP_ENV"),
                "APR" => env("DWOLLA_APR_" . strtoupper(env("APP_ENV"))),
                "compoundable" => $compoundable,
                "plaid" => false,
                "API_ACTIVE" => false
            ]);

            // send the email that a new statement is available

            
        }

        echo "Completed at: " . \Carbon\Carbon::now('UTC')->timezone("America/New_York")->format("Y-m-d H:i:s");
    }

    public function getPoints($user, $db){
        $transactions = [];

        $transactionDocs = $db->collection("Transactions");
         $transactionDocs = $transactionDocs
                ->where("uid","=",$user->uid)
                ->where("env","=",env("APP_ENV"))
                ->documents();
            // I need a separate firebase instance for sandboxing

            foreach($transactionDocs as $transactionDoc){
                array_push($transactions,  $transactionDoc->data());
            }

        return getPointsBalance($transactions);
    }
}
