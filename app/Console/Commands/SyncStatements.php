<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use GuzzleHttp\Pool;
use GuzzleHttp\Client;
use Newsletter;
use Kreait\Firebase\Exception\FirebaseException;
use GuzzleHttp\Psr7\Request as HTTP_Request;
use Guzzle\Http\Exception\ClientErrorResponseException;
use Mail;
use Dwolla;
use Carbon\Carbon;
use Exception;

class SyncStatements extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:statements';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This will disburse statements to customers';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $auth = app('firebase.auth');
        $firestore = app('firebase.firestore');
        $db = $firestore->database();

        // we are only going to run this function on verified customers

        $customers = $db->collection("Customers");
        $customers = $customers->where("status","=", "verified")->documents();

        $dwolla = new Dwolla;
        $now = \Carbon\Carbon::now('UTC')->timestamp; // UTC
        $date = \Carbon\Carbon::now('UTC')->format("Y-m-d H:i:s");
        $lastMonth = strtotime("last month");

        if(env("APP_ENV") == "local"){
            $lastMonth = strtotime("yesterday");
        }
        $z = 0;

        foreach($customers as $customer){
            // get the status and subscription of the user

            $subscription = "buddy";

            try{
                $user = $auth->getUser($customer["uid"]);
                if($user->disabled == true){
                    echo $customer["uid"] . " user disabled \n";
                    continue;
                }
                $subscription = $user->customClaims["subscription"] ?? "buddy";


            }
            catch(FirebaseException $e){
                // not going to waste resources on deleted or disabled accounts
                //continue;
            }

            if($subscription == "buddy"){
                // buddy pass does not receive points
                echo $user->uid . " is buddy pass \n";
                continue;
            }


            // get their wallet
            $LastMonthsBalanceSum = $LastMonthsPointsSum = 0;
            $snapshots = $db->collection("BalanceSnapshots");
            $snapshots = $snapshots
            ->where("uid","=", $customer["uid"])
            ->where("timestamp","<=",$now)
            ->where("timestamp",">=",$lastMonth)
            ->orderBy('timestamp','DESC')
            ->documents();

            $numSnapshots = 0;

            // at this step, we need to s

            $counter = 0;

            foreach($snapshots as $snapshot){
                $snapshot = $snapshot->data();

                if($snapshot["compoundable"] == true){
                    // we need to place restraints
                    // this may not be necessary
                    $LastMonthsBalanceSum += (float) $snapshot["balance"];
                    $LastMonthsPointsSum += (float) ($snapshot["points"] ?? 0);
                    $snapshotDoc = $db->collection("BalanceSnapshots")->document($snapshot["snapshotID"]);
                    $snapshotDoc->update([
                        ["path" => "reconciled","value" => true]
                    ]);

                    if($counter == 0){
                        echo "Got $LastMonthsBalanceSum with $LastMonthsPointsSum for latest statement for " . $user->displayName . " after $numSnapshots snapshots \n";
                        // the newest snapshot becomes the account statement for the monthly period

                        $lastMonthy = \Carbon\Carbon::now()->subMonth()->format("F");
                        $lastYeary = \Carbon\Carbon::now()->subMonth()->format("Y");
                        
                        $snapshotDoc->update([
                            ["path" => "statement","value" => true],
                            ["path" => "month","value" => $lastMonthy],
                            ["path" => "year","value" => $lastYeary],
                        ]);
                        
                    }

                    $numSnapshots++;

                }
                $counter++;
            }

            // proceed

            if($numSnapshots == 0){
                $numSnapshots = 1;
            }

            // get the avg balance held over 30 days

            $LastMonthsBalanceSum = (float) $LastMonthsBalanceSum/$numSnapshots;
            $LastMonthsPointsSum = (float) $LastMonthsPointsSum/$numSnapshots; 

            // $LastMonthsBalanceSum is now equal to the average balance held over the last 30 days.

            $transactionCol = $db->collection("Transactions");
            
            if($LastMonthsBalanceSum == 0){
                // no point in wasting storage space if the balance of the user was zero
                // the balance cannot be zero to earn interest, regardless of points balance. This prevents cheating.
                echo "Found zero balance $LastMonthsBalanceSum with $counter snapshots for " . $user->displayName . "\n";
                continue;
            }

            // we add the acumulated interest from last month to the cash balance held from last month and the points balance

            $LastMonthsBalanceSum += (float) $LastMonthsPointsSum;

            // calculate the APR to reward

            

            // this command only sends statements

            // now that the statement is reconciled, send email

            $subject = "[Boarding Pass] A New Account Statement is Available";

            if(env('APP_ENV') == "production"){
                $user = $auth->getUser($customer["uid"]);
                $email = $user->email;

                $data = [
                    
                ];

                try{
                    /*
                    Mail::send('emails.transactions.statement', array("data" => $data), function($message) use($email, $subject)
                        {
                            $message
                                ->to($email)
                                ->from("no-reply@venti.co","Venti")
                                ->replyTo("no-reply@venti.co")
                                ->subject($subject);
                    });
                    */
                }
                catch(Exception $e){
                    //
                }
            }
            $z++;
        }

        echo "$z customers evaluated <br>\n\n";

        echo "Completed at: " . \Carbon\Carbon::now('UTC')->timezone("America/New_York")->format("Y-m-d H:i:s");
    }
}

