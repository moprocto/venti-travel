<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use GuzzleHttp\Pool;
use GuzzleHttp\Client;
use Newsletter;
use Kreait\Firebase\Exception\FirebaseException;
use GuzzleHttp\Psr7\Request as HTTP_Request;
use Guzzle\Http\Exception\ClientErrorResponseException;
use Illuminate\Support\Facades\Http;
use Carbon\Carbon as Carbon;
use Exception;

class SyncLeaderboard extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:leaderboard';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Grabs ranking for customers';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(){
        $cutoff = Carbon::now('UTC')->subHours(12)->timestamp;

        // we are going to pull the latest balance snapshots

        $auth = app('firebase.auth');
        $firestore = app('firebase.firestore');
        $db = $firestore->database();

        $snapshotsDocs = $db->collection("BalanceSnapshots");
        $snapshotsDocs = $snapshotsDocs
                        ->where("timestamp",">=",$cutoff)
                        ->documents();

        $leaderboard = [];

        foreach($snapshotsDocs as $snapshot){

            $snapshot = $snapshot->data();

            $user = $auth->getUser($snapshot["uid"]);

            if($user->disabled == true){
                continue;
            }
            $subscription = $user->customClaims["subscription"];

            $entries = getSweepstakeEntries($subscription, $snapshot["balance"], $snapshot["points"]);

            array_push($leaderboard, 
                [
                    "uid" => $snapshot["uid"],
                    "entries" => $entries,
                    "claims" => $user->customClaims
                ]
            );
        }

        // sort leaderboard

        usort($leaderboard, function($a, $b) {
            return $b['entries'] <=> $a['entries'];
        });

        for($i = 0; $i < sizeof($leaderboard); $i++){
            $position = $i + 1;

            $uid = $leaderboard[$i]["uid"];
            $claims = $leaderboard[$i]["claims"];
            $claims["leaderboard"] = $position;
            usleep(30000);
            $auth->setCustomUserClaims($uid, $claims);
        }

        echo "complete";
    }
}

