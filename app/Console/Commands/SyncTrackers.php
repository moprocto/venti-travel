<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use GuzzleHttp\Pool;
use GuzzleHttp\Client;
use Newsletter;
use Kreait\Firebase\Exception\FirebaseException;
use GuzzleHttp\Psr7\Request as HTTP_Request;
use Guzzle\Http\Exception\ClientErrorResponseException;
use Illuminate\Support\Facades\Http;
use Mail;
use Dwolla;
use Carbon\Carbon;
use Exception;

class SyncTrackers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:trackers';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Will notify folks when price alerts are available';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(){
        // get all trackers in the system

        echo "starting \n";

        $auth = app('firebase.auth');
        $firestore = app('firebase.firestore');
        $db = $firestore->database();

        // we are only going to run this function on verified customers with active Venti accounts

        $customers = $db->collection("Customers");
        $customers = $customers->documents();
        $activeCustomers = [];

        foreach($customers as $customer){
            $customer = $customer->data();
            if($customer["status"] == "verified" || $customer["status"] == "unverified"){
                array_push($activeCustomers, $customer);
            }
        }

        $activeTrackers = [];

        foreach($activeCustomers as $activeCustomer){
            // get all their active trackers
            $trackers = $db->collection("Trackers");
            $trackers = $trackers->where("status","=","active")->where("uid","=", $activeCustomer["uid"])->documents();

            foreach($trackers as $tracker){
                array_push($activeTrackers, $tracker);
            }
        }

        // loop through each tracker and pull the required data

        foreach($activeTrackers as $request){
            $request = (object) $request->data();

            // we need to see if this tracker is in the "past";

            $now = Carbon::now()->timezone('UTC')->timestamp;


            if($now >= $request->expiry){
                $trackerDoc = $db->collection("Trackers")->document($request->trackerID);
                //update the tracker to expired

                $trackerDoc->update([
                    ["path" => "status", "value" => "expired"],
                ]);
                
                // skip to next tracker

                continue;
            }

            // check if departure is less than 24 hours away


            $departureStamp = Carbon::createFromFormat('Y-m-d', $request->start)->format('Y-m-d');

            // curl new offer
            $request = (object) $request;

            $headers = ['Application-Type:application/json','Accept-Encoding:gzip','Accept:application/json','Duffel-Version:v1','Content-Type:application/json',"Authorization:Bearer " . env('DUFFEL_API_KEY')];

            $cabin_class = strtolower(str_replace(" ","_",$request->class));
            $iata_home_original = explode(" ", str_replace("(","",str_replace(")","",$request->iata_origin)))[0] ?? null;
            $iata_home = $request->iata_origin;
            $iata_destination = $request->iata_destination ?? null;

            $departure_date = $request->start;
            $departure_date = Carbon::createFromFormat('Y-m-d', $departure_date)->format('Y-m-d');
            
            $isReturn = false;
            if($iata_home_original != $iata_home){
                $isReturn = true;
            }

            $return_date = $request->end;
            if($request->type != "one-way"){
                $return_date = Carbon::createFromFormat('Y-m-d', $return_date)->format('Y-m-d');
            }
            

            $startFlight = [
                "origin" => "$iata_home", 
                "destination" => "$iata_destination", 
                "departure_date" => "$departure_date"
            ];

            $returnFlight = [
                "origin" => "$iata_destination", 
                "destination" => "$iata_home", 
                "departure_date" => "$return_date"
            ];

            $slices = [$startFlight];

            if($request->type == "round-trip"){
                if($isReturn){
                    $startFlight = [
                        "origin" => "$iata_destination", 
                        "destination" => "$iata_home", 
                        "departure_date" => "$departure_date"
                    ];

                    $returnFlight = [
                        "origin" => "$iata_home", 
                        "destination" => "$iata_destination", 
                        "departure_date" => "$return_date"
                    ];
                }
                $slices = [$startFlight, $returnFlight];
            }

            $passengers = [];

            for($adults = 0; $adults < $request->adults; $adults++){
                array_push($passengers, ["type" => "adult", "given_name" => "Ventii", "family_name" => "Adult-$adults"]);
            }
            for($children = 0; $children < $request->children; $children++){
                array_push($passengers, ["type" => "child", "given_name" => "Ventii", "family_name" => "Child-$children"]);
            }

            /*

            for($infant = 0; $infant < $request->infants; $infants++){
                array_push($passengers, ["type" => "infant_without_seat", "given_name" => "Ventii", "family_name" => "Infant-$infant"]);
            }

            */

            $body = ["data" => [
                "slices" => $slices,    
                "passengers" => $passengers,
                "cabin_class" => $cabin_class
                ]
            ];

            if($request->type == "one-way"){
                $url = env("DUFFEL_API_URL") . 'air/offer_requests?return_offers=true';
            } else {
                // round trip is the only other supported option
                $url = env("DUFFEL_API_URL") . 'air/partial_offer_requests?return_offers=true';
                if($isReturn){
                    $PARTIAL_OFFER_REQUEST_ID_FROM_OUTBOUND_SEARCH = $request->partial_id;
                    $PARTIAL_OFFER_ID_FROM_OUTBOUND_SEARCH = $request->selected_partial_id;

                    $url = env("DUFFEL_API_URL") . "air/partial_offer_requests/$PARTIAL_OFFER_REQUEST_ID_FROM_OUTBOUND_SEARCH?selected_partial_offer[]=$PARTIAL_OFFER_ID_FROM_OUTBOUND_SEARCH";
                }
            }

            $flights = $this->curlNewOffer($url,$headers, $body, $isReturn);

            // sort flights from cheapest total
            
            usort($flights, function($a, $b) {
                return $a['total_amount'] <=> $b['total_amount'];
            });

            // store the lowest in the table

            $cheapest = $flights[0];

            $trackerID = generateRandomString(12);


            $url = $this->createSearchUrl($request);


            $data = [
                "tracker" => $request,
                "trackerID" => $request->trackerID,
                "data" => $cheapest,
                "createdAt" => Carbon::now()->timezone("UTC")->timestamp,
                "url" => $url
            ];

            $db->collection("TrackedFlights")->document($trackerID)->set($data);

            // update the tracker with the last known price

            $trackerDoc = $db->collection("Trackers")->document($request->trackerID);
                //update the tracker to expired

            $trackerDoc->update([
                ["path" => "price", "value" => $cheapest["total_amount"]],
            ]);

            // send an email if the cheapest price is at or below target
            $alerts = 0;

            if($cheapest["total_amount"] <= $request->strike){

                try{
                    $auth = app('firebase.auth');
                    $user = $auth->getUser($request->uid);
                    $email = $user->email;
                    $subject = "[Price Alert] We Found a Flight to $iata_destination under " . $request->strike . " USD";
                    Mail::send('emails.alerts.flight-tracker', array("data" => $data), function($message) use($email, $subject)
                        {
                            $message
                                ->to($email)
                                ->from("no-reply@venti.co","Venti")
                                ->replyTo("no-reply@venti.co")
                                ->subject($subject);
                        });

                    $alerts++;
                }
                catch(FirebaseException $e){
                    //
                }
            }

            echo "Done. Sent $alerts alerts ";
        }
    }

    public function curlNewOffer($url, $headers, $body, $isReturn){
        $post_data = json_encode($body);

        // Prepare new cURL resource
        $crl = curl_init($url);
        curl_setopt($crl, CURLOPT_ENCODING , "gzip");
        curl_setopt($crl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($crl, CURLINFO_HEADER_OUT, true);
        if(!$isReturn){
            curl_setopt($crl, CURLOPT_POST, true);
            curl_setopt($crl, CURLOPT_POSTFIELDS, $post_data);
        } else {
            curl_setopt($crl, CURLOPT_CUSTOMREQUEST, "GET");
        }
        
        // Set HTTP Header for POST request 
        curl_setopt($crl, CURLOPT_HTTPHEADER, $headers);

        // Submit the POST request
        $result = curl_exec($crl);

        $result = json_decode($result, true);

        if(!array_key_exists("data", $result)){
            // this query did not return any data for some reason.
            return [];
        }

        $offers = $result["data"]["offers"] ?? [];

        $search_id = $result["data"]["id"];

        $results = [];
        $i = 0;
        $d = 0;

        $offerPreview = [];

        foreach($offers as $offer){

                // we are going to skip flights that require instant payment

                $payment_requirements = $offer["payment_requirements"] ?? false;

                if($payment_requirements){
                    if($payment_requirements["requires_instant_payment"] == true){
                        //continue;
                    }
                }
                else{
                    //continue;
                }

                if($i < 10){
                    array_push($offerPreview, $offer);
                    
                } else {

                }
                $legs = [];
                $slices = $offer["slices"];
                $totalDuration = 0; // minutes 
                $totalFlights = 0; // counting each slice


                foreach($slices as $slice){
                
                  if(!array_key_exists("segments",$slice)){

                    $totalFlights++;

                    $flightNumber = $slice["operating_carrier_flight_number"];

                    if(is_null($flightNumber)){
                      $flightNumber = $slice["marketing_carrier_flight_number"];
                    }

                    $durationMinutes = getDurationinMinutes($slice["duration"]);

                    $totalDuration += $durationMinutes;

                    $carrier = $slice["operating_carrier"] ?? [];

                    if(!$carrier){
                        $carrier = $slice["marketing_carrier"];
                    }

                    array_push($legs,
                        [
                            "destination" => $slice["destination"] ?? null,
                            "origin" => $slice["origin"] ?? null,
                            "duration" => $slice["duration"] ?? null,
                            "aircraft" => $slice["segments"]["aircraft"] ?? null,
                            "departing_at" => $slice["departing_at"],
                            "arriving_at" => $slice["arriving_at"],
                            "carrier" => $carrier,
                            "flight_number" =>  $flightNumber,
                            "passengers" => $slice["passengers"] ?? [],
                            "class" => $slice["fare_brand_name"] ?? false
                        ]
                    );
                    

                  } else {

                    foreach($slice["segments"] as $segment){
                        // the flight is not direct.
                        $totalFlights++;
                        $flightNumber = $segment["operating_carrier_flight_number"];

                        if(is_null($flightNumber)){
                            $flightNumber = $segment["marketing_carrier_flight_number"];
                        }

                        $carrier = $segment["operating_carrier"] ?? [];

                        if(!$carrier){
                            $carrier = $segment["marketing_carrier"];
                        }

                        $durationMinutes = getDurationinMinutes($segment["duration"]);

                        $totalDuration += $durationMinutes;


                        array_push($legs,
                            [
                                "destination" => $segment["destination"] ?? null,
                                "origin" => $segment["origin"] ?? null,
                                "duration" => $segment["duration"] ?? null,
                                "aircraft" => $segment["segments"]["aircraft"] ?? null,
                                "departing_at" => $segment["departing_at"],
                                "arriving_at" => $segment["arriving_at"],
                                "carrier" => $carrier,
                                "flight_number" => $flightNumber,
                                "passengers" => $segment["passengers"] ?? [],
                                "class" => $slice["fare_brand_name"]  ?? false,
                                "segments" => $slice["segments"]
                            ]
                      );
                    }
                  }   
                }

                array_push($results, [
                    "search_id" => $search_id,
                    "base_amount" => $offer["base_amount"],
                    "tax_amount" => $offer["tax_amount"],
                    "total_amount" => $offer["total_amount"],
                    "airline" => $offer["owner"],
                    "slices" => $legs,
                    "default_order" => $i,
                    "offer" => $offer["id"],
                    "partial" => $offer["partial"] ?? false,
                    "total_duration" => $totalDuration,
                    "total_flights" => $totalFlights,
                    "venti_offer_id" => $totalDuration . "-" . $offer["total_amount"] . "-" . $offer["owner"]["name"] . "-" . sizeof($legs)
                ]);
                
                $i++;
            }

            // remove duplicates by venti offer id

            $flights = $dupeFlights = [];

            foreach ($results AS $key => $line ) { 
                if ( !in_array($line['venti_offer_id'], $dupeFlights) ) { 
                    $dupeFlights[] = $line['venti_offer_id']; 
                    $flights[$key] = $line; 
                } 
            } 

        curl_close($crl);

        return $results;
    }

    public function createSearchUrl($params){
        $from = "(" . $params->iata_origin . ")" . $params->origin;
        $to = "(" . $params->iata_destination . ")" . $params->destination;

        $start = Carbon::parse($params->start)->format("m/d/Y");
        $end = Carbon::parse($params->end)->format("m/d/Y");

        $url = "https://venti.co/flights/search?from=$from&to=$to";

        $url .= "&type=" . $params->type . "&class=" . $params->class . "&start=" . $start;

        if($params->type == "round-trip"){
            $url .= "&end=" . $end;
        } else {
            $url .= "&end=null";
        }

        $url .= "&adults=" . $params->adults . "&children=" . $params->children . "&infants=0";

        return $url;
    }
}

