<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use GuzzleHttp\Pool;
use GuzzleHttp\Client;
use Newsletter;
use Kreait\Firebase\Exception\FirebaseException;
use GuzzleHttp\Psr7\Request as HTTP_Request;
use Guzzle\Http\Exception\ClientErrorResponseException;
use Illuminate\Support\Facades\Http;
use Carbon\Carbon;
use Exception;

class SyncCurrency extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:currency';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Grabs the latest currency conversions every hour';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(){
        $result = HTTP::get(env('FIXER_API'));

        $result = json_decode($result->body(),true);

        if(array_key_exists("rates", $result)){
            // store rates data
            $firestore = app('firebase.firestore');
            $db = $firestore->database();
            $db->collection("Currency")->document("rates")->set($result["rates"]);
        }


        
    }
}

