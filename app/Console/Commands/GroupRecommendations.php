<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use GuzzleHttp\Pool;
use GuzzleHttp\Client;
use Newsletter;
use Kreait\Firebase\Exception\FirebaseException;
use GuzzleHttp\Psr7\Request as HTTP_Request;
use Guzzle\Http\Exception\ClientErrorResponseException;
use Mail;


class GroupRecommendations extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'recommendations:group';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This will calculate the recommendations of potential groups for every user';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // grab all active users in the system
        $auth = app('firebase.auth');
        $users = $auth->listUsers($defaultMaxResults = 1000, $defaultBatchSize = 1000);
        $potentialMatchesList  = $usersList = [];

        // loop through each user

        foreach($users as $user){

        	if(isProfileComplete($user)[0] == true){
        		
                array_push($usersList, $user);
        	}
            
        }

        foreach($usersList as $user){
            $group = [];
            for($t = 0; $t < 100; $t++){
                // we'll make 100 attempts to form a group of 4
                if(sizeof($group) < 4){
                    $randomUser = $this->getRandomCompatibleUser($user, $usersList);
                    if($randomUser != null && $user->uid == "TAzwYCpTDngZLApoGrWN5nLaocy1"){
                        echo "\n Found Compat";
                        array_push($group, $randomUser);
                        echo "\n " . sizeof($group); 
                    }
                }
                else {

                    break;
                }
            }

            $groupChemistry = $this->calcGroupChemistry($group);
        
            $size = sizeof($groupChemistry);
            
            for($i = 0; $i <= $size; $i++){
                if(array_key_exists($i, $groupChemistry)){
                $match = $groupChemistry[$i];
                $needle = ["match" => $match["matchee"], "matchee" => $match["match"]];
                
                if(in_array($needle, $groupChemistry)){
                        unset($groupChemistry[$i]);
                    }     
                }
            }

            $uids = [$user->uid];
            foreach($groupChemistry as $groupie){
                array_push($uids, $groupie["match"]);
                array_push($uids, $groupie["matchee"]);
            }



            $uids = $uUIDS = array_unique($uids);
            $finalGroupMembers = [];
            $users = collect($usersList);


            
            foreach($uids as $uid){
                $userstoadd = $users->where("uid", $uid);
                $userstoadd = reset($userstoadd);
                $key = key($userstoadd);

                array_push($finalGroupMembers, [
                    "displayName" =>  getFirstName($userstoadd[$key]->displayName),
                    "photoUrl" => $userstoadd[$key]->photoUrl,
                    "customClaims" => $userstoadd[$key]->customClaims
                ]);
            }

            $email = "markus@venti.co";
            $subject = "New Group Recommendation";
            $emailCC = "";
            

            if(sizeof($finalGroupMembers) > 3){
                $uids = collect($uids)->sort();
                $groupID = implode("", $uids->toArray());

                $firestore = app('firebase.firestore');
                $db = $firestore->database();
                $idea = $db->collection("Ideas")->document($groupID)->snapshot()->data();


                if(is_null($idea)){
                    // must be unique
                    $membersArray = [];
                    $i = 0;
                    foreach($uUIDS as $uUID){
                        if($i == 0){
                            array_push($membersArray, ["uid" => $uUID, "role" => "owner"]);
                        } else {
                            array_push($membersArray, ["uid" => $uUID, "role" => "invited"]);
                        }
                        $i++;
                    }

                    $docRef = $db->collection("Ideas")->document($groupID)->set(
                    [  
                        "group" => $membersArray,
                        "type" => "activity",
                        "color" => getRandomColor(),
                        "tripID" => $groupID,
                        "archived" => 2, // archived 2 is a special category for pending approval groups. 0 = active. 1 = archived.
                        "privacy" => "full",
                        "title" =>  "Venti Recommended Group",
                        "createdAt" => strtotime("now"),
                        "description" => "",
                        "imageURL" => "",
                        "imageAuthor" => "",
                        "imageAuthorProfile" => "",
                        "destination" => "",
                        "country" => "",
                        "tags" => []
                    ]);

                    $data = [
                        "members" => $finalGroupMembers,
                        "tripID" => $groupID
                    ];

                    Mail::send("emails.application.recommendation", array('data' => $data), function($message) use($email, $emailCC, $subject)
                        {
                            $message
                                    ->to($email)
                                    ->replyTo("no-reply@venti.co")
                                    ->subject($subject);
                    });

                }

                echo "Recommendation Sent!";
            }
        }

        return "Terminating function";
    }

    public function getRandomCompatibleUser($user, $subusers){
        $randomKey = rand(0, sizeof($subusers) -1);
        $compatibility = calcCompatibility($user, $subusers[$randomKey]);
        $compatibility = (int) explode(" ", $compatibility)[1];
        $tries = 0;
        if($compatibility >= 90){
            return $subusers[$randomKey];
        }
        return null;
    }

    public function calcGroupChemistry($groupies){
        $results = [];
        foreach($groupies as $group){
            for($i = 0; $i < sizeof($groupies); $i++){

                $compatibility = calcCompatibility($group, $groupies[$i]);
                $compatibility = (int) explode(" ", $compatibility)[1];
                if($group->uid != $groupies[$i]->uid){
                    if($compatibility > 90){
                        array_push($results, 
                            [
                                "match" => $group->uid,
                                "matchee" => $groupies[$i]->uid
                            ]
                        );
                    } 
                }
            }
        }

        return $results;
    }
}
