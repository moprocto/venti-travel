<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use GuzzleHttp\Pool;
use GuzzleHttp\Client;
use Newsletter;
use Kreait\Firebase\Exception\FirebaseException;
use GuzzleHttp\Psr7\Request as HTTP_Request;
use Guzzle\Http\Exception\ClientErrorResponseException;
use Illuminate\Support\Facades\Http;
use Mail;
use Dwolla;
use Carbon\Carbon as Carbon;
use App\Models\User as User;
use Exception;

class SyncAutosave extends Command
{
	/**
     * The name and signature of the console command.
     *
     * @var string
	 */

    protected $signature = 'sync:autosave';

    /**
     * The console command description.
     *
     * @var string
     */

    protected $description = 'Will update perform automatic deposits';


    public function handle(){
    	// get all the deposit requests for today

    	$firestore = app('firebase.firestore');
    	$auth = app('firebase.auth');
        $db = $firestore->database();
        $today = Carbon::now('UTC')->timestamp;
        $tomorrow = Carbon::now('UTC')->addDays(1)->timestamp;

        $autosaveDocs = $db
        	->collection("Autosaves")
        	->where("deleted", "=", null)
        	->where("start", "<=", $today)
        	->documents();

        foreach($autosaveDocs as $autosave){
            $autosave = $autosave->data();


        	// get the user info 
        	$user = new User($autosave["uid"]);

        	if(!$user){
        		continue;
        	}

        	// check: for date relevancy
            $shouldDeposit = $this->shouldProcessDeposit($autosave["frequency"], $autosave["start"]);
        	// 

            if(!$shouldDeposit){
                continue;
            }

        	$balance = $user->customer()->wallet()->getCashBalance();
        	$projectedBalance = $balance + (int) $autosave["amount"];

        	$depositCap = (int) env("VENTI_" . $user->subscription() . "_DEPOSIT_CAP");

        	if($projectedBalance > $depositCap){
                // let the user know that 
        		continue;
        	}
        	// check if the limit of the wallet exceeds the maximum for their account


            echo $autosave["id"] . " will fire \n";

            // send a POST request to the API

            

                $result = Http::withHeaders([
                    'x-api-key' => 'h3z5uLO9xJxK7Ae6Flq8uYqPIqaPAHuOEuoW4v4r601R9Ez7',
                    'uid' => $autosave["uid"]
                ])
                ->post(env("VENTI_API_URL") . "boardingpass/customer/fundingSource/bank/deposit",[
                    "depositAmount" => $autosave["amount"],
                    "depositSpeed" => "standard",
                    "depositSource" => $autosave["source"],
                    "depositEnv" => "autosave"
                ]);

                $result = json_decode($result,true);

                // update the autosave with most recent success run

                if($result == 200){
                    $autosaveDocs = $db->collection("Autosaves")->document($autosave["id"])
                        ->update([
                            ["path" => "lastSuccessfulRun","value" => $today]
                        ]);
                } else {
                    // email error
                    $email = $user->user->email;
                    $name = $user->user->displayName;
                    $subject = "[Boarding Pass] We were unable to process your scheduled deposit";
                }


        }
    }

    public function shouldProcessDeposit($frequency, $startTimestamp) {
        $startDate = Carbon::createFromTimestampUTC($startTimestamp);
        $today = Carbon::now('UTC');

        switch ($frequency) {
            case 'everyWeek':
                return $today->diffInDays($startDate) % 7 == 0;
            case 'everyMonth':
                return $today->diffInMonths($startDate) % 1 == 0;
            case 'biWeekly':
                return $today->diffInDays($startDate) % 14 == 0;
            case 'semiMonthly':
                $is15th = $today->day == 15;
                $isLastDayOfMonth = $today->isLastOfMonth();
                return $is15th || $isLastDayOfMonth;
            default:
                return false;
        }
    }
}