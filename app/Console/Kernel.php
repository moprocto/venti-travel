<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')->hourly();
        $schedule->command('sync:wallets')->hourly();
        $schedule->command('sync:interest')->nightly();
        $schedule->command('sync:statements')->nightly();
        $schedule->command('sync:mailing')->hourly();
        $schedule->command('sync:tags')->hourly();
        $schedule->command('sync:trackers')->hourly();
        $schedule->command('sync:currency')->hourly();
        $schedule->command('sync:leaderboard')->hourly();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
