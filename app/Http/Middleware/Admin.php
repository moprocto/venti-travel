<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use Session;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Session::get('user') === null){
            return redirect('/login');
        }

        $admins = explode(",",env('APP_ADMINS_' . strtoupper(env('APP_ENV'))));

        if(in_array(Session::get('user')->uid, $admins)){
            // the user is an admin
            return $next($request);
        }

        return redirect('/');
    }
}
