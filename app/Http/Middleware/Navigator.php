<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use Session;

class Navigator
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Session::get('user') === null){
            return redirect('/login');
        }
        if((Session::get('user')->customClaims["nav"] ?? null) == true){
            return $next($request);
        } else {

        }
        return redirect('/navigator');
    }
}
