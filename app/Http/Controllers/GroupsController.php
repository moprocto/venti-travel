<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use Http;
use Redirect;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Kreait\Firebase\Exception\FirebaseException;
use SKAgarwal\GoogleApi\PlacesApi;
use Barryvdh\DomPDF\Facade\Pdf;
use Mail;
use View;
use Route;
use Validator;
use Firebase\JWT\JWT;
use Firebase\JWT\Key;

class GroupsController extends Controller
{
    public function browse(){
        $user = null;
        $auth = app('firebase.auth');

        if(Session::get('user') !== null && isset(Session::get('user')->uid)){
            $user = Session::get('user')->uid;
            $user = $auth->getUser($user);
        }

        $data = [
            "user" => $user
        ];

        return view("groups.browse", $data);
    }

    public function browseAJAX($limit = 25, $cache){
        // cache Y means return cached version
        $user = null;
        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $auth = app('firebase.auth');
        $ideas = $db->collection('Ideas');
        $ideas = $ideas->where("archived","=", 0)->limit($limit)->documents();
        $ideaList = [];

        if(Session::get('user') !== null && isset(Session::get('user')->uid)){
            $user = Session::get('user')->uid;
            $user = $auth->getUser($user);
            Session::put('user', $user);
        }

        if(Session::get('groups') !== null){
            $now = strtotime("now"); 
            if($now - Session::get('groups')["set"]  < 600 && $cache == "Y"){
                return json_encode(Session::get('groups')["groups"]);
            }   
        }


        foreach($ideas as $idea){
            $data = $idea->data();


            if(array_key_exists("clientID", $data) && array_key_exists("group", $data)){
                if($data["clientID"] == "mnTmABt3MDOWyy76pwG7V0QACpD2"){
                    // for now, venti admin creations are hidden for all
                    continue;
                }
                try{
                    if (isset($data["privacy"]) && ($data["privacy"] != "full" && $data["privacy"] != "Invitation Only. Invisible to Others")) {
                        //make sure it is visible to the public

                        $isTravel = true;

                        if(array_key_exists("type", $data) && strtolower($data["type"]) == "activity"){
                            $isTravel = false;
                            $daysRemaining = false;
                            $checkOut = null;
                        }

                        if($isTravel == true){
                            //make sure it's not in the past
                            $departure = Carbon::createFromFormat('Y-m-d', str_replace("/","-",$data["departure"]))->timezone('America/New_York');
                            $today = Carbon::now()->timezone('America/New_York');
                            
                            $checkOut = Carbon::createFromFormat('Y-m-d', str_replace("/","-",$data["departure"]))->timezone('America/New_York')->addDays($data["days"]);

                            $daysRemaining = $departure->isPast();
                        }

                        

                        if($daysRemaining == false){
                            $data["daysRemaining"] = $daysRemaining;

                            try{
                                $author = $auth->getUser($data["clientID"]);
                               

                                if($author->disabled == true){
                                    continue;
                                }

                                $data["owner"] = $data["clientID"];
                                $data["checkOut"] = $checkOut;
                                $data["author"] = (array) $author;
                                $data["groupSize"] = getGroupSize($data["group"]);
                                $data["groupChemistry"] = 0;
                                $data["isTravel"] = $isTravel;
                                // get travel group avatars

                                $groupMembers = $data["group"];
                                $groupAvatars = [];


                                $groupMembers = organizeMembersList($groupMembers);

                                for($i = 0; $i < 3; $i++){
                                    if(array_key_exists($i, $groupMembers)){
                                        if($groupMembers[$i]["role"] == "viewer"){
                                            $member = $auth->getUser($groupMembers[$i]["uid"]);
                                            array_push($groupAvatars, $member->photoUrl);
                                        }
                                    }
                                }
                                $data["groupAvatars"] = $groupAvatars;

                                // here we calculate group chemistry

                                if(!is_null($user)){
                                    if($data["clientID"] != $user->uid){
                                        //$groupChemistry = $this->calcGroupCompatibility($auth, $user, $data["group"]);

                                        $data["groupChemistry"] = 0; //$groupChemistry;
                                    }
                                }
                                $isNavigated = false;

                                if(array_key_exists("navigated",$data) && $data["navigated"] == "1"){
                                    $data = View::make('groups.components.widget-ajax-navigated',compact('data'))->render();
                                }
                                else {
                                    if($isTravel == true){
                                        $data = View::make('groups.components.widget-ajax-travel',compact('data'))->render();
                                    }
                                    else {
                                        $data = View::make('groups.components.widget-ajax-activity',compact('data'))->render();
                                    }
                                }

                                

                                array_push($ideaList, $data);
                            }
                            catch(FirebaseException $e){

                            }
                        }
                    }
                }
                catch(FirebaseException $e){

                }
            }
        }
        if($cache == "Y"){
        Session::put('groups', ["groups" => $ideaList, "set" => strtotime("now")]); 
        }
        

        return json_encode($ideaList);
    }

    public function createGroup(){
        $auth = app('firebase.auth');
        if(Session::get('user') === null){
            // The user needs to be logged in to create a trip idea
            return view('errors.405');
        }
        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $user = Session::get('user');
        $user = $auth->getUserByEmail($user->email);

        if($user->emailVerified !== true){
            return view('errors.407');
        }

        $size = 8;


        if(array_key_exists("sizePref", Session::get('user')->customClaims)){
            switch (Session::get('user')->customClaims['sizePref']) {
                case 'small':
                    $size = 4;
                    break;
                case 'medium':
                    $size = 10;
                    break;
                case 'large':
                    $size = 20;
                    break;
                default:
                    break;
            }
        }

        $data = [
            "size" => $size
        ];

        return view("groups.create", $data);
    }

    public function addGroup(Request $request){
        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $tags = [];

        if($request->tags != null && $request->tags != ""){
            if(is_string($request->tags)){
                $tags = explode(",", $request->tags);
            }
            else {
                $tags = $request->tags;
            }
            
        }

        if($request->type == "travel"){
            if($request->destination == "" || !isset($request->destination) || is_null($request->destination)){
                Session::flash('destinationError', 'yes');
                return Redirect::back();
            }
        }

        $title = "";
        if(isset($request->title) && $request->title != ""){
            $title = $request->title;
        }

        $navigated = 0;

        if((Session::get('user')->customClaims["nav"] ?? null) == true){
            $navigated = $request->navigated;
        }

        $category = null;

        try{
            $category = $this->googlePlaces($request->country);
        }
        catch(Exception $e){

        }
        

        $docRef = $db->collection("Ideas")->document($request->tripID)->set(
            [             
                "tripID" => $request->tripID,
                "archived" => 0,
                "budgetOverall" => 0,
                "country" => $request->country,
                "latitude" => $request->latitude,
                "longitude" => $request->longitude,
                "destination" => $request->destination,
                "title" => $title,
                "departure" => $request->departure,
                "days" => $request->nights + 1,
                "category" => $category,
                "favorite" => 0,
                "foodBudget" => 0,
                "lodgingBudget" => 0,
                "transportationBudget" => 0,
                "activityBudget" => 0,
                "tags" => $tags,
                "imageURL" => $request->imageURL,
                "imageAuthor" => $request->imageAuthor,
                "imageAuthorProfile" => $request->imageAuthorProfile,
                "navigated" => $navigated,
                "personalizationCost" => 0,
                "personalizationNote" => "",
                "purchasePrice" => 0,
                "purchaseNote" => "",
                "clientID" => Session::get('user')->uid,
                "description" => $request->description,
                "worldee" => "",
                "sizePref" => $request->sizePref,
                "privacy" => $request->privacy,
                "group" => [0 => ["uid" => Session::get('user')->uid,"role" => "owner"]],
                "itinerary" => "",
                "type" => $request->type,
                "color" => getRandomColor()
            ]
        );


        return Redirect::to('/group/' . $request->tripID);
    }

    public function editGroup(Request $request){
        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $ideaDoc = $db->collection("Ideas")->document($request->tripID);
        $idea = $ideaDoc->snapshot()->data();
        $isTravel = true;

        if(array_key_exists("type", $idea) && $idea["type"] == "activity"){
            $isTravel = false;
        }

        if($isTravel == true){
            if($request->departure == "" || $request->nights == ""){
                Session::flash('editGroupError', "Each trip must have a departure date and set number of days");
                return Redirect::back();
            }


            if(Session::get('user')->uid == $idea["clientID"]){
                // gotta be the owner to edit

                $tagsToObject = [];
                if(isset($request->tags)){
                    $tagsToObject = tagsToObject($request->tags);
                }

                $update = $ideaDoc->update([
                    ["path" => "title","value" => $request->title],
                    ["path" => "description","value" => $request->description],
                    ["path" => "departure","value" => $request->departure],
                    ["path" => "days","value" => $request->nights + 1],
                    ["path" => "sizePref","value" => $request->sizePref],
                    ["path" => "privacy","value" => $request->privacy],
                    ["path" => "tags","value" => $tagsToObject]
                ]);

                if($request->selectedImage != null){
                    $update = $ideaDoc->update([
                        ["path" => "imageURL","value" => $request->selectedImage]
                    ]);
                }
                Session::flash('editGroupSuccess', "Great!");
                return Redirect::back();
            }
        }

        else {

            if(Session::get('user')->uid == $idea["clientID"]){
                // gotta be the owner to edit
                $update = $ideaDoc->update([
                    ["path" => "title","value" => $request->title],
                    ["path" => "description","value" => $request->description],
                    ["path" => "color", "value" => $request->color],
                    ["path" => "sizePref","value" => $request->sizePref],
                    ["path" => "privacy","value" => $request->privacy],
                    ["path" => "tags","value" => $request->tags]
                ]);

                Session::flash('editGroupSuccess', "Great!");
                return Redirect::back();
            }

        }

        
    }

    public function groupInvite(Request $request){
        $invitee = $request->user;

        if(Session::get('user')->customClaims['username'] == $invitee || Session::get('user')->email == $invitee){
            // can't add yourself :)
            return 501;
        }
        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $auth = app('firebase.auth');
        $tripID = $request->tripID;
        
        $user = null;

        // check if the user is already a user


        // before we waste any compute, check how many open invites exist for the group




        if($invitee == ""){
            return 500; // user cannot be blank
        }

        // check if already in group
            $ideaDoc = $db->collection("Ideas")->document($tripID);
            $idea = $ideaDoc->snapshot()->data();

            $openInvites = countInvited($idea["group"]);

        if($openInvites > 10){
            return 507; // too many invites
        }

        try{
            $user = $auth->getUserByEmail($invitee);
            

        } catch(FirebaseException $e){
            // email is invalid or it's a username
            $username = validateUsername($invitee, $db);
            if($username !== false){
                try{
                    $user = $auth->getUser($username["uid"]);
                }
                catch(FirebaseException $e){
                    $user = null;
                }
            }
        }

        

        if(!is_null($user)){

            if(array_key_exists("requests", $user->customClaims) && $user->customClaims["requests"] != "all"){
                return 505;
            }

            $compatibility = calcCompatibility(Session::get('user'), $user);
            $compatibility = explode(" ",$compatibility)[1] ?? null;

            if($compatibility == "" || $compatibility < 50){
                return 505;
            }

            

            $isTravel = true;

            if(array_key_exists("type", $idea) && $idea["type"] == "activity"){
                $isTravel = false;
            }

            if(Session::get('user')->uid == $idea["clientID"]){
                // I'm the owner
                $members = $idea["group"];
                $count = 0;
                foreach($members as $member){
                    if($member["uid"] == $user->uid){
                        // user already in the group
                        $role = getGroupRole($user->uid, $idea);
                        if($role == "viewer" || $role == "invited" || $role == "decline"){
                            return 502;
                        }
                        return 503;
                    }
                }
                if($count == 0){
                    // user is not in the group
                    array_push($members, ["uid" => $user->uid, "role" => "invited"]);
                    $update = $ideaDoc->update([
                        ["path" => "group","value" => $members],
                    ]);


                    

                    if($isTravel == true){
                        $subject =  getFirstName(Session::get('user')->displayName) . " is inviting you to join a travel group to " . getTripLocation($idea["destination"], $idea["country"]);
                    } else {
                        $subject =  getFirstName(Session::get('user')->displayName) . " is inviting you to join their group: " . $idea["title"];
                    }

                    $idea["isTravel"] = $isTravel;
                    $this->attemptEmailSend($user, $idea, $subject, "emailInvited", "emails.application.invited", $isTravel, $db);
                    
                    return json_encode([
                        "displayName" => getFirstName($user->displayName),
                        "photoUrl" => $user->photoUrl,
                        "username" => $user->customClaims["username"],
                        "uid" => $user->uid,
                        "role" => "viewer"
                    ]);
                }
            }
        }

        $emailValidator = Validator::make(['email' => $invitee],[
          'email' => 'required|email'
        ]);

        if($emailValidator->passes()){
            // invite the off-platform user here
            $ideaDoc = $db->collection("Ideas")->document($tripID);
            $idea = $ideaDoc->snapshot()->data();

            $author = Session::get('user');

            $data = [
                "author" => $author,
                "trip" => (object) $idea,
                "isTravel" => $isTravel
            ];

            $email = $invitee;
            $emailCC = $author->email;

            if($isTravel == true){
                $subject =  getFirstName($author->displayName) . " is inviting you to join their travel group to " . getTripLocation($idea["destination"], $idea["country"]);
            } else {
                $subject =  getFirstName($author->displayName) . " is inviting you to join their group: " . $idea["title"];
            }
                
            Mail::send("emails.application.organic-invite", array('data' => $data), function($message) use($email, $emailCC, $subject)
            {
                $message
                        ->to($email)
                        ->cc($emailCC)
                        ->replyTo("no-reply@venti.co")
                        ->subject($subject);
            });

            return 506; // not found but send invite
        }

        return 504; // not found

        /*

        

        if(is_null($members) || !is_array($members)){
            return 501; // group doesn't exist
        }

        */
    }

    public function readGroup($tripID){
        $user = Session::get('user');
        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $ideaDoc = $db->collection("Ideas")->document($tripID);
        $idea = $ideaDoc->snapshot()->data();
        $isMember = false;
        $myRole = $title = $description = null;

        if(is_null($idea)){
            return view("errors.404");
        }

        if($idea["privacy"] == "Invitation Only. Invisible to Others"){
            if(Session::get('user') === null){
                return view("errors.405");
            }
        }

        $uids = $members = [];
        $idea["group"] = organizeMembersList($idea["group"]);

        if(is_array($idea["group"])){
            foreach($idea["group"] as $member){
                if($member["role"] != "decline" && $member["role"] != "denied"){
                    array_push($uids, $member["uid"]);
                    if(Session::get('user') !== null && Session::get('user')->uid == $member["uid"]){
                        $isMember = true;
                    }
                }
                if($member["role"] != "denied"){
                    if(Session::get('user') !== null && Session::get('user')->uid == $member["uid"]){
                        $myRole = $member["role"];
                    }
                }
            }
        }

        if($idea["privacy"] == "Invitation Only. Invisible to Others"){
            if(is_null($myRole)){
                return view("errors.408");
            }
        }

        $users = $this->getMembers($uids);

        foreach($idea["group"] as $member){

            if(array_key_exists($member["uid"], $users)){
                array_push($members, ["member" => $users[$member["uid"]], "role" => $member["role"]]);
            }
        }

        if($idea["description"] != ""){
            $description = $idea["description"];
        }

        if($idea["title"] != ""){
            $title = $idea["title"];
        }

        if(!is_array($idea["tags"])){
            $idea["tags"] = explode(",", $idea["tags"]);
        }
        $isTravel = true;

        if(array_key_exists("type", $idea) && strtolower($idea["type"]) == "activity" || $idea["clientID"] == "mnTmABt3MDOWyy76pwG7V0QACpD2"){
            $isTravel = false;
        }

        if($isTravel == true){
            $departure = $checkIn = Carbon::createFromFormat('Y-m-d', str_replace("/","-",$idea["departure"]))->timezone('America/New_York');
            $today = Carbon::now()->timezone('America/New_York');
            $daysRemaining = (int) $today->diffInDays($departure, false);
            $checkOut = $checkIn->addDays($idea["days"]);
            $idea["checkOut"] = $checkOut;
            $idea["daysRemaining"] = $daysRemaining;
        }

        

        $idea["groupSize"] = getGroupSize($idea["group"]);
        $VIDEOSDK_JWT = Session::get($idea["tripID"] . "-JWT");

        if($isMember == true && $VIDEOSDK_JWT === null){
            $key = env("VIDEOSDK_API_SECRET");

            $payload = [
                'apikey' => env('VIDEOSDK_API_KEY'),
                'permissions' => array("allow_join", "allow_mod"),
                'iss' => 'https://api.videosdk.live',
                'aud' => env("APP_URL"),
                'iat' => strtotime("now"),
                'exp' => strtotime('+1 hour')
            ];

            try{
                $jwt = JWT::encode($payload, $key, 'HS256');
                Session::put($idea["tripID"] . "-JWT", $jwt);

                if(array_key_exists("VIDEOSDK_MEETING_ID", $idea)){
                    $videoMeeting = $this->createVideoMeeting($jwt, $idea["VIDEOSDK_MEETING_ID"], $user, "validate");
                    if($videoMeeting["disabled"] == true){
                        // old meeting ID has expired.. create a new one
                        $videoMeeting = $this->createVideoMeeting($jwt, null, $user, "create");
                    }
                } else {
                    $videoMeeting = $this->createVideoMeeting($jwt, null, $user, "create");
                    $videoMeeting =  $videoMeeting["meetingId"];
                }

                if(is_array($videoMeeting) && array_key_exists("meetingId", $videoMeeting)){
                    // update the group to include the new meetingID
                    $update = $ideaDoc->update([
                        ["path" => "VIDEOSDK_MEETING_ID","value" => $videoMeeting["meetingId"]],
                    ]);
                }
                if(is_string($videoMeeting)){
                    $update = $ideaDoc->update([
                        ["path" => "VIDEOSDK_MEETING_ID","value" => $videoMeeting],
                    ]);
                }

            } catch(Exception $e){

            }
        }

        $owner = $members[0];
        unset($members[0]);

        $organizedMembers = collect($members)->sortBy('role')->reverse()->toArray();
        $organizedMembers[0] =  $owner;
        $organizedMembers = move_to_top($organizedMembers, 0);

        $rec = false;
        if($idea["clientID"] == "mnTmABt3MDOWyy76pwG7V0QACpD2"){
            $rec = true;
        }

        $data = [
            "group" => $idea,
            "author" => $owner["member"],
            "members" => $organizedMembers,
            "isMember" => $isMember,
            "myRole" => $myRole,
            "description" => $description,
            "title" => $title,
            "url" => Route('read-group', ["tripID" => $idea["tripID"]]),
            "image" => $idea["imageURL"],
            "isTravel" => $isTravel,
            "recommended" => $rec
        ];
        
        return view('groups.read', $data);
    }

    public function getMembers($uids){
        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $auth = app('firebase.auth');
        $users = [];
        
        try{
            $users = $auth->getUsers($uids);
        } catch(FirebaseException $e){
            
        }

        return $users;
    }

    public function getPlanner($tripID){
        
    }

    public function editPlanner(Request $request){
        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $ideaDoc = $db->collection("Ideas")->document($request->tripID);
        $idea = $ideaDoc->snapshot()->data();

        if(Session::get('user')->uid == $idea["clientID"]){
            $update = $ideaDoc->update([[
                    "path" => "itinerary",
                    "value" => $request->plan
            ]]);
            return 200;
        }
        return 201;
    }

    public function groupRemoveMember(Request $request){
        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $ideaDoc = $db->collection("Ideas")->document($request->tripID);
        $idea = $ideaDoc->snapshot()->data();
        $user = $request->user;

        if(Session::get('user')->uid == $idea["clientID"]){
            if(Session::get('user')->uid != $user){
                // can't delete yourself
                $members = $idea["group"];

                $key = array_search($user, array_column($members, 'uid'));

                if(array_key_exists($key, $members)){
                    unset($members[$key]);
                }   

                $members = array_values($members); 
                
                $update = $ideaDoc->update([
                        ["path" => "group","value" => $members],
                ]);
                
                return 200;
            }
        }
    }

    public function archiveTrip($tripID){
        $user = Session::get('user');
        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $ideaDoc = $db->collection("Ideas")->document($tripID);
        $idea = $ideaDoc->snapshot()->data();

        if($idea["clientID"] != $user->uid){
            // not the owner
            return Redirect::back();
        }

        $update = $ideaDoc->update([
            ["path" => "archived","value" => 1],
        ]);

        Session::flash("archived");

        return Redirect::back();

    }

    public function republishTrip($tripID){
        $user = Session::get('user');
        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $ideaDoc = $db->collection("Ideas")->document($tripID);
        $idea = $ideaDoc->snapshot()->data();

        if($idea["clientID"] != $user->uid){
            // not the owner
            return Redirect::back();
        }

        $update = $ideaDoc->update([
            ["path" => "archived","value" => 0],
        ]);

        Session::flash("published");

        return Redirect::back();

    }

    public function groupInviteReply(Request $request){
        // this function fires when the owner of a trip makes a decision on requests to join their travel group
        // or when the invited user makes a decision
        // based on the owner's decision, the group attribute will update
        // an email is sent to user that sent the request if the owner approves them

        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $ideaDoc = $db->collection("Ideas")->document($request->tripID);
        $idea = $ideaDoc->snapshot()->data();
        $user = $request->uid;
        if(!isset($user)){
            $user = Session::get('user')->uid;
        }
        $decide = $request->decide;
        $group = $idea["group"];
        $key = array_search($user, array_column($group, 'uid'));

        if($idea["clientID"] != Session::get('user')->uid && Session::get('user')->uid != $group[$key]["uid"]){
            // don't have permission to do this
            return 500;
        }

        $isTravel = true;

        if(array_key_exists("type", $idea) && $idea["type"] == "activity"){
            $isTravel = false;
        }

         $rec = false;
            if($idea["clientID"] == "mnTmABt3MDOWyy76pwG7V0QACpD2"){
                $rec = true;
            }

        switch ($decide) {

            case 'accept':
                // the invitee accepted the owner's request
                // the owner approved the request
                $group[$key]["role"] = "viewer";

                $update = $ideaDoc->update([
                        ["path" => "group","value" => $group],
                ]);

                    // email them
                    $auth = app('firebase.auth');

                    $user = Session::get('user'); // the logged in person
                    $author = $auth->getUser($idea["clientID"]);
                    $emailsOk = $this->getUserSettings($author, "emailInviteAccepted", $db);

                    if($emailsOk){
                        $data = [
                            "author" => $author,
                            "user" => $user,
                            "trip" => (object) $idea,
                            "isTravel" => $isTravel
                        ];

                        $email = $author->email;

                        if($isTravel == true){
                            $subject =  getFirstName($user->displayName) . " accepted your invitation to join your group to " . getTripLocation($idea["destination"], $idea["country"]);
                        } else {
                            $subject =  getFirstName($user->displayName) . " accepted your invitation to join your group: " . $idea["title"];
                        }
                        if(!$rec){
                            Mail::send('emails.application.accepted', array('data' => $data), function($message) use($email, $subject)
                            {
                                $message
                                        ->to($email)
                                        ->replyTo("no-reply@venti.co")
                                        ->subject($subject);
                            });
                        }
                        
                    }
                    //
                    return 200;

            case 'approve':
                // the owner approved the request
                $group[$key]["role"] = "viewer";

                $update = $ideaDoc->update([
                        ["path" => "group","value" => $group],
                ]);

                    // email them
                    $auth = app('firebase.auth');

                    $author = Session::get('user'); // the logged in person
                    $user = $auth->getUser($request->uid);
                    $emailsOk = $this->getUserSettings($user, "emailInviteAccepted", $db);

                    if($emailsOk){
                        $data = [
                            "author" => $author,
                            "user" => $user,
                            "trip" => (object) $idea,
                            "isTravel" => $isTravel
                        ];

                        $email = $user->email;

                        if($isTravel == true){
                            $subject =  getFirstName($author->displayName) . " approved your request to join their travel group to " . getTripLocation($idea["destination"], $idea["country"]);
                        }
                        else {
                            $subject =  getFirstName($author->displayName) . " approved your request to join their group: " . $idea["title"];
                        }
                        
                            
                        Mail::send('emails.application.approved', array('data' => $data), function($message) use($email, $subject)
                        {
                            $message
                                    ->to($email)
                                    ->replyTo("no-reply@venti.co")
                                    ->subject($subject);
                        });
                    }
                    //
                    return 200;
                break;
            case 'decline':
                $group[$key]["role"] = "decline";
                $update = $ideaDoc->update([
                        ["path" => "group","value" => $group],
                ]);

                $declineID = strtotime("now");
               

                $docRef = $db->collection("Declines")->document($declineID)->set(
                    [             
                        "tripID" => $idea["tripID"],
                        "uid" => Session::get('user')->uid,
                        "recommended" => $rec,
                        "reason" => $request->reason,
                        "createdAt" => Date("Y-m-d"),
                        "declineID" => $declineID,
                        "type" => $idea["type"] ?? null
                    ]
                );


                return 201;
                break;
            case 'denied':
                $group[$key]["role"] = "denied";
                $update = $ideaDoc->update([
                        ["path" => "group","value" => $group],
                ]);
                return 201;
                break;
            case 'cancel':
                unset($group[$key]);
                $group = array_values($group);
                $update = $ideaDoc->update([
                        ["path" => "group","value" => $group],
                ]);
                return 201;
                break;
            default:
                // code...
                break;
        }

        return 200;

    }

    public function readGroupJoin($tripID){
        // this function is fired when a user visits a travel group page and requests to join
        // the group attribute is updated
        // the author receives an email or push notification

        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $ideaDoc = $db->collection("Ideas")->document($tripID);
        $idea = $ideaDoc->snapshot()->data();
        $user = Session::get('user');

        if(!isset($user->uid)){
            return view('errors.405');
        }



        if($user->uid == $idea["clientID"] || $idea["privacy"] != "Anyone Can Request to Join"){
            return Redirect::back();
        }

        if(!is_null($idea)){
            // idea is valid
            // check if member
            $group = $idea["group"];
            $auth = app('firebase.auth');


            $key = array_search($user->uid, array_column($group, 'uid'));

            $isTravel = true;

                if(array_key_exists("type", $idea) && $idea["type"] == "activity"){
                    $isTravel = false;
                }

            if($key === false){
                array_push($group,["uid" => $user->uid, "role" => "requested"]);
                $update = $ideaDoc->update([
                        ["path" => "group","value" => $group],
                ]);

                // send email to author of the trip
                $author = $auth->getUser($idea["clientID"]);

                

                if($isTravel == true){
                    $subject = getFirstName($user->displayName) . " has requested to join your group to " . getTripLocation($idea["destination"], $idea["country"]);
                } else {
                    $subject = getFirstName($user->displayName) . " has requested to join your group: " . $idea["title"];
                }

                

                $idea["isTravel"] = $isTravel;
                $this->attemptEmailSend($author, $idea, $subject, "emailJoinRequest", "emails.application.requested", $isTravel, $db);


            } else {
                // if the user has declined this in the past, they can request to join again
                $role = $group[$key]["role"];
                if($role == "decline"){
                    unset($group[$key]);
                    $group = array_values($group);
                    array_push($group,["uid" => $user->uid, "role" => "requested"]);

                    $update = $ideaDoc->update([
                        ["path" => "group","value" => $group],
                    ]);

                    // send email to author of the trip
                    $author = $auth->getUser($idea["clientID"]);

                    if($isTravel == true){
                        $subject = getFirstName($user->displayName) . " has requested to join your group to " . getTripLocation($idea["destination"], $idea["country"]);
                    } else {
                        $subject = getFirstName($user->displayName) . " has requested to join your group: " . $idea["title"];
                    }

                $idea["isTravel"] = $isTravel;
                    $this->attemptEmailSend($author, $idea, $subject, "emailJoinRequest", "emails.application.requested", $isTravel, $db);

                }
            }

        } else{
            return view('errors.404');
        }

        return Redirect::back();
    }

    public function readGroupLeave($tripID){

        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $ideaDoc = $db->collection("Ideas")->document($tripID);
        $idea = $ideaDoc->snapshot()->data();
        $user = Session::get('user');

        if(!isset($user->uid)){
            return view('errors.405');
        }

        if($user->uid == $idea["clientID"] || $idea["privacy"] != "Anyone Can Request to Join"){
            return Redirect::back();
        }

        if(!is_null($idea)){
            $group = $idea["group"];

            $key = array_search($user->uid, array_column($group, 'uid'));

            if($key !== false){
                unset($group[$key]);
                $group = array_values($group);

                $update = $ideaDoc->update([
                        ["path" => "group","value" => $group],
                ]);
            }

        } else{
            return view('errors.404');
        }

        return Redirect::back();
    }

    public function attemptEmailSend($user, $trip, $subject, $permission, $template, $isTravel, $db){

        $emailsOk = $this->getUserSettings($user, $permission, $db);

        if($emailsOk){
            $author = Session::get('user');

            $data = [
                "author" => $author,
                "user" => $user,
                "trip" => (object) $trip,
                "isTravel" => $isTravel
            ];

            $email = $user->email;
            
                
            Mail::send($template, array('data' => $data), function($message) use($email, $subject)
            {
                $message
                        ->to($email)
                        ->replyTo("no-reply@venti.co")
                        ->subject($subject);
            });
            
        }
    }

    public function getStudio($tripID){
        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $ideaDoc = $db->collection("Ideas")->document($tripID);
        $idea = $ideaDoc->snapshot()->data();
        $user = Session::get('user');
        if(Session::get('user') === null){
            return view('errors.405');
        }


        $group = $idea["group"];

        $role = getGroupRole($user->uid, $idea);

        if($role == "owner" || $role == "viewer"){
            $auth = app('firebase.auth');
            $departure = $checkIn = Carbon::createFromFormat('Y-m-d', str_replace("/","-",$idea["departure"]))->timezone('America/New_York');
            $today = Carbon::now()->timezone('America/New_York');
            $idea["checkOut"] = $checkIn->addDays($idea["days"]);

            $groupMembers = $idea["group"];
            $groupAvatars = [];

            $uids = $members = [];

            if(is_array($idea["group"])){
                foreach($idea["group"] as $member){
                    if($member["role"] != "decline" && $member["role"] != "denied"){
                        array_push($uids, $member["uid"]);
                        if(Session::get('user') !== null && Session::get('user')->uid == $member["uid"]){
                            $isMember = true;
                        }
                    }
                    if($member["role"] != "denied"){
                        if(Session::get('user') !== null && Session::get('user')->uid == $member["uid"]){
                            $myRole = $member["role"];
                        }
                    }
                }
            }

            $users = $this->getMembers($uids);

            foreach($idea["group"] as $member){
                if(array_key_exists($member["uid"], $users)){
                    array_push($members, ["member" => $users[$member["uid"]], "role" => $member["role"]]);
                }
            }

            for($i = 0; $i < 3; $i++){
                if(array_key_exists($i, $groupMembers)){
                    if($groupMembers[$i]["role"] == "viewer"){
                        $member = $auth->getUser($groupMembers[$i]["uid"]);
                        array_push($groupAvatars, $member->photoUrl);
                    }
                }
            }
            $idea["groupAvatars"] = $groupAvatars;

            $data = [
                "group" => $idea,
                "myRole" => $role,
                "members" => $members
            ];

            return view("groups.studio", $data);

        }

        return view('errors.408');
    }

    public function studioRefresh($tripID){
        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $ideaDoc = $db->collection("Ideas")->document($tripID);
        $idea = $ideaDoc->snapshot()->data();
        $user = Session::get('user');
        if(Session::get('user') === null){
            return view('errors.405');
        }

        $group = $idea["group"];

        $role = getGroupRole($user->uid, $idea);

        if($role == "owner" || $role == "viewer"){
            return json_encode($idea["itinerary"]);
        }
    }

    public function studioPDF($tripID){
        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $ideaDoc = $db->collection("Ideas")->document($tripID);
        $idea = $ideaDoc->snapshot()->data();
        $user = Session::get('user');
        $auth = app('firebase.auth');
        if(Session::get('user') === null){
            return view('errors.405');
        }

        $group = $idea["group"];

        $role = getGroupRole($user->uid, $idea);

        if($role == "owner" || $role == "viewer"){
            $departure = $checkIn = Carbon::createFromFormat('Y-m-d', str_replace("/","-",$idea["departure"]))->timezone('America/New_York');
            $today = Carbon::now()->timezone('America/New_York');
            $idea["checkOut"] = $checkIn->addDays($idea["days"]);

            $groupMembers = $idea["group"];
            $groupAvatars = [];

            $uids = $members = [];

            if(is_array($idea["group"])){
                foreach($idea["group"] as $member){
                    if($member["role"] != "decline" && $member["role"] != "denied"){
                        array_push($uids, $member["uid"]);
                        if(Session::get('user') !== null && Session::get('user')->uid == $member["uid"]){
                            $isMember = true;
                        }
                    }
                    if($member["role"] != "denied"){
                        if(Session::get('user') !== null && Session::get('user')->uid == $member["uid"]){
                            $myRole = $member["role"];
                        }
                    }
                }
            }

            $users = $this->getMembers($uids);

            foreach($idea["group"] as $member){
                if(array_key_exists($member["uid"], $users)){
                    array_push($members, ["member" => $users[$member["uid"]], "role" => $member["role"]]);
                }
            }

            for($i = 0; $i < 3; $i++){
                if(array_key_exists($i, $groupMembers)){
                    if($groupMembers[$i]["role"] == "viewer"){
                        $member = $auth->getUser($groupMembers[$i]["uid"]);
                        array_push($groupAvatars, $member->photoUrl);
                    }
                }
            }
            $data["groupAvatars"] = $groupAvatars;

            $data = [
                "group" => $idea,
                "members" => $members
            ];

            $pdf = Pdf::loadView('groups.components.pdf', $data);
            
            return $pdf->download(str_replace(" ","_", $idea["title"]) . '.pdf');
        } else {
            return view('errors.408');
        }
    }

    public function getUserSettings($user, $perm, $db){
        $settings = $db->collection("users/" . $user->uid . "/Settings")->document("data")->snapshot()->data();
        $emailsOk = true;

        if(!is_null($settings)){
            if(array_key_exists($perm, $settings) && $settings[$perm] == 0){
                // user opted out
                $emailsOk = false;
            }
        }

        return $emailsOk;
    }

    public function calcGroupCompatibility($auth, $match, $groupMembers){
        // first, we have to check if the logged in user has a completed profile before we waste computation

        if(isProfileComplete($match)){
            // step one complete 
            // next we need to loop through the groupMembers array
            $groupChemistryScore = $records = $overallChemistryPercentage = 0;
            foreach($groupMembers as $groupMember){
                // check if their profile is complete
                // unfortunately, we have to grab the Firebase object for every person
                try{
                    $matchee = $auth->getUser($groupMember["uid"]);
                    if(isProfileComplete($matchee)){
                        // now we can calculate compatibility among those that are owner or viewers
                        if($groupMember["role"] == "viewer" || $groupMember["role"] == "owner"){
                            $compatibility = explode(" ", calcCompatibility(Session::get('user'), $matchee) );
                            if(array_key_exists(1, $compatibility)){
                                $groupChemistryScore += $compatibility[1];
                                $records++;
                            }
                        }
                    }
                }
                catch(FirebaseException $e){

                }
            }
            if($records > 0){
                $overallChemistryPercentage = round(($groupChemistryScore / $records), 0);
            }
            

            return $overallChemistryPercentage;
            
        }

        return null;
    }

    public function createVideoMeeting($token, $meetingID, $userID, $mode = "create"){
        // creates a meeting ID for the video call if it doesn't already exist or is expired    

          $post_data = json_encode(["userMeetingId" => $userID]);
            
          // Prepare new cURL resource
          $url = "https://api.videosdk.live/v1/meetings";

          if($mode == "validate"){
            $url = "https://api.videosdk.live/v1/meetings/$meetingID";
          }

          $result = Http::withHeaders([
                "Content-Type" => "application/json",
                "authorization" => $token
            ])->post($url, [
                'name' => 'Taylor',
            ]);

          return json_decode($result, true);
    }

    public function googlePlaces($countryName){
        $googlePlaces = new PlacesApi(env('GOOGLE_API_KEY'));

        $response = $googlePlaces->findPlace($countryName, "textquery"); # line 2

        if(isset($response["candidates"])){
            foreach($response["candidates"] as $prediction){
                if(strlen($prediction["place_id"]) > 120){
                    return view('errors.410');
                }
                $responses = $googlePlaces->placeDetails($prediction["place_id"]);
                foreach($responses as $response){
                    $continent = $responses["result"]["address_components"][0]["short_name"];
                    return getContinent($continent);
                }
            }
        }
    }
}
