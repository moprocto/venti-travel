<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Redirect;
use Session;
use Mail;
use Http;
use Exception;

class BoardingAdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        // show admin dashboard with contacts data

        $data = [
            "vips" => $this->subscribers()
        ];

        return view("admin.boardingpass.index", $data);
    }

    public function subscribers(){

        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $auth = app('firebase.auth');

        $subscribersList = $db->collection("VIPS")->documents();

        $subscribers = [];

        foreach($subscribersList as $contact){
            $contact = $contact->data();
            $fields = $contact["fields"];

            array_push($subscribers, [
                "email" => $contact["email_address"],
                "fields" => $fields,
                "code" => getEmailID($contact["id"]),
                "id" => $contact["id"]
            ]);
        }

        return $subscribers;
    }
}