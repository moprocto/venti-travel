<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Session;
use Location;
use Redirect;
use Mail;
use Exception;
use Route;
use View;
use Dwolla;
use Validator;

use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use Kreait\Firebase\Database;
use Kreait\Firebase\Auth;
use Google\Cloud\Firestore\FirestoreClient;
use Kreait\Firebase\Exception\FirebaseException;

use Carbon\Carbon;
use Carbon\CarbonPeriod;

use Algolia\AlgoliaSearch\SearchClient;
use Stripe\Stripe as Stripe;
use \Stripe\StripeClient as StripeClient;


class FlightsController extends Controller
{
    public function index(Request $request){

        if($request->fbid == "fb"){
            Session::put('referrer', "facebook");
        }
        elseif($request->fbid == "ig"){
            Session::put('referrer', "instagram");
        }
        elseif($request->fbid == "msg"){
            Session::put('referrer', "messenger");
        }
        elseif($request->fbid == "adwords"){
            Session::put('referrer', "adwords");
        }
        elseif(strlen($request->fbid) > 12){
            Session::put('referrer', $request->fbid);
        }
        if(Session::get('fbclid') === null){
            Session::put('fbclid', $request->fbclid);
        }

        $data = [
            "title" => "Book Flights with Venti",
            "description" => "Get flights up to 100% off",
            "priorSearch" => Session::get('flight-search') ?? []
        ];

        if(env('APP_ENV') == "local"){
            // get the IP address
            $ip = \Request::ip();
            if($ip != "68.100.62.8" && $ip != "127.0.0.1"){
                return Redirect::to('https://venti.co/flights');
            }
        }
        
        return view('flights.index', $data);
    }

    public function airports(Request $request){
        if(strlen($request->term) < 3){
            return false;
        }

        $client = SearchClient::create(env("ALGOLIA_APP_ID"),env("ALGOLIA_APP_KEY"));
        $index = $client->initIndex("Airports");
        $hits = $index->search($request->term)["hits"] ?? null;
        $results = [];

        if($hits){
            for($i =0; $i<10;$i++){

                $hit = $hits[$i] ?? null;

                if($hit){
                    array_push($results, [
                        "id" => $i,
                        "text" => "(" . $hit["iata_code"] . ") " . $hit["name"]
                    ]);
                }
            }
        }

        return json_encode(
            [
                "results" => json_encode($results)
            ]
        );
    
    }

    public function nearbyAirports($lat, $long){
        /*
            curl -X GET --compressed "https://api.duffel.com/places/suggestions?lat=37.129665&lng=-8.669586&rad=100000"
                -H "Accept-Encoding: gzip"
                -H "Accept: application/json"
                -H "Duffel-Version: v1"
            })
        */
    }

    public function searchResults(Request $request){
        $iata_home = explode(" ", str_replace("(","",str_replace(")","",$request->from)))[0] ?? null;
        $iata_destination = explode(" ", str_replace("(","",str_replace(")","",$request->to)))[0] ?? null;

        $url = explode('flights/search',url()->full())[1] ?? "/flights/search/results";
        $path = str_replace('&amp;','&',$url) . "&iata_home=$iata_home&iata_destination=$iata_destination";
        $returnPath = str_replace('&amp;','&',$url) . "&iata_home=$iata_destination&iata_destination=$iata_home";

        $data = [
            "title" => "Flights from $iata_home to $iata_destination",
            "path" => $path,
            "returnPath" => $returnPath,
            "request" => $request->all(),
            "priorSearch" => Session::get('flight-search'),
            "iata_home" => $iata_home,
            "iata_destination" => $iata_destination,
            "trip_type" => $request->type
        ];

        // insert this query into the Firebase

        try{
            $firestore = app('firebase.firestore');
            $db = $firestore->database();
            $user = Session::get('user');
            $ip = \Request::ip();
            $location = Location::get($ip);
            $agent = $request->header('User-Agent');
            $region = "none";
            $timestamp = Carbon::now('UTC')->timestamp;

            if(isset($location) && !is_bool($location)){
                $region = $location->regionName;
            }


            $uid = null;

            if($user !== null){
                $uid = $user->uid;
            }

            // for each query, we will store a record for big data

            $search = $request->all();
            $queryID = generateRandomString(12);

            $search["session_id"] = $queryID;
            
            Session::put('flight-search', $search);
            $data["priorSearch"] = $search;

            $insertQuery = $db->collection("FlightSearches")->document($queryID)->set([
                "request" => $request->all(),
                "queryID" => $queryID,
                "region" => $region,
                "ip" => $ip,
                "uid" => $uid,
                "timestamp" => $timestamp,
                "agent" => $agent,
                "session_id" => $queryID
            ]);

        }
        catch(FirebaseException $e){

        }

        return view("flights.results", $data);
    }

    public function search(Request $request){
        // time to look for flights

        $headers = ['Application-Type:application/json','Accept-Encoding:gzip','Accept:application/json','Duffel-Version:v1','Content-Type:application/json',"Authorization:Bearer " . env('DUFFEL_API_KEY')];

        $cabin_class = strtolower(str_replace(" ","_",$request->class));
        $iata_home_original = explode(" ", str_replace("(","",str_replace(")","",$request->from)))[0] ?? null;
        $iata_home = $request->iata_home;
        $iata_destination = $request->iata_destination ?? null;

        $departure_date = $request->start;
        $departure_date = Carbon::createFromFormat('m/d/Y', $departure_date)->format('Y-m-d');
        

        $isReturn = false;
        if($iata_home_original != $iata_home){
            $isReturn = true;
        }

        $return_date = $request->end;
        if($request->type != "one-way"){
            $return_date = Carbon::createFromFormat('m/d/Y', $return_date)->format('Y-m-d');
        }
        

        $startFlight = [
            "origin" => "$iata_home", 
            "destination" => "$iata_destination", 
            "departure_date" => "$departure_date"
        ];

        $returnFlight = [
            "origin" => "$iata_destination", 
            "destination" => "$iata_home", 
            "departure_date" => "$return_date"
        ];

        $slices = [$startFlight];

        if($request->type == "round-trip"){
            if($isReturn){
                $startFlight = [
                    "origin" => "$iata_destination", 
                    "destination" => "$iata_home", 
                    "departure_date" => "$departure_date"
                ];

                $returnFlight = [
                    "origin" => "$iata_home", 
                    "destination" => "$iata_destination", 
                    "departure_date" => "$return_date"
                ];
            }
            $slices = [$startFlight, $returnFlight];
        }

        $passengers = [];

        for($adults = 0; $adults < $request->adults; $adults++){
            array_push($passengers, ["type" => "adult", "given_name" => "Ventii", "family_name" => "Adult-$adults"]);
        }
        for($children = 0; $children < $request->children; $children++){
            array_push($passengers, ["type" => "child", "given_name" => "Ventii", "family_name" => "Child-$children"]);
        }

        for($infant = 0; $infant < $request->infants; $infants++){
            array_push($passengers, ["type" => "infant_without_seat", "given_name" => "Ventii", "family_name" => "Infant-$infant"]);
        }

        $body = ["data" => [
            "slices" => $slices,    
            "passengers" => $passengers,
            "cabin_class" => $cabin_class
            ]
        ];

        if($request->type == "one-way"){
            $url = env("DUFFEL_API_URL") . 'air/offer_requests?return_offers=true';
        } else {
            // round trip is the only other supported option
            $url = env("DUFFEL_API_URL") . 'air/partial_offer_requests?return_offers=true';
            if($isReturn){

                $PARTIAL_OFFER_REQUEST_ID_FROM_OUTBOUND_SEARCH = $request->partial_id;
                $PARTIAL_OFFER_ID_FROM_OUTBOUND_SEARCH = $request->selected_partial_id;

                $url = env("DUFFEL_API_URL") . "air/partial_offer_requests/$PARTIAL_OFFER_REQUEST_ID_FROM_OUTBOUND_SEARCH?selected_partial_offer[]=$PARTIAL_OFFER_ID_FROM_OUTBOUND_SEARCH";
            }
        }

        $flights = $this->curlNewOffer($url,$headers, $body, $isReturn);

        $cards = [];

        // sort flights from cheapest total

        try{
            usort($flights, function($a, $b) {
                return $a['total_amount'] <=> $b['total_amount'];
            });
        }
        catch(Exception $e){

        }

        foreach($flights as $flight){

            $data = $flight;
            $data["iata_home"] = $iata_home;
            $data["iata_destination"] = $iata_destination;
            $data["class"] = $cabin_class;
            $data["trip_type"] = $request->type;
            $data["isReturn"] = $isReturn;

            if(isset($data["total_amount"])){
                $html = View::make('flights.components.flight-result', compact('data'))->render();
                array_push($cards, $html);
            }
        }

        if(empty($cards) || !$cards){
            // this search produced no results
            $searchSession = Session::get('flight-search');
            $firestore = app('firebase.firestore');
            $db = $firestore->database();
            $sessionQueryDoc = $db->collection("FlightSearches")->document($searchSession["session_id"]);
            $sessionQueryDoc->update([
                ["path" => "noResults","value" => true]
            ]);
        }

        return json_encode($cards);
    }

    public function updateSearchSelection(Request $request){
        $searchSession = Session::get('flight-search');
        $direction = $request->direction;
        $selectedAmount = $request->selectedAmount;
        $selectedOffer = $request->selectedOffer;
        $msrpd = $request->msrpd;

        if($direction && $selectedAmount && $selectedOffer){
            $firestore = app('firebase.firestore');
            $db = $firestore->database();
        }

        $sessionQueryDoc = $db->collection("FlightSearches")->document($searchSession["session_id"]);
        $sessionQuery = $sessionQueryDoc->snapshot()->data();

        $selections = $sessionQuery["selections"] ?? [];

        array_push($selections, [
            "offer" => $selectedOffer,
            "direction" => $direction,
            "amount" => $selectedAmount,
            "discountPresented" => $msrpd,
        ]);

        $sessionQueryDoc->update([
            ["path" => "selections","value" => $selections]
        ]);
    }

    public function curlNewOffer($url, $headers, $body, $isReturn){

        // A sample PHP Script to POST data using cURL

        $post_data = json_encode($body);

        // Prepare new cURL resource
        $crl = curl_init($url);
        curl_setopt($crl, CURLOPT_ENCODING , "gzip");
        curl_setopt($crl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($crl, CURLINFO_HEADER_OUT, true);
        if(!$isReturn){
            curl_setopt($crl, CURLOPT_POST, true);
            curl_setopt($crl, CURLOPT_POSTFIELDS, $post_data);
        } else {
            curl_setopt($crl, CURLOPT_CUSTOMREQUEST, "GET");
        }
        
        // Set HTTP Header for POST request 
        curl_setopt($crl, CURLOPT_HTTPHEADER, $headers);

        // Submit the POST request
        $result = curl_exec($crl);

        $result = json_decode($result, true) ?? [];

        if(!array_key_exists("data", $result)){
            // this query did not return any data for some reason.
            return $result;
        }

        $offers = $result["data"]["offers"] ?? [];

        $search_id = $result["data"]["id"];

        $results = [];
        $i = 0;
        $d = 0;

        $offerPreview = [];

            foreach($offers as $offer){

                // we are going to skip flights that require instant payment

                $payment_requirements = $offer["payment_requirements"] ?? false;

                if($payment_requirements){
                    if($payment_requirements["requires_instant_payment"] == true){
                        //continue;
                    }
                }
                else{
                    //continue;
                }

                if($i < 10){
                    array_push($offerPreview, $offer);
                    
                } else {

                }
                $legs = [];
                $slices = $offer["slices"];
                $totalDuration = 0; // minutes 
                $totalFlights = 0; // counting each slice




                foreach($slices as $slice){
                
                  if(!array_key_exists("segments",$slice)){

                    $totalFlights++;

                    $flightNumber = $slice["operating_carrier_flight_number"];

                    if(is_null($flightNumber)){
                      $flightNumber = $slice["marketing_carrier_flight_number"];
                    }

                    $durationMinutes = getDurationinMinutes($slice["duration"]);

                    $totalDuration += $durationMinutes;

                    $carrier = $slice["operating_carrier"] ?? [];

                    if(!$carrier){
                        $carrier = $slice["marketing_carrier"];
                    }

                    array_push($legs,
                        [
                            "destination" => $slice["destination"] ?? null,
                            "origin" => $slice["origin"] ?? null,
                            "duration" => $slice["duration"] ?? null,
                            "aircraft" => $slice["segments"]["aircraft"] ?? null,
                            "departing_at" => $slice["departing_at"],
                            "arriving_at" => $slice["arriving_at"],
                            "carrier" => $carrier,
                            "flight_number" =>  $flightNumber,
                            "passengers" => $slice["passengers"] ?? [],
                            "class" => $slice["fare_brand_name"] ?? false
                        ]
                    );

                  } else {

                    foreach($slice["segments"] as $segment){
                        // the flight is not direct.
                        $totalFlights++;
                        $flightNumber = $segment["operating_carrier_flight_number"];

                        if(is_null($flightNumber)){
                            $flightNumber = $segment["marketing_carrier_flight_number"];
                        }

                        $carrier = $segment["operating_carrier"] ?? [];

                        if(!$carrier){
                            $carrier = $segment["marketing_carrier"];
                        }

                        $durationMinutes = getDurationinMinutes($segment["duration"]);

                        $totalDuration += $durationMinutes;


                        array_push($legs,
                            [
                                "destination" => $segment["destination"] ?? null,
                                "origin" => $segment["origin"] ?? null,
                                "duration" => $segment["duration"] ?? null,
                                "aircraft" => $segment["segments"]["aircraft"] ?? null,
                                "departing_at" => $segment["departing_at"],
                                "arriving_at" => $segment["arriving_at"],
                                "carrier" => $carrier,
                                "flight_number" => $flightNumber,
                                "passengers" => $segment["passengers"] ?? [],
                                "class" => $slice["fare_brand_name"]  ?? false
                            ]
                      );
                    }
                  }   
                }

                array_push($results, [
                    "search_id" => $search_id,
                    "base_amount" => $offer["base_amount"],
                    "tax_amount" => $offer["tax_amount"],
                    "total_amount" => $offer["total_amount"],
                    "airline" => $offer["owner"],
                    "slices" => $legs,
                    "default_order" => $i,
                    "offer" => $offer["id"],
                    "partial" => $offer["partial"] ?? false,
                    "total_duration" => $totalDuration,
                    "total_flights" => $totalFlights,
                    "venti_offer_id" => $totalDuration . "-" . $offer["total_amount"] . "-" . $offer["owner"]["name"] . "-" . sizeof($legs)
                ]);
                
                $i++;
            }

            // remove duplicates by venti offer id

            $flights = $dupeFlights = [];

            foreach ($results AS $key => $line ) { 
                if ( !in_array($line['venti_offer_id'], $dupeFlights) ) { 
                    $dupeFlights[] = $line['venti_offer_id']; 
                    $flights[$key] = $line; 
                } 
            } 

        curl_close($crl);

        return $results;

    }


    public function offer($search = null, $offer, $return = null,$iata_destination,$type,$amount = null){

        // details page for a flight

        if(env('DUFFEL_BOOKABLE') != "enabled"){
            return Redirect::to('/boardingpass');
        }

        if($return == "direct"){
            return Redirect::to('/flights/search/offer/' . $offer . "/$iata_destination/checkout");
        }

        $data = $this->getOfferDetail($search,$offer,$return, $type, $amount);

        $data = [
            "title" => "Flight Offers for $iata_destination",
            "data" => $data,
            "priorSearch" => Session::get('flight-search') ?? [],
            "iata_destination" => $iata_destination
        ];

        // update the flight search session recorder with the selected offer

        $searchSession = Session::get('flight-search');

        if($searchSession !== null){
            $sessionID = $searchSession["session_id"] ?? null;
            if($sessionID){
                $firestore = app('firebase.firestore');
                $db = $firestore->database();
                $sessionQueryDoc = $db->collection("FlightSearches")->document($sessionID);
                $sessionQueryData = $sessionQueryDoc->snapshot()->data();

                $selections = $sessionQuery["preselectedOffers"] ?? [];

                array_push($selections, [
                    "offer" => $offer,
                ]);

                $sessionQueryDoc->update([
                    ["path" => "preselectedOffers","value" => $selections]
                ]);
            }
        }

        

        return view('flights.offer', $data);
    }

    public function getOfferDetailCheckout($offer){
        $url = env("DUFFEL_API_URL") . "air/offers/" . $offer . "?return_available_services=true";
        $headers = ['Application-Type:application/json','Accept-Encoding:gzip','Accept:application/json','Duffel-Version:v1','Content-Type:application/json',"Authorization:Bearer " . env('DUFFEL_API_KEY')];
        $crl = curl_init($url);
        curl_setopt($crl, CURLOPT_ENCODING , "gzip");
        curl_setopt($crl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($crl, CURLINFO_HEADER_OUT, true);

        // Set HTTP Header for GET request 
        curl_setopt($crl, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($crl, CURLOPT_HTTPHEADER, $headers);

        // Submit the POST request
        $result = curl_exec($crl);

        $result = json_decode($result, true);

        curl_close($crl);

        $data = [];
        $data = $result["data"] ?? false;

        return $data;
    }

    public function getOfferDetail($search_id, $offer, $return, $type,$amount){
        if($return == null || $return == "direct"){
            $url = env("DUFFEL_API_URL") . "air/offers/" . $offer . "?return_available_services=true";
        }
        else {
            $url =  env("DUFFEL_API_URL") . "air/partial_offer_requests/$search_id/fares?selected_partial_offer[]=$offer&selected_partial_offer[]=$return&offers=true";
        }

        $headers = ['Application-Type:application/json','Accept-Encoding:gzip','Accept:application/json','Duffel-Version:v1','Content-Type:application/json',"Authorization:Bearer " . env('DUFFEL_API_KEY')];
            
        // Prepare new cURL resource
        $crl = curl_init($url);
        curl_setopt($crl, CURLOPT_ENCODING , "gzip");
        curl_setopt($crl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($crl, CURLINFO_HEADER_OUT, true);

        // Set HTTP Header for GET request 
        curl_setopt($crl, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($crl, CURLOPT_HTTPHEADER, $headers);

        // Submit the POST request
        $result = curl_exec($crl);

        $result = json_decode($result, true);

        curl_close($crl);

        $data = [];
        $data = $result["data"] ?? false;



      if($return == null || $return == "direct"){

        $data["slices"][0]["total_amount"] = $data["total_amount"];
        $data["slices"][0]["tax_amount"] = $data["tax_amount"];
        $data["slices"][0]["base_amount"] = $data["base_amount"];
        $data["cabin_class"] = $type;
      } else {
        $upgradableOffers = [];

        foreach($data["offers"] as $offer){
            if((float) $offer["total_amount"] >= (float) $amount){
                array_push($upgradableOffers, $offer);
            }
        }
        $data["offers"] = $upgradableOffers;
      }

      return $data;
    }



    public function orderPreview(Request $request){

        // need to re-pull order data

        $data = $this->getOfferDetailCheckout($request->offer);

        if(!$data){
            return json_encode(500);
        }

        $user = Session::get('user');

        $dwolla = new Dwolla;
        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $auth = app('firebase.auth');

        // this is a good opportunity to update the search session in case they did add baggage and seat selection

        // we need to render the html that produces the checkout table

        $availableServices = $data["available_services"] ?? false;
        $baggies = [];

        foreach($availableServices as $availableService){
            if($availableService["type"] != "seat"){
                array_push($baggies, $availableService);
            }
        }



        $selectedSeats = $request->addedSeats;
        $selectedBags = $request->addedBags;

        $seatUpgrades = searchUpgrades($availableServices, $selectedSeats, 'seat');
        $bagUpgrades = searchUpgrades($availableServices, $selectedBags, 'baggage');

        $cabin_class = "economy";


        $data = [
            "offer" => $data,
            "priorSearch" => Session::get('flight-search') ?? [],
            "seatUpgrades" => $seatUpgrades,
            "bagUpgrades" => $bagUpgrades,
            "total_amount" => $data["total_amount"],
            "base_amount" => $data["base_amount"],
            "tax_amount" => $data["tax_amount"]
        ];

        $html = View::make('flights.components.offer-price-table', compact('data'))->render();
        
        return json_encode($html);

        
    }

    public function checkout($offer, $iata_destination){
        // details page for a flight
        if(env('DUFFEL_BOOKABLE') != "enabled"){
            return Redirect::to('/boardingpass');
        }
        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        
        $data = $this->getOfferDetailCheckout($offer);

        // first we need to get the passenger data for this flight into an array

        // we need to breakdown each slice by passenger

        // now that the round trip has been re-consolidated into a single offer, we have to redo the logic of presentation and looping through available services

        $data["offer"] = $offer;
        $data["baggable"] = [];

        if(!array_key_exists("slices", $data)){
            // the offer has expired
            return view('errors.411');
        }

        $originData = getTripOriginData($data["slices"]);

        $availableServices = $data["available_services"] ?? [];

        foreach($availableServices as $availableService){
            if($availableService["type"] != "seat"){
                // we only care about seats right now
                // get the 
                $serviceSegments = $availableService["segment_ids"];

                foreach($serviceSegments as $serviceSegment){
                    // we need to get the segment that matches id
                    $serviceSegment = getSegmentsByID($data['slices'], $serviceSegment);
                    $availableService["segment"] = $serviceSegment;
                    array_push($data["baggable"], $availableService);
                }
            }
        }


        $baggable = $dupeBaggable = [];

        foreach ($data["baggable"] AS $key => $line) { 
            if (!in_array($line['id'], $dupeBaggable) ) { 
                $dupeBaggable[] = $line['id']; 
                $baggable[$key] = $line; 
            } 
        } 

        $data["baggable"] = $baggable;

        // remove duplicate services in baggagle

        $data["iata_destination"] = $iata_destination;

        if($data == false){
            return view('errors.404');
        }

        $data = [
            "title" => "Checkout | Flight to $iata_destination",
            "data" => $data,
            "priorSearch" => Session::get('flight-search') ?? []
        ];

        $wallet = $customer = $transactable = false;
        $cards = [];
        $user = Session::get('user');      

        if($user !== null){
            $dwolla = new Dwolla;
            $auth = app('firebase.auth');

            $customer = $dwolla->getCustomer($user, $auth, $db);

            if($customer){
                // they have a customer record

                $fundingSources = [];// $dwolla->fundingSources($customer["customerID"]);

                if($fundingSources){
                    foreach($fundingSources as $fundingSource){
                        if(!$fundingSource->removed 
                            && isset($fundingSource->bankAccountType)
                            && $fundingSource->status == "verified"
                        ){
                            // this user can now make deposits and withdrawals using their bank account

                            $transactable = true;
                        }
                        if($fundingSource->type == "balance"){
                            $wallet = explode("funding-sources/",$fundingSource->_links->self->href)[1];
                        }
                    }

                    // account balances
                }
            }

            $cardDocs = $db->collection("Cards")
            ->where("uid", "=", $user->uid)
            ->where("deletedAt","=", null)
            ->documents();

            foreach($cardDocs as $card){
                $card = $card->data();
                array_push($cards, $card);
            }
        }

        $data["cards"] = $cards;
        $data["wallet"] = $wallet;
        $data["transactable"] = $transactable;
        $data["customer"] = $customer;

        // for seat maps

        // get the customer wallet if they are logged in

        // update the flight search session recorder with the selected offer

        $searchSession = Session::get('flight-search');

        if($searchSession !== null){
            $sessionID = $searchSession["session_id"] ?? null;
            if($sessionID){
                $sessionQueryDoc = $db->collection("FlightSearches")->document($sessionID);
                $sessionQueryData = $sessionQueryDoc->snapshot()->data();

                $selections = $sessionQuery["checkoutOffers"] ?? [];

                array_push($selections, [
                    "offer" => $offer,
                    "msrp" => $data["data"]["total_amount"],
                    "discountPresented" => getDiscountPercent("airfare", $data["data"]["total_amount"], "USD")
                ]);

                $sessionQueryDoc->update([
                    ["path" => "checkoutOffers","value" => $selections]
                ]);
            }
        }

        $data["loyalty_programme_accepted"] = isLoyaltyAvailable($data["data"]["owner"]["id"] ?? false);


        // get user credit cards


        return view('flights.checkout', $data);
    }

    public function seatMap(Request $request){

        // we should only allow this function to be called when the customer has identified all the passengers for the trip

        // first we need to get the passenger data for this flight into an array

        $offer = $this->getOfferDetailCheckout($request->offer);

        // we need to breakdown each slice by passenger

        $passengers = $offer["passengers"];


        $slicesByPassenger = $this->extractSlices($offer["slices"], $passengers);

        $maps = $this->getSeats($slicesByPassenger,$request->offer);

        $seats = [];

        $cabinIterator = 0;

        if($maps){
            foreach($maps as $map){

                $data = [
                    "map" => $map,
                    "passenger_id" => $map["passenger_id"],
                    "origin_iata" => $map["origin_iata"],
                    "destination_iata" => $map["destination_iata"],
                    "cabinIterator" => $cabinIterator
                ];

                $map["map"] = View::make('flights.components.seatmap', compact('data'))->render();

                array_push($seats, $map);
                $cabinIterator++;
            }
        }  

        return json_encode($seats); 
    }

    public function extractSlices($slices, $paxList){
        $passengerSchema = [];

        foreach($slices as $slice){
            $segments = $slice["segments"];

            foreach($segments as $segment){
                $passengers = $segment["passengers"];

                $p = 1;
                foreach($passengers as $passenger){
                    array_push($passengerSchema,[
                        "passenger_name" => getPassengerFirstNameById($paxList, $passenger["passenger_id"]),
                        "segment_id" => $segment["id"],
                        "passenger_id" => $passenger["passenger_id"],
                        "origin" => $segment["origin"]["city_name"] ?? $segment["origin"]["city"]["name"],
                        "origin_iata" => $segment["origin"]["city"]["iata_code"] ?? $segment["origin"]["iata_city_code"],
                        "destination" => $segment["destination"]["city_name"] ?? $segment["destination"]["city"]["name"],
                        "destination_iata" => $segment["destination"]["city"]["iata_code"] ?? $segment["destination"]["iata_city_code"],
                        "seat_id" => null
                    ]);
                    $p++;
                }
            }
        }       

        return $passengerSchema; 
    }

    public function getSeats($segments, $offer){
        $headers = ['Application-Type:application/json','Accept-Encoding:gzip','Accept:application/json','Duffel-Version:v1','Content-Type:application/json',"Authorization:Bearer " . env('DUFFEL_API_KEY')];

        $url ="https://api.duffel.com/air/seat_maps?offer_id=" . $offer;

        $crl = curl_init($url);
        curl_setopt($crl, CURLOPT_ENCODING , "gzip");
        curl_setopt($crl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($crl, CURLINFO_HEADER_OUT, true);

        // Set HTTP Header for GET request 
        curl_setopt($crl, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($crl, CURLOPT_HTTPHEADER, $headers);

        // Submit the POST request
        $result = curl_exec($crl);

        $maps = json_decode($result, true)["data"] ?? false; // if false, seat selection is not available
        $segs = [];

        if($maps){
            foreach($segments as $segment){
                $segment_id = $segment["segment_id"];

                $matchingSegments = findMatchingSegments($segment, $maps);
                array_push($segs, $matchingSegments);
            }
        }

        return $segs;
    }



    public function airportSync(){
        // Connect and authenticate with your Algolia app
        $client = SearchClient::create(env("ALGOLIA_APP_ID"),env("ALGOLIA_APP_KEY"));

        // Create a new index and add a record
        $index = $client->initIndex("Airports");

        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $auth = app('firebase.auth');

        $airports = $db->collection("Airport")->listDocuments();

        $i = 0;

        foreach($airports as $airport){
            if($i> 7000){
                $airport = $airport->snapshot()->data();
                $airport["objectID"] = $airport["id"];
                unset($airport["id"]);
                $index->saveObject($airport)->wait();
            }
            

            $i++;
        }

        // Search the index and print the results
       

        return var_dump($results["hits"]);
    }

    public function updateOfferPax(Request $request){

        $validator = Validator::make($request->all(), [
            'offer' => 'required|max:255',
            'pax_id' => 'required|max:255',
            'dob' => 'required|max:255'
        ]);

        $headers = ['Application-Type:application/json','Accept-Encoding:gzip','Accept:application/json','Duffel-Version:v1','Content-Type:application/json',"Authorization:Bearer " . env('DUFFEL_API_KEY')];

        $url = "https://api.duffel.com/air/offers/" . $request->offer . "/passengers/" . $request->pax_id;


        $identity_documents = [];

        if(isset($request->document_type)){
            $identity_documents = [[
              [
                "unique_identifier" => $request->passport_number,
                "type" => $request->document_type,
                "issuing_country_code" => $request->passport_country,
                "expires_on" => $request->passport_expiration
              ]
            ]];
        }

        $rewards = [
            [
                "airline_iata_code" => $request->rewards_iata,
                "account_number" => $request->rewards ?? "venti"
            ]
        ];

        $age = \Carbon\Carbon::parse($request->dob)->age;

        $data = [
                    "given_name" => $request->first_name,
                    "family_name" => $request->last_name,
                    "title" => $request->title,
                    "phone_number" => $request->phone,
                    "identity_documents" => $identity_documents,
                    "gender" => $request->gender,
                    "email" => $request->email,
                    "age" => $age  
                ];

        $data["loyalty_programme_accounts"] = $rewards;

        if($request->rewards == "" || $request->rewards == "undefined" || $request->rewards == null){
            // the user is saving PAX without rewards data
            Session::put($request->pax_id, $data);
            return json_encode(200);
        }

        $post_data = [
            "data" => $data
        ];

        $crl = curl_init($url);
        curl_setopt($crl, CURLOPT_ENCODING , "gzip");
        curl_setopt($crl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($crl, CURLINFO_HEADER_OUT, true);

        curl_setopt($crl, CURLOPT_CUSTOMREQUEST, 'PATCH');
        curl_setopt($crl, CURLOPT_POSTFIELDS, json_encode($post_data));
        curl_setopt($crl, CURLOPT_HTTPHEADER, $headers);

        $headers = [];

        curl_setopt($crl, CURLOPT_HEADERFUNCTION,
          function($curl, $header) use (&$headers)
          {
            $len = strlen($header);
            $header = explode(':', $header, 2);
            if (count($header) < 2) // ignore invalid headers
              return $len;

            $headers[strtolower(trim($header[0]))][] = trim($header[1]);
            
            return $len;
          }
        );

        $data = curl_exec($crl);

        $requestID = $headers["x-request-id"] ?? [0 => ""];
        $requestID = $requestID[0];

        // Submit the POST request
        $output = json_decode($data, true);
        $result = $output["data"] ?? false;

        // we need to get the

        if($result){
            Session::put($request->pax_id, $data);
            return json_encode(200);
        }

        $messageOne = $output["errors"][0]["message"] ?? "Offer rescinded by airline.";
        $messageTwo = $output["errors"][1]["message"] ?? "";

        return json_encode([
                "error" => 500,
                "message" => "Request ID: $requestID <br> $messageOne <br> $messageTwo"
            ]);
    }

    public function submitOrder(Request $request){

        // * VALIDATION * //

        $user = Session::get('user');
        $destination = $request->destination ?? "";
        $paymentPref = $request->paymentPref;

        if($user === null){
            // session expired
            return json_encode(400);
        }

        $validator = Validator::make($request->all(), [
            'offer' => 'required|max:255',
            'pax' => 'required|max:255'
        ]);

        if ($validator->fails()) {
            // required data is missing
            $error = [
                "error" => 401,
                "message" => "We are missing critical information needed to process this order."
            ];

            return json_encode($error);
        }

        $offer = $this->getOfferDetailCheckout($request->offer);

        if(!array_key_exists("slices", $offer)){
            // the offer has expired
            $error = [
                "error" => 402,
                "message" => "Unfortunately, this offer has expired. This happens when you did not press the book button in time, or the airline rescinded this order. Try refreshing the page. If that does not work, please rebuild your search."
            ];

            return json_encode($error);
        }

        /*
            we need to validate the added services if there are any
        
        */

        $availableServices = $offer["available_services"] ?? false;
        $baggies = [];


        foreach($availableServices as $availableService){
            if($availableService["type"] != "seat"){
                array_push($baggies, $availableService);
            }
        }

        $selectedSeats = $request->addedSeats;
        $selectedBags = $request->addedBags;

        $seatUpgrades = searchUpgrades($availableServices, $selectedSeats, 'seat');
        $bagUpgrades = searchUpgrades($availableServices, $selectedBags, 'baggage');

        $addedServices = [];
        $addOnsCost = 0;
        $addOnsDuffel = 0;

        foreach($bagUpgrades as $bagUpgrade){
            array_push($addedServices,[
                "quantity" => 1,
                "id" => $bagUpgrade["id"]
            ]);
            $addOnsCost += $bagUpgrade["newPrice"];
            $addOnsDuffel += $bagUpgrade["total_amount"];
        }
        foreach($seatUpgrades as $seatUpgrade){
            array_push($addedServices,[
                "quantity" => 1,
                "id" => $seatUpgrade["id"]
            ]);
            $addOnsCost += $seatUpgrade["newPrice"];
            $addOnsDuffel += $seatUpgrade["total_amount"];
        }

        $pointsApplied = (float) $request->points ?? 0;

        // we need to get the wallet of the user to check their balance

        $dwolla = new Dwolla;
        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $auth = app('firebase.auth');
        $wallet = $customer = $transactable = $bookingSucceeded = $intent = false;

        $customer = $dwolla->getCustomer($user, $auth, $db);
        $customerWallet = "";

        if($customer){
            // they have a customer record

            // we need to make sure they are not suspended

            if($customer["status"] == "suspended" || $customer["status"] == "deactivated"){
                $error = [
                    "error" => 403,
                    "message" => "Suspended and deactivated accounts cannot place orders. You can submit an appeal by emailing admin@venti.co"
                ];

                return json_encode($error);
            }

            $fundingSources = $dwolla->fundingSources($customer["customerID"]);

            if($fundingSources){
                foreach($fundingSources as $fundingSource){
                    if(!$fundingSource->removed 
                        && isset($fundingSource->bankAccountType)
                        && $fundingSource->status == "verified"
                    ){
                        // this user can now make deposits and withdrawals using their bank account

                        $transactable = true;
                    }
                    if($fundingSource->type == "balance"){
                        $wallet = $fundingSource->_links->self->href;
                        $customerWallet = $fundingSource->_links->self->href;
                    }
                }
            }
        } else {
            // customer record does not exist. ABORT!
            // the offer has expired
            $error = [
                "error" => 403,
                "message" => "This order can only be processed by active and verified Boarding Pass holders. Please contact admin@venti.co if you believe this is an error."
            ];

            return json_encode($error);
        }

        // we need to make sure there's enough funds to cover the cost of the purchase and the points being requested 

        $wallet = $dwolla->getFundingSourceBalance($wallet);
        $wallet["balance"] = (float) $wallet->balance->value;

        // the amount the customer is asked to pay with addons

        $total = $offer["total_amount"] + $addOnsCost;

        $pax_docs_required = $offer["passenger_identity_documents_required"] ?? false;

        $cfar = $request->cfar;
        $cfarPrice = 0;

        if($cfar == "true"){
            $cfar = true;
            // protection was added, so we bump the price up by 30%
            $cfarPrice = round($total * 0.3,2);
            $total = $total + $cfarPrice;
        } else {
            $cfar = false;
        }

        // give this lucky person a discount

        $maxDiscount = getDiscountPercent("airfare", $total, "USD");
        $lowestPossible = $total * $maxDiscount;
        $maxPoints = round($total - $lowestPossible,2);


        // make sure the amount of points provided is not greater than was is computationally allowed

        $transactions = [];

        $transactionDocs = $db->collection("Transactions");
        $transactionDocs = $transactionDocs
            ->where("uid","=",$user->uid)
            ->documents();

        foreach($transactionDocs as $transactionDoc){
            array_push($transactions,  $transactionDoc->data());
        }

        // 
        $points = getPointsBalance($transactions);

        if($points < $pointsApplied){
            // more rounding issues
            if(($points - $pointsApplied) < 0.01){

                $pointsApplied = bcdiv((float) $points, 1, 2);

            }
        }



        if($points < $pointsApplied){
            // the user is trying to apply more points than they actually have
            $error = [
                "error" => 405,
                "message" => "Insufficient Points on your Boarding Pass. Please insert a lesser amount and try again."
            ];

            return json_encode($error);
        }

        if($pointsApplied > $maxPoints){
            // the user is trying to apply more points than what is allowed for this transaction

            $error = [
                "error" => 406,
                "message" => "You've exceeded the maximum amount of Boarding Pass points allowed for this order. Please insert a lesser amount and try again."
            ];

            return json_encode($error);
        }


        $amountDue = $total - $pointsApplied;

        if($amountDue > $wallet["balance"] && $paymentPref == "pass"){
            // the user is trying to spend more Cash than what is in their cash balance
            $error = [
                "error" => 407,
                "message" => "The cost of this order exceeds the Cash Balance of your Boarding Pass. Please deposit more funds. Otherwise, shop for a more affordable flight and try again."
            ];

            return json_encode($error);
        }


        // ** BY THIS POINT IT IS SAFE TO PROCEED WITH THE TRANSACTION ** //

        $passengers = $request->pax;

        $paxData = [];

        foreach($passengers as $passenger){
            // we have to update the passengers associated with this order since we've only inserted dummy data

            $dob = \Carbon\Carbon::parse($passenger["dob"])->format("Y-m-d"); // 1987-07-24

            $identity_documents = [];

            if($pax_docs_required){
                $identity_documents = [
                    [
                      [
                        "unique_identifier" => $passenger["passport_number"],
                        "type" => $passenger["document_type"],
                        "issuing_country_code" => $passenger["passport_country"],
                        "expires_on" => $passenger["passport_expiration"]
                      ]
                    ]
                ];

                if($passenger["ktn"] != "undefined" && $passenger["ktn"] != ""){
                    $identity_documents = [
                        [
                          [
                            "unique_identifier" => $passenger["passport_number"],
                            "type" => $passenger["document_type"],
                            "issuing_country_code" => $passenger["passport_country"],
                            "expires_on" => $passenger["passport_expiration"]
                          ],
                          [
                            "type" => "known_traveler_number",
                            "unique_identifier" => $passenger["ktn"],
                            "issuing_country_code" => $passenger["citizenship"]
                          ]
                        ]
                    ];
                }
            } else {
                if($passenger["ktn"] != "undefined" && $passenger["ktn"] != ""){
                    $identity_documents = [
                        [
                                "type" => "known_traveler_number",
                                "unique_identifier" => $passenger["ktn"],
                                "issuing_country_code" => $passenger["citizenship"]
                        ]
                    ];
                }
            }
            
            array_push($paxData,[
                "title" => $passenger["title"],
                "phone_number" => $passenger["phone"],
                "identity_documents" => $identity_documents,
                "id" => $passenger["id"],
                "given_name" => $passenger["first_name"],
                "gender" => $passenger["gender"],
                "family_name" => $passenger["last_name"],
                "email" => $passenger["email"],
                "born_on" => $dob
            ]);
        }

        $post_data = [
            "data" => [
                "type" => "instant", // "hold"
                "selected_offers" => [
                  $request->offer
                ],

                // we are not paying instantly. We need customer funds to clear first. 
                "payments" => [
                  [
                    "type" => "balance",
                    "currency" => "USD",
                    "amount" => "" . $offer["total_amount"] + $addOnsDuffel . ""
                  ]
                ],

                "passengers" => $paxData
            ]
        ];

        if($addOnsCost > 0){
            $post_data["data"]["services"] = $addedServices;
        }

        $ccFee = 0;
        $from = "My Boarding Pass";


        if($amountDue > 0.01 && $paymentPref != "pass"){
            // the customer is paying with credit card
            $card = $db->collection("Cards")->document($paymentPref)->snapshot()->data();
            $ccFee = bcdiv($amountDue * .03, 1, 2);
            $amountDue += $ccFee; // adding credit card fee
            // create payment intent
            $owner = $offer["owner"]["name"] ?? "Venti Flight";
            $description = "$owner";
            $metadata = [
                'description' => "Flight Purchase " . $description,
                'mcc' => '4511', // Airlines MCC
            ];
            $intent = \Stripe::createPaymentIntent($amountDue * 100, $paymentPref, $card["customerID"], $description, $metadata);

            if($intent && isset($intent->id)){
                $transferID = $transfer = $intent->id;
                $nickname = $card["cardNickname"] ?? false;
                if(!$nickname){
                    $from = $card["cardBrand"] . " Card (". $card["cardLast4"] .")" ;
                }else{
                    $from = $nickname . ": " . $card["cardBrand"] . " (". $card["cardLast4"] .")" ;
                }

            }
            else {
                $error = [
                    "error" => 407,
                    "message" => "We are unable to use this card to process your transaction. " . $intent . " If you believe this was an error, you can contact your bank. Otherwise, please try a different payment method."
                ];

                return json_encode($error);
            }
        }


        $headers = ['Application-Type:application/json','Accept-Encoding:gzip','Accept:application/json','Duffel-Version:v1','Content-Type:application/json',"Authorization:Bearer " . env('DUFFEL_API_KEY')];

        $url ="https://api.duffel.com/air/orders";

        $crl = curl_init($url);
        curl_setopt($crl, CURLOPT_ENCODING , "gzip");
        curl_setopt($crl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($crl, CURLINFO_HEADER_OUT, true);

        curl_setopt($crl, CURLOPT_POST, true);
        curl_setopt($crl, CURLOPT_POSTFIELDS, json_encode($post_data));
        curl_setopt($crl, CURLOPT_HTTPHEADER, $headers);

        $headers = [];

        curl_setopt($crl, CURLOPT_HEADERFUNCTION,
          function($curl, $header) use (&$headers)
          {
            $len = strlen($header);
            $header = explode(':', $header, 2);
            if (count($header) < 2) // ignore invalid headers
              return $len;

            $headers[strtolower(trim($header[0]))][] = trim($header[1]);
            
            return $len;
          }
        );

        $requestID = $headers["x-request-id"] ?? [0 => ""];
        $requestID = $requestID[0];


        // Submit the POST request
        $result = curl_exec($crl);

        $resultRaw = json_decode($result,true);
        $result = json_decode($result,true);

        if(array_key_exists("data",$result)){
            $result = $result["data"];
            $bookingSucceeded = true;
            // ** BY THIS POINT THE FLIGHT HAS BEEN BOOKED AND HELD FOR PAYMENT ** //

            // the order is done, time to record into firebase and send email

            // add to Firebase Tables

            // we record the withdraw

            // this is tricky because we have to check to make sure the offer has not expired taking funds. So checking wallet balances is critical.

            $amountDue = round($amountDue,2); // dwolla will not allow more than two decimal places
            $date = \Carbon\Carbon::now('UTC')->format("Y-m-d H:i:s");
            $timestamp = \Carbon\Carbon::now('UTC')->timestamp; // UTC
            $owner = $result["owner"]["name"];
            $refID = $result["id"];
            $booking_reference = $result["booking_reference"];

            if($amountDue > 0.01){
                if($paymentPref == "pass"){
                    $transfer = $dwolla->transferFromUserWalletToVentiWallet($user, $customerWallet, $amountDue, "flight-purchase");

                    if(!$transfer){
                        // the transfer was not successful. This could be due to the customer account being frozen.

                        $error = [
                            "error" => 408,
                            "message" => "Our ACH payments provider, Dwolla, blocked this transaction. It may be due for a variety of reasons. Please contact admin@venti.co."
                        ];

                        return json_encode($error);
                    }

                    // ** BY THIS POINT THE CUSTOMER HAS PAID CASH FOR THE FLIGHT ** //

                    $transferID = explode("transfers/",$transfer)[1];
                }
                
                $transactionID = generateRandomString(12);
                

                // all the dates will be the same to guarantee they are properly included in the same statement
                
                

                $data = [
                    "transactionID" => $transactionID,
                    "uid" => $user->uid,
                    "type" => "withdraw",
                    "timestamp" => $timestamp,
                    "date" => $date,
                    "amount" => $amountDue,
                    "speed" => "next-available",
                    "fee" => $ccFee,
                    "status" => "complete",
                    "from" => $from,
                    "to" => "Venti",
                    "total" => $amountDue,
                    "note" => "Flight Purchase $owner | REF# $booking_reference",
                    "transferID" => $transferID,
                    "transactionUrl" => $transfer,
                    "refID" => $refID,
                    "paymentPref" => $paymentPref,
                    "env" => env('APP_ENV')
                ];

                $purchase = $db->collection("Transactions")->document($transferID)->set($data);

            }

            if($pointsApplied > 0){

                // points were used to complete this purchase

                $transactionID2 = generateRandomString(12);

                $data = [
                    "transactionID" => $transactionID2,
                    "uid" => $user->uid,
                    "type" => "points-withdraw",
                    "timestamp" => $timestamp,
                    "date" => $date,
                    "amount" => $pointsApplied,
                    "speed" => "next-available",
                    "fee" => 0,
                    "status" => "complete",
                    "from" => "My Boarding Pass",
                    "to" => "Venti",
                    "total" => $pointsApplied,
                    "note" => "Flight Purchase $owner | REF# $booking_reference",
                    "transferID" => $transferID ?? null, // it's wise to link this with the previous transfer
                    "transactionUrl" => null,
                    "env" => env('APP_ENV'),
                    "refID" => $refID,
                    "paymentPref" => $paymentPref,
                    "taxable" => true // points are taxable when converted to cash equivalent value
                ];

                $pointsDeduction = $db->collection("Transactions")->document($transactionID2)->set($data);
            }

            // RECORD THE ORDER //

            $searchSession = Session::get('flight-search');
            $sessionID = null;

            if($searchSession !== null){
                $sessionID = $searchSession["session_id"];
            }

            $data = [
                "transactionID" => $transactionID ?? $transactionID2, // this connects the order is 2 if the flight is free
                "uid" => $user->uid,
                "timestamp" => $timestamp,
                "date" => $date,
                "type" => "flight",
                "airlock" => $cfar,
                "airlockPrice" => $cfarPrice,
                "amount" => $amountDue,
                "points" => $pointsApplied,
                "order" => json_encode(["data"=>$result]),
                "passengers" => sizeof($paxData),
                "booking_reference" => $booking_reference,
                "refID" => $refID, // we will need this value to query duffel
                "sessionID" => $sessionID, // tracking which session led to this booking
                "seatUpgrades" => $seatUpgrades,
                "bagUpgrades" => $bagUpgrades,
                "from" => $from
            ];

            $saveOrder = $db->collection("Orders")->document($refID)->set($data);

            // SEND THE CONFIRMATION EMAIL.

            $email = $user->email;
            $subject = "[Boarding Pass] Booking $booking_reference Confirmed with $owner";

            $timezone = Session::get('user')->customClaims["timezone"] ?? "America/New_York";

            $data = [
                "airlock" => $cfar,
                "airlockPrice" => $cfarPrice,
                "user" => $user,
                "paidAmount" => $amountDue,
                "pointsApplied" => $pointsApplied,
                "order" => $result,
                "transactionID" => $transactionID ?? $transactionID2,
                "passengers" => $paxData,
                "booking_reference" => $result["booking_reference"],
                "timestamp" => $timestamp,
                "date" => $date,
                "timezone" => $timezone,
                "iata_destination" => $destination,
                "destination" => $destination,
                "refID" => $refID,
                "seatUpgrades" => $seatUpgrades,
                "bagUpgrades" => $bagUpgrades,
                "base_amount" => $offer["base_amount"],
                "tax_amount" => $offer["tax_amount"],
                "from" => $from
            ];
            
            try{
                Mail::send('emails.transactions.flight-purchase', array("data" => $data), function($message) use($email, $subject)
                    {
                        $message
                            ->to($email)
                            ->from("no-reply@venti.co","Venti")
                            ->replyTo("no-reply@venti.co")
                            ->bcc("admin@venti.co")
                            ->subject($subject);
                });
            }
            catch(Exception $e){
                // the email was not fired.
                
            }

            $success = [
                "success" => 200,
                "message" => "Success! Your booking with $owner has been submitted for processing. We've also sent a confirmation email to $email. This page will automatically redirect in 10 seconds.",
                "order_id" => $refID
            ];

            return json_encode($success); // simulating failure
        }

        $errorMessageString = "Unfortunately, the airline refused this order. This can happen for a variety of reasons. No transaction has taken place. You can retry this order or contact support by including your origin, destination, travel dates, passengers, and this Request ID: $requestID";

        if(array_key_exists("errors", $result)){
            foreach($result["errors"] as $error){
                $errorMessageString = $errorMessageString . " " . $error["message"] ?? "";
            }
        }

        $errorMessageString = $errorMessageString . "<br>. You can also send a screenshot of this error code to support via admin@venti.co: " . json_encode($result);

        $error = [
                "error" => 410,
                "message" => $errorMessageString
        ];

        if($intent && !$bookingSucceeded){
            // refunding the charge due to failed booking
            $refund = \Stripe::refundPayment($intent);

            if($refund){
                // successfully reversed the charge
                $errorMessageString = $errorMessageString . "<br>. We reversed the transaction on your card. Please contact admin@venti.co for support with this booking before trying again.";
                $error = [
                        "error" => 410,
                        "message" => $errorMessageString
                ];
            } else {
                $errorMessageString = $errorMessageString . "<br>. We were unable to reverse the transaction with your card. Please contact admin@venti.co for support with this booking.";
                $error = [
                        "error" => 410,
                        "message" => $errorMessageString
                ];
            }
        }

        return json_encode($error);
    }

    public function updatePassenger($pax){

    }

    public function offerStatus(Request $request){
        // we need to check the offer every X seconds to see if the deal expired while the customer was shopping

        $offer = $this->getOfferDetailCheckout($request->offer);

        if(!$offer){
            // the offer expired
            return json_encode(500);
        }

        return json_encode($offer["expires_at"]);
    }

    public function orderStatus(Request $request){
        // check to make sure the order exists

        $user = Session::get('user');

        if(!$request->orderID){
            
        }

        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $auth = app('firebase.auth');

        $order = $db->collection("Orders")->document($request->orderID)->snapshot()->data();

        // check permission of user to get details about it

        if(!$order["uid"] == $user->uid){
            // permission denied
            return json_encode(501);
        }



        // query duffel to get the latest data about the order

        $headers = ['Application-Type:application/json','Accept-Encoding:gzip','Accept:application/json','Duffel-Version:v1','Content-Type:application/json',"Authorization:Bearer " . env('DUFFEL_API_KEY')];

        $url ="https://api.duffel.com/air/orders/" . $request->orderID;

        $crl = curl_init($url);
        curl_setopt($crl, CURLOPT_ENCODING , "gzip");
        curl_setopt($crl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($crl, CURLINFO_HEADER_OUT, true);
        curl_setopt($crl, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($crl, CURLOPT_HTTPHEADER, $headers);

        // Submit the POST request
        $result = curl_exec($crl);

        $result = json_decode($result,true)["data"] ?? false;

        if(!$result){
            // there was an error pulling the data from duffel

            return json_encode(502);
        }

        $canceledOn = $order["canceledOn"] ?? false;

        if($request["cancelled_at"] != null || $canceledOn ){
            return json_encode(202); // ordered canceled
        }

        return json_encode(200);

        // report the status back to the user
    }
}