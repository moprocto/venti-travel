<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Carbon\Carbon as Carbon;
use Session;
use Plaid;
use DateTime;


class ProController extends Controller
{

    public function __construct(Plaid $plaidService)
    {
        $this->plaidService = $plaidService;
        $this->middleware('firebase.auth');
    }

    public function index(){

        // this is exclusive to Pro members

        /**
         * Create a new controller instance.
         *
         * @return void
         */

        

        /**
         * Show the application dashboard.
         *
         * @return \Illuminate\Contracts\Support\Renderable
         */

        $user = Session::get('user');

        $transactable = [];
        $transactable = false;
        $points = 0;

        $fundingSources = $this->getBanks(new Request(["user" => $user->uid])) ?? [];
        $cards = $this->getCards(new Request(["user" => $user->uid])) ?? [];

        $completedProSteps = $this->getCompletedProSteps($user, $fundingSources, $cards);

        $data = [
            "title" => "Boarding Pass",
            "description" => "Traveling for free just got whole lot easier.",
            "image" => "https://venti.co/assets/img/boarding-pass-social.jpg",
            "url" => "https://venti.co/boardingpass",
            "user" => $user,
            "transactable" => $transactable,
            "transactions" => [],
            "points" => 0,
            "fundingSources" => $fundingSources ?? [],
            "subscription" => "pro",
            "ranking" => "X",
            "upgrade" => "false",
            "cards" => $cards ?? [],
            "completedProSteps" => $completedProSteps
        ];

        return view("pro.index", $data);
    }

    public function getBanks(Request $request) {

        $result = Http::withHeaders([
            'x-api-key' => 'h3z5uLO9xJxK7Ae6Flq8uYqPIqaPAHuOEuoW4v4r601R9Ez7',
            'uid' => $request->user
        ])
        ->post(env("VENTI_API_URL") . "pro/list",[
        ]);

        $result = $result->body();
        
        return json_decode($result, true);
    }

    public function getCards(Request $request){
        $result = Http::withHeaders([
            'x-api-key' => 'h3z5uLO9xJxK7Ae6Flq8uYqPIqaPAHuOEuoW4v4r601R9Ez7',
            'uid' => $request->user
        ])
        ->post(env("VENTI_API_URL") . "boardingpass/customer/fundingSource/card/list",[
        ]);

        $result = $result->body();
        
        return json_decode($result, true);
    }

    public function getCompletedProSteps($user, $fundingSources, $cards){
        $progress = [];

        $progress["card"] = (sizeof($cards) > 0) ? true : false;
        $progress["funding"] = $progress["transactions"] = $progress["complete"] = $progress["checking"] = false;

        $approved_ids = ["ins_15","ins_14"];

        foreach($fundingSources as $fundingSource){
            $accounts = $fundingSource["accounts"] ?? [];

            foreach($accounts as $account){
                if(
                    in_array($fundingSource["institution_id"], $approved_ids) &&
                    $account["subtype"] == "savings" && 
                    $account["type"] == "depository"
                ){
                    $progress["funding"] = true;
                }

                if(
                    in_array($fundingSource["institution_id"], $approved_ids) &&
                    $account["subtype"] == "checking" && 
                    $account["type"] == "depository"
                ){
                    $progress["checking"] = true;
                }

                $transactions = $account["transactions"] ?? false;

                if($transactions){
                    if(sizeof($transactions) > 0){
                        $progress["transactions"] = true;
                    }
                }
            }
        }

        if($progress["funding"] == true && $progress["transactions"] == true){
            $progress["complete"] = true;
        }

        return $progress;
    }

    public function getAccount($vid){
        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $auth = app('firebase.auth');
        $user = Session::get('user');

        $result = Http::withHeaders([
            'x-api-key' => 'h3z5uLO9xJxK7Ae6Flq8uYqPIqaPAHuOEuoW4v4r601R9Ez7',
            'uid' => $user->uid
        ])
        ->post(env("VENTI_API_URL") . "pro/account",[
            "vid" => $vid
        ]);

        $bank = json_decode($result->body(), true);

        if(!$bank || !isset($bank) || $bank["uid"] != $user->uid){
            return view('errors.404');
        }

        $plaidAccounts = Http::withHeaders([
            'x-api-key' => 'h3z5uLO9xJxK7Ae6Flq8uYqPIqaPAHuOEuoW4v4r601R9Ez7',
            'uid' => $user->uid
        ])
        ->post(env("VENTI_API_URL") . "pro/bank/subaccounts",[
            "access_token" => $bank["access_token"]
        ]);

        $accounts = json_decode($plaidAccounts->body()) ?? [];

        $data = [
            "title" => "My " . $bank["institution"],
            "bank" => $bank,
            "accounts" => $accounts
        ];

        return view("pro.account", $data);
    }

    public function findAccountById($accounts, $id) {
        foreach ($accounts as $account) {
            if ($account["account_id"] === $id) {
                return $account;
            }
        }
        return null; // Return null if no account with the given ID is found
    }

    public function deleteSubaccount(Request $request){
        $user = Session::get('user');
        $result = Http::withHeaders([
            'x-api-key' => 'h3z5uLO9xJxK7Ae6Flq8uYqPIqaPAHuOEuoW4v4r601R9Ez7',
            'uid' => $user->uid
        ])
        ->post(env("VENTI_API_URL") . "pro/subaccount/remove",[
            "vid" => $request->vid
        ]);

        // returns item_id

        $accessToken = json_decode($result->body());

        dd($accessToken);
    }

    public function deleteBank(Request $request){
        $user = Session::get('user');

        $result = Http::withHeaders([
            'x-api-key' => 'h3z5uLO9xJxK7Ae6Flq8uYqPIqaPAHuOEuoW4v4r601R9Ez7',
            'uid' => $user->uid
        ])
        ->post(env("VENTI_API_URL") . "pro/account/remove",[
            "vid" => $request->vid
        ]);

        // returns item_id

        $accessToken = json_decode($result->body());

        return json_encode(200);
    }

    public function removeItem($accessToken)
    {
        $user = Session::get('user');
        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $auth = app('firebase.auth');

        // we need to get the item info from the DB

        $this->plaidService->removeItem($accessToken);
    }
}
