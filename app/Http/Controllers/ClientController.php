<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use Kreait\Firebase\Database;
use Kreait\Firebase\Auth;
use Google\Cloud\Firestore\FirestoreClient;
use Kreait\Firebase\Exception\FirebaseException;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Illuminate\Support\Facades\Http;

use Redirect;

class ClientController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
    */

    public function index(){
        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $auth = app('firebase.auth');
        $clients = $db->collection("Clients")->listDocuments();
        $clientList = [];

        foreach($clients as $client){
            array_push($clientList, $client->snapshot()->data());
        }

        $data = [
            "clients" => $clientList
        ];

        return view("dashboard.clients.index", $data);
    }

    public function add(){
        return view("dashboard.clients.add");
    }

    public function read($client){

        if(Auth::user()->client_id != $client && Auth::user()->admin == 0){
            return Redirect::back();
        }


        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $auth = app('firebase.auth');
        $client = $db->collection("Clients")->document($client)->snapshot()->data();
        

        $data = [
            "client" => $client
        ];

        return view("dashboard.clients.read", $data);
    }


    // CRUD POSTS

    public function create(Request $request){
        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $auth = app('firebase.auth');

        $id = str_replace(' ', '', $request->name);
        $id = strtolower($id);

        $docRef = $db->collection("Clients")->document($id)->set(
            [             
                "id" => $id,
                "name" => $request->name,
                "email" => $request->email,
                "bio" => $request->bio,
                "avatar" => $request->avatar,
                "expertise" => $request->expertise,
                "website" => $request->website,
                "paymentMethod" => $request->paymentMethod,
                "paymentHandle" => $request->paymentHandle
            ]
        );

        return Redirect::to('/dashboard/clients/read/' . $id);
    }

    public function update(){
        if(Auth::user()->client_id != $client && Auth::user()->admin == 0){
            return Redirect::back();
        }

        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $auth = app('firebase.auth');

        $docRef = $db->collection("Clients")->document($id)->set(
            [             
                "id" => $id,
                "name" => $request->name,
                "email" => $request->email,
                "bio" => $request->bio,
                "avatar" => $request->avatar,
                "expertise" => $request->expertise,
                "website" => $request->website,
                "paymentMethod" => $request->paymentMethod,
                "paymentHandle" => $request->paymentHandle
            ]
        );
    }
}
