<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Newsletter;
use \DrewM\MailChimp\MailChimp;
use Kreait\Firebase\Exception\FirebaseException;
use Kreait\Firebase\Contract\Messaging;
use Kreait\Firebase\Messaging\CloudMessage;
use Kreait\Firebase\Messaging\Notification as Notification;
use Kreait\Firebase\Messaging\ApnsConfig as ApnsConfig;
use Redirect;
use Session;
use App\Models\Group as Groups;
use App\Models\Claim as Claims;
use App\Models\Member as Members;
use App\Models\Interest as Interests;
use App\Models\Language as Languages;
use App\Models\Setting as Settings;
use App\Models\Recommendation as Recommendations;
use App\Models\Wishlist as Wishlists;
use Mail;
use Http;

class AdminControllerLegacy extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index($focus = "home")
    {
        if($focus == "groups"){
            return $this->groups();
        }

        $auth = app('firebase.auth');
        $firestore = app('firebase.firestore');

        $groups = Groups::where('uid','!=', "mnTmABt3MDOWyy76pwG7V0QACpD2")->get();

        $usersList = [];

        // we should pull this from the database

        $users = Members::all();
        
        $completed = $emailVerified = $smsVerified = $hasPhoto = $facebookCount = $recommendable = $appleCount = $emailCount = $expired = 0;

        foreach($users as $user){

            if($user->emailVerified == 1){
                $emailVerified++;
            }
            if($user->isComplete()){
                $completed++;
            }
            if($user->source == "apple.com"){
                $appleCount++;
            }
            if($user->source == "password" || $user->source == "email"){
                $emailCount++;
            }
            if($user->source == "facebook.com" || str_contains($user->source, "facebook") || strlen($user->uid) <= 20){
                $facebookCount++;
            }
            if($user->customClaims["smsVerified"] == "Y"){
                $smsVerified++;
            }
            if($user->hasPhoto()){
                $hasPhoto++;
            }
            if($user->isRecommendable()){
                $recommendable++;
            }
        }

        $recommendations = Groups::where('uid', "mnTmABt3MDOWyy76pwG7V0QACpD2")->get();

        $recommendedGroups = [];
        $fullRecommendCount = 0;
        foreach($recommendations as $recommendation){
                $group = $recommendation;
                $viewers = $group->connections->where("role", "viewer");
                $group["created_at"] = $recommendation->created_at;
                $group["viewers"] = $viewers->count();
                $viewers->count() == 4 ? $fullRecommendCount++ : $fullRecommendCount;
                array_push($recommendedGroups, $group);
        }

        $recommendedGroups = array_unique($recommendedGroups);

        $db = $firestore->database();

        $plans = $db->collection('Plans');
        $plans = $plans->documents();
        $planList = [];

        foreach($plans as $plan){
            array_push($planList, $plan->data());
        }

        $data = [
            "focus" => $focus,
            "users" => $users,
            "groups" => $groups,
            "completed" => $completed,
            "emailVerified" => $emailVerified,
            "smsVerified" => $smsVerified,
            "hasPhoto" => $hasPhoto,
            "facebookCount" => $facebookCount,
            "recommendable" => $recommendable,
            "recommendations" => $recommendedGroups,
            "fullRecommendCount" => $fullRecommendCount,
            "appleCount" => $appleCount,
            "emailCount" => $emailCount,
            "plans" => $planList
        ];

        return view("admin.index", $data);
    }

    public function wishlists(){
        $wishlists = Wishlists::all();

        $data = [
            "wishlists" => $wishlists
        ];

        return view("admin.wishlists", $data);
    }

    public function deleteWishlist($id){
        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $item = $db->collection('Wishlists')->document($id)->delete();
    }

    public function navigators(){

        $auth = app('firebase.auth');
        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $navigators = $db->collection('Applications');
        $navigators = $navigators->documents();
        $navigatorList = [];
        $allstars = $oneventi = $invited = $waitlisted = $pro = 0;


        foreach($navigators as $navigator){
            // mark all pending
            
            

            $navigator = $navigator->data();
            if(array_key_exists("triptype",$navigator)){
        
                $navigator["allstar"] = "No";
                $navigator["onventi"] = "No";
                $navigator["guider"] = "No"; // has guide and planning experience
                

                if(array_key_exists("pro", $navigator) && $navigator["pro"] == "Yes"){
                    $pro++;
                }

                if(
                    $navigator["countries"] >= 8 &&
                    $navigator["experience"] != "I just want to travel more"
                ){
                    $navigator["allstar"] = "Yes";
                    $allstars++;
                    
                    
                }
                
                if($navigator["status"] == "invited"){
                    $invited++;
                }
                if($navigator["status"] == "waitlisted"){
                    $waitlisted++;
                }

                array_push($navigatorList, $navigator);
            }
        }

        $data = [
            "navigators" => $navigatorList,
            "allstars" => $allstars,
            "invited" => $invited,
            "waitlisted" => $waitlisted,
            "pro" => $pro
        ];

        return view("admin.navigators", $data);
    }

    public function scheduleNavigator($id){

        $auth = app('firebase.auth');
        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $onventi = false;

        // send email to applicatnat

        $ideaDoc = $db->collection('Applications')->document($id);

        $update = $ideaDoc->update([["path" => "status","value" => "invited"]]);

        $navigator = $ideaDoc->snapshot()->data();

        $data = $navigator;

        try{
            $user = $auth->getUserByEmail($navigator["email"]);
            if($user){
                $onventi = true;
            }
        }
        catch(FirebaseException $e){

        }


        $data = ["onventi" => $onventi];

        $email = $navigator["email"];
        $subject = "Schedule Your Venti Navigator Interview";
        $from = "no-reply@venti.co";


        Mail::send('emails.application.interview', $data, function($message) use($email , $subject, $from)
        {
            $message
                ->to($email)
                ->replyTo($email)
                ->from($from)
                ->subject($subject);
        });
    }

    public function waitlistNavigator($id){

        $auth = app('firebase.auth');
        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $onventi = false;

        // send email to applicatnat

        $ideaDoc = $db->collection('Applications')->document($id);

        $update = $ideaDoc->update([["path" => "status","value" => "waitlisted"]]);

        $navigator = $ideaDoc->snapshot()->data();

        $data = $navigator;

        try{
            $user = $auth->getUserByEmail($navigator["email"]);
            if($user){
                $onventi = true;
            }
        }
        catch(FirebaseException $e){

        }

        $data = ["onventi" => $onventi];

        $email = $navigator["email"];
        $subject = "Regarding Your Venti Navigator Application";
        $from = "no-reply@venti.co";


        Mail::send('emails.application.waitlisted', $data, function($message) use($email , $subject, $from)
        {
            $message
                ->to($email)
                ->replyTo($email)
                ->from($from)
                ->subject($subject);
        });
    }

    public function deleteNavigator($id){

        $firestore = app('firebase.firestore');
        $db = $firestore->database();

        $planRef = $db->collection('Applications')->document($id);
        $planRef->delete();
    }

    public function acceptNavigator($id){
        $auth = app('firebase.auth');
        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $onventi = false;

        // send email to applicatnat

        $ideaDoc = $db->collection('Applications')->document($id);

        $update = $ideaDoc->update([["path" => "status","value" => "accepted"]]);

        $navigator = $ideaDoc->snapshot()->data();

        $data = $navigator;

        try{
            $user = $auth->getUserByEmail($navigator["email"]);
            if($user){
                $onventi = true;
            }
        }
        catch(FirebaseException $e){

        }

        $data = ["onventi" => $onventi];

        $email = $navigator["email"];
        $subject = "Your Invitation Awaits to Become a Venti Navigator";
        $from = "no-reply@venti.co";


        Mail::send('emails.application.accepted', $data, function($message) use($email , $subject, $from)
        {
            $message
                ->to($email)
                ->replyTo($email)
                ->from($from)
                ->subject($subject);
        });
    }

    public function user($uid){
        $auth = app('firebase.auth');
        $user = $auth->getUser($uid);


        $verificationComplete = false;
        $emailVerified = $user->emailVerified;
        $smsVerified = false;
        $interests = [];

        
        if(array_key_exists("smsVerified", $user->customClaims)){
            $smsVerified = $user->customClaims["smsVerified"];
        }

        if($emailVerified === true && $smsVerified === true){
            $verificationComplete = true;
        }

        if(isset($user->customClaims["interests"])){
            $interests = $user->customClaims["interests"];
        }

        $data = [
            "user" => $user,
            "verificationComplete" => $verificationComplete,
            "smsVerified" => $smsVerified,
            "interests" => $interests
        ];

        return view ("admin.user", $data);
    }

    public function updateUser($uid, Request $request){
        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $auth = app('firebase.auth');
        $user = $auth->getUser($uid);
        $mediaLink = $request->pic;
        $smsVerified = $user->customClaims["smsVerified"] ?? false;

        $properties = [
            'displayName' => $request->displayName
        ];

        $updatedUser = $auth->updateUser($user->uid, $properties);

        if(strlen($request->bio) > 150){
            Return Redirect::back()->withErrors(["bioerror" => "Your answer to the math question was incorrect"]);
        }

        $auth->setCustomUserClaims($user->uid, [
            'username' => $user->customClaims['username'],
            "gender" => $request->gender,
            "smoking" => $request->smoking,
            "drinking" => $request->drinking,
            "dob" => $request->dob,
            "bio" => $request->bio,
            "occupation" => $request->occupation,
            "instagram" => str_replace('@','',$request->instagram),
            "requests" => $request->requests,
            'genderPref' => $request->genderPref,
            'sizePref' => $request->sizePref,
            'agePref' => $request->agePref,
            'spendingPref' => $request->spendingPref,
            'smokingPref' => $request->smokingPref,
            'drinkingPref' => $request->drinkingPref,
            "religion" => $request->religion,
            "smsVerified" => $smsVerified,
            "languages" => languagesToObject($request->languages),
            "interests" => interestsToObject($request->interests)
        ]);

        if($request->file("avatar") !== null){

            //dd($request->file("avatar"));
            $storage =  app('firebase.storage');
            $defaultBucket = $storage->getBucket();
            $file = $request->file("avatar");
            $filename = rand(999,9999999) . "-" . cleanFileName($file->getClientOriginalName());
            $extension = strtolower($request->file("avatar")->getClientOriginalExtension());

            $file = $this->shrinkImage($file, true);

            $uploadResult = $defaultBucket->upload(
                
                file_get_contents($file), [
                    'name' => $filename,
                    'uploadType'=> "media",
                    'predefinedAcl' => 'publicRead',
                    "metadata" => ["contentType"=> 'image/' . $extension],
                ]
            );

            $mediaLink = $uploadResult->info()["mediaLink"];

            $properties = [
                'photoUrl' => $mediaLink
            ];

            $updatedUser = $auth->updateUser($user->uid, $properties);


        }

        // SMS Auth

        Return Redirect::back()->withErrors(["success" => "Your answer to the math question was incorrect"]);
    }

    public function mailchimpList(){

        $MailChimp = new MailChimp(env("MAILCHIMP_APIKEY"));
        $result = $MailChimp->get('lists/' . env('MAILCHIMP_LIST_ID') . "/members", ["count" => 100]);
        
    }

    public function groups(){
        $auth = app('firebase.auth');
        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $groups = Groups::all();
        $groupsList = [];
        $archived = $expired = 0;
        $user = null;

        foreach($groups as $group){

            if(isset($group->clientID)){
                $user = Members::where('uid', $group["clientID"])->first();
            }

            if($group->isExpired()){
                $expired++;
            }

            $group["author"] = $user;


            array_push($groupsList, $group);

            if($group["archived"] == 1){
                $archived++;
            }
        }

        $data = [
            "focus" => "groups",
            "groups" => $groupsList,
            "archived" => $archived,
            "expired" => $expired
        ];

        return view("admin.index", $data);


    }

    public function mailchimpStatus($email){
        $uid = "DKeCom9cqsfMAa5G3tdq6Ksnb2D2"; //vitaliy
         $auth = app('firebase.auth');

        // send user an email
        $uid = "DKeCom9cqsfMAa5G3tdq6Ksnb2D2"; //vitaliy
        $user = $auth->getUser($uid);
        $customClaims = $user->customClaims;

        $customClaims["username"] = "clarkb";

        dd($user);


        return json_encode(Newsletter::isSubscribed($email));
    }

    public function activeNavigators(){
        $auth = app('firebase.auth');
        $firestore = app('firebase.firestore');
        $db = $firestore->database();

        $navigators = $db->collection('Navigators')->documents();
        $navigatorList = [];

        foreach($navigators as $navigator){
            $navigator = $navigator->data();
            $user = $auth->getUser($navigator["uid"]);
            array_push($navigatorList, $user);
        }

        $data = [
            "navigators" => $navigatorList
        ];

        return view("admin.navigator", $data);
    }

    public function verifyEmail(Request $request){
        $auth = app('firebase.auth');
        $firestore = app('firebase.firestore');
        $uid = $request->uid;
        
        $updatedUser = $auth->updateUser($uid, [
            "emailVerified" => true
        ]);
    }

    public function resetPhotoUrl($uid){
        $auth = app('firebase.auth');

        // send user an email
        $uid = "DKeCom9cqsfMAa5G3tdq6Ksnb2D2"; //vitaliy



        /*

        $user = $auth->getUser($uid);

        $data = ["user" => $user];

        $email = $user->email;
        $subject = "Your Venti Profile Picture Was Removed";
        $from = "no-reply@venti.co";


        Mail::send('emails.system.photo-removed', $data, function($message) use($email , $subject, $from)
        {
            $message
                ->to($email)
                ->replyTo($email)
                ->from($from)
                ->subject($subject);
        });
        
        $updatedUser = $auth->updateUser($uid, [
            "photoUrl" => "/assets/img/profile.jpg"
        ]);

        */
    }

    public function makeNavigator($uid){
        $auth = app('firebase.auth');
        $user = $auth->getUser($uid);
        $customClaims = $user->customClaims;

        $customClaims["nav"] = true;
        $auth->setCustomUserClaims($user->uid, $customClaims);
    }

    public function sendVerificationEmail($email){
        $auth = app('firebase.auth');
        $auth->sendEmailVerificationLink($email);
    }

    public function scoresheet($match, $matchee){
        $auth = app('firebase.auth');
        $match = $auth->getUser($match);
        $matchee = $auth->getUser($matchee);

        $compatibility = calcCompatibility($match, $matchee);

        dd([$match, $matchee, $compatibility]);
    }

    public function sendBoardingPassEmails(){
        dd("no");

        $i = 0;

        foreach($subscribers as $subscriber){
            $memberID = md5(strtolower($subscriber));
            

            $result = Http::get("https://emailoctopus.com/api/1.6/lists/3b3c3c3a-3b19-11ee-9258-cf345e7859db/contacts/$memberID", [
                'api_key' => env('EMAIL_OCTOPUS_API_KEY')
            ]);

            $subscriber = json_decode($result->body(),true);

            $referral = getEmailID($subscriber["id"]);

            $email = $subscriber["email_address"];
            $subject = "[Venti Boarding Pass]: Your Referral Link is Enclosed";
            $from = "markus@venti.co";
            $message = "";

            $data = ["referral" => $referral];
            

            echo "$email<br>";
        }

        dd("$i emails sent. Last recipient was $email");
    }

    public function subscribers(){

        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $auth = app('firebase.auth');

        $subscribersList = $db->collection("VIPS")->documents();

        $subscribers = [];

        foreach($subscribersList as $contact){
            $contact = $contact->data();
            $fields = $contact["fields"];

            array_push($subscribers, [
                "email" => $contact["email_address"],
                "fields" => $fields,
                "id" => $contact["id"]
            ]);
        }

        return $subscribers;
    }
}
