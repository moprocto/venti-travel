<?php

namespace App\Http\Controllers;

use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use Kreait\Firebase\Database;
use Kreait\Firebase\Auth;
use Google\Cloud\Firestore\FirestoreClient;
use Kreait\Firebase\Exception\FirebaseException;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Illuminate\Support\Facades\Http;

use App\Models\Airport as Airports;
use App\Models\Country as Countries;

use Illuminate\Http\Request;
use Redirect;
use Stripe;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function home(){

        return view('dashboard.home');
    }

    public function index()
    {
        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $auth = app('firebase.auth');
        $payments = $db->collection("Payments")->document("users")->collections();
        $transactions = [];

        foreach($payments as $payment){
            $userTransactions = $payment->listDocuments();
            foreach($userTransactions as $userTransaction){
                $path = explode("/", $userTransaction->path());
                $userID = $path[2];
                $tripID = explode("-",$path[3])[0];
                $idea = $db->collection("Ideas")->document($tripID)->snapshot()->data();
                $payment = $userTransaction->snapshot()->data();

                $personalized = false;

                if($payment["charged"] == ($payment["purchasePrice"] + $payment["personalizationCost"])){
                    $personalized = true;

                }

                array_push($transactions, [
                    "tripID" => $tripID,
                    "Title" => $idea["title"],
                    "Author" => $idea["author"],
                    "userID" => $userID,
                    "charged" => $payment["charged"],
                    "purchasePrice" => $payment["purchasePrice"],
                    "personalizationCost" => $payment["personalizationCost"],
                    "personalized" => $personalized,
                    "paymentDate" => $payment["paymentDate"]
                ]);
            }
        }

        $data = [
            "transactions" => $transactions
        ];



        return view("dashboard.index", $data);
    }


    public function catalog(){
        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $auth = app('firebase.auth');
        $ideas = $db->collection("Ideas")->listDocuments();
        $ideaList = [];
        
        foreach($ideas as $idea){
            $idea = $idea->snapshot()->data();
            if(array_key_exists("clientID", $idea)){
                $client = $db->collection("Clients")->document($idea["clientID"])->snapshot()->data();
                if(isset($client)){
                    $idea["author"] = $client["name"];
                }
            }

            array_push($ideaList, $idea);
        }

        $data = [
            "ideas" => $ideaList
        ];

        return view("dashboard.catalog.index", $data);
    }

    public function report($type, $id){
        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $auth = app('firebase.auth');
        $payments = $db->collection("Payments")->document("users")->collections();
        $transactions = [];

        foreach($payments as $payment){
            $userTransactions = $payment->listDocuments();
            foreach($userTransactions as $userTransaction){
                $path = explode("/", $userTransaction->path());
                $userID = $path[2];
                $tripID = explode("-",$path[3])[0];
                $idea = $db->collection("Ideas")->document($tripID)->snapshot()->data();
                $payment = $userTransaction->snapshot()->data();

                $personalized = false;

                if($payment["charged"] == ($payment["purchasePrice"] + $payment["personalizationCost"])){
                    $personalized = true;

                }

                if($type == "product" && $tripID == $id){
                    array_push($transactions, [
                        "tripID" => $tripID,
                        "Title" => $idea["title"],
                        "Author" => $idea["author"],
                        "userID" => $userID,
                        "charged" => $payment["charged"],
                        "purchasePrice" => $payment["purchasePrice"],
                        "personalizationCost" => $payment["personalizationCost"],
                        "personalized" => $personalized,
                        "paymentDate" => $payment["paymentDate"]
                    ]);
                }

                if($type == "user" && $userID == $id){
                    array_push($transactions, [
                        "tripID" => $tripID,
                        "Title" => $idea["title"],
                        "Author" => $idea["author"],
                        "userID" => $userID,
                        "charged" => $payment["charged"],
                        "purchasePrice" => $payment["purchasePrice"],
                        "personalizationCost" => $payment["personalizationCost"],
                        "personalized" => $personalized,
                        "paymentDate" => $payment["paymentDate"]
                    ]);
                }

                if($type == "author" && $idea["author"] == $id){
                    array_push($transactions, [
                        "tripID" => $tripID,
                        "Title" => $idea["title"],
                        "Author" => $idea["author"],
                        "userID" => $userID,
                        "charged" => $payment["charged"],
                        "purchasePrice" => $payment["purchasePrice"],
                        "personalizationCost" => $payment["personalizationCost"],
                        "personalized" => $personalized,
                        "paymentDate" => $payment["paymentDate"]
                    ]);
                }
            }
        }

        $data = [
            "transactions" => $transactions
        ];

        return view("dashboard.index", $data);
        
    }

    public function getStripeTransaction(){
        Stripe\Stripe::setApiKey(env('STRIPE_SANDBOX_SECRET'));
        
        $stripe = new \Stripe\StripeClient(
            env('STRIPE_SANDBOX_SECRET')
        );

        $charge = $stripe->charges->retrieve(
          'ch_3LB7DqFW7h4EERqb1E9HNHnG',
          []
        );

        dd($charge);
    }


    public function read($trip)
    {
        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $auth = app('firebase.auth');
        $idea = $db->collection("Ideas")->document($trip)->snapshot()->data();

        $departure = Carbon::createFromFormat('m-d-Y', str_replace("/","-",$idea["departure"]))->timezone('America/New_York');
        $return = Carbon::createFromFormat('m-d-Y', str_replace("/","-",$idea["departure"]))->addDays($idea["days"] - 1)->timezone('America/New_York');

        $schedule = null;
        $itineraryList = [];
        $clients = $db->collection("Clients")->listDocuments();
        $clientList = [];

        foreach($clients as $client){
            array_push($clientList, $client->snapshot()->data());
        }

        try{
            $schedules = CarbonPeriod::create($departure, $return);

            foreach($schedules as $schedule){
                array_push($itineraryList, $schedule->format("D, M j"));
            }

        } catch(Exception $e){

        }

        $data = [
            "departure" => $departure,
            "return" => $return,
            "trip" => $idea,
            "itineraryList" => $itineraryList,
            "clients" => $clientList
        ];

        return view('dashboard.edit', $data);
    }

    public function edit(Request $request){

            $firestore = app('firebase.firestore');
            $db = $firestore->database();
            $auth = app('firebase.auth');
            $idea = $db->collection("Ideas")->document($request->tripID);
            $itinerary = [];



            foreach($request->all() as $key => $value){
                if($key != "_token" && $key != "month" && $key != "day" && $key != "city" && $key != "agenda"){
                    $idea->update([
                        [
                            "path" => $key,
                            "value" => $value
                        ]
                    ]);
                }

            }

            $departure = Carbon::createFromFormat('Y-m-d', $request->departure)->format("m/d/Y");

            $idea->update([
                [
                    "path" => "departure",
                    "value" => $departure
                ],
                [
                    "path" => "itinerary",
                    "value" => $itinerary
                ],
                [
                    "path" => "budgetOverall",
                    "value" => ($request->foodBudget + $request->transportationBudget + $request->lodgingBudget + $request->activityBudget) * $request->days
                ]
            ]);
        
            Return Redirect::back();
    }

    public function create(){
        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $auth = app('firebase.auth');
        $clients = $db->collection("Clients")->listDocuments();
        $clientList = [];

        foreach($clients as $client){
            array_push($clientList, $client->snapshot()->data());
        }

        $data = [
            "clients" => $clientList
        ];


        return view('dashboard.new', $data);
        //return view('dashboard.catalog.new');
    }

    public function add(Request $request){

        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        
        $foodBudget  = $request->foodBudget; 
        $lodgingBudget = $request->lodgingBudget; 
        $transportationBudget = $request->transportationBudget; 
        $activityBudget = $request->activityBudget; 
        $budgetOverall = $foodBudget + $lodgingBudget + $transportationBudget + $activityBudget;

        $tags = [];

        if($request->tag != null && $request->tag != ""){
            $tags = $request->tag;
        }

        $departure = Carbon::createFromFormat('Y-m-d', $request->departure)->format("m/d/Y");

        $docRef = $db->collection("Ideas")->document($request->tripID)->set(
            [             
                "tripID" => $request->tripID,
                "archived" => $request->archived,
                "budgetOverall" => $budgetOverall,
                "country" => $request->country,
                "latitude" => $request->latitude,
                "longitude" => $request->longitude,
                "destination" => $request->destination,
                "title" => $request->title,
                "departure" => $departure,
                "days" => $request->days,
                "foodBudget" => $foodBudget,
                "lodgingBudget" => $lodgingBudget,
                "transportationBudget" => $transportationBudget,
                "activityBudget" => $activityBudget,
                "tags" => $request->tags,
                "imageURL" => $request->imageURL,
                "imageAuthor" => $request->imageAuthor,
                "imageAuthorProfile" => $request->imageAuthorProfile,
                "personalizationCost" => $request->personalizationCost,
                "personalizationNote" => $request->personalizationNote,
                "purchasePrice" => $request->purchasePrice,
                "purchaseNote" => $request->itineraryNotes,
                "clientID" => $request->clientID,
                "description" => $request->description,
                "worldee" => $request->worldee,
                "itinerary" => []
            ]
        );

        return Redirect::to('/dashboard/catalog/read/' . $request->tripID);
    }

    public function preview(){
        
    }

    public function builder(){
        return view('dashboard.builder');
    }
}
