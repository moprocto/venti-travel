<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Redirect;
use Mail;
use Validator;

class CRMController extends Controller
{
    public function __construct()
    {
        $this->middleware('manager');
    }

    public function index(){
        $data = [
            "title" => "Venti CRM"
        ];

        return view("crm.index", $data);
    }
    
    public function massMailer(){
        $data = [
            "title" => "Venti CRM - Mass Mailer"
        ];

        return view("crm.mass", $data);
    }

    public function sendEmail(Request $request){
        $email = $request->to;
        $name = $request->fname;
        $subject = $request->subject;
        $message = $request->message;
        
        $data = [
            "content" => $message
        ];

        if($request->cc == 1){
            Mail::send('emails.crm.plaintext', $data, function($message) use($email, $subject, $name)
                {
                    $message
                        ->to($email,"$name")
                        ->cc("nikhil@venti.co", "Nikhil Kumar")
                        ->bcc("markus@venti.co", "Markus Proctor")
                        ->from("markus@venti.co","Markus Proctor")
                        ->replyTo("markus@venti.co")
                        ->subject($subject);
            });
        } else {
            Mail::send('emails.crm.plaintext', $data, function($message) use($email, $subject, $name)
                {
                        $message
                            ->to($email,"$name")
                            ->bcc("markus@venti.co", "Markus Proctor")
                            ->from("markus@venti.co","Markus Proctor")
                            ->replyTo("markus@venti.co")
                            ->subject($subject);
            });
        }

        

        return Redirect::back();
    }
    
   public function massMailerSend(Request $request)
{
    $csvContent = $request->input('CSVcontent');
    $template = $request->input('template');

    // Read the CSV content
    $lines = explode("\n", $csvContent);
    $headers = str_getcsv($lines[0]);

    // Loop through each row in the CSV
    for ($i = 1; $i < count($lines); $i++) {
        $row = str_getcsv($lines[$i]);

        $data = array_combine($headers, $row);

        $email = $data['Email'];
        $name = $data['First Name'] . ' ' . $data['Last Name'];
        $subject = 'Make Banking Fun Again with Microrewards';
        $message = str_replace([
            '{{first_name}}',
            '{{company}}'
        ], [
            $data['First Name'],
            $data['Company']
        ], $template);

        $mailData = [
            "content" => $message
        ];

        Mail::send('emails.crm.plaintext', $mailData, function($message) use($email, $subject, $name)
        {
            $message
                ->to($email, $name)
                ->bcc("markus@venti.co", "Markus Proctor")
                ->from("markus@venti.co", "Markus Proctor")
                ->replyTo("markus@venti.co")
                ->subject($subject);
        });

        // Add a 300ms delay between each email
        usleep(300000);
    }

    return Redirect::back();
}
}
