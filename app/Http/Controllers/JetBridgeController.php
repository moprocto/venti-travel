<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

use Session;
use Location;
use Redirect;
use Mail;
use Exception;
use Validator;

class JetBridgeController extends Controller
{
    public function index(){
        $data = [
            "title" => "$100,000 Travel Line of Credit",
            "description" => "Explore the world with ease using Jet Bridge — your flexible travel line of credit of up to $100,000. Enjoy stress-free vacations with our quick approval process and rock-bottom rates.",
            "image" => "https://venti.co/assets/img/jet-bridge-social.jpg",
            "url" => "https://venti.co/jetbridge"
        ];

        return view("tailwind.index", $data);
    }

    public function success(Request $request){

        $email = $request->email;
        $creditScoreValue = $request->creditScoreValue;
        $savingsBalance = $request->savingsBalance;

        $scores = ["580-620", "621-660", "661 - 700", "701-740", "741-780","781+"];
        $balances = ["< $10,000", "$20,000", "$40,000", "$60,000", "$80,000", "$100,000+"];

        $score = $scores[$creditScoreValue] ?? 0;
        $balance = $balances[$savingsBalance] ?? 0;

        if($email != null && $email != ""){

            $source = Session::get('referrer') ?? "direct";
            $ip = \Request::ip();
            $location = Location::get($ip);
            $agent = $request->header('User-Agent');
            $region = "none";

            if(isset($location) && !is_bool($location)){
                $region = $location->regionName;
            }

            $fields = [
                "Score" => $score,
                "Savings" => $balance,
                "Source" => $source,
                "IP" => $ip ?? "none",
                "Location" => $location ? $region : "none",
                "Product" => $request->product,
                "Income" => $request->income,
                "Rent" => $request->rent,
                "Spending" => $request->spending,
                "Company" => $request->company,
                "Hires" => $request->hires
            ];

            $result = Http::post("https://emailoctopus.com/api/1.6/lists/3dfbd682-9b91-11ee-8942-415093378f84/contacts", [
                'api_key' => env('EMAIL_OCTOPUS_API_KEY'),
                'email_address' => $email,
                "fields" => $fields
            ]);

            $result = json_decode($result->body(),true);


            if(!array_key_exists("error",$result)){
                // add contact to email octopus
                // send the success message to facebook
                if(env('APP_ENV') != "local"){
                    $this->submitFacebookConversionAPI($email, $ip, $location, $agent);
                } else {
                    //$this->submitFacebookConversionAPITEST($email, $ip, $location, $agent);
                }

                $id = getEmailID($result["id"]);

                $data = [
                    "title" => "Travel for Free Without Credit Cards",
                    "description" => "",
                    "image" => "https://venti.co/assets/img/boarding-pass-social.jpg",
                    "url" => "https://venti.co/boardingpass",
                    "referral" => $id
                ];

                // send email

                try{

                    $email = $email;
                    $subject = "[Venti Rewards Debit] VIP Confirmation";
                    if($request->product == "business"){
                        $subject = "[Venti for Business] Inquiry Confirmation";
                    }
                    if($request->product == "business"){
                        $subject = "[Venti] 20 Countries Waitlist Confirmation";
                    }
                    $from = "no-reply@venti.co";
                    $message = "";

                    Mail::send('emails.application.' . $request->product, $data, function($message) use($email , $subject, $from)
                    {
                        $message->to($email)->replyTo($email)->from($from)->subject($subject); 

                    });

                    // try adding to firebase

                    $firestore = app('firebase.firestore');
                    $db = $firestore->database();
                    $auth = app('firebase.auth');

                    $db->collection("VIPS")->document($id)->set($result);

                }
                catch(Exception){

                }

                return json_encode(200);
            }
        }

        return json_encode(500);
    }

    public function submitFacebookConversionAPI($email, $ip = null, $location = null, $agent = null){
        

        $userdata = [
            "em" => hash('sha256', strtolower($email))
        ];

        if($ip != "none" && !is_null($ip)){
            $userdata["client_ip_address"] = $ip;
        }

        if(!is_null($agent)){
            $userdata["client_user_agent"] = $agent;
        }

        if(!is_null($location)){
            $userdata["country"] = hash('sha256', strtolower($location->countryName));
            $userdata["st"] = hash('sha256', strtolower($location->regionName));
            $userdata["zp"] = hash('sha256', strtolower($location->zipCode));
        }

        if(Session::get('fbclid') !== null){
            // add the fbclick id to the pixel conversion
            $userdata["fbc"] = "fb.1." . (int) (microtime(true) * 1000) . "." . Session::get('fbclid');
        }

        $data = [ // main object
            "data" => [ // data array
                [
                    "event_name" => "Lead",
                    "event_time" => time(),
                    "user_data" => $userdata,
                    "action_source" => "website"
               ],
            ],
            
            "access_token" => env('FACEBOOK_CONVERSION_TOKEN')
        ];
    
        $dataString = json_encode($data);
        // pixel ID: 1169331916805166
        $ch = curl_init('https://graph.facebook.com/v11.0/1169331916805166/events');                                                                      
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
        curl_setopt($ch, CURLOPT_POSTFIELDS, $dataString);                                                                  
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
            'Content-Type: application/json',                                                                                
            'Content-Length: ' . strlen($dataString))                                                                       
        );                       


        // clear fbclic

        Session::flush();                                                                                                                                                
        $response = curl_exec($ch);

    }
}
