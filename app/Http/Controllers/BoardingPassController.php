<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Kreait\Firebase\Exception\FirebaseException;
use Carbon\Carbon;
use Carbon\CarbonPeriod;

use Session;
use Location;
use Redirect;
use Mail;
use Exception;
use Validator;
use Dwolla;

use Stripe\Stripe as Stripe;
use \Stripe\StripeClient as StripeClient;

use TomorrowIdeas\Plaid\Plaid;


class BoardingPassController extends Controller
{
    public function index(Request $request){

        if($request->fbid == "fb"){
            Session::put('referrer', "facebook");
        }
        elseif($request->fbid == "ig"){
            Session::put('referrer', "instagram");
        }
        elseif($request->fbid == "msg"){
            Session::put('referrer', "messenger");
        }
        elseif($request->fbid == "adwords"){
            Session::put('referrer', "adwords");
        }
        elseif(strlen($request->fbid) >= 12){
            Session::put('referrer', $request->fbid);
        }
        if(Session::get('fbclid') === null){
            Session::put('fbclid', $request->fbclid);
        }
        
        $data = [
            "title" => "9% APY Travel Savings Account",
            "description" => "Traveling for free trip just got whole lot easier.",
            "image" => "https://venti.co/assets/img/boarding-pass-social.jpg",
            "url" => "https://venti.co/boardingpass"
        ];

        if(env('APP_ENV') == "local"){
            // get the IP address
            $ip = \Request::ip();
            if($ip != "68.100.62.8" && $ip != "127.0.0.1"){
                return Redirect::to('https://venti.co');
            }
        }

        // get recent searches

        return view('boardingpass.index', $data);
    }

    public function buddyIndex(Request $request){
        if($request->fbid == "fb"){
            Session::put('referrer', "facebook");
        }
        elseif($request->fbid == "ig"){
            Session::put('referrer', "instagram");
        }
        elseif($request->fbid == "msg"){
            Session::put('referrer', "messenger");
        }
        elseif($request->fbid == "adwords"){
            Session::put('referrer', "adwords");
        }
        elseif(strlen($request->fbid) > 12){
            Session::put('referrer', $request->fbid);
        }
        if(Session::get('fbclid') === null){
            Session::put('fbclid', $request->fbclid);
        }

        $data = [
            "title" => "Venti Buddy Pass",
            "description" => "Our free forever tier to unlock travel savings",
            "image" => "https://venti.co/assets/img/boarding-pass-social.jpg",
            "url" => "https://venti.co/boardingpass"
        ];

        return view('boardingpass.buddy', $data);

    }

    public function indexTest(Request $request){

        if($request->fbid == "fb"){
            Session::put('referrer', "facebook");
        }
        elseif($request->fbid == "ig"){
            Session::put('referrer', "instagram");
        }
        elseif($request->fbid == "msg"){
            Session::put('referrer', "messenger");
        }
        elseif(strlen($request->fbid) > 12){
            Session::put('referrer', $request->fbid);
        }
        
        $data = [
            "title" => "Travel for Free Without Credit Cards",
            "description" => "Traveling for free just got whole lot easier",
            "image" => "https://venti.co/assets/img/boarding-pass-social.jpg",
            "url" => "https://venti.co/boardingpass"
        ];

        return view('tests.landing', $data);
    }

    public function authMFA(){
        $smsVerified = Session::get('user')->customClaims["smsVerified"] ?? false;
        $phoneNumber = Session::get('user')->customClaims["phoneNumber"] ?? false;

        if(!$phoneNumber || !$smsVerified){
            // this user needs to go back to onboarding
            return Redirect::to('boardingpass/onboarding/phone');
        }

        $data = [
            "title" => "Venti MFA Checkpoint",
            "smsVerified" => $smsVerified
        ];

        return view('auth.mfa', $data);
    }

    public function register(Request $request){

        // do all the same validations here. 
        // proceed user to onboarding
        
        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $auth = app('firebase.auth');
        $photoUrl = '/assets/img/profile.jpg';
        $createdUser = null;

        // validation

        $validator = Validator::make($request->all(), [
            'fname' => 'required|max:255',
            'lname' => 'required|max:255',
            'email' => 'required|max:255',
            'password' => 'required|min:8|',
            'subscription' => 'required'
        ]);

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        try{
            $userProperties = [
                'email' => $request->email,
                'emailVerified' => false,
                'password' => $request->password,
                'displayName' => $request->fname . "|" . $request->lname,
                'photoUrl' => $photoUrl,
                'disabled' => false
            ];

            try{
                // create profile in user collection
                $createdUser = $auth->createUser($userProperties);
            }
            catch(FirebaseException $e){
                // usually a bot trying to re-register
                return Redirect::back()->withErrors($validator)->withInput();
            }

            

            $referrer = Session::get('referrer') ?? "none";

            $auth->setCustomUserClaims($createdUser->uid, [
                'dob' => $request->dob,
                'referrer' => $referrer,
                "smsVerified" => false,
                "religion" => "none",
                "languages" => ["English"],
                "interests" => [],
                "timezone" => "America/New_York",
                "subscription" => $request->subscription,
                "subscriptionID" => null
            ]);

            // don't trust the data coming in. fix for later otherwise usernames can get stolen

            $createdUser = $auth->getUser($createdUser->uid);

            try{
                // send custom template

                $email = $createdUser->email;

                $link = $auth->getEmailVerificationLink($email);

                $data = [
                    "verificationLink" => $link,
                    "user" => $createdUser,
                    "last" => false
                ];

                $subject = "Welcome to Venti! You Need to Verify Your Email";

                try{
                    Mail::send('emails.onboarding.email-verification', array("data" => $data), function($message) use($email, $subject)
                        {
                            $message
                                ->to($email)
                                ->from("no-reply@venti.co","Venti")
                                ->replyTo("no-reply@venti.co")
                                ->subject($subject);
                    });
                }
                catch(Exception $e){
                    //
                }

            } catch (FirebaseException $e) {
                
            }

            if(env('APP_ENV') != "local"){

                $ip = \Request::ip();
                $location = Location::get($ip);
                $agent = $request->header('User-Agent');
                $region = "none";

                if(isset($location) && !is_bool($location)){
                    $region = $location->regionName;
                }

                $this->submitFacebookConversionAPI($email, $ip, $location, $agent);

            }

            Session::put('user', $createdUser);
            Session::put('active', true);

            return Redirect::to('/boardingpass/onboarding/email');
        }
        catch(FirebaseException $e){

            return Redirect::back()->withErrors(["This email is already in use"]);
        }

    }

    public function continueCheck(){
        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $auth = app('firebase.auth');
        $user = Session::get('user');

        try{
            $user = $auth->getUserByEmail($user->email);

            if(!$user->emailVerified){
                return Redirect::to('boardingpass/onboarding/email');
            }

            $customClaims = $user->customClaims ?? false;

            if(!$customClaims["smsVerified"]){
                return Redirect::to('boardingpass/onboarding/phone');
            }

            // the user has completed all the basic steps to create a Venti Account

            return Redirect::to('/home');
        }
        catch(FirebaseException $e){
            // error page for when we cannot find the account
        }

        // error page for when we cannot return a result

        
    }

    public function onboardingEmail(){

        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $auth = app('firebase.auth');
        $user = Session::get('user');

        if($user === null){
            // session expired?
            return Redirect::to('/login');
        }

        try{
            $user = $auth->getUserByEmail($user->email);

            $data = [
                "title" => "Venti Onboarding | Email",
                "email" => $user->email,
                "emailVerified" => $user->emailVerified
            ];

            return view("boardingpass.onboarding.email", $data);
        }
        catch(FirebaseException $e){

        }
    }

    public function onboardingPhone(){
        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $auth = app('firebase.auth');
        $user = Session::get('user');

        if($user === null){
            return Redirect::to('/login');
        }

        try{
            $data = [
                "title" => "Venti Onboarding | Phone",
                "email" => $user->email,
                "smsVerified" => $user->emailVerified
            ];

            return view("boardingpass.onboarding.phone", $data);
        }

        catch(FirebaseException $e){

        }
    }

    public function onboardingBank(){

        // initiate dwolla flow
        // we need to confirm that they are completely new to this process
        $dwolla = new Dwolla;

        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $auth = app('firebase.auth');
        $user = Session::get('user');

        $customer = $dwolla->getCustomer($user, $auth, $db) ;

        if(!$customer){
            // we need to create a customer profile

            //$customer = $dwolla->createCustomer($user);

        }

        // 

        return view("boardingpass.connect-bank");
    }

    public function success(Request $request){

        $email = $request->email;
        $deposit = $request->deposit;
        $contribution = $request->contribution;
        $balanceFinal = $request->balanceFinal;
        $pointsFinal = $request->pointsFinal;

        if($email != null && $email != ""){

            $source = Session::get('referrer') ?? "direct";
            $ip = \Request::ip();
            $location = Location::get($ip);
            $agent = $request->header('User-Agent');
            $region = "none";

            if(isset($location) && !is_bool($location)){
                $region = $location->regionName;
            }

            $fields = [
                "Deposit" => $deposit ?? 0,
                "Contribution" => $contribution ?? 0,
                "BalanceFinal" => $balanceFinal ?? 0,
                "PointsFinal" => $pointsFinal ?? 0,
                "Source" => $source,
                "IP" => $ip ?? "none",
                "Location" => $location ? $region : "none"
            ];

            $result = Http::post("https://emailoctopus.com/api/1.6/lists/3b3c3c3a-3b19-11ee-9258-cf345e7859db/contacts", [
                'api_key' => env('EMAIL_OCTOPUS_API_KEY'),
                'email_address' => $email,
                "fields" => $fields
            ]);

            $result = json_decode($result->body(),true);

            if(!array_key_exists("error",$result)){
                // add contact to email octopus
                // send the success message to facebook
                if(env('APP_ENV') != "local"){
                    $this->submitFacebookConversionAPI($email, $ip, $location, $agent);
                } else {
                    $this->submitFacebookConversionAPITEST($email, $ip, $location, $agent);
                }

                $id = getEmailID($result["id"]);

                $data = [
                    "title" => "Travel for Free Without Credit Cards",
                    "description" => "Traveling for free just got whole lot easier.",
                    "image" => "https://venti.co/assets/img/boarding-pass-social.jpg",
                    "url" => "https://venti.co/boardingpass",
                    "referral" => $id
                ];

                // send email

                try{
                    $referral = $id;
                    $email = $email;
                    $subject = "[Venti Boarding Pass]: Your Referral Link is Enclosed";
                    $from = "markus@venti.co";
                    $message = "";

                    $data = ["referral" => $referral];


                    Mail::send('emails.application.boardingpass', $data, function($message) use($email , $subject, $from)
                    {
                        $message
                            ->to($email)
                            ->replyTo($email)
                            ->from($from)
                            ->subject($subject);
                    });

                    // try adding to firebase

                    $firestore = app('firebase.firestore');
                    $db = $firestore->database();
                    $auth = app('firebase.auth');

                    $db->collection("VIPS")->document($id)->set($result);

                }
                catch(Exception){

                }

                return view('boardingpass.success', $data);
            }
        }

        Session::flash('subscribeError', "yes");

        return Redirect::back();
    }

    public function successTest(Request $request){

        $email = $request->email;
        $deposit = $request->deposit;
        $contribution = $request->contribution;
        $balanceFinal = $request->balanceFinal;
        $pointsFinal = $request->pointsFinal;

        if($email != null && $email != ""){

            $source = Session::get('referrer') ?? "direct";
            $ip = \Request::ip();
            $location = Location::get($ip);
            $agent = $request->header('User-Agent');
            $region = "none";

            if(isset($location) && !is_bool($location)){
                $region = $location->regionName;
            }

            $fields = [
                "Deposit" => $deposit ?? 0,
                "Contribution" => $contribution ?? 0,
                "BalanceFinal" => $balanceFinal ?? 0,
                "PointsFinal" => $pointsFinal ?? 0,
                "Source" => $source,
                "IP" => $ip ?? "none",
                "Location" => $location ? $region : "none"
            ];

            $result = Http::post("https://emailoctopus.com/api/1.6/lists/3b3c3c3a-3b19-11ee-9258-cf345e7859db/contacts", [
                'api_key' => env('EMAIL_OCTOPUS_API_KEY'),
                'email_address' => $email,
                "fields" => $fields
            ]);

            $result = json_decode($result->body(),true);

            if(!array_key_exists("error",$result)){
                // add contact to email octopus
                // send the success message to facebook
                if(env('APP_ENV') != "local"){
                    $this->submitFacebookConversionAPI($email, $ip, $location, $agent);
                }

                $id = getEmailID($result["id"]);

                $data = [
                    "title" => "Travel for Free Without Credit Cards",
                    "description" => "Traveling for free just got whole lot easier.",
                    "image" => "https://venti.co/assets/img/boarding-pass-social.jpg",
                    "url" => "https://venti.co/boardingpass",
                    "referral" => $id
                ];

                // send email

                try{
                    $referral = $id;
                    $email = $email;
                    $subject = "[Venti Boarding Pass]: Your Referral Link is Enclosed";
                    $from = "markus@venti.co";
                    $message = "";

                    $data = ["referral" => $referral];


                    Mail::send('emails.application.boardingpass', $data, function($message) use($email , $subject, $from)
                    {
                        $message
                            ->to($email)
                            ->replyTo($email)
                            ->from($from)
                            ->subject($subject);
                    });
                }
                catch(Exception){

                }

                return view('boardingpass.success', $data);
            }
        }

        Session::flash('subscribeError', "yes");

        return Redirect::back();
    }

    public function submitFacebookConversionAPITEST($email){
        $data = [ // main object
            "data" => [ // data array
                [
                    "event_name" => "TestEvent",
                    "event_time" => time(),
                    "user_data" => [
                        "em" => hash('sha256', strtolower($email))
                    ],
                    "action_source" => "website"
               ],
            ],
            "test_event_code" => "TEST44999",
            "access_token" => env('FACEBOOK_CONVERSION_TOKEN')
        ];          
    
        $dataString = json_encode($data);
        // pixel ID: 1169331916805166
        $ch = curl_init('https://graph.facebook.com/v11.0/1169331916805166/events');                                                                      
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
        curl_setopt($ch, CURLOPT_POSTFIELDS, $dataString);                                                                  
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
            'Content-Type: application/json',                                                                                
            'Content-Length: ' . strlen($dataString))                                                                       
        );                                                                                                                                                                       
        $response = curl_exec($ch);


    }

    public function submitFacebookConversionAPI($email, $ip = null, $location = null, $agent = null){
        

        $userdata = [
            "em" => hash('sha256', strtolower($email))
        ];

        if($ip != "none" && !is_null($ip)){
            $userdata["client_ip_address"] = $ip;
        }

        if(!is_null($agent)){
            $userdata["client_user_agent"] = $agent;
        }

        if(!is_null($location)){
            $userdata["country"] = hash('sha256', strtolower($location->countryName));
            $userdata["st"] = hash('sha256', strtolower($location->regionName));
            $userdata["zp"] = hash('sha256', strtolower($location->zipCode));
        }

        if(Session::get('fbclid') !== null){
            // add the fbclick id to the pixel conversion
            $userdata["fbc"] = "fb.1." . (int) (microtime(true) * 1000) . "." . Session::get('fbclid');
        }

        $data = [ // main object
            "data" => [ // data array
                [
                    "event_name" => "Lead",
                    "event_time" => time(),
                    "user_data" => $userdata,
                    "action_source" => "website"
               ],
            ],
            
            "access_token" => env('FACEBOOK_CONVERSION_TOKEN')
        ];
    
        $dataString = json_encode($data);
        // pixel ID: 1169331916805166
        $ch = curl_init('https://graph.facebook.com/v11.0/1169331916805166/events');                                                                      
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
        curl_setopt($ch, CURLOPT_POSTFIELDS, $dataString);                                                                  
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
            'Content-Type: application/json',                                                                                
            'Content-Length: ' . strlen($dataString))                                                                       
        );                       


        // clear fbclic

        Session::flush();                                                                                                                                                
        $response = curl_exec($ch);

    }

    public function createCustomer(Request $request){
        //

        if($request->subscription != "buddy"){
            $validator = Validator::make($request->all(), [
                'ssn' => 'required|min:4|max:9',
                'zipcode' => 'required|min:5|max:5',
                'address1' => 'required|max:255',
                'city' => 'required|max:255',
                'state' => 'required|max:255',
                'dob' => 'required|min:8|max:255',
                'zipcode' => 'required|min:5',
                'subscription' => 'required|min:5'
            ]);

            if ($validator->fails()) {
                // not all required fields were given, or the data was manipulated in transport
                return 201;
            }
        }

        // check if we already have a customer record
        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $auth = app('firebase.auth');
        $user = Session::get('user');
        
        if($user === null){
            // login expired
            return 202;
        }

        // set paid to true

        $user = $auth->getUserByEmail($user->email);
        $customClaims = $user->customClaims;
        $customClaims["appPaid"] = true;
        // setting the subscription ID prevents a loop
        $subscription = $request->subscription;

        $dwolla = new Dwolla;

        $customer = $dwolla->getCustomer($user, $auth, $db);
        $status = "pending";
        $upgrade = $request->upgrade;
        $request->customer = $customer;

        // we can confirm there is no existing customer record. proceed
        // if upgrade is true, we are upgrading their account to paid

        if($subscription != "buddy"){
            if($upgrade == "false"){
                $customerUrl = $dwolla->createCustomer($user, $request, "personal");
            }
            else{
                $customerUrl = $customer["customerID"];
                $dwolla->updateCustomer($user, $request, $customerUrl);
            }
        }
        else {
            // create a non-verified cutomer
            $status = "unverified";
            $customerUrl = $dwolla->createCustomer($user, $request, "unverified");
        }

        if($customerUrl){

            $customClaims["subscription"] = $subscription; 
            $customClaims["subscriptionID"] = strtotime("now"); 

            $auth->setCustomUserClaims($user->uid, $customClaims);
            
            $customer = explode("customers/",$customerUrl)[1];

            // we store the customer id in the document path because the webhook does carry the corelation ID organically.

            $age = \Carbon\Carbon::parse($request->dob)->age;
            $createdAt = \Carbon\Carbon::now('UTC')->timestamp;

            $db->collection("Customers")->document($customer)->set(
                [
                    "uid" => $user->uid,
                    "customerID" => $customerUrl,
                    "status" => $status,
                    "age" => $age,
                    "createdAt" => $createdAt
                ]
            );

            // send email confirmation

            if($subscription != "buddy"){
                $email = Session::get('user')->email;
                $subject = "[Venti Boarding Pass] Receipt for Account Subscription";

                $fee = env("VENTI_" . strtoupper($subscription) . "_ANNUAL_FEE")/100;


                $data = [
                    "fee" => $fee,
                    "product" => $subscription
                ];


                Mail::send('emails.onboarding.app-receipt', array("data" => $data), function($message) use($email, $subject)
                    {
                        $message
                            ->to($email)
                            ->from("no-reply@venti.co","Venti")
                            ->replyTo("no-reply@venti.co")
                            ->subject($subject);
                });
            }

            if($upgrade == "true"){
                // we need to update the current session to denote change in membership tier
                $user = $auth->getUser($user->uid);
                Session::put('user', $user);
            }

            return 200;

        } else {
            // contact support
            return 203;
        }

        return json_encode(201);
    }

    public function updateCustomer(Request $request){
        // a customer record exists, and we're updating it with newly submitted info

        $validator = Validator::make($request->all(), [
            'ssn' => 'required|min:9|max:9',
            'zipcode' => 'required|min:5|max:5',
            'address1' => 'required|max:255',
            'city' => 'required|max:255',
            'state' => 'required|max:255',
            'dob' => 'required|min:8|max:255',
            'zipcode' => 'required|min:5'
        ]);

        if ($validator->fails()) {
            // not all required fields were given, or the data was manipulated in transport
            return 201;
        }

        // check if we already have a customer record
        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $auth = app('firebase.auth');
        $user = Session::get('user');
        if($user === null){
            // login expired
            return 202;
        }

        // set paid to true

        $user = $auth->getUserByEmail($user->email);
        $customClaims = $user->customClaims;
        $customClaims["appPaid"] = true;

        $auth->setCustomUserClaims($user->uid, $customClaims);


        $dwolla = new Dwolla;

        $customer = $dwolla->getCustomer($user, $auth, $db);

        $customerUrl = $customer["customerID"];

        $customer = explode("customers/",$customerUrl)[1];

        $updateCustomer = $dwolla->updateCustomer($user, $request, $customerUrl);

        // get customer age

        $age = \Carbon\Carbon::parse($request->dob)->age;
        $createdAt = \Carbon\Carbon::now('UTC')->timestamp;

        $db->collection("Customers")->document($customer)->set(
            [
                "uid" => $user->uid,
                "customerID" => $customerUrl,
                "status" => "pending",
                "age" => $age,
                "createdAt" => $createdAt
            ]
        );

        $email = $user->email;
        $subject = "[Venti Boarding Pass] Application Successfully Resubmitted";

        Mail::send('emails.onboarding.resubmitted', array("data" => []), function($message) use($email, $subject)
                {
                    $message
                        ->to($email)
                        ->from("no-reply@venti.co","Venti")
                        ->replyTo("no-reply@venti.co")
                        ->subject($subject);
        });

        return 200;
    }

    public function checkCustomerStatus(){
        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $auth = app('firebase.auth');
        $user = Session::get('user');
        $dwolla = new Dwolla;

        $customer = $dwolla->getCustomer($user, $auth, $db);

        $customer = $dwolla->readCustomer($customer["customerID"]);

        $customerDoc = $db->collection("Customers")->document($customer->id);

        if($customer->status == "verified"){
            $update = $customerDoc->update([
                    ["path" => "status","value" => "verified"]
            ]);

            return 200;
        }

        return 500; // still pending
    }

    public function uploadCustomerDocument(Request $request){


        $validator = Validator::make($request->all(), [
            'documentType' => 'required',
            'file' => 'required|max:10256|mimes:jpeg,jpg,png',
        ]);

        if ($validator->fails()) {
            return json_encode(500);
        }

        $file = $request->file('file');

        $dwolla = new Dwolla;

        $user = Session::get('user');
        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $auth = app('firebase.auth');

        $customer = $dwolla->getCustomer($user, $auth, $db);


        $customerID = explode("customers/", $customer["customerID"])[1];

        $customerDoc = $db->collection("Customers")->document($customerID);

        $document = $dwolla->uploadCustomerDocument($customerID,$request->documentType, $file);

        if($document != false){
            
            $update = $customerDoc->update([
                    ["path" => "status","value" => "pending"],
                    ["path" => "document","value" => $document]
            ]);

            return 200;
        }

        return 500;

    }

    public function createFundingSource(Request $request){
        // check inputs again
        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $auth = app('firebase.auth');
        $user = Session::get('user');
        $dwolla = new Dwolla;
        $customer = $dwolla->getCustomer($user, $auth, $db);

        // we need to check if the bank's routing number has been black listed

        $blacklisted = isBankBlacklisted($request->routingNumber);

        if($blacklisted){
            // the customer tried to add a blacklisted bank account
            return json_encode([
                "error" => 500,
                "message" => "We are not allowing deposits and withdrawals with Idaho Central Credit Union at this time. Please use a different bank."
            ]);
        }

        // get an inventory of funding sources

        $fundingSources = $dwolla->fundingSources($customer["customerID"]);
        $fundingSourcesCount = 0;

        foreach($fundingSources as $fundingSource){
            if(!$fundingSource->removed 
                && isset($fundingSource->bankAccountType)
                && $fundingSource->status == "verified"
            ){
                $fundingSourcesCount++;
            }
            if(!$fundingSource->removed 
                && isset($fundingSource->bankAccountType)
                && $fundingSource->status == "unverified"
            ){
                $fundingSourcesCount++;
            }
        }

        // check the age of the account

        $createdAt = $user->metadata->createdAt->format("Y-m-d");

        $creationDate = new Carbon($createdAt);

        $accountAge = $creationDate->diffInDays(Carbon::now());

        if($fundingSourcesCount > 0 && $accountAge < 30){
            return json_encode([
                "error" => 500,
                "message" => "Accounts created within the last 30 days are limited to one funding source."
            ]);
        }


        
        $fundingSource = $dwolla->createFundingSource($customer, $request);

        if($fundingSource){
            // store funding source url with form details $fundingSource 

            # => "https://api-sandbox.dwolla.com/funding-sources/375c6781-2a17-476c-84f7-db7d2f6ffb31"


            $fundingSourceID = explode("/funding-sources/",$fundingSource)[1] ?? false;

            if($fundingSourceID){

                // db connection

                // initiate micro deposits

                $initiateMicrodeposits = $dwolla->initiateMicrodeposits($fundingSource);

                $fundingDoc = $db->collection("Sources")->document($fundingSourceID)->set([
                    "uid" => $user->uid,
                    "source" => $fundingSource,
                    "id" => $fundingSourceID,
                    "microdeposited" => $initiateMicrodeposits,
                    "name" => $request->name,
                    "owner" => cleanName($request->accountName),
                    "number" => $request->accountNumber
                ]);

            }
            else{
                return json_encode([
                    "error" => 500,
                    "message" => "A system error occured. Please contact venti support via admin@venti.co"
                ]);
            }

            return 200;
        }

        return json_encode([
            "error" => 500,
            "message" => "Your bank account details did not pass verification. Please try again or contact support via admin@venti.co"
        ]);
    }   

    public function removeFundingSource(Request $request){

        if(!isset($request->fundingSourceID)){
            return json_encode(500);
        }
        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $dwolla = new Dwolla;

        try{
            $fundingDoc = $db->collection("Sources")->document($request->fundingSourceID)->snapshot()->data();
            if($fundingDoc == null){
                return json_encode(500);
            }
            $dwolla->removeFundingSource($fundingDoc["source"]);
            return json_encode(200); 
        }
        catch(FirebaseException $e){
            return json_encode(500); 
        }

        return json_encode(500);
    }

    public function saveCard(Request $request){

        $user = Session::get('user');

        $validator = Validator::make($request->all(), [
            'payment_method_id' => 'required',
            'cardName' => 'nullable|string|max:40|regex:/^[a-zA-Z\s]+$/',
            'cardNickname' => 'nullable|string|max:30',
            'cardBrand' => 'nullable|string|max:30',
        ]);

        if ($validator->fails()) {
            return json_encode(["status" => 500, "message" => $validator->errors()->first()]);
        }

        $result = Http::withHeaders([
            'x-api-key' => 'h3z5uLO9xJxK7Ae6Flq8uYqPIqaPAHuOEuoW4v4r601R9Ez7',
            'uid' => $user->uid
        ])
        ->post(env("VENTI_API_URL") . "boardingpass/customer/fundingSource/card/save",[
            'payment_method_id' => $request->payment_method_id,
            'cardName' => $request->cardName,
            'cardNickname' => $request->cardNickname,
            'cardBrand' => $request->cardBrand,
            "cardBrand" => ucfirst($request->cardBrand),
            "cardLast4" => $request->cardLast4,
            "cardExpMonth" => $request->cardExpMonth,
            "cardExpYear" => $request->cardExpYear,
            "cardType" => $request->cardType
        ]);

        return $result;
    }

    public function getCards(Request $request){
        $uid = $request->uid;

        $result = Http::withHeaders([
            'x-api-key' => 'h3z5uLO9xJxK7Ae6Flq8uYqPIqaPAHuOEuoW4v4r601R9Ez7',
            'uid' => $uid
        ])
        ->post(env("VENTI_API_URL") . "boardingpass/customer/fundingSource/card/list",[
            
        ]);

        $result = json_decode($result,true) ?? [];

        $cards = [];

        foreach($result as $card){
             $card["cardNumber"] = "<span style='font-size:7px; opacity:0.3;'>⬤⬤⬤⬤</span> - <span style='font-size:7px; opacity:0.3;'>⬤⬤⬤⬤</span> - <span style='font-size:7px; opacity:0.3;'>⬤⬤⬤⬤</span> -" . $card["cardLast4"];
            if($card["cardBrand"] == "Amex"){
                $card["cardNumber"] = "<span style='font-size:7px; opacity:0.3;'>⬤⬤⬤⬤</span> - <span style='font-size:7px; opacity:0.3;'>⬤⬤⬤⬤⬤⬤</span> -" . $card["cardLast4"];
            }
            array_push($cards, $card);
        }
    
        return json_encode($cards);
    }

    public function deleteCard(Request $request){
        $user = Session::get('user');

        $result = Http::withHeaders([
            'x-api-key' => 'h3z5uLO9xJxK7Ae6Flq8uYqPIqaPAHuOEuoW4v4r601R9Ez7',
            'uid' => $user->uid
        ])
        ->post(env("VENTI_API_URL") . "boardingpass/customer/fundingSource/remove",[
            "fundingSourceID" => $request->paymentMethodId,
            "sourceType" => "card"
        ]);

        return $result;
    }

    public function track($code){
        // we need to get all subscribers from email octopus that have the user's code as their referral

        $paging = true;
        $i = 1;
        $contacts = [];

        $subscribers =  $this->subscribers();
        $me = null;

        foreach($subscribers as $contact){
            $fields = $contact["fields"];
            // only push those that have a source in their fields

            if(array_key_exists("Source", $fields) && $fields["Source"] != null){

                if($code == getEmailID($contact["id"])){
                    $me = $contact;
                }
                if($fields["Source"] == $code){

                    array_push($contacts,[
                        "email" => $contact["email"],
                        "source" => $fields["Source"]
                    ]);

                    
                }
            }
        }



        $data = [
            "code" => $code,
            "me" => $me,
            "referrals" => $contacts,
            "title" => "Track Your Referrals",
            "description" => "Traveling for free just got whole lot easier.",
            "image" => "https://venti.co/assets/img/boarding-pass-social.jpg",
            "url" => "https://venti.co"
        ];

        return view("boardingpass.tracker", $data);
    }

    public function preview($email){

        // get the referral code of the user based on their email address

        $subscribers = $this->subscribers();

        $key = array_search($email, array_column($subscribers, 'email'));

        $subscriber = $subscribers[$key];
        $referral = getEmailID($subscriber["id"]);

        $data = [
            "referral" => $referral
        ];

        return view("emails.application.boardingpass", $data);
    }

    public function withdrawToBank(Request $request){
        // lots of validation

        // I think withdraws should be instant because it can be difficult to actuate the balance

        // get the wallet balance

        $user = Session::get('user');
        $dwolla = new Dwolla;


        if($request->withdrawSpeed != "standard"){
            //return json_encode(500);
        }

        $validator = Validator::make($request->all(), [
            'withdrawAmount' => 'required|max:8',
            'withdrawSpeed' => 'required',
            'withdrawSource' => 'required',
        ]);

        if ($validator->fails()) {
            return json_encode(500);
        }

        $withdrawAmount = $request->withdrawAmount;

        $fee = 0;

        if($request->withdrawSpeed == "next-available"){
            $fee = 2;
        }

        $total = $withdrawAmount - $fee;

        if($total <= 0){
            // the user entered an invalid value
            return json_encode(501);
        }

        // total represents the amount in their wallet minus the Venti fee
        

        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $auth = app('firebase.auth');

        // make sure the withdraw amount does not exceed the wallet balance
        $customer = $dwolla->getCustomer($user, $auth, $db);
        $wallet = $dwolla->getWallet($customer["customerID"]);
        $walletDetails = $dwolla->getFundingSourceBalance($wallet->_links->self->href);

        $balance = (float) $walletDetails->balance->value;
        

        if($total > $balance){
            // the slick user is trying withdraw $10 (with fee) when the wallet only has $8
            return json_encode(502);
        }


        // get the bank URL

        $bank = $db->collection("Sources")->document($request->withdrawSource)->snapshot()->data();

        if($bank){

            if($customer){
                
                $bankUrl = $bank["source"];
                $transactionID = generateRandomString(12);
                $transfer = $dwolla->withdrawToBank($user, $wallet->_links->self->href, $bankUrl, $transactionID, $total, $request->withdrawSpeed, "Venti Withdraw: $transactionID");

                // store the deposit result into firebase

                if($transfer){
                    // the transfer was successful

                    $transferID = explode("transfers/",$transfer)[1];

                    $data = [
                        "transactionID" => $transactionID,
                        "uid" => $user->uid,
                        "type" => "withdraw",
                        "timestamp" => \Carbon\Carbon::now('UTC')->timestamp, // UTC
                        "date" => \Carbon\Carbon::now('UTC')->format("Y-m-d H:i:s"),
                        "amount" => $request->withdrawAmount,
                        "speed" => $request->withdrawSpeed,
                        "fee" => $fee,
                        "status" => "pending",
                        "from" => "My Boarding Pass",
                        "to" => $bank["name"],
                        "total" => $total,
                        "note" => "Venti Boarding Pass Withdraw",
                        "transferID" => $transferID,
                        "transactionUrl" => $transfer,
                        "env" => env('APP_ENV')
                    ];

                    $bank = $db->collection("Transactions")->document($transferID)->set($data);

                    // send the user an email at this step to confirm

                    // store the fee info

                    // collect the fee

                    $email = Session::get('user')->email;
                    $subject = "[Venti Boarding Pass] Withdraw Confirmation";

                    /*

                    Mail::send('emails.transactions.withdraw-confirmation', array("data" => $data), function($message) use($email, $subject)
                        {
                            $message
                                ->to($email)
                                ->from("no-reply@venti.co","Venti")
                                ->replyTo("no-reply@venti.co")
                                ->subject($subject);
                    });
                    */

                    if($fee != 0){
                        $feeWithdraw = $dwolla->transferFromUserWalletToVentiWallet($user, $wallet->_links->self->href, $fee, "Withdraw: $transactionID");

                        $transferID = explode("transfers/",$feeWithdraw)[1];

                        $data = [
                            "transactionID" => $transactionID,
                            "uid" => $user->uid,
                            "type" => "withdraw",
                            "timestamp" => \Carbon\Carbon::now('UTC')->timestamp, // UTC
                            "date" => \Carbon\Carbon::now('UTC')->format("Y-m-d H:i:s"),
                            "amount" => $fee,
                            "speed" => "next-available", // wallet to wallet is always next available
                            "fee" => 0,
                            "from" => "My Boarding Pass",
                            "status" => "pending",
                            "to" => "Venti Financial",
                            "total" => $fee,
                            "note" => "Withdraw ACH Fee",
                            "transferID" => $transferID,
                            "transactionUrl" => $feeWithdraw,
                            "env" => env('APP_ENV')
                        ];

                        $bank = $db->collection("Transactions")->document($transferID)->set($data);
                    }

                    return json_encode(200);
                }
                else {
                    return json_encode(500);
                }
            }
        }
    }

    public function depositToBoardingPass(Request $request){
        // lots of validation

        if($request->depositAmount > 7500){
            $error = [
                "code" => 500,
                "message" => 'Your request exceeds the $7,500 per transaction limit. Please lower the deposit amount and try again. Error code: 500.'
            ];
            return json_encode($error);  
        }

        if($request->depositSpeed != "standard" && $request->depositSpeed != "next-available"){
            $error = [
                "code" => 501,
                "message" => 'Your request has an invalid transaction speed. Please select "Standard" and try again. Error code: 501.'
            ];
            return json_encode($error);
        }

        $validator = Validator::make($request->all(), [
            'depositAmount' => 'required|min:1|max:5',
            'depositSpeed' => 'required',
            'depositSource' => 'required',
        ]);

        if ($validator->fails()) {
            $error = [
                "code" => 502,
                "message" => 'Your request has an invalid form data. Please try again. Error code: 502.'
            ];
            return json_encode($error);
        }

        // need to verify the deposit amount minus fees is not zero or below zero

        // store the deposit result into firebase

        $fee = 0;
        if($request->depositSpeed == "next-available"){
            $fee = 2;
        }

        $depositAmount = $request->depositAmount;
        $total = (float) $depositAmount - $fee;

        if($total - $fee <= 0){
            // this deposit will not add to the user wallet
            $error = [
                "code" => 503,
                "message" => 'Your request will not result in deposit due to fees. Please enter a higher deposit amount and try again. Error code: 503.'
            ];
            return json_encode($error);
        }

        // get the bank URL
        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $auth = app('firebase.auth');
        $user = Session::get('user');

        $bank = $db->collection("Sources")->document($request->depositSource)->snapshot()->data();

        if($bank){
            $dwolla = new Dwolla;

            $customer = $dwolla->getCustomer($user, $auth, $db);
            if($customer){
                $wallet = $dwolla->getWallet($customer["customerID"]);


                // we need to ensure there is enough funds in the wallet to cover any fees

                
                $walletDetails = $dwolla->getFundingSourceBalance($wallet->_links->self->href);

                $balance = (float) $walletDetails->balance->value;

                if($fee > $balance){
                    // insufficient funds
                    $error = [
                        "code" => 504,
                        "message" => 'Due to fees, this transaction cannot be processed since it exceeds available funds. Please switch to "Standard" for transaction speed and try again. Error code: 504.'
                    ];
                    return json_encode($error);
                }

                $subscription = strtoupper($user->customClaims["subscription"] ?? "BUDDY");
                $depositCap = (int) env("VENTI_" . $subscription . "_DEPOSIT_CAP");

                if($balance >= $depositCap){
                    // reached deposit limit
                    $error = [
                        "code" => 505,
                        "message" => 'Your subscription is limited to a maximum Cash Balance of $' . $depositCap . '. You can lower your Cash Balance by purchasing travel services and Points. Error code: 505.'
                    ];
                    return json_encode($error); 
                }

                $bankUrl = $bank["source"];
                $transactionID = generateRandomString(12); // for customer reference only
                
                $transfer = $dwolla->depositToBoardingPass($user, $wallet->_links->self->href, $bankUrl, $transactionID, $total, $request->depositSpeed, "Deposit: $transactionID");

                if($transfer){
                    // the transfer from the bank was successful
                    // take the fee if there is one


                    $transferID = explode("transfers/",$transfer)[1];

                    $data = [
                        "transactionID" => $transactionID,
                        "uid" => $user->uid,
                        "type" => "deposit",
                        "timestamp" => \Carbon\Carbon::now('UTC')->timestamp, // UTC
                        "date" => \Carbon\Carbon::now('UTC')->format("Y-m-d H:i:s"),
                        "amount" => $request->depositAmount,
                        "speed" => $request->depositSpeed,
                        "fee" => $fee,
                        "from" => $bank["name"],
                        "status" => "pending",
                        "to" => "My Boarding Pass",
                        "total" => $total,
                        "note" => "Venti Boarding Pass Deposit",
                        "transferID" => $transferID,
                        "transactionUrl" => $transfer,
                        "env" => env('APP_ENV')
                    ];

                    $bank = $db->collection("Transactions")->document($transferID)->set($data);

                    // now we need to record the second transaction

                    $email = Session::get('user')->email;
                    $subject = "[Venti Boarding Pass] Deposit Confirmation";

                    /*
                    Mail::send('emails.transactions.deposit-confirmation', array("data" => $data), function($message) use($email, $subject)
                        {
                            $message
                                ->to($email)
                                ->from("no-reply@venti.co","Venti")
                                ->replyTo("no-reply@venti.co")
                                ->subject($subject);
                    });
                    */

                    if($fee != 0){
                        $feeWithdraw = $dwolla->transferFromUserWalletToVentiWallet($user, $wallet->_links->self->href, $fee, "Deposit: $transactionID");

                        $transferID = explode("transfers/",$feeWithdraw)[1];

                        $data = [
                            "transactionID" => $transactionID,
                            "uid" => $user->uid,
                            "type" => "deposit",
                            "timestamp" => \Carbon\Carbon::now('UTC')->timestamp, // UTC
                            "date" => \Carbon\Carbon::now('UTC')->format("Y-m-d H:i:s"),
                            "amount" => $fee,
                            "speed" => "next-available", // wallet to wallet is always next available
                            "fee" => 0,
                            "from" => "My Boarding Pass",
                            "status" => "pending",
                            "to" => "Venti Financial",
                            "total" => $fee,
                            "note" => "Deposit ACH Fee",
                            "transferID" => $transferID,
                            "transactionUrl" => $feeWithdraw,
                            "env" => env('APP_ENV')
                        ];

                        $bank = $db->collection("Transactions")->document($transferID)->set($data);
                    }

                    // send the user an email at this step to confirm

                    return json_encode(200);
                }
                else {
                    // auto fail if unverified
                    $error = [
                        "code" => 506,
                        "message" => 'Our ACH partner, Dwolla, has denied this deposit request. Please try again or contact support via admin@venti.co. Error code: 506.'
                    ];
                    return json_encode($error);
                }
            }
        }

        $error = [
            "code" => 507,
            "message" => 'We do not have record of this bank account. Please contact support via admin@venti.co. Error code: 507'
        ];

        return json_encode($error);
    }

    public function recurringDepositToBoardingPass(Request $request){
        // lots of validation

        if($request->depositAmount > 1000){
            $error = [
                "code" => 500,
                "message" => 'Your request exceeds the $7,500 per transaction limit. Please lower the deposit amount and try again. Error code: 500.'
            ];
            return json_encode($error);  
        }

        if($request->depositSpeed != "standard" && $request->depositSpeed != "next-available"){
            $error = [
                "code" => 501,
                "message" => 'Your request has an invalid transaction speed. Please select "Standard" and try again. Error code: 501.'
            ];
            return json_encode($error);
        }

        $validator = Validator::make($request->all(), [
            'depositAmount' => 'required|numeric|min:25|max:7500',
            'depositFrequency' => 'required',
            'depositStartDate' => 'required||date_format:Y-m-d',
            'depositSpeed' => 'required',
            'depositSource' => 'required',
        ]);

        if ($validator->fails()) {
            $error = [
                "code" => 502,
                "message" => 'Your request has an invalid form data. Please try again. Error code: 502.'
            ];
            return json_encode($error);
        }

        // need to verify the deposit amount minus fees is not zero or below zero

        // store the deposit result into firebase

        $fee = 0;
        if($request->depositSpeed == "next-available"){
            $fee = 2;
        }

        $depositAmount = $request->depositAmount;
        $total = (float) $depositAmount - $fee;

        if($total - $fee <= 0){
            // this deposit will not add to the user wallet
            $error = [
                "code" => 503,
                "message" => 'Your request will not result in deposit due to fees. Please enter a higher deposit amount and try again. Error code: 503.'
            ];
            return json_encode($error);
        }

        // get the bank URL
        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $user = Session::get('user');

        // create the autosave entry

        $autosaveID = generateRandomString(12);

        $db->collection("Autosaves")->document($autosaveID)->set([
            "deleted" => null,
            "start" => Carbon::parse($request->depositStartDate)->timezone("UTC")->timestamp,
            "startDate" => $request->depositStartDate,
            "source" => $request->depositSource,
            "speed" => "standard",
            "frequency" => $request->depositFrequency,
            "amount" => $request->depositAmount,
            "uid" => $user->uid,
            "id" => $autosaveID,
            "createdAt" => Carbon::now('UTC')->timestamp
        ]);


        // send the user an email

        $email = $user->email;
        $subject = "[Boarding Pass] A New Recurring Deposit Was Created";
        $summary = "Starting " . Carbon::parse($request->depositStartDate)->format("F d, Y") . 
        ', we will deduct $' . number_format($request->depositAmount,2) . " from " . $request->bankName . " " . $request->helperText . " until you stop this deposit, a deposit fails to processs, or your maximum Cash Balance will be exceeded.";

        $data = [
            "id" => $autosaveID,
            "summary" => $summary
        ];

        Mail::send('emails.transactions.recurring-deposit', $data, function($message) use($email, $subject)
            {
                $message
                    ->to($email)
                    ->from("no-reply@venti.co","Venti")
                    ->replyTo("no-reply@venti.co")
                    ->subject($subject);
        });

        return json_encode(200);
    }

    public function recurringDepositStatus(Request $request){
        $user = Session::get('user');
        $firestore = app('firebase.firestore');
        $db = $firestore->database();

        $autosaves = $db->collection("Autosaves")
            ->where("uid", "=", $user->uid)
            ->where("deleted","=", null)
            ->documents();

        $autosave = [];

        foreach($autosaves as $autosave){
            $autosave = $autosave->data();
        }

        if(!isset($autosave) || !array_key_exists("source", $autosave)){
            return json_encode(["code" => 400]);
        }

        // get the bank info

        $fundingSourceUrl = env("DWOLLA_URL_" . strtoupper(env("APP_ENV"))) . "/funding-sources/" . $autosave["source"];

        $dwolla = new Dwolla;

        $fundingSource = $dwolla->getFundingSource($fundingSourceUrl);

        $bankName = $fundingSource->name;

        $helper = $summary = "";

        switch ($autosave["frequency"]) {
            case 'everyWeek':
                $helper = "every week";
                break;
            case 'biWeekly':
                $helper = "every other week";
                break;
            case 'semiMonthly':
                $helper = "every 15th and last day of each month";
                break;
            case 'monthly':
                $helper = "every month";
                break;
            default:
                return "";
                break;
        }

        $summary = "ENABLED: ";

        if($autosave["deleted"] != null || $fundingSource->status != "verified"){
            $summary = "DISABLED: ";
        }

        $summary .= 'Depositing <strong>$' . number_format($autosave["amount"], 2) . "</strong> from <strong>$bankName</strong> $helper starting " . Carbon::parse($autosave["start"])->timezone("UTC")->format("F d, Y") . ". You must delete this recurrcing deposit to create a new one.";

        $response = [
            "code" => 200, 
            "summary" => $summary,
            "active" => ($autosave["deleted"] == null) ? true : false,
            "id" => $autosave["id"]
        ];

        return json_encode($response);
    }

    public function recurringDepositDelete(Request $request){
        $user = Session::get('user');
        $firestore = app('firebase.firestore');
        $db = $firestore->database();

        $autosaves = $db->collection("Autosaves")->document($request->id)
            ->update([
                ["path" => "deleted","value" => true]
        ]);

        return json_encode(200);
    }

    public function verifyBank(Request $request){
        $firstDeposit = (int) $request->firstDeposit;
        $secondDeposit = (int) $request->secondDeposit;

        if(
            $firstDeposit > 20 || $firstDeposit < 1 ||
            $secondDeposit > 20 || $secondDeposit < 1
        ){
            // deposit amounts are invalid
            return json_encode(500);  
        }

        $validator = Validator::make($request->all(), [
            'firstDeposit' => 'required|min:1|max:2',
            'secondDeposit' => 'required|min:1|max:2',
            'fundingSourceID' => 'required',
        ]);

        if ($validator->fails()) {
            // failed validation

            return json_encode(500);
        }

        // get the bank URL
        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $auth = app('firebase.auth');
        $user = Session::get('user');

        $bank = $db->collection("Sources")->document($request->fundingSourceID)->snapshot()->data();

        if($bank){
            $dwolla = new Dwolla;
            $verificationCheck =  $dwolla->verifyFundingSource($bank["source"], toPennies($firstDeposit), toPennies($secondDeposit));

            if($verificationCheck){
                return json_encode(200);
            } else{
                // 
                return json_encode(201);
            }
        }

        return json_encode(500);
    }

    public function subscribers(){
        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $auth = app('firebase.auth');

        $subscribersList = $db->collection("VIPS")->documents();

        $subscribers = [];

        foreach($subscribersList as $contact){
            $contact = $contact->data();
            $fields = $contact["fields"];

            array_push($subscribers, [
                "email" => $contact["email_address"],
                "fields" => $fields,
                "id" => $contact["id"],
                "source" => $fields["Source"] ?? "direct",
                "code" => getEmailID($contact["id"])
            ]);
        }

        return $subscribers;
    }

    public function changeSubscription($subscription){
        $auth = app('firebase.auth');
        $user = Session::get('user');
        $user = $auth->getUser($user->uid);
        $customClaims = $user->customClaims;

        $subscriptionID = $customClaims["subscriptionID"] ?? false;

        if($subscriptionID === false){
            // this action should be blocked if they already are subscribed
            $customClaims["subscription"] = $subscription;
            
            $updatedUser = $auth->setCustomUserClaims($user->uid, $customClaims);
            $user = $auth->getUser($user->uid);

            return Redirect::back();
        }

        return Redirect::back();
    }

    public function bankDetails(Request $request){
        $fundingSourceUrl = env("DWOLLA_URL_" . strtoupper(env("APP_ENV"))) . "/funding-sources/" . $request->source;

        $dwolla = new Dwolla;

        $fundingSource = $dwolla->getFundingSource($fundingSourceUrl);

        return json_encode($fundingSource);
    }

    public function getMarketingStatus(Request $request){
            $auth = app('firebase.auth');
            $user = $auth->getUser($request->user);
            $memberID = md5($user->email);

            $url = "https://emailoctopus.com/api/1.6/lists/3b3c3c3a-3b19-11ee-9258-cf345e7859db/contacts/$memberID";

            $result = Http::get($url, [
                'api_key' => env('EMAIL_OCTOPUS_API_KEY')
            ]);

            $result = json_decode($result->body(), true);

            $tags = [];

            if(array_key_exists("status", $result)){
                $tags = $result["tags"];
                if($result["status"] != "SUBSCRIBED"){
                    return false;
                }
            }

            return json_encode($tags);
    }

    public function updateMarketingStatus(Request $request){
        $auth = app('firebase.auth');
        $user = $auth->getUser($request->user);
        $memberID = md5($user->email);

        $url = "https://emailoctopus.com/api/1.6/lists/3b3c3c3a-3b19-11ee-9258-cf345e7859db/contacts/$memberID";

        $tag = $request->tag;
        $enabled = true;
        if($request->enabled == "false"){
            $enabled = false;
        }

        $result = Http::put($url, [
            'api_key' => env('EMAIL_OCTOPUS_API_KEY'),
            "tags" => [
                "$tag"  => $enabled,
            ]
        ]);
    }
}
