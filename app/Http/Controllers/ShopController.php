<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use Kreait\Firebase\Database;
use Kreait\Firebase\Auth;
use Google\Cloud\Firestore\FirestoreClient;
use Kreait\Firebase\Exception\FirebaseException;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Illuminate\Support\Facades\Http;
use Stripe;
use Mail;
use Redirect;

class ShopController extends Controller
{
    public function index(){

        $firestore = app('firebase.firestore');
        $db = $firestore->database();

        $productsList = $db->collection("Catalog")->documents();
        $products = [];

        foreach($productsList as $product){
            array_push($products, $product);
        }

        $data = [
            "title" => "Venti Shop",
            "products" => $products
        ];

        return view('shop.index', $data);
    }

    public function product($productID, $filter = null){

        $firestore = app('firebase.firestore');
        $db = $firestore->database();

        $product = $db->collection("Catalog")->document($productID)->snapshot()->data();
        
        if($product){
            $data = [
                "title" => $product["name"],
                "filter" => $filter,
                "product" => $product
            ];

            return view('shop.item', $data);
        }

        return error('404');

    }

    public function browse(){
        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $auth = app('firebase.auth');
        $docRef = $db->collection("Ideas");
        $ideas = $docRef->listDocuments();



        $results = [];

        foreach($ideas as $idea){
            $result = $idea->snapshot()->data();
            array_push($results, $result);
        }

        $data = [
            "ideas" => $results,
        ];

        return view('store.search', $data);
    }

    public function profile($clientID){
        // lists all trips for a specific seller

        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $auth = app('firebase.auth');
        $client = $db->collection("Clients")->document($clientID)->snapshot()->data();

        $ideas = $db->collection("Ideas")->listDocuments();
        $ideaList = [];

        foreach($ideas as $idea){
            $idea = $idea->snapshot()->data();

            if(isset($idea["clientID"]) && $idea["clientID"] == $client["id"]){
                array_push($ideaList, $idea);
            }
        }

         $data = [
            "title" => $client["name"] . " Listings on Venti",
            "description" => $client["bio"],
            "url" => "https://venti.co/creator/$clientID",
            "image" => $client["avatar"],
            "ideas" => $ideaList,
            "client" => $client
        ];
        
        return view("store.profile", $data);
    }

    public function read($tripID){
        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $auth = app('firebase.auth');
        $docRef = $db->collection("Ideas")->document($tripID);
        $idea = $docRef->snapshot()->data();

        if(is_null($idea)){
            return view("errors.404");
        }

        $client = $db->collection("Clients")->document($idea['clientID'])->snapshot()->data();

                $idea["author"] = $client["name"];
                $idea["authorID"] = $client["id"];
                $idea["authorBio"] = $client["bio"];
                $idea["authorEmail"] = $client["email"];
                $idea["authorImage"] = $client["avatar"];

        $data = [
            "title" => $idea["days"] . "-Day Itinerary for " . $idea["destination"],
            "description" => $idea["title"] . ". " . $idea["description"],
            "url" => "https://venti.co/shop/$tripID",
            "image" => $idea["imageURL"],
            "idea" => $idea,
            "client" => $client
        ];

        return view('store.item', $data);
    }

    public function purchase(Request $request){
        $tripID = $request->tripID;
        $email = $request->email;
        $personlizationChecked = $request->personlizationChecked;
        $stripeToken = $request->stripeToken;
        $cardnumber = $request->cardnumber;
        $cardID = $request->cardID;

        // obfuscate tripID on the page, then unwind the onion

        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $auth = app('firebase.auth');
        $docRef = $db->collection("Ideas")->document($tripID);
        $idea = $docRef->snapshot()->data();
        $country = $idea["country"];

        if($country == "United States of America"){
            $country = "USA";
        }

        $amount = $idea["purchasePrice"];
        $personalizationCost = 0;

        $description = "Itinerary for " . $idea["title"];

        if(array_key_exists("personalizationCost", $idea) && $personlizationChecked == 1){
            $amount = $amount + $idea["personalizationCost"];
            $description = "Itinerary for " . $idea["title"] . " with personalization by " . $idea["author"];
        }

        $paymentAmount = $amount * 100;

        $charge = $this->chargeCardForItinerary($paymentAmount, $stripeToken, $cardnumber, $description, $idea["destination"]);

        if($charge === false || !isset($charge->status) ){
            return Redirect::back()->withInput()->withErrors(["paymentError" => "Card declined"]);
        }

        $paymentDate = $charge->created;

        // grab client details

        // add the trip to payments 

        $client = $db->collection("Clients")->document($idea['clientID'])->snapshot()->data();

        $idea["author"] = $client["name"];
        $idea["authorBio"] = $client["bio"];
        $idea["authorEmail"] = $client["email"];
        $idea["authorImage"] = $client["avatar"];

        // check if user has venti mobile app account

        $user = $auth->getUserByEmail($email);

        if($user != null){
            $uid = $user->uid;

            $itineraryURL = "/$tripID/$uid/$paymentDate";




            $docRef = $db->collection("users/" . $uid . "/Destinations")->document("Trip-" . $tripID . "-" . $paymentDate)->set(
                    [
                        'archived' => 0,
                        'deleted' => 0,
                        'purchased' => 1,
                        'tripID' => $tripID . "-" . $charge->created,
                        'country' => $country,
                        'destination' => $idea["destination"],
                        'days' => $idea["days"],
                        'purchasePrice' => $idea["purchasePrice"],
                        'personalizationCost' => $idea["personalizationCost"],
                        'charged' => $amount,
                        'imageAuthor' => $idea["imageAuthor"],
                        'imageAuthorProfile' => $idea["imageAuthorProfile"],
                        'imageURL' => $idea["imageURL"],
                        'savings' => 0,
                        'transportation' => "flight",
                        'latitude' => $idea["latitude"],
                        'longitude' => $idea["longitude"],
                        'itineraryURL' => $itineraryURL,
                        'departure' => $idea["departure"],
                        'foodBudget' => $idea["foodBudget"],
                        'lodgingBudget' => $idea["lodgingBudget"],
                        'author' => $idea["author"],
                        'authorImage' => $idea["authorImage"],
                        "itineraryNotes" => $idea["itineraryNotes"],
                        'activityBudget' => $idea["activityBudget"]
                    ]
            );
        } else {
            $uid = $email;
        }
        
        $docRef = $db->collection("Payments/users/" . $uid)->document($tripID . "-" . $paymentDate)->set(
            [
                'email' => $email,
                'tripID' => $tripID,
                'title' => $idea["title"],
                'purchasePrice' => $idea["purchasePrice"],
                'personalizationCost' => $idea["personalizationCost"],
                'charged' => $amount,
                'paymentID' => $charge->id,
                'paymentDate' => $charge->created,
                'paymentAmount' => $paymentAmount
            ]
        );
        

        

        $data = [
            "imageURL" => $idea["imageURL"],
            "itineraryURL" => $idea["itineraryURL"],
            "title" => $idea["title"],
            "purchasePrice" => $idea["purchasePrice"],
            "personalizationCost" => $idea["personalizationCost"],
            "total" => $paymentAmount/100,
            "author" => $idea["author"],
            "authorImage" => $idea["authorImage"],
            "itineraryNotes" => $idea["itineraryNotes"],
            "description" => $idea["description"],
            "tripID" => $tripID,
            "userID" => $uid,
            "stampID" => $charge->created
        ];

        Mail::send('emails.purchased.itinerary', array('data' => $data), function($message) use($email)
        {
            $message
                ->to($email)
                ->replyTo("admin@venti.co")
                ->subject('Order Confirmation: Itinerary for Your Trip');
        });


        return Redirect::to("/itinerary/$tripID/$uid/$charge->created");
    }

    public function chargeCardForItinerary($paymentAmount, $stripeToken, $cardnumber, $description, $destination){
        Stripe\Stripe::setApiKey(env('STRIPE_SANDBOX_SECRET'));
        
        $stripe = new \Stripe\StripeClient(
            env('STRIPE_SANDBOX_SECRET')
        );

        $chargeParameters = [
            'amount' => $paymentAmount, 
            'currency' => 'usd',
            'source' => $stripeToken,
            'description' => $description,
            'statement_descriptor' => "Venti: $destination"
        ];

        try{
            $charge = \Stripe\Charge::create($chargeParameters);
            if(isset($charge->id) && $charge->status == "succeeded"){
                return $charge;
            }
        } catch(\Stripe\Exception\CardException $e){
            return $e;
        }

        return false;
    }

    public function confirmation(){
        $tripID = "05050404202216590909";
        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $auth = app('firebase.auth');
        $docRef = $db->collection("Ideas")->document($tripID);
        $idea = $docRef->snapshot()->data();
        $country = $idea["country"];
        $purchasePrice = 10;
        $personalizationCost = 100;
        $charged = 10;

        $itinerary = Http::withHeaders([
            "x-api-key" => env("VENTI_API_KEY"),
            "tripID" => $idea["worldee"]
        ])->post(env("VENTI_API_URL") . "ideas/read/worldee");

        $itinerary = json_decode($itinerary);

        $data = [
            "idea" => $idea,
            "imageURL" => $idea["imageURL"],
            "title" => $idea["title"],
            "personalize" => true,
            "author" => $idea["author"],
            "authorImage" => $idea["authorImage"],
            "itineraryNotes" => $idea["itineraryNotes"],
            "description" => $idea["description"],
            "email" => "markus@venti.co",
            "itinerary" => $itinerary
        ];

        return view("store.confirmation", $data);
    }

    public function viewReceipt($tripID, $userID, $stampID){
        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $auth = app('firebase.auth');
        $docRef = $db->document("Payments/users/" . $userID . "/$tripID-$stampID");
        $trip = $docRef->snapshot()->data();

        if(!isset($trip)){
            return view("errors.404");
        }

        $docRef = $db->collection("Ideas")->document($tripID);
        $idea = $docRef->snapshot()->data();

        // add the trip to payments 

        $client = $db->collection("Clients")->document($idea['clientID'])->snapshot()->data();

        $idea["author"] = $client["name"];
        $idea["authorBio"] = $client["bio"];
        $idea["authorEmail"] = $client["email"];
        $idea["authorImage"] = $client["avatar"];

        $data = [
            "imageURL" => $idea["imageURL"],
            "itineraryURL" => $idea["itineraryURL"],
            "title" => $idea["title"],
            "purchasePrice" => $trip["purchasePrice"],
            "personalizationCost" => $trip["personalizationCost"],
            "total" => $trip["paymentAmount"]/100,
            "author" => $idea["author"],
            "authorImage" => $idea["authorImage"],
            "itineraryNotes" => $idea["itineraryNotes"],
            "description" => $idea["description"],
            "tripID" => $tripID,
            "userID" => $userID,
            "stampID" => $stampID
        ];

        return view("emails.purchased.itinerary", ["data" => $data]);

    }


}
