<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use Kreait\Firebase\Database;
use Kreait\Firebase\Auth;
use Google\Cloud\Firestore\FirestoreClient;
use Kreait\Firebase\Exception\FirebaseException;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Illuminate\Support\Facades\Http;

use Redirect;
use Dwolla;
use Session;
use Validator;
use Mail;
use Exception;

class WalletController extends Controller
{
    public function customerDetails(Request $request){
        // loads data for home page

        $user = Session::get('user');

        if(!isAdmin($user)){
            if($user->uid != $request->user){
                // the person requesting this infor is not an approved admin and is not the owner of this profile
                return json_encode(500);
            }
        }

        
        $balances = $this->getBalances();

        /*

        if($request->wallet == false || $request->wallet == "false"){
            // customer account does not exist
            return json_encode([
                "cashBalance" => 0,
                "cashHTML" => '<h1 style=" color:white; font-size: 235%; margin: 0">$0</h1><span style="font-weight: 500; color: white;">Cash Balance</span>',
                "spendingPowerHTML" => '<h1 style=" color:white; font-size: 235%; margin: 0">$0</h1><span style="font-weight: 500; color: white;">Spending Power</span>',
                "pointsHTML" => '<h1 style=" color:white; font-size: 235%; margin: 0">0</h1><span style="font-weight: 500; color: white;">Points</span>',
                "withdrawButtonHTML" => '<a href="#" data-bs-toggle="modal"><h1><img src="/assets/img/icons/bank.png" style="width: 100%; max-width:55px;"></h1><span style="font-weight: 500;">Withdraw</span>',
                "pointsBalance" => 0,
                "spendingPower" => 0
            ]);
        }

        */

        $wallet = []; //env("DWOLLA_URL_" . strtoupper(env('APP_ENV'))) . "/funding-sources/" . $request->wallet;

        /*

        $dwolla = new Dwolla;

        $walletDetails = $dwolla->getFundingSourceBalance($wallet);

        $wallet = $transactions = [];
        */

        $wallet["balance"] = 0;// (float) $walletDetails->balance->value ?? 0;
        $wallet["last_updated"] = ""; //\Carbon\Carbon::parse($walletDetails->last_updated)->format("m-d-Y H:i:s") ;

        
        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $auth = app('firebase.auth');

        $user = $auth->getUser($request->user);

        $points = $this->getPoints($user, $db);

        $spendingPowerHTML = '<h1 style=" color:white; font-size: 235%; margin: 0">$' . number_format($wallet["balance"] + $points,2) . '</h1><span style="font-weight: 500; color: white;" class="text-small">Spending Power</span>';

        $cashHTML = '<h1 style=" color:white; font-size: 235%; margin: 0">$' . number_format($wallet["balance"],2) . '</h1><span style="font-weight: 500; color: white;" class="text-small">Cash Balance</span>';
        $pointsHTML = '<h1 style=" color:white; font-size: 235%; margin: 0">' . number_format($points,2) . '</h1><span style="font-weight: 500; color: white;" class="text-small">Points</span>';

        $withdrawButtonHTML = '<a href="#" data-bs-toggle="modal" data-bs-target="#withdrawFunds"><h1><img src="/assets/img/icons/bank.png" style="width: 100%; max-width:55px;"></h1><span style="font-weight: 500;">Withdraw</span>
                        </a>';

        $subscription = $user->customClaims["subscription"] ?? "buddy";

        $sweepstakeEntries = getSweepstakeEntries($subscription, $wallet["balance"], $points);

        return json_encode([
            "cashBalance" => $wallet["balance"],
            "cashHTML" => $cashHTML,
            "spendingPowerHTML" => $spendingPowerHTML,
            "pointsHTML" => $pointsHTML,
            "withdrawButtonHTML" => $withdrawButtonHTML,
            "pointsBalance" => $points,
            "spendingPower" => (float) $wallet["balance"] + $points,
            "sweepstakeEntries" => $sweepstakeEntries
        ]);
    }
    public function getBalances(){
        // returns balance, points, and spending power

        $user = Session::get('user');
        $dwolla = new Dwolla;

        // get the customer

        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $auth = app('firebase.auth');
        $wallet = "";
        $balance = 0;

        // make sure the withdraw amount does not exceed the wallet balance
        $customer = $dwolla->getCustomer($user, $auth, $db);
        $points = $this->getPoints($user, $db);

        if($customer == false){
            
            $data = [
                "wallet" => "",
                "balance" => 0,
                "points" => $points
            ];

            return json_encode($data);
        }

        if($customer["status"] == "verified"){

            /*

            $wallet = $dwolla->getWallet($customer["customerID"]);

            $walletDetails = $dwolla->getFundingSourceBalance($wallet->_links->self->href);

            $balance = (float) $walletDetails->balance->value;

            $wallet = $wallet->_links->self->href;

            */

        }

        

        $data = [
            "wallet" => $wallet,
            "balance" => $balance,
            "points" => $points
        ];

        return json_encode($data);

    }

    public function getPoints($user = null, $db = null){

        if(!$user){
            $user = Session::get('user');
            $firestore = app('firebase.firestore');
            $db = $firestore->database();
        }

        $transactions = [];

        $transactionDocs = $db->collection("Transactions");
         $transactionDocs = $transactionDocs
                ->where("uid","=",$user->uid)
                ->documents();
            // I need a separate firebase instance for sandboxing

            foreach($transactionDocs as $transactionDoc){
                array_push($transactions,  $transactionDoc->data());
            }

        return getPointsBalance($transactions);
    }

    public function cancelRequest(Request $request){
        $cancelLink = $this->checkCancelability($request->transferID, $request->transactionID);


        if($cancelLink){

            $cancelLink = explode("transfers/", $cancelLink)[1];

            return json_encode(["cancelationLink" => $cancelLink, "transferID" => $request->transferID]);
        }

        // check to make sure I own this transaction

        return json_encode(false);
        
    }

    public function cancelConfirm(Request $request){
        $dwolla = new Dwolla;
        $cancel = $dwolla->cancelTransaction($request->cancelationLink);

        if($cancel){
            return json_encode(200);
        }

        return json_encode(500);
    }

    public function checkCancelability($transferID, $transactionID = null){
        $user = Session::get('user');
        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $auth = app('firebase.auth');

        $transactionDocs = $db->collection("Transactions");
        $transactionDocs = $transactionDocs
        ->where("uid","=",$user->uid)
        ->where("transactionID","=",$transactionID)
        ->documents();

        $transactionDoc = false;

        foreach($transactionDocs as $transactionDoc){

            $transactionDoc = $transactionDoc->data();
        }

        $transactionCancelLink = false;

        if($transactionDoc){
            $dwolla = new Dwolla;

            $transactionCancelLink = $dwolla->getCancel($transactionDoc["transferID"]) ?? false;
        }

        return $transactionCancelLink;
    }

    public function purchasePoints(Request $request){
        // when the user purchases something, we log it here

        $user = Session::get('user');
        $firestore = app('firebase.firestore');
        $db = $firestore->database();

        $validator = Validator::make($request->all(), [
            'warpAmount' => 'required',
            'termsWarp' => 'required',
        ]);

        if ($validator->fails() || $request->warpAmount < 1) {
            // not all required fields presented
            return json_encode(501);
        }

        $canPurchase = $this->canPointsPurchasePoints($user->uid, $db);

        if(!$canPurchase){
            return json_encode(502);
        }

        // get the user cash balance

        $balance = json_decode($this->getBalances(),true);

        $cashBalance = $balance["balance"];
        $warpAmount = $request->warpAmount;

        if($warpAmount > ($cashBalance * 0.25)){
            // request exceeds the allowed amount
            return json_encode(500);
        }

        // time to process the transaction

        $pointsToAward = $warpAmount * 0.9; // reducee to 0.95
        $customerWallet = $balance["wallet"];

        $dwolla = new Dwolla;

        $transfer = $dwolla->transferFromUserWalletToVentiWallet($user, $customerWallet, $warpAmount, "points-purchase");

        if($transfer){
            // the transfer succeeded. Time to record to firebase

            $firestore = app('firebase.firestore');
            $db = $firestore->database();
            $auth = app('firebase.auth');

            $transactionID = generateRandomString(12);

            $transferID = explode("transfers/",$transfer)[1];

            // we recorded the withdraw.

            $data = [
                "transactionID" => $transactionID,
                "uid" => $user->uid,
                "type" => "withdraw",
                "timestamp" => \Carbon\Carbon::now('UTC')->timestamp, // UTC
                "date" => \Carbon\Carbon::now('UTC')->format("Y-m-d H:i:s"),
                "amount" => $warpAmount,
                "speed" => "next-available",
                "fee" => 0,
                "status" => "complete",
                "from" => "My Boarding Pass",
                "to" => "Venti",
                "total" => $warpAmount,
                "note" => "Points Purchase",
                "transferID" => $transferID,
                "transactionUrl" => $transfer,
                "env" => env('APP_ENV')
            ];

            $bank = $db->collection("Transactions")->document($transferID)->set($data);

            // now record the deposit

            $transactionID2 = generateRandomString(12);

            $data = [
                "transactionID" => $transactionID2,
                "uid" => $user->uid,
                "type" => "points-deposit",
                "timestamp" => \Carbon\Carbon::now('UTC')->timestamp, // UTC
                "date" => \Carbon\Carbon::now('UTC')->format("Y-m-d H:i:s"),
                "amount" => $pointsToAward,
                "speed" => "next-available",
                "fee" => 0,
                "status" => "complete",
                "from" => "Venti",
                "to" => "My Boarding Pass",
                "total" => $pointsToAward,
                "note" => "Points Purchase: $transactionID",
                "transferID" => $transferID, // it's wise to link this with the previous transfer
                "transactionUrl" => null,
                "env" => env('APP_ENV')
            ];

            $bank = $db->collection("Transactions")->document($transactionID2)->set($data);

            // send receipt

            $email = $user->email;
            $subject = "[Venti Boarding Pass] Receipt for Points Purchase";
            
                try{
                    Mail::send('emails.transactions.points-receipt', array("data" => [
                        "amount" => $warpAmount,
                        "pointsToAward" => $pointsToAward
                    ]), function($message) use($email, $subject)
                    {
                            $message
                                ->to($email)
                                ->from("no-reply@venti.co","Venti")
                                ->replyTo("no-reply@venti.co")
                                ->bcc("admin@venti.co")
                                ->subject($subject);
                    });
                }
                catch(Exception $e){

                }
                
            return json_encode(200);
        }

        return json_encode(503);
    }

    public function canPointsPurchasePoints($uid, $db, $transactions = null){
        $transactions = $db->collection("Transactions")
            ->where("type","=","points-deposit")
            ->where("uid","=",$uid)
            ->orderBy("timestamp","DESC")
            ->documents();

        $now = \Carbon\Carbon::now();

        foreach($transactions as $transaction){
            $transaction = $transaction->data();
            if(str_contains($transaction["note"], "Points Purchase") && $transaction["from"] == "Venti"){
                $transactionDif = \Carbon\Carbon::parse($transaction["timestamp"])->diffInDays($now);
                if($transactionDif < 7){
                    return false;
                }
            }
        }

        return true;
    }

    public function sharePoints(Request $request){
        $email = $request->pointsRecipient;
        $amount = $request->pointsToSend;

        $fee = round($amount * .02,2);
        $amount = $amount - $fee; // we take a 2% fee

        // we need to check to see if the email belongs to a user

        $firestore = app('firebase.firestore');
        $auth = app('firebase.auth');

        $validator = Validator::make($request->all(), [
            'pointsToSend' => 'required',
            'pointsRecipient' => 'required',
        ]);

        if ($validator->fails() || $request->pointsToSend <= 0.1) {
            // not all required fields presented
            return json_encode(504);
        }

        try{
            $user = $auth->getUserByEmail($email);
            // check to see if the user has a plan)
            $customClaims = $user->customClaims;
            $subscription = $customClaims["subscription"] ?? "buddy";
            $subscriptionID = $customClaims["subscriptionID"] ?? false;

            if(!$subscriptionID){
                // the customer has not finished onboarding
                return json_encode(501);
            }

            $sender = Session::get('user');

            if($sender->uid == $user->uid){
                // can't send to yourself
                return json_encode(502);
            }

            $db = $firestore->database();

            // check the points balance of the recipient if they are buddy pass holder

            if($subscription == "buddy"){
                $points = $this->getPoints($user, $db);

                if($amount + $points > 250){
                    // the buddy pass holder will exceed their maximum balance
                    return json_encode(503);
                }
            }

            // check to make sure the points being sent does not exceed my points

            $myPoints = $this->getPoints(Session::get('user'), $db);

            if($request->pointsToSend > $myPoints){
                return json_encode(504);
            }

            // process the transaction from my perspective
            $transactionCol = $db->collection("Transactions");
            $transactionID = generateRandomString(12);
            $name = getFirstName($user->displayName);

            $data = [
                "transactionID" => $transactionID,
                "uid" => $sender->uid,
                "type" => "points-withdraw",
                "timestamp" => \Carbon\Carbon::now('UTC')->timestamp, // UTC
                "date" => \Carbon\Carbon::now('UTC')->format("Y-m-d H:i:s"),
                "amount" => $request->pointsToSend,
                "speed" => "next-available",
                "fee" => $fee,
                "status" => "complete",
                "from" => "My Boarding Pass",
                "to" => $name,
                "total" => $amount,
                "note" => "Points Transfer: $transactionID",
                "transferID" => null,
                "transactionUrl" => null,
                "recipient" => $user->uid,
                "sender" => $sender->uid,
                "env" => env('APP_ENV'),
                "taxable" => true // points are taxable when converted to cash equivalent value. In this case, the recipient is earning 
            ];

            $bank = $db->collection("Transactions")->document($transactionID)->set($data);

            $transactionID2 = generateRandomString(12);

            $name = getFirstName($sender->displayName);

            $data = [
                "transactionID" => $transactionID2,
                "uid" => $user->uid,
                "type" => "points-deposit",
                "timestamp" => \Carbon\Carbon::now('UTC')->timestamp, // UTC
                "date" => \Carbon\Carbon::now('UTC')->format("Y-m-d H:i:s"),
                "amount" => $amount,
                "speed" => "next-available",
                "fee" => 0,
                "status" => "complete",
                "from" => $name,
                "to" => "My Boarding Pass",
                "total" => $amount,
                "note" => "Points Transfer: $transactionID2",
                "transferID" => $transactionID,
                "transactionUrl" => null,
                "recipient" => $user->uid,
                "sender" => $sender->uid,
                "env" => env('APP_ENV')
            ];

            $bank = $db->collection("Transactions")->document($transactionID2)->set($data);

            // email the recipient

            $subject = "[Boarding Pass] Your Received $amount Points";
            $title = "You've Got Points!";
            $email = $user->email;
            

            $data = [
                "title" => $title,
                "amount" => $amount,
                "description" => "You received $amount Points from $name with confirmation ID: $transactionID2.
                <br><br>
                Points help you buy down the cost of flights, hotels, and more on the Venti platform by up to 100%. Use the link below to check your balance, make deposits, open Mystery Boxes, and more."
            ];

             Mail::send('emails.transactions.admin-transact', array("data" => $data), function($message) use($email, $subject)
                {
                    $message
                        ->to($email)
                        ->from("no-reply@venti.co","Venti")
                        ->replyTo("no-reply@venti.co")
                        ->subject($subject);
            });

            return json_encode(200);

        }
        catch(FirebaseException $e){
            // the user does not exist

            return json_encode(500);
        }


    }

}
