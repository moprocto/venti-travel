<?php

namespace App\Http\Controllers;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Carbon\Carbon as Carbon;


class ItineraryController extends Controller
{
    public function browse(){
        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $auth = app('firebase.auth');
        $docRef = $db->collection("Ideas");
        $ideas = $docRef->listDocuments();
        $results = [];

        foreach($ideas as $idea){
            array_push();
        }
    }

    public function read(){

    }

    public function edit(){

    }

    public function add(){

    }

    public function delete(){

    }

    public function secureEmbed($userID, $tripID, $width, $height){
        $data = [
            "tripID" => $tripID,
            "width" => $width,
            "height" => $height
        ];

        $html = file_get_contents("https://www.worldee.com/trip/detail?tripId=368327");
        $html = str_replace('href="/client/', 'href="https://www.worldee.com/client/', $html);
        $html = str_replace("src='/client", "src='https://www.worldee.com/client", $html);
        $html = str_replace("src='/js", "src='https://www.worldee.com/js", $html);
        $html = str_replace("https://www.worldee.com/client/js/app.aa992f851bdcd059a1d9.js", "/js/worldee/app.js", $html);
        $html = str_replace("/temp/css", "https://www.worldee.com/temp/css", $html);
        //$html = str_replace("https://www.worldee.com/client/js/scripts.aa992f851bdcd059a1d9.js", "/js/worldee/scripts.js", $html);

        echo $html;
        

        //return view("embed", $data);
    }

    public function secureView($tripID, $userID, $stampID, $month = null, $day = null, $year = null, Request $request){
        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $auth = app('firebase.auth');
        $docRef = $db->document("Payments/users/" . $userID . "/$tripID-$stampID");
        $trip = $docRef->snapshot()->data();

        

        if(!isset($trip)){
            return view("errors.404");
        }

        $docRef = $db->collection("Ideas")->document($tripID);
        $idea = $docRef->snapshot()->data();

        $personalize = false;

        if($trip["paymentAmount"] = $trip["purchasePrice"] + $trip["personalizationCost"]){
            $personalize = true;
        }

        if($month == null || $day == null || $year == null){
            $start = $idea["departure"];
        } else {
            $start = "$month/$day/$year";
        }

        $itinerary = Http::withHeaders([
            "x-api-key" => env("VENTI_API_KEY"),
            "tripID" => $idea["worldee"]
        ])->post(env("VENTI_API_URL") . "ideas/read/worldee");

        $itinerary = json_decode($itinerary);

        $client = $db->collection("Clients")->document($idea['clientID'])->snapshot()->data();

        $data = [
            "idea" => $idea,
            "imageURL" => $idea["imageURL"],
            "title" => $idea["title"],
            "personalize" => $personalize,
            "client" => $client,
            "itineraryNotes" => $idea["purchaseNote"],
            "description" => $idea["description"],
            "email" => $trip["email"],
            "itinerary" => $itinerary,
            "start" => $start,
            "tripID" => $tripID,
            "stampID" => $stampID,
            "userID" => $userID,
            "hide" => true,
            "embed" => $request->embed,
            "story" => $request->story,
            "preview" => false
        ];

        return view("store.confirmation", $data);
    }

    public function securePreview($trip, Request $request){
        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $auth = app('firebase.auth');
        $docRef = $db->document("Ideas/$trip");
        $idea = $docRef->snapshot()->data();

        $itinerary = Http::withHeaders([
            "x-api-key" => env("VENTI_API_KEY"),
            "tripID" => $idea["worldee"]
        ])->post(env("VENTI_API_URL") . "ideas/preview/worldee");

        $itinerary = json_decode($itinerary);

        $client = $db->collection("Clients")->document($idea['clientID'])->snapshot()->data();

        $data = [
            "idea" => $idea,
            "imageURL" => $idea["imageURL"],
            "title" => $idea["title"],
            "personalize" => null,
            "client" => $client,
            "itineraryNotes" => $idea["purchaseNote"],
            "description" => $idea["description"],
            "email" => null,
            "itinerary" => $itinerary,
            "start" => $idea["departure"],
            "tripID" => $trip,
            "stampID" => null,
            "userID" => null,
            "hide" => true,
            "embed" => $request->embed,
            "story" => $request->story,
            "preview" => true
        ];

        return view("store.confirmation", $data);
    }

    public function preview($trip, Request $request){
        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $auth = app('firebase.auth');
        $docRef = $db->document("Ideas/$trip");
        $idea = $docRef->snapshot()->data();

        $itinerary = Http::withHeaders([
            "x-api-key" => env("VENTI_API_KEY"),
            "tripID" => $idea["worldee"]
        ])->post(env("VENTI_API_URL") . "ideas/read/worldee");

        $itinerary = json_decode($itinerary);

        $client = $db->collection("Clients")->document($idea['clientID'])->snapshot()->data();

        $data = [
            "idea" => $idea,
            "imageURL" => $idea["imageURL"],
            "title" => $idea["title"],
            "personalize" => null,
            "client" => $client,
            "itineraryNotes" => $idea["purchaseNote"],
            "description" => $idea["description"],
            "email" => null,
            "itinerary" => $itinerary,
            "start" => $idea["departure"],
            "tripID" => $trip,
            "stampID" => null,
            "userID" => null,
            "hide" => true,
            "embed" => $request->embed,
            "story" => $request->story,
            "preview" => true
        ];

        return view("store.confirmation", $data);
    }
}
