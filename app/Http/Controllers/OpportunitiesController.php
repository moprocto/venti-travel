<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class OpportunitiesController extends Controller
{
    public function index(){
        $data = [
            "title" => "Employment Opportunities | Jobs Board",
            "description" => "Jobs, internships, and other employment opportunities with companies and organizations that sponsor",
            "url" => "https://venti.co/opportunities",
            "image" => "https://venti.co/assets/img/navigator-backdrop-t7.jpg",
        ];

        return view('opportunities.index', $data);
    }
}
