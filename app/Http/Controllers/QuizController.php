<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use Illuminate\Support\Str;

class QuizController extends Controller
{
    public function index(){
        $data = [
            "title" => "What's Your Travel Personality?",
            "description" => "A travel personality describes who you are and what you prefer when you're on a trip regardless of destination. Take this short quiz to find out what your travel personality is!",
            "image" => "https://venti.co/assets/img/friends-hiking.jpg",
            "url" => "https://venti.co/quiz"
        ];

        return view("quiz.index", $data);
    }

    public function read(Request $request){
        $auth = app('firebase.auth');
        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $result = $db->collection("Quizzes")->document($request->personality)->snapshot()->data();

        if($result == null){
            return view('errors.404');
        }

        $users = $auth->listUsers($defaultMaxResults = 1000, $defaultBatchSize = 1000);
        $usersList = [];
        $i = 1;
        foreach($users as $user){
            if(isProfileComplete($user)[0] == true && isRecommendable($user)){
                array_push($usersList,[
                    "displayName" => getFirstName($user->displayName),
                    "photoUrl" => $user->photoUrl,
                    "interests" => $user->customClaims["interests"],
                    "languages" => $user->customClaims["languages"] ?? ["English"],
                    "username" => $user->customClaims["username"],
                    "bio" => $user->customClaims["bio"],
                    "age" => calculateAge($user->customClaims["dob"])
                ]);  
            }
        }

        shuffle($usersList);

        $header = getQuizEmoji(strtolower($result['cat1'] . "_" . $result['cat2']));

        $data = [
            "header" => $header[0],
            "description" => $header[1],
            "users" => $usersList
        ];

        return view("quiz.results", $data);
    }

    public function save(Request $request){

        $auth = app('firebase.auth');
        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $user = "guest";

        if(Session::get('user') !== null){
            $user = Session::get('user')->uid;
        }

       

        $quizID = Str::random(12);

        $docRef = $db->collection("Quizzes")->document($quizID)->set(
            [             
                "cat1" => $request->cat1,
                "cat2" => $request->cat2,
                "peaceful" => $request->peaceful,
                "spending" => $request->spending,
                "taboo" => $request->taboo,
                "classic" => $request->classic,
                "adventure" => $request->adventure,
                "uid" => $user,
                "quizID" => $quizID,
                "createdAt" => Date("Y-m-d")
            ]
        );

        return json_encode($quizID);
    }

    public function getUsers(){

    }
}
