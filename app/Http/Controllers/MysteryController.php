<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Kreait\Firebase\Exception\FirebaseException;
use Illuminate\Support\Facades\Http;
use Carbon\Carbon as Carbon;
use Carbon\CarbonPeriod;
use Session;
use Redirect;
use Validator;
use Mail;
use Exception;


class MysteryController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('firebase.auth');
    }

    public function index(){
        $opens = [];
        $opens["opens24Hours"] = 0;
        $user = Session::get('user');
        try{
            $opens = Http::withHeaders([
                'x-api-key' => 'h3z5uLO9xJxK7Ae6Flq8uYqPIqaPAHuOEuoW4v4r601R9Ez7',
                'uid' => $user->uid
            ])
            ->post(env("VENTI_API_URL") . "boardingpass/customer/transactions/mysterybox/opens",[
                    "env" => "production"
            ]);
            $opens = json_decode($opens->body(), true);
        }
        catch(Exception $e){

        }

        $data = [
            "title" => "Mystery Box",
            "user" => $user,
            "opens" => $opens
        ];

        return view("boardingpass.mystery", $data);
    }

    public function open(Request $request){
        $wager = $request->wager;
        $user = Session::get('user');  

        $result = Http::withHeaders([
                'x-api-key' => 'h3z5uLO9xJxK7Ae6Flq8uYqPIqaPAHuOEuoW4v4r601R9Ez7',
                'uid' => $user->uid
            ])
        ->post(env("VENTI_API_URL") . "boardingpass/customer/transactions/mysterybox",[
                "wager" => $wager,
                "env" => "production"
        ]);

        return json_decode($result,true);  
    }

    public function mysterySimulator(){
        $results = [];
        $revenue = $beat = $profit = $lost = $loss = 0;
        $games = 10000;
        for($i = 1; $i <= $games; $i++){
            $wagers = [0,1,3,5];
            $wager = $wagers[0];
            $reward = $this->openMysteryBox($wager);
            array_push($results, [
                "wager" => ($wager == 0) ? 1 : $wager, 
                "result" => $reward, 
                "profit" => (($wager == 0) ? 1 : $wager) - $reward["prize"]
            ]);
            $revenue += $wager;
        }

        $dud = $dragon = $captain = $hightide = $lowtide = $gold = $silver = $bronze= $maincabin = $economyplus = $tenX = $fiveX = $double = $winback = 0;
        
        foreach($results as $result){
            
            if($result["result"]["tier"] == 0){
                // won nothing
                $dud++;
                $beat++;
                $profit += $result["wager"];
            } else {
                if($result['profit'] == 0){
                    $lost++;
                }
                
                if($result['profit'] < 0){
                    // won something
                    $loss += $result["profit"];
                } else {
                    $beat++;
                    $profit += $result["profit"];
                }

                if($result["result"]["tier"] == "dragon"){
                    $dragon++;
                }
                if($result["result"]["tier"] == "captain"){
                    $captain++;
                }
                if($result["result"]["tier"] == "High Tide"){
                    $hightide++;
                }
                if($result["result"]["tier"] == "Low Tide"){
                    $lowtide++;
                }
                if($result["result"]["tier"] == "gold"){
                    $gold++;
                }
                
                if($result["result"]["tier"] == "silver"){
                    $silver++;
                }

                if($result["result"]["tier"] == "bronze"){
                    $bronze++;
                }
                if($result["result"]["tier"] == "Main Cabin"){
                    $maincabin++;
                }
                if($result["result"]["tier"] == "Economy Plus"){
                    $economyplus++;
                }

                if($result["result"]["tier"] == "10xWin"){
                    $tenX++;
                }
                if($result["result"]["tier"] == "quintupleWin"){
                    $fiveX++;
                }
                if($result["result"]["tier"] == "doubleWin"){
                    $double++;
                }
                if($result["result"]["tier"] == "winBackOdds"){
                    $winback++;
                }
            }

            $profit += $result['profit'];
        }

        dd([
            "Games" => $games,
            "Gross Revenue" => $revenue,
            "Games Where House Wins" => $beat,
            "Games Where House Loses" => $lost,
            "Dollar Value of Losses" => $loss,
            "Profit from Games" => $profit, 
            "dragon: 20,000" => $dragon,
            "captain: 10,000" => $captain,
            "High Tide: 5,000" => $hightide,
            "Low Tide: 2,500" => $lowtide,
            "gold: 2,000" => $gold,
            "silver: 1,000" => $silver,
            "bronze" => $bronze,
            "Main Cabin" => $maincabin,
            "Economy Plus" => $economyplus,
            "10xWin" => $tenX,
            "5xWin" => $fiveX,
            "2xWin" => $double,
            "1xWin" => $winback,
            "dud" => $dud
        ]);
    }
    public function openMysteryBox($betAmount) {
        $oddsAmount = 500000;

        // Define the odds, including win back, double, triple, and quintuple win odds
        $odds = [];
        for($i = 1; $i <= 5; $i+=2){
            $odds[$i] = [
                'dragon' => $oddsAmount / $i, // 20000
                'captain' => ($oddsAmount / ($i*(2**1))), // 10000
                'hightide' => ($oddsAmount/ ($i*(2**2))), // 5000
                'lowtide' => ($oddsAmount / ($i*(2**3))), // 2500
                'gold' => ($oddsAmount / ($i*2**4)), // 2000
                'silver' => ($oddsAmount / ($i*2**5)), // 1000
                'bronze' => ($oddsAmount / ($i*2**6)), // 500
                'maincabin' => ($oddsAmount / ($i*2**7)), // 250
                'economyplus' => ($oddsAmount / ($i*2**8)), // 150
                'tenXOdds' => $i == 5 ? 128 : ($i == 3 ? 192 : 256), // 50
                'quintupleWinOdds' => $i == 5 ? 32 : ($i == 3 ? 48 : 64),
                'doubleWinOdds' => $i == 5 ? 8 : ($i == 3 ? 12 : 16),
                'winBackOdds' => $i == 5 ? 2 : ($i == 3 ? 3 : 4)
            ];
        }

        $dragonPrize = 20000; // Grand Prize amount
        $captainPrize = 10000;
        $hightidePrize = 5000;
        $lowtidePrize = 2500;
        $goldPrize = 2000;
        $silverPrize = 1000;
        $bronzePrize = 500;
        $maincabinPrize = 250;
        $economyplusPrize = 200;

        if($betAmount == 0){
            $betAmount = 1;
        }

        // Validate the bet amount
        if (!isset($odds[$betAmount])) {
            return "Invalid bet amount. Please bet $1, $3, or $5.";
        }

        $dragon = mt_rand(1, $odds[$betAmount]['dragon']);
        $captain = mt_rand(1, $odds[$betAmount]['captain']);
        $hightide = mt_rand(1, $odds[$betAmount]['hightide']);
        $lowtide = mt_rand(1, $odds[$betAmount]['lowtide']);
        $gold = mt_rand(1, $odds[$betAmount]['gold']);
        $silver = mt_rand(1, $odds[$betAmount]['silver']);
        $bronze = mt_rand(1, $odds[$betAmount]['bronze']);
        $maincabin = mt_rand(1, $odds[$betAmount]['maincabin']);
        $economyplus = mt_rand(1, $odds[$betAmount]['economyplus']);
        $tenXOdds = mt_rand(1, $odds[$betAmount]['tenXOdds']);
        $quintupleWinOdds = mt_rand(1, $odds[$betAmount]['quintupleWinOdds']);
        $doubleWinOdds = mt_rand(1, $odds[$betAmount]['doubleWinOdds']);
        $winBackOdds = mt_rand(1, $odds[$betAmount]['winBackOdds']);
        
        

        if($doubleWinOdds == 1){
                return ["tier" => "doubleWin", "prize" => $betAmount * 2, "odds" => $odds[$betAmount]["doubleWinOdds"]];
        }

        if($dragon == 1){
                return ["tier" => "dragon", "prize" => $dragonPrize, "odds" => 100/$odds[$betAmount]["dragon"]];
        }
        if($captain == 1){
                return ["tier" => "captain", "prize" => $captainPrize, "odds" => 100/$odds[$betAmount]["captain"]];
        }
        if($hightide == 1){
                return ["tier" => "High Tide", "prize" => $hightidePrize, "odds" => 100/$odds[$betAmount]["hightide"]];
        }
        if($lowtide == 1){
                return ["tier" => "Low Tide", "prize" => $lowtidePrize, "odds" => 100/$odds[$betAmount]["lowtide"]];
        }
        if($gold == 1){
                return ["tier" => "gold", "prize" => $goldPrize, "odds" => 100/$odds[$betAmount]["gold"]];
        }
        if($silver == 1){
                return ["tier" => "silver", "prize" => $silverPrize, "odds" => 100/$odds[$betAmount]["silver"]];
        }
        if($bronze == 1){
                return ["tier" => "bronze", "prize" => $bronzePrize, "odds" => 100/$odds[$betAmount]["bronze"]];
        }
        if($maincabin == 1){
                return ["tier" => "Main Cabin", "prize" => $maincabinPrize, "odds" => 100/$odds[$betAmount]["maincabin"]];
        }
        if($economyplus == 1){
                return ["tier" => "Economy Plus", "prize" => $economyplusPrize, "odds" => 100/$odds[$betAmount]["economyplus"]];
        }
        if($tenXOdds == 1){
                return ["tier" => "10xWin", "prize" => $betAmount * 10, "odds" => $odds[$betAmount]["tenXOdds"]];
        }
        if($quintupleWinOdds == 1){
                return ["tier" => "quintupleWin", "prize" => $betAmount * 5, "odds" => $odds[$betAmount]["quintupleWinOdds"]];
        }
        
        if($winBackOdds == 1){
                return ["tier" => "winBackOdds", "prize" => $betAmount, "odds" => $odds[$betAmount]["winBackOdds"]];
        }

        return ["tier" => 0, "prize" => 0, "odds" => [1/$odds[$betAmount]["winBackOdds"], "1:" . $odds[$betAmount]["winBackOdds"]]]; // No win
    }

    public function opens(Request $request){
        $user = Session::get('user');  

        $result = Http::withHeaders([
                'x-api-key' => 'h3z5uLO9xJxK7Ae6Flq8uYqPIqaPAHuOEuoW4v4r601R9Ez7',
                'uid' => $user->uid
            ])
        ->post(env("VENTI_API_URL") . "boardingpass/customer/transactions/mysterybox/opens",[
                "env" => "production"
        ]);

        return $result;
    }
}
