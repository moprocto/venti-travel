<?php

namespace App\Http\Controllers;

use Plaid;
use Illuminate\Http\Request;
use Carbon\Carbon as Carbon;
use Session;
use DateTime;

class PlaidController extends Controller
{
    protected $plaidService;

    public function __construct(Plaid $plaidService)
    {
        $this->plaidService = $plaidService;
    }

    public function index(){
        $data = [
            "title" => "Link New Bank Account"
        ];

        return view("pro.plaid");
    }

    public function createLinkToken()
    {
        $linkTokenResponse = $this->plaidService->createLinkToken();
        return response()->json($linkTokenResponse);
    }

    public function exchangeToken(Request $request)
    {
        $publicToken = $request->input('public_token');
        $exchangeResponse = $this->plaidService->exchangePublicToken($publicToken);

        // Store the access token securely
        $accessToken = $exchangeResponse->access_token;

        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $auth = app('firebase.auth');
        $user = Session::get('user');

        // get the bank name and accounts accessible

        $bank = $this->getBank($accessToken);

        $accounts = [];

        $startDate = new DateTime(Carbon::now()->subDays(30)->format('Y-m-d')); // Transactions from the past 30 days
        $endDate = new DateTime(Carbon::now()->format('Y-m-d'));

        $transactionAccounts = $this->plaidService->getTransactions($accessToken, $startDate, $endDate, null);

        $transactionsCollection = collect($transactionAccounts->transactions ?? []);

        // if we can't get transactions, we'll just stop here

        $Taccounts = $transactionAccounts->accounts ?? [];

        $timestamp = Carbon::now('UTC')->timestamp;

        foreach($Taccounts as $account){
            $account = (array) $account;
            if(array_key_exists("account_id", $account)){

                $bvid = generateRandomString(12);

                $db->collection("PlaidSubaccounts")->document($bvid)->set([
                    'uid' => $user->uid,
                    'vid' => $bvid,
                    "account_id" => $account["account_id"],
                    "mask" => $account["mask"],
                    "name" => $account["name"],
                    "official_name" => $account["official_name"],
                    "persistent_account_id" => $account["persistent_account_id"] ?? null,
                    "subtype" => $account["subtype"],
                    "type" => $account["type"],
                    "balances" => $account["balances"],
                    'institution_id' => $bank['institution_id'],
                    'access_token' => $accessToken,
                    'item_id' => $exchangeResponse->item_id,
                    'created_at' => $timestamp
                ]);

                // save transactions into separate table
                $transactions = $transactionsCollection->where('account_id', $account["account_id"])->all();

                foreach($transactions as $transaction){
                    $tvid = generateRandomString(12);
                    $db->collection("PlaidTransactions")->document($tvid)->set((array)$transaction);
                }
            }
        }

        $vid = generateRandomString(12);

        $save = $db->collection("Plaids")->document($vid)->set([
            'uid' => $user->uid,
            'vid' => $vid,
            'access_token' => $accessToken,
            'item_id' => $exchangeResponse->item_id,
            'created_at' => $timestamp,
            'institution_id' => $bank['institution_id'],
            'institution' => $bank['institution']->name,
            "boardingpass" => false
        ]);

        // add this account to dwolla. But we need to get the 

        return response()->json([
            'vid' => $vid,
            'access_token' => $accessToken,
            'item_id' => $exchangeResponse->item_id,
        ]);
    }

    public function getBalance($accessToken)
    {
        $balanceResponse = $this->plaidService->getBalance($accessToken);
        return $balanceResponse;
    }

    public function getBank($accessToken){
        $bankDetails = $this->plaidService->getItem($accessToken);
        $institution = $this->plaidService->getBankName($bankDetails->item->institution_id);
    
        return ["institution" => $institution, "institution_id" => $bankDetails->item->institution_id];

    }

    public function connectToBoardingPass(Request $request){
        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $auth = app('firebase.auth');
        $user = Session::get('user');

        // 
    }

    public function getFirstTransactionDate($accessToken, $accountId)
    {
        // Fetch transactions for a wide date range to find the earliest transaction
        $startDate = new \DateTime('2000-01-01'); // Arbitrary early date
        $endDate = new \DateTime(); // Current date

        $transactionsResponse = $this->getTransactions($accessToken, $accountId, $startDate, $endDate);

        if (!empty($transactionsResponse->transactions)) {
            // Find the earliest transaction date
            $earliestTransaction = min(array_column($transactionsResponse->transactions, 'date'));
            return new \DateTime($earliestTransaction);
        }

        return null; // No transactions found
    }
}