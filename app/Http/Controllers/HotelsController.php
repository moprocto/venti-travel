<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Session;
use Location;
use Redirect;
use Mail;
use Exception;
use Route;
use View;
use Dwolla;
use Validator;

use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use Kreait\Firebase\Database;
use Kreait\Firebase\Auth;
use Google\Cloud\Firestore\FirestoreClient;
use Kreait\Firebase\Exception\FirebaseException;

use Carbon\Carbon as Carbon;
use Carbon\CarbonPeriod;

use Stripe\Stripe as Stripe;
use \Stripe\StripeClient as StripeClient;
use Algolia\AlgoliaSearch\SearchClient;

class HotelsController extends Controller
{
    public function index(Request $request){

        if($request->fbid == "fb"){
            Session::put('referrer', "facebook");
        }
        elseif($request->fbid == "ig"){
            Session::put('referrer', "instagram");
        }
        elseif($request->fbid == "msg"){
            Session::put('referrer', "messenger");
        }
        elseif($request->fbid == "adwords"){
            Session::put('referrer', "adwords");
        }
        elseif(strlen($request->fbid) > 12){
            Session::put('referrer', $request->fbid);
        }
        if(Session::get('fbclid') === null){
            Session::put('fbclid', $request->fbclid);
        }

        $data = [
            "title" => "Book Hotels with Venti",
            "description" => "Get hotels up to 99% off",
            "priorSearch" => Session::get('hotel-search') ?? []
        ];

        if(env('APP_ENV') == "local"){
            // get the IP address
            $ip = \Request::ip();
            if($ip != "68.100.62.8" && $ip != "127.0.0.1"){
                //return Redirect::to('https://venti.co/flights');
            }
        }

        return view('hotels.index', $data);
    }

    public function searchResults(Request $request){

        $rooms = $request->rooms;
        $lat = $request->lat;
        $long = $request->long;
        $start = $request->start;
        $end = $request->end; 
        $adults = $request->adults;
        $children = $request->children;

        $data = [
            "title" => "Hotels in " . $request->to,
            "request" => $request->all(),
            "priorSearch" => Session::get('hotel-search'),
            "city" => $request->city,
            "lat" => $lat,
            "long" => $long,
            "path" => "?city=" . $request->to . "&lat=$lat&long=$long&start=$start&end=$end&rooms=$rooms&adults=$adults&children=$children"
        ];

        // insert this query into the Firebase

        try{
            $firestore = app('firebase.firestore');
            $db = $firestore->database();
            $user = Session::get('user');
            $ip = \Request::ip();
            $location = Location::get($ip);
            $agent = $request->header('User-Agent');
            $region = "none";
            $timestamp = Carbon::now('UTC')->timestamp;

            if(isset($location) && !is_bool($location)){
                $region = $location->regionName;
            }

            $uid = null;

            if($user !== null){
                $uid = $user->uid;
            }

            // for each query, we will store a record for big data

            $search = $request->all();
            $queryID = generateRandomString(12);

            $search["session_id"] = $queryID;
            
            Session::put('hotel-search', $search);
            $data["priorSearch"] = $search;

            $insertQuery = $db->collection("HotelSearches")->document($queryID)->set([
                "request" => $request->all(),
                "queryID" => $queryID,
                "region" => $region,
                "ip" => $ip,
                "uid" => $uid,
                "timestamp" => $timestamp,
                "agent" => $agent,
                "session_id" => $queryID
            ]);

        }
        catch(FirebaseException $e){

        }

        return view("hotels.results", $data);
    }

    public function search(Request $request){
        // four months

        // get the data from duffel

        $url = "https://api.duffel.com/stays/search";

        $start = Carbon::parse($request->start)->format("Y-m-d");
        $end = Carbon::parse($request->end)->format("Y-m-d");

        $numAdults = (int) $request->adults;
        $numChildren = (int) $request->children;
        $guests = [];

        for($i = 1; $i <= $numAdults; $i++){
            array_push($guests, ["type" => "adult"]);
        }

        for($i = 1; $i <= $numChildren; $i++){
            array_push($guests, ["type" => "child", "age" => "11"]);
        }

        $data = json_encode([
            "data" => [
                "rooms" => $request->rooms,
                "location" => [
                    "radius" => 5,
                    "geographic_coordinates" => [
                        "longitude" => $request->long,
                        "latitude" => $request->lat
                    ]
                ],
                "check_out_date" => $end,
                "check_in_date" => $start,
                "guests" => $guests
            ]
        ]);


        $hotels = $this->curlNewOffer($url, $data);

        for($i = 0; $i < sizeof($hotels); $i++){
            if(!array_key_exists("accommodation", $hotels[$i])){
                unset($hotels[$i]);
            }
        }

        // sort hotels from least to most expensive

        usort($hotels, function($a, $b) {
            return $a["accommodation"]["cheapest_rate_total_amount"] <=> $b["accommodation"]["cheapest_rate_total_amount"];
        });

        $cards = [];
        $order = 1;
        
        foreach($hotels as $hotel){
            $data = $hotel;
            $data["order"] = $order;

            // skip hotels if with incomplete information

            if(
                $data["accommodation"]['review_score'] == null ||
                $data["accommodation"]['rating'] == null ||
                $data["accommodation"]['photos'] == null
            ){
                continue;
            }

            $data["showPricing"] = true;
            $data["imgBlock"] = true;
            
            $html = View::make('hotels.components.hotel-result', compact('data'))->render();

            // pricing data
                $price = $data["accommodation"]["cheapest_rate_total_amount"];
                $discountedPriceMultiplier = getDiscountPercent("hotel", $price, $data["accommodation"]["cheapest_rate_currency"]);
                $discountedPrice = $price * $discountedPriceMultiplier;
                $totalSavings = $price - $discountedPrice;
                $totalSavingsPercent = (($discountedPrice - $price) / $price) * 100;
                // Create Carbon instances for the check-in and check-out dates
                $checkIn = Carbon::createFromFormat('Y-m-d', $data["check_in_date"]);
                $checkOut = Carbon::createFromFormat('Y-m-d', $data["check_out_date"]);

                // Calculate the number of nights
                $numberOfNights = $checkIn->diffInDays($checkOut);

                // Ensure there's at least one night
                $numberOfNights = max($numberOfNights, 1);

                // Calculate the cost per night
                $costPerNight = round($price / $numberOfNights,2);

                $discountedCostPerNight = round($discountedPrice / $numberOfNights,2);
            //

            array_push($cards, [
                "name" => $data["accommodation"]["name"],
                "photo" => $data["accommodation"]["photos"][0]["url"] ?? "/assets/img/icons/hotel.png",
                "lowestPrice" => $discountedCostPerNight,
                "totalSavings" => $totalSavings,
                "offerID" => $data['id'],
                "accommodationID" => $data["accommodation"]["id"],
                "lat" => $data["accommodation"]["location"]["geographic_coordinates"]["latitude"] ?? null,
                "long" => $data["accommodation"]["location"]["geographic_coordinates"]["longitude"] ?? null,
                "html" => $html,
                "msrpd" => $discountedPriceMultiplier
            ]);
            $order++;
        }

        

        return json_encode($cards);
    }

    public function verifyHotel(Request $request){

        $id = $request->searchID;

        $url = "https://api.duffel.com/stays/search_results/$id/actions/fetch_all_rates";

        $headers = ['Application-Type:application/json','Accept-Encoding:gzip','Accept:application/json','Duffel-Version:v1','Content-Type:application/json',"Authorization:Bearer " . env('DUFFEL_API_KEY')];

        // A sample PHP Script to POST data using cURL

        // Prepare new cURL resource
        $crl = curl_init($url);
        curl_setopt($crl, CURLOPT_ENCODING , "gzip");
        curl_setopt($crl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($crl, CURLINFO_HEADER_OUT, true);
        curl_setopt($crl, CURLOPT_POST, true);

        
        // Set HTTP Header for POST request 
        curl_setopt($crl, CURLOPT_HTTPHEADER, $headers);

        // Submit the POST request
        $result = curl_exec($crl);

        $result = json_decode($result, true);

        if(!array_key_exists("data", $result)){
            $this->blockHotel($request->accommodationID);
            return json_encode(500);
        }

        return json_encode(200);
    }

    public function blockHotel($accommodationID){
        // block this hotel from ever being seen again

        $firestore = app('firebase.firestore');
        $db = $firestore->database();

        $accommodations = $db->collection("BlockedHotels")->document("List")->snapshot()->data();

        array_push($accommodations, $accommodationID);

        $accommodations = $db->collection("BlockedHotels")->document("List")->set($accommodations);

    }

    public function getHotelAjax(Request $request){
        return $this->getHotel($request->id, $request->hotel, true);
    }

    public function getHotel($id, $hotel, $ajax = null){
        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $result = $this->curlOfferCheckout($id);

        if(!$result){
            // 404
            // block this hotel
            $block = $this->blockHotel($hotel);

            if($ajax){

                return json_encode(400);
            }

            
            return view('errors.404-h');
        }

        if($ajax){
            return json_encode(200);
        }

        $data = [
            "title" => $result["accommodation"]["name"],
            "hotel" => $result
        ];

        // session update

        $searchSession = Session::get('hotel-search');

        if($searchSession !== null){
            $sessionQueryDoc = $db->collection("HotelSearches")->document($searchSession["session_id"]);
            $sessionQuery = $sessionQueryDoc->snapshot()->data();

            $selections = $sessionQuery["preselectedOffers"] ?? [];

            $price = $result["accommodation"]["cheapest_rate_total_amount"];

            array_push($selections, [
                "offer" => $id,
                "amount" => $price,
                "discountPresented" => getDiscountPercent("hotel", $price, $result["accommodation"]["cheapest_rate_currency"]),
            ]);

            $sessionQueryDoc->update([
                ["path" => "selections","value" => $selections]
            ]);
        }

        return view("hotels.offer", $data);
    }

    public function getCheckout($id, $rate){
        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $result = $this->curlCheckoutQuote($rate);
        $searchSession = Session::get('hotel-search');

        if(!$result){
            // 404
            return view('errors.404');
        }

        $data = [
            "title" => "Checkout: " . $result["accommodation"]["name"],
            "hotel" => $result["accommodation"],
            "quote" => $result
        ];

        $wallet = $customer = $transactable = false;
        $user = Session::get('user'); 

        $cards = [];

        if($user !== null){
            $dwolla = new Dwolla;
            $firestore = app('firebase.firestore');
            $db = $firestore->database();
            $auth = app('firebase.auth');

            $customer = $dwolla->getCustomer($user, $auth, $db);

            if($customer){
                // they have a customer record

                $fundingSources = []; //$dwolla->fundingSources($customer["customerID"]);

                if($fundingSources){
                    foreach($fundingSources as $fundingSource){
                        if(!$fundingSource->removed 
                            && isset($fundingSource->bankAccountType)
                            && $fundingSource->status == "verified"
                        ){
                            // this user can now make deposits and withdrawals using their bank account

                            $transactable = true;
                        }
                        if($fundingSource->type == "balance"){
                            $wallet = explode("funding-sources/",$fundingSource->_links->self->href)[1];
                        }
                    }

                    // account balances
                }
            }

            $cardDocs = $db->collection("Cards")
            ->where("uid", "=", $user->uid)
            ->where("deletedAt","=", null)
            ->documents();

            foreach($cardDocs as $card){
                $card = $card->data();
                array_push($cards, $card);
            }
        }

        $data["wallet"] = $wallet;
        $data["transactable"] = $transactable;
        $data["customer"] = $customer;
        $data["user"] = $user;
        $data["orderID"] = $id;
        $data["rateID"] = $rate;

        // cards

        

        $data["cards"] = $cards;

        Session::put('currentRate', $result);

        // track the rate id that made it to checkout

        if(isset($searchSession)){
            $sessionQueryDoc = $db->collection("HotelSearches")->document($searchSession["session_id"]);
            $sessionQuery = $sessionQueryDoc->snapshot()->data();

            $selections = $sessionQuery["checkoutOffers"] ?? [];

            $price = $result["total_amount"];


            array_push($selections, [
                "offer" => $id,
                "amount" => $price,
                "discountPresented" => getDiscountPercent("hotel", $price, $result["total_currency"]),
            ]);

            $sessionQueryDoc->update([
                ["path" => "selections","value" => $selections]
            ]);
        }

        return view("hotels.checkout", $data);

    }

    public function curlNewOffer($url, $data){

        $headers = ['Application-Type:application/json','Accept-Encoding:gzip','Accept:application/json','Duffel-Version:v1','Content-Type:application/json',"Authorization:Bearer " . env('DUFFEL_API_KEY')];

        // A sample PHP Script to POST data using cURL

        // Prepare new cURL resource
        $crl = curl_init($url);
        curl_setopt($crl, CURLOPT_ENCODING , "gzip");
        curl_setopt($crl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($crl, CURLINFO_HEADER_OUT, true);
        curl_setopt($crl, CURLOPT_POST, true);
        curl_setopt($crl, CURLOPT_POSTFIELDS, $data);
        
        // Set HTTP Header for POST request 
        curl_setopt($crl, CURLOPT_HTTPHEADER, $headers);

        // Submit the POST request
        $result = curl_exec($crl);

        $result = json_decode($result, true) ?? [];
        
        if(!array_key_exists("data", $result)){
            // this query did not return any data for some reason.
            return $result;
        }

        $results = $result["data"]["results"] ?? [];

        // filter out blocked hotels

        $hotels = [];
        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $blockedHotels = $hotelDoc = $db->collection("BlockedHotels")->document("List")->snapshot()->data();

        foreach($results as $hotel){
            $accommodationID = $hotel["accommodation"]["id"];
            
            if(!in_array($accommodationID, $blockedHotels)){
                // hotel is not blocked
                array_push($hotels, $hotel);
            }
        }

        return $hotels;
    }

    public function curlOfferCheckout($id){
        $url = "https://api.duffel.com/stays/search_results/$id/actions/fetch_all_rates";

        $headers = ['Application-Type:application/json','Accept-Encoding:gzip','Accept:application/json','Duffel-Version:v1','Content-Type:application/json',"Authorization:Bearer " . env('DUFFEL_API_KEY')];

        // A sample PHP Script to POST data using cURL

        // Prepare new cURL resource
        $crl = curl_init($url);
        curl_setopt($crl, CURLOPT_ENCODING , "gzip");
        curl_setopt($crl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($crl, CURLINFO_HEADER_OUT, true);
        curl_setopt($crl, CURLOPT_POST, true);

        
        // Set HTTP Header for POST request 
        curl_setopt($crl, CURLOPT_HTTPHEADER, $headers);

        // Submit the POST request
        $result = curl_exec($crl);

        $result = json_decode($result, true);

        if(!array_key_exists("data", $result)){
            // this query did not return any data for some reason.

            return false;
        }

        return $result["data"];
    }

    public function curlCheckoutQuote($id){
        $headers = ['Application-Type:application/json','Accept-Encoding:gzip','Accept:application/json','Duffel-Version:v1','Content-Type:application/json',"Authorization:Bearer " . env('DUFFEL_API_KEY')];

        $url = "https://api.duffel.com/stays/quotes";

        $data = json_encode([
            "data" => [
                "rate_id" => $id
            ]
        ]);

        // Prepare new cURL resource
        $crl = curl_init($url);
        curl_setopt($crl, CURLOPT_ENCODING , "gzip");
        curl_setopt($crl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($crl, CURLINFO_HEADER_OUT, true);
        curl_setopt($crl, CURLOPT_POST, true);
        curl_setopt($crl, CURLOPT_POSTFIELDS, $data);
        
        // Set HTTP Header for POST request 
        curl_setopt($crl, CURLOPT_HTTPHEADER, $headers);

        // Submit the POST request
        $result = curl_exec($crl);

        $result = json_decode($result, true) ?? false;


        
        if(!array_key_exists("data", $result)){
            // this query did not return any data for some reason.
            return false;
        }


        return $result["data"];
    }

    public function updateSelection(Request $request){
        $searchSession = Session::get('hotel-search');

        $selectedAmount = $request->selectedAmount;
        $selectedOffer = $request->selectedOffer;
        $msrpd = $request->msrpd;

        $firestore = app('firebase.firestore');
        $db = $firestore->database();

        $sessionQueryDoc = $db->collection("HotelSearches")->document($searchSession["session_id"]);
        $sessionQuery = $sessionQueryDoc->snapshot()->data();

        $selections = $sessionQuery["selections"] ?? [];

        array_push($selections, [
            "offer" => $selectedOffer,
            "amount" => $selectedAmount,
            "discountPresented" => $msrpd,
        ]);

        $sessionQueryDoc->update([
            ["path" => "selections","value" => $selections]
        ]);
    }
    
    public function submitOrder(Request $request){

        // * VALIDATION * //

        $user = Session::get('user');
        $destination = $request->destination ?? "";
        $paymentPref = $request->paymentPref;

        if($user === null){
            // session expired
            return json_encode(400);
        }

        $validator = Validator::make($request->all(), [
            'order' => 'required|max:255',
            'guests' => 'required',
            'quote' => 'required|max:255'
        ]);

        if ($validator->fails()) {
            // required data is missing
            $error = [
                "error" => 401,
                "message" => "We are missing critical information needed to process this order."
            ];

            return json_encode($error);
        }

        $offer = Session::get('currentRate');

        if($offer === null){
            // the offer has expired
            $error = [
                "error" => 402,
                "message" => "Unfortunately, this offer has expired. This happens when you did not submit your order fast enough, or the hotel rescinded this order. Try refreshing the page. If that does not work, please rebuild your search."
            ];

            return json_encode($error);
        }

        // we need to get the wallet of the user to check their balance

        $dwolla = new Dwolla;
        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $auth = app('firebase.auth');
        $wallet = $customer = $transactable = $bookingSucceeded = $intent = false;

        $customer = $dwolla->getCustomer($user, $auth, $db);
        $customerWallet = "";

        if($customer){
            // they have a customer record

            // we need to make sure they are not suspended

            if($customer["status"] == "suspended" || $customer["status"] == "deactivated"){
                $error = [
                    "error" => 403,
                    "message" => "Suspended and deactivated accounts cannot place orders."
                ];

                return json_encode($error);
            }

            $fundingSources = $dwolla->fundingSources($customer["customerID"]);

            if($fundingSources){
                foreach($fundingSources as $fundingSource){
                    if(!$fundingSource->removed 
                        && isset($fundingSource->bankAccountType)
                        && $fundingSource->status == "verified"
                    ){
                        // this user can now make deposits and withdrawals using their bank account

                        $transactable = true;
                    }
                    if($fundingSource->type == "balance"){
                        $wallet = $fundingSource->_links->self->href;
                        $customerWallet = $fundingSource->_links->self->href;
                    }
                }
            }

        } else {
            // customer record does not exist. ABORT!
            // the offer has expired
            $error = [
                "error" => 403,
                "message" => "This order can only be processed by active and verified Boarding Pass holders. Please contact admin@venti.co if you believe this is an error."
            ];

            return json_encode($error);
        }

        // we need to make sure there's enough funds to cover the cost of the purchase and the points being requested 

        $wallet = $dwolla->getFundingSourceBalance($wallet);

        if(!$wallet){
            $error = [
                "error" => 405,
                "message" => "A valid Boarding Pass is required to book hotels on Venti"
            ];

            return json_encode($error);
        }

        $wallet["balance"] = (float) $wallet->balance->value;

        // the amount the customer is asked to pay with addons

        $offer = Session::get('currentRate');

        $total = convertCurrency($offer["total_amount"], $offer["total_currency"]);

        $pointsApplied = (float) $request->pointsApplied ?? 0;

        $discountedPriceMultiplier = getDiscountPercent("hotel", $total, $offer["total_currency"]);
        $discountedPrice = $total * $discountedPriceMultiplier;
        $maxPoints = $total - $discountedPrice;

        $transactions = [];

        $transactionDocs = $db->collection("Transactions");
        $transactionDocs = $transactionDocs
            ->where("uid","=",$user->uid)
            ->documents();

        foreach($transactionDocs as $transactionDoc){
            array_push($transactions,  $transactionDoc->data());
        }

        // 
        $points = getPointsBalance($transactions);

        if($points < $pointsApplied){
            // more rounding issues
            if(($points - $pointsApplied) < 0.01){

                $pointsApplied = bcdiv((float) $points, 1, 2);
            }
        }

        if($points < $pointsApplied){
            // the user is trying to apply more points than they actually have
            $error = [
                "error" => 405,
                "message" => "Insufficient Points on your Boarding Pass. Please purchase more points via Warp Drive or by waiting a bit longer for your next disbursement. Otherwise, insert a lesser amount and try again."
            ];

            return json_encode($error);
        }

        if($pointsApplied > $maxPoints && ($pointsApplied - $maxPoints > 0.01)){
            // the user is trying to apply more points than what is allowed for this transaction
            $error = [
                "error" => 406,
                "message" => "You've exceeded the maximum amount of Points allowed for this order. Please insert a lesser amount and try again."
            ];

            return json_encode($error);
        }

        $amountDue = $total - $pointsApplied;

        if($amountDue > $wallet["balance"] && $paymentPref == "pass"){
            // the user is trying to spend more Cash than what is in their cash balance
            $error = [
                "error" => 407,
                "message" => "The cost of this order exceeds the Cash Balance of your Boarding Pass. Please deposit more funds. Otherwise, shop for a more affordable accommodation and try again."
            ];

            return json_encode($error);
        }

        $ccFee = 0;
        $from = "My Boarding Pass";

        if($amountDue > 0.01 && $paymentPref != "pass"){
            // the customer is paying with credit card
            $card = $db->collection("Cards")->document($paymentPref)->snapshot()->data();
            $ccFee = bcdiv($amountDue * .03, 1, 2);
            $amountDue += $ccFee; // adding credit card fee
            // create payment intent
            $owner = substr($offer["accommodation"]["name"] ?? "Venti Hotel",0,22);
            $description = "$owner";
            $metadata = [
                'description' => "Hotel Reservation " . $description,
                'mcc' => '7011', // Airlines MCC
            ];
            $intent = \Stripe::createPaymentIntent($amountDue * 100, $paymentPref, $card["customerID"], $description, $metadata);

            if($intent && isset($intent->id)){
                $transferID = $transfer = $intent->id;
                $nickname = $card["cardNickname"] ?? false;
                if(!$nickname){
                    $from = $card["cardBrand"] . " Card (". $card["cardLast4"] .")" ;
                }else{
                    $from = $nickname . ": " . $card["cardBrand"] . " (". $card["cardLast4"] .")" ;
                }

            }
            else {
                $error = [
                    "error" => 407,
                    "message" => "We are unable to use this card to process your transaction. " . $intent . " If you believe this was an error, you can contact your bank. Otherwise, please try a different payment method."
                ];

                return json_encode($error);
            }
        }

        // PLACE THE ORDER

        $post_data = json_encode([
            "data" => [
                "quote_id" => $request->quote,
                "phone_number" => $user->customClaims["phoneNumber"],
                "loyalty_programme_account_number" => $request->programme,
                "guests" => $request->guests,
                "email" => $user->email,
                "accommodation_special_requests" => $request->accommodation_special_requests
            ]
        ]);

        $headers = ['Application-Type:application/json','Accept-Encoding:gzip','Accept:application/json','Duffel-Version:v1','Content-Type:application/json',"Authorization:Bearer " . env('DUFFEL_API_KEY')];

        $url ="https://api.duffel.com/stays/bookings";

        $crl = curl_init($url);
        curl_setopt($crl, CURLOPT_ENCODING , "gzip");
        curl_setopt($crl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($crl, CURLINFO_HEADER_OUT, true);

        curl_setopt($crl, CURLOPT_POST, true);
        curl_setopt($crl, CURLOPT_POSTFIELDS, $post_data);
        curl_setopt($crl, CURLOPT_HTTPHEADER, $headers);

        $headers = [];

        // Submit the POST request
        $result = curl_exec($crl);

        $resultRaw = json_decode($result,true);
        $result = json_decode($result,true);

        if(array_key_exists("data",$result)){
            $result = $result["data"];
            $bookingSucceeded = true;
            // ** BY THIS POINT THE HOTEL HAS BEEN BOOKED AND HELD FOR PAYMENT ** //

            $amountDue = round($amountDue,2); // dwolla will not allow more than two decimal places

            if($amountDue > 0.01 && $paymentPref == "pass"){
                $transfer = $dwolla->transferFromUserWalletToVentiWallet($user, $customerWallet, $amountDue, "hotel-purchase");

                if(!$transfer){
                    // the transfer was not successful. This could be due to the customer account being frozen.

                    $error = [
                        "error" => 408,
                        "message" => "Our ACH payments provider, Dwolla, blocked this transaction. It may be due for a variety of reasons. Please contact admin@venti.co."
                    ];

                    return json_encode($error);
                }

                $transferID = explode("transfers/",$transfer)[1];
            }

            

            // ** BY THIS POINT THE CUSTOMER HAS PAID CASH FOR THE HOTEL ** //

            $transactionID = generateRandomString(12);
            

            $refID = $result["id"];
            $booking_reference = $result["reference"];

            // all the dates will be the same to guarantee they are properly included in the same statement
            $timestamp = \Carbon\Carbon::now('UTC')->timestamp; // UTC
            $date = \Carbon\Carbon::now('UTC')->format("Y-m-d H:i:s");

            $name = $result["accommodation"]["name"];

            if(isset($transferID)){
                $data = [
                "transactionID" => $transactionID,
                "uid" => $user->uid,
                "type" => "withdraw",
                "timestamp" => $timestamp,
                "date" => $date,
                "amount" => $amountDue,
                "speed" => "next-available",
                "fee" => $ccFee,
                "status" => "complete",
                "from" => $from,
                "to" => "Venti",
                "total" => $amountDue,
                "note" => "Hotel Purchase $name | REF# $booking_reference",
                "transferID" => $transferID,
                "transactionUrl" => $transfer ?? null,
                "refID" => $refID,
                "env" => env('APP_ENV')
            ];

            $purchase = $db->collection("Transactions")->document($transferID)->set($data);
            }

            

            if($pointsApplied > 0){

                // points were used to complete this purchase

                $transactionID2 = generateRandomString(12);

                $data = [
                    "transactionID" => $transactionID2,
                    "uid" => $user->uid,
                    "type" => "points-withdraw",
                    "timestamp" => $timestamp,
                    "date" => $date,
                    "amount" => $pointsApplied,
                    "speed" => "next-available",
                    "fee" => 0,
                    "status" => "complete",
                    "from" => "My Boarding Pass",
                    "to" => "Venti",
                    "total" => $pointsApplied,
                    "note" => "Hotel Purchase $name | REF# $booking_reference",
                    "transferID" => $transferID ?? null, // it's wise to link this with the previous transfer
                    "transactionUrl" => null,
                    "env" => env('APP_ENV'),
                    "refID" => $refID,
                    "taxable" => true // points are taxable when converted to cash equivalent value
                ];

                $pointsDeduction = $db->collection("Transactions")->document($transactionID2)->set($data);
            }

            // RECORD THE ORDER //

            $searchSession = Session::get('hotel-search');
            $sessionID = null;

            if($searchSession !== null){
                $sessionID = $searchSession["session_id"];
            }

            $data = [
                "transactionID" => $transactionID, // this connects the order
                "uid" => $user->uid,
                "timestamp" => $timestamp,
                "date" => $date,
                "type" => "hotel",
                "amount" => $amountDue,
                "points" => $pointsApplied,
                "order" => json_encode($result),
                "booking_reference" => $booking_reference,
                "refID" => $refID, // we will need this value to query duffel
                "sessionID" => $sessionID, // tracking which session led to this booking,
                "accommodation_special_requests" => $request->accommodation_special_requests,
                "from" => $from
            ];

            $saveOrder = $db->collection("Orders")->document($refID)->set($data);


            // SEND EMAIL CONFIRMATION

            $email = $user->email;
            $owner = $result["accommodation"]["name"];
            $subject = "[Boarding Pass] Booking $booking_reference Confirmed with $owner";

            $timezone = Session::get('user')->customClaims["timezone"] ?? "America/New_York";

            // due at accomodation needs to be in the email


            $rate = $result["accommodation"]["rooms"][0]["rates"][0];
            $guests = $result["guests"];
            $owner = $result["accommodation"]["name"];
            $address = $result["accommodation"]["location"]["address"];

            $addressString = "";
            $conditionsString = "";
            $cancelationTimelines = [];
            $cancelation = $rate["cancellation_timeline"] ?? false;
            $conditions = $rate["conditions"] ?? [];

            if(is_array($cancelation)){
                $cancelationTimelines = array_merge($cancelationTimelines, $rate["cancellation_timeline"]);
            }

            foreach($conditions as $condition){
                if($condition["description"] != null){
                    $conditionsString .= "<h3>" . $condition["title"] . "</h3><p>" . $condition["description"] . "</p>";
                }
            }

            foreach($address as $key => $value){
                $addressString .= $value . "<br>";
            }

            
            
            if($cancelation == null){
                $cancelation = [];
            }


            $data = [
                "total_amount" => $rate["total_amount"],
                "user" => $user,
                "paidAmount" => $amountDue,
                "pointsApplied" => $pointsApplied,
                "order" => $refID,
                "transactionID" => $transactionID,
                "booking_reference" => $booking_reference,
                "guests" => $guests,
                "timestamp" => $timestamp,
                "timezone" => $timezone,
                "date" => $date,
                "refID" => $refID,
                "base_amount" => $rate["base_amount"],
                "tax_amount" => $rate["tax_amount"],
                "fee_amount" => $rate["fee_amount"],
                "due_at_accommodation_amount" => $rate["due_at_accommodation_amount"],
                "name" => $owner,
                "address" => $address,
                "addressString" => $addressString,
                "adults" => sizeof($result["guests"]),
                "rooms" => $result["rooms"],
                "accommodation" => $result["accommodation"],
                "check_in_date" => $result["check_in_date"],
                "check_out_date" => $result["check_out_date"],
                "id" => $refID,
                "showPricing" => false,
                "imgBlock" => false,
                "accommodation_special_requests" => $request->accommodation_special_requests ?? null,
                "conditionsString" => $conditionsString,
                "from" => $from
            ];
            
            try{
                Mail::send('emails.transactions.hotel-purchase', array("data" => $data), function($message) use($email, $subject)
                    {
                        $message
                            ->to($email)
                            ->from("no-reply@venti.co","Venti")
                            ->replyTo("no-reply@venti.co")
                            ->bcc("admin@venti.co")
                            ->subject($subject);
                });
            }
            catch(Exception $e){
                // the email was not fired.
                
            }

            $success = [
                "success" => 200,
                "message" => "Success! Your booking with $owner has been submitted for processing. We've also sent a confirmation email to $email. This page will automatically redirect in 10 seconds.",
                "order_id" => $refID
            ];

            return json_encode($success);
        }

        $errorMessageString = "Unfortunately, this offer has expired or was rescinded by the provider.";

        $error = [
            "error" => 402,
            "message" => $errorMessageString
        ];

        if($intent && !$bookingSucceeded){
            // refunding the charge due to failed booking
            $refund = \Stripe::refundPayment($intent);



            if($refund){
                // successfully reversed the charge
                $errorMessageString = $errorMessageString . " We reversed the transaction on your card. Please contact admin@venti.co for support with this booking before trying again.";
                $error = [
                        "error" => 410,
                        "message" => $errorMessageString
                ];
            } else {
                $errorMessageString = $errorMessageString . " We were unable to reverse the transaction with your card. Please contact admin@venti.co for support with this booking.";
                $error = [
                        "error" => 410,
                        "message" => $errorMessageString
                ];
            }
        }

        return json_encode($error);
        
    }
}
