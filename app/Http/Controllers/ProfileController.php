<?php

namespace App\Http\Controllers;

use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use Kreait\Firebase\Database;
use Kreait\Firebase\Auth;
use Kreait\Firebase\Contract\Storage;
use Illuminate\Http\Request;
use Kreait\Firebase\Exception\FirebaseException;
use Http;
use Session;
use Redirect;
use Image;
use Input;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Twilio\Rest\Client;
use Twilio\Exceptions\RestException as TwilioRestException;
use Mail;
use Validator;

class ProfileController extends Controller
{
    public function __construct(){
        $this->middleware('firebase.auth');
    }

    public function getProfile(){

        $auth = app('firebase.auth');
        $user = Session::get('user');
        $user = $auth->getUserByEmail($user->email);
        Session::put('user', $user);

        $verificationComplete = false;
        $emailVerified = $user->emailVerified;
        $smsVerified = false;
        $interests = [];

        
        if(array_key_exists("smsVerified", $user->customClaims)){
            $smsVerified = $user->customClaims["smsVerified"];
        }

        if($emailVerified === true && $smsVerified === true){
            $verificationComplete = true;
        }

        if(isset($user->customClaims["interests"])){
            $interests = $user->customClaims["interests"];
        }

        $data = [
            "user" => $user,
            "verificationComplete" => $verificationComplete,
            "smsVerified" => $smsVerified,
            "interests" => $interests
        ];

        return view("users.profile", $data);
    }

    public function editProfile(Request $request){
        
        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $auth = app('firebase.auth');
        $user = Session::get('user');
        $user = $auth->getUserByEmail($user->email);
        $mediaLink = $request->pic;
        $smsVerified = $user->customClaims["smsVerified"] ?? false;

        $properties = [
            'displayName' => $request->displayName
        ];

        $updatedUser = $auth->updateUser($user->uid, $properties);

        if(strlen($request->bio) > 150){
            Return Redirect::back()->withErrors(["bioerror" => "Your answer to the math question was incorrect"]);
        }

        $auth->setCustomUserClaims($user->uid, [
            'username' => $user->customClaims['username'],
            "gender" => $request->gender,
            "smoking" => $request->smoking,
            "drinking" => $request->drinking,
            "dob" => $request->dob,
            "bio" => $request->bio,
            "occupation" => $request->occupation,
            "instagram" => str_replace('@','',$request->instagram),
            "requests" => $request->requests,
            'genderPref' => $request->genderPref,
            'sizePref' => $request->sizePref,
            'agePref' => $request->agePref,
            'spendingPref' => $request->spendingPref,
            'smokingPref' => $request->smokingPref,
            'drinkingPref' => $request->drinkingPref,
            "religion" => $request->religion,
            "smsVerified" => $smsVerified,
            "languages" => languagesToObject($request->languages),
            "interests" => interestsToObject($request->interests)
        ]);

        if($request->file("avatar") !== null){

            //dd($request->file("avatar"));
            $storage =  app('firebase.storage');
            $defaultBucket = $storage->getBucket();
            $file = $request->file("avatar");
            $filename = rand(999,9999999) . "-" . cleanFileName($file->getClientOriginalName());
            $extension = strtolower($request->file("avatar")->getClientOriginalExtension());

            $file = $this->shrinkImage($file, true);

            $uploadResult = $defaultBucket->upload(
                
                file_get_contents($file), [
                    'name' => $filename,
                    'uploadType'=> "media",
                    'predefinedAcl' => 'publicRead',
                    "metadata" => ["contentType"=> 'image/' . $extension],
                ]
            );

            $mediaLink = $uploadResult->info()["mediaLink"];

            $properties = [
                'photoUrl' => $mediaLink
            ];

            $updatedUser = $auth->updateUser($user->uid, $properties);


        }

        // SMS Auth

        Session::put("user", $auth->getUser($user->uid));

        Return Redirect::back()->withErrors(["success" => "Your answer to the math question was incorrect"]);
    }

    public function sendVerifySMS(Request $request){
        $sid = getenv("TWILIO_ACCOUNT_SID");
        $token = getenv("TWILIO_AUTH_TOKEN");
        $twilio = new Client($sid, $token);

        // check to see how long it's been since the last code was sent

        $phoneNumber = $request->phoneNumber;

        if(!str_contains($phoneNumber, '+1')){
            // add u.s. country code
            $phoneNumber = "+1" . $phoneNumber;
        }

        if(Session::get('lastSMSCodeSent') !== null){
            $now = strtotime("now"); 
            if($now - Session::get('lastSMSCodeSent')  < 120){ // 3 minutes
                return json_encode(201);
            }   
        }
        

        try{
            $verification = $twilio->verify->v2->services("VA323a370670b9ac3e024ebc1331e166c7")
            ->verifications
            ->create($request->phoneNumber, "sms");
        }
        catch(TwilioRestException $e){
            return json_encode(500);
        }
        
        // to prevent code request abuse, we'll track the last time a code was sent.
        Session::put('lastSMSCodeSent', strtotime("now"));
        Session::put('smsRecipient', $request->phoneNumber);
        Session::put('serviceID', "VA323a370670b9ac3e024ebc1331e166c7");

        return json_encode(["serviceID" =>"VA323a370670b9ac3e024ebc1331e166c7", "phoneNumber" => $request->phoneNumber]);
    }

    public function verifySMSCode(Request $request){
        $sid = getenv("TWILIO_ACCOUNT_SID");
        $token = getenv("TWILIO_AUTH_TOKEN");
        $twilio = new Client($sid, $token);

        try{
            $verification_check = $twilio->verify->v2->services($request->serviceID)
            ->verificationChecks
            ->create([
                "to" => $request->phoneNumber,
                "code" => $request->smsCode
            ]);
        }
        catch(TwilioRestException $e){
            return json_encode("error");
        }

        
         if($verification_check->status == "approved"){

            $user = Session::get('user');
            $auth = app('firebase.auth');
            $user = $auth->getUserByEmail($user->email);

            $phoneNumber = $request->phoneNumber;

            if(!str_contains($phoneNumber, '+1')){
                // add u.s. country code
                $phoneNumber = "+1" . $phoneNumber;
            }

            $customClaims = $user->customClaims;
            $customClaims["phoneNumber"] = $phoneNumber;
            $customClaims["smsVerified"] = true;

            $auth->setCustomUserClaims($user->uid, $customClaims);

            $user = $auth->getUserByEmail($user->email);
            
            Session::put('active', true);
            Session::put('user', $user);

            return json_encode("ok");
         } else {
            return json_encode("error");
         }
    }

    public function verifySMSCodeMFA(Request $request){
        $sid = getenv("TWILIO_ACCOUNT_SID");
        $token = getenv("TWILIO_AUTH_TOKEN");
        $twilio = new Client($sid, $token);

        try{
            $verification_check = $twilio->verify->v2->services("VA323a370670b9ac3e024ebc1331e166c7")
            ->verificationChecks
            ->create([
                "to" => Session::get('user')->customClaims["phoneNumber"],
                "code" => $request->smsCode
            ]);
        }
        catch(TwilioRestException $e){
            return json_encode("error");
        }

        
         if($verification_check->status == "approved"){
            Session::put('active', true);

            return json_encode("ok");
         } else {
            
            return json_encode("error");
         }
    }

    public function verifyEmailValidation(){
        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $auth = app('firebase.auth');
        $user = Session::get('user');

        $user = $auth->getUserByEmail($user->email);

        if($user->emailVerified){
            Session::put('user', $user);
            return 200;
        }

        return 201;
    }

    public function sendEmailValidation(){
        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $auth = app('firebase.auth');
        $user = Session::get('user');

        $user = $auth->getUserByEmail($user->email);

        // create link

        try{
            $email = $user->email;
            $link = $auth->getEmailVerificationLink($user->email);

            $data = [
                "verificationLink" => $link,
                "user" => $user,
                "last" => false
            ];

            $subject = "Welcome to Venti! You Need to Verify Your Email";

            Mail::send('emails.onboarding.email-verification', array("data" => $data), function($message) use($email, $subject)
                {
                    $message
                    ->to($email)
                    ->replyTo("no-reply@venti.co")
                    ->subject($subject);
            });

            return 200;
        }

        catch(FirebaseException $e){
            // too many attempts
            return 201;
        }

        return 201;
    }

    public function read($clientID){
        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $doc = $db->collection("Clients")->document($clientID);

        $profile = $doc->snapshot()->data();

        $data = [
            "profile" => $profile
        ];

        return view("dashboard.clients.profile", $data);

    }

    public function getInvitations(){
        // get all invitations awaiting a user
        $firestore = app('firebase.firestore');
        $auth = app('firebase.auth');
        $db = $firestore->database();
        $user = Session::get('user');
        $ideas = $db->collection("Ideas");
        $groups = $ideas->where('group', 'array-contains-any', [
            ["uid" => $user->uid, "role" => "invited"],
            ["uid" => $user->uid, "role" => "requested"],
            ["uid" => $user->uid, "role" => "recommended"]])->documents();
        $inviteList = [];
        
        foreach($groups as $group){
            $group = $group->data();
            $isTravel = true;

            if(array_key_exists("type", $group) && $group["type"] == "activity" || $group["clientID"] == "mnTmABt3MDOWyy76pwG7V0QACpD2"){
                $isTravel = false;
            }

            $role = getGroupRole($user->uid, $group);
            $group["myStatus"] = $role;
            $group["isTravel"] = $isTravel;
            
            if($isTravel == true){
                //make sure it's not in the past
                $departure = Carbon::createFromFormat('Y-m-d', str_replace("/","-",$group["departure"]))->timezone('America/New_York');
                $today = Carbon::now()->timezone('America/New_York');
                $daysRemaining = (int) $today->diffInDays($departure, false);
                $group["daysRemaining"] = $daysRemaining;
                if($daysRemaining > 0){
                    $author = $auth->getUser($group["clientID"]);
                    $group["author"] = $author;

                    if($group["archived"] == 0){
                        array_push($inviteList, $group); 
                    }
                }
            } else {
                $author = $auth->getUser($group["clientID"]);
                $group["author"] = $author;

                if($group["archived"] == 0){
                    array_push($inviteList, $group); 
                }
            }
        }

        $data = [
            "invites" => $inviteList
        ];

        return view('users.invitations', $data);
    }

    public function getWishlist(){
        $firestore = app('firebase.firestore');
        $auth = app('firebase.auth');
        $db = $firestore->database();
        $user = Session::get('user');
        $wishlist = $db->collection("Wishlists");
        $wishlistItems = $wishlist->where('clientID','=', Session::get('user')->uid)->documents();
        $wishListArray = [];

        foreach($wishlistItems as $wishlist){
            $wishlist = $wishlist->data();

            if($wishlist["archived"] == 0){
                array_push($wishListArray, $wishlist);
            }
        }

        $data = [
            "wishlists" => $wishListArray
        ];

        return view("users.wishlist", $data);
    }

    public function createWishlist(Request $request){
        $auth = app('firebase.auth');
        $user = Session::get('user');
        $firestore = app('firebase.firestore');
        $db = $firestore->database();

        $wishlistID = strtotime("now");

        $docRef = $db->collection("Wishlists")->document($wishlistID)->set(
            [             
                "archived" => 0, // is 0 by default until user deletes it
                "month" => $request->month,
                "year" => $request->year,
                "country" => $request->country,
                "latitude" => $request->latitude,
                "longitude" => $request->longitude,
                "destination" => $request->destination,
                "destinationLabel" => $request->destination . ", " . $request->country,
                "wishlistID" => $wishlistID,
                "clientID" => $user->uid,
                "privacy" => $request->privacy,
                "createdAt" => Date("Y-m-d")
            ]
        );

        return 200;
    }

    public function updateWishlist(Request $request)
    {
        $firestore = app('firebase.firestore');
        $auth = app('firebase.auth');
        $db = $firestore->database();
        $user = Session::get('user');

        $wishlistDoc = $db->collection("Wishlists")->document($request->wishlistID);
        $wishlist = $wishlistDoc->snapshot()->data();

        if($wishlist !== null){
            if($user->uid == $wishlist["clientID"]){
                $update = $wishlistDoc->update([
                    ["path" => "privacy","value" => $request->privacy],
                    ["path" => "month","value" => $request->month],
                    ["path" => "year","value" => $request->year],
                ]);

                return 200;
            }
        }

        return 500;
    }

    public function removeWishlist(Request $request){
        $firestore = app('firebase.firestore');
        $auth = app('firebase.auth');
        $db = $firestore->database();
        $user = Session::get('user');

        $wishlistDoc = $db->collection("Wishlists")->document($request->itemID);
        $wishlist = $wishlistDoc->snapshot()->data();

        if($wishlist !== null){
            if($user->uid == $wishlist["clientID"]){
                $update = $wishlistDoc->update([
                    ["path" => "archived","value" => 1]
                ]);

                return 200;
            }
        }

        return 500;
    }

    public function getUserGroups($uid){
        // for invitation purposes
        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $ideas = $db->collection("Ideas");
        $ideas = $ideas->where('clientID','=', Session::get('user')->uid)->documents();

        $ideaList = [];

        foreach($ideas as $idea){
            if($idea["archived"] == 1 || $idea["clientID"] == "mnTmABt3MDOWyy76pwG7V0QACpD2"){
                continue;
            }
            //dd($group);
            $idea = $idea->data();
            $role = getGroupRole($uid, $idea);

            if($role != "decline" && $role != "viewer" && $role != "invited"){
                array_push($ideaList, $idea);
            }
        }

        return json_encode($ideaList);
    }

    public function getSettings(){
        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $settings = $db->collection("users/" . Session::get('user')->uid . "/Settings")->document("data")->snapshot()->data();

        $data = [
            "settings" => $settings
        ];

        return view('users.settings', $data);
    }

    public function updateSettings(Request $request){
        $firestore = app('firebase.firestore');
        $db = $firestore->database();

        $docRef = $db->collection("users/" . Session::get('user')->uid . "/Settings")->document("data")->set(
        [
            "emailJoinRequest" => $request->emailJoinRequest ?? 0,
            "emailInvited" => (int) $request->emailInvited ?? 0,
            "emailInviteAccepted" => $request->emailInviteAccepted ?? 0,
            "emailTripRecommendations" => $request->emailTripRecommendations ?? 0,
            "emailGroupRecommendations" => $request->emailGroupRecommendations ?? 0,
            "emailNewFeatures" => $request->emailNewFeatures ?? 0
        ]);

        Session::flash('success', 'yes');

        return Redirect::back();
    }



    public function verifyEmail(){
        $auth = app('firebase.auth');
        try{
            $auth->sendEmailVerificationLink(Session::get('user')->email);
            return json_encode(200);
        }
        catch(FirebaseException $e){
            return json_encode(500);
        }
    }

    public function shrinkImage($OGimage, $crop = false){
        $image = Image::make($OGimage)->orientate();
        if($crop){
            $image->fit(300);
        } else{
            $image->resize(300, null, function ($constraint) {
                $constraint->aspectRatio();
            });
        }

        $extension = $OGimage->getClientOriginalExtension();
    
        $thumbnailPath = public_path() .'/thumbnail/';
        $fileName = $thumbnailPath.time(). ".jpg";
        $image->save($fileName);
    
        return $fileName;
    }

    public function updateTimezone(Request $request){

        // the only allowed change to a user profile

        $validator = Validator::make($request->all(), [
            'timezone' => 'required|max:255'
        ]);

        if ($validator->fails()) {
            return 500;
        }

        $auth = app('firebase.auth');
        $customClaims = Session::get('user')->customClaims;
        $customClaims["timezone"] = $request->timezone;

        try{
            $auth->setCustomUserClaims(Session::get('user')->uid, $customClaims);

            return 200;
        }

        catch(FirebaseException $e){
            return 500;
        }
    }
}
