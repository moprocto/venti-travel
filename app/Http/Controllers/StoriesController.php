<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;
use Mail;
use Redirect;
use Kreait\Firebase\Exception\FirebaseException;

class StoriesController extends Controller
{
    public function browse(){
        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $stories = $db->collection('Stories');
        $stories = $stories->where("archived","=","0")->documents();
        $storiesList = [];
        $auth = app('firebase.auth');
        

        foreach($stories as $story){
            $liked = false;
            $story = $story->data();

            try{
                $story["author"] = $auth->getUser($story["clientID"]);
            }

            catch(FirebaseException $e){
                continue;
            }


            if(Session::get('user') !== null){
                    if(in_array(Session::get('user')->uid, $story["likes"], true) == true){
                        $liked = true;
                    }   
            }

            $story["liked"] = $liked;
            $story["timestamp"] = Carbon::createFromFormat("Y-m-d", $story["createdAt"])->timezone('America/New_York')->timestamp;

            array_push($storiesList, $story);
        }

        $storiesList = collect($storiesList)->sortBy('timestamp')->reverse()->toArray();

        $data = [
            "stories" => $storiesList
        ];

        return view("stories.browse", $data);
    }

    public function like(Request $request){

        $storyID = $request->storyID;
        $likes = [];
        $liked = false;
        $user = Session::get('user');

        if($storyID != "" && !is_null($storyID)){
            $firestore = app('firebase.firestore');
            $db = $firestore->database();
            $story = $db->collection('Stories')->document($storyID)->snapshot()->data();
            if($user !== null){
                
                foreach($story["likes"] as $like){
                    if($user->uid == $like){
                        $liked = true;
                    }

                }

                $likes = $story["likes"];
            }

            if($liked == false){
                // user is submitting a new like
                array_push($likes, $user->uid);
                // update the array in fb
                $docRef = $db->collection('Stories')->document($storyID)->update([
                    [
                    "path" => "likes",
                    "value" => $likes
                    ]
                ]);

                return json_encode(200); // like
            }

            $likes = [];

            foreach($story["likes"] as $like){
                if($like != $user->uid){
                    array_push($likes, $like);
                }
            }

            $docRef = $db->collection('Stories')->document($storyID)->update([
                    [
                    "path" => "likes",
                    "value" => $likes // forces array insert on unlike
                    ]
            ]);

            return json_encode(201); // unlike
        }
    }

    public function share(Request $request){
        if(Session::get('user') !== null){
            $data = [
                "category" => $request->category,
                "author" => Session::get('user')->email,
                "title" => $request->title,
                "content" => $request->content,
                "media" => $request->media,
                "location" => $request->location
            ];

            $data = json_encode($data);

            Mail::raw($data, function ($message) {
                $message->subject("New Story Request");
                $message->from("markus@venti.co");
                $message->to('markus@venti.co')->cc("2404412060@vtext.com");
            });
            Session::flash("shareSuccess");
            return Redirect::back();
        }
        Session::flash("shareError");
        return Redirect::back();
    }

    public function read($id){
        if(!isset($id) || $id == ""){
            return view('errors.409');
        }

        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $story = $db->collection('Stories')->document($id)->snapshot()->data();
        $auth = app('firebase.auth');


        if($story["archived"] != 0){
            if(env('APP_ENV') != "local"){
                return view('errors.409');
            }
        }

        $author = $auth->getUser($story["clientID"]);

        unset($author->providerData);
        unset($author->email);

        $data = [
            "story" => $story,
            "author" => $author
        ];

        return view("stories.read", $data);
    }
}
