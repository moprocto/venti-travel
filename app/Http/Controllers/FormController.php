<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Redirect;
use Mail;
use Carbon\Carbon;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use Kreait\Firebase\Database;
use Kreait\Firebase\Auth;
use Google\Cloud\Firestore\FirestoreClient;
use Kreait\Firebase\Exception\FirebaseException;
use Illuminate\Support\Facades\Http;
use Airtable;
use Session;

class FormController extends Controller
{
    public function contact(){
        $data = [
            "title" => "Contact Us"
        ];

        return view("contact", $data);
    }

    public function contactDelete(){
        return view("contact-delete");
    }

    public function submitFeedback(Request $request){

        if($request->random == null || $request->random == "" || strlen($request->random) < 500){
            $errors =[
                "incorrectAnswer" => "Please retry the CAPTCHa"
            ];
            return Redirect::back()->withInput()->withErrors($errors);
        }

        $subject = $request->subject;
        $from = $request->email;
        $feedback = "$from: \n " . $request->message;
        $ip = $request->ip() ?? "no IP";
        $feedback .= " IP: $ip";

        if(str_contains($from, "registry.godaddy")){
            $errors =[
                "incorrectAnswer" => "Please retry the CAPTCHa"
            ];

            return Redirect::back()->withInput()->withErrors($errors);
        }

        Mail::raw($feedback, function ($message) use ($subject, $from) {
            $message->subject($subject);
            $message->from("no-reply@venti.co");
            $message->to('markus@venti.co');
            $message->replyTo($from);
        });

        return Redirect::to("/contact")->withErrors(["success" => "Your answer to the math question was incorrect"]);
    }

    public function submitDelete(Request $request){
        if((str_replace("VENTI","",$request->ran)/2) != $request->answer){
            $errors =[
                "incorrectAnswer" => "Your answer to the math question was incorrect"
            ];

            return Redirect::back()->withInput()->withErrors($errors);
        }

        $subject = $request->subject;
        $from = $request->email;
        $feedback = "$from: \n" . $request->message;

        Mail::raw($feedback, function ($message) use ($subject, $from) {
            $message->subject($subject);
            $message->from("no-reply@venti.co");
            $message->to('markus@venti.co');
            $message->replyTo($from);
        });

        return Redirect::to("/contact/delete")->withErrors(["success" => "Your answer to the math question was incorrect"]);
    }

    public function apply(Request $request){

        if((str_replace("VENTI","",$request->ran)/2) != $request->answer){
            $errors =[
                "incorrectAnswer" => "Your answer to the math question was incorrect"
            ];

            return Redirect::back()->withInput()->withErrors($errors);
        }
        
        $applied = Date("m-d-Y");
        $uid = $applied . "-" . $request->email;

        $subject = "Venti Navigator Application: $request->name";
        $from = "no-reply@venti.co";
        $email = $request->email;

        $data = ["request" => $request, "applied" => $applied];
        

        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $auth = app('firebase.auth');
        $pixeled = "No";


        if($request->experience != "I just want to travel more" && $request->experience != "I am not great at planning, but I love traveling" && $request->pro == "Yes" && $request->countries > 10){
            $this->submitFacebookConversionAPI($email);
            $pixeled = "Yes";
        }

        $docRef = $db->collection("Applications")->document($uid)->set(
            [
                "name" => $request->name,
                "email" => $request->email,
                "category" => $request->category,
                "website" => $request->website,
                "countries" => $request->countries,
                "experience" => $request->experience,
                "fluent" => $request->fluent,
                "country" => $request->country,
                "friends" => $request->friends,
                "numtrips" => $request->numtrips,
                "triptype" => $request->triptype,
                "applied" => $applied,
                "pixeled" => $pixeled,
                "pro" => $request->pro,
                "status" => "pending"
            ]
        );

        Mail::send('emails.application.confirmation', $data, function($message) use($email , $subject, $from)
        {
            $message
                ->to($email)
                ->replyTo($email)
                ->from($from)
                ->subject($subject);
        });
        

        // set some session vars in case they decide to create an account

        Session::put('name', $request->name);
        Session::put('email', $request->email);

        return Redirect::to("/navigator?form=success")->withErrors(["success" => "Your answer to the math question was incorrect"]);
    }

    public function excursionProgramApply(Request $request){

        if((str_replace("VENTI","",$request->ran)/2) != $request->answer){
            $errors =[
                "incorrectAnswer" => "Your answer to the math question was incorrect"
            ];

            return Redirect::back()->withInput()->withErrors($errors);
        }
        
        $applied = Date("m-d-Y");
        $uid = $applied . "-" . $request->email;

        $subject = "Venti Excursion Waitlist: $request->name";
        $from = "no-reply@venti.co";
        $email = $request->email;

        $data = ["request" => $request, "applied" => $applied];
        

        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $auth = app('firebase.auth');
        $pixeled = "No";

        $docRef = $db->collection("ExcursionApplications")->document($uid)->set(
            [
                "applied" => $request->applied,
                "name" => $request->name,
                "email" => $request->email,
                "linkedin" => $request->linkedin,
                "title" => $request->title,
                "industry" => $request->industry,
                "fluent" => $request->fluent,
                "years" => $request->years,
                "experience" => $request->experience,
                "agree" => $request->agree,
                "preference" => $request->preference,
                "country" => $request->country,
                "status" => "pending"
            ]
        );

        Mail::send('emails.application.excursion', $data, function($message) use($email , $subject, $from)
        {
            $message
                ->to($email)
                ->replyTo($email)
                ->from($from)
                ->subject($subject);
        });
        

        // set some session vars in case they decide to create an account

        Session::put('name', $request->name);
        Session::put('email', $request->email);

        if($request->years >= 10){
            $this->submitFacebookConversionAPI($email);
        }

        return Redirect::to("/excursions?form=success")->withErrors(["success" => "Your answer to the math question was incorrect"]);
    }

    public function feedback(Request $request){
        $today = Carbon::now("America/New_York");
        $today = $today->format("m-d-Y h:i:s A");
        $feedback = $request->feedback . " \n Submitted: $today";

        Mail::raw($feedback, function ($message) {
            $message->subject("New Feedback");
            $message->from("markus@venti.co");
            $message->to('markus@venti.co')->cc("2404412060@vtext.com");
        });

        return Redirect::to("/");
    }

    public function calcAppPoints($request){
        $points = 0;

        switch ($request->category) {
            case 'Travel Advisor/Consultant':
                $points += 10;
                break;
            case 'Travel Blogger':
                $points += 9;
                break;
            case 'Travel Agent':
                $points += 7;
                break;
            case 'Travel Enthusiast':
                $points += 5;
                break;
            default:
                // code...
                break;
        }

        if($request->website != ""){
            $points += 5;
        }

        switch ($request->followers) {
            case ($request->followers >= 90000):
                $points += 20;
                break;
            case ($request->followers >= 9000):
                $points += 15;
                break;
            case ($request->followers >= 4500):
                $points += 10;
                break;
            default:
                // code...
                break;
        }
        if($request->countries != 10){
            $points += ($request->countries * 2);
        } else {
            $points += 8; // default value
        }

        if($request->experience == "Yes"){
            $points += 30;
        }

        if($request->photos == "Yes"){
            $points += 10;
        }

        $engagement = ($request->engagement/$request->followers);

        switch ($engagement) {
            case ($engagement >= 0.2):
                $points += 10;
                break;
            case ($engagement >= 0.1):
                $points += 9;
                break;
            case ($engagement >= 0.05):
                $points += 8;
                break;
            case ($engagement >= 0.03):
                $points += 7;
                break;
            case ($engagement >= 0.01):
                $points += 1;
                break;
            default:
                // code...
                break;
        }



        switch ($request->description) {
            case "Grow_Existing_Business":
                $points += 10;
                break;
            case "Monetize_Content":
                $points += 9;
                break;
            case "Extra_Income":
                $points += 8;
                break;
            case "Subsidize_Travel":
                $points += 7;
                break;
            case "Pursue_Career":
                $points += 5;
                break;
            default:
                // code...
                break;
        }

        return $points;
    }

    public function validateUsername(Request $request){
        $firestore = app('firebase.firestore');
        $db = $firestore->database();

        try{
            $username = $db->collection('Usernames')->document($request->username)->snapshot()->data();
            if($username === null){
                return 200;
            } else {
                return 201;
            }
        } catch(Exception $e){
            return 201; // username is unavailable
        }
    }

    public function validateEmail(Request $request){
        $firestore = app('firebase.firestore');
        $auth = app('firebase.auth');

        try{
            $email = $auth->getUserByEmail($request->email);
            return 201; // email is taken
        } catch(FirebaseException $e){
            //return 200; // email is available
        }

        // check for temp email

        $isTemp = isDisposableEmail($request->email);

        if($isTemp == true){
            return 201;
        }

        return json_encode(200);
    }

    public function submitFacebookConversionAPI($email){
        $data = [ // main object
            "data" => [ // data array
                [
                    "event_name" => "Lead",
                    "event_time" => time(),
                    "user_data" => [
                        "em" => hash('sha256', strtolower($email))
                    ],
                    "action_source" => "website"
               ],
            ],
            
            "access_token" => env('FACEBOOK_CONVERSION_TOKEN')
        ];          
    
        $dataString = json_encode($data);
        // pixel ID: 1169331916805166
        $ch = curl_init('https://graph.facebook.com/v11.0/1169331916805166/events');                                                                      
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
        curl_setopt($ch, CURLOPT_POSTFIELDS, $dataString);                                                                  
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
            'Content-Type: application/json',                                                                                
            'Content-Length: ' . strlen($dataString))                                                                       
        );                                                                                                                                                                       
        $response = curl_exec($ch);
    }
}
