<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TailwindController extends Controller
{
    public function index(){
        $data = [
            "title" => "Travel Rewards Card",
            "description" => "Earn free flights, hotels, and more",
            "url" => "https://venti.co",
            "image" => "https://venti.co/assets/img/tailwind-social.jpg",
        ];

        return view('tailwind.index', $data);
    }

    public function waitlist(){

    }
}
