<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Dwolla;
use Mail;
use Kreait\Firebase\Exception\FirebaseException;
use Carbon\Carbon;

class WebhooksController extends Controller
{
    public function createSubscription(){
        dd();
        $dwolla = new Dwolla;
        $dwolla->createWebhookSubscription();
    }

    public function retrieveSubscription(){
        dd();
        $dwolla = new Dwolla;
        $dwolla->retrieveWebhookSubscription(env('DWOLLA_WEBHOOK_URL_' . strtoupper(env('APP_ENV'))));
    }

    public function listSubscription(){
        dd();
        $dwolla = new Dwolla;
        $dwolla->listWebhookSubscription(env('DWOLLA_WEBHOOK_URL_' . strtoupper(env('APP_ENV'))));
    }

    public function deleteWebhook(){
        dd();
        // Using dwollaswagger - https://github.com/Dwolla/dwolla-swagger-ph
        $dwolla = new Dwolla;
        $dwolla->deleteWebhookSubscription();
    }

    public function listenDwolla(Request $request){
        $topic = $request->header('X-Dwolla-Topic'); // customer_created
        $signature = $request->header('X-Request-Signature-SHA-256'); // signature must be present
        $links = $request->_links;
        $customer = $links->customer ?? null;
        $resource = $links->resource ?? null;
        $resourceId = $request->resourceId;
        if(!isset($signature) || $signature == null){
            $signature = json_encode($request->header());
        }

        //if(isset($topic) && isset($signature) && $signature == env('DWOLLA_WEBHOOK_KEY_' . strtoupper(env('APP_ENV')) )){

        if(isset($topic)){
            // only init db connection if a valid request comes in, otherwise we could get spammed
            $firestore = app('firebase.firestore');
            $db = $firestore->database();
            $auth = app('firebase.auth');
        } else {
            return 500;
        }

        if( str_contains($topic, "customer_verifi") || 
            str_contains($topic, "customer_reverifi") ||
            str_contains($topic, "customer_suspended") ||
            str_contains($topic, "customer_deactivated")
        ){

            $resourceId = explode("/customers/",$request->_links["customer"]["href"])[1];

            $customerDoc = $db->collection("Customers")->document($resourceId);
            $customerData = $customerDoc->snapshot()->data();
            try{
                if(!$customerData){
                    // this happens when the resource ID is a document and not a customer
                    // we can return 200 for now until we have a better way to deal with this
                    // just need to clean up the error log
                    
                    return 200;
                }
                $user = $auth->getUser($customerData["uid"]);
            }
            catch(FirebaseException $e){
                return 500;
            }
            
        }

        if(str_contains($topic, "customer_bank_transfer") || str_contains($topic, "transfer_completed")){
            $transactionDoc = $db->collection("Transactions")->document($resourceId);
            $transactionData = $transactionDoc->snapshot()->data();

            try{
                $uid = $transactionData["uid"] ?? false;
                if($uid){
                    $user = $auth->getUser($uid);
                    $email = $user->email;
                }
                else {
                    return 200;
                }
            }
            catch(FirebaseException $e){
                return 500;
            }
        }

        switch ($topic) {
            // for the customer 

            case 'bank_transfer_completed':
                // dwolla responded that the customer requires full SSN

                return 200;

                break;

            case 'customer_transfer_completed':
                // dwolla responded that the customer requires full SSN

                return 200;

                break;

            case 'customer_transfer_completed':
                // dwolla responded that the customer requires full SSN

                return 200;

                break;

            case 'customer_reverification_needed':
                // dwolla responded that the customer requires full SSN

                $customerDoc->update([
                    ["path" => "status","value" => "retry"]
                ]);

                break;


            case 'customer_verification_document_needed':
                // dwolla responded that the customer requires documents to be uploaded to pass KYC
                $customerDoc->update([
                    ["path" => "status","value" => "document"]
                ]);

                // email the update to the user

                $email = $user->email;
                $subject = "[Venti Boarding Pass] Account Verification Update";

                /*

                Mail::send('emails.onboarding.customer-verification-docs-requested', array("data" => []), function($message) use($email, $subject)
                    {
                        $message
                            ->to($email)
                            ->from("no-reply@venti.co","Venti")
                            ->replyTo("no-reply@venti.co")
                            ->subject($subject);
                });

                */

                break;
            case 'customer_verification_document_failed':
                // dwolla rejected the customer document
                $customerDoc->update([
                    ["path" => "status","value" => "document"]
                ]);

                $email = $user->email;
                $subject = "[Venti Boarding Pass] Account Verification Update";

                /*

                Mail::send('emails.onboarding.customer-verification-failed', array("data" => []), function($message) use($email, $subject)
                    {
                        $message
                            ->to($email)
                            ->from("no-reply@venti.co","Venti")
                            ->replyTo("no-reply@venti.co")
                            ->subject($subject);
                });

                */

                break;
            case 'customer_verified':
                // dwolla verified a customer
                // send them a happy email

                // dwolla approved the customer document
                $customerDoc->update([
                    ["path" => "status","value" => "verified"]
                ]);

                $email = $user->email;
                $subject = "[Venti Boarding Pass] Account Verified";

                /*

                Mail::send('emails.onboarding.customer-verification-succeeded', array("data" => []), function($message) use($email, $subject)
                    {
                        $message
                            ->to($email)
                            ->from("no-reply@venti.co","Venti")
                            ->replyTo("no-reply@venti.co")
                            ->subject($subject);
                });

                */

                // get this user's wallet address and add it to their firestore

                $dwolla = new Dwolla;

                $wallet = $dwolla->getWallet($customerData["customerID"]);
                $timestamp = strtotime("now");

                $customerDoc->update([
                                    ["path" => "wallet","value" => $wallet->_links->self->href],
                                    ["path" => "latestBalance","value" => 0],
                                    ["path" => "latestBalanceTimestamp","value" => $timestamp]
                            ]);


                break;
            case 'customer_suspended':
                // dwolla angry
                $customerDoc->update([
                    ["path" => "status","value" => "suspended"]
                ]);

                $email = $user->email;
                $subject = "[Venti Boarding Pass] Account Suspended";
                $data = [
                    "date" => \Carbon\Carbon::now('UTC')->timezone("America/New_York")->format("Y-m-d @ h:i A")
                ];

                Mail::send('emails.transactions.account-suspended', array("data" => $data), function($message) use($email, $subject)
                    {
                        $message
                            ->to($email)
                            ->from("no-reply@venti.co","Venti")
                            ->replyTo("no-reply@venti.co")
                            ->subject($subject);
                });

                break;
            case 'customer_deactivated':

                $customerDoc->update([
                    ["path" => "status","value" => "deactivated"]
                ]);
                break;
            case 'customer_activated':
                // dwolla happy again
                break;
            case 'customer_funding_source_added':
                // funding source added

                $firestore = app('firebase.firestore');
                $db = $firestore->database();
                $auth = app('firebase.auth');

                break;
            case 'customer_funding_source_verified':
                // funding source passed deposit verification
                break;
            case 'customer_funding_source_negative':
                // customer attempted to withdraw more than what was available. 
                // we are responsible for closing this gap
                break;
            case 'customer_bank_transfer_cancelled':
                // customer requested a transfer be canceled

                $timestamp = \Carbon\Carbon::now('UTC')->format("Y-m-d H:i:s");


                $transactionDoc = $db->collection("Transactions")->document($transactionData["transferID"]);
                $transactionDoc->update([
                    ["path" => "status","value" => "canceled"],
                    ["path" => "canceledAt","value" => $timestamp]
                ]);

                break;
            case 'customer_bank_transfer_completed':
                // customer's request is complete and we should update firebase

            
                // we need to see if there were any fees associated with this transaction that also need to be marked complete

                $transactionID = $transactionData["transactionID"];

                $transactions = $db->collection("Transactions")->where("transactionID","=",$transactionID)->documents();

                foreach($transactions as $transaction){
                    $transactionData = $transaction->data();
                    $transactionDoc = $db->collection("Transactions")->document($transactionData["transferID"]);
                    $transactionDoc->update([
                        ["path" => "status","value" => "complete"]
                    ]);

                    if($transactionData["env"] == "autosave"){
                        $pointsToAward = 0.5;

                        if($transactionData["amount"] > 100){
                            $pointsToAward = round($transactionData["amount"] * .0013, 0);
                        }

                        // autosave reward

                        $transactionID2 = generateRandomString(12);

                        $data = [
                            "transactionID" => $transactionID2,
                            "uid" => $transactionData["uid"],
                            "type" => "points-deposit",
                            "timestamp" => \Carbon\Carbon::now('UTC')->timestamp, // UTC
                            "date" => \Carbon\Carbon::now('UTC')->format("Y-m-d H:i:s"),
                            "amount" => $pointsToAward,
                            "speed" => "next-available",
                            "fee" => 0,
                            "status" => "complete",
                            "from" => "Venti",
                            "to" => "My Boarding Pass",
                            "total" => $pointsToAward,
                            "note" => "Recurring Deposit Credit: " . $transactionData["transactionID"] ?? "",
                            "transferID" => null, // it's wise to link this with the previous transfer
                            "transactionUrl" => null,
                            "env" => env('APP_ENV')
                        ];

                        $bank = $db->collection("Transactions")->document($transactionID2)->set($data);
                    }
                }

                break;
            case 'customer_bank_transfer_failed':
                // customer's request is complete and we should update firebase
                

                $transactionDoc->update([
                    ["path" => "status","value" => "failed"]
                ]);

                $email = $user->email;
                $subject = "[Venti Boarding Pass] Your Recent Bank Transfer failed";
                $data = [
                    "transactionData" => $transactionData
                ];

                /*

                Mail::send('emails.transactions.transfer-failed', array("data" => $data), function($message) use($email, $subject)
                    {
                        $message
                            ->to($email)
                            ->cc("admin@venti.co")
                            ->from("no-reply@venti.co","Venti")
                            ->replyTo("no-reply@venti.co")
                            ->subject($subject);
                });

                */

                // we should deduct a fee from this customer's account for screwing us up

                //$dwolla = new Dwolla;

                //$dwolla->achFailed($resourceId);

                break;
            case 'transfer_completed':
                // this is usually when we charge a transaction fee
                if($transactionDoc){
                    $transactionDoc->update([
                        ["path" => "status","value" => "complete"]
                    ]);
                }

                break;
            case '':
                break;
            default:
                // code...
                break;
        }

        

        $db->collection("Webhooks")->document($request->id)->set([
            "links" => $links,
            "created" => $request->created,
            "id" => $request->id,
            "resourceId" => $resourceId,
            "topic" => $topic,
            "signature" => $signature,
            "env" => env("APP_ENV")
        ]);

        // store each webhook for review
    }

    public function listenStripe(Request $request)
    {
        $payload = $request->getContent();
        $sig_header = $request->header('Stripe-Signature');
        $endpoint_secret = env('STRIPE_WEBHOOK_SECRET');
        
        try {
            $event = \Stripe\Webhook::constructEvent(
                $payload, $sig_header, $endpoint_secret
            );
        } catch(\UnexpectedValueException $e) {
            return response()->json(['error' => 'Invalid payload'], 400);
        } catch(\Stripe\Exception\SignatureVerificationException $e) {
            return response()->json(['error' => 'Invalid signature'], 400);
        }

        $firestore = app('firebase.firestore');
        $db = $firestore->database();

        switch ($event->type) {
            case 'customer.subscription.updated':
            case 'customer.subscription.deleted':
                $subscription = $event->data->object;
                
                // Find the AirlockPlan document by subscription ID
                $plans = $db->collection('AirlockPlans')
                    ->where('stripeSubscriptionId', '=', $subscription->id)
                    ->documents();

                foreach ($plans as $plan) {
                    $db->collection('AirlockPlans')
                        ->document($plan->id())
                        ->update([
                            ['path' => 'status', 'value' => $subscription->status],
                            ['path' => 'updatedAt', 'value' => now()->timestamp]
                        ]);
                }
                break;
        }

        return response()->json(['status' => 'success']);
    }

    public function listenDuffel(Request $request){
        $changeData = $request->all();

        $type = $changeData["type"];
        $subject = "[Venti Boarding Pass] Order Created";
        $from = "no-reply@venti.co";

        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $auth = app('firebase.auth');

        switch ($type) {
            case 'order.created':
                $feedback = json_encode($changeData);

                Mail::raw($feedback, function ($message) {
                    $message->subject("New Order");
                    $message->from("markus@venti.co");
                    $message->to('markus@venti.co')->cc("2404412060@vtext.com");
                });
                
                break;
            case 'order.airline_initiated_change_detected':

                // find the owner of the order

                $order = $db->collection('Orders')->document($changeData["data"]["object"]["id"])->snapshot()->data();

                $data = json_decode($order["order"], true)["data"];


                try{
                    $booking_reference = $order["booking_reference"];
                    $user = $auth->getUser($order["uid"]);
                    $subject = "[Venti Boarding Pass] Airline Initiated Change for Flight: $booking_reference";
                    $email = $user->email;

                    $changeData = compareSegmentTimes($changeData["data"]);

                    $data = [
                        "customer" => getFirstName($user->displayName),
                        "booking_reference" => $booking_reference,
                        "owner" => $data["owner"]["name"],
                        "changes" => $changeData
                    ];


                    Mail::send('emails.flights.change-notice', array("data" => $data), function($message) use($email, $subject)
                    {
                        $message
                            ->to($email)
                            ->cc("admin@venti.co")
                            ->from("no-reply@venti.co","Venti")
                            ->replyTo("admin@venti.co")
                            ->subject($subject);
                    });

                    return json_encode($changeData);
                }
                catch(FirebaseException $e){

                }

                

                break;
            default:
                
                break;
        }
    }

    public function customer_funding_source_added(){

    }
}
