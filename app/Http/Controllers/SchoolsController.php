<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Http;

use Session;
use Location;
use Redirect;
use Mail;
use Exception;
use Validator;
use Str;

class SchoolsController extends Controller
{
    public function index(Request $request){
        if($request->fbid == "fb"){
            Session::put('referrer', "facebook");
        }
        elseif($request->fbid == "ig"){
            Session::put('referrer', "instagram");
        }
        elseif($request->fbid == "msg"){
            Session::put('referrer', "messenger");
        }
        elseif($request->fbid == "adwords"){
            Session::put('referrer', "adwords");
        }
        elseif(strlen($request->fbid) >= 12){
            Session::put('referrer', $request->fbid);
        }
        if(Session::get('fbclid') === null){
            Session::put('fbclid', $request->fbclid);
        }

        $data = [
            "title" => "Earn Travel Rewards for Good Grades",
            "description" => "College students travel for free when they show good grades on their assignments, quizzes, and exams.",
            "url" => "https://venti.co/schools",
            "image" => "https://venti.co/assets/img/navigator-backdrop-t89.jpg",
        ];

        return view('schools.index', $data);
    }

    public function success(Request $request){

        $email = $request->email;

        $validator = Validator::make($request->all(), [
            'email' => [
                'required',
                'string',
                'email'
                /*
                function ($attribute, $value, $fail) {
                    $allowedDomains = [
                        '.edu',       // United States
                        '.ac.uk',     // United Kingdom
                        '.edu.au',    // Australia
                        '.ca',        // Canada
                        '.de',        // Germany
                        '.fr',        // France
                        '.ac.jp',     // Japan
                        '.ac.in',     // India
                        '.edu.cn',    // China
                        '.edu.br',    // Brazil
                        '.ac.za',     // South Africa
                    ];

                    $domain = substr(strrchr($value, "@"), 1);
                    if (!Str::endsWith($domain, $allowedDomains)) {
                        $fail('The :attribute must be from an approved educational institution.');
                    }
                },
                */
            ],
        ]);

        if ($validator->fails()) {
            return Redirect::to('/schools#waitlist')->withErrors($validator)->withInput();
        }


        $to = $request->name;

        if($email == null && $email == ""){
            return json_encode(500);
        }

        $email = $email;
        $subject = "[Venti] You're On the Waitlist";
        $from = "no-reply@venti.co";
        $message = "";
        $data = [
            "company" => $request->company,
            "name" => $to,
            "email" => $email,
            "website" => $request->website
        ];

        

        $source = Session::get('referrer') ?? "direct";
        $ip = \Request::ip();
        $location = Location::get($ip);
        $agent = $request->header('User-Agent');
        $region = "none";

        if(isset($location) && !is_bool($location)){
            $region = $location->regionName;
        }

        $fields = [
            "Source" => $source,
            "Company" => "SCHOOL",
            "IP" => $ip ?? "none",
            "Location" => $location ? $region : "none",
            "School" => $request->school,
            "Country" => $request->country
        ];

        $result = Http::post("https://emailoctopus.com/api/1.6/lists/6c5be602-0aef-11ef-9067-5f3bee8c335a/contacts", [
            'api_key' => env('EMAIL_OCTOPUS_API_KEY'),
            'email_address' => $email,
            "fields" => $fields
        ]);

        $result = json_decode($result->body(),true);

        if(!array_key_exists("error",$result)){
            // add contact to email octopus
            // send the success message to facebook
            if(env('APP_ENV') != "local"){
                $this->submitFacebookConversionAPI($email, $ip, $location, $agent);
            } else {
                //$this->submitFacebookConversionAPITEST($email, $ip, $location, $agent);
            }

            $id = getEmailID($result["id"]);

            $data = [
                "title" => "Earn Travel Rewards for Good Grades",
                "description" => "College students travel for free when they show good grades on their assignments, quizzes, and exams.",
                "url" => "https://venti.co/schools",
                "image" => "https://venti.co/assets/img/navigator-backdrop-t89.jpg",
                "referral" => $id
            ];

            // send email

            try{

                // try adding to firebase

                $firestore = app('firebase.firestore');
                $db = $firestore->database();
                $auth = app('firebase.auth');

                $db->collection("VIPS")->document($id)->set($result);

            }
            catch(Exception){

            }

            Mail::send('emails.application.school', $data, function($message) use($email , $subject, $from, $to)
            {
                $message
                    ->to($email, $to)
                    ->from($from, "Venti")
                    ->subject($subject); 
            });

            return Redirect::to('/schools#waitlist')->with('success', 'You have been successfully added to the waitlist!');

        }
        
        return Redirect::to('/schools#waitlist')->withErrors(['email' => 'This email is already taken.'])->withInput();

    }

    public function submitFacebookConversionAPI($email, $ip = null, $location = null, $agent = null){
        

        $userdata = [
            "em" => hash('sha256', strtolower($email))
        ];

        if($ip != "none" && !is_null($ip)){
            $userdata["client_ip_address"] = $ip;
        }

        if(!is_null($agent)){
            $userdata["client_user_agent"] = $agent;
        }

        if(!is_null($location)){
            $userdata["country"] = hash('sha256', strtolower($location->countryName));
            $userdata["st"] = hash('sha256', strtolower($location->regionName));
            $userdata["zp"] = hash('sha256', strtolower($location->zipCode));
        }

        if(Session::get('fbclid') !== null){
            // add the fbclick id to the pixel conversion
            $userdata["fbc"] = "fb.1." . (int) (microtime(true) * 1000) . "." . Session::get('fbclid');
        }

        $data = [ // main object
            "data" => [ // data array
                [
                    "event_name" => "Lead",
                    "event_time" => time(),
                    "user_data" => $userdata,
                    "action_source" => "website"
               ],
            ],
            
            "access_token" => env('FACEBOOK_CONVERSION_TOKEN')
        ];
    
        $dataString = json_encode($data);
        // pixel ID: 1169331916805166
        $ch = curl_init('https://graph.facebook.com/v11.0/1169331916805166/events');                                                                      
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
        curl_setopt($ch, CURLOPT_POSTFIELDS, $dataString);                                                                  
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
            'Content-Type: application/json',                                                                                
            'Content-Length: ' . strlen($dataString))                                                                       
        );                       

        // clear fbclic

        Session::flush();                                                                                                                                                
        $response = curl_exec($ch);

    }

    public function getSchoolStats(){    

        $waitlist = [];        

        $result = Http::get("https://emailoctopus.com/api/1.6/lists/6c5be602-0aef-11ef-9067-5f3bee8c335a/contacts", [
            'api_key' => env('EMAIL_OCTOPUS_API_KEY')
        ]);

        $users = json_decode($result->body(),true)["data"];

        foreach($users as $user){
            array_push($waitlist, [
                "email_address" => $user["email_address"] ?? "",
                "school" => $user["fields"]["School"] ?? "",
                "country" => $user["fields"]["Country"] ?? ""
            ]);
        }

        $data = [
            "users" => $waitlist
        ];

        return view('schools.stats', $data);
    }
}
