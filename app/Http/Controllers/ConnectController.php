<?php

namespace App\Http\Controllers;

use Kreait\Firebase\Exception\FirebaseException;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Session;

class ConnectController extends Controller
{
    public function index(){
        $auth = app('firebase.auth');
        $usersList = [];
        $users = $auth->listUsers($defaultMaxResults = 1500, $defaultBatchSize = 1500);
        
        foreach($users as $user){
            $push = true;
            if(array_key_exists("username",$user->customClaims) && array_key_exists("dob",$user->customClaims)){
                // only show users that have created a username and set their DOB
                $user = (array) $user;
                $user["compatibility"] = "";
                if(Session::get('user') !== null){
                    if(isset($user["customClaims"])){
                        $user["compatibility"] = calcCompatibility(Session::get('user'), $user);
                    }
                    if(Session::get('user')->uid == $user["uid"] || $user["disabled"] == true){
                        $push = false;
                    }
                }
                $user = (object) $user;
                
                if($push){
                    array_push($usersList, $user);
                }
            }
        }

        $data = [
            "users" => $usersList
        ];

        return view("users.index", $data);
    }

    public function read($username){
        // check if username is valid
        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $auth = app('firebase.auth');
        $username = validateUsername($username, $db);
        $user = null;
        
        if($username === false){
            return view("errors.406");
        }
        

        try{
            $user = $auth->getUser($username['uid']);
        }
        catch(FirebaseException $e){
            return view("errors.406");
        }

        $ideaList = [];
        $storiesList = [];

        if(Session::get('user') === null){
            return view('errors.405');
        }

        $me = Session::get('user');

        if($username){
            $ideas = $db->collection("Ideas");
            $ideas = $ideas->where('group', 'array-contains-any', [
                [
                    "uid" => $username['uid'],
                    "role" => "viewer"
                ],
                [
                    "uid" => $username['uid'], 
                    "role" => "owner"
                ],
                [
                    "uid" => $username['uid'], 
                    "role" => "recommended"
                ]
            ])->documents();

            $postcardsList = $wishlistList = [];

            $postcards = $db->collection("Stories");
            $postcards = $postcards->where("clientID","=", $user->uid)->documents();

            foreach($postcards as $postcard){
                $liked = false;
                $postcard = $postcard->data();
                $postcard["author"] = $user;

                if(Session::get('user') !== null){
                    if(in_array(Session::get('user')->uid, (array) $postcard["likes"], true) == true){
                        $liked = true;
                    }   
                }

                $postcard["liked"] = $liked;
                $postcard["timestamp"] = Carbon::createFromFormat("Y-m-d", $postcard["createdAt"])->timezone('America/New_York')->timestamp;

                if($postcard["archived"] == 0){
                    array_push($postcardsList,$postcard);
                }

                
            }


            $postcardsList = collect($postcardsList)->sortBy('timestamp')->reverse()->toArray();

            $wishlist = $db->collection("Wishlists");
            $wishlists = $wishlist->where("clientID","=",$user->uid)->where("archived", "=",0)->documents();

            foreach($wishlists as $wishlist){
                array_push($wishlistList,$wishlist->data());
            }

            foreach($ideas as $idea){

                if($idea["archived"] == 0){
                    try{

                        $author = $auth->getUser($idea['clientID']);
                        $idea = $idea->data();
                        $idea["author"] = (array) $author;
                        $isTravel = true;
                        if(array_key_exists("type", $idea) && $idea["type"] == "activity" || $idea["clientID"] == "mnTmABt3MDOWyy76pwG7V0QACpD2"){
                            $isTravel = false;
                        }
                        if($isTravel == true){
                            $departure = $checkIn = Carbon::createFromFormat('Y-m-d', str_replace("/","-",$idea["departure"]))->timezone('America/New_York');
                            $today = Carbon::now()->timezone('America/New_York');
                            $daysRemaining = (int) $today->diffInDays($departure, false);
                            $checkOut = $checkIn->addDays($idea["days"]);
                            $idea["checkOut"] = $checkOut;
                            $idea["daysRemaining"] = $daysRemaining;
                        }
                        $idea["isTravel"] = $isTravel;
                        $idea["groupSize"] = getGroupSize($idea["group"]);

                        //

                        if($idea["clientID"] == "mnTmABt3MDOWyy76pwG7V0QACpD2" && $user->customClaims["username"] != $me->customClaims["username"]){
                            continue;
                        }

                        array_push($ideaList, $idea);

                    } catch(FirebaseException $e){

                    }
                    
                }
            }


            $compatibility = calcCompatibility(Session::get('user'), $user);
            if($compatibility == ""){
                $compatibility = "nocompat 0";
            }
            if(isProfileComplete($user)){
                $complete = true;
            }

            $data = [
                "user" => $user,
                "compatibility" => explode(" ",$compatibility) ?? null,
                "complete" => $complete,
                "groups" => $ideaList,
                "postcards" => $postcardsList,
                "wishlists" => $wishlistList
            ];

            return view("users.read", $data);
        }
        return view("errors.406");
    }

    
}
