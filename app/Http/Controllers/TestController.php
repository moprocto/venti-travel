<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Kreait\Firebase\Contract\Messaging;
use Kreait\Firebase\Messaging\CloudMessage;
use Kreait\Firebase\Messaging\Notification as Notification;
use Kreait\Firebase\Messaging\ApnsConfig as ApnsConfig;
use Mail;
use Session;
use Newsletter;
use Carbon\Carbon;
use DB;
use Exception;
use Redirect;
use Duffel;
use Dwolla;

use App\Models\Group as Groups;
use App\Models\Connection as Connections;
use App\Models\Claim as Claims;
use App\Models\Member as Members;
use App\Models\Interest as Interests;
use App\Models\Language as Languages;
use App\Models\Wishlist as Wishlists;
use App\Models\Setting as Settings;
use App\Models\Recommendation as Recommendations;
use Kreait\Firebase\Exception\FirebaseException;

use Illuminate\Support\Str;

use Postmark\PostmarkClient;

class TestController extends Controller
{
    public function dbSync(){

        // this script syncs the firestore databases of users to a SQL format
        // run this first

        $auth = app('firebase.auth');
        $usersList = [];
        $users = $auth->listUsers($defaultMaxResults = 1500, $defaultBatchSize = 1500);
        $emailList = [];
        $firestore = app('firebase.firestore');
        $db = $firestore->database();

        DB::table('members')->truncate();
        DB::table('claims')->truncate();
        DB::table('interests')->truncate();
        DB::table('languages')->truncate();
        DB::table('groups')->truncate();
        DB::table('wishlists')->truncate();
        DB::table('settings')->truncate();
        DB::table('tokens')->truncate();
        DB::table('connections')->truncate();

        $groups = $db->collection("Ideas")->documents();

        foreach($groups as $group){
            $group = $group->data();
            if(array_key_exists("clientID", $group)){
                DB::table('groups')->insert([
                    "archived" => $group["archived"],
                    "type" => $group["type"] ?? "Travel",
                    "days" => $group["days"],
                    "privacy" => $group["privacy"] ?? "Anyone Can Request to Join",
                    "destination" => $group["destination"] ?? null,
                    "departure" => $group["departure"],
                    "sizePref" => $group["sizePref"] ?? 0,
                    "title" => $group["title"],
                    "country" => $group["country"],
                    "uid" => $group["clientID"],
                    "description" => $group["description"],
                    "notes" => $group["notes"] ?? null,
                    "tripID" => $group["tripID"]
                ]);

                

                /*
                // fix privacy issues


                $ideaDoc = $db->collection("Ideas")->document($group["tripID"]);
                if(array_key_exists("privacy", $group) && $group["privacy"] == "full"){
                    $update = $ideaDoc->update([
                        ["path" => "privacy","value" => "Invitation Only. Invisible to Others"]
                    ]);
                }
                */

                if($group){

                }
                

                // insert group members

                $members = $group["group"] ?? [];


                foreach($members as $member){

                    DB::table('connections')->updateOrInsert([
                        "tripID" => $group["tripID"],
                        "uid" =>$member["uid"],
                        "role" => $member["role"]
                    ]);
                }
            }
            else {
                //echo json_encode($group);
            }
        }

        $wishlists =  $db->collection("Wishlists")->documents();


        foreach($wishlists as $wishlist){

            if(!is_null($wishlist["destination"])){
                //dd($wishlist);
                DB::table('wishlists')->insert([
                "archived" => $wishlist["archived"],
                "month" => $wishlist["month"],
                "year" => $wishlist["year"],
                "privacy" => $wishlist["privacy"],
                "destination" => $wishlist["destination"] ?? $wishlist["country"],
                "country" => $wishlist["country"],
                "uid" => $wishlist["clientID"],
                "wishlistID" => $wishlist["wishlistID"]
            ]);
            }
            
            
        }


        foreach($users as $user){

            $interests = $languages = [];
            $customClaims = $user->customClaims;

            if($customClaims == []){
                continue;
            }

            if(array_key_exists("interests", $customClaims)){
                $interests = $customClaims["interests"];
                unset($customClaims["interests"]);
            }
            if(array_key_exists("languages", $customClaims)){
                $languages = $customClaims["languages"];
                unset($customClaims["languages"]);
            }

            if(!array_key_exists("smsVerified", $customClaims)){
                $customClaims["smsVerified"] = 0;
            }
            if(!array_key_exists("gender", $customClaims)){
                $customClaims["gender"] = "none";
            }
            if(!array_key_exists("dob", $customClaims)){
                $customClaims["dob"] = 0;
            }
            if(!array_key_exists("smoking", $customClaims)){
                $customClaims["smoking"] = 0;
            }
            if(!array_key_exists("drinking", $customClaims)){
                $customClaims["drinking"] = 0;
            }
            if(isset($user->providerData[0]->providerId)){
                $providerData = $user->providerData[0]->providerId;
            }

            if(strlen($user->uid) <= 20){
                $providerData = "facebook.com";
            }

            $lastLogin = $user->metadata->lastLoginAt;

            if($lastLogin){
                $lastLogin = $lastLogin->format('Y-m-d H:i:s');
            }


            DB::table('members')->insert([
                "uid" => $user->uid,
                "emailVerified" => $user->emailVerified,
                "email" => $user->email,
                "displayName" => $user->displayName,
                "photoUrl" => $user->photoUrl ?? "/assets/img/profile.jpg",
                "createdAt" => $user->metadata->createdAt->format('Y-m-d H:i:s'),
                "disabled" => $user->disabled,
                "source" => $providerData,
                "last_login" =>  $lastLogin
            ]);


            $gender = $customClaims["gender"];

            if($gender == "Male"){
                $gender = "men";
            }

            $age = null;

            if(isset($customClaims["dob"]) && $customClaims["dob"] != null && $customClaims["dob"] != 0){
                $age = calculateAge($customClaims["dob"]);
            }   

            DB::table('claims')->insert([
                "uid" => $user->uid,
                "username" => $customClaims["username"],
                "gender" => $gender ?? "nil",
                "smoking" => $customClaims["smoking"] ?? "I Don't Smoke",
                "drinking" => $customClaims["drinking"] ?? "I Don't Drink",
                "dob" => $customClaims["dob"] ?? 0,
                "age" => $age,
                "bio" => str_replace(",","-",$customClaims["bio"] ?? ""),
                "occupation" => str_replace(",","-",$customClaims["occupation"] ?? ""),
                "instagram" => str_replace(",","",$customClaims["instagram"] ?? "nil"),
                "requests" => $customClaims["requests"] ?? "nil",
                "genderPref" => $customClaims["genderPref"] ?? "No Preference",
                "sizePref" => $customClaims["sizePref"] ?? "No Preference",
                "agePref" => $customClaims["agePref"] ?? "No Preference",
                "spendingPref" => $customClaims["spendingPref"] ?? "No Preference",
                "smokingPref" => $customClaims["smokingPref"] ?? "No Preference",
                "drinkingPref" => $customClaims["drinkingPref"] ?? "No Preference",
                "religion" => $customClaims["religion"] ?? "No Preference",
                "smsVerified" => $customClaims["smsVerified"] ? "Y" : "N"
            ]);

            $x = 0;

            foreach((array) $interests as $interest){
                if($x <= 3){
                    DB::table('interests')->insert([
                    "uid" => $user->uid,
                    "interest" => $interest["value"] ?? interestEmoji($interest) . " " . $interest
                ]);
                }
                else {
                    $x = 0;
                    break;
                }
                $x++;
            }

            foreach($languages as $language){
                DB::table('languages')->insert([
                    "uid" => $user->uid,
                    "language" => $language["value"] ?? $language
                ]);
            }

            unset($user->customClaims);
            unset($user->providerData);
            unset($user->metadata);
            unset($user->tokensValidAfterTime);
        }

        // pull in groups
        
        $members = Members::all();

        foreach($members as $member){

            $hasWishlist = $member->wishlist->count();
            
            $hasInterests = $member->interests->count();

            $hasBio = strlen($member->customClaims["bio"] ?? "");

            $hasOccupation = strlen($member->customClaims["occupation"] ?? "");

            DB::table('members')->where('uid', $member->uid)->update([
                "hasWishlist" => ($hasWishlist > 0) ? "Y" : "N" , 
                "hasInterests" => ($hasInterests > 0) ? "Y" : "N" ,
                "hasBio" => ($hasBio > 14) ? "Y" : "N",
                "hasOccupation" => ($hasOccupation > 0) ? "Y" : "N"
            ]);

        }

        $fbusers = $db->collection("users")->listDocuments();

        foreach($fbusers as $user){
            $uid = str_replace("users/","",$user->path());
            $doc = $user->collection("Settings")->document("data");
            $settingsData = $doc->snapshot()->data();

            if(!is_null($settingsData)){
                // store the settings data
                foreach($settingsData as $key => $value){
                    DB::table('settings')->insert([
                        "uid" => $uid,
                        "rule" => $key,
                        "value" => $value
                    ]);
                }
            }

            $tokenDoc = $user->collection("Tokens")->document("data");
            $token = $tokenDoc->snapshot()->data();

            if(!is_null($token)){
                DB::table('tokens')->insert([
                    "uid" => $uid,
                    "token" => $token["id"],
                    "timestamp" => $token["timestamp"]
                ]);
            }
        }

        return Redirect::to('/admin');
    }


    public function cleanRecommendations(){
        // First, we're going to query all existing recommendations and delete them if they failed for form.

        $firestore = app('firebase.firestore');
        $db = $firestore->database();

        $previousRecommendations = Recommendations::where('deleted', 0)->get();
        $nulls = [];

        foreach($previousRecommendations as $previousRecommendation){
            
            $timeAgo = Carbon::parse($previousRecommendation->created_at)->diffInDays();
            if($timeAgo > 14){
                $group = Groups::where("tripID", $previousRecommendation->tripID)->first();
                $connections = Connections::where("tripID", $previousRecommendation->tripID)->get();
                if(!isset($group)){
                    $docRef = $db->collection("Ideas")->document($previousRecommendation->tripID)->snapshot()->data();
                    if($docRef == null){
                        echo "$previousRecommendation->tripID <br>";
                        
                        Recommendations::where("tripID", $previousRecommendation->tripID)->update([
                            "deleted" => 1
                        ]);
                        
                    }                    
                }else{
                    if($group->numMembers() < 2){
                        echo "$group->tripID <br>";
                        // after two weeks, this group has failed to garner at least 50% opt-in
                        // we are going to remove it from our databases
                        
                        Recommendations::where("tripID", $group->tripID)->update([
                            "deleted" => 1
                        ]);
                        

                        $db->collection("Ideas")->document($group->tripID)->delete();
                    }
                }
            }
        }

        // we're also going to auto archive any group that is at least 60 days in the past.

        $groups = Groups::all();

        foreach($groups as $group){
            if(strtolower($group->type) == "travel" && $group->archived == 0){
                $timeAgo = Carbon::parse($group->departure);
                $now = Carbon::now()->diffInDays($timeAgo, false);
                if($now < -60){
                    // archive
                    $ideaDoc = $db->collection("Ideas")->document($group->tripID);
                    $update = $ideaDoc->update([
                        ["path" => "archived","value" => 1]
                    ]);
                }
            }
        }
    }

    public function sendGroupRecommendation(){
        $firestore = app('firebase.firestore');
        $db = $firestore->database();

        /*
            This function is designed to send every capable user a predesigned group for them to join and discuss common interests and new destinations. For now, this is a manual process to carefully make improvements.

            We must run the dbSync() function first to reduce the number of queries to FB
        */



        

        // the above should be a different function to tbh
        

        $firestore = app('firebase.firestore');
        
        $start = strtotime("now");
        set_time_limit(120);
        $members = Members::where('emailVerified',1)->where('photoUrl','!=','/assets/img/profile.jpg')->where('email','!=',null)->where('uid','!=',"mnTmABt3MDOWyy76pwG7V0QACpD2")->inRandomOrder()->get();
        // we get all users in random order


        $profilesAnalyed = $profilesSkipped = $formationAttempts = $chemistryFails = $groupSizeFails = $groupsFormed = 0;

        $omit = [];
        // When we form a group recommendation, we omit the set from the pass through to prevent double-recs


        $accidents = ["eddyatallahbey@gmail.com",
            "reshanatappin@gmail.com",
            "buithi.thaotruc@gmail.com",
            "rezqmohammed131@gmail.com",
            "r_schmiechen@outlook.com",
            "namsioner@gmail.com",
            "fortunestaunton@gmail.com",
            "exe1109@hotmail.com",
            "covenent0314@gmail.com",
            "n6mcn6shrm@privaterelay.appleid.com",
            "briannac.ford@gmail.com",
            "trangdai0711@gmail.com",
            "nyghazi@gmail.com",
            "rannjella@yahoo.com",
            "stavinohova.klara@centrum.cz",
            "amrosaleem@yahoo.com",
            "gtv.povilas@gmail.com",
            "tarmorrow@gmail.com",
            "neupaneprakash70@gmail.com"];

            foreach($accidents as $accident){
                $accident = Members::where("email", $accident)->first();
                array_push($omit, $accident->uid);
            }


        $tryAgains = [];
        // some users get unlucky from the math. We'll store them separately to try one last time. This is not critical because we run this weekly

        $groups = Groups::where('uid', "mnTmABt3MDOWyy76pwG7V0QACpD2")->orderBy("id","desc")->get();



        foreach($groups as $group){
            $connections = $group->connections;

            foreach($connections as $connection){
                $createdAt = Carbon::parse($connection->created_at)->diffInDays();

                if($connection->role == "recommended"){
                    if($createdAt > 13){
                        // this person has not responded to their last request in at least 13 days.
                        array_push($omit, $connection->uid);
                    }
                }
            }
        }
        $omit = array_unique($omit);

        echo "Omitting " . sizeof($omit) . " from process because they've yet to respond to previous recommendations in more than 13 days<br>";

        foreach($members as $member){
            $profilesAnalyed++;
            if($member->isComplete()){
                if($member->isRecommendable() !== true || in_array($member->uid, $omit)){
                    //echo "$member->displayName is not recommendable<br>";
                    $profilesSkipped++;
                    continue;
                }

                // we are pulling a user's past groups to make sure they're not too similar.
                $pastGroups = [];
                $pastConnections = $member->connections;

                foreach($pastConnections as $pastConnection){
                    array_push($pastGroups, $pastConnection->group);
                }


                $member = $member->toArray();

                $groupList = $this->groupList($member, $omit, $pastGroups);

                if(!$groupList){
                    continue;
                }

                $tryAgains = array_merge($tryAgains, $groupList["tryAgains"]);
                $groupList = $groupList["list"];

                print_r($tryAgains);
               
                // by this point. We have a group of four people! Everyone is compatible with each other. 
                // let's get some visuals

                $table = $this->structureGroup($groupList, $db);
                            
                $groupsFormed++;
                echo $table;

                // now we need to omit each person so they don't get another recommendation

                foreach($groupList as $groupListMember){
                    array_push($omit, $groupListMember["uid"]);
                }
                     
            }
            else {
                // the profile is not complete
                $profilesSkipped++;
            }
        }
        echo "<h3>Total Profiles: " . Members::all()->count() . "</h3>";
        echo "<h3>Profiles Analyzed: $profilesAnalyed</h3>";
        echo "<h3>Profiles Skipped: $profilesSkipped</h3>";
        echo "<h3>Groups Formation Fails due to size < 4: $groupSizeFails</h3>";
        echo "<h3>Groups Formed: $groupsFormed</h3>";

        echo "<h3>Try Agains: " . sizeof($tryAgains) . "</h3>";

        print_r($tryAgains);

        foreach($tryAgains as $tryAgain){

        }

        $finish = strtotime("now");
        $elapsed = $finish - $start;

        echo "<h2>Elapsed Time: $elapsed</h2>";

    }

    public function nudgeToComplete(){
        // this email goes to those that have verified their emails, but are not recommendable yet
        // last sent March 4 at 2:55 p.m.
        $members = Members::where('emailVerified', 1)->get();
        $i = 0;
        foreach($members as $member){
            if(!$member->isRecommendable()){
                // send email
                
                $email = $member->email;
                
                $data = [
                    "user" => $member 
                ];

                $subject = "Complete Your Venti Profile";
                try{
                    Mail::send('emails.application.nudgeToComplete', array("data" => $data), function($message) use($email, $subject)
                        {
                            $message
                                ->to($email)
                                ->replyTo("no-reply@venti.co")
                                ->subject($subject);
                        });
                    $i++;
                }
                
                catch(Exception $e){
                    //
                }
                $i++;
            }
        }

        echo "$i emails sent";
    }

    public function getRandomCompatibleUserThatsNotMatcher($me, $groupMembers, $omit, $pastGroups){
        $repeat = true;
        $retryCount = 1;
        $matchees = Members::where('emailVerified',1)->where('photoUrl','!=','/assets/img/profile.jpg')->where('uid','!=',"mnTmABt3MDOWyy76pwG7V0QACpD2")->where("uid","!=",$me["uid"])->inRandomOrder()->limit(200)->get();

        foreach($matchees as $matchee){
            if($matchee->isRecommendable() === true){
                $uidArray = [];

                foreach($groupMembers as $groupMember){
                    array_push($uidArray, $groupMember["uid"]);
                }

                if(
                    $matchee->isComplete() && // the user has all the elements of a completed profile
                    isset($matchee->customClaims) && // 
                    !in_array($matchee->uid, $uidArray) && // not adding 'them'
                    !in_array($matchee->uid, $omit) // not adding if they are in another recommendation group
                ) {
                    $matchee = $matchee->toArray();


                    // we need to see if this person has already been recommended to me in the past two weeks
                    // a quality recommendation is not recommending me to the same 2 or three people over and over again

                    $freshRec = false;

                    foreach($pastGroups as $groupConnection){
                        $group = $groupConnection->group ?? null;
                        if(!is_null($group)){
                            $memberUIDs = $group->groupMemberUIDs();
                        } else {
                            $freshRec = true;
                        }
                    }

                    if(sizeof($pastGroups) == 0){
                        $freshRec = true;
                    }
                    
                    if($freshRec == true){
                        $compatibility = explode(" ", calcCompatibility((object) $me, (object) $matchee))[1];

                        if($compatibility > 80){
                            // at least 80%
                            $repeat = false;


                            return $matchee;
                        }
                    }
                    
                    
                }   
            }else{
                if($matchee->uid != $me["uid"]){ // this is redundant
                    array_push($omit, $matchee->uid);
                    // we can save resources by adding this non-recommendable user to the omit array
                    // not adding myself of course
                }
            }
            $retryCount++;

            if($retryCount > 200){
                $repeat = false;
            }
        }

        return false;
    }

    public function groupList($member, $omit, $groups){

        if(is_string($member)){
            $member = Members::where('uid', $member)->first()->toArray();
        }

        $tryAgains = [];
        $groupList = [$member];

        for($i = 0; $i < 3; $i++){
            $newMember = $this->getRandomCompatibleUserThatsNotMatcher($member, $groupList, $omit, $groups);

            if(!$newMember){
                $newMember = $this->getRandomCompatibleUserThatsNotMatcher($member, $groupList, $omit, $groups);
            }

            if($newMember != false){
                array_push($groupList, $newMember);
            }
        }
        
        if(sizeof($groupList) != 4){
            // we tried to find a compatible user but could not
            //echo "We could not form a group for " . $member["displayName"] . ". Size accomplished: " . sizeof($groupList) . "<br>";

            
            // we'll try one more time with this person 
            array_push($tryAgains, $groupList[0]["uid"]);
            return false;
        }

        // we need to make sure everyone in the group is compatible with each other
        // for now we can do it mechanically since the group size is static and there are not that many calculations left

        $chemistryFail = false;

        $calc1 = (int) explode(" ", calcCompatibility((object) $groupList[1], (object) $groupList[2]))[1];
        $calc2 = (int) explode(" ", calcCompatibility((object) $groupList[1], (object) $groupList[3]))[1];
        $calc3 = (int) explode(" ", calcCompatibility((object) $groupList[2], (object) $groupList[3]))[1];

        // every member of the group needs to be compatible with the other

        if($calc1 < 80 || $calc2 < 80 || $calc3 < 80){
            $chemistryFail = true;
            // everyone is not compatible. Try again later
            array_push($tryAgains, $groupList[0]["uid"]);
            return false;
        }

        return ["list" => $groupList, "tryAgains" => $tryAgains];
    }

    public function structureGroup($groupList, $db){
        $table = "<table style='width:600px; margin: 0 auto; border-spacing:5px 10px;'><tr>";

        $groupID = Str::random(20);

        for($i = 0; $i < 4; $i++){
            // needs deep linking for mobile app
            $bio = $groupList[$i]['customClaims']['bio'];
            if($bio == ""){
                $bio = $groupList[$i]['customClaims']['occupation'] ?? "";
            }
            if($bio == ""){
                $bio = getWishlistString($groupList[$i]["uid"]);
            }
            $table .= "<td style='text-align:center;width:200px; border:1px solid rgba(0,0,0,0.2); padding:10px; border-radius:20px;'><a href='https://venti.co/u/" . $groupList[$i]['customClaims']['username'] . "' target='_blank'><img src='" . $groupList[$i]['photoUrl'] . "' style='width:200px'></a><br>
                <p style='font-weight:bold;'>" . ucfirst(getFirstName($groupList[$i]['displayName'])) . ", " . calculateAge($groupList[$i]['customClaims']['dob']) . "</p>";

            if($bio != ""){
                $table .=  "<p style='min-height:50px;'>" . $bio. "</p>";
            }

            // add the user's interests


            $interests = Interests::where('uid', $groupList[$i]["uid"])->get();
            if($interests->count() != 0){
                $table .= "<span style='font-weight:bold; font-size:11px;'>My Interests:</span><br>";
            }
            foreach($interests as $interest){
                $table .= '<span style="font-size: 11px; border: 1px solid rgba(0,0,0,0.3); display: inline-block; text-transform:  uppercase; font-weight: bold; padding: 5px 10px; border-radius: 20px; margin: 5px; background-color:whitesmoke;">' . $interest->interest . "</span>";
            }

            $table .= "<span style='font-weight:bold; font-size:11px; display:block; margin-top:5px; margin-bottom:5px;'>My Languages:</span>";

            $languages = Languages::where('uid', $groupList[$i]["uid"])->get();

            foreach($languages as $language){
                $table .= '<span style="font-size: 11px; border: 1px solid rgba(0,0,0,0.3); display: inline-block; text-transform:  uppercase; font-weight: bold; padding: 5px 10px; border-radius: 20px; margin: 5px; background-color:white;">' . $language->language . "</span>";
            }

            $table .= "</td>";

            if($i == 1){
                $table .= "</tr><tr>";
            }

            if($i == 3){
                $table .= "</tr></table>";
            }
            
            // we store a record of this for analytics purposes
            
           DB::table('recommendations')->insert([  "tripID" => $groupID, "uid" => $groupList[$i]["uid"] ]);
            

            // we check to see if notifcations are enabled for this user
        }

                // we create the record in the firebase database
                
                
                $docRef = $db->collection("Ideas")->document($groupID)->set(
                    [             
                        "tripID" => $groupID,
                        "archived" => 0,
                        "budgetOverall" => 0,
                        "country" => null,
                        "latitude" => null,
                        "longitude" => null,
                        "destination" => null,
                        "title" => "New Group Suggestion",
                        "departure" => null,
                        "days" => null,
                        "foodBudget" => 0,
                        "lodgingBudget" => 0,
                        "transportationBudget" => 0,
                        "activityBudget" => 0,
                        "tags" => [],
                        "imageURL" => null,
                        "imageAuthor" => null,
                        "imageAuthorProfile" => null,
                        "personalizationCost" => 0,
                        "personalizationNote" => "",
                        "purchasePrice" => 0,
                        "purchaseNote" => "",
                        "clientID" => "mnTmABt3MDOWyy76pwG7V0QACpD2", // venti admin will always be the default owner
                        "description" => "We think you all would make a great group based on your travel preferences and interests. If everyone accepts this recommendation, we'll enable the chat and video call features.",
                        "worldee" => "",
                        "sizePref" => 4,
                        "privacy" => "Invitation Only. Invisible to Others",
                        "group" => [
                            0 => [
                                "uid" => "mnTmABt3MDOWyy76pwG7V0QACpD2",
                                "role" => "owner"
                            ],
                            1 => [
                                "uid" => $groupList[0]["uid"],
                                "role" => "recommended"
                            ],
                            2 => [
                                "uid" => $groupList[1]["uid"],
                                "role" => "recommended"
                            ],
                            3 => [
                                "uid" => $groupList[2]["uid"],
                                "role" => "recommended"
                            ],
                            4 => [
                                "uid" => $groupList[3]["uid"],
                                "role" => "recommended"
                            ]
                        ],
                        "itinerary" => "",
                        "type" => "Activity",
                        "recommended" => true,
                        "color" => getRandomColor()
                    ]
                );

                

                for($i = 0; $i < 4; $i++){
                    // we loop over the group again to communicate about the new group
                    $user = Members::where('uid',$groupList[$i]["uid"])->first();

                    // 
                    
                        $email = $user->email;
                        $subject = "New Travel Group Recommendation from Venti";
                        $name = getFirstName($user->displayName);
                        $data = [
                            "tripID" => $groupID,
                            "table" => $table,
                            "name" => $name
                        ];

                        //check if we can send this person a push notification


                        $pushable = $user->token->token ?? false;

                        // 

                        if(!$pushable){
                            // we can't send a push notification
                            $settings = Settings::where('uid', $user->uid)->where("rule","emailGroupRecommendations")->first();

                                if(!isset($settings) || $settings->value == "1"){
                                    Mail::send("emails.application.recommendation", array('data' => $data), function($message) use($email, $subject, $name)
                                        {
                                            $message
                                                    ->to($email, "$name")
                                                    ->replyTo("no-reply@venti.co")
                                                    ->subject($subject);
                                    });
                                }
                        } else {
                            // we have a token to send a push notification

                            $message = CloudMessage::withTarget('token', $pushable)
                                ->withNotification(
                                        Notification::create('New Group Recommendation', "We think the four of you would make an awesome group.")
                                    );
                            $messaging = app('firebase.messaging');
                            try{
                                $messaging->send($message);
                            } catch(Exception $e){

                            }

                            // still send an email

                            $settings = Settings::where('uid', $user->uid)->where("rule","emailGroupRecommendations")->first();

                            if(!isset($settings) || $settings->value == "1"){
                                Mail::send("emails.application.recommendation", array('data' => $data), function($message) use($email, $subject, $name)
                                    {
                                        $message
                                                ->to($email, "$name")
                                                ->replyTo("no-reply@venti.co")
                                                ->subject($subject);
                                });
                            }
                        }

                    // we need to push the user to the omit array so we don't repeat them for someone else

                }
                
                
        return $table;
    }

    public function sendGroupReminder($uid){
        // we check all previous recommendations and send a reminder if no response

        /*
            $client = new PostmarkClient("e42b3768-8dd2-4b3d-b204-337e62788bc1");
            $fromEmail = "markus@venti.co";
            $toEmail = "markus@venti.co";
            $subject = "Hello from Postmark";
            $htmlBody = "<strong>Hello</strong> dear Postmark user.";
            $textBody = "Hello dear Postmark user.";
            $tag = "example-email-tag";
            $trackOpens = true;
            $trackLinks = "None";
            $messageStream = "outbound";

            // Send an email:
            $sendResult = $client->sendEmail(
              $fromEmail,
              $toEmail,
              $subject,
              $htmlBody,
              $textBody,
              $tag,
              $trackOpens,
              NULL, // Reply To
              NULL, // CC
              NULL, // BCC
              NULL, // Header array
              NULL, // Attachment array
              $trackLinks,
              NULL, // Metadata array
              $messageStream
            );
        */
    }

    public function dobErrors(){
        $auth = app('firebase.auth');
        $usersList = [];
        $users = $auth->listUsers($defaultMaxResults = 1000, $defaultBatchSize = 1000);
        $emailList = [];

        foreach($users as $user){
            $dob = $user->customClaims["dob"] ?? null;
            if($dob == null || $dob == 0){
                $createdAt = $user->metadata->createdAt->format('Y-m-d');
                $daysSince = Carbon::parse($createdAt)->diff(Carbon::now())->days;

                if($user->emailVerified == false){
                    array_push($emailList, ["email" => $user->email, "daysSince" => $daysSince]);
                    echo "$user->email, $daysSince <br>";
                }

            }
        }
    }

    public function updateAvatar(){
        $auth = app('firebase.auth');
        $user = $auth->getUserByEmail("kevinhoangvu@gmail.com");

        $customClaims = $user->customClaims;
        $customClaims["middleName"] = "Hoang";

        dd($user);

        $t = $auth->setCustomUserClaims($user->uid, $customClaims);

        
        dd($t);
       

        $updatedUser = $auth->updateUser($user->uid, $properties);

        $user = $auth->getUser($uid);
    }

    public function octopusSync(){
        $auth = app('firebase.auth');
        $usersList = [];
        $users = $auth->listUsers($defaultMaxResults = 3000, $defaultBatchSize = 3000);
        $subscribed = $unsubscribed = 0;
        $new = 0;
        $removed = 0;
        $skips = [];
        $firestore = app('firebase.firestore');
        $db = $firestore->database();

        // loop through all dwolla customers

        $customerDocs = $db->collection('Customers')->documents();
        $customers = [];

        $users = ["rufshana@icloud.com", "jazzhole@gmail.com", "arbermuharemi@gmail.com", "suhaibzahid940@gmail.com", "mani.chandrapatla2@gmail.com", "ekuhaupt@gmail.com", "jshirolikar@yahoo.com", "quyen.tran.112@gmail.com", "shiddentrvl@nc.rr.com", "matthomandersloot@hotmail.com", "ksong12@gmail.com", "ashlielambrecht12@gmail.com", "eric.cevallos@gmail.com", "joshsilvanyc@gmail.com", "nightfir@onmail.com", "gideonsism@outlook.com", "r_vladislav@yahoo.com", "kelly.reyes95@gmail.com", "milo.tcwen@gmail.com", "kliu1209@gmail.com", "enuno15@gmail.com", "ryandhalsey@gmail.com", "28hmckoy@gmail.com", "iknowiknowme@gmail.com", "irobertson511@gmail.com", "abrahamchoi@gmail.com", "aag2114@gmail.com", "justinchilelli@gmail.com", "matthewberardi1@gmail.com", "markvliet@hotmail.com", "amylld72@gmail.com", "budo1@umbc.edu", "proctor.markus@gmail.com", "tuancnguyen@gmail.com", "nvshukla@gmail.com", "justinwilliams317@yahoo.com", "kevzho141@gmail.com", "cathie.roering@gmail.com", "barnesconnor33@gmail.com", "leeklderek@gmail.com", "jxieeducation@gmail.com", "meghavenga@gmail.com", "bpant2@yahoo.com", "acura108@yahoo.com", "shah.nemi@outlook.com", "brandonhartmann@gmail.com", "dinopapadi@yahoo.com", "misha680@gmail.com", "william.p.clements@gmail.com", "notmikekramer@gmail.com", "cherukarabobby@live.com", "naureenzahid0@gmail.com", "helfrich.courtney@gmail.com", "stullbarbara26@gmail.com", "jesandmimi@gmail.com", "lego606@aol.com", "eehpyoung@hotmail.com", "newsletter@flyenpoints.com", "laurie.bernhard072@gmail.com", "elyseeriksson@gmail.com", "valennzuela4583@gmail.com", "acura108@icloud.com", "kylechase23@gmail.com", "happystars808@gmail.com", "kwcortum@gmail.com", "da8088@gmail.com", "rkannah@gmail.com", "markalbert@mac.com", "liennguyen0927@gmail.com", "adisonallen@gmail.com", "montana.mcleod@yahoo.com", "devinbanner111@yahoo.com", "vinhton.survey.promo@gmail.com", "michellemarye@gmail.com", "kevinhoangvu@gmail.com", "bluelintrain8@aol.com", "itspk87@gmail.com", "christopher.dettling@gmail.com", "investor271@gmail.com", "5kzamotorpizdec@gmail.com", "charleshyde@gmail.com", "wardaddyy92@gmail.com", "minsoomachado@gmail.com", "jhayes806@gmail.com"];
        
        
        foreach($users as $user){
            // check if the user is already in the octopus list
            $memberID = md5($user);

            $url = "https://emailoctopus.com/api/1.6/lists/3b3c3c3a-3b19-11ee-9258-cf345e7859db/contacts/$memberID";

            $result = Http::get($url, [
                'api_key' => env('EMAIL_OCTOPUS_API_KEY')
            ]);

            $result = json_decode($result->body());
            $id = $result->id ?? false;

            if($id){

                if($result->status == "SUBSCRIBED"){
                    // inform octopus that they have a Venti account


                    $url = "https://emailoctopus.com/api/1.6/lists/3b3c3c3a-3b19-11ee-9258-cf345e7859db/contacts/$memberID";

                    $result = Http::put($url, [
                        'api_key' => env('EMAIL_OCTOPUS_API_KEY'),
                        "tags" => [
                            "onVenti" => true
                        ]
                    ]);

                }

                // only update the status of subscribed members


                try{
                    $user = $auth->getUserByEmail($user);

                    foreach($customerDocs as $customer){
                        $customer = $customer->data();
                        
                        if($user->uid == $customer["uid"]){
                            $result = Http::put($url, [
                                'api_key' => env('EMAIL_OCTOPUS_API_KEY'),
                                "tags" => [
                                    "boardingpass" => true
                                ]
                            ]);
                        }
                    }
                }
                catch(FirebaseException $e){

                }
            }
            else {
                // the person does not exist

                // add to the list

                $result = Http::post("https://emailoctopus.com/api/1.6/lists/3b3c3c3a-3b19-11ee-9258-cf345e7859db/contacts", [
                    'api_key' => env('EMAIL_OCTOPUS_API_KEY'),
                    'email_address' => $user
                ]);

                $new++;
            }


            // 


        }


        dd("f");
        

        $users = $firestore;
        

        $firestore = app('firebase.firestore');
        $db = $firestore->database();

        // loop through all dwolla customers

        $customerDocs = $db->collection('Customers')->documents();
        $customers = [];

        foreach($customerDocs as $customer){
            $customer = $customer->data();
            $customer["fundingSources"] = [];

            $fundingSourcesDoc = $db->collection('Sources')->documents();
            $fundingSources = [];

            foreach($fundingSourcesDoc as $fundingSource){
                $fundingSource = $fundingSource->data();

                if($fundingSource["uid"] == $customer["uid"]){
                    array_push($customer["fundingSources"],  $fundingSource);
                }
            }

            // we need to assign our marketing tags to the customer

            $zeroBalance = false;
            $hasFundingSource = true;
            $lowBalance = false;
            $sourcePendingVerification = false;
            $latestBalance = $customer["latestBalance"] ?? null;

            if((float) $latestBalance == 0){
                $zeroBalance = true;
            }
            if((float) $latestBalance < 250){
                $lowBalance = true;
            }

            if(sizeof($customer["fundingSources"]) == 0){
                $hasFundingSource = false;
            }

            $customer["zeroBalance"] = $zeroBalance;
            $customer["lowBalance"] = $lowBalance;
            $customer["hasFundingSource"] = $hasFundingSource;

            $user = $auth->getUser($customer["uid"]);

            $memberID = md5($user->email);

            $url = "https://emailoctopus.com/api/1.6/lists/3b3c3c3a-3b19-11ee-9258-cf345e7859db/contacts/$memberID";

            $result = Http::get($url, [
                'api_key' => env('EMAIL_OCTOPUS_API_KEY')
            ]);

            $result = json_decode($result->body());

            $id = $result->id ?? false;

            if($id){

                if($result->status == "SUBSCRIBED"){
                    // update the contact

                    $url = "https://emailoctopus.com/api/1.6/lists/3b3c3c3a-3b19-11ee-9258-cf345e7859db/contacts/$memberID";

                    $result = Http::put($url, [
                        'api_key' => env('EMAIL_OCTOPUS_API_KEY'),
                        "tags" => [
                            "zeroBalance" => $zeroBalance,
                            "lowBalance" => $lowBalance,
                            "hasFundingSource" => $hasFundingSource
                        ]
                    ]);
                }

                // only update the status of subscribed members
            }
        }
    }

   

    public function sendCustomEmailVerficationLink($user = null){
        $auth = app('firebase.auth');

        if($user == null){
            
            $members = Members::where('emailVerified',0)->where('email','!=',null)->get();
            $i = 0;

            // last email sent was March 4 at 1:20 p.m. EST
            // last email sent March 13 at 5:15 pm est
            // last email was sent April 6 at 1:50 pm EST

            foreach($members as $member){

                    $email = $member->email;


                    $link = $auth->getEmailVerificationLink($email);

                    $data = [
                        "verificationLink" => $link,
                        "user" => $member,
                        "last" => false
                    ];

                    $subject = "Welcome to Venti! You Need to Verify Your Email";


                    try{
                        Mail::send('emails.application.verification', array("data" => $data), function($message) use($email, $subject)
                            {
                                $message
                                        ->to($email)
                                        ->replyTo("no-reply@venti.co")
                                        ->subject($subject);
                        });
                        $i++;
                    }
                    catch(Exception $e){
                        //
                    }
                    

            }

            return "$i emails sent!";
        }

        return "done";

        $email = $user;
        $member = Members::where('email', $email)->first();
        $link = $auth->getEmailVerificationLink($email);
        $data = [
                    "verificationLink" => $link,
                    "user" => $member,
                    "last" => true
                ];
        $subject = "Last Chance to Verify Your Email";
        try{
            Mail::send('emails.application.verification', array("data" => $data), function($message) use($email, $subject)
                {
                    $message
                            ->to($email)
                            ->replyTo("no-reply@venti.co")
                            ->subject($subject);
            });
            $i++;
        }
        catch(Exception $e){
            //
        }

        return "Email sent to $user";

    }

    public function createFakeRecommendation(){
/*
        $auth = app('firebase.auth');
        $firestore = app('firebase.firestore');
        $db = $firestore->database();

        $docRef = $db->collection("Ideas")->document("xxxxxxxxxx")->set(
            [             
                "tripID" => "xxxxxxxxxx",
                "archived" => 0,
                "budgetOverall" => 0,
                "country" => null,
                "latitude" => null,
                "longitude" => null,
                "destination" => null,
                "title" => "New Group Suggestion",
                "departure" => null,
                "days" => null,
                "foodBudget" => 0,
                "lodgingBudget" => 0,
                "transportationBudget" => 0,
                "activityBudget" => 0,
                "tags" => [],
                "imageURL" => null,
                "imageAuthor" => null,
                "imageAuthorProfile" => null,
                "personalizationCost" => 0,
                "personalizationNote" => "",
                "purchasePrice" => 0,
                "purchaseNote" => "",
                "clientID" => "mnTmABt3MDOWyy76pwG7V0QACpD2", // venti admin will always be the default owner
                "description" => "We think you all would make a great group based on your travel preferences and interests. If everyone accepts this recommendation, we'll enable the chat and video call features.",
                "worldee" => "",
                "sizePref" => 4,
                "privacy" => "Invitation Only. Invisible to Others",
                "group" => [
                    0 => [
                        "uid" => "mnTmABt3MDOWyy76pwG7V0QACpD2",
                        "role" => "owner"
                    ],
                    1 => [
                        "uid" => "sswO2vBpt2aRcLNCoGvALJTj59e2",
                        "role" => "recommended"
                    ],
                    2 => [
                        "uid" => "tszLcgRJfvaP1Be1peZMoEEKEZs1",
                        "role" => "recommended"
                    ]
                ],
                "itinerary" => "",
                "type" => "Activity",
                "color" => getRandomColor()
            ]
        );

        //$users = array_unique($users);

*/
        $auth = app('firebase.auth');
        $users = $auth->listUsers($defaultMaxResults = 1500, $defaultBatchSize = 1500);

        echo "<table>";
        foreach($users as $user){
            if($user->emailVerified == false){
                if($user->disabled == true){
                   // array_push($delete, $user->email);
                }
                $member = Members::where('email',  $user->email)->first();
                $lastLogin = $user->metadata->lastLoginAt;
                $hasPhoto = "N";
                    if($lastLogin != null){
                        $lastLogin = Carbon::parse($lastLogin)->diffInDays();
                    }
                    if(isset($member)){
                        if($member->hasPhoto() == true){
                        $hasPhoto = "Y";
                    }
                }

                if($lastLogin >= 159){

                // $this->sendCustomEmailVerficationLink($user->email);
                // reminded on March 22 at 11 am
                // delete on March 24 at 11 am
                
                //echo "<tr><td>" . $user->displayName ."</td><td>" . $user->email . "</td><td>" . $hasPhoto . "</td><td>$lastLogin</td></tr>";
                }
            }
            
        }

        echo "</table><br><br>";

        $delete = ["cassandra.louis@telenet.be",
"humam7585800@gmail.com",
"diazzumaetajoe@gmail.com",
"ayymanu@gmail.com",
"tanit2711@gmail.com",
"glowingplacestravel@gmail.com",
"msembra.22@gmail.com",
"yoga2k20@gmail.com",
"urinboevasabina@gmail.com",
"ahmad1234aldere@gmail.com",
"minorextasy@gmail.com",
"porhapangkhai@gmail.com",
"rbijaya995@gmail.com",
"julietperseverancia18@gmail.com",
"grahamnortonrules@hotmail.com",
"olegmolvin@gmail.com",
"blagicastojcheva@gmail.com",
"helloanavee@gmail.com",
"feyman1@yahoo.com",
"armenvanbuuren@hotmail.com",
"tarik.sahinovic@gmail.com",
"brokenfenced@gmail.com",
"play_please190299@hotmail.com",
"dvvlzz777@hotmail.com",
"miranahmetagic@gmail.com",
"josh_tap@yahoo.com",
"maritesjuncopabila39@gmail.com",
"partapcheema250@gmail.com",
"dudasbence94@gmail.com",
"m_chriz6@yahoo.com",
"business947star@gmail.com",
"martasimoes11@hotmail.com",
"khalil_sabine@live.com",
"dsalojs@gmail.com",
"santosha2001@outlook.com",
"prabhamayi2009@gmail.com",
"aleh7@live.com",
"j.i.a-a-k@hotmail.com",
"rox-leyva1@hotmail.com",
"tristaprince2@gmail.com",
"fateko.mojaa@gmail.com",
"zetaxdinio2@gmail.com",
"pysvwjziul1642053633@gmail.com",
"farhad_nazir@yahoo.com",
"ed_gar623@hotmail.com",
"speranza-lolthe@windowslive.com",
"darius.redeagle@gmail.com",
"brytt_bfs@yahoo.es",
"lofotenisl@gmail.com",
"rex3222@yahoo.com",
"jmarce881008@hotmail.com",
"end_lagarto@hotmail.com",
"acct-cleplus@hotmail.com",
"dana_dnk@yahoo.com",
"keith.duca@gmail.com",
"rami_hanna4@hotmail.com"];


        foreach($delete as $deleted){
            try{
                $user = $auth->getUserByEmail($deleted);
                $auth->deleteUser($user->uid);
                Newsletter::unsubscribe($user->email);
                echo $deleted . " deleted and unsubscribed<br>";
            }
            catch(FirebaseException $e){

            }
        }
    }

   public function pushNotification(){
    
   }

   public function statistics(){
        $members = Members::all();

        $women = $men = $sumWomenAge = $sumMenAge = $private = $sumPrivate = 0;

        foreach($members as $member){
            $dob = $member->customClaims->dob;
            $age = null;
            if($dob != 0){
                $age = calculateAge($dob);
            }
            if(!is_null($age)){
                if($member->customClaims["gender"] == "Female"){
                    $women++;
                    $sumWomenAge += $age;
                }
                if($member->customClaims["gender"] == "men"){
                    $men++;
                    $sumMenAge += $age;
                }
                if($member->customClaims["gender"] == "Prefer Not to Say"){
                    $private++;
                    $sumPrivate += $age;
                }
            }
        }

        $total = $men + $women + $private;

        dd([
            "# of Men: " .  $men,
            "Percent Men: " .  number_format(($men/$total) * 100,2),
            "Avg. Age of Men: " .  number_format($sumMenAge/$men,2),
            "# of Women: " .  $women,
            "Percent Women: " .  number_format(($women/$total) * 100,2),
            "Avg. Age of Women: " .  number_format($sumWomenAge/$women,2),
            "# of Prefer Not to Say: " .  $private,
            "Percent Private: " .  number_format(($private/$total) * 100,2),
            "Avg. Age of Prefer Not to Say: " .  number_format($sumPrivate/$private, 2)
        ]);
   }

   public function manualSMSVerify(){
    // used only when certain phone numbers don't work
    // Bulgaria
    // South Africa
    
        $uid = "jdtjordaan@gmail.com";
        $auth = app('firebase.auth');
        $user = $auth->getUserByEmail($uid);

        $claims = $user->customClaims;
        $claims["smsVerified"] = true;

        $auth->setCustomUserClaims($user->uid, $claims);
   }

   public function deletePlan($planID){
        $firestore = app('firebase.firestore');
        $db = $firestore->database();


        $planRef = $db->collection('Plans')->document($planID);
        $planRef->delete();
   }

   public function testRecommendationEmail(){
    $table = '<table style="width:600px;margin: 0 auto;border-spacing: 5px 10px;"><tbody><tr><td style="text-align:center;width:200px; border:1px solid rgba(0,0,0,0.2); padding:10px; border-radius:20px;"><a href="https://venti.co/u/ngugicynt7269" target="_blank"><img src="https://storage.googleapis.com/download/storage/v1/b/venti-344622.appspot.com/o/6992603-inbound5915401309553414144.jpg?generation=1676791623317508&amp;alt=media" style="width:200px"></a><br>
                            <p style="font-weight:bold;">Cynthia, 21</p>
                            <p style="min-height:50px;">I\'m from Kenya  open minded and ready to explore .</p><span style="font-size: 11px; border: 1px solid rgba(0,0,0,0.3); display: inline-block; text-transform:  uppercase; font-weight: bold; padding: 5px 10px; border-radius: 20px; margin: 5px; background-color:whitesmoke;">🗽 Tours</span><span style="font-size: 11px; border: 1px solid rgba(0,0,0,0.3); display: inline-block; text-transform:  uppercase; font-weight: bold; padding: 5px 10px; border-radius: 20px; margin: 5px; background-color:whitesmoke;">🍱 Foodie</span><span style="font-size: 11px; border: 1px solid rgba(0,0,0,0.3); display: inline-block; text-transform:  uppercase; font-weight: bold; padding: 5px 10px; border-radius: 20px; margin: 5px; background-color:whitesmoke;">📸 Photography</span><br><h4>Languages I Speak:</h4><span style="font-size: 11px; border: 1px solid rgba(0,0,0,0.3); display: inline-block; text-transform:  uppercase; font-weight: bold; padding: 5px 10px; border-radius: 20px; margin: 5px; background-color:white;">English</span></td><td style="text-align:center;width:200px; border:1px solid rgba(0,0,0,0.2); padding:10px; border-radius:20px;"><a href="https://venti.co/u/exploresolo" target="_blank"><img src="https://storage.googleapis.com/download/storage/v1/b/venti-344622.appspot.com/o/5117849-20220509_121929.jpg?generation=1676055976705376&amp;alt=media" style="width:200px"></a><br>
                            <p style="font-weight:bold;">Lea, 32</p>
                            <p style="min-height:50px;">I am living in a beautiful city of Stockholm who loves solo travelling to connect with locals- learn the culture and explore the country.</p><span style="font-size: 11px; border: 1px solid rgba(0,0,0,0.3); display: inline-block; text-transform:  uppercase; font-weight: bold; padding: 5px 10px; border-radius: 20px; margin: 5px; background-color:whitesmoke;">🗽 Tours</span><span style="font-size: 11px; border: 1px solid rgba(0,0,0,0.3); display: inline-block; text-transform:  uppercase; font-weight: bold; padding: 5px 10px; border-radius: 20px; margin: 5px; background-color:whitesmoke;">🍱 Foodie</span><span style="font-size: 11px; border: 1px solid rgba(0,0,0,0.3); display: inline-block; text-transform:  uppercase; font-weight: bold; padding: 5px 10px; border-radius: 20px; margin: 5px; background-color:whitesmoke;">📸 Photography</span><span style="font-size: 11px; border: 1px solid rgba(0,0,0,0.3); display: inline-block; text-transform:  uppercase; font-weight: bold; padding: 5px 10px; border-radius: 20px; margin: 5px; background-color:whitesmoke;">👋 Meeting Locals</span><br><h4>Languages I Speak:</h4><span style="font-size: 11px; border: 1px solid rgba(0,0,0,0.3); display: inline-block; text-transform:  uppercase; font-weight: bold; padding: 5px 10px; border-radius: 20px; margin: 5px; background-color:white;">English</span></td></tr><tr style="padding-top: 47px;"><td style="text-align:center;width:200px;border:1px solid rgba(0,0,0,0.2);padding:10px;border-radius:20px;margin-top: 20px;"><a href="https://venti.co/u/ingridha8153" target="_blank"><img src="https://graph.facebook.com/v3.3/5625161274262647/picture?width=400" style="width:200px"></a><br>
                            <p style="font-weight:bold;">Ingrid, 24</p>
                            <p style="min-height:50px;">Hi 🙂</p><span style="font-size: 11px; border: 1px solid rgba(0,0,0,0.3); display: inline-block; text-transform:  uppercase; font-weight: bold; padding: 5px 10px; border-radius: 20px; margin: 5px; background-color:whitesmoke;">🥾 Hiking</span><span style="font-size: 11px; border: 1px solid rgba(0,0,0,0.3); display: inline-block; text-transform:  uppercase; font-weight: bold; padding: 5px 10px; border-radius: 20px; margin: 5px; background-color:whitesmoke;">🏖️ Beach</span><span style="font-size: 11px; border: 1px solid rgba(0,0,0,0.3); display: inline-block; text-transform:  uppercase; font-weight: bold; padding: 5px 10px; border-radius: 20px; margin: 5px; background-color:whitesmoke;">🕺💃 Dancing</span><span style="font-size: 11px; border: 1px solid rgba(0,0,0,0.3); display: inline-block; text-transform:  uppercase; font-weight: bold; padding: 5px 10px; border-radius: 20px; margin: 5px; background-color:whitesmoke;">🗽 Tours</span><br><h4>Languages I Speak:</h4><span style="font-size: 11px; border: 1px solid rgba(0,0,0,0.3); display: inline-block; text-transform:  uppercase; font-weight: bold; padding: 5px 10px; border-radius: 20px; margin: 5px; background-color:white;">English</span></td><td style="text-align:center;width:200px; border:1px solid rgba(0,0,0,0.2); padding:10px; border-radius:20px;"><a href="https://venti.co/u/graeznavales" target="_blank"><img src="https://firebasestorage.googleapis.com/v0/b/venti-344622.appspot.com/o/2023-01-24T19%3A21%3A59.161Z?alt=media&amp;token=cdf2a973-f2f5-4a1a-8fd5-2c7310e5b61d" style="width:200px"></a><br>
                            <p style="font-weight:bold;">Grazzle, 33</p>
                            <p style="min-height:50px;">Looking for a go- to friend or a travel buddy.
Starting to see the beauty of each country…
I’ve been to Georgia- Azerbaijan- UAE- Bahrain and Kyrgyzstan.
And more soon 🍀</p><span style="font-size: 11px; border: 1px solid rgba(0,0,0,0.3); display: inline-block; text-transform:  uppercase; font-weight: bold; padding: 5px 10px; border-radius: 20px; margin: 5px; background-color:whitesmoke;">🥾 Hiking</span><span style="font-size: 11px; border: 1px solid rgba(0,0,0,0.3); display: inline-block; text-transform:  uppercase; font-weight: bold; padding: 5px 10px; border-radius: 20px; margin: 5px; background-color:whitesmoke;">🗽 Tours</span><span style="font-size: 11px; border: 1px solid rgba(0,0,0,0.3); display: inline-block; text-transform:  uppercase; font-weight: bold; padding: 5px 10px; border-radius: 20px; margin: 5px; background-color:whitesmoke;">🍱 Foodie</span><span style="font-size: 11px; border: 1px solid rgba(0,0,0,0.3); display: inline-block; text-transform:  uppercase; font-weight: bold; padding: 5px 10px; border-radius: 20px; margin: 5px; background-color:whitesmoke;">📸 Photography</span><br><h4>Languages I Speak:</h4><span style="font-size: 11px; border: 1px solid rgba(0,0,0,0.3); display: inline-block; text-transform:  uppercase; font-weight: bold; padding: 5px 10px; border-radius: 20px; margin: 5px; background-color:white;">English</span><span style="font-size: 11px; border: 1px solid rgba(0,0,0,0.3); display: inline-block; text-transform:  uppercase; font-weight: bold; padding: 5px 10px; border-radius: 20px; margin: 5px; background-color:white;">Arabic</span></td></tr></tbody></table>';

        $email = "markus@venti.co";
        $subject = "We think you all would make a great group";

        $data = [
            "table" => $table,
            "name" => "Markus",
            "tripID" => "xxxxxxxxxx"
        ];

        Mail::send("emails.application.recommendation", array('data' => $data), function($message) use($email, $subject)
            {
                $message
                ->to($email, "Markus")
                ->replyTo("no-reply@venti.co")
                ->subject($subject);
        });
        

        echo $table;

   }

   

    public function preview(){
        
        // get all users from firebase

        // test design of email receipt

        $result = json_decode('{"object":{"id":"ord_0000AcZNHsD8V4jn63p7om"},"changes":{"updatedAt":"2023-12-14T14:27:41.557191Z","removed":[{"updatedAt":"2023-12-07T14:58:27.815067Z","sid":"F1","segments":[{"updatedAt":"2023-12-07T14:58:27.816509Z","sid":"S2","passengers":[{"updatedAt":"2023-12-07T14:58:27.817981Z","seat":null,"providedId":null,"insertedAt":"2023-12-07T14:58:27.817981Z","id":"osp_0000AcZNHsDqSRIx8G9gvS","cabinClassMarketingName":"Economy","cabinClass":"economy","baggages":[{"type":"carry_on","quantity":1,"id":"2d97d62a-941d-473d-b673-2b99b0cff55c"},{"type":"checked","quantity":2,"id":"fe772061-6a2b-4689-b191-3fa73ed8683a"}],"airOrderSegmentId":"seg_0000AcZNHsD8V4jn63p7on","airOrderPassengerId":"pas_0000AcZMusO1NnD5P6WCeY"}],"originId":"arp_iad_us","origin":{"updatedAt":"2023-10-24T14:53:51.930098Z","timeZone":"America/New_York","sourceType":"ALGORITHM","name":"Washington Dulles International Airport","longitude":-77.45617,"latitude":38.948808,"insertedAt":"2023-10-24T14:53:51.930098Z","id":"arp_iad_us","icaoCode":"KIAD","iataCountryCode":"US","iataCode":"IAD","iataCityCode":"WAS","disabledAt":null,"cityName":"Washington","city":{"updatedAt":"2019-07-30T16:35:28.391340Z","name":"Washington","insertedAt":"2019-07-30T16:35:28.391340Z","id":"cit_was_us","iataCountryCode":"US","iataCode":"WAS"},"alternativeNames":[]},"operatingCarrierName":null,"operatingCarrierId":"arl_00009VME7DDoV8ZkzmMkaN","operatingCarrierFlightNumber":"3645","operatingCarrierCode":null,"operatingCarrier":{"updatedAt":"2022-06-20T14:40:51.263807Z","name":"United Airlines","logoSymbolUrl":"https://assets.duffel.com/img/airlines/for-light-background/full-color-logo/UA.svg","logoLockupUrl":"https://assets.duffel.com/img/airlines/for-light-background/full-color-lockup/UA.svg","insertedAt":"2022-06-16T10:30:28.260610Z","id":"arl_00009VME7DDoV8ZkzmMkaN","icaoCode":"UAL","iataCode":"UA","disabledAt":null,"country":"United States","conditionsOfCarriageUrl":"https://www.united.com/ual/en/GB/fly/contract.html","callSign":"UNITED","aliases":null,"active":true,"accountingCode":"016"},"marketingCarrierId":"arl_00009VME7DDoV8ZkzmMkaN","marketingCarrierFlightNumber":"3645","marketingCarrier":{"updatedAt":"2022-06-20T14:40:51.263807Z","name":"United Airlines","logoSymbolUrl":"https://assets.duffel.com/img/airlines/for-light-background/full-color-logo/UA.svg","logoLockupUrl":"https://assets.duffel.com/img/airlines/for-light-background/full-color-lockup/UA.svg","insertedAt":"2022-06-16T10:30:28.260610Z","id":"arl_00009VME7DDoV8ZkzmMkaN","icaoCode":"UAL","iataCode":"UA","disabledAt":null,"country":"United States","conditionsOfCarriageUrl":"https://www.united.com/ual/en/GB/fly/contract.html","callSign":"UNITED","aliases":null,"active":true,"accountingCode":"016"},"linkId":"ost_0000AcZNHsC4Z1t32lKHA1","insertedAt":"2023-12-07T14:58:27.816509Z","id":"seg_0000AcZNHsD8V4jn63p7on","duration":"PT2H1M","distance":"785.35792","destinationId":"arp_yul_ca","destination":{"updatedAt":"2023-10-24T14:53:52.540052Z","timeZone":"America/Toronto","sourceType":"ALGORITHM_AND_MANUAL_REVIEW","name":"Montréal–Trudeau International Airport","longitude":-73.743,"latitude":45.469156,"insertedAt":"2023-10-24T14:53:52.540052Z","id":"arp_yul_ca","icaoCode":"CYUL","iataCountryCode":"CA","iataCode":"YUL","iataCityCode":"YMQ","disabledAt":null,"cityName":"Montreal","city":{"updatedAt":"2019-07-30T16:35:28.420840Z","name":"Montreal","insertedAt":"2019-07-30T16:35:28.420840Z","id":"cit_ymq_ca","iataCountryCode":"CA","iataCode":"YMQ"},"alternativeNames":["Montréal–Dorval International Airport"]},"departureTerminal":null,"departureDatetime":"2024-03-31T08:22:00","deletedAt":null,"arrivalTerminal":null,"arrivalDatetime":"2024-03-31T10:23:00","aircraftId":"arc_00009VMF8AiFPp0xSPcfNy","aircraft":{"updatedAt":"2021-06-11T12:53:27.289188Z","name":"Embraer 175 (Enhanced Winglets)","jetFuelConsumption":null,"insertedAt":"2021-06-11T12:53:27.289188Z","id":"arc_00009VMF8AiFPp0xSPcfNy","iataCode":"E7W"},"airOrderSliceId":"sli_0000AcZNHsDUTl1N79zPN2"},{"updatedAt":"2023-12-07T14:58:27.819736Z","sid":"S3","passengers":[{"updatedAt":"2023-12-07T14:58:27.821175Z","seat":null,"providedId":null,"insertedAt":"2023-12-07T14:58:27.821175Z","id":"osp_0000AcZNHsDqSRIx8G9gvU","cabinClassMarketingName":"Economy","cabinClass":"economy","baggages":[{"type":"carry_on","quantity":1,"id":"bf664121-04bb-4c91-a5d0-6b793d7eb3bd"},{"type":"checked","quantity":2,"id":"fd961192-ee84-4383-a99f-7685899fb966"}],"airOrderSegmentId":"seg_0000AcZNHsD8V4jn63p7oo","airOrderPassengerId":"pas_0000AcZMusO1NnD5P6WCeY"}],"originId":"arp_yul_ca","origin":{"updatedAt":"2023-10-24T14:53:52.540052Z","timeZone":"America/Toronto","sourceType":"ALGORITHM_AND_MANUAL_REVIEW","name":"Montréal–Trudeau International Airport","longitude":-73.743,"latitude":45.469156,"insertedAt":"2023-10-24T14:53:52.540052Z","id":"arp_yul_ca","icaoCode":"CYUL","iataCountryCode":"CA","iataCode":"YUL","iataCityCode":"YMQ","disabledAt":null,"cityName":"Montreal","city":{"updatedAt":"2019-07-30T16:35:28.420840Z","name":"Montreal","insertedAt":"2019-07-30T16:35:28.420840Z","id":"cit_ymq_ca","iataCountryCode":"CA","iataCode":"YMQ"},"alternativeNames":["Montréal–Dorval International Airport"]},"operatingCarrierName":null,"operatingCarrierId":"arl_00009VME7DAch01WpssCc4","operatingCarrierFlightNumber":"5","operatingCarrierCode":null,"operatingCarrier":{"updatedAt":"2022-06-20T14:40:50.513452Z","name":"Air Canada","logoSymbolUrl":"https://assets.duffel.com/img/airlines/for-light-background/full-color-logo/AC.svg","logoLockupUrl":"https://assets.duffel.com/img/airlines/for-light-background/full-color-lockup/AC.svg","insertedAt":"2022-06-16T10:30:28.260610Z","id":"arl_00009VME7DAch01WpssCc4","icaoCode":"ACA","iataCode":"AC","disabledAt":null,"country":"Canada","conditionsOfCarriageUrl":"https://www.aircanada.com/us/en/aco/home/legal/conditions-carriage-tariffs.html#/","callSign":"AIR CANADA","aliases":null,"active":true,"accountingCode":"014"},"marketingCarrierId":"arl_00009VME7DAch01WpssCc4","marketingCarrierFlightNumber":"5","marketingCarrier":{"updatedAt":"2022-06-20T14:40:50.513452Z","name":"Air Canada","logoSymbolUrl":"https://assets.duffel.com/img/airlines/for-light-background/full-color-logo/AC.svg","logoLockupUrl":"https://assets.duffel.com/img/airlines/for-light-background/full-color-lockup/AC.svg","insertedAt":"2022-06-16T10:30:28.260610Z","id":"arl_00009VME7DAch01WpssCc4","icaoCode":"ACA","iataCode":"AC","disabledAt":null,"country":"Canada","conditionsOfCarriageUrl":"https://www.aircanada.com/us/en/aco/home/legal/conditions-carriage-tariffs.html#/","callSign":"AIR CANADA","aliases":null,"active":true,"accountingCode":"014"},"linkId":"ost_0000AcZNHsC4Z1t32lKHA0","insertedAt":"2023-12-07T14:58:27.819736Z","id":"seg_0000AcZNHsD8V4jn63p7oo","duration":"PT13H30M","distance":"10349.66554","destinationId":"arp_nrt_jp","destination":{"updatedAt":"2023-10-24T14:53:51.982454Z","timeZone":"Asia/Tokyo","sourceType":"ALGORITHM_AND_MANUAL_REVIEW","name":"Narita International Airport","longitude":140.39285,"latitude":35.773554,"insertedAt":"2023-10-24T14:53:51.982454Z","id":"arp_nrt_jp","icaoCode":"RJAA","iataCountryCode":"JP","iataCode":"NRT","iataCityCode":"TYO","disabledAt":null,"cityName":"Tokyo","city":{"updatedAt":"2019-07-30T16:35:28.376867Z","name":"Tokyo","insertedAt":"2019-07-30T16:35:28.376867Z","id":"cit_tyo_jp","iataCountryCode":"JP","iataCode":"TYO"},"alternativeNames":[]},"departureTerminal":null,"departureDatetime":"2024-03-31T12:55:00","deletedAt":null,"arrivalTerminal":"1","arrivalDatetime":"2024-04-01T15:25:00","aircraftId":"arc_00009VMF8AhXSSRnQDI6HH","aircraft":{"updatedAt":"2021-06-11T12:53:25.963614Z","name":"Boeing 777-300ER","jetFuelConsumption":{"sorted":[[125,2766.1386],[200,4100.777],[250,4991.2845],[500,8946.7407],[750,13035.2647],[1000,17038.6852],[1500,25263.5252],[2000,33595.8434],[2500,41726.9976],[3000,50246.1853],[3500,58377.6459],[4000,67068.1742],[4500,75222.9282],[5000,86373.7705],[5500,94753.4989]],"lto":2821.8},"insertedAt":"2021-06-11T12:53:25.963614Z","id":"arc_00009VMF8AhXSSRnQDI6HH","iataCode":"77W"},"airOrderSliceId":"sli_0000AcZNHsDUTl1N79zPN2"}],"originId":"arp_iad_us","origin":{"updatedAt":"2023-10-24T14:53:51.930098Z","timeZone":"America/New_York","sourceType":"ALGORITHM","name":"Washington Dulles International Airport","longitude":-77.45617,"latitude":38.948808,"insertedAt":"2023-10-24T14:53:51.930098Z","id":"arp_iad_us","icaoCode":"KIAD","iataCountryCode":"US","iataCode":"IAD","iataCityCode":"WAS","disabledAt":null,"cityName":"Washington","city":{"updatedAt":"2019-07-30T16:35:28.391340Z","name":"Washington","insertedAt":"2019-07-30T16:35:28.391340Z","id":"cit_was_us","iataCountryCode":"US","iataCode":"WAS"},"alternativeNames":[]},"linkId":"osc_0000AcZNHsC4Z1t32lKHA2","insertedAt":"2023-12-07T14:58:27.815067Z","id":"sli_0000AcZNHsDUTl1N79zPN2","fareBrandName":"Basic Economy","duration":1083,"destinationId":"arp_nrt_jp","destination":{"updatedAt":"2023-10-24T14:53:51.982454Z","timeZone":"Asia/Tokyo","sourceType":"ALGORITHM_AND_MANUAL_REVIEW","name":"Narita International Airport","longitude":140.39285,"latitude":35.773554,"insertedAt":"2023-10-24T14:53:51.982454Z","id":"arp_nrt_jp","icaoCode":"RJAA","iataCountryCode":"JP","iataCode":"NRT","iataCityCode":"TYO","disabledAt":null,"cityName":"Tokyo","city":{"updatedAt":"2019-07-30T16:35:28.376867Z","name":"Tokyo","insertedAt":"2019-07-30T16:35:28.376867Z","id":"cit_tyo_jp","iataCountryCode":"JP","iataCode":"TYO"},"alternativeNames":[]},"deletedAt":null,"conditions":[],"changeable":null,"airOrderId":"ord_0000AcZNHsD8V4jn63p7om"},{"updatedAt":"2023-12-07T14:58:27.815067Z","sid":"F1","segments":[{"updatedAt":"2023-12-07T14:58:27.816509Z","sid":"S2","passengers":[{"updatedAt":"2023-12-07T14:58:27.817981Z","seat":null,"providedId":null,"insertedAt":"2023-12-07T14:58:27.817981Z","id":"osp_0000AcZNHsDqSRIx8G9gvS","cabinClassMarketingName":"Economy","cabinClass":"economy","baggages":[{"type":"carry_on","quantity":1,"id":"2d97d62a-941d-473d-b673-2b99b0cff55c"},{"type":"checked","quantity":2,"id":"fe772061-6a2b-4689-b191-3fa73ed8683a"}],"airOrderSegmentId":"seg_0000AcZNHsD8V4jn63p7on","airOrderPassengerId":"pas_0000AcZMusO1NnD5P6WCeY"}],"originId":"arp_iad_us","origin":{"updatedAt":"2023-10-24T14:53:51.930098Z","timeZone":"America/New_York","sourceType":"ALGORITHM","name":"Washington Dulles International Airport","longitude":-77.45617,"latitude":38.948808,"insertedAt":"2023-10-24T14:53:51.930098Z","id":"arp_iad_us","icaoCode":"KIAD","iataCountryCode":"US","iataCode":"IAD","iataCityCode":"WAS","disabledAt":null,"cityName":"Washington","city":{"updatedAt":"2019-07-30T16:35:28.391340Z","name":"Washington","insertedAt":"2019-07-30T16:35:28.391340Z","id":"cit_was_us","iataCountryCode":"US","iataCode":"WAS"},"alternativeNames":[]},"operatingCarrierName":null,"operatingCarrierId":"arl_00009VME7DDoV8ZkzmMkaN","operatingCarrierFlightNumber":"3645","operatingCarrierCode":null,"operatingCarrier":{"updatedAt":"2022-06-20T14:40:51.263807Z","name":"United Airlines","logoSymbolUrl":"https://assets.duffel.com/img/airlines/for-light-background/full-color-logo/UA.svg","logoLockupUrl":"https://assets.duffel.com/img/airlines/for-light-background/full-color-lockup/UA.svg","insertedAt":"2022-06-16T10:30:28.260610Z","id":"arl_00009VME7DDoV8ZkzmMkaN","icaoCode":"UAL","iataCode":"UA","disabledAt":null,"country":"United States","conditionsOfCarriageUrl":"https://www.united.com/ual/en/GB/fly/contract.html","callSign":"UNITED","aliases":null,"active":true,"accountingCode":"016"},"marketingCarrierId":"arl_00009VME7DDoV8ZkzmMkaN","marketingCarrierFlightNumber":"3645","marketingCarrier":{"updatedAt":"2022-06-20T14:40:51.263807Z","name":"United Airlines","logoSymbolUrl":"https://assets.duffel.com/img/airlines/for-light-background/full-color-logo/UA.svg","logoLockupUrl":"https://assets.duffel.com/img/airlines/for-light-background/full-color-lockup/UA.svg","insertedAt":"2022-06-16T10:30:28.260610Z","id":"arl_00009VME7DDoV8ZkzmMkaN","icaoCode":"UAL","iataCode":"UA","disabledAt":null,"country":"United States","conditionsOfCarriageUrl":"https://www.united.com/ual/en/GB/fly/contract.html","callSign":"UNITED","aliases":null,"active":true,"accountingCode":"016"},"linkId":"ost_0000AcZNHsC4Z1t32lKHA1","insertedAt":"2023-12-07T14:58:27.816509Z","id":"seg_0000AcZNHsD8V4jn63p7on","duration":"PT2H1M","distance":"785.35792","destinationId":"arp_yul_ca","destination":{"updatedAt":"2023-10-24T14:53:52.540052Z","timeZone":"America/Toronto","sourceType":"ALGORITHM_AND_MANUAL_REVIEW","name":"Montréal–Trudeau International Airport","longitude":-73.743,"latitude":45.469156,"insertedAt":"2023-10-24T14:53:52.540052Z","id":"arp_yul_ca","icaoCode":"CYUL","iataCountryCode":"CA","iataCode":"YUL","iataCityCode":"YMQ","disabledAt":null,"cityName":"Montreal","city":{"updatedAt":"2019-07-30T16:35:28.420840Z","name":"Montreal","insertedAt":"2019-07-30T16:35:28.420840Z","id":"cit_ymq_ca","iataCountryCode":"CA","iataCode":"YMQ"},"alternativeNames":["Montréal–Dorval International Airport"]},"departureTerminal":null,"departureDatetime":"2024-03-31T08:22:00","deletedAt":null,"arrivalTerminal":null,"arrivalDatetime":"2024-03-31T10:23:00","aircraftId":"arc_00009VMF8AiFPp0xSPcfNy","aircraft":{"updatedAt":"2021-06-11T12:53:27.289188Z","name":"Embraer 175 (Enhanced Winglets)","jetFuelConsumption":null,"insertedAt":"2021-06-11T12:53:27.289188Z","id":"arc_00009VMF8AiFPp0xSPcfNy","iataCode":"E7W"},"airOrderSliceId":"sli_0000AcZNHsDUTl1N79zPN2"},{"updatedAt":"2023-12-07T14:58:27.819736Z","sid":"S3","passengers":[{"updatedAt":"2023-12-07T14:58:27.821175Z","seat":null,"providedId":null,"insertedAt":"2023-12-07T14:58:27.821175Z","id":"osp_0000AcZNHsDqSRIx8G9gvU","cabinClassMarketingName":"Economy","cabinClass":"economy","baggages":[{"type":"carry_on","quantity":1,"id":"bf664121-04bb-4c91-a5d0-6b793d7eb3bd"},{"type":"checked","quantity":2,"id":"fd961192-ee84-4383-a99f-7685899fb966"}],"airOrderSegmentId":"seg_0000AcZNHsD8V4jn63p7oo","airOrderPassengerId":"pas_0000AcZMusO1NnD5P6WCeY"}],"originId":"arp_yul_ca","origin":{"updatedAt":"2023-10-24T14:53:52.540052Z","timeZone":"America/Toronto","sourceType":"ALGORITHM_AND_MANUAL_REVIEW","name":"Montréal–Trudeau International Airport","longitude":-73.743,"latitude":45.469156,"insertedAt":"2023-10-24T14:53:52.540052Z","id":"arp_yul_ca","icaoCode":"CYUL","iataCountryCode":"CA","iataCode":"YUL","iataCityCode":"YMQ","disabledAt":null,"cityName":"Montreal","city":{"updatedAt":"2019-07-30T16:35:28.420840Z","name":"Montreal","insertedAt":"2019-07-30T16:35:28.420840Z","id":"cit_ymq_ca","iataCountryCode":"CA","iataCode":"YMQ"},"alternativeNames":["Montréal–Dorval International Airport"]},"operatingCarrierName":null,"operatingCarrierId":"arl_00009VME7DAch01WpssCc4","operatingCarrierFlightNumber":"5","operatingCarrierCode":null,"operatingCarrier":{"updatedAt":"2022-06-20T14:40:50.513452Z","name":"Air Canada","logoSymbolUrl":"https://assets.duffel.com/img/airlines/for-light-background/full-color-logo/AC.svg","logoLockupUrl":"https://assets.duffel.com/img/airlines/for-light-background/full-color-lockup/AC.svg","insertedAt":"2022-06-16T10:30:28.260610Z","id":"arl_00009VME7DAch01WpssCc4","icaoCode":"ACA","iataCode":"AC","disabledAt":null,"country":"Canada","conditionsOfCarriageUrl":"https://www.aircanada.com/us/en/aco/home/legal/conditions-carriage-tariffs.html#/","callSign":"AIR CANADA","aliases":null,"active":true,"accountingCode":"014"},"marketingCarrierId":"arl_00009VME7DAch01WpssCc4","marketingCarrierFlightNumber":"5","marketingCarrier":{"updatedAt":"2022-06-20T14:40:50.513452Z","name":"Air Canada","logoSymbolUrl":"https://assets.duffel.com/img/airlines/for-light-background/full-color-logo/AC.svg","logoLockupUrl":"https://assets.duffel.com/img/airlines/for-light-background/full-color-lockup/AC.svg","insertedAt":"2022-06-16T10:30:28.260610Z","id":"arl_00009VME7DAch01WpssCc4","icaoCode":"ACA","iataCode":"AC","disabledAt":null,"country":"Canada","conditionsOfCarriageUrl":"https://www.aircanada.com/us/en/aco/home/legal/conditions-carriage-tariffs.html#/","callSign":"AIR CANADA","aliases":null,"active":true,"accountingCode":"014"},"linkId":"ost_0000AcZNHsC4Z1t32lKHA0","insertedAt":"2023-12-07T14:58:27.819736Z","id":"seg_0000AcZNHsD8V4jn63p7oo","duration":"PT13H30M","distance":"10349.66554","destinationId":"arp_nrt_jp","destination":{"updatedAt":"2023-10-24T14:53:51.982454Z","timeZone":"Asia/Tokyo","sourceType":"ALGORITHM_AND_MANUAL_REVIEW","name":"Narita International Airport","longitude":140.39285,"latitude":35.773554,"insertedAt":"2023-10-24T14:53:51.982454Z","id":"arp_nrt_jp","icaoCode":"RJAA","iataCountryCode":"JP","iataCode":"NRT","iataCityCode":"TYO","disabledAt":null,"cityName":"Tokyo","city":{"updatedAt":"2019-07-30T16:35:28.376867Z","name":"Tokyo","insertedAt":"2019-07-30T16:35:28.376867Z","id":"cit_tyo_jp","iataCountryCode":"JP","iataCode":"TYO"},"alternativeNames":[]},"departureTerminal":null,"departureDatetime":"2024-03-31T12:55:00","deletedAt":null,"arrivalTerminal":"1","arrivalDatetime":"2024-04-01T15:25:00","aircraftId":"arc_00009VMF8AhXSSRnQDI6HH","aircraft":{"updatedAt":"2021-06-11T12:53:25.963614Z","name":"Boeing 777-300ER","jetFuelConsumption":{"sorted":[[125,2766.1386],[200,4100.777],[250,4991.2845],[500,8946.7407],[750,13035.2647],[1000,17038.6852],[1500,25263.5252],[2000,33595.8434],[2500,41726.9976],[3000,50246.1853],[3500,58377.6459],[4000,67068.1742],[4500,75222.9282],[5000,86373.7705],[5500,94753.4989]],"lto":2821.8},"insertedAt":"2021-06-11T12:53:25.963614Z","id":"arc_00009VMF8AhXSSRnQDI6HH","iataCode":"77W"},"airOrderSliceId":"sli_0000AcZNHsDUTl1N79zPN2"}],"originId":"arp_iad_us","origin":{"updatedAt":"2023-10-24T14:53:51.930098Z","timeZone":"America/New_York","sourceType":"ALGORITHM","name":"Washington Dulles International Airport","longitude":-77.45617,"latitude":38.948808,"insertedAt":"2023-10-24T14:53:51.930098Z","id":"arp_iad_us","icaoCode":"KIAD","iataCountryCode":"US","iataCode":"IAD","iataCityCode":"WAS","disabledAt":null,"cityName":"Washington","city":{"updatedAt":"2019-07-30T16:35:28.391340Z","name":"Washington","insertedAt":"2019-07-30T16:35:28.391340Z","id":"cit_was_us","iataCountryCode":"US","iataCode":"WAS"},"alternativeNames":[]},"linkId":"osc_0000AcZNHsC4Z1t32lKHA2","insertedAt":"2023-12-07T14:58:27.815067Z","id":"sli_0000AcZNHsDUTl1N79zPN2","fareBrandName":"Basic Economy","duration":"PT18H3M","destinationId":"arp_nrt_jp","destination":{"updatedAt":"2023-10-24T14:53:51.982454Z","timeZone":"Asia/Tokyo","sourceType":"ALGORITHM_AND_MANUAL_REVIEW","name":"Narita International Airport","longitude":140.39285,"latitude":35.773554,"insertedAt":"2023-10-24T14:53:51.982454Z","id":"arp_nrt_jp","icaoCode":"RJAA","iataCountryCode":"JP","iataCode":"NRT","iataCityCode":"TYO","disabledAt":null,"cityName":"Tokyo","city":{"updatedAt":"2019-07-30T16:35:28.376867Z","name":"Tokyo","insertedAt":"2019-07-30T16:35:28.376867Z","id":"cit_tyo_jp","iataCountryCode":"JP","iataCode":"TYO"},"alternativeNames":[]},"deletedAt":null,"conditions":[],"changeable":null,"airOrderId":"ord_0000AcZNHsD8V4jn63p7om"}],"orderId":"ord_0000AcZNHsD8V4jn63p7om","id":"aic_0000Acnq8bk5jZu3kFSLJ2","createdAt":"2023-12-14T14:27:41.557191Z","availableActions":["accept"],"added":[{"updatedAt":"2023-12-07T14:58:27.815067Z","sid":"F1","segments":[{"updatedAt":"2023-12-07T14:58:27.816509Z","sid":"S2","passengers":[{"updatedAt":"2023-12-07T14:58:27.817981Z","seat":null,"providedId":null,"insertedAt":"2023-12-07T14:58:27.817981Z","id":"osp_0000AcZNHsDqSRIx8G9gvS","cabinClassMarketingName":"Economy","cabinClass":"economy","baggages":[{"type":"carry_on","quantity":1,"id":"2d97d62a-941d-473d-b673-2b99b0cff55c"},{"type":"checked","quantity":2,"id":"fe772061-6a2b-4689-b191-3fa73ed8683a"}],"airOrderSegmentId":"seg_0000AcZNHsD8V4jn63p7on","airOrderPassengerId":"pas_0000AcZMusO1NnD5P6WCeY"}],"originId":"arp_iad_us","origin":{"updatedAt":"2023-10-24T14:53:51.930098Z","timeZone":"America/New_York","sourceType":"ALGORITHM","name":"Washington Dulles International Airport","longitude":-77.45617,"latitude":38.948808,"insertedAt":"2023-10-24T14:53:51.930098Z","id":"arp_iad_us","icaoCode":"KIAD","iataCountryCode":"US","iataCode":"IAD","iataCityCode":"WAS","disabledAt":null,"cityName":"Washington","city":{"updatedAt":"2019-07-30T16:35:28.391340Z","name":"Washington","insertedAt":"2019-07-30T16:35:28.391340Z","id":"cit_was_us","iataCountryCode":"US","iataCode":"WAS"},"alternativeNames":[]},"operatingCarrierName":null,"operatingCarrierId":"arl_00009VME7DDoV8ZkzmMkaN","operatingCarrierFlightNumber":"3645","operatingCarrierCode":null,"operatingCarrier":{"updatedAt":"2022-06-20T14:40:51.263807Z","name":"United Airlines","logoSymbolUrl":"https://assets.duffel.com/img/airlines/for-light-background/full-color-logo/UA.svg","logoLockupUrl":"https://assets.duffel.com/img/airlines/for-light-background/full-color-lockup/UA.svg","insertedAt":"2022-06-16T10:30:28.260610Z","id":"arl_00009VME7DDoV8ZkzmMkaN","icaoCode":"UAL","iataCode":"UA","disabledAt":null,"country":"United States","conditionsOfCarriageUrl":"https://www.united.com/ual/en/GB/fly/contract.html","callSign":"UNITED","aliases":null,"active":true,"accountingCode":"016"},"marketingCarrierId":"arl_00009VME7DDoV8ZkzmMkaN","marketingCarrierFlightNumber":"3645","marketingCarrier":{"updatedAt":"2022-06-20T14:40:51.263807Z","name":"United Airlines","logoSymbolUrl":"https://assets.duffel.com/img/airlines/for-light-background/full-color-logo/UA.svg","logoLockupUrl":"https://assets.duffel.com/img/airlines/for-light-background/full-color-lockup/UA.svg","insertedAt":"2022-06-16T10:30:28.260610Z","id":"arl_00009VME7DDoV8ZkzmMkaN","icaoCode":"UAL","iataCode":"UA","disabledAt":null,"country":"United States","conditionsOfCarriageUrl":"https://www.united.com/ual/en/GB/fly/contract.html","callSign":"UNITED","aliases":null,"active":true,"accountingCode":"016"},"linkId":"ost_0000AcZNHsC4Z1t32lKHA1","insertedAt":"2023-12-07T14:58:27.816509Z","id":"seg_0000AcZNHsD8V4jn63p7on","duration":"PT2H1M","distance":"785.35792","destinationId":"arp_yul_ca","destination":{"updatedAt":"2023-10-24T14:53:52.540052Z","timeZone":"America/Toronto","sourceType":"ALGORITHM_AND_MANUAL_REVIEW","name":"Montréal–Trudeau International Airport","longitude":-73.743,"latitude":45.469156,"insertedAt":"2023-10-24T14:53:52.540052Z","id":"arp_yul_ca","icaoCode":"CYUL","iataCountryCode":"CA","iataCode":"YUL","iataCityCode":"YMQ","disabledAt":null,"cityName":"Montreal","city":{"updatedAt":"2019-07-30T16:35:28.420840Z","name":"Montreal","insertedAt":"2019-07-30T16:35:28.420840Z","id":"cit_ymq_ca","iataCountryCode":"CA","iataCode":"YMQ"},"alternativeNames":["Montréal–Dorval International Airport"]},"departureTerminal":null,"departureDatetime":"2024-03-31T08:22:00","deletedAt":null,"arrivalTerminal":null,"arrivalDatetime":"2024-03-31T10:23:00","aircraftId":"arc_00009VMF8AiFPp0xSPcfNy","aircraft":{"updatedAt":"2021-06-11T12:53:27.289188Z","name":"Embraer 175 (Enhanced Winglets)","jetFuelConsumption":null,"insertedAt":"2021-06-11T12:53:27.289188Z","id":"arc_00009VMF8AiFPp0xSPcfNy","iataCode":"E7W"},"airOrderSliceId":"sli_0000AcZNHsDUTl1N79zPN2"},{"updatedAt":"2023-12-07T14:58:27.819736Z","sid":"S3","passengers":[{"updatedAt":"2023-12-07T14:58:27.821175Z","seat":null,"providedId":null,"insertedAt":"2023-12-07T14:58:27.821175Z","id":"osp_0000AcZNHsDqSRIx8G9gvU","cabinClassMarketingName":"Economy","cabinClass":"economy","baggages":[{"type":"carry_on","quantity":1,"id":"bf664121-04bb-4c91-a5d0-6b793d7eb3bd"},{"type":"checked","quantity":2,"id":"fd961192-ee84-4383-a99f-7685899fb966"}],"airOrderSegmentId":"seg_0000AcZNHsD8V4jn63p7oo","airOrderPassengerId":"pas_0000AcZMusO1NnD5P6WCeY"}],"originId":"arp_yul_ca","origin":{"updatedAt":"2023-10-24T14:53:52.540052Z","timeZone":"America/Toronto","sourceType":"ALGORITHM_AND_MANUAL_REVIEW","name":"Montréal–Trudeau International Airport","longitude":-73.743,"latitude":45.469156,"insertedAt":"2023-10-24T14:53:52.540052Z","id":"arp_yul_ca","icaoCode":"CYUL","iataCountryCode":"CA","iataCode":"YUL","iataCityCode":"YMQ","disabledAt":null,"cityName":"Montreal","city":{"updatedAt":"2019-07-30T16:35:28.420840Z","name":"Montreal","insertedAt":"2019-07-30T16:35:28.420840Z","id":"cit_ymq_ca","iataCountryCode":"CA","iataCode":"YMQ"},"alternativeNames":["Montréal–Dorval International Airport"]},"operatingCarrierName":null,"operatingCarrierId":"arl_00009VME7DAch01WpssCc4","operatingCarrierFlightNumber":"5","operatingCarrierCode":null,"operatingCarrier":{"updatedAt":"2022-06-20T14:40:50.513452Z","name":"Air Canada","logoSymbolUrl":"https://assets.duffel.com/img/airlines/for-light-background/full-color-logo/AC.svg","logoLockupUrl":"https://assets.duffel.com/img/airlines/for-light-background/full-color-lockup/AC.svg","insertedAt":"2022-06-16T10:30:28.260610Z","id":"arl_00009VME7DAch01WpssCc4","icaoCode":"ACA","iataCode":"AC","disabledAt":null,"country":"Canada","conditionsOfCarriageUrl":"https://www.aircanada.com/us/en/aco/home/legal/conditions-carriage-tariffs.html#/","callSign":"AIR CANADA","aliases":null,"active":true,"accountingCode":"014"},"marketingCarrierId":"arl_00009VME7DAch01WpssCc4","marketingCarrierFlightNumber":"5","marketingCarrier":{"updatedAt":"2022-06-20T14:40:50.513452Z","name":"Air Canada","logoSymbolUrl":"https://assets.duffel.com/img/airlines/for-light-background/full-color-logo/AC.svg","logoLockupUrl":"https://assets.duffel.com/img/airlines/for-light-background/full-color-lockup/AC.svg","insertedAt":"2022-06-16T10:30:28.260610Z","id":"arl_00009VME7DAch01WpssCc4","icaoCode":"ACA","iataCode":"AC","disabledAt":null,"country":"Canada","conditionsOfCarriageUrl":"https://www.aircanada.com/us/en/aco/home/legal/conditions-carriage-tariffs.html#/","callSign":"AIR CANADA","aliases":null,"active":true,"accountingCode":"014"},"linkId":"ost_0000AcZNHsC4Z1t32lKHA0","insertedAt":"2023-12-07T14:58:27.819736Z","id":"seg_0000AcZNHsD8V4jn63p7oo","duration":"PT13H30M","distance":"10349.66554","destinationId":"arp_nrt_jp","destination":{"updatedAt":"2023-10-24T14:53:51.982454Z","timeZone":"Asia/Tokyo","sourceType":"ALGORITHM_AND_MANUAL_REVIEW","name":"Narita International Airport","longitude":140.39285,"latitude":35.773554,"insertedAt":"2023-10-24T14:53:51.982454Z","id":"arp_nrt_jp","icaoCode":"RJAA","iataCountryCode":"JP","iataCode":"NRT","iataCityCode":"TYO","disabledAt":null,"cityName":"Tokyo","city":{"updatedAt":"2019-07-30T16:35:28.376867Z","name":"Tokyo","insertedAt":"2019-07-30T16:35:28.376867Z","id":"cit_tyo_jp","iataCountryCode":"JP","iataCode":"TYO"},"alternativeNames":[]},"departureTerminal":null,"departureDatetime":"2024-03-31T12:55:00","deletedAt":null,"arrivalTerminal":"1","arrivalDatetime":"2024-04-01T15:25:00","aircraftId":"arc_00009VMF8AhXSSRnQDI6HH","aircraft":{"updatedAt":"2021-06-11T12:53:25.963614Z","name":"Boeing 777-300ER","jetFuelConsumption":{"sorted":[[125,2766.1386],[200,4100.777],[250,4991.2845],[500,8946.7407],[750,13035.2647],[1000,17038.6852],[1500,25263.5252],[2000,33595.8434],[2500,41726.9976],[3000,50246.1853],[3500,58377.6459],[4000,67068.1742],[4500,75222.9282],[5000,86373.7705],[5500,94753.4989]],"lto":2821.8},"insertedAt":"2021-06-11T12:53:25.963614Z","id":"arc_00009VMF8AhXSSRnQDI6HH","iataCode":"77W"},"airOrderSliceId":"sli_0000AcZNHsDUTl1N79zPN2"}],"originId":"arp_iad_us","origin":{"updatedAt":"2023-10-24T14:53:51.930098Z","timeZone":"America/New_York","sourceType":"ALGORITHM","name":"Washington Dulles International Airport","longitude":-77.45617,"latitude":38.948808,"insertedAt":"2023-10-24T14:53:51.930098Z","id":"arp_iad_us","icaoCode":"KIAD","iataCountryCode":"US","iataCode":"IAD","iataCityCode":"WAS","disabledAt":null,"cityName":"Washington","city":{"updatedAt":"2019-07-30T16:35:28.391340Z","name":"Washington","insertedAt":"2019-07-30T16:35:28.391340Z","id":"cit_was_us","iataCountryCode":"US","iataCode":"WAS"},"alternativeNames":[]},"linkId":"osc_0000AcZNHsC4Z1t32lKHA2","insertedAt":"2023-12-07T14:58:27.815067Z","id":"sli_0000AcZNHsDUTl1N79zPN2","fareBrandName":"Basic Economy","duration":1088,"destinationId":"arp_nrt_jp","destination":{"updatedAt":"2023-10-24T14:53:51.982454Z","timeZone":"Asia/Tokyo","sourceType":"ALGORITHM_AND_MANUAL_REVIEW","name":"Narita International Airport","longitude":140.39285,"latitude":35.773554,"insertedAt":"2023-10-24T14:53:51.982454Z","id":"arp_nrt_jp","icaoCode":"RJAA","iataCountryCode":"JP","iataCode":"NRT","iataCityCode":"TYO","disabledAt":null,"cityName":"Tokyo","city":{"updatedAt":"2019-07-30T16:35:28.376867Z","name":"Tokyo","insertedAt":"2019-07-30T16:35:28.376867Z","id":"cit_tyo_jp","iataCountryCode":"JP","iataCode":"TYO"},"alternativeNames":[]},"deletedAt":null,"conditions":[],"changeable":null,"airOrderId":"ord_0000AcZNHsD8V4jn63p7om"},{"updatedAt":"2023-12-07T14:58:27.815067Z","sid":"F1","segments":[{"updatedAt":"2023-12-07T14:58:27.816509Z","sid":"S2","passengers":[{"updatedAt":"2023-12-07T14:58:27.817981Z","seat":null,"providedId":null,"insertedAt":"2023-12-07T14:58:27.817981Z","id":"osp_0000AcZNHsDqSRIx8G9gvS","cabinClassMarketingName":"Economy","cabinClass":"economy","baggages":[{"type":"carry_on","quantity":1,"id":"2d97d62a-941d-473d-b673-2b99b0cff55c"},{"type":"checked","quantity":2,"id":"fe772061-6a2b-4689-b191-3fa73ed8683a"}],"airOrderSegmentId":"seg_0000AcZNHsD8V4jn63p7on","airOrderPassengerId":"pas_0000AcZMusO1NnD5P6WCeY"}],"originId":"arp_iad_us","origin":{"updatedAt":"2023-10-24T14:53:51.930098Z","timeZone":"America/New_York","sourceType":"ALGORITHM","name":"Washington Dulles International Airport","longitude":-77.45617,"latitude":38.948808,"insertedAt":"2023-10-24T14:53:51.930098Z","id":"arp_iad_us","icaoCode":"KIAD","iataCountryCode":"US","iataCode":"IAD","iataCityCode":"WAS","disabledAt":null,"cityName":"Washington","city":{"updatedAt":"2019-07-30T16:35:28.391340Z","name":"Washington","insertedAt":"2019-07-30T16:35:28.391340Z","id":"cit_was_us","iataCountryCode":"US","iataCode":"WAS"},"alternativeNames":[]},"operatingCarrierName":null,"operatingCarrierId":"arl_00009VME7DDoV8ZkzmMkaN","operatingCarrierFlightNumber":"3645","operatingCarrierCode":null,"operatingCarrier":{"updatedAt":"2022-06-20T14:40:51.263807Z","name":"United Airlines","logoSymbolUrl":"https://assets.duffel.com/img/airlines/for-light-background/full-color-logo/UA.svg","logoLockupUrl":"https://assets.duffel.com/img/airlines/for-light-background/full-color-lockup/UA.svg","insertedAt":"2022-06-16T10:30:28.260610Z","id":"arl_00009VME7DDoV8ZkzmMkaN","icaoCode":"UAL","iataCode":"UA","disabledAt":null,"country":"United States","conditionsOfCarriageUrl":"https://www.united.com/ual/en/GB/fly/contract.html","callSign":"UNITED","aliases":null,"active":true,"accountingCode":"016"},"marketingCarrierId":"arl_00009VME7DDoV8ZkzmMkaN","marketingCarrierFlightNumber":"3645","marketingCarrier":{"updatedAt":"2022-06-20T14:40:51.263807Z","name":"United Airlines","logoSymbolUrl":"https://assets.duffel.com/img/airlines/for-light-background/full-color-logo/UA.svg","logoLockupUrl":"https://assets.duffel.com/img/airlines/for-light-background/full-color-lockup/UA.svg","insertedAt":"2022-06-16T10:30:28.260610Z","id":"arl_00009VME7DDoV8ZkzmMkaN","icaoCode":"UAL","iataCode":"UA","disabledAt":null,"country":"United States","conditionsOfCarriageUrl":"https://www.united.com/ual/en/GB/fly/contract.html","callSign":"UNITED","aliases":null,"active":true,"accountingCode":"016"},"linkId":"ost_0000AcZNHsC4Z1t32lKHA1","insertedAt":"2023-12-07T14:58:27.816509Z","id":"seg_0000AcZNHsD8V4jn63p7on","duration":"PT2H1M","distance":"785.35792","destinationId":"arp_yul_ca","destination":{"updatedAt":"2023-10-24T14:53:52.540052Z","timeZone":"America/Toronto","sourceType":"ALGORITHM_AND_MANUAL_REVIEW","name":"Montréal–Trudeau International Airport","longitude":-73.743,"latitude":45.469156,"insertedAt":"2023-10-24T14:53:52.540052Z","id":"arp_yul_ca","icaoCode":"CYUL","iataCountryCode":"CA","iataCode":"YUL","iataCityCode":"YMQ","disabledAt":null,"cityName":"Montreal","city":{"updatedAt":"2019-07-30T16:35:28.420840Z","name":"Montreal","insertedAt":"2019-07-30T16:35:28.420840Z","id":"cit_ymq_ca","iataCountryCode":"CA","iataCode":"YMQ"},"alternativeNames":["Montréal–Dorval International Airport"]},"departureTerminal":null,"departureDatetime":"2024-03-31T08:22:00","deletedAt":null,"arrivalTerminal":null,"arrivalDatetime":"2024-03-31T10:23:00","aircraftId":"arc_00009VMF8AiFPp0xSPcfNy","aircraft":{"updatedAt":"2021-06-11T12:53:27.289188Z","name":"Embraer 175 (Enhanced Winglets)","jetFuelConsumption":null,"insertedAt":"2021-06-11T12:53:27.289188Z","id":"arc_00009VMF8AiFPp0xSPcfNy","iataCode":"E7W"},"airOrderSliceId":"sli_0000AcZNHsDUTl1N79zPN2"},{"updatedAt":null,"sid":"S3","passengers":[{"updatedAt":null,"seat":null,"providedId":null,"insertedAt":null,"id":"osp_0000Acnq8bPswhjwja0CoQ","cabinClassMarketingName":"Economy","cabinClass":"economy","baggages":[{"type":"carry_on","quantity":1,"id":null},{"type":"checked","quantity":2,"id":null}],"airOrderSegmentId":null,"airOrderPassengerId":"pas_0000AcZMusO1NnD5P6WCeY"}],"originId":"arp_yul_ca","origin":{"updatedAt":"2023-10-24T14:53:52.540052Z","timeZone":"America/Toronto","sourceType":"ALGORITHM_AND_MANUAL_REVIEW","name":"Montréal–Trudeau International Airport","longitude":-73.743,"latitude":45.469156,"insertedAt":"2023-10-24T14:53:52.540052Z","id":"arp_yul_ca","icaoCode":"CYUL","iataCountryCode":"CA","iataCode":"YUL","iataCityCode":"YMQ","disabledAt":null,"cityName":"Montreal","city":{"updatedAt":"2019-07-30T16:35:28.420840Z","name":"Montreal","insertedAt":"2019-07-30T16:35:28.420840Z","id":"cit_ymq_ca","iataCountryCode":"CA","iataCode":"YMQ"},"alternativeNames":["Montréal–Dorval International Airport"]},"operatingCarrierName":null,"operatingCarrierId":"arl_00009VME7DAch01WpssCc4","operatingCarrierFlightNumber":"5","operatingCarrierCode":"AC","operatingCarrier":{"updatedAt":"2022-06-20T14:40:50.513452Z","name":"Air Canada","logoSymbolUrl":"https://assets.duffel.com/img/airlines/for-light-background/full-color-logo/AC.svg","logoLockupUrl":"https://assets.duffel.com/img/airlines/for-light-background/full-color-lockup/AC.svg","insertedAt":"2022-06-16T10:30:28.260610Z","id":"arl_00009VME7DAch01WpssCc4","icaoCode":"ACA","iataCode":"AC","disabledAt":null,"country":"Canada","conditionsOfCarriageUrl":"https://www.aircanada.com/us/en/aco/home/legal/conditions-carriage-tariffs.html#/","callSign":"AIR CANADA","aliases":null,"active":true,"accountingCode":"014"},"marketingCarrierId":"arl_00009VME7DAch01WpssCc4","marketingCarrierFlightNumber":"5","marketingCarrier":{"updatedAt":"2022-06-20T14:40:50.513452Z","name":"Air Canada","logoSymbolUrl":"https://assets.duffel.com/img/airlines/for-light-background/full-color-logo/AC.svg","logoLockupUrl":"https://assets.duffel.com/img/airlines/for-light-background/full-color-lockup/AC.svg","insertedAt":"2022-06-16T10:30:28.260610Z","id":"arl_00009VME7DAch01WpssCc4","icaoCode":"ACA","iataCode":"AC","disabledAt":null,"country":"Canada","conditionsOfCarriageUrl":"https://www.aircanada.com/us/en/aco/home/legal/conditions-carriage-tariffs.html#/","callSign":"AIR CANADA","aliases":null,"active":true,"accountingCode":"014"},"linkId":"ost_0000Acnq8bIRONmkMUWGCu","insertedAt":null,"id":"seg_0000Acnq8bPAzLAmhNfdhp","duration":"PT13H35M","distance":"10349.66554","destinationId":"arp_nrt_jp","destination":{"updatedAt":"2023-10-24T14:53:51.982454Z","timeZone":"Asia/Tokyo","sourceType":"ALGORITHM_AND_MANUAL_REVIEW","name":"Narita International Airport","longitude":140.39285,"latitude":35.773554,"insertedAt":"2023-10-24T14:53:51.982454Z","id":"arp_nrt_jp","icaoCode":"RJAA","iataCountryCode":"JP","iataCode":"NRT","iataCityCode":"TYO","disabledAt":null,"cityName":"Tokyo","city":{"updatedAt":"2019-07-30T16:35:28.376867Z","name":"Tokyo","insertedAt":"2019-07-30T16:35:28.376867Z","id":"cit_tyo_jp","iataCountryCode":"JP","iataCode":"TYO"},"alternativeNames":[]},"departureTerminal":null,"departureDatetime":"2024-03-31T12:55:00","deletedAt":null,"arrivalTerminal":"1","arrivalDatetime":"2024-04-01T15:30:00","aircraftId":"arc_00009VMF8AhXSSRnQDI6HH","aircraft":{"updatedAt":"2021-06-11T12:53:25.963614Z","name":"Boeing 777-300ER","jetFuelConsumption":{"sorted":[[125,2766.1386],[200,4100.777],[250,4991.2845],[500,8946.7407],[750,13035.2647],[1000,17038.6852],[1500,25263.5252],[2000,33595.8434],[2500,41726.9976],[3000,50246.1853],[3500,58377.6459],[4000,67068.1742],[4500,75222.9282],[5000,86373.7705],[5500,94753.4989]],"lto":2821.8},"insertedAt":"2021-06-11T12:53:25.963614Z","id":"arc_00009VMF8AhXSSRnQDI6HH","iataCode":"77W"},"airOrderSliceId":null}],"originId":"arp_iad_us","origin":{"updatedAt":"2023-10-24T14:53:51.930098Z","timeZone":"America/New_York","sourceType":"ALGORITHM","name":"Washington Dulles International Airport","longitude":-77.45617,"latitude":38.948808,"insertedAt":"2023-10-24T14:53:51.930098Z","id":"arp_iad_us","icaoCode":"KIAD","iataCountryCode":"US","iataCode":"IAD","iataCityCode":"WAS","disabledAt":null,"cityName":"Washington","city":{"updatedAt":"2019-07-30T16:35:28.391340Z","name":"Washington","insertedAt":"2019-07-30T16:35:28.391340Z","id":"cit_was_us","iataCountryCode":"US","iataCode":"WAS"},"alternativeNames":[]},"linkId":"osc_0000AcZNHsC4Z1t32lKHA2","insertedAt":"2023-12-07T14:58:27.815067Z","id":"sli_0000AcZNHsDUTl1N79zPN2","fareBrandName":"Basic Economy","duration":"PT18H3M","destinationId":"arp_nrt_jp","destination":{"updatedAt":"2023-10-24T14:53:51.982454Z","timeZone":"Asia/Tokyo","sourceType":"ALGORITHM_AND_MANUAL_REVIEW","name":"Narita International Airport","longitude":140.39285,"latitude":35.773554,"insertedAt":"2023-10-24T14:53:51.982454Z","id":"arp_nrt_jp","icaoCode":"RJAA","iataCountryCode":"JP","iataCode":"NRT","iataCityCode":"TYO","disabledAt":null,"cityName":"Tokyo","city":{"updatedAt":"2019-07-30T16:35:28.376867Z","name":"Tokyo","insertedAt":"2019-07-30T16:35:28.376867Z","id":"cit_tyo_jp","iataCountryCode":"JP","iataCode":"TYO"},"alternativeNames":[]},"deletedAt":null,"conditions":[],"changeable":null,"airOrderId":"ord_0000AcZNHsD8V4jn63p7om"}]}}', true);


        $change = compareSegmentTimes($result);

        dd($change);

        $timestamp = \Carbon\Carbon::now('UTC')->timestamp; // UTC
        $date = \Carbon\Carbon::now('UTC')->format("Y-m-d H:i:s");
        $timezone = Session::get('user')->customClaims["timezone"] ?? "America/New_York";

        $paxData = [
            "title" => "mr",
            "given_name" => "Markus",
            "family_name" => "Proctor"
        ];

        $result["baggages"] = [];

        $user = Session::get('user');

        $cfar = true;

        $owner = $result["owner"]["name"];
            $refID = $result["id"];
            $booking_reference = $result["booking_reference"];

            $transactionID = generateRandomString(12);

        $data = [
            "total_amount" => $result["total_amount"],
            "cfar" => $cfar,
            "user" => $user,
            "paidAmount" => 800.23,
            "pointsApplied" => 0.00,
            "order" => $result,
            "transactionID" => $transactionID,
            "booking_reference" => $booking_reference,
            "passengers" => $paxData,
            "timestamp" => $timestamp,
            "date" => $date,
            "timezone" => $timezone,
            "iata_destination" => "Paris",
            "destination" => "Paris",
            "refID" => "ord_0000AbykEXBI85hlvy5i2S"
        ];

        $email = $user->email;
        $subject = "[Boarding Pass] Booking $booking_reference Confirmed with $owner";

        Mail::send('emails.transactions.flight-purchase', array("data" => $data), function($message) use($email, $subject)
        {
            $message
                ->to($email)
                ->from("no-reply@venti.co","Venti")
                ->replyTo("no-reply@venti.co")
                ->bcc("admin@venti.co")
                ->subject($subject);
        });

        return "ok";
        
    }   

    public function previewHotelBooking(){
        $result = '{"data":{"key_collection":null,"check_out_date":"2024-02-23","check_in_date":"2024-02-20","supported_loyalty_programme":null,"guests":[{"family_name":"Proctor","given_name":"Markus"}],"loyalty_programme_account_number":null,"accommodation":{"cheapest_rate_total_amount":"235.71","ratings":[{"source":"priceline","value":4}],"check_in_information":null,"cheapest_rate_currency":"USD","review_score":8.0,"rooms":[{"beds":[{"type":"double","count":2},{"type":"double","count":1},{"type":"king","count":1},{"type":"double","count":2},{"type":"queen","count":2}],"rates":[{"due_at_accommodation_currency":"USD","due_at_accommodation_amount":"144.57","quantity_available":47,"cancellation_timeline":[{"refund_amount":"235.71","currency":"USD","before":"2024-02-17T08:00:00Z"},{"refund_amount":"157.14","currency":"USD","before":"2024-02-20T08:00:00Z"}],"supported_loyalty_programme":null,"board_type":"room_only","fee_amount":"0.00","tax_currency":"USD","base_currency":"USD","payment_type":"pay_now","base_amount":"207.90","tax_amount":"27.81","fee_currency":"USD","total_currency":"USD","total_amount":"235.71","payment_method":"balance","conditions":[{"title":"Cancellation Policy","description":"Good news! Between Monday, May 25th, 2020 at 5pm and Saturday, Feb 17th, 2024 at midnight local hotel time (in Los Angeles), you may cancel your reservation for a full refund. Between Saturday, Feb 17th, 2024 at midnight and Tuesday, Feb 20th, 2024 at midnight local hotel time (in Los Angeles), you may cancel your reservation for a partial refund. After Tuesday, Feb 20th, 2024 at midnight local hotel time (in Los Angeles), you are not allowed to change or cancel your reservation. If you cancel your reservation after Feb 20, you will still be charged for the full amount."},{"title":"Refund Policy","description":"Risk-free booking! Cancel before 2024-02-17 and you\'ll pay nothing! Any cancellation received within 3 days prior to the arrival date will incur the first night\'s charge. Failure to arrive at your hotel or property will be treated as a No-Show and will incur the first night\'s charge (Hotel policy)."},{"title":"Photo Policy","description":"The reservation holder must present a valid photo ID and credit card at check-in. The credit card is required for <b>the mandatory fee listed above as well as</b> any additional hotel incidental charges such as parking, phone calls or minibar charges which are not included in the room rate."},{"title":"Promo has been applied","description":"<b>Negotiated Special Details:</b><li><em>Early Booking Saver. Super Hot Deal</em>&nbsp;&#151;&nbsp;null</li><li>Offer Details:&nbsp;Negotiated Specials may be limited to certain dates and subject to availability.</li>"},{"title":"Rate Description","description":"Special Rate"},{"title":"Mandatory Fee Policy","description":"This hotel charges an additional $144.57 USD mandatory fee. This fee will be charged to you directly by the hotel. Mandatory fees are not optional and typically cover items such as resort fees, energy charges or safe fees. The amount of the charge is subject to change."},{"title":"Hotel Occupancy Policy","description":"All rooms booked for single occupancy (i.e. 1 adults). Accommodations for more than this are not guaranteed."},{"title":"Room Charge Disclosure","description":"Your credit card is charged the total cost at time of purchase. Prices and room availability are not guaranteed until full payment is received."},{"title":"Hotel Pet Policy","description":"Pets are not allowed.\n"},{"title":"Important Information","description":"Guests are required to show a photo ID and credit card upon check-in. Please note that all Special Requests are subject to availability and additional charges may apply. In the event of an early departure, the property will charge you the full amount for your stay. A damage deposit of USD 200 is required on arrival. This will be collected by credit card. You should be reimbursed on check-out. Your deposit will be refunded in full by credit card, subject to an inspection of the property. Guests under the age of 21 can only check in with a parent or official guardian. Should the guest folio exceed the daily limit, an additional pre-authorization will be made automatically. A maximum of 4 reservations can be made under 1 guest name at any given time. If a guest is booking multiple rooms, they will need to provide the correct guest name for that specific room. Name on the reservation must match the valid ID shown as verification upon check in. Roll-away beds are available upon request and may not fit in all rooms. Daily Resort Fee includes: - WiFi (In-room and property wide)-no device restrictions - Local and Toll Free Calls - 2 in-room bottles of water upon arrival - Concierge Services - Boarding Pass Print Out - Parking (Self & Valet) -Exclusive access to our virtual funbook – enjoy hundreds of dollars in savings!"}],"source":"priceline","id":"rat_0000Ae5UU8ltPT5BaUFagS"}],"photos":[],"name":"Alexandria Iconic Double"}],"rating":4,"photos":[{"url":"https://q-xx.bstatic.com/xdata/images/hotel/max1000/432614912.jpg?k=6e1ba3e70a09012db2024a0a139be037852f6112e8fbb3a82ff3516ca7698070&o="},{"url":"https://q-xx.bstatic.com/xdata/images/hotel/max1000/495698838.jpg?k=7ad01514c3b2c25c2daa88d16541a1e3176248beaea2b460e121d9f97fb0c146&o="},{"url":"https://q-xx.bstatic.com/xdata/images/hotel/max1000/495698780.jpg?k=46804f1a3003ad246505222d6b046a81ed79387909d0be93f8a5f78bca567f60&o="},{"url":"https://q-xx.bstatic.com/xdata/images/hotel/max1000/495568007.jpg?k=e6508a74e3e09ae8077890fc091eed84db84be2f6dbfe19fb61783636cfd2d4e&o="},{"url":"https://q-xx.bstatic.com/xdata/images/hotel/max1000/495567679.jpg?k=cd042b97558d2b9cbf7e131cba4d3422c9a1797ef0017964025b634378769905&o="},{"url":"https://q-xx.bstatic.com/xdata/images/hotel/max1000/495567132.jpg?k=6579213558aaa04283b5980d1f2ecb8b1ab7fad73322e9fdf829962048414f0d&o="},{"url":"https://q-xx.bstatic.com/xdata/images/hotel/max1000/495567144.jpg?k=47250cb7f7c912aff356b9732e8feeeaf94547db36cfd24c77b01dd4614e74e2&o="},{"url":"https://q-xx.bstatic.com/xdata/images/hotel/max1000/347682602.jpg?k=3479c8fc47aff899bb1b20841fdd24d168d9ac28d63db311a7e5128f2ac4f771&o="},{"url":"https://q-xx.bstatic.com/xdata/images/hotel/max1000/347681013.jpg?k=bdc870bd60ea0b8cda63340a17b18b7cc67118918d4fb49cd3a6c62106f55027&o="},{"url":"https://q-xx.bstatic.com/xdata/images/hotel/max1000/434402335.jpg?k=76e245c87fbcf2979f912c8c28f30b24e8ba1cbf65eb8145d17b9e3482cbdab2&o="}],"amenities":[{"description":"Parking","type":"parking"}],"phone_number":"00000 000 000","chain":{"name":"Independent Hotels"},"email":"test-mode-accommodation@example.com","location":{"geographic_coordinates":{"latitude":36.142917633057,"longitude":-115.15780639648},"address":{"line_one":"2535 South Las Vegas Boulevard","city_name":"Las Vegas","postal_code":"89109","country_code":"US","region":null}},"description":"Expansive outdoor pools, gourmet restaurants and a spa and wellness center are featured at the SAHARA Las Vegas. Free WiFi is provided. Las Vegas Convention Center is 5 minutes’ drive. Views of the surrounding area can be viewed from the terrace of SAHARA Las Vegas. A casino and on-site shops are offered. Free parking is provided. Spanish flavors mixed with contemporary culinary techniques are featured in dishes at the on-site Bazaar restaurant. Bellagio Fountains are 2.2 mi away. McCarran International Airport is 4.3 mi away from SAHARA Las Vegas.","name":"SAHARA Las Vegas","id":"acc_0000AWPszPDP9j7mVO1ukt"},"rooms":1,"confirmed_at":"2024-01-22T00:43:56.233924Z","cancelled_at":null,"reference":"POZDTF","status":"confirmed","id":"bok_0000Ae5Ueyix6e1Nby2jOi"}}';

        $result = json_decode($result, true)["data"];

        $guests = $result["guests"];

        $owner = $result["accommodation"]["name"];
        $refID = $result["id"];
        $booking_reference = $result["reference"];
        $transactionID = generateRandomString(12);

        $user = Session::get('user');

        $timestamp = \Carbon\Carbon::now('UTC')->timestamp; // UTC
        $date = \Carbon\Carbon::now('UTC')->format("Y-m-d H:i:s");

        $rate = $result["accommodation"]["rooms"][0]["rates"][0];

        $timezone = Session::get('user')->customClaims["timezone"] ?? "America/New_York";

        $address = $result["accommodation"]["location"]["address"];

        $addressString = "";

        foreach($address as $key => $value){
            $addressString .= $value . "<br>";
        }

        $data = [
            "total_amount" => $rate["total_amount"],
            "user" => $user,
            "paidAmount" => 800.23,
            "pointsApplied" => 20.00,
            "order" => $refID,
            "transactionID" => $transactionID,
            "booking_reference" => $booking_reference,
            "guests" => $guests,
            "timestamp" => $timestamp,
            "timezone" => $timezone,
            "date" => $date,
            "refID" => $refID,
            "base_amount" => $rate["base_amount"],
            "tax_amount" => $rate["tax_amount"],
            "fee_amount" => $rate["fee_amount"],
            "due_at_accommodation_amount" => $rate["due_at_accommodation_amount"],
            "name" => $owner,
            "address" => $address,
            "addressString" => $addressString,
            "adults" => sizeof($result["guests"]),
            "rooms" => $result["rooms"],
            "accommodation" => $result["accommodation"],
            "check_in_date" => $result["check_in_date"],
            "check_out_date" => $result["check_out_date"],
            "id" => $refID,
            "showPricing" => false,
            "imgBlock" => false
        ];

        $email = $user->email;
        $subject = "[Boarding Pass] Booking $booking_reference Confirmed with $owner";

        Mail::send('emails.transactions.hotel-purchase', array("data" => $data), function($message) use($email, $subject)
        {
            $message
                ->to($email)
                ->from("no-reply@venti.co","Venti")
                ->replyTo("no-reply@venti.co")
                ->bcc("admin@venti.co")
                ->subject($subject);
        });

        return "ok";
    }

    public function previewTrackerEmail(){
        $tracker = (object) [
            "iata_origin" => "IAD",
            "origin" => " Washington Dulles International Airport",
            "adults" => "1",
            "strike" => "50",
            "destination" => " Paris Charles de Gaulle Airport",
            "type" => "round-trip",
            "points" => 1,
            "uid" => "fZH7V5jAxVcfBSYUT7dAlskwpUi1",
            "createdAt" => 1707423582,
            "trackerID" => "JJbRmc6H5L6r",
            "children" => "0",
            "iata_destination" => "CDG",
            "expiry" => 1708992000,
            "class" => "economy",
            "end" => "2024-03-15",
            "start" => "2024-02-27",
            "infants" => 0,
            "status" => "active",
            "price" => "561.78",
        ];

        $iata_destination = "CDG";
        $strike = "50";
        $data = [
            "tracker" => $tracker,
            "trackerID" => "JJbRmc6H5L6r",
            "data" => [
                "search_id" => "prq_0000Af3AMx9gIksAeNCXSn",
            "base_amount" => "51.00",
            "tax_amount" => "487.4",
            "total_amount" => "538.40",
            "airline" => [
              "logo_symbol_url" => "https://assets.duffel.com/img/airlines/for-light-background/full-color-logo/SK.svg",
              "logo_lockup_url" => "https://assets.duffel.com/img/airlines/for-light-background/full-color-lockup/SK.svg",
              "conditions_of_carriage_url" => "https://www.flysas.com/gb-en/legal-info/conditions-of-carriage/",
              "iata_code" => "SK",
              "name" => "SAS",
              "id" => "arl_00009VME7DDSWSIAygCT1S",
            ],
            "slices" => [
                0 => [
                    "destination" =>  [
                        "icao_code" => "EKCH",
                        "iata_city_code" => "CPH",
                        "city_name" => "Copenhagen",
                        "airports" => null,
                        "iata_country_code" => "DK",
                        "iata_code" => "CPH",
                        "latitude" => 55.617962,
                        "longitude" => 12.653382,
                        "city" =>  [
                        "icao_code" => null,
                        "iata_city_code" => "CPH",
                        "city_name" => null,
                        "iata_country_code" => "DK",
                        "iata_code" => "CPH",
                        "latitude" => null,
                        "longitude" => null,
                        "time_zone" => null,
                        "type" => "city",
                        "name" => "Copenhagen",
                        "id" => "cit_cph_dk",
                        ],
                      "time_zone" => "Europe/Copenhagen",
                      "type" => "airport",
                      "name" => "Copenhagen Airport",
                      "id" => "arp_cph_dk",
                    ],
                "origin" =>  [
                  "icao_code" => "KIAD",
                  "iata_city_code" => "WAS",
                  "city_name" => "Washington",
                  "airports" => null,
                  "iata_country_code" => "US",
                  "iata_code" => "IAD",
                  "latitude" => 38.948808,
                  "longitude" => -77.45617,
                  "city" =>  [
                    "icao_code" => null,
                    "iata_city_code" => "WAS",
                    "city_name" => null,
                    "iata_country_code" => "US",
                    "iata_code" => "WAS",
                    "latitude" => null,
                    "longitude" => null,
                    "time_zone" => null,
                    "type" => "city",
                    "name" => "Washington",
                    "id" => "cit_was_us",
                  ],
                  "time_zone" => "America/New_York",
                  "type" => "airport",
                  "name" => "Washington Dulles International Airport",
                  "id" => "arp_iad_us",
                ],
                "duration" => "PT8H",
                "aircraft" => null,
                "departing_at" => "2024-02-27T17:15:00",
                "arriving_at" => "2024-02-28T07:15:00",
                "carrier" => [
                  "logo_symbol_url" => "https://assets.duffel.com/img/airlines/for-light-background/full-color-logo/SK.svg",
                  "logo_lockup_url" => "https://assets.duffel.com/img/airlines/for-light-background/full-color-lockup/SK.svg",
                  "conditions_of_carriage_url" => "https://www.flysas.com/gb-en/legal-info/conditions-of-carriage/",
                  "iata_code" => "SK",
                  "name" => "SAS",
                  "id" => "arl_00009VME7DDSWSIAygCT1S",
                ],
                "flight_number" => "926",
                "passengers" => [
                  0 => [
                    "cabin" => [
                      "amenities" => null,
                      "marketing_name" => "Economy",
                      "name" => "economy",
                    ],
                    "baggages" => [
                      0 => [
                        "quantity" => 1,
                        "type" => "carry_on",
                      ],
                    ],
                    "cabin_class_marketing_name" => "Economy",
                    "fare_basis_code" => "LLXA6GHT",
                    "passenger_id" => "pas_0000Af3AMx54ZtBcQ52rHv",
                    "cabin_class" => "economy",
                  ],
                ],
                "class" => "Sas Go Light",
              ],
              1 =>  [
                "destination" =>  [
                  "icao_code" => "LFPG",
                  "iata_city_code" => "PAR",
                  "city_name" => "Paris",
                  "airports" => null,
                  "iata_country_code" => "FR",
                  "iata_code" => "CDG",
                  "latitude" => 49.011244,
                  "longitude" => 2.548962,
                  "city" =>  [
                    "icao_code" => null,
                    "iata_city_code" => "PAR",
                    "city_name" => null,
                    "iata_country_code" => "FR",
                    "iata_code" => "PAR",
                    "latitude" => null,
                    "longitude" => null,
                    "time_zone" => null,
                    "type" => "city",
                    "name" => "Paris",
                    "id" => "cit_par_fr",
                  ],
                  "time_zone" => "Europe/Paris",
                  "type" => "airport",
                  "name" => "Paris Charles de Gaulle Airport",
                  "id" => "arp_cdg_fr",
                ],
                "origin" =>  [
                  "icao_code" => "EKCH",
                  "iata_city_code" => "CPH",
                  "city_name" => "Copenhagen",
                  "airports" => null,
                  "iata_country_code" => "DK",
                  "iata_code" => "CPH",
                  "latitude" => 55.617962,
                  "longitude" => 12.653382,
                  "city" =>  [
                    "icao_code" => null,
                    "iata_city_code" => "CPH",
                    "city_name" => null,
                    "iata_country_code" => "DK",
                    "iata_code" => "CPH",
                    "latitude" => null,
                    "longitude" => null,
                    "time_zone" => null,
                    "type" => "city",
                    "name" => "Copenhagen",
                    "id" => "cit_cph_dk",
                  ],
                  "time_zone" => "Europe/Copenhagen",
                  "type" => "airport",
                  "name" => "Copenhagen Airport",
                  "id" => "arp_cph_dk",
                ],
                "duration" => "PT1H55M",
                "aircraft" => null,
                "departing_at" => "2024-02-28T17:25:00",
                "arriving_at" => "2024-02-28T19:20:00",
                "carrier" => [
                  "logo_symbol_url" => "https://assets.duffel.com/img/airlines/for-light-background/full-color-logo/SK.svg",
                  "logo_lockup_url" => "https://assets.duffel.com/img/airlines/for-light-background/full-color-lockup/SK.svg",
                  "conditions_of_carriage_url" => "https://www.flysas.com/gb-en/legal-info/conditions-of-carriage/",
                  "iata_code" => "SK",
                  "name" => "SAS",
                  "id" => "arl_00009VME7DDSWSIAygCT1S",
                ],
                "flight_number" => "559",
                "passengers" => [
                  0 => [
                    "cabin" => [
                      "amenities" => [
                        "wifi" => [
                          "cost" => "n/a",
                          "available" => false,
                        ],
                        "seat" => [
                          "pitch" => "31",
                          "legroom" => "standard",
                        ],
                        "power" => [
                          "available" => false,
                        ],
                      ],
                      "marketing_name" => "Economy",
                      "name" => "economy",
                    ],
                    "baggages" => [
                      0 => [
                        "quantity" => 1,
                        "type" => "carry_on",
                      ],
                    ],
                    "cabin_class_marketing_name" => "Economy",
                    "fare_basis_code" => "LLXA6GHT",
                    "passenger_id" => "pas_0000Af3AMx54ZtBcQ52rHv",
                    "cabin_class" => "economy",
                  ],
                ],
                "class" => "Sas Go Light",
              ],
            ],
            "default_order" => 0,
            "offer" => "off_0000Af3ANKfirnAXaPMx6O_0",
            "partial" => true,
            "total_duration" => 595,
            "total_flights" => 2,
            "venti_offer_id" => "595-538.40-SAS-2"
            ],
            "createdAt" => 1708371571,
            "url" => "https://venti.co/flights/search?from=(IAD) Washington Dulles International Airport&to=(CDG) Paris Charles de Gaulle Airport&type=round-trip&class=economy&start=02/27/2024&end=03/15/2024&adults=1&children=0&infants=0"
        ];


        $auth = app('firebase.auth');
        $user = $auth->getUserByEmail("proctor.markus@gmail.com");
        $email = $user->email;

        return view("emails.alerts.flight-tracker", array("data" => $data));
        
        $subject = "[Price Alert] We Found a Flight to $iata_destination under " . $strike . " USD";
        Mail::send('emails.alerts.flight-tracker', array("data" => $data), function($message) use($email, $subject)
            {
                $message
                    ->to($email)
                    ->from("no-reply@venti.co","Venti")
                    ->replyTo("no-reply@venti.co")
                    ->subject($subject);
            });
    }

}