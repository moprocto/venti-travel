<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Newsletter;
use \DrewM\MailChimp\MailChimp;
use Kreait\Firebase\Exception\FirebaseException;
use Kreait\Firebase\Contract\Messaging;
use Kreait\Firebase\Messaging\CloudMessage;
use Kreait\Firebase\Messaging\Notification as Notification;
use Kreait\Firebase\Messaging\ApnsConfig as ApnsConfig;
use Carbon\Carbon as Carbon;
use Carbon\CarbonPeriod;
use Redirect;
use Session;
use Mail;
use Http;
use Dwolla;
use Validator;
use App\Models\Wallet as Wallet;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $firestore = app('firebase.firestore');
        $db = $firestore->database();

        $customerDocs = $db->collection('Customers')->documents();
        $customers = [];

        foreach($customerDocs as $customer){
            $customer = $customer->data();
            if($customer["status"] != "suspended" && $customer["status"] != "deactivated" && $customer["status"] != "unverified"){
                array_push($customers, $customer);
            }            
        }

        $data = [
            "title" => "Admin | Customers",
            "customers" => $customers
        ];

        return view("admin.customers", $data);
    }

    public function users(){
        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $auth = app('firebase.auth');

        $i = 0;
        $users = $this->listUsers();

        foreach($users as $user){

            // all these accounts are related to BoardingPass

            // check customer record in firebase

            $customer = false;

            $customerDocs = $db->collection("Customers")->where("uid","=",$user->uid)
                    ->documents();
            foreach($customerDocs as $customer){
                $customer = $customer->data();
            }

            $users[$user->uid] = (array) $user;

            if($customer){
                $users[$user->uid]["customer"] = $customer;
            }
        }

        $data = [
            "title" => "Admin | Users",
            "users" => $users
        ];

        return view("admin.users", $data);
    }

    public function customers(){
        ini_set('max_execution_time', 800);
        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $auth = app('firebase.auth');
        $dwolla = new Dwolla;

        $i = 0;
        $userList = $auth->listUsers($defaultMaxResults = 3000, $defaultBatchSize = 3000);
        $users = [];

        $emailVerified = $phoneVerified = $appPaid = $active = $nonZero = $balances = $zero = $high = $med = $points =  $redeemed = 0;

        foreach($userList as $user){
            $createdAt = $user->metadata->createdAt->format("Y-m-d");
            $carbonCreatedAt = Carbon::createFromFormat("Y-m-d", $createdAt)->timestamp;
            $carbonCutoff = Carbon::createFromFormat("Y-m-d", "2023-11-01")->timestamp;

            if($carbonCreatedAt < $carbonCutoff){
                continue;
            }
            if($user->disabled == true){
                continue;
            }

            array_push($users, $user);

        }

        foreach($users as $user){

            if($user->emailVerified == true){
                $emailVerified++;
            } else {
                continue;
            }
            $phoneNumber = $user->customClaims["phoneNumber"] ?? false;
            $appPay = $user->customClaims["appPaid"] ?? false;

            if($phoneNumber){
                $phoneVerified++;
            } else {
                continue;
            }
            if($appPay){
                $appPaid++;
            }

            $customer = $dwolla->getCustomer($user, $auth, $db);
                
            if($customer){
                if($customer["status"] == "verified"){
                    $active++;
                    // 

                    // get wallet info 

                    $wallet = $dwolla->getWallet($customer["customerID"]);
                    $walletDetails = $dwolla->getFundingSourceBalance($wallet->_links->self->href);
                    $balance = (float) $walletDetails->balance->value;

                    if($balance > 0){
                        $nonZero++;
                        if($balance > 25000){
                            $high++;
                        }
                        if($balance <= 25000 && $balance > 7500){
                            $med++;
                        }
                    } else {
                        $zero++;
                        continue;
                    }

                    $balances += $balance;
                }
            }

            if($user->uid != "ClQ5SM3JWah2zYAu7pjMZRg14482"){
                // excludes admin transactions
                $transactions = [];

                $transactionDocs = $db->collection("Transactions");
                $transactionDocs = $transactionDocs
                    ->where("uid","=",$user->uid)
                    ->documents();
                // I need a separate firebase instance for sandboxing

                foreach($transactionDocs as $transactionDoc){
                    $transactionDoc = $transactionDoc->data();

                    if($transactionDoc["type"] == "points-withdraw"){
                        $redeemed += $transactionDoc["amount"];
                    }

                    array_push($transactions,  $transactionDoc);
                }

                $points += getPointsBalance($transactions);
            }
        }



        

        
        $data = [
            "title" => "Admin | Users",
            "users" => $users,
            "dwolla" => new Dwolla
        ];

        return view("admin.users", $data);

    }

    public function userStats(){
        dd([
            "users" => sizeof($users),
            "emailVerified" => $emailVerified,
            "phoneVerified" => $phoneVerified,
            "appPaid" => $appPaid,
            "active" => $active,
            "nonZero" => $nonZero,
            "zero" => $zero,
            "balances" => $balances,
            "avgBal" => ($balances/$nonZero),
            "firstRenew" => $high,
            "businessRenew" => $med,
            "points" => $points,
            "redeemed" => $redeemed,
            "redemptionrate" => round(($redeemed/$points) * 100, 2)
        ]);
    }

    public function getProfile(Request $request){
        $uid = $request->uid;
        $firestore = app('firebase.firestore');
        $auth = app('firebase.auth');

        try{
            $user = (array) $auth->getUser($uid);

            $customClaims = $user["customClaims"] ?? false;

            $html = '<p class="p-0 m-0"><a href="/admin/user/' . $uid .'" target="_blank">' . $user["displayName"] . '</a><br>' . $uid . '</p>';
            $html .= '<p class="p-0 m-0">' . $user["email"] . '</p>';

            if($customClaims){
                $isPaidAvailable = $customClaims["appPaid"] ?? false;

                if($isPaidAvailable){
                    $paid = $customClaims["appPaid"] ? "Paid" : "Not Paid";
                    $html .= '<p class="p-0 m-0" data-paid="' . $paid . '">' . $paid . '</p>';         
                }
                
                $html .= '<p class="p-0 m-0">' . $customClaims["phoneNumber"] ?? 'n/a' . '</p>';
                $html .= '<p class="p-0 m-0">' . ($customClaims["referralKey"] ?? 'n/a') . '</p>';
                $html .= '<p class="p-0 m-0">Referred By: ' . ($customClaims["referrer"] ?? 'n/a') . '</p>';
                $html .= '<p class="p-0 m-0">' . ($customClaims["subscription"] ?? 'n/a') . '</p>';
            }

        }
        catch(FirebaseException $e){
            $html = "<p>User Deleted: $uid</p>";
        }

        return json_encode($html);

    }

    public function loginAs($uid){
        Session::flush();
        Session::regenerate();


        $firestore = app('firebase.firestore');
        $auth = app('firebase.auth');

        try{
            $user = $auth->getUser($uid);
            Session::put('user', $user);
            Session::put('active', true);
            return Redirect::to('/trips');
        }
        catch(FirebaseException $e) {

        }
    }

    public function getDwolla(Request $request){
        $dwolla = new Dwolla;

        $customerUrl = env("DWOLLA_URL_" . strtoupper(env('APP_ENV'))) . "/customers/" . $request->customer;

        $customer = $dwolla->readCustomer($customerUrl);

        // get the funding sources

        $fundingSources = $dwolla->fundingSources($customerUrl);

        $html = '<p class="p-0 m-0">' . $customer->status . '</p>';

            $firestore = app('firebase.firestore');
            $db = $firestore->database();
            $user = Session::get('user');
            $transactions = [];

            $transactionDocs = $db->collection("Transactions");
            $transactionDocs = $transactionDocs
                ->where("uid","=",$customer->correlation_id)
                ->documents();
            // I need a separate firebase instance for sandboxing

            foreach($transactionDocs as $transactionDoc){
                array_push($transactions,  $transactionDoc->data());
            }

            $points = getPointsBalance($transactions);

        if($fundingSources){
            $html .= "<ul>";
            $html .= "<li class='walletPoints'>" . $points . " Points</li>";

            foreach($fundingSources as $fundingSource){
                $type = ucfirst($fundingSource->type);
                $balance = "";
                if($type == "Bank"){
                    $balance = $fundingSource->bankName . " | " . ucfirst($fundingSource->status);
                    $html .= "<li class=''>" . $type . ": " . "$balance</li>";
                }
                if($type == "Balance"){
                    $wallet = env("DWOLLA_URL_" . strtoupper(env('APP_ENV'))) . "/funding-sources/" . $fundingSource->id;
                    $walletDetails = $dwolla->getFundingSourceBalance($wallet);
                    $balance = "$". number_format((float) $walletDetails->balance->value,2);
                    $html .= "<li class='walletBalance' data-balance='" . str_replace("$","",str_replace(",","",$balance)) . "'>" . $type . ": " . "$balance</li>";
                }
                if($type != "Bank" && $type != "Balance"){
                    $html .= "<li>" . $type . ": " . "$balance</li>";
                } else {
                    //;
                    
                }
                
            }
            $html .= "</ul>";
        }

        return json_encode($html);
    }

    public function getUser(Request $request){
        $user = $request->user;
        $firestore = app('firebase.firestore');
        $auth = app('firebase.auth');

        $user = $auth->getUser($user);

        //
    }

    public function wishlists(){
        $wishlists = Wishlists::all();

        $data = [
            "wishlists" => $wishlists
        ];

        return view("admin.wishlists", $data);
    }

    public function getOrders(){
        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $auth = app('firebase.auth');
        $orders = [];

        $orderList = $db->collection("Orders")->documents();

        foreach($orderList as $order){
            $order = $order->data();
            array_push($orders, $order);
        }
        
        $data = [
            "title" => "Admin | Orders",
            "orders" => $orders
        ];

        return view("admin.orders", $data);
    }

    public function getFundingSources(){
        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $auth = app('firebase.auth');
        $sources = [];

        $sourcesList = $db->collection("Sources")->documents();

        foreach($sourcesList as $source){
            $source = $source->data();
            array_push($sources, $source);
        }
        
        $data = [
            "title" => "Admin | Funding Sources",
            "sources" => $sources
        ];

        return view("admin.sources", $data);
    }

    public function deleteWishlist($id){
        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $item = $db->collection('Wishlists')->document($id)->delete();
    }

    public function navigators(){

        $auth = app('firebase.auth');
        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $navigators = $db->collection('Applications');
        $navigators = $navigators->documents();
        $navigatorList = [];
        $allstars = $oneventi = $invited = $waitlisted = $pro = 0;


        foreach($navigators as $navigator){
            // mark all pending

            $navigator = $navigator->data();
            if(array_key_exists("triptype",$navigator)){
        
                $navigator["allstar"] = "No";
                $navigator["onventi"] = "No";
                $navigator["guider"] = "No"; // has guide and planning experience
                

                if(array_key_exists("pro", $navigator) && $navigator["pro"] == "Yes"){
                    $pro++;
                }

                if(
                    $navigator["countries"] >= 8 &&
                    $navigator["experience"] != "I just want to travel more"
                ){
                    $navigator["allstar"] = "Yes";
                    $allstars++;
                    
                    
                }
                
                if($navigator["status"] == "invited"){
                    $invited++;
                }
                if($navigator["status"] == "waitlisted"){
                    $waitlisted++;
                }

                array_push($navigatorList, $navigator);
            }
        }

        $data = [
            "navigators" => $navigatorList,
            "allstars" => $allstars,
            "invited" => $invited,
            "waitlisted" => $waitlisted,
            "pro" => $pro
        ];

        return view("admin.navigators", $data);
    }

    public function scheduleNavigator($id){

        $auth = app('firebase.auth');
        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $onventi = false;

        // send email to applicatnat

        $ideaDoc = $db->collection('Applications')->document($id);

        $update = $ideaDoc->update([["path" => "status","value" => "invited"]]);

        $navigator = $ideaDoc->snapshot()->data();

        $data = $navigator;

        try{
            $user = $auth->getUserByEmail($navigator["email"]);
            if($user){
                $onventi = true;
            }
        }
        catch(FirebaseException $e){

        }


        $data = ["onventi" => $onventi];

        $email = $navigator["email"];
        $subject = "Schedule Your Venti Navigator Interview";
        $from = "no-reply@venti.co";


        Mail::send('emails.application.interview', $data, function($message) use($email , $subject, $from)
        {
            $message
                ->to($email)
                ->replyTo($email)
                ->from($from)
                ->subject($subject);
        });
    }

    public function waitlistNavigator($id){

        $auth = app('firebase.auth');
        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $onventi = false;

        // send email to applicatnat

        $ideaDoc = $db->collection('Applications')->document($id);

        $update = $ideaDoc->update([["path" => "status","value" => "waitlisted"]]);

        $navigator = $ideaDoc->snapshot()->data();

        $data = $navigator;

        try{
            $user = $auth->getUserByEmail($navigator["email"]);
            if($user){
                $onventi = true;
            }
        }
        catch(FirebaseException $e){

        }

        $data = ["onventi" => $onventi];

        $email = $navigator["email"];
        $subject = "Regarding Your Venti Navigator Application";
        $from = "no-reply@venti.co";


        Mail::send('emails.application.waitlisted', $data, function($message) use($email , $subject, $from)
        {
            $message
                ->to($email)
                ->replyTo($email)
                ->from($from)
                ->subject($subject);
        });
    }

    public function deleteNavigator($id){

        $firestore = app('firebase.firestore');
        $db = $firestore->database();

        $planRef = $db->collection('Applications')->document($id);
        $planRef->delete();
    }

    public function acceptNavigator($id){
        $auth = app('firebase.auth');
        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $onventi = false;

        // send email to applicatnat

        $ideaDoc = $db->collection('Applications')->document($id);

        $update = $ideaDoc->update([["path" => "status","value" => "accepted"]]);

        $navigator = $ideaDoc->snapshot()->data();

        $data = $navigator;

        try{
            $user = $auth->getUserByEmail($navigator["email"]);
            if($user){
                $onventi = true;
            }
        }
        catch(FirebaseException $e){

        }

        $data = ["onventi" => $onventi];

        $email = $navigator["email"];
        $subject = "Your Invitation Awaits to Become a Venti Navigator";
        $from = "no-reply@venti.co";


        Mail::send('emails.application.accepted', $data, function($message) use($email , $subject, $from)
        {
            $message
                ->to($email)
                ->replyTo($email)
                ->from($from)
                ->subject($subject);
        });
    }

    public function user($uid){
        $auth = app('firebase.auth');
        $user = $auth->getUser($uid);
        $firestore = app('firebase.firestore');
        $db = $firestore->database();

        $dwolla = new Dwolla;
        $customer = $dwolla->getCustomer($user, $auth, $db);

        $fundingSources = [];
        $fundingSourcesCount = 0;

        $transactable = $wallet = $secret = $appPaid = false;

        if($customer){
            $fundingSources = []; //$dwolla->fundingSources($customer["customerID"]);

            $appPaid = $user->customClaims["appPaid"] ?? false;

            if($fundingSources){
                foreach($fundingSources as $fundingSource){
                    if(!$fundingSource->removed 
                        && isset($fundingSource->bankAccountType)
                        && $fundingSource->status == "verified"
                    ){
                        // this user can now make deposits and withdrawals using their bank account
                        $fundingSourcesCount++;
                        $transactable = true;
                    }
                    if($fundingSource->type == "balance"){
                        $wallet = explode("funding-sources/",$fundingSource->_links->self->href)[1];
                    }
                    if(!$fundingSource->removed 
                        && isset($fundingSource->bankAccountType)
                        && $fundingSource->status == "unverified"
                    ){
                        $fundingSourcesCount++;
                    }
                }
                // account balances
            }
        }

        else{
            // we need to see if the end user has already paid their application fee
            $appPaid = $user->customClaims["appPaid"] ?? false;

        }

        $canPointsPurchasePoints = $this->canPointsPurchasePoints($user->uid, $db);

        $createdAt = $user->metadata->createdAt->format("Y-m-d");

        $creationDate = new Carbon($createdAt);

        $accountAge = $creationDate->diffInDays(Carbon::now());

        $ranking = $user->customClaims["leaderboard"] ?? "X";


        $data = [
            "title" => "Boarding Pass",
            "description" => "Traveling for free just got whole lot easier.",
            "image" => "https://venti.co/assets/img/boarding-pass-social.jpg",
            "url" => "https://venti.co/boardingpass",
            "user" => $user,
            "customer" => $customer,
            "fundingSources" => $fundingSources,
            "transactable" => $transactable,
            "transactions" => [],
            "wallet" => $wallet,
            "points" => 0,
            "secret" => $secret,
            "appPaid" => $appPaid ? "true" : "false",
            "canPointsPurchasePoints" => $canPointsPurchasePoints,
            "subscription" => $user->customClaims["subscription"] ?? "buddy",
            "upgrade" => "false",
            "accountAge" => $accountAge,
            "fundingSourcesCount" => $fundingSourcesCount,
            "ranking" => $ranking
        ];

        return view ("boardingpass.home", $data);
    }

    public function updateUser($uid, Request $request){
        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $auth = app('firebase.auth');
        $user = $auth->getUser($uid);
        $mediaLink = $request->pic;
        $smsVerified = $user->customClaims["smsVerified"] ?? false;

        $properties = [
            'displayName' => $request->displayName
        ];

        $updatedUser = $auth->updateUser($user->uid, $properties);

        if(strlen($request->bio) > 150){
            Return Redirect::back()->withErrors(["bioerror" => "Your answer to the math question was incorrect"]);
        }

        $auth->setCustomUserClaims($user->uid, [
            'username' => $user->customClaims['username'],
            "gender" => $request->gender,
            "smoking" => $request->smoking,
            "drinking" => $request->drinking,
            "dob" => $request->dob,
            "bio" => $request->bio,
            "occupation" => $request->occupation,
            "instagram" => str_replace('@','',$request->instagram),
            "requests" => $request->requests,
            'genderPref' => $request->genderPref,
            'sizePref' => $request->sizePref,
            'agePref' => $request->agePref,
            'spendingPref' => $request->spendingPref,
            'smokingPref' => $request->smokingPref,
            'drinkingPref' => $request->drinkingPref,
            "religion" => $request->religion,
            "smsVerified" => $smsVerified,
            "languages" => languagesToObject($request->languages),
            "interests" => interestsToObject($request->interests)
        ]);

        if($request->file("avatar") !== null){

            //dd($request->file("avatar"));
            $storage =  app('firebase.storage');
            $defaultBucket = $storage->getBucket();
            $file = $request->file("avatar");
            $filename = rand(999,9999999) . "-" . cleanFileName($file->getClientOriginalName());
            $extension = strtolower($request->file("avatar")->getClientOriginalExtension());

            $file = $this->shrinkImage($file, true);

            $uploadResult = $defaultBucket->upload(
                
                file_get_contents($file), [
                    'name' => $filename,
                    'uploadType'=> "media",
                    'predefinedAcl' => 'publicRead',
                    "metadata" => ["contentType"=> 'image/' . $extension],
                ]
            );

            $mediaLink = $uploadResult->info()["mediaLink"];

            $properties = [
                'photoUrl' => $mediaLink
            ];

            $updatedUser = $auth->updateUser($user->uid, $properties);


        }

        // SMS Auth

        Return Redirect::back()->withErrors(["success" => "Your answer to the math question was incorrect"]);
    }

    public function mailchimpList(){

        $MailChimp = new MailChimp(env("MAILCHIMP_APIKEY"));
        $result = $MailChimp->get('lists/' . env('MAILCHIMP_LIST_ID') . "/members", ["count" => 100]);
        
    }

    public function groups(){
        $auth = app('firebase.auth');
        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $groups = Groups::all();
        $groupsList = [];
        $archived = $expired = 0;
        $user = null;

        foreach($groups as $group){

            if(isset($group->clientID)){
                $user = Members::where('uid', $group["clientID"])->first();
            }

            if($group->isExpired()){
                $expired++;
            }

            $group["author"] = $user;


            array_push($groupsList, $group);

            if($group["archived"] == 1){
                $archived++;
            }
        }

        $data = [
            "focus" => "groups",
            "groups" => $groupsList,
            "archived" => $archived,
            "expired" => $expired
        ];

        return view("admin.index", $data);


    }

    public function mailchimpStatus($email){
        $uid = "DKeCom9cqsfMAa5G3tdq6Ksnb2D2"; //vitaliy
         $auth = app('firebase.auth');

        // send user an email
        $uid = "DKeCom9cqsfMAa5G3tdq6Ksnb2D2"; //vitaliy
        $user = $auth->getUser($uid);
        $customClaims = $user->customClaims;

        $customClaims["username"] = "clarkb";

        dd($user);


        return json_encode(Newsletter::isSubscribed($email));
    }

    public function activeNavigators(){
        $auth = app('firebase.auth');
        $firestore = app('firebase.firestore');
        $db = $firestore->database();

        $navigators = $db->collection('Navigators')->documents();
        $navigatorList = [];

        foreach($navigators as $navigator){
            $navigator = $navigator->data();
            $user = $auth->getUser($navigator["uid"]);
            array_push($navigatorList, $user);
        }

        $data = [
            "navigators" => $navigatorList
        ];

        return view("admin.navigator", $data);
    }

    public function verifyEmail(Request $request){
        $auth = app('firebase.auth');
        $firestore = app('firebase.firestore');
        $uid = $request->uid;
        
        $updatedUser = $auth->updateUser($uid, [
            "emailVerified" => true
        ]);
    }

    public function resetPhotoUrl($uid){
        $auth = app('firebase.auth');

        // send user an email
        $uid = "DKeCom9cqsfMAa5G3tdq6Ksnb2D2"; //vitaliy



        /*

        $user = $auth->getUser($uid);

        $data = ["user" => $user];

        $email = $user->email;
        $subject = "Your Venti Profile Picture Was Removed";
        $from = "no-reply@venti.co";


        Mail::send('emails.system.photo-removed', $data, function($message) use($email , $subject, $from)
        {
            $message
                ->to($email)
                ->replyTo($email)
                ->from($from)
                ->subject($subject);
        });
        
        $updatedUser = $auth->updateUser($uid, [
            "photoUrl" => "/assets/img/profile.jpg"
        ]);

        */
    }

    public function makeNavigator($uid){
        $auth = app('firebase.auth');
        $user = $auth->getUser($uid);
        $customClaims = $user->customClaims;

        $customClaims["nav"] = true;
        $auth->setCustomUserClaims($user->uid, $customClaims);
    }

    public function sendVerificationEmail($email){
        $auth = app('firebase.auth');
        $auth->sendEmailVerificationLink($email);
    }

    public function scoresheet($match, $matchee){
        $auth = app('firebase.auth');
        $match = $auth->getUser($match);
        $matchee = $auth->getUser($matchee);

        $compatibility = calcCompatibility($match, $matchee);

        dd([$match, $matchee, $compatibility]);
    }

    public function sendBoardingPassEmails(){
        dd("no");

        $i = 0;

        foreach($subscribers as $subscriber){
            $memberID = md5(strtolower($subscriber));
            

            $result = Http::get("https://emailoctopus.com/api/1.6/lists/3b3c3c3a-3b19-11ee-9258-cf345e7859db/contacts/$memberID", [
                'api_key' => env('EMAIL_OCTOPUS_API_KEY')
            ]);

            $subscriber = json_decode($result->body(),true);

            $referral = getEmailID($subscriber["id"]);

            $email = $subscriber["email_address"];
            $subject = "[Venti Boarding Pass]: Your Referral Link is Enclosed";
            $from = "markus@venti.co";
            $message = "";

            $data = ["referral" => $referral];
            

            echo "$email<br>";
        }

        dd("$i emails sent. Last recipient was $email");
    }

    private function transact(Request $request){

    }

    public function getAutosavesScreen(){
        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $auth = app('firebase.auth');

        $autosaveDocs = $db->collection("Autosaves")->documents();
        $autosaveList = [];

        foreach($autosaveDocs as $autosave){
            $autosave = $autosave->data();
            array_push($autosaveList, $autosave);
        }

        $data = [
            "autosaves" => $autosaveList
        ];

        return view("admin.boardingpass.autosaves", $data);
    }

    public function subscribers(){

        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $auth = app('firebase.auth');

        $subscribersList = $db->collection("VIPS")->documents();

        $subscribers = [];

        foreach($subscribersList as $contact){
            $contact = $contact->data();
            $fields = $contact["fields"];

            array_push($subscribers, [
                "email" => $contact["email_address"],
                "fields" => $fields,
                "id" => $contact["id"]
            ]);
        }

        return $subscribers;
    }

    public function getTransactions(){
        // this retrieves all transactions in the system

        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $auth = app('firebase.auth');

        $transactionData = [];
        $transactions = $db->collection("Transactions")->documents();

        foreach($transactions as $transaction){
            array_push($transactionData, $transaction->data());
        }

        usort($transactionData, function($a, $b) {
            return $b['timestamp'] <=> $a['timestamp'];
        });

        $data = [
            "title" => "Admin | Transactions",
            "transactions" => $transactionData
        ];

        return view("admin.boardingpass.transactions", $data);
    }

    public function getTransactScreen(){
        $data = [
            "title" => "Admin | Create Transaction",
        ];

        return view("admin.boardingpass.transact", $data);
    }



    public function postTransact(Request $request){
        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $auth = app('firebase.auth');
        $dwolla = new Dwolla;
        $user = $auth->getUser($request->customer);

        $customer = $dwolla->getCustomer($user, $auth, $db);

        $transactionCol = $db->collection("Transactions");

        $transactionID = generateRandomString(12);
        $type = $request->type;
        $interest = $request->amount;
        $now = \Carbon\Carbon::now('UTC')->timestamp; // UTC
        $date = \Carbon\Carbon::now('UTC')->format("Y-m-d H:i:s");
        $email = $user->email;
        $note = $request->note;
        $taxable = ($request->taxable == 1) ? true : false;


        if($type == "points-deposit"){
            $transactionInsert = $transactionCol->document($transactionID)->set([
                "transactionID" => $transactionID,
                "uid" => $user->uid,
                "type" => "points-deposit",
                "timestamp" => $now, // UTC
                "date" => $date,
                "amount" => (float) $interest,
                "speed" => "next-available",
                "fee" => 0,
                "status" => "complete",
                "from" => "Venti",
                "to" => "My Boarding Pass",
                "total" => (float) $interest,
                "note" => $note,
                "transferID" => null,
                "transactionUrl" => null,
                "taxable" => $taxable,
                "env" => env('APP_ENV')
            ]);

            $subject = "[Boarding Pass] You Received " . $interest . " Points from Venti";
            $title = "Congrats!";

            $data = [
                "title" => $title,
                "amount" => $interest,
                "description" => "Your Boarding Pass was credited $interest Points for $note with confirmation ID: $transactionID.
                <br><br>
                Points help you buy down the cost of flights, hotels, and more on the Venti platform by up to 100%. Use the link below to check your balance, make deposits, open Mystery Boxes, and more."
            ];
        }

        if($type == "points-withdraw"){
            $transactionInsert = $transactionCol->document($transactionID)->set([
                "transactionID" => $transactionID,
                "uid" => $user->uid,
                "type" => "points-withdraw",
                "timestamp" => $now, // UTC
                "date" => $date,
                "amount" => (float) $interest,
                "speed" => "next-available",
                "fee" => 0,
                "status" => "complete",
                "from" => "My Boarding Pass",
                "to" => "Venti",
                "total" => (float) $interest,
                "note" => $note,
                "transferID" => null,
                "transactionUrl" => null,
                "env" => env('APP_ENV')
            ]);

            $subject = "[Boarding Pass] $interest Points Were Withdrawn";
            $title = "Transaction Notification";

            $data = [
                "title" => $title,
                "amount" => $interest,
                "description" => "Your Boarding Pass was debited $interest Points for $note with confirmation ID: $transactionID.
                <br><br>
                Points are manually deducted from your account when an ACH payment fails or if there is an additional penalty tied to your account. Note that ACH payment failures result in additional fees that may be invoiced to you separately.<br><br>
                <strong>No cash was withdrawn from your account or funding sources.</strong>
                "
            ];
        }

        if($type == "cash-withdraw"){

            $customer = $dwolla->getCustomer($user, $auth, $db);
            $wallet = $dwolla->getWallet($customer["customerID"]);

            $feeWithdraw = $dwolla->transferFromUserWalletToVentiWallet($user, $wallet->_links->self->href, $interest, "Withdraw: $transactionID");

            $transferID = explode("transfers/",$feeWithdraw)[1];

            $data = [
                "transactionID" => $transactionID,
                "uid" => $user->uid,
                "type" => "withdraw",
                "timestamp" => \Carbon\Carbon::now('UTC')->timestamp, // UTC
                "date" => \Carbon\Carbon::now('UTC')->format("Y-m-d H:i:s"),
                "amount" => $interest,
                "speed" => "next-available", // wallet to wallet is always next available
                "fee" => 0,
                "from" => "My Boarding Pass",
                "status" => "pending",
                "to" => "Venti Financial",
                "total" => $interest,
                "note" => $note,
                "transferID" => $transferID,
                "transactionUrl" => $feeWithdraw,
                "env" => env('APP_ENV')
            ];

            $bank = $db->collection("Transactions")->document($transferID)->set($data);
        }

        if($type == "cash-deposit"){
            $customer = $dwolla->getCustomer($user, $auth, $db);
            $wallet = $dwolla->getWallet($customer["customerID"]);

            $feeWithdraw = $dwolla->transferFromVentiWalletToUserWallet($user, $wallet->_links->self->href, $interest, $type);

            $transferID = explode("transfers/",$feeWithdraw)[1];

            $data = [
                "transactionID" => $transactionID,
                "uid" => $user->uid,
                "type" => "deposit",
                "timestamp" => \Carbon\Carbon::now('UTC')->timestamp, // UTC
                "date" => \Carbon\Carbon::now('UTC')->format("Y-m-d H:i:s"),
                "amount" => $interest,
                "speed" => "next-available", // wallet to wallet is always next available
                "fee" => 0,
                "from" => "Venti Financial",
                "status" => "pending",
                "to" => "My Boarding Pass",
                "total" => $interest,
                "note" => $note,
                "transferID" => $transferID,
                "transactionUrl" => $feeWithdraw,
                "env" => env('APP_ENV')
            ];

            $bank = $db->collection("Transactions")->document($transferID)->set($data);
        }

        if($request->email == 1){
            // send an email
            try{
                Mail::send('emails.transactions.admin-transact', array("data" => $data), function($message) use($email, $subject)
                    {
                        $message
                            ->to($email)
                            ->from("no-reply@venti.co","Venti")
                            ->replyTo("no-reply@venti.co")
                            ->subject($subject);
                });
            }
            catch(Exception $e){
                //
            }
        }

        return Redirect::back();
    }

    public function getSnapshots(){
        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $auth = app('firebase.auth');

        $transactionData = [];
        $transactions = $db->collection("BalanceSnapshots")->documents();

        foreach($transactions as $transaction){
            array_push($transactionData, $transaction->data());
        }

        usort($transactionData, function($a, $b) {
            return $b['timestamp'] <=> $a['timestamp'];
        });

        $data = [
            "title" => "Admin | Snapshots",
            "transactions" => $transactionData
        ];

        return view("admin.boardingpass.snapshots", $data);
    }

    public function deleteSnapshot($id){
        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $auth = app('firebase.auth');

        $transactionData = [];
        $transactions = $db->collection("BalanceSnapshots")->document($id);

        $transactions->delete();
    }

    public function deleteTransaction($id){
        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $auth = app('firebase.auth');

        $transactionData = [];
        $transactions = $db->collection("Transactions")->document($id);

        $transactions->delete();
    }

    public function flightSearches(){
        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $auth = app('firebase.auth');

        $searches = [];
        $destinations = [];

        $searchesDocs = $db->collection("FlightSearches")->documents();

        foreach($searchesDocs as $search){
            $search = $search->data();
            if(isset($search["request"]["to"])){
                array_push($destinations, $search["request"]["to"]);
            }
            array_push($searches, $search);
        }

        $data = [
            "searches" => $searches,
            "populars" => array_count_values($destinations)
        ];

        return view("admin.searches", $data);
    }

    public function listUsers(){
        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $auth = app('firebase.auth');

        $i = 0;
        $userList = $auth->listUsers($defaultMaxResults = 3000, $defaultBatchSize = 3000);
        $users =[];


        foreach($userList as $user){
            $createdAt = $user->metadata->createdAt->format("Y-m-d");
            $carbonCreatedAt = Carbon::createFromFormat("Y-m-d", $createdAt)->timestamp;
            $carbonCutoff = Carbon::createFromFormat("Y-m-d", "2023-11-01")->timestamp;

            if($carbonCreatedAt < $carbonCutoff){
                continue;
            }
            if($user->disabled == true){
                continue;
            }

            array_push($users, 
                [
                    "uid" => $user->uid,
                    "displayName" => $user->displayName,
                    "customClaims" => $user->customClaims,
                    "email" => $user->email,
                    "disabled" => $user->disabled,
                    "emailVerified" => $user->emailVerified,
                    "metadata" => $user->metadata
                ]
            );
        }

        return $users;
    }

    public function exportUsers(){
        $users = json_encode($this->listUsers());
        echo $users;
    }
    
    public function exportCustomers(){
        echo $this->exportCollection("Customers");
    }

    public function exportBanks(){
        echo $this->exportCollection("Sources");
    }

    public function exportCards(){
        echo $this->exportCollection("Cards");
    }

    public function exportTransactions(){
        echo $this->exportCollection("Transactions");
    }

    public function exportOrders(){
        echo $this->exportCollection("Orders");
    }

    public function exportMysterbox(){
        echo $this->exportCollection("MysteryBoxOpens");
    }

    public function exportBalanceSnapshots(){
        echo $this->exportCollection("BalanceSnapshots");
    }

    public function exportAutosaves(){
        echo $this->exportCollection("Autosaves");
    }

    public function exportCollection($collection){
        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $auth = app('firebase.auth');

        $items = [];

        $itemDocs = $db->collection($collection)->documents();
        foreach($itemDocs as $item){

            $item = $item->data();

            if($item["uid"]){
                if($item["uid"] == "ClQ5SM3JWah2zYAu7pjMZRg14482"){
                    continue;
                }
                // only export the first 14 chars
                
            }
            array_push($items, $item);
        }

        return json_encode($items);
    }

    public function exportFlightSearch(){
        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $auth = app('firebase.auth');

        $searches = [];

        $searchesDocs = $db->collection("FlightSearches")->documents();
        foreach($searchesDocs as $search){
            $search = $search->data();
            if($search["uid"]){
                // only export the first 14 chars
                if($search["uid"] == "ClQ5SM3JWah2zYAu7pjMZRg14482"){
                    continue;
                }

                // remove template content

                if(isset($search["request"]["from"]) && isset($search["request"]["to"])){
                    if($search["request"]["from"] == "(LGA) LaGuardia Airport" && $search["request"]["to"] == "(CDG) Paris Charles de Gaulle Airport"){
                        continue;
                    }
                    if($search["request"]["from"] == "(MIA) Miami International Airport" && $search["request"]["to"] == "(SIN) Singapore Changi Airport"){
                        continue;
                    }
                    if($search["request"]["from"] == "(SFO) San Francisco International Airport" && $search["request"]["to"] == "(LAS) McCarran International Airport"){
                        continue;
                    }
                    if($search["request"]["from"] == "(SFO) San Francisco International Airport" && $search["request"]["to"] == "Harry Reid International Airport"){
                        continue;
                    }
                }
            }
            if(isset($search["request"]["from"])){
                //echo $search["request"]["to"] ?? "" . "<br>";
            }
            
            array_push($searches, $search);
        }
        //echo "<br><br><br>";
        echo json_encode($searches);
    }

    public function exportHotelSearch(){
        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $auth = app('firebase.auth');

        $searches = [];

        $searchesDocs = $db->collection("HotelSearches")->documents();
        foreach($searchesDocs as $search){
            $search = $search->data();
            if($search["uid"]){
                // only export the first 14 chars
                if($search["uid"] == "ClQ5SM3JWah2zYAu7pjMZRg14482"){
                    continue;
                }


            }
            if(isset($search["request"]["to"])){
                echo $search["request"]["to"] ?? "" . "<br>";
            }
            
            array_push($searches, $search);
        }
        echo "<br><br><br>";
        echo json_encode($searches);
    }

    public function importFlightSearch(){
        $searches = json_decode('[{"request":{"infants":"0","children":"0","adults":"1","start":"12\/22\/2023","from":"(ATL) Hartsfield-Jackson Atlanta International Airport","end":"01\/02\/2024","to":"(PHL) Philadelphia International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (iPhone; CPU iPhone OS 16_6 like Mac OS X) AppleWebKit\/605.1.15 (KHTML, like Gecko) Version\/16.6 Mobile\/15E148 Safari\/604.1","ip":"108.162.238.76","session_id":"0KBE1uJxfdV5","region":"Georgia","queryID":"0KBE1uJxfdV5","timestamp":1701650721},{"request":{"infants":"0","children":"0","adults":"1","start":"01\/01\/2024","from":"(EWR) Newark Liberty International Airport","end":"undefined","to":"(MCO) Orlando International Airport","type":"one-way","class":"first"},"uid":null,"agent":"Mozilla\/5.0 (iPhone; CPU iPhone OS 17_1_2 like Mac OS X) AppleWebKit\/605.1.15 (KHTML, like Gecko) Mobile\/21B101 Instagram 309.1.1.28.108 (iPhone13,4; iOS 17_1_2; en_US; en; scale=3.00; 1284x2778; 537288535)","ip":"162.158.62.97","session_id":"0M6HmjKWIOGH","region":"New Jersey","queryID":"0M6HmjKWIOGH","timestamp":1701550729},{"request":{"infants":"0","children":"0","adults":"1","start":"12\/23\/2023","from":"(SFO) San Francisco International Airport","end":"01\/02\/2024","to":"(YVR) Vancouver International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (iPhone; CPU iPhone OS 16_3 like Mac OS X) AppleWebKit\/605.1.15 (KHTML, like Gecko) Mobile\/20D47 Instagram 309.1.1.28.108 (iPhone14,2; iOS 16_3; en_CA; en; scale=3.00; 1170x2532; 537288535)","ip":"172.71.158.7","session_id":"0MYwHpwg8Mbg","region":"California","queryID":"0MYwHpwg8Mbg","timestamp":1701542306,"selections":[{"offer":"off_0000AcPKMJJ0k8XzzfEARq_0","amount":"592.97","discountPresented":"0.85","direction":"departure"},{"offer":"off_0000AcPKMJJ0k8XzzfEARq_1","amount":"592.97","discountPresented":"0.85","direction":"return"}]},{"request":{"infants":"0","children":"0","adults":"1","start":"12\/29\/2023","from":"(RDU) Raleigh\u2013Durham International Airpor","end":"12\/31\/2023","to":"(AZA) Phoenix-Mesa-Gateway Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (X11; CrOS x86_64 14541.0.0) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/118.0.0.0 Safari\/537.36","ip":"172.70.175.206","region":"Virginia","queryID":"0MxRLOSR2YrV","timestamp":1700388722},{"request":{"infants":"0","children":"0","adults":"1","start":"04\/02\/2024","from":"(LAX) Los Angeles International Airport","end":"04\/08\/2024","to":"(MBJ) Sangster International Airport","type":"round-trip","class":"economy"},"uid":"ONfSXiBmiWTlti","agent":"Mozilla\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/116.0.0.0 Safari\/537.36","ip":"172.71.158.31","region":"California","queryID":"0gyPKpk90lVs","timestamp":1700069167},{"request":{"infants":"0","children":"0","adults":"1","start":"01\/02\/2024","from":"(LGA) LaGuardia Airport","end":"undefined","to":"(TPA) Tampa International Airport","type":"one-way","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (iPhone; CPU iPhone OS 16_6_1 like Mac OS X) AppleWebKit\/605.1.15 (KHTML, like Gecko) Version\/16.6 Mobile\/15E148 Safari\/604.1","ip":"162.158.63.156","session_id":"0k7QaX8C1KYV","region":"New Jersey","queryID":"0k7QaX8C1KYV","timestamp":1701626240,"selections":[{"offer":"off_0000AcRLCOF01lbkft6XQM","amount":"128.90","discountPresented":"0.75","direction":"departure"}]},{"request":{"infants":"0","children":"0","adults":"1","start":"06\/21\/2024","from":"(HPN) Westchester County Airport","end":"06\/28\/2024","to":"(HNL) Daniel K. Inouye International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/119.0.0.0 Safari\/537.36","ip":"172.70.230.15","region":"New Jersey","queryID":"13SWbwMpC3wo","timestamp":1700663437},{"request":{"infants":"0","children":"0","adults":"1","start":"12\/15\/2023","from":"(MKE) General Mitchell International Airport","end":"12\/17\/2023","to":"(OTH) Southwest Oregon Regional Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (iPhone; CPU iPhone OS 17_1_1 like Mac OS X) AppleWebKit\/605.1.15 (KHTML, like Gecko) Mobile\/21B91 Instagram 309.1.1.28.108 (iPhone15,2; iOS 17_1_1; en_US; en; scale=3.00; 1179x2556; 537288535)","ip":"172.69.6.234","session_id":"1XTEbz1lcFJI","region":"Illinois","queryID":"1XTEbz1lcFJI","timestamp":1701724969,"noResults":true},{"request":{"infants":"0","children":"0","adults":"1","start":"06\/28\/2024","from":"(BNA) Nashville International Airport","end":"undefined","to":"(HAN) Noi Bai International Airport","type":"one-way","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (iPhone; CPU iPhone OS 17_1_1 like Mac OS X) AppleWebKit\/605.1.15 (KHTML, like Gecko) Mobile\/21B91 Instagram 309.1.1.28.108 (iPhone13,2; iOS 17_1_1; en_US; en; scale=3.00; 1170x2532; 537288535)","ip":"172.68.73.33","session_id":"1ej81ZkaA1xd","region":"Tennessee","queryID":"1ej81ZkaA1xd","timestamp":1701573122},{"request":{"infants":"0","children":"0","adults":"1","start":"04\/10\/2024","from":"(PNS) Pensacola International Airport","end":"undefined","to":"(NRT) Narita International Airport","type":"one-way","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/117.0.0.0 Safari\/537.36","ip":"172.69.6.192","region":"Illinois","queryID":"1txQbuAW5kA3","timestamp":1700890151},{"request":{"infants":"0","children":"0","adults":"1","start":"12\/08\/2023","from":"(SJC) San Jose International Airport","end":"undefined","to":"(SNA) John Wayne Airport","type":"one-way","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (iPhone; CPU iPhone OS 17_1_1 like Mac OS X) AppleWebKit\/605.1.15 (KHTML, like Gecko) Mobile\/21B91 Instagram 309.1.1.28.108 (iPhone16,2; iOS 17_1_1; en_US; en; scale=3.00; 1290x2796; 537288535)","ip":"172.71.222.94","session_id":"2VZNvYBW3jh6","region":"Virginia","queryID":"2VZNvYBW3jh6","timestamp":1701541073},{"request":{"infants":"0","children":"0","adults":"1","start":"05\/14\/2024","from":"(CHS) Charleston International Airport","end":"05\/19\/2024","to":"(SLC) Salt Lake City International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (iPhone; CPU iPhone OS 17_1_2 like Mac OS X) AppleWebKit\/605.1.15 (KHTML, like Gecko) Mobile\/21B101 Instagram 309.1.1.28.108 (iPhone14,3; iOS 17_1_2; en_US; en; scale=3.00; 1284x2778; 537288535)","ip":"172.69.70.242","session_id":"2YnCLmqv2xWY","region":"Georgia","queryID":"2YnCLmqv2xWY","timestamp":1701605325},{"request":{"infants":"0","children":"0","adults":"1","start":"01\/01\/2024","from":"(LGB) Long Beach Airport","end":"01\/08\/2024","to":"(PDX) Portland International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (iPhone; CPU iPhone OS 17_1_2 like Mac OS X) AppleWebKit\/605.1.15 (KHTML, like Gecko) Mobile\/21B101 Instagram 309.1.1.28.108 (iPhone16,1; iOS 17_1_2; en_US; en; scale=3.00; 1179x2556; 537288535)","ip":"162.158.90.152","session_id":"2pSMFlg37qQh","region":"California","queryID":"2pSMFlg37qQh","timestamp":1701587301,"noResults":true},{"request":{"infants":"0","children":"0","adults":"1","start":"12\/20\/2023","from":"(IND) Indianapolis International Airport","end":"12\/27\/2023","to":"(DFW) Dallas\/Fort Worth International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit\/605.1.15 (KHTML, like Gecko) Version\/17.1 Safari\/605.1.15","ip":"172.71.167.220","region":"Texas","queryID":"32vuXDy0wV68","timestamp":1700533406},{"request":{"infants":"0","children":"0","adults":"1","start":"01\/01\/2024","from":"(BTV) Burlington International Airport","end":"01\/08\/2024","to":"(DEN) Denver International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"facebookexternalhit\/1.1 (+http:\/\/www.facebook.com\/externalhit_uatext.php)","ip":"172.70.34.233","session_id":"3ROaJdsrllTe","region":"Virginia","queryID":"3ROaJdsrllTe","timestamp":1701644747},{"request":{"infants":"0","children":"0","adults":"1","start":"12\/27\/2023","from":"(LAX) Los Angeles International Airport","end":"12\/31\/2023","to":"(PDX) Portland International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (iPhone; CPU iPhone OS 17_1_2 like Mac OS X) AppleWebKit\/605.1.15 (KHTML, like Gecko) Mobile\/21B101 Instagram 309.1.1.28.108 (iPhone16,1; iOS 17_1_2; en_US; en; scale=3.00; 1179x2556; 537288535)","ip":"162.158.90.152","session_id":"3TPcb3cV129j","region":"California","queryID":"3TPcb3cV129j","timestamp":1701587335},{"request":{"infants":"0","children":"0","adults":"1","start":"05\/28\/2024","from":"(OAK) Oakland International Airport","end":"05\/31\/2024","to":"(RNO) Reno\u2013Tahoe International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (Linux; Android 13; SM-G986U Build\/TP1A.220624.014; wv) AppleWebKit\/537.36 (KHTML, like Gecko) Version\/4.0 Chrome\/119.0.6045.164 Mobile Safari\/537.36 Instagram 309.1.0.41.113 Android (33\/13; 450dpi; 1080x2192; samsung; SM-G986U; y2q; qcom; en_US; 541635884)","ip":"162.158.166.227","session_id":"4LB6SqqeT6M0","region":"California","queryID":"4LB6SqqeT6M0","timestamp":1701715004},{"request":{"infants":"0","children":"0","adults":"1","start":"01\/03\/2024","from":"(ORD) O\'Hare International Airport","end":"01\/10\/2024","to":"(LGA) LaGuardia Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/119.0.0.0 Safari\/537.36","ip":"172.70.178.33","region":"Illinois","queryID":"4NHwAjrQvfUM","timestamp":1701115451},{"request":{"infants":"0","children":"0","adults":"1","start":"01\/02\/2024","from":"(RDU) Raleigh\u2013Durham International Airpor","end":"01\/09\/2024","to":"(DFW) Dallas\/Fort Worth International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (iPhone; CPU iPhone OS 16_5 like Mac OS X) AppleWebKit\/605.1.15 (KHTML, like Gecko) Version\/16.5 Mobile\/15E148 Safari\/604.1","ip":"172.70.43.4","session_id":"4PhUP27XYzvV","region":"Virginia","queryID":"4PhUP27XYzvV","timestamp":1701628259},{"request":{"infants":"0","children":"0","adults":"1","start":"11\/21\/2023","from":"(DFW) Dallas\/Fort Worth International Airport","end":"11\/29\/2023","to":"(MSP) Minneapolis-Saint Paul International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (Linux; Android 10; K) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/119.0.0.0 Mobile Safari\/537.36","ip":"172.71.167.209","region":"Texas","queryID":"4TnbGsOY89e9","timestamp":1700340990},{"request":{"infants":"0","children":"0","adults":"1","start":"01\/02\/2024","from":"(ATL) Hartsfield-Jackson Atlanta International Airport","end":"01\/04\/2024","to":"(BWI) Baltimore Washington International Thurgood Marshall Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (Linux; Android 12; SM-G973U Build\/SP1A.210812.016; wv) AppleWebKit\/537.36 (KHTML, like Gecko) Version\/4.0 Chrome\/119.0.6045.193 Mobile Safari\/537.36 Instagram 309.1.0.41.113 Android (31\/12; 280dpi; 720x1361; samsung; SM-G973U; beyond1q; qcom; en_US; 541635892)","ip":"172.69.70.242","session_id":"4blvrGFG2SfA","region":"Georgia","queryID":"4blvrGFG2SfA","timestamp":1701646163},{"request":{"infants":"0","children":"0","adults":"1","start":"01\/27\/2024","from":"(SYD) Sydney Airport","end":"undefined","to":"(AKL) Auckland Airport","type":"one-way","class":"economy"},"uid":"MwMSiSWmrLUIC3","agent":"Mozilla\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/119.0.0.0 Safari\/537.36","ip":"172.70.211.109","session_id":"4e25tL25yUcx","region":"California","queryID":"4e25tL25yUcx","timestamp":1701624490,"selections":[{"offer":"off_0000AcRIayJitlPic21XBM","amount":"130.01","discountPresented":"0.75","direction":"departure"}]},{"request":{"infants":"0","children":"0","adults":"1","start":"12\/15\/2023","from":"(EWR) Newark Liberty International Airport","end":"12\/19\/2023","to":"(LAS) McCarran International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (iPhone; CPU iPhone OS 17_1_1 like Mac OS X) AppleWebKit\/605.1.15 (KHTML, like Gecko) Mobile\/21B91 Instagram 309.1.1.28.108 (iPhone12,1; iOS 17_1_1; en_US; en; scale=2.00; 750x1624; 537288535)","ip":"172.70.110.154","session_id":"4gR4jRU3OaLw","region":"New Jersey","queryID":"4gR4jRU3OaLw","timestamp":1701580106},{"request":{"infants":"0","children":"0","adults":"1","start":"01\/19\/2024","from":"(OKC) Will Rogers World Airport","end":"01\/28\/2024","to":"(MIA) Miami International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (iPhone; CPU iPhone OS 17_1_2 like Mac OS X) AppleWebKit\/605.1.15 (KHTML, like Gecko) Mobile\/21B101 Instagram 309.1.1.28.108 (iPhone14,2; iOS 17_1_2; en_US; en; scale=3.00; 1170x2532; 537288535)","ip":"172.69.65.107","session_id":"5KYSJkHsIuAU","region":"Texas","queryID":"5KYSJkHsIuAU","timestamp":1701553781},{"request":{"infants":"0","children":"0","adults":"1","start":"01\/08\/2024","from":"(BGR) Bangor International Airport","end":"undefined","to":"(RDU) Raleigh\u2013Durham International Airpor","type":"one-way","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (iPhone; CPU iPhone OS 17_0_3 like Mac OS X) AppleWebKit\/605.1.15 (KHTML, like Gecko) Mobile\/21A360 Instagram 309.1.1.28.108 (iPhone15,2; iOS 17_0_3; en_US; en; scale=3.00; 1179x2556; 537288535)","ip":"172.70.130.63","session_id":"5XHuyDkgn2gD","region":"Illinois","queryID":"5XHuyDkgn2gD","timestamp":1701645133},{"request":{"infants":"0","children":"0","adults":"1","start":"03\/07\/2024","from":"(SYD) Sydney Airport","end":"undefined","to":"(AKL) Auckland Airport","type":"one-way","class":"economy"},"uid":"MwMSiSWmrLUIC3","agent":"Mozilla\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/119.0.0.0 Safari\/537.36","ip":"172.69.34.46","session_id":"5kt9vBrrgn7V","region":"California","queryID":"5kt9vBrrgn7V","timestamp":1701624410},{"request":{"infants":"0","children":"0","adults":"1","start":"12\/25\/2023","from":"(TLV) Ben Gurion International Airport","end":"undefined","to":"(BCN) Barcelona\u2013El Prat Josep Tarradellas Airport","type":"one-way","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (Linux; Android 13; SM-S911U Build\/TP1A.220624.014; wv) AppleWebKit\/537.36 (KHTML, like Gecko) Version\/4.0 Chrome\/119.0.6045.164 Mobile Safari\/537.36 Instagram 309.1.0.41.113 Android (33\/13; 480dpi; 1080x2259; samsung; SM-S911U; dm1q; qcom; en_US; 541635892)","ip":"141.101.68.39","session_id":"5yKcRDE49j60","region":"\u00cele-de-France","queryID":"5yKcRDE49j60","timestamp":1701541982},{"request":{"infants":"0","children":"0","adults":"1","start":"01\/03\/2024","from":"(ORD) O\'Hare International Airport","end":"01\/10\/2024","to":"(SEA) Seattle-Tacoma International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (iPhone; CPU iPhone OS 16_6_1 like Mac OS X) AppleWebKit\/605.1.15 (KHTML, like Gecko) Mobile\/20G81 Instagram 309.1.1.28.108 (iPhone13,2; iOS 16_6_1; de_US; de; scale=3.00; 1170x2532; 537288535)","ip":"162.158.155.19","session_id":"60ZbIv6TFxFs","region":"New Jersey","queryID":"60ZbIv6TFxFs","timestamp":1701720564,"selections":[{"offer":"off_0000AcTbVWJr0vRBHsghx4_0","amount":"361.07","discountPresented":"0.75","direction":"departure"},{"offer":"off_0000AcTbVWKCzbilIyqzV7_1","amount":"361.07","discountPresented":"0.75","direction":"return"}]},{"request":{"infants":"0","children":"0","adults":"1","start":"02\/21\/2024","from":"(LAX) Los Angeles International Airport","end":"02\/28\/2024","to":"(MAA) Chennai International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/119.0.0.0 Safari\/537.36","ip":"172.69.34.39","region":"California","queryID":"69rzmWyfcb3F","timestamp":1700264644},{"request":{"infants":"0","children":"0","adults":"1","start":"01\/15\/2024","from":"(BWI) Baltimore Washington International Thurgood Marshall Airport","end":"undefined","to":"(PHX) Phoenix Sky Harbor International Airport","type":"one-way","class":"economy"},"uid":"MltFTJdfyTdUKA","agent":"Mozilla\/5.0 (Windows NT 10.0; Win64; x64; rv:109.0) Gecko\/20100101 Firefox\/119.0","ip":"172.70.38.81","region":"Virginia","queryID":"6WRyJ2oCjzP7","timestamp":1700076470},{"request":{"infants":"0","children":"0","adults":"1","start":"01\/05\/2024","from":"(SEA) Seattle-Tacoma International Airport","end":"undefined","to":"(SMF) Sacramento International Airport","type":"one-way","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (iPhone; CPU iPhone OS 17_1_1 like Mac OS X) AppleWebKit\/605.1.15 (KHTML, like Gecko) Mobile\/21B91 Instagram 309.1.1.28.108 (iPhone16,1; iOS 17_1_1; en_US; en; scale=3.00; 1179x2556; 537288535)","ip":"172.71.147.56","session_id":"7AbXnTPedpu8","region":"Washington","queryID":"7AbXnTPedpu8","timestamp":1701645635,"selections":[{"offer":"off_0000AcRo2xx9IFPnCzc6Gp","amount":"72.18","discountPresented":"0.30728733721252","direction":"departure"}]},{"request":{"infants":"0","children":"0","adults":"8","start":"12\/26\/2023","from":"(RDU) Raleigh\u2013Durham International Airpor","end":"01\/01\/2024","to":"(LAS) McCarran International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (Linux; Android 12; SM-A125U Build\/SP1A.210812.016; wv) AppleWebKit\/537.36 (KHTML, like Gecko) Version\/4.0 Chrome\/114.0.5735.130 Mobile Safari\/537.36 [FB_IAB\/FB4A;FBAV\/421.0.0.33.47;]","ip":"108.162.237.149","session_id":"7FPAGgaAoXsx","region":"Georgia","queryID":"7FPAGgaAoXsx","timestamp":1701550419},{"request":{"infants":"0","children":"0","adults":"1","start":"12\/31\/2023","from":"(SFO) San Francisco International Airport","end":"01\/07\/2024","to":"(DFW) Dallas\/Fort Worth International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (iPhone; CPU iPhone OS 17_1_1 like Mac OS X) AppleWebKit\/605.1.15 (KHTML, like Gecko) Mobile\/21B91 Instagram 309.1.1.28.108 (iPhone15,3; iOS 17_1_1; en_US; en; scale=3.00; 1290x2796; 537288535)","ip":"172.69.65.107","session_id":"7LBuvf7IDfNL","region":"Texas","queryID":"7LBuvf7IDfNL","timestamp":1701469780},{"request":{"infants":"0","children":"0","adults":"1","start":"01\/01\/2024","from":"(TPA) Tampa International Airport","end":"01\/08\/2024","to":"(CMH) John Glenn Columbus International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (iPhone; CPU iPhone OS 17_1_2 like Mac OS X) AppleWebKit\/605.1.15 (KHTML, like Gecko) Mobile\/21B101 Instagram 309.1.1.28.108 (iPhone14,5; iOS 17_1_2; en_US; en; scale=3.00; 1170x2532; 537288535)","ip":"172.69.71.64","session_id":"7jhFybuvWXtb","region":"Georgia","queryID":"7jhFybuvWXtb","timestamp":1701539651},{"request":{"infants":"0","children":"0","adults":"1","start":"12\/27\/2023","from":"(ORD) O\'Hare International Airport","end":"01\/03\/2024","to":"(CPT) Cape Town International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/119.0.0.0 Safari\/537.36","ip":"172.69.6.77","region":"Illinois","queryID":"7lfCVCzHZw98","timestamp":1701114498},{"request":{"infants":"0","children":"0","adults":"1","start":"01\/01\/2024","from":"(DCA) Ronald Reagan Washington National Airport","end":"undefined","to":"(MIA) Miami International Airport","type":"one-way","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (iPhone; CPU iPhone OS 17_1_2 like Mac OS X) AppleWebKit\/605.1.15 (KHTML, like Gecko) Mobile\/21B101 Instagram 309.1.1.28.108 (iPhone15,4; iOS 17_1_2; en_US; en; scale=3.00; 1179x2556; 537288535)","ip":"162.158.62.179","session_id":"7v2PwAgsao35","region":"New Jersey","queryID":"7v2PwAgsao35","timestamp":1701544121},{"request":{"infants":"0","children":"0","adults":"1","start":"01\/26\/2024","from":"(RDU) Raleigh\u2013Durham International Airpor","end":"01\/29\/2024","to":"(FLL) Fort Lauderdale-Hollywood International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (iPhone; CPU iPhone OS 16_5 like Mac OS X) AppleWebKit\/605.1.15 (KHTML, like Gecko) Version\/16.5 Mobile\/15E148 Safari\/604.1","ip":"172.70.43.4","session_id":"8Eb22gAdJmQk","region":"Virginia","queryID":"8Eb22gAdJmQk","timestamp":1701628291,"selections":[{"offer":"off_0000AcROFTqDxfzAm7gIVa_0","amount":"101.78","discountPresented":"0.75","direction":"departure"},{"offer":"off_0000AcROFTqDxfzAm7gIVa_1","amount":"101.78","discountPresented":"0.75","direction":"return"}]},{"request":{"infants":"0","children":"0","adults":"1","start":"12\/27\/2023","from":"(ORD) O\'Hare International Airport","end":"01\/03\/2024","to":"(LGA) LaGuardia Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/119.0.0.0 Safari\/537.36","ip":"172.70.178.33","region":"Illinois","queryID":"8JQO422WuZyL","timestamp":1701115420},{"request":{"infants":"0","children":"0","adults":"1","start":"04\/23\/2024","from":"(SFO) San Francisco International Airport","end":"04\/30\/2024","to":"(EWR) Newark Liberty International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (iPhone; CPU iPhone OS 17_1_1 like Mac OS X) AppleWebKit\/605.1.15 (KHTML, like Gecko) Mobile\/15E148 [FBAN\/FBIOS;FBAV\/435.0.0.20.109;FBBV\/537085204;FBDV\/iPhone14,2;FBMD\/iPhone;FBSN\/iOS;FBSV\/17.1.1;FBSS\/3;FBCR\/;FBID\/phone;FBLC\/en;FBOP\/80]","ip":"172.69.134.65","session_id":"8T3SVe1KZyiX","region":"California","queryID":"8T3SVe1KZyiX","timestamp":1701219137,"selections":[{"offer":"off_0000AcHZfo5OIc5xSEqpHU_0","amount":"217.80","direction":"departure"},{"offer":"off_0000AcHZfo5OIc5xSEqpHd_1","amount":"217.80","direction":"return"}]},{"request":{"infants":"0","children":"0","adults":"1","start":"02\/29\/2024","from":"(PHX) Phoenix Sky Harbor International Airport","end":"03\/10\/2024","to":"(SEA) Seattle-Tacoma International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (iPhone; CPU iPhone OS 17_1_1 like Mac OS X) AppleWebKit\/605.1.15 (KHTML, like Gecko) Mobile\/21B91 Instagram 309.1.1.28.108 (iPhone12,1; iOS 17_1_1; en_US; en; scale=2.00; 828x1792; 537288535)","ip":"172.70.215.11","session_id":"8b28tR0bjXSL","region":"California","queryID":"8b28tR0bjXSL","timestamp":1701489749},{"request":{"infants":"0","children":"1","adults":"2","start":"12\/22\/2023","from":"(LAX) Los Angeles International Airport","end":"12\/30\/2023","to":"(MCO) Orlando International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit\/605.1.15 (KHTML, like Gecko) Version\/16.6 Safari\/605.1.15","ip":"172.69.33.209","region":"California","queryID":"8cgzF8kVRBGI","timestamp":1700164553},{"request":{"infants":"0","children":"0","adults":"1","start":"12\/24\/2023","from":"(JFK) John F. Kennedy International Airport","end":"12\/31\/2023","to":"(LHR) Heathrow Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (Linux; Android 13; IN2025 Build\/RKQ1.211119.001; wv) AppleWebKit\/537.36 (KHTML, like Gecko) Version\/4.0 Chrome\/119.0.6045.114 Mobile Safari\/537.36 [FB_IAB\/FB4A;FBAV\/442.0.0.32.114;]","ip":"162.158.154.162","region":"New Jersey","queryID":"8jEVQGdiBraK","timestamp":1700802843},{"request":{"infants":"0","children":"0","adults":"1","start":"12\/27\/2023","from":"(SEA) Seattle-Tacoma International Airport","end":"01\/03\/2024","to":"(NEG) Negril Airport","type":"round-trip","class":"business"},"uid":null,"agent":"Mozilla\/5.0 (Linux; Android 10; K) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/119.0.0.0 Mobile Safari\/537.36","ip":"172.71.151.138","region":"Washington","queryID":"8neCtibTwFXK","timestamp":1701131753},{"request":{"infants":"0","children":"0","adults":"1","start":"12\/14\/2023","from":"(EWR) Newark Liberty International Airport","end":"undefined","to":"(MCO) Orlando International Airport","type":"one-way","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (Linux; Android 13; SM-G998U Build\/TP1A.220624.014; wv) AppleWebKit\/537.36 (KHTML, like Gecko) Version\/4.0 Chrome\/119.0.6045.114 Mobile Safari\/537.36 [FB_IAB\/FB4A;FBAV\/442.0.0.44.114;]","ip":"162.158.63.84","session_id":"8p9hXLGMxod7","region":"New Jersey","queryID":"8p9hXLGMxod7","timestamp":1701570365},{"request":{"infants":"0","children":"0","adults":"1","start":"08\/15\/2024","from":"(CAN) Guangzhou Baiyun International Airport","end":"undefined","to":"(SEA) Seattle-Tacoma International Airport","type":"one-way","class":"economy"},"uid":"ATKOjqj7LqWMvW","agent":"Mozilla\/5.0 (Linux; Android 10; K) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/119.0.0.0 Mobile Safari\/537.36","ip":"172.71.150.97","region":"Washington","queryID":"8q1OuPqoUNIy","timestamp":1701108067},{"request":{"infants":"0","children":"0","adults":"1","start":"01\/01\/2024","from":"(LAX) Los Angeles International Airport","end":"01\/08\/2024","to":"(LHR) Heathrow Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (iPhone; CPU iPhone OS 16_7_2 like Mac OS X) AppleWebKit\/605.1.15 (KHTML, like Gecko) Mobile\/20H115 Instagram 309.1.1.28.108 (iPhone14,5; iOS 16_7_2; en_US; en; scale=3.00; 1170x2532; 537288535)","ip":"172.71.26.202","session_id":"8sITYVxsmKDG","region":"Georgia","queryID":"8sITYVxsmKDG","timestamp":1701480531,"selections":[{"offer":"off_0000AcNqTVJ7xMvxa3mTHG_0","amount":"790.00","discountPresented":"0.85","direction":"departure"},{"offer":"off_0000AcNqTVJ7xMvxa3mTHG_1","amount":"790.00","discountPresented":"0.85","direction":"return"}]},{"request":{"infants":"0","children":"0","adults":"1","start":"01\/05\/2024","from":"(SEA) Seattle-Tacoma International Airport","end":"01\/05\/2024","to":"(SMF) Sacramento International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (iPhone; CPU iPhone OS 17_1_1 like Mac OS X) AppleWebKit\/605.1.15 (KHTML, like Gecko) Mobile\/21B91 Instagram 309.1.1.28.108 (iPhone16,1; iOS 17_1_1; en_US; en; scale=3.00; 1179x2556; 537288535)","ip":"172.71.147.56","session_id":"9AcBmgkFG8vc","region":"Washington","queryID":"9AcBmgkFG8vc","timestamp":1701645629},{"request":{"infants":"0","children":"0","adults":"1","start":"12\/29\/2023","from":"(PGV) Pitt-Greenville Airport","end":"12\/31\/2023","to":"(AZA) Phoenix-Mesa-Gateway Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (X11; CrOS x86_64 14541.0.0) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/118.0.0.0 Safari\/537.36","ip":"172.70.175.206","region":"Virginia","queryID":"9lCjsUzgHDAP","timestamp":1700388705},{"request":{"infants":"0","children":"0","adults":"1","start":"02\/29\/2024","from":"(PHX) Phoenix Sky Harbor International Airport","end":"03\/10\/2024","to":"(LHR) Heathrow Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (iPhone; CPU iPhone OS 17_1_1 like Mac OS X) AppleWebKit\/605.1.15 (KHTML, like Gecko) Mobile\/21B91 Instagram 309.1.1.28.108 (iPhone12,1; iOS 17_1_1; en_US; en; scale=2.00; 828x1792; 537288535)","ip":"172.70.215.12","session_id":"A9n3JIKhVNPQ","region":"California","queryID":"A9n3JIKhVNPQ","timestamp":1701489724},{"request":{"infants":"0","children":"0","adults":"1","start":"02\/06\/2024","from":"(ORL) Orlando Executive Airport","end":"undefined","to":"(ATL) Hartsfield-Jackson Atlanta International Airport","type":"one-way","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/118.0.0.0 Safari\/537.36","ip":"172.71.26.197","region":"Georgia","queryID":"AUANFfoRb1VQ","timestamp":1700069304},{"request":{"infants":"0","children":"0","adults":"1","start":"02\/03\/2024","from":"(SEA) Seattle-Tacoma International Airport","end":"undefined","to":"(BOS) General Edward Lawrence Logan International Airport","type":"one-way","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (iPhone; CPU iPhone OS 17_1_2 like Mac OS X) AppleWebKit\/605.1.15 (KHTML, like Gecko) Mobile\/21B101 Instagram 309.1.1.28.108 (iPhone14,2; iOS 17_1_2; en_US; en; scale=3.00; 1170x2532; 537288535)","ip":"162.158.155.15","session_id":"AaSH7sjsSOkj","region":"New Jersey","queryID":"AaSH7sjsSOkj","timestamp":1701721305,"selections":[{"offer":"off_0000AcTcbOoVvqHZhXObxi","amount":"98.90","discountPresented":"0.49443882709808","direction":"departure"}]},{"request":{"infants":"0","children":"0","adults":"1","start":"12\/08\/2023","from":"(SJC) San Jose International Airport","end":"undefined","to":"(LGB) Long Beach Airport","type":"one-way","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (iPhone; CPU iPhone OS 17_1_1 like Mac OS X) AppleWebKit\/605.1.15 (KHTML, like Gecko) Mobile\/21B91 Instagram 309.1.1.28.108 (iPhone16,2; iOS 17_1_1; en_US; en; scale=3.00; 1290x2796; 537288535)","ip":"172.71.222.94","session_id":"AvhG9OsPbDX7","region":"Virginia","queryID":"AvhG9OsPbDX7","timestamp":1701541091,"noResults":true},{"request":{"infants":"0","children":"0","adults":"1","start":"06\/28\/2024","from":"(BNA) Nashville International Airport","end":"undefined","to":"(HAN) Noi Bai International Airport","type":"one-way","class":"premium_economy"},"uid":null,"agent":"Mozilla\/5.0 (iPhone; CPU iPhone OS 17_1_1 like Mac OS X) AppleWebKit\/605.1.15 (KHTML, like Gecko) Mobile\/21B91 Instagram 309.1.1.28.108 (iPhone13,2; iOS 17_1_1; en_US; en; scale=3.00; 1170x2532; 537288535)","ip":"172.68.73.58","session_id":"BC0Rbvo6CUxu","region":"Tennessee","queryID":"BC0Rbvo6CUxu","timestamp":1701573314},{"request":{"infants":"0","children":"0","adults":"1","start":"03\/24\/2024","from":"(PHL) Philadelphia International Airport","end":"undefined","to":"(MIA) Miami International Airport","type":"one-way","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (iPhone; CPU iPhone OS 17_1_1 like Mac OS X) AppleWebKit\/605.1.15 (KHTML, like Gecko) Mobile\/21B91 Instagram 309.1.1.28.108 (iPhone14,3; iOS 17_1_1; es_US; es; scale=3.00; 1284x2778; 537288535)","ip":"172.70.110.46","session_id":"BQLgg61pts4D","region":"New Jersey","queryID":"BQLgg61pts4D","timestamp":1701520291},{"request":{"infants":"0","children":"0","adults":"1","start":"01\/09\/2024","from":"(BWI) Baltimore Washington International Thurgood Marshall Airport","end":"undefined","to":"(ATL) Hartsfield-Jackson Atlanta International Airport","type":"one-way","class":"economy"},"uid":"MltFTJdfyTdUKA","agent":"Mozilla\/5.0 (Windows NT 10.0; Win64; x64; rv:109.0) Gecko\/20100101 Firefox\/119.0","ip":"172.70.135.201","region":"Virginia","queryID":"BRCRnIcONloe","timestamp":1700085007},{"request":{"infants":"0","children":"0","adults":"1","start":"12\/20\/2023","from":"(RDU) Raleigh\u2013Durham International Airpor","end":"12\/27\/2023","to":"(ATL) Hartsfield-Jackson Atlanta International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (iPhone; CPU iPhone OS 17_1_1 like Mac OS X) AppleWebKit\/605.1.15 (KHTML, like Gecko) Mobile\/21B91 [FBAN\/FBIOS;FBAV\/441.0.0.23.105;FBBV\/537255468;FBDV\/iPhone14,3;FBMD\/iPhone;FBSN\/iOS;FBSV\/17.1.1;FBSS\/3;FBID\/phone;FBLC\/en_US;FBOP\/5;FBRV\/538862801]","ip":"108.162.238.87","region":"Georgia","queryID":"BayH9prOVwsD","timestamp":1700461905},{"request":{"infants":"0","children":"0","adults":"1","start":"12\/24\/2023","from":"(SAN) San Diego International Airport","end":"undefined","to":"(TPA) Tampa International Airport","type":"one-way","class":"economy"},"uid":"IgmupcuMnQOaEi","agent":"Mozilla\/5.0 (iPhone; CPU iPhone OS 17_0 like Mac OS X) AppleWebKit\/605.1.15 (KHTML, like Gecko) Version\/17.0 Mobile\/15E148 Safari\/604.1","ip":"172.70.54.234","session_id":"Bq3a8m0UL02R","region":"Florida","queryID":"Bq3a8m0UL02R","timestamp":1701469905,"selections":[{"offer":"off_0000AcNaf4NdJqpb7M7Hun","amount":"348.90","discountPresented":"0.75","direction":"departure"}]},{"request":{"infants":"0","children":"0","adults":"1","start":"01\/01\/2024","from":"(BTV) Burlington International Airport","end":"01\/08\/2024","to":"(DEN) Denver International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (iPhone; CPU iPhone OS 17_1_2 like Mac OS X) AppleWebKit\/605.1.15 (KHTML, like Gecko) Mobile\/21B101 Instagram 309.1.1.28.108 (iPhone14,2; iOS 17_1_2; en_US; en; scale=3.00; 1170x2532; 537288535)","ip":"162.158.159.148","session_id":"C0WkixV5IGmc","region":"New Jersey","queryID":"C0WkixV5IGmc","timestamp":1701561712},{"request":{"infants":"0","children":"0","adults":"1","start":"01\/01\/2024","from":"(EWR) Newark Liberty International Airport","end":"01\/08\/2024","to":"(LHR) Heathrow Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (iPhone; CPU iPhone OS 16_7_2 like Mac OS X) AppleWebKit\/605.1.15 (KHTML, like Gecko) Mobile\/20H115 Instagram 305.0.0.16.118 (iPhone10,1; iOS 16_7_2; en_JP; en; scale=2.00; 640x1136; 526608156)","ip":"172.70.230.148","session_id":"CJwCzrSnPBAe","region":"New Jersey","queryID":"CJwCzrSnPBAe","timestamp":1701563273},{"request":{"infants":"0","children":"0","adults":"1","start":"06\/21\/2024","from":"(JFK) John F. Kennedy International Airport","end":"06\/28\/2024","to":"(HNL) Daniel K. Inouye International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/119.0.0.0 Safari\/537.36","ip":"172.70.110.78","region":"New Jersey","queryID":"CpvukzBFXUDq","timestamp":1700663800},{"request":{"infants":"0","children":"0","adults":"1","start":"04\/19\/2024","from":"(LGA) LaGuardia Airport","end":"04\/26\/2024","to":"(LAX) Los Angeles International Airport","type":"round-trip","class":"economy"},"uid":"Hw4GZ4XMv4VRGx","agent":"Mozilla\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/118.0.0.0 Safari\/537.36 OPR\/104.0.0.0","ip":"162.158.63.135","region":"New Jersey","queryID":"D2e2uclyvhtY","timestamp":1700656561},{"request":{"infants":"0","children":"0","adults":"1","start":"09\/23\/2024","from":"(OAK) Oakland International Airport","end":"09\/30\/2024","to":"(DFW) Dallas\/Fort Worth International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (Linux; Android 13; SM-G986U Build\/TP1A.220624.014; wv) AppleWebKit\/537.36 (KHTML, like Gecko) Version\/4.0 Chrome\/119.0.6045.164 Mobile Safari\/537.36 Instagram 309.1.0.41.113 Android (33\/13; 450dpi; 1080x2192; samsung; SM-G986U; y2q; qcom; en_US; 541635884)","ip":"172.71.158.238","session_id":"DyRc48PjiaAB","region":"California","queryID":"DyRc48PjiaAB","timestamp":1701714942},{"request":{"infants":"0","children":"0","adults":"1","start":"02\/08\/2024","from":"(CLE) Cleveland Hopkins International Airport","end":"02\/11\/2024","to":"(IAH) George Bush Intercontinental Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (iPhone; CPU iPhone OS 16_6_1 like Mac OS X) AppleWebKit\/605.1.15 (KHTML, like Gecko) Mobile\/20G81 [FBAN\/FBIOS;FBAV\/440.0.0.27.105;FBBV\/534883268;FBDV\/iPhone13,1;FBMD\/iPhone;FBSN\/iOS;FBSV\/16.6.1;FBSS\/3;FBID\/phone;FBLC\/en_US;FBOP\/5;FBRV\/537846234]","ip":"172.69.59.2","session_id":"E6tCJ1WOAWHE","region":"Illinois","queryID":"E6tCJ1WOAWHE","timestamp":1701615614,"selections":[{"offer":"off_0000AcR5PBsBiwXcoXW5Du_0","amount":"207.80","discountPresented":"0.75","direction":"departure"},{"offer":"off_0000AcR5PBozunzOee1XFG_1","amount":"262.80","discountPresented":"0.75","direction":"return"}]},{"request":{"infants":"0","children":"0","adults":"1","start":"01\/31\/2024","from":"(RNO) Reno\u2013Tahoe International Airport","end":"undefined","to":"(SBA) Santa Barbara Municipal Airport","type":"one-way","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (iPhone; CPU iPhone OS 16_7_2 like Mac OS X) AppleWebKit\/605.1.15 (KHTML, like Gecko) Version\/16.6 Mobile\/15E148 Safari\/604.1","ip":"172.69.135.9","session_id":"ESghebmpvbgK","region":"California","queryID":"ESghebmpvbgK","timestamp":1701469313},{"request":{"infants":"0","children":"0","adults":"1","start":"11\/24\/2023","from":"(PUQ) Punta Arenas Airport","end":"undefined","to":"(SCL) Comodoro Arturo Merino Ben\u00edtez International Airport","type":"one-way","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (iPhone; CPU iPhone OS 16_6_1 like Mac OS X) AppleWebKit\/605.1.15 (KHTML, like Gecko) Version\/16.6 Mobile\/15E148 Safari\/604.1","ip":"172.69.59.101","region":"Illinois","queryID":"F4x44VsDFeYX","timestamp":1700240799},{"request":{"infants":"0","children":"0","adults":"1","start":"11\/22\/2023","from":"(SCL) Comodoro Arturo Merino Ben\u00edtez International Airport","end":"11\/24\/2023","to":"(IPC) Mataveri International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (iPhone; CPU iPhone OS 16_6_1 like Mac OS X) AppleWebKit\/605.1.15 (KHTML, like Gecko) Version\/16.6 Mobile\/15E148 Safari\/604.1","ip":"162.158.175.96","region":"Texas","queryID":"FArAOAKh2WYB","timestamp":1700184791},{"request":{"infants":"0","children":"0","adults":"1","start":"12\/27\/2023","from":"(HND) Haneda Airport","end":"undefined","to":"(SFO) San Francisco International Airport","type":"one-way","class":"economy"},"uid":"ATKOjqj7LqWMvW","agent":"Mozilla\/5.0 (Linux; Android 10; K) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/119.0.0.0 Mobile Safari\/537.36","ip":"172.71.146.37","region":"Washington","queryID":"FI9inlRBAYzv","timestamp":1701107835},{"request":{"infants":"0","children":"0","adults":"1","start":"03\/25\/2024","from":"(TTN) Trenton-Mercer Airport","end":"03\/27\/2024","to":"(MCO) Orlando International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (iPhone; CPU iPhone OS 16_5_1 like Mac OS X) AppleWebKit\/605.1.15 (KHTML, like Gecko) Mobile\/20F75 Instagram 309.1.1.28.108 (iPhone14,7; iOS 16_5_1; en_US; en; scale=3.00; 1170x2532; 537288535)","ip":"172.70.115.4","session_id":"FYfJ8VhdFSVS","region":"New Jersey","queryID":"FYfJ8VhdFSVS","timestamp":1701470831},{"request":{"infants":"0","children":"0","adults":"8","start":"12\/26\/2023","from":"(RDU) Raleigh\u2013Durham International Airpor","end":"01\/01\/2024","to":"(LAS) McCarran International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (Linux; Android 12; SM-A125U Build\/SP1A.210812.016; wv) AppleWebKit\/537.36 (KHTML, like Gecko) Version\/4.0 Chrome\/114.0.5735.130 Mobile Safari\/537.36 [FB_IAB\/FB4A;FBAV\/421.0.0.33.47;]","ip":"108.162.237.148","session_id":"FzZ97OLZ7L86","region":"Georgia","queryID":"FzZ97OLZ7L86","timestamp":1701550431},{"request":{"infants":"0","children":"0","adults":"1","start":"12\/07\/2023","from":"(SFO) San Francisco International Airport","end":"12\/12\/2023","to":"(CUN) Canc\u00fan International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (iPhone; CPU iPhone OS 17_1_2 like Mac OS X) AppleWebKit\/605.1.15 (KHTML, like Gecko) Version\/17.1.2 Mobile\/15E148 Safari\/604.1","ip":"172.69.135.115","session_id":"GYKYmUqtA5gu","region":"California","queryID":"GYKYmUqtA5gu","timestamp":1701655097},{"request":[],"uid":null,"agent":"Mozilla\/5.0 (compatible; Googlebot\/2.1; +http:\/\/www.google.com\/bot.html)","ip":"108.162.237.35","session_id":"GlT5CdnVAiMn","region":"Georgia","queryID":"GlT5CdnVAiMn","timestamp":1701710543},{"request":{"infants":"0","children":"0","adults":"1","start":"12\/14\/2023","from":"(DSM) Des Moines International Airport","end":"12\/21\/2023","to":"(TOJ) Madrid\u2013Torrej\u00f3n Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (iPhone; CPU iPhone OS 17_1_1 like Mac OS X) AppleWebKit\/605.1.15 (KHTML, like Gecko) Mobile\/21B91 [FBAN\/FBIOS;FBAV\/440.0.0.27.105;FBBV\/534883268;FBDV\/iPhone15,2;FBMD\/iPhone;FBSN\/iOS;FBSV\/17.1.1;FBSS\/3;FBID\/phone;FBLC\/en_US;FBOP\/5;FBRV\/536434821]","ip":"172.70.127.98","region":"Illinois","queryID":"HJNiMVPUMiJZ","timestamp":1700011882},{"request":{"infants":"0","children":"0","adults":"1","start":"12\/31\/2023","from":"(DRO) Durango-La Plata County Airport","end":"01\/07\/2024","to":"(SAT) San Antonio International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (iPhone; CPU iPhone OS 17_1_1 like Mac OS X) AppleWebKit\/605.1.15 (KHTML, like Gecko) Mobile\/21B91 Instagram 309.1.1.28.108 (iPhone14,2; iOS 17_1_1; en_US; en; scale=3.00; 1170x2532; 537288535)","ip":"172.68.34.38","session_id":"HMlvAp5jprRL","region":"Colorado","queryID":"HMlvAp5jprRL","timestamp":1701489812,"selections":[{"offer":"off_0000AcO4GvPI1kn6xjIJav_0","amount":"515.27","discountPresented":"0.85","direction":"departure"},{"offer":"off_0000AcO4GvPe0R4gypSbAO_1","amount":"515.27","discountPresented":"0.85","direction":"return"}]},{"request":{"infants":"0","children":"0","adults":"1","start":"12\/15\/2023","from":"(TPE) Taiwan Taoyuan International Airport","end":"undefined","to":"(SEA) Seattle-Tacoma International Airport","type":"one-way","class":"economy"},"uid":"ATKOjqj7LqWMvW","agent":"Mozilla\/5.0 (Linux; Android 10; K) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/118.0.0.0 Mobile Safari\/537.36","ip":"108.162.245.166","region":"Washington","queryID":"HS6zhjupOkKW","timestamp":1700071891},{"request":{"infants":"0","children":"0","adults":"1","start":"01\/01\/2024","from":"(EWR) Newark Liberty International Airport","end":"01\/08\/2024","to":"(ATL) Hartsfield-Jackson Atlanta International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (iPhone; CPU iPhone OS 16_7_2 like Mac OS X) AppleWebKit\/605.1.15 (KHTML, like Gecko) Mobile\/20H115 Instagram 305.0.0.16.118 (iPhone10,1; iOS 16_7_2; en_JP; en; scale=2.00; 640x1136; 526608156)","ip":"172.70.230.148","session_id":"HgzuaZ4ymxoP","region":"New Jersey","queryID":"HgzuaZ4ymxoP","timestamp":1701563223},{"request":{"infants":"0","children":"0","adults":"1","start":"12\/14\/2023","from":"(SFO) San Francisco International Airport","end":"12\/21\/2023","to":"(BOM) Chhatrapati Shivaji International Airport","type":"round-trip","class":"economy"},"uid":"NRQO5D5HgsWS2j","agent":"Mozilla\/5.0 (iPhone; CPU iPhone OS 17_1_1 like Mac OS X) AppleWebKit\/605.1.15 (KHTML, like Gecko) Version\/17.1.1 Mobile\/15E148 Safari\/604.1","ip":"172.71.158.116","region":"California","queryID":"IOafUoLyewsO","timestamp":1700015726},{"request":{"infants":"0","children":"0","adults":"1","start":"01\/12\/2024","from":"(SFO) San Francisco International Airport","end":"01\/14\/2024","to":"(LAX) Los Angeles International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/119.0.0.0 Safari\/537.36","ip":"172.69.134.233","session_id":"IbXnALzmDaZe","region":"California","queryID":"IbXnALzmDaZe","timestamp":1701156206},{"request":{"infants":"0","children":"1","adults":"2","start":"12\/22\/2023","from":"(LAX) Los Angeles International Airport","end":"12\/30\/2023","to":"(MCO) Orlando International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit\/605.1.15 (KHTML, like Gecko) Version\/16.6 Safari\/605.1.15","ip":"172.69.33.209","region":"California","queryID":"IecuNkQEsx5q","timestamp":1700164535},{"request":{"infants":"0","children":"0","adults":"1","start":"01\/31\/2024","from":"(RNO) Reno\u2013Tahoe International Airport","end":"undefined","to":"(SBA) Santa Barbara Municipal Airport","type":"one-way","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (iPhone; CPU iPhone OS 16_7_2 like Mac OS X) AppleWebKit\/605.1.15 (KHTML, like Gecko) Mobile\/20H115 Instagram 309.1.1.28.108 (iPhone13,2; iOS 16_7_2; en_US; en; scale=3.00; 1170x2532; 537288535)","ip":"172.69.22.181","session_id":"Ir7lkDMFy3Cl","region":"California","queryID":"Ir7lkDMFy3Cl","timestamp":1701469294},{"request":{"infants":"0","children":"0","adults":"1","start":"02\/02\/2024","from":"(HOU) William P. Hobby Airport","end":"02\/04\/2024","to":"(EWR) Newark Liberty International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (iPhone; CPU iPhone OS 17_1_1 like Mac OS X) AppleWebKit\/605.1.15 (KHTML, like Gecko) Mobile\/21B91 Instagram 309.1.1.28.108 (iPhone16,2; iOS 17_1_1; en_US; en; scale=3.00; 1290x2796; 537288535)","ip":"172.71.170.215","session_id":"IrEdqwRPu2Pi","region":"Texas","queryID":"IrEdqwRPu2Pi","timestamp":1701542229,"selections":[{"offer":"off_0000AcPKEmPadxKJwpiKMe_0","amount":"153.78","discountPresented":"0.75","direction":"departure"},{"offer":"off_0000AcPKEmPadxKJwpiKMe_1","amount":"153.78","discountPresented":"0.75","direction":"return"}]},{"request":{"infants":"0","children":"0","adults":"1","start":"01\/03\/2024","from":"(ORD) O\'Hare International Airport","end":"01\/10\/2024","to":"(LGA) LaGuardia Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (iPhone; CPU iPhone OS 16_6_1 like Mac OS X) AppleWebKit\/605.1.15 (KHTML, like Gecko) Mobile\/20G81 Instagram 309.1.1.28.108 (iPhone13,2; iOS 16_6_1; de_US; de; scale=3.00; 1170x2532; 537288535)","ip":"162.158.155.20","session_id":"JR1v79IOlkpk","region":"New Jersey","queryID":"JR1v79IOlkpk","timestamp":1701720552},{"request":{"infants":"0","children":"0","adults":"1","start":"06\/28\/2024","from":"(LAX) Los Angeles International Airport","end":"undefined","to":"(HAN) Noi Bai International Airport","type":"one-way","class":"premium_economy"},"uid":null,"agent":"Mozilla\/5.0 (iPhone; CPU iPhone OS 17_1_1 like Mac OS X) AppleWebKit\/605.1.15 (KHTML, like Gecko) Mobile\/21B91 Instagram 309.1.1.28.108 (iPhone13,2; iOS 17_1_1; en_US; en; scale=3.00; 1170x2532; 537288535)","ip":"172.68.73.58","session_id":"JaZbS8z8qgGs","region":"Tennessee","queryID":"JaZbS8z8qgGs","timestamp":1701573273},{"request":{"infants":"0","children":"0","adults":"1","start":"01\/05\/2024","from":"(LGA) LaGuardia Airport","end":"01\/07\/2024","to":"(BNA) Nashville International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (iPhone; CPU iPhone OS 17_1_1 like Mac OS X) AppleWebKit\/605.1.15 (KHTML, like Gecko) Mobile\/21B91 Instagram 309.1.1.28.108 (iPhone14,5; iOS 17_1_1; en_US; en; scale=3.00; 1170x2532; 537288535)","ip":"172.70.115.95","session_id":"JamWgh3XD1GQ","region":"New Jersey","queryID":"JamWgh3XD1GQ","timestamp":1701553346},{"request":{"infants":"0","children":"0","adults":"1","start":"02\/14\/2024","from":"(IAD) Washington Dulles International Airport","end":"02\/21\/2024","to":"(NRT) Narita International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (Linux; Android 11; Mi 9T Build\/RKQ1.200826.002; wv) AppleWebKit\/537.36 (KHTML, like Gecko) Version\/4.0 Chrome\/119.0.6045.163 Mobile Safari\/537.36 Instagram 310.0.0.23.328 Android (30\/11; 440dpi; 1080x2210; Xiaomi; Mi 9T; davinci; qcom; en_US; 542676978)","ip":"172.70.111.14","session_id":"Jriz2uLISBBb","region":"New Jersey","queryID":"Jriz2uLISBBb","timestamp":1701722643},{"request":{"infants":"0","children":"0","adults":"1","start":"01\/03\/2024","from":"(DEN) Denver International Airport","end":"01\/10\/2024","to":"(DMK) Don Mueang International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (Linux; Android 11; motorola one 5G ace Build\/RZKS31.Q3-45-16-3-11; wv) AppleWebKit\/537.36 (KHTML, like Gecko) Version\/4.0 Chrome\/119.0.6045.114 Mobile Safari\/537.36 [FB_IAB\/FB4A;FBAV\/442.0.0.44.114;]","ip":"172.68.34.61","session_id":"KDPuHZdYOdMB","region":"Colorado","queryID":"KDPuHZdYOdMB","timestamp":1701718566,"noResults":true},{"request":{"infants":"0","children":"0","adults":"1","start":"12\/27\/2023","from":"(SMF) Sacramento International Airport","end":"12\/29\/2023","to":"(SEA) Seattle-Tacoma International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/119.0.0.0 Safari\/537.36","ip":"172.69.140.177","session_id":"KJzW6sMp3Ui6","region":"California","queryID":"KJzW6sMp3Ui6","timestamp":1701560407},{"request":{"infants":"0","children":"0","adults":"1","start":"03\/08\/2024","from":"(ABE) Lehigh Valley International Airport","end":"03\/16\/2024","to":"(PDX) Portland International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (iPhone; CPU iPhone OS 16_6_1 like Mac OS X) AppleWebKit\/605.1.15 (KHTML, like Gecko) Mobile\/20G81 Instagram 309.1.1.28.108 (iPhone13,1; iOS 16_6_1; en_US; en; scale=3.00; 1125x2436; 537288535)","ip":"162.158.158.10","session_id":"KU9phv1rE4BX","region":"New Jersey","queryID":"KU9phv1rE4BX","timestamp":1701718306,"selections":[{"offer":"off_0000AcTY8lCDEjowu6ZLao_0","amount":"463.95","discountPresented":"0.75","direction":"departure"},{"offer":"off_0000AcTY8lCDEjowu6ZLao_1","amount":"463.95","discountPresented":"0.75","direction":"return"}]},{"request":{"infants":"0","children":"0","adults":"1","start":"01\/01\/2024","from":"(BTV) Burlington International Airport","end":"01\/08\/2024","to":"(DEN) Denver International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"facebookexternalhit\/1.1 (+http:\/\/www.facebook.com\/externalhit_uatext.php)","ip":"172.70.43.115","session_id":"KaVLbdg4TIeD","region":"Virginia","queryID":"KaVLbdg4TIeD","timestamp":1701644748},{"request":{"infants":"0","children":"0","adults":"1","start":"01\/02\/2024","from":"(ATL) Hartsfield-Jackson Atlanta International Airport","end":"01\/09\/2024","to":"(LGA) LaGuardia Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (iPhone; CPU iPhone OS 17_1_1 like Mac OS X) AppleWebKit\/605.1.15 (KHTML, like Gecko) Mobile\/21B91 Instagram 309.1.1.28.108 (iPhone11,8; iOS 17_1_1; en_US; en; scale=2.00; 750x1624; 537288535)","ip":"172.71.30.44","session_id":"KcjPUbH316Fi","region":"Georgia","queryID":"KcjPUbH316Fi","timestamp":1701659825},{"request":{"infants":"0","children":"0","adults":"1","start":"02\/09\/2024","from":"(IAD) Washington Dulles International Airport","end":"02\/18\/2024","to":"(PUJ) Punta Cana International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (Linux; Android 10; K) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/119.0.0.0 Mobile Safari\/537.36","ip":"172.70.35.54","region":"Virginia","queryID":"KnbxZyeUSJaQ","timestamp":1701138713},{"request":{"infants":"0","children":"0","adults":"1","start":"12\/02\/2023","from":"(LGB) Long Beach Airport","end":"12\/10\/2023","to":"(LIH) Lihue Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (Linux; Android 10; K) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/119.0.0.0 Mobile Safari\/537.36","ip":"162.158.186.114","region":"California","queryID":"Krvyxrva3qDp","timestamp":1700885110},{"request":{"infants":"0","children":"0","adults":"1","start":"01\/04\/2024","from":"(CPT) Cape Town International Airport","end":"undefined","to":"(HRE) Harare International Airport","type":"one-way","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (Linux; Android 10; K) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/117.0.0.0 Mobile Safari\/537.36","ip":"172.68.186.89","session_id":"Kt48c7LiYyen","region":"Western Cape","queryID":"Kt48c7LiYyen","timestamp":1701346468},{"request":{"infants":"0","children":"0","adults":"1","start":"01\/01\/2024","from":"(LGA) LaGuardia Airport","end":"01\/08\/2024","to":"(DTW) Detroit Metro Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (iPhone; CPU iPhone OS 16_0 like Mac OS X) AppleWebKit\/605.1.15 (KHTML, like Gecko) Mobile\/20A362 Instagram 309.1.1.28.108 (iPhone13,2; iOS 16_0; en_US; en; scale=3.00; 1170x2532; 537288535)","ip":"172.70.110.177","session_id":"KvGlTaW9IrBe","region":"New Jersey","queryID":"KvGlTaW9IrBe","timestamp":1701540785},{"request":{"infants":"0","children":"0","adults":"1","start":"12\/29\/2023","from":"(JFK) John F. Kennedy International Airport","end":"01\/02\/2024","to":"(PHX) Phoenix Sky Harbor International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (Linux; Android 13; Pixel 6 Build\/TQ3A.230901.001.C2; wv) AppleWebKit\/537.36 (KHTML, like Gecko) Version\/4.0 Chrome\/119.0.6045.163 Mobile Safari\/537.36 [FB_IAB\/FB4A;FBAV\/442.0.0.44.114;]","ip":"162.158.159.34","session_id":"L1h2REFoPgXz","region":"New Jersey","queryID":"L1h2REFoPgXz","timestamp":1701487314},{"request":{"infants":"0","children":"0","adults":"1","start":"03\/19\/2024","from":"(BTV) Burlington International Airport","end":"03\/24\/2024","to":"(ATL) Hartsfield-Jackson Atlanta International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (iPhone; CPU iPhone OS 16_2 like Mac OS X) AppleWebKit\/605.1.15 (KHTML, like Gecko) Mobile\/20C65 Instagram 287.0.0.18.74 (iPhone13,2; iOS 16_2; en_US; en; scale=3.00; 1170x2532; 483425622)","ip":"172.70.39.193","session_id":"LS8KqUJJIqpR","region":"Virginia","queryID":"LS8KqUJJIqpR","timestamp":1701579891},{"request":{"infants":"0","children":"0","adults":"1","start":"12\/25\/2023","from":"(ORD) O\'Hare International Airport","end":"12\/30\/2023","to":"(SVV) San Salvador de Paul Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/116.0.0.0 Safari\/537.36","ip":"172.70.126.8","session_id":"M7W9TZhzSufE","region":"Illinois","queryID":"M7W9TZhzSufE","timestamp":1701383406},{"request":{"infants":"0","children":"0","adults":"1","start":"02\/07\/2024","from":"(PHX) Phoenix Sky Harbor International Airport","end":"undefined","to":"(LAS) McCarran International Airport","type":"one-way","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/119.0.0.0 Safari\/537.36","ip":"172.71.222.214","session_id":"M7Wy1xhMBomI","region":"Virginia","queryID":"M7Wy1xhMBomI","timestamp":1701495018},{"request":{"infants":"0","children":"0","adults":"1","start":"01\/02\/2024","from":"(LGA) LaGuardia Airport","end":"01\/09\/2024","to":"(ORD) O\'Hare International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (Linux; Android 11; moto g power (2021) Build\/RZBS31.Q2-143-27-25; wv) AppleWebKit\/537.36 (KHTML, like Gecko) Version\/4.0 Chrome\/119.0.6045.164 Mobile Safari\/537.36 Instagram 309.1.0.41.113 Android (30\/11; 280dpi; 720x1446; motorola; moto g power (2021); borneo; qcom; fr_FR; 541635890)","ip":"162.158.62.106","session_id":"MC1dJjC4eX2p","region":"New Jersey","queryID":"MC1dJjC4eX2p","timestamp":1701583275},{"request":{"infants":"0","children":"0","adults":"1","start":"12\/22\/2023","from":"(SFO) San Francisco International Airport","end":"undefined","to":"(LAX) Los Angeles International Airport","type":"one-way","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (Linux; Android 13; SM-G991U Build\/TP1A.220624.014; wv) AppleWebKit\/537.36 (KHTML, like Gecko) Version\/4.0 Chrome\/119.0.6045.163 Mobile Safari\/537.36 Instagram 309.1.0.41.113 Android (33\/13; 480dpi; 1080x2176; samsung; SM-G991U; o1q; qcom; en_US; 541635892)","ip":"172.69.22.230","session_id":"MF8eHxTKckvl","region":"California","queryID":"MF8eHxTKckvl","timestamp":1701622053},{"request":{"infants":"0","children":"0","adults":"1","start":"12\/26\/2023","from":"(ORD) O\'Hare International Airport","end":"12\/31\/2023","to":"(MEX) Benito Juarez International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/116.0.0.0 Safari\/537.36","ip":"172.70.126.9","session_id":"MWZknpXeD1PD","region":"Illinois","queryID":"MWZknpXeD1PD","timestamp":1701383434},{"request":{"infants":"0","children":"0","adults":"1","start":"01\/16\/2024","from":"(DCA) Ronald Reagan Washington National Airport","end":"01\/23\/2024","to":"(OMA) Eppley Airfield","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (Linux; Android 10; K) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/119.0.0.0 Mobile Safari\/537.36","ip":"172.69.6.193","region":"Illinois","queryID":"MXmAmvSwIYvR","timestamp":1700804416},{"request":{"infants":"0","children":"0","adults":"1","start":"01\/09\/2024","from":"(PHX) Phoenix Sky Harbor International Airport","end":"01\/17\/2024","to":"(LAS) McCarran International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (Linux; Android 10; K) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/119.0.0.0 Mobile Safari\/537.36","ip":"172.69.34.45","session_id":"MbfpPiVedcy9","region":"California","queryID":"MbfpPiVedcy9","timestamp":1701494520},{"request":{"infants":"0","children":"0","adults":"1","start":"12\/21\/2023","from":"(LAX) Los Angeles International Airport","end":"undefined","to":"(LAS) McCarran International Airport","type":"one-way","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (iPhone; CPU iPhone OS 17_1_1 like Mac OS X) AppleWebKit\/605.1.15 (KHTML, like Gecko) Mobile\/21B91 [FBAN\/FBIOS;FBAV\/442.0.0.57.113;FBBV\/541398377;FBDV\/iPhone16,1;FBMD\/iPhone;FBSN\/iOS;FBSV\/17.1.1;FBSS\/3;FBID\/phone;FBLC\/en_US;FBOP\/5;FBRV\/0]","ip":"172.69.33.248","session_id":"N9GLNUOOUzl1","region":"California","queryID":"N9GLNUOOUzl1","timestamp":1701465027,"selections":[{"offer":"off_0000AcNTPFmUSoYiAGFqDB","amount":"43.90","discountPresented":"0.01","direction":"departure"}],"checkoutOffers":[{"offer":"off_0000AcNTPFmUSoYiAGFqDB","discountPresented":0.01,"msrp":"43.90"}]},{"request":{"infants":"0","children":"0","adults":"1","start":"01\/02\/2024","from":"(ORD) O\'Hare International Airport","end":"03\/24\/2024","to":"(YVR) Vancouver International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (iPhone; CPU iPhone OS 17_1_2 like Mac OS X) AppleWebKit\/605.1.15 (KHTML, like Gecko) Mobile\/21B101 Instagram 309.1.1.28.108 (iPhone14,5; iOS 17_1_2; en_US; en; scale=3.00; 1170x2532; 537288535)","ip":"172.70.127.26","session_id":"NK0RSJaYjkup","region":"Illinois","queryID":"NK0RSJaYjkup","timestamp":1701658175},{"request":{"infants":"0","children":"0","adults":"1","start":"01\/02\/2024","from":"(JFK) John F. Kennedy International Airport","end":"01\/09\/2024","to":"(COS) Colorado Springs Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (iPhone; CPU iPhone OS 14_4_2 like Mac OS X) AppleWebKit\/605.1.15 (KHTML, like Gecko) Mobile\/18D70 Instagram 306.0.0.20.118 (iPhone13,2; iOS 14_4_2; en_US; en; scale=3.00; 1170x2532; 529083166)","ip":"162.158.154.20","session_id":"NY7HtTKap6xp","region":"New Jersey","queryID":"NY7HtTKap6xp","timestamp":1701652095},{"request":{"infants":"0","children":"0","adults":"1","start":"12\/12\/2023","from":"(ILM) Wilmington International Airport","end":"12\/15\/2023","to":"(DCA) Ronald Reagan Washington National Airport","type":"round-trip","class":"economy"},"uid":"9JVrq6eW6QQnRf","agent":"Mozilla\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/119.0.0.0 Safari\/537.36 Edg\/119.0.0.0","ip":"172.71.222.254","region":"Virginia","queryID":"NbBvNBTNplP8","timestamp":1700936923},{"request":{"infants":"0","children":"0","adults":"1","start":"01\/05\/2024","from":"(LGA) LaGuardia Airport","end":"01\/07\/2024","to":"(BNA) Nashville International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (iPhone; CPU iPhone OS 17_1_1 like Mac OS X) AppleWebKit\/605.1.15 (KHTML, like Gecko) Mobile\/21B91 Instagram 309.1.1.28.108 (iPhone14,5; iOS 17_1_1; en_US; en; scale=3.00; 1170x2532; 537288535)","ip":"172.70.115.96","session_id":"NhSyXaW5fSmJ","region":"New Jersey","queryID":"NhSyXaW5fSmJ","timestamp":1701553354,"selections":[{"offer":"off_0000AcPamjVQBL5SARb9TS_0","amount":"142.36","discountPresented":"0.75","direction":"departure"},{"offer":"off_0000AcPamjVQBL5SARb9TS_1","amount":"142.36","discountPresented":"0.75","direction":"return"}]},{"request":{"infants":"0","children":"0","adults":"1","start":"01\/01\/2024","from":"(MCO) Orlando International Airport","end":"01\/08\/2024","to":"(LGA) LaGuardia Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (iPhone; CPU iPhone OS 17_2 like Mac OS X) AppleWebKit\/605.1.15 (KHTML, like Gecko) Mobile\/21C5054b Instagram 309.1.1.28.108 (iPhone14,5; iOS 17_2; en_US; en; scale=3.00; 1170x2532; 537288535)","ip":"172.70.55.157","session_id":"Nv4pXDCNgZw9","region":"Florida","queryID":"Nv4pXDCNgZw9","timestamp":1701567910},{"request":{"infants":"0","children":"0","adults":"1","start":"01\/25\/2024","from":"(CVG) Cincinnati\/Northern Kentucky International Airport","end":"02\/07\/2024","to":"(DFW) Dallas\/Fort Worth International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (Linux; Android 13; moto g 5G - 2023 Build\/T1TPN33.58-42; wv) AppleWebKit\/537.36 (KHTML, like Gecko) Version\/4.0 Chrome\/119.0.6045.163 Mobile Safari\/537.36 [FB_IAB\/FB4A;FBAV\/442.0.0.44.114;]","ip":"172.71.254.122","session_id":"NxdqHbgh9Cpf","region":"Illinois","queryID":"NxdqHbgh9Cpf","timestamp":1701545553},{"request":{"infants":"0","children":"0","adults":"1","start":"12\/31\/2023","from":"(ORD) O\'Hare International Airport","end":"01\/07\/2024","to":"(YYZ) Toronto Pearson International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (Linux; Android 14; Pixel 7 Pro Build\/UP1A.231105.003; wv) AppleWebKit\/537.36 (KHTML, like Gecko) Version\/4.0 Chrome\/119.0.6045.163 Mobile Safari\/537.36 Instagram 310.0.0.12.328 Android (34\/14; 420dpi; 1080x2106; Google\/google; Pixel 7 Pro; cheetah; cheetah; en_US; 542365075)","ip":"172.71.255.21","session_id":"OCQbEHTzicTK","region":"Illinois","queryID":"OCQbEHTzicTK","timestamp":1701481808},{"request":{"infants":"0","children":"0","adults":"1","start":"03\/11\/2024","from":"(SAT) San Antonio International Airport","end":"03\/20\/2024","to":"(FCO) Leonardo da Vinci-Fiumicino Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (iPhone; CPU iPhone OS 16_6_1 like Mac OS X) AppleWebKit\/605.1.15 (KHTML, like Gecko) Mobile\/15E148 Instagram 278.0.0.19.115 (iPhone14,3; iOS 16_6_1; en_US; en-US; scale=3.00; 1284x2778; 463736449)","ip":"162.158.158.24","session_id":"OI61H5GcT4fE","region":"New Jersey","queryID":"OI61H5GcT4fE","timestamp":1701513571},{"request":{"infants":"0","children":"0","adults":"1","start":"01\/02\/2024","from":"(LGA) LaGuardia Airport","end":"01\/09\/2024","to":"(AUS) Austin-Bergstrom International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (iPhone; CPU iPhone OS 17_1_1 like Mac OS X) AppleWebKit\/605.1.15 (KHTML, like Gecko) Mobile\/21B91 Instagram 309.1.1.28.108 (iPhone15,4; iOS 17_1_1; en_US; en; scale=3.00; 1179x2556; 537288535)","ip":"162.158.63.73","session_id":"OaW5b6o6J7VY","region":"New Jersey","queryID":"OaW5b6o6J7VY","timestamp":1701643367},{"request":{"infants":"0","children":"0","adults":"1","start":"12\/25\/2023","from":"(TLV) Ben Gurion International Airport","end":"undefined","to":"(BCN) Barcelona\u2013El Prat Josep Tarradellas Airport","type":"one-way","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (Linux; Android 10; K) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/119.0.0.0 Mobile Safari\/537.36","ip":"172.71.135.59","session_id":"OztTFuIMMJdw","region":"\u00cele-de-France","queryID":"OztTFuIMMJdw","timestamp":1701542270,"selections":[{"offer":"off_0000AcPKIgAXRfgWhsDGMQ","amount":"270.62","discountPresented":"0.75","direction":"departure"},{"offer":"off_0000AcPKIgAXRfgWhsDGLg","amount":"230.62","discountPresented":"0.75","direction":"departure"}]},{"request":{"infants":"0","children":"0","adults":"1","start":"12\/14\/2023","from":"(DSM) Des Moines International Airport","end":"12\/21\/2023","to":"(TOJ) Madrid\u2013Torrej\u00f3n Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (iPhone; CPU iPhone OS 17_1_1 like Mac OS X) AppleWebKit\/605.1.15 (KHTML, like Gecko) Mobile\/21B91 [FBAN\/FBIOS;FBAV\/440.0.0.27.105;FBBV\/534883268;FBDV\/iPhone15,2;FBMD\/iPhone;FBSN\/iOS;FBSV\/17.1.1;FBSS\/3;FBID\/phone;FBLC\/en_US;FBOP\/5;FBRV\/536434821]","ip":"172.70.127.98","region":"Illinois","queryID":"PBIwj5hIZeDx","timestamp":1700011875},{"request":{"infants":"0","children":"0","adults":"1","start":"12\/29\/2023","from":"(PHL) Philadelphia International Airport","end":"undefined","to":"(BOS) General Edward Lawrence Logan International Airport","type":"one-way","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (iPhone; CPU iPhone OS 17_0_3 like Mac OS X) AppleWebKit\/605.1.15 (KHTML, like Gecko) Mobile\/21A360 Instagram 309.1.1.28.108 (iPhone16,1; iOS 17_0_3; en_US; en; scale=3.00; 1179x2556; 537288535)","ip":"172.70.111.54","session_id":"PZubTdxZOjMt","region":"New Jersey","queryID":"PZubTdxZOjMt","timestamp":1701479387,"selections":[{"offer":"off_0000AcNolS5JUvzzjQU8sb","amount":"73.90","discountPresented":"0.3234100135318","direction":"departure"}]},{"request":{"infants":"0","children":"0","adults":"1","start":"04\/20\/2024","from":"(LGA) LaGuardia Airport","end":"04\/27\/2024","to":"(LAX) Los Angeles International Airport","type":"round-trip","class":"economy"},"uid":"Hw4GZ4XMv4VRGx","agent":"Mozilla\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/118.0.0.0 Safari\/537.36 OPR\/104.0.0.0","ip":"162.158.154.244","region":"New Jersey","queryID":"Ps6rJMnlEggy","timestamp":1700656491},{"request":{"infants":"0","children":"0","adults":"1","start":"01\/12\/2024","from":"(ORD) O\'Hare International Airport","end":"01\/14\/2024","to":"(YVR) Vancouver International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (iPhone; CPU iPhone OS 17_1_2 like Mac OS X) AppleWebKit\/605.1.15 (KHTML, like Gecko) Mobile\/21B101 Instagram 309.1.1.28.108 (iPhone14,5; iOS 17_1_2; en_US; en; scale=3.00; 1170x2532; 537288535)","ip":"172.70.127.26","session_id":"PtTVOkXXWdZ6","region":"Illinois","queryID":"PtTVOkXXWdZ6","timestamp":1701658196},{"request":{"infants":"0","children":"0","adults":"1","start":"12\/15\/2023","from":"(ATL) Hartsfield-Jackson Atlanta International Airport","end":"undefined","to":"(MAA) Chennai International Airport","type":"one-way","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (Linux; Android 10; K) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/119.0.0.0 Mobile Safari\/537.36","ip":"172.70.174.213","region":"Virginia","queryID":"Q50yqc2DXiMO","timestamp":1700069226},{"request":{"infants":"0","children":"0","adults":"1","start":"01\/09\/2024","from":"(SFB) Orlando Sanford International Airport","end":"01\/16\/2024","to":"(ATL) Hartsfield-Jackson Atlanta International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/119.0.0.0 Safari\/537.36","ip":"141.101.105.152","region":"Vienna","queryID":"QDu1RguXFmbu","timestamp":1700243097},{"request":{"infants":"0","children":"0","adults":"1","start":"01\/01\/2024","from":"(BTV) Burlington International Airport","end":"01\/08\/2024","to":"(DEN) Denver International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"facebookexternalhit\/1.1 (+http:\/\/www.facebook.com\/externalhit_uatext.php)","ip":"172.70.43.96","session_id":"QGX9nBgLVPFs","region":"Virginia","queryID":"QGX9nBgLVPFs","timestamp":1701644748},{"request":{"infants":"0","children":"0","adults":"1","start":"12\/27\/2023","from":"(ORD) O\'Hare International Airport","end":"01\/03\/2024","to":"(CPT) Cape Town International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/119.0.0.0 Safari\/537.36","ip":"172.69.58.62","region":"Illinois","queryID":"QL1N42MhPqe4","timestamp":1701107658},{"request":{"infants":"0","children":"0","adults":"1","start":"01\/01\/2024","from":"(LAS) McCarran International Airport","end":"undefined","to":"(JFK) John F. Kennedy International Airport","type":"one-way","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (Linux; Android 12; SM-G973U Build\/SP1A.210812.016; wv) AppleWebKit\/537.36 (KHTML, like Gecko) Version\/4.0 Chrome\/119.0.6045.163 Mobile Safari\/537.36 Instagram 309.1.0.41.113 Android (31\/12; 420dpi; 1080x2042; samsung; SM-G973U; beyond1q; qcom; en_US; 541635892)","ip":"162.158.186.249","session_id":"QWCYRkZO8n1E","region":"California","queryID":"QWCYRkZO8n1E","timestamp":1701548505},{"request":{"infants":"0","children":"0","adults":"1","start":"12\/31\/2023","from":"(LAS) McCarran International Airport","end":"01\/07\/2024","to":"(LAX) Los Angeles International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (iPhone; CPU iPhone OS 17_1 like Mac OS X) AppleWebKit\/605.1.15 (KHTML, like Gecko) CriOS\/119.0.6045.169 Mobile\/15E148 Safari\/604.1","ip":"162.158.245.47","session_id":"QiJI4GrkrdeS","region":"Nevada","queryID":"QiJI4GrkrdeS","timestamp":1701469791},{"request":{"infants":"0","children":"0","adults":"1","start":"01\/15\/2024","from":"(SJC) San Jose International Airport","end":"01\/22\/2024","to":"(HNL) Daniel K. Inouye International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (iPhone; CPU iPhone OS 16_6_1 like Mac OS X) AppleWebKit\/605.1.15 (KHTML, like Gecko) Mobile\/20G81 [FBAN\/FBIOS;FBAV\/441.0.0.23.105;FBBV\/537255468;FBDV\/iPhone13,2;FBMD\/iPhone;FBSN\/iOS;FBSV\/16.6.1;FBSS\/3;FBID\/phone;FBLC\/en_US;FBOP\/5;FBRV\/539094653]","ip":"172.71.158.251","region":"California","queryID":"QnynZ6g61olp","timestamp":1700547660},{"request":{"infants":"0","children":"0","adults":"1","start":"12\/26\/2023","from":"(ORD) O\'Hare International Airport","end":"12\/31\/2023","to":"(SVV) San Salvador de Paul Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/116.0.0.0 Safari\/537.36","ip":"172.70.126.9","session_id":"Qr7DTcY9MgA1","region":"Illinois","queryID":"Qr7DTcY9MgA1","timestamp":1701383420},{"request":{"infants":"0","children":"0","adults":"1","start":"12\/31\/2023","from":"(ORD) O\'Hare International Airport","end":"01\/07\/2024","to":"(SEA) Seattle-Tacoma International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/119.0.0.0 Safari\/537.36","ip":"172.70.100.57","session_id":"Qt5FeqLpda0H","region":"Illinois","queryID":"Qt5FeqLpda0H","timestamp":1701470337},{"request":{"infants":"0","children":"0","adults":"1","start":"12\/03\/2023","from":"(SEA) Seattle-Tacoma International Airport","end":"12\/07\/2023","to":"(EWR) Newark Liberty International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (Android 14; Mobile; rv:109.0) Gecko\/119.0 Firefox\/119.0","ip":"172.71.146.113","region":"Washington","queryID":"R057KlWCeFI0","timestamp":1700723527},{"request":{"infants":"0","children":"0","adults":"1","start":"06\/21\/2024","from":"(LGA) LaGuardia Airport","end":"06\/28\/2024","to":"(HNL) Daniel K. Inouye International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/119.0.0.0 Safari\/537.36","ip":"172.70.110.78","region":"New Jersey","queryID":"R7rKyWSbhgSZ","timestamp":1700663824},{"request":{"infants":"0","children":"0","adults":"1","start":"01\/03\/2024","from":"(ORD) O\'Hare International Airport","end":"01\/10\/2024","to":"(LGA) LaGuardia Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (iPhone; CPU iPhone OS 16_6_1 like Mac OS X) AppleWebKit\/605.1.15 (KHTML, like Gecko) Mobile\/20G81 Instagram 309.1.1.28.108 (iPhone13,2; iOS 16_6_1; de_US; de; scale=3.00; 1170x2532; 537288535)","ip":"162.158.155.19","session_id":"RCNW3IETMLyl","region":"New Jersey","queryID":"RCNW3IETMLyl","timestamp":1701720402,"selections":[{"offer":"off_0000AcTbGKtPDQ9hVM4na7_0","amount":"192.09","discountPresented":"0.75","direction":"departure"},{"offer":"off_0000AcTbGKtPDQ9hVM4na7_1","amount":"192.09","discountPresented":"0.75","direction":"return"}]},{"request":{"infants":"0","children":"0","adults":"1","start":"06\/18\/2024","from":"(JFK) John F. Kennedy International Airport","end":"06\/25\/2024","to":"(HNL) Daniel K. Inouye International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/119.0.0.0 Safari\/537.36","ip":"172.70.115.56","region":"New Jersey","queryID":"RW90JuvaoHRc","timestamp":1700666996},{"request":{"infants":"0","children":"0","adults":"1","start":"01\/02\/2024","from":"(MCO) Orlando International Airport","end":"01\/09\/2024","to":"(LAS) McCarran International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (iPhone; CPU iPhone OS 16_6_1 like Mac OS X) AppleWebKit\/605.1.15 (KHTML, like Gecko) Mobile\/20G81 [FBAN\/FBIOS;FBDV\/iPhone12,8;FBMD\/iPhone;FBSN\/iOS;FBSV\/16.6.1;FBSS\/2;FBID\/phone;FBLC\/en_US;FBOP\/5]","ip":"172.70.254.86","session_id":"RY9hC0182LM5","region":"Florida","queryID":"RY9hC0182LM5","timestamp":1701612718},{"request":{"infants":"0","children":"0","adults":"1","start":"04\/19\/2024","from":"(JFK) John F. Kennedy International Airport","end":"04\/27\/2024","to":"(MXP) Milan\u2013Malpensa Airport","type":"round-trip","class":"economy"},"uid":"Hw4GZ4XMv4VRGx","agent":"Mozilla\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/118.0.0.0 Safari\/537.36 OPR\/104.0.0.0","ip":"162.158.63.84","region":"New Jersey","queryID":"Ramc8PEiUEoq","timestamp":1700103233},{"request":{"infants":"0","children":"0","adults":"1","start":"04\/19\/2024","from":"(JFK) John F. Kennedy International Airport","end":"undefined","to":"(LAX) Los Angeles International Airport","type":"one-way","class":"economy"},"uid":"Hw4GZ4XMv4VRGx","agent":"Mozilla\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/118.0.0.0 Safari\/537.36 OPR\/104.0.0.0","ip":"162.158.62.213","region":"New Jersey","queryID":"RglehHHeOanR","timestamp":1701055652},{"request":{"infants":"0","children":"0","adults":"1","start":"02\/08\/2024","from":"(LGA) LaGuardia Airport","end":"02\/13\/2024","to":"(DEN) Denver International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (Linux; Android 13; SM-A716U1 Build\/TP1A.220624.014; wv) AppleWebKit\/537.36 (KHTML, like Gecko) Version\/4.0 Chrome\/119.0.6045.193 Mobile Safari\/537.36 Instagram 309.1.0.41.113 Android (33\/13; 420dpi; 1080x2183; samsung; SM-A716U1; a71xq; qcom; en_US; 541635892)","ip":"162.158.155.11","session_id":"RjD2li0AXoEr","region":"New Jersey","queryID":"RjD2li0AXoEr","timestamp":1701579858,"noResults":true},{"request":{"infants":"0","children":"0","adults":"1","start":"01\/02\/2024","from":"(LAX) Los Angeles International Airport","end":"01\/09\/2024","to":"(DFW) Dallas\/Fort Worth International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (iPhone; CPU iPhone OS 16_2 like Mac OS X) AppleWebKit\/605.1.15 (KHTML, like Gecko) Mobile\/20C65 Instagram 309.1.1.28.108 (iPhone14,7; iOS 16_2; en_US; en; scale=3.00; 1170x2532; 537288535)","ip":"172.70.211.42","session_id":"Rk9k39Z8SEAk","region":"California","queryID":"Rk9k39Z8SEAk","timestamp":1701636433},{"request":{"infants":"0","children":"0","adults":"1","start":"12\/19\/2023","from":"(MCI) Kansas City International Airport","end":"undefined","to":"(PHX) Phoenix Sky Harbor International Airport","type":"one-way","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (Linux; Android 14; Pixel 7a Build\/UP1A.231105.003; wv) AppleWebKit\/537.36 (KHTML, like Gecko) Version\/4.0 Chrome\/119.0.6045.163 Mobile Safari\/537.36 [FB_IAB\/FB4A;FBAV\/441.1.0.39.109;]","ip":"172.70.211.57","session_id":"RzrFgGljPcWV","region":"California","queryID":"RzrFgGljPcWV","timestamp":1701234481},{"request":{"infants":"0","children":"0","adults":"1","start":"01\/10\/2024","from":"(BOS) General Edward Lawrence Logan International Airport","end":"undefined","to":"(PHL) Philadelphia International Airport","type":"one-way","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (iPhone; CPU iPhone OS 16_6_1 like Mac OS X) AppleWebKit\/605.1.15 (KHTML, like Gecko) Mobile\/20G81 Instagram 309.1.1.28.108 (iPhone14,5; iOS 16_6_1; en_US; en; scale=3.00; 1170x2532; 537288535)","ip":"172.70.114.19","session_id":"S0VXvv5fYfz5","region":"New Jersey","queryID":"S0VXvv5fYfz5","timestamp":1701553362},{"request":{"infants":"0","children":"0","adults":"1","start":"01\/01\/2024","from":"(OKC) Will Rogers World Airport","end":"01\/08\/2024","to":"(MIA) Miami International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (iPhone; CPU iPhone OS 17_1_2 like Mac OS X) AppleWebKit\/605.1.15 (KHTML, like Gecko) Mobile\/21B101 Instagram 309.1.1.28.108 (iPhone14,2; iOS 17_1_2; en_US; en; scale=3.00; 1170x2532; 537288535)","ip":"172.69.65.107","session_id":"S865pcbGYKg6","region":"Texas","queryID":"S865pcbGYKg6","timestamp":1701553757},{"request":{"infants":"0","children":"0","adults":"1","start":"04\/19\/2024","from":"(EWR) Newark Liberty International Airport","end":"04\/26\/2024","to":"(LAX) Los Angeles International Airport","type":"round-trip","class":"economy"},"uid":"Hw4GZ4XMv4VRGx","agent":"Mozilla\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/118.0.0.0 Safari\/537.36 OPR\/104.0.0.0","ip":"162.158.158.178","region":"New Jersey","queryID":"SHpzWBYZzlE1","timestamp":1700656776},{"request":{"infants":"0","children":"0","adults":"1","start":"05\/19\/2024","from":"(LAX) Los Angeles International Airport","end":"05\/22\/2024","to":"(ACY) Atlantic City International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (iPhone; CPU iPhone OS 17_1_1 like Mac OS X) AppleWebKit\/605.1.15 (KHTML, like Gecko) Mobile\/21B91 Instagram 309.1.1.28.108 (iPhone14,5; iOS 17_1_1; en_US; en; scale=3.00; 1170x2532; 537288535)","ip":"162.158.90.222","session_id":"SJCnc6h83pxH","region":"California","queryID":"SJCnc6h83pxH","timestamp":1701626758,"noResults":true},{"request":{"infants":"0","children":"0","adults":"1","start":"01\/12\/2024","from":"(SFO) San Francisco International Airport","end":"01\/14\/2024","to":"(LAX) Los Angeles International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/119.0.0.0 Safari\/537.36","ip":"172.69.134.86","region":"California","queryID":"STzpGpWqgvIf","timestamp":1701108027},{"request":{"infants":"0","children":"0","adults":"1","start":"01\/16\/2024","from":"(BOS) General Edward Lawrence Logan International Airport","end":"undefined","to":"(IAH) George Bush Intercontinental Airport","type":"one-way","class":"economy"},"uid":"ooivo8C8EBOk42","agent":"Mozilla\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/119.0.0.0 Safari\/537.36 Edg\/119.0.0.0","ip":"172.71.167.222","region":"Texas","queryID":"SppsnHA4SRKN","timestamp":1700168086},{"request":{"infants":"0","children":"0","adults":"1","start":"12\/31\/2023","from":"(SFO) San Francisco International Airport","end":"01\/07\/2024","to":"(BOS) General Edward Lawrence Logan International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (Linux; Android 14; Pixel 8 Pro Build\/UD1A.231105.004; wv) AppleWebKit\/537.36 (KHTML, like Gecko) Version\/4.0 Chrome\/119.0.6045.67 Mobile Safari\/537.36 Instagram 309.0.0.40.113 Android (34\/14; 360dpi; 1008x2077; Google\/google; Pixel 8 Pro; husky; husky; en_US; 536988435)","ip":"172.71.154.35","session_id":"Srm2GfPDZno8","region":"California","queryID":"Srm2GfPDZno8","timestamp":1701467694},{"request":{"infants":"0","children":"0","adults":"1","start":"02\/16\/2024","from":"(MSP) Minneapolis-Saint Paul International Airport","end":"02\/19\/2024","to":"(EWR) Newark Liberty International Airport","type":"round-trip","class":"economy"},"uid":"C5HyJVjfOLfsog","agent":"Mozilla\/5.0 (iPhone; CPU iPhone OS 17_0_3 like Mac OS X) AppleWebKit\/605.1.15 (KHTML, like Gecko) Version\/17.0.1 Mobile\/15E148 Safari\/604.1","ip":"172.69.6.192","region":"Illinois","queryID":"SwzembU1GWSN","timestamp":1700830930},{"request":{"infants":"0","children":"0","adults":"1","start":"01\/09\/2024","from":"(JFK) John F. Kennedy International Airport","end":"01\/16\/2024","to":"(BWI) Baltimore Washington International Thurgood Marshall Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/119.0.0.0 Safari\/537.36","ip":"141.101.105.152","region":"Vienna","queryID":"T50RExPts5mM","timestamp":1700243113},{"request":{"infants":"0","children":"0","adults":"1","start":"02\/20\/2024","from":"(PDX) Portland International Airport","end":"02\/27\/2024","to":"(SJU) Luis Mu\u00f1oz Mar\u00edn International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (Linux; Android 13; SM-S908U1 Build\/TP1A.220624.014; wv) AppleWebKit\/537.36 (KHTML, like Gecko) Version\/4.0 Chrome\/119.0.6045.193 Mobile Safari\/537.36 Instagram 309.1.0.41.113 Android (33\/13; 560dpi; 1440x2988; samsung; SM-S908U1; b0q; qcom; en_US; 541635884)","ip":"172.68.174.235","session_id":"TFIaAYZtw4mu","region":"Oregon","queryID":"TFIaAYZtw4mu","timestamp":1701537858},{"request":{"infants":"0","children":"0","adults":"1","start":"12\/14\/2023","from":"(PHL) Philadelphia International Airport","end":"12\/21\/2023","to":"(CUN) Canc\u00fan International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (Macintosh; Intel Mac OS X 10.15; rv:109.0) Gecko\/20100101 Firefox\/118.0","ip":"162.158.154.251","region":"New Jersey","queryID":"TirunO1krrhb","timestamp":1701112409},{"request":{"infants":"0","children":"0","adults":"1","start":"12\/22\/2023","from":"(BWI) Baltimore Washington International Thurgood Marshall Airport","end":"undefined","to":"(PHX) Phoenix Sky Harbor International Airport","type":"one-way","class":"economy"},"uid":"MltFTJdfyTdUKA","agent":"Mozilla\/5.0 (Windows NT 10.0; Win64; x64; rv:109.0) Gecko\/20100101 Firefox\/119.0","ip":"172.70.42.215","region":"Virginia","queryID":"TjLo5CHxYWpR","timestamp":1700068969},{"request":{"infants":"0","children":"0","adults":"1","start":"01\/09\/2024","from":"(BWI) Baltimore Washington International Thurgood Marshall Airport","end":"undefined","to":"(ATL) Hartsfield-Jackson Atlanta International Airport","type":"one-way","class":"economy"},"uid":"MltFTJdfyTdUKA","agent":"Mozilla\/5.0 (Windows NT 10.0; Win64; x64; rv:109.0) Gecko\/20100101 Firefox\/119.0","ip":"172.70.38.81","region":"Virginia","queryID":"TrME4fNJfQG8","timestamp":1700076527},{"request":{"infants":"0","children":"0","adults":"1","start":"01\/01\/2024","from":"(EWR) Newark Liberty International Airport","end":"undefined","to":"(DEN) Denver International Airport","type":"one-way","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (iPhone; CPU iPhone OS 16_6_1 like Mac OS X) AppleWebKit\/605.1.15 (KHTML, like Gecko) Mobile\/20G81 Instagram 309.1.1.28.108 (iPhone14,2; iOS 16_6_1; en_US; en; scale=3.00; 1170x2532; 537288535)","ip":"172.70.115.96","session_id":"TsXywm7SA2De","region":"New Jersey","queryID":"TsXywm7SA2De","timestamp":1701565745,"selections":[{"offer":"off_0000AcPtDVyzO3cc3v5Fkg","amount":"262.23","discountPresented":"0.75","direction":"departure"}]},{"request":{"infants":"0","children":"0","adults":"1","start":"02\/16\/2024","from":"(JFK) John F. Kennedy International Airport","end":"02\/29\/2024","to":"(HND) Haneda Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit\/605.1.15 (KHTML, like Gecko) Version\/17.1 Safari\/605.1.15","ip":"162.158.158.120","region":"New Jersey","queryID":"U0jJmITy2BsG","timestamp":1700259205},{"request":{"infants":"0","children":"0","adults":"1","start":"04\/11\/2024","from":"(SFO) San Francisco International Airport","end":"04\/14\/2024","to":"(CUN) Canc\u00fan International Airport","type":"round-trip","class":"economy"},"uid":"n4ID8eelAsUiXl","agent":"Mozilla\/5.0 (X11; CrOS x86_64 14541.0.0) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/119.0.0.0 Safari\/537.36","ip":"172.69.22.230","session_id":"U5zd6aGguFJB","region":"California","queryID":"U5zd6aGguFJB","timestamp":1701570585},{"request":{"infants":"0","children":"0","adults":"1","start":"12\/29\/2023","from":"(PHX) Phoenix Sky Harbor International Airport","end":"01\/05\/2024","to":"(JFK) John F. Kennedy International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/119.0.0.0 Safari\/537.36","ip":"172.70.211.57","session_id":"UDRhSreViJEv","region":"California","queryID":"UDRhSreViJEv","timestamp":1701289222},{"request":{"infants":"0","children":"0","adults":"2","start":"01\/13\/2024","from":"(EWR) Newark Liberty International Airport","end":"01\/20\/2024","to":"(MCO) Orlando International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (iPhone; CPU iPhone OS 16_0_3 like Mac OS X) AppleWebKit\/605.1.15 (KHTML, like Gecko) Version\/16.0 Mobile\/15E148 Safari\/604.1","ip":"162.158.154.237","session_id":"UQeStpyrC7p0","region":"New Jersey","queryID":"UQeStpyrC7p0","timestamp":1701549567,"selections":[{"offer":"off_0000AcPV9WBzFPtboOzPnK_0","amount":"216.00","discountPresented":"0.75","direction":"departure"},{"offer":"off_0000AcPV9WFB3YRpyITxjw_1","amount":"216.00","discountPresented":"0.75","direction":"return"}]},{"request":{"infants":"0","children":"0","adults":"1","start":"02\/08\/2024","from":"(CLT) Charlotte Douglas International Airport","end":"undefined","to":"(LAS) McCarran International Airport","type":"one-way","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (Linux; Android 12; SM-G975U Build\/SP1A.210812.016; wv) AppleWebKit\/537.36 (KHTML, like Gecko) Version\/4.0 Chrome\/119.0.6045.163 Mobile Safari\/537.36 Instagram 309.1.0.41.113 Android (31\/12; 420dpi; 1080x2047; samsung; SM-G975U; beyond2q; qcom; en_US; 541635892)","ip":"172.70.42.224","session_id":"UXPjdzPJ6oOu","region":"Virginia","queryID":"UXPjdzPJ6oOu","timestamp":1701656577,"selections":[{"offer":"off_0000AcS4K8XpOYJ4MovMRh","amount":"317.40","discountPresented":"0.75","direction":"departure"}]},{"request":{"infants":"0","children":"0","adults":"1","start":"12\/11\/2023","from":"(MDW) Chicago Midway International Airport","end":"undefined","to":"(PHX) Phoenix Sky Harbor International Airport","type":"one-way","class":"economy"},"uid":"MltFTJdfyTdUKA","agent":"Mozilla\/5.0 (Windows NT 10.0; Win64; x64; rv:109.0) Gecko\/20100101 Firefox\/119.0","ip":"172.70.38.81","region":"Virginia","queryID":"UkNIh8Rv9LZO","timestamp":1700076453},{"request":{"infants":"0","children":"0","adults":"1","start":"12\/14\/2023","from":"(EWR) Newark Liberty International Airport","end":"01\/07\/2024","to":"(MCO) Orlando International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (Linux; Android 13; SM-G998U Build\/TP1A.220624.014; wv) AppleWebKit\/537.36 (KHTML, like Gecko) Version\/4.0 Chrome\/119.0.6045.114 Mobile Safari\/537.36 [FB_IAB\/FB4A;FBAV\/442.0.0.44.114;]","ip":"162.158.158.143","session_id":"UurHif5y0TLL","region":"New Jersey","queryID":"UurHif5y0TLL","timestamp":1701570450,"selections":[{"offer":"off_0000AcQ0DNLNBZZq6chPCJ_0","amount":"143.47","discountPresented":"0.75","direction":"departure"},{"offer":"off_0000AcQ0DNLNBZZq6chPCJ_1","amount":"143.47","discountPresented":"0.75","direction":"return"}]},{"request":{"infants":"0","children":"0","adults":"1","start":"01\/03\/2024","from":"(ATL) Hartsfield-Jackson Atlanta International Airport","end":"01\/10\/2024","to":"(TPA) Tampa International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (iPhone; CPU iPhone OS 15_6_1 like Mac OS X) AppleWebKit\/605.1.15 (KHTML, like Gecko) Mobile\/19G82 Instagram 309.1.1.28.108 (iPhone13,2; iOS 15_6_1; en_US; en; scale=3.00; 1170x2532; 537288535)","ip":"172.71.222.56","session_id":"V5Tm1PbkzU8P","region":"Virginia","queryID":"V5Tm1PbkzU8P","timestamp":1701716653},{"request":{"infants":"0","children":"0","adults":"1","start":"03\/11\/2024","from":"(PHX) Phoenix Sky Harbor International Airport","end":"03\/20\/2024","to":"(FCO) Leonardo da Vinci-Fiumicino Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (iPhone; CPU iPhone OS 16_6_1 like Mac OS X) AppleWebKit\/605.1.15 (KHTML, like Gecko) Mobile\/15E148 Instagram 278.0.0.19.115 (iPhone14,3; iOS 16_6_1; en_US; en-US; scale=3.00; 1284x2778; 463736449)","ip":"162.158.158.24","session_id":"VHVlSst7VYgq","region":"New Jersey","queryID":"VHVlSst7VYgq","timestamp":1701513671,"selections":[{"offer":"off_0000AcOdlnEKxEkC8KtHcc_0","amount":"725.90","discountPresented":"0.85","direction":"departure"},{"offer":"off_0000AcOdlnEKxEkC8KtHcc_1","amount":"725.90","discountPresented":"0.85","direction":"return"}]},{"request":{"infants":"0","children":"0","adults":"1","start":"12\/02\/2023","from":"(LAX) Los Angeles International Airport","end":"12\/10\/2023","to":"(SFO) San Francisco International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (Linux; Android 10; K) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/119.0.0.0 Mobile Safari\/537.36","ip":"162.158.186.114","region":"California","queryID":"VKPOSe1vcjKn","timestamp":1700885181},{"request":{"infants":"0","children":"0","adults":"1","start":"01\/02\/2024","from":"(BOS) General Edward Lawrence Logan International Airport","end":"01\/09\/2024","to":"(JFK) John F. Kennedy International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (iPhone; CPU iPhone OS 17_1_1 like Mac OS X) AppleWebKit\/605.1.15 (KHTML, like Gecko) Mobile\/21B91 Instagram 309.1.1.28.108 (iPhone14,3; iOS 17_1_1; en_US; en; scale=3.00; 1284x2778; 537288535)","ip":"162.158.154.96","session_id":"Va8kEH2Y6LLW","region":"New Jersey","queryID":"Va8kEH2Y6LLW","timestamp":1701614368},{"request":{"infants":"0","children":"0","adults":"1","start":"01\/01\/2024","from":"(ORD) O\'Hare International Airport","end":"undefined","to":"(DFW) Dallas\/Fort Worth International Airport","type":"one-way","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (iPhone; CPU iPhone OS 17_1_1 like Mac OS X) AppleWebKit\/605.1.15 (KHTML, like Gecko) Mobile\/21B91 Instagram 309.1.1.28.108 (iPhone14,5; iOS 17_1_1; en_US; en; scale=3.00; 1170x2532; 537288535)","ip":"172.69.6.36","session_id":"VbQOFbzCwH4U","region":"Illinois","queryID":"VbQOFbzCwH4U","timestamp":1701580469},{"request":{"infants":"0","children":"0","adults":"1","start":"01\/03\/2024","from":"(PHX) Phoenix Sky Harbor International Airport","end":"01\/10\/2024","to":"(LAS) McCarran International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (iPhone; CPU iPhone OS 17_2 like Mac OS X) AppleWebKit\/605.1.15 (KHTML, like Gecko) Mobile\/21C5054b Instagram 309.1.1.28.108 (iPhone16,1; iOS 17_2; en_US; en; scale=3.00; 1179x2556; 537288535)","ip":"172.70.211.125","session_id":"Vpurr3lf6gui","region":"California","queryID":"Vpurr3lf6gui","timestamp":1701716620},{"request":{"infants":"0","children":"0","adults":"1","start":"12\/02\/2023","from":"(LAX) Los Angeles International Airport","end":"12\/10\/2023","to":"(ANC) Ted Stevens Anchorage International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (Linux; Android 10; K) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/119.0.0.0 Mobile Safari\/537.36","ip":"172.69.33.186","region":"California","queryID":"W3Shu2kLkTPf","timestamp":1700885264},{"request":{"infants":"0","children":"0","adults":"1","start":"05\/28\/2024","from":"(OAK) Oakland International Airport","end":"05\/31\/2024","to":"(DFW) Dallas\/Fort Worth International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (Linux; Android 13; SM-G986U Build\/TP1A.220624.014; wv) AppleWebKit\/537.36 (KHTML, like Gecko) Version\/4.0 Chrome\/119.0.6045.164 Mobile Safari\/537.36 Instagram 309.1.0.41.113 Android (33\/13; 450dpi; 1080x2192; samsung; SM-G986U; y2q; qcom; en_US; 541635884)","ip":"162.158.166.227","session_id":"WmS3AzUpbVQq","region":"California","queryID":"WmS3AzUpbVQq","timestamp":1701714984},{"request":{"infants":"0","children":"0","adults":"1","start":"01\/01\/2024","from":"(ATL) Hartsfield-Jackson Atlanta International Airport","end":"01\/08\/2024","to":"(PHL) Philadelphia International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (iPhone; CPU iPhone OS 17_1_1 like Mac OS X) AppleWebKit\/605.1.15 (KHTML, like Gecko) Mobile\/21B91 Instagram 309.1.1.28.108 (iPhone15,2; iOS 17_1_1; en_US; en; scale=3.00; 1179x2556; 537288535)","ip":"172.71.22.81","session_id":"XQMEWe4Zlglt","region":"Georgia","queryID":"XQMEWe4Zlglt","timestamp":1701575986},{"request":{"infants":"0","children":"0","adults":"1","start":"01\/10\/2024","from":"(SFO) San Francisco International Airport","end":"01\/16\/2024","to":"(LAS) McCarran International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (iPhone; CPU iPhone OS 17_1_1 like Mac OS X) AppleWebKit\/605.1.15 (KHTML, like Gecko) Mobile\/15E148 [FBAN\/FBIOS;FBAV\/435.0.0.20.109;FBBV\/537085204;FBDV\/iPhone14,2;FBMD\/iPhone;FBSN\/iOS;FBSV\/17.1.1;FBSS\/3;FBCR\/;FBID\/phone;FBLC\/en;FBOP\/80]","ip":"172.69.134.65","session_id":"XqHTbHNgCdaL","region":"California","queryID":"XqHTbHNgCdaL","timestamp":1701219038},{"request":{"infants":"0","children":"0","adults":"1","start":"01\/02\/2024","from":"(CLE) Cleveland Hopkins International Airport","end":"01\/05\/2024","to":"(NBG) New Orleans NAS JRB\/Alvin Callender Field","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (Linux; Android 12; moto g pure Build\/S3RHS32.20-42-10-14-6-7; wv) AppleWebKit\/537.36 (KHTML, like Gecko) Version\/4.0 Chrome\/119.0.6045.163 Mobile Safari\/537.36 Instagram 309.1.0.41.113 Android (31\/12; 280dpi; 720x1462; motorola; moto g pure; ellis; mt6762; en_US; 541635874)","ip":"172.70.42.174","session_id":"YERXfxq7pAFv","region":"Virginia","queryID":"YERXfxq7pAFv","timestamp":1701607256,"noResults":true},{"request":{"infants":"0","children":"0","adults":"1","start":"01\/11\/2024","from":"(LAS) McCarran International Airport","end":"01\/12\/2024","to":"(SFO) San Francisco International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (Linux; Android 13; BOLD N3 Build\/TP1A.220624.014; wv) AppleWebKit\/537.36 (KHTML, like Gecko) Version\/4.0 Chrome\/119.0.6045.163 Mobile Safari\/537.36 [FB_IAB\/FB4A;FBAV\/442.0.0.44.114;]","ip":"172.70.207.8","session_id":"YH0GGRmcPALD","region":"California","queryID":"YH0GGRmcPALD","timestamp":1701556328},{"request":{"infants":"0","children":"0","adults":"1","start":"12\/02\/2023","from":"(LAX) Los Angeles International Airport","end":"12\/10\/2023","to":"(ANC) Ted Stevens Anchorage International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (Linux; Android 10; K) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/119.0.0.0 Mobile Safari\/537.36","ip":"162.158.186.115","region":"California","queryID":"YSyF0qfYEw8M","timestamp":1700885452},{"request":{"infants":"0","children":"0","adults":"1","start":"01\/02\/2024","from":"(PIT) Pittsburgh International Airport","end":"01\/15\/2024","to":"(SRQ) Sarasota\u2013Bradenton International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (iPhone; CPU iPhone OS 16_6 like Mac OS X) AppleWebKit\/605.1.15 (KHTML, like Gecko) Mobile\/20G75 [FBAN\/FBIOS;FBDV\/iPhone14,7;FBMD\/iPhone;FBSN\/iOS;FBSV\/16.6;FBSS\/3;FBID\/phone;FBLC\/en_US;FBOP\/5]","ip":"172.69.6.213","session_id":"YbmMccK5kdyB","region":"Illinois","queryID":"YbmMccK5kdyB","timestamp":1701470518},{"request":{"infants":"0","children":"0","adults":"1","start":"12\/25\/2023","from":"(TLV) Ben Gurion International Airport","end":"undefined","to":"(TOJ) Madrid\u2013Torrej\u00f3n Airport","type":"one-way","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (Linux; Android 13; SM-S911U Build\/TP1A.220624.014; wv) AppleWebKit\/537.36 (KHTML, like Gecko) Version\/4.0 Chrome\/119.0.6045.164 Mobile Safari\/537.36 Instagram 309.1.0.41.113 Android (33\/13; 480dpi; 1080x2259; samsung; SM-S911U; dm1q; qcom; en_US; 541635892)","ip":"141.101.68.39","session_id":"YmshaoNZIe3B","region":"\u00cele-de-France","queryID":"YmshaoNZIe3B","timestamp":1701541967,"noResults":true},{"request":{"infants":"0","children":"0","adults":"1","start":"05\/26\/2024","from":"(HNL) Daniel K. Inouye International Airport","end":"07\/06\/2024","to":"(HND) Haneda Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/119.0.0.0 Safari\/537.36","ip":"172.70.206.213","region":"California","queryID":"YzkN8uKzqnxy","timestamp":1700074518},{"request":{"infants":"0","children":"0","adults":"1","start":"12\/29\/2023","from":"(RDU) Raleigh\u2013Durham International Airpor","end":"12\/31\/2023","to":"(PHX) Phoenix Sky Harbor International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (X11; CrOS x86_64 14541.0.0) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/118.0.0.0 Safari\/537.36","ip":"172.70.175.206","region":"Virginia","queryID":"ZI3Ozm9ZAXQo","timestamp":1700388744},{"request":{"infants":"0","children":"0","adults":"1","start":"12\/27\/2023","from":"(CAN) Guangzhou Baiyun International Airport","end":"undefined","to":"(SEA) Seattle-Tacoma International Airport","type":"one-way","class":"economy"},"uid":"ATKOjqj7LqWMvW","agent":"Mozilla\/5.0 (Linux; Android 10; K) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/119.0.0.0 Mobile Safari\/537.36","ip":"172.71.150.37","region":"Washington","queryID":"ZIqF8JcwsAIT","timestamp":1701107965},{"request":{"infants":"0","children":"0","adults":"1","start":"12\/31\/2023","from":"(ORD) O\'Hare International Airport","end":"01\/07\/2024","to":"(MEX) Benito Juarez International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (iPhone; CPU iPhone OS 16_3_1 like Mac OS X) AppleWebKit\/605.1.15 (KHTML, like Gecko) Version\/16.3 Mobile\/15E148 Safari\/604.1","ip":"172.70.127.3","session_id":"ZKaJlmvCzw0f","region":"Illinois","queryID":"ZKaJlmvCzw0f","timestamp":1701470343},{"request":{"infants":"0","children":"0","adults":"1","start":"12\/27\/2023","from":"(SNA) John Wayne Airport","end":"12\/31\/2023","to":"(PDX) Portland International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (iPhone; CPU iPhone OS 17_1_2 like Mac OS X) AppleWebKit\/605.1.15 (KHTML, like Gecko) Mobile\/21B101 Instagram 309.1.1.28.108 (iPhone16,1; iOS 17_1_2; en_US; en; scale=3.00; 1179x2556; 537288535)","ip":"162.158.90.152","session_id":"ZOEvtaXVaxHQ","region":"California","queryID":"ZOEvtaXVaxHQ","timestamp":1701587493},{"request":[],"uid":null,"agent":"Mozilla\/5.0 (Linux; Android 6.0.1; Nexus 5X Build\/MMB29P) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/99.0.4844.84 Mobile Safari\/537.36 (compatible; Googlebot\/2.1; +http:\/\/www.google.com\/bot.html)","ip":"172.71.26.183","session_id":"ZioR3qMFnDLa","region":"Georgia","queryID":"ZioR3qMFnDLa","timestamp":1701710543},{"request":{"infants":"0","children":"0","adults":"1","start":"01\/02\/2024","from":"(JFK) John F. Kennedy International Airport","end":"01\/09\/2024","to":"(LAS) McCarran International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (iPhone; CPU iPhone OS 16_3_1 like Mac OS X) AppleWebKit\/605.1.15 (KHTML, like Gecko) Mobile\/20D67 Instagram 309.1.1.28.108 (iPhone12,1; iOS 16_3_1; en_US; en; scale=2.00; 828x1792; 537288535)","ip":"172.70.114.49","session_id":"Zk9AG8HMrHYy","region":"New Jersey","queryID":"Zk9AG8HMrHYy","timestamp":1701622014},{"request":{"infants":"0","children":"0","adults":"1","start":"12\/27\/2023","from":"(IAD) Washington Dulles International Airport","end":"01\/03\/2024","to":"(PUJ) Punta Cana International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (Linux; Android 10; K) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/119.0.0.0 Mobile Safari\/537.36","ip":"172.70.38.172","region":"Virginia","queryID":"a7ZvHdHJP2gD","timestamp":1701138624},{"request":{"infants":"0","children":"0","adults":"1","start":"12\/31\/2023","from":"(LGA) LaGuardia Airport","end":"01\/07\/2024","to":"(SEA) Seattle-Tacoma International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (Linux; Android 13; SM-G981U Build\/TP1A.220624.014; wv) AppleWebKit\/537.36 (KHTML, like Gecko) Version\/4.0 Chrome\/119.0.6045.193 Mobile Safari\/537.36 Instagram 309.1.0.41.113 Android (33\/13; 480dpi; 1080x2178; samsung; SM-G981U; x1q; qcom; en_US; 541635892)","ip":"172.70.178.34","session_id":"aCOuQchfZg0t","region":"Illinois","queryID":"aCOuQchfZg0t","timestamp":1701472253},{"request":{"infants":"0","children":"0","adults":"1","start":"12\/15\/2023","from":"(ATL) Hartsfield-Jackson Atlanta International Airport","end":"undefined","to":"(MAA) Chennai International Airport","type":"one-way","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (Linux; Android 10; K) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/119.0.0.0 Mobile Safari\/537.36","ip":"172.71.222.57","region":"Virginia","queryID":"b6zeDgxAJ0bY","timestamp":1700068541},{"request":{"infants":"0","children":"0","adults":"1","start":"02\/06\/2024","from":"(FLL) Fort Lauderdale-Hollywood International Airport","end":"undefined","to":"(ATL) Hartsfield-Jackson Atlanta International Airport","type":"one-way","class":"economy"},"uid":"MltFTJdfyTdUKA","agent":"Mozilla\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/119.0.0.0 Safari\/537.36","ip":"172.69.34.45","region":"California","queryID":"bFfjefl00V1U","timestamp":1700603483},{"request":[],"uid":null,"agent":"Mozilla\/5.0 (Linux; Android 6.0.1; Nexus 5X Build\/MMB29P) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/119.0.6045.123 Mobile Safari\/537.36 (compatible; Googlebot\/2.1; +http:\/\/www.google.com\/bot.html)","ip":"172.71.22.186","session_id":"bHCkw0YgR5KN","region":"Georgia","queryID":"bHCkw0YgR5KN","timestamp":1701710545},{"request":{"infants":"0","children":"0","adults":"1","start":"01\/12\/2024","from":"(SFO) San Francisco International Airport","end":"01\/14\/2024","to":"(LAX) Los Angeles International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/119.0.0.0 Safari\/537.36","ip":"172.69.23.193","region":"California","queryID":"bXqcHrUdHQ67","timestamp":1700007888},{"request":{"infants":"0","children":"0","adults":"1","start":"04\/05\/2024","from":"(PHX) Phoenix Sky Harbor International Airport","end":"04\/08\/2024","to":"(JFK) John F. Kennedy International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (iPhone; CPU iPhone OS 16_6_1 like Mac OS X) AppleWebKit\/605.1.15 (KHTML, like Gecko) Version\/16.6 Mobile\/15E148 Safari\/604.1","ip":"172.70.206.197","region":"California","queryID":"ba2At3Dhgcuw","timestamp":1700069980},{"request":{"infants":"0","children":"0","adults":"1","start":"01\/16\/2024","from":"(BOS) General Edward Lawrence Logan International Airport","end":"undefined","to":"(IAH) George Bush Intercontinental Airport","type":"one-way","class":"economy"},"uid":"ooivo8C8EBOk42","agent":"Mozilla\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/119.0.0.0 Safari\/537.36 Edg\/119.0.0.0","ip":"172.71.167.222","region":"Texas","queryID":"baZsag4HenZF","timestamp":1700168188},{"request":{"infants":"0","children":"0","adults":"1","start":"01\/02\/2024","from":"(SJC) San Jose International Airport","end":"01\/12\/2024","to":"(OGG) Kahului Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (iPhone; CPU iPhone OS 17_1_1 like Mac OS X) AppleWebKit\/605.1.15 (KHTML, like Gecko) Mobile\/21B91 [FBAN\/FBIOS;FBAV\/439.1.0.36.116;FBBV\/532746663;FBDV\/iPhone12,3;FBMD\/iPhone;FBSN\/iOS;FBSV\/17.1.1;FBSS\/3;FBID\/phone;FBLC\/en_US;FBOP\/5;FBRV\/535993309]","ip":"172.71.158.235","session_id":"c7pC8Fyaawl1","region":"California","queryID":"c7pC8Fyaawl1","timestamp":1701154207,"selections":[{"offer":"off_0000AcG15QHIxZjZ7FkRyz_0","amount":"306.96","direction":"departure"},{"offer":"off_0000AcG15QHIxZjZ7FkRyz_1","amount":"306.96","direction":"return"}]},{"request":{"infants":"0","children":"0","adults":"1","start":"06\/21\/2024","from":"(EWR) Newark Liberty International Airport","end":"06\/28\/2024","to":"(HNL) Daniel K. Inouye International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/119.0.0.0 Safari\/537.36","ip":"172.70.230.15","region":"New Jersey","queryID":"cbxhFAGf1s9C","timestamp":1700663466},{"request":{"infants":"0","children":"0","adults":"1","start":"12\/31\/2023","from":"(MDW) Chicago Midway International Airport","end":"01\/07\/2024","to":"(YYZ) Toronto Pearson International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (Linux; Android 14; Pixel 7 Pro Build\/UP1A.231105.003; wv) AppleWebKit\/537.36 (KHTML, like Gecko) Version\/4.0 Chrome\/119.0.6045.163 Mobile Safari\/537.36 Instagram 310.0.0.12.328 Android (34\/14; 420dpi; 1080x2106; Google\/google; Pixel 7 Pro; cheetah; cheetah; en_US; 542365075)","ip":"172.71.255.21","session_id":"ce43IVETRKjf","region":"Illinois","queryID":"ce43IVETRKjf","timestamp":1701481782},{"request":{"infants":"0","children":"0","adults":"1","start":"06\/28\/2024","from":"(BNA) Nashville International Airport","end":"undefined","to":"(HAN) Noi Bai International Airport","type":"one-way","class":"premium_economy"},"uid":null,"agent":"Mozilla\/5.0 (iPhone; CPU iPhone OS 17_1_1 like Mac OS X) AppleWebKit\/605.1.15 (KHTML, like Gecko) Mobile\/21B91 Instagram 309.1.1.28.108 (iPhone13,2; iOS 17_1_1; en_US; en; scale=3.00; 1170x2532; 537288535)","ip":"172.68.73.58","session_id":"coHgUYMePdnm","region":"Tennessee","queryID":"coHgUYMePdnm","timestamp":1701573244},{"request":{"infants":"0","children":"0","adults":"1","start":"01\/01\/2024","from":"(SFO) San Francisco International Airport","end":"01\/08\/2024","to":"(LAX) Los Angeles International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (iPhone; CPU iPhone OS 17_1_2 like Mac OS X) AppleWebKit\/605.1.15 (KHTML, like Gecko) Mobile\/21B101 Instagram 309.1.1.28.108 (iPhone15,3; iOS 17_1_2; en_US; en; scale=3.00; 1290x2796; 537288535)","ip":"172.71.155.41","session_id":"d2pIbk23VZYF","region":"California","queryID":"d2pIbk23VZYF","timestamp":1701561858},{"request":{"infants":"0","children":"0","adults":"1","start":"06\/28\/2024","from":"(BNA) Nashville International Airport","end":"undefined","to":"(HAN) Noi Bai International Airport","type":"one-way","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (iPhone; CPU iPhone OS 17_1_1 like Mac OS X) AppleWebKit\/605.1.15 (KHTML, like Gecko) Mobile\/21B91 Instagram 309.1.1.28.108 (iPhone13,2; iOS 17_1_1; en_US; en; scale=3.00; 1170x2532; 537288535)","ip":"172.68.73.53","session_id":"dMOoIxUnuVAi","region":"Tennessee","queryID":"dMOoIxUnuVAi","timestamp":1701572520},{"request":{"infants":"0","children":"0","adults":"1","start":"12\/31\/2023","from":"(LAS) McCarran International Airport","end":"01\/07\/2024","to":"(MSY) Louis Armstrong New Orleans International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (iPhone; CPU iPhone OS 17_1 like Mac OS X) AppleWebKit\/605.1.15 (KHTML, like Gecko) CriOS\/119.0.6045.169 Mobile\/15E148 Safari\/604.1","ip":"162.158.245.47","session_id":"dPumkE0ddFxx","region":"Nevada","queryID":"dPumkE0ddFxx","timestamp":1701469819},{"request":{"infants":"0","children":"0","adults":"1","start":"12\/28\/2023","from":"(BWI) Baltimore Washington International Thurgood Marshall Airport","end":"01\/04\/2024","to":"(MIA) Miami International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (iPhone; CPU iPhone OS 17_1_1 like Mac OS X) AppleWebKit\/605.1.15 (KHTML, like Gecko) Version\/17.1.1 Mobile\/15E148 Safari\/604.1","ip":"172.71.223.116","session_id":"dRcvzKoP2qcM","region":"Virginia","queryID":"dRcvzKoP2qcM","timestamp":1701222604},{"request":{"infants":"0","children":"0","adults":"1","start":"01\/09\/2024","from":"(BWI) Baltimore Washington International Thurgood Marshall Airport","end":"undefined","to":"(ATL) Hartsfield-Jackson Atlanta International Airport","type":"one-way","class":"economy"},"uid":"MltFTJdfyTdUKA","agent":"Mozilla\/5.0 (Windows NT 10.0; Win64; x64; rv:109.0) Gecko\/20100101 Firefox\/119.0","ip":"172.70.175.28","region":"Virginia","queryID":"dXl75piXQxrk","timestamp":1700083527},{"request":{"infants":"0","children":"0","adults":"2","start":"12\/21\/2023","from":"(LAS) McCarran International Airport","end":"12\/23\/2023","to":"(PHX) Phoenix Sky Harbor International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (iPhone; CPU iPhone OS 17_1_2 like Mac OS X) AppleWebKit\/605.1.15 (KHTML, like Gecko) Mobile\/21B101 [FBAN\/FBIOS;FBAV\/442.0.0.57.113;FBBV\/541398377;FBDV\/iPhone16,1;FBMD\/iPhone;FBSN\/iOS;FBSV\/17.1.2;FBSS\/3;FBID\/phone;FBLC\/en_US;FBOP\/5;FBRV\/0]","ip":"162.158.245.68","session_id":"e5Q9AdblwSLt","region":"Nevada","queryID":"e5Q9AdblwSLt","timestamp":1701488536,"selections":[{"offer":"off_0000AcO2NQpyrWenQg1Dox_0","amount":"601.60","discountPresented":"0.85","direction":"departure"},{"offer":"off_0000AcO2NQpyrWenQg1Dox_1","amount":"601.60","discountPresented":"0.85","direction":"return"}]},{"request":{"infants":"0","children":"0","adults":"1","start":"05\/04\/2024","from":"(LAX) Los Angeles International Airport","end":"05\/26\/2024","to":"(DEL) Indira Gandhi International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (iPhone; CPU iPhone OS 17_1_1 like Mac OS X) AppleWebKit\/605.1.15 (KHTML, like Gecko) Mobile\/21B91 Instagram 309.1.1.28.108 (iPhone14,7; iOS 17_1_1; en_IN; en; scale=3.00; 1170x2532; 537288535)","ip":"172.68.26.93","session_id":"eC429vTWGrGT","region":"Texas","queryID":"eC429vTWGrGT","timestamp":1701580731},{"request":{"infants":"0","children":"0","adults":"1","start":"12\/08\/2023","from":"(SJC) San Jose International Airport","end":"undefined","to":"(SNA) John Wayne Airport","type":"one-way","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (iPhone; CPU iPhone OS 17_1_1 like Mac OS X) AppleWebKit\/605.1.15 (KHTML, like Gecko) Mobile\/21B91 Instagram 309.1.1.28.108 (iPhone16,2; iOS 17_1_1; en_US; en; scale=3.00; 1290x2796; 537288535)","ip":"172.71.222.94","session_id":"eGB08Pz6a5X7","region":"Virginia","queryID":"eGB08Pz6a5X7","timestamp":1701541057},{"request":{"infants":"0","children":"0","adults":"1","start":"12\/27\/2023","from":"(ORD) O\'Hare International Airport","end":"01\/01\/2024","to":"(MEX) Benito Juarez International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/116.0.0.0 Safari\/537.36","ip":"172.69.58.136","region":"Illinois","queryID":"eMwFIheqTiQ9","timestamp":1701103133},{"request":{"infants":"0","children":"0","adults":"1","start":"12\/31\/2023","from":"(ORD) O\'Hare International Airport","end":"01\/07\/2024","to":"(NBG) New Orleans NAS JRB\/Alvin Callender Field","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (iPhone; CPU iPhone OS 17_1_1 like Mac OS X) AppleWebKit\/605.1.15 (KHTML, like Gecko) Mobile\/21B91 Instagram 308.0.2.18.106 (iPhone14,8; iOS 17_1_1; en_US; en; scale=3.00; 1284x2778; 533874066)","ip":"172.70.127.125","session_id":"eV8EsB58kPI7","region":"Illinois","queryID":"eV8EsB58kPI7","timestamp":1701486869},{"request":{"infants":"0","children":"0","adults":"1","start":"01\/09\/2024","from":"(SFB) Orlando Sanford International Airport","end":"01\/16\/2024","to":"(ATL) Hartsfield-Jackson Atlanta International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/119.0.0.0 Safari\/537.36","ip":"141.101.105.152","region":"Vienna","queryID":"edL2bU9ypLP4","timestamp":1700243087},{"request":{"infants":"0","children":"0","adults":"1","start":"01\/09\/2024","from":"(JFK) John F. Kennedy International Airport","end":"01\/16\/2024","to":"(NRT) Narita International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/119.0.0.0 Safari\/537.36","ip":"141.101.105.21","region":"Vienna","queryID":"f1AE8mjffBLQ","timestamp":1700243169},{"request":{"infants":"0","children":"0","adults":"1","start":"02\/22\/2024","from":"(LGA) LaGuardia Airport","end":"02\/29\/2024","to":"(DEN) Denver International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (iPhone; CPU iPhone OS 17_0_3 like Mac OS X) AppleWebKit\/605.1.15 (KHTML, like Gecko) Mobile\/21A360 Instagram 309.1.1.28.108 (iPhone16,2; iOS 17_0_3; en_US; en; scale=3.00; 1290x2796; 537288535)","ip":"162.158.155.10","session_id":"fK3FbXEfejKy","region":"New Jersey","queryID":"fK3FbXEfejKy","timestamp":1701551493,"noResults":true},{"request":{"infants":"0","children":"0","adults":"1","start":"01\/15\/2024","from":"(MDW) Chicago Midway International Airport","end":"undefined","to":"(PHX) Phoenix Sky Harbor International Airport","type":"one-way","class":"economy"},"uid":"MltFTJdfyTdUKA","agent":"Mozilla\/5.0 (Windows NT 10.0; Win64; x64; rv:109.0) Gecko\/20100101 Firefox\/119.0","ip":"172.70.38.81","region":"Virginia","queryID":"fS0oa6bsQTdO","timestamp":1700076439},{"request":{"infants":"0","children":"0","adults":"1","start":"12\/27\/2023","from":"(CAN) Guangzhou Baiyun International Airport","end":"undefined","to":"(SFO) San Francisco International Airport","type":"one-way","class":"economy"},"uid":"ATKOjqj7LqWMvW","agent":"Mozilla\/5.0 (Linux; Android 10; K) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/119.0.0.0 Mobile Safari\/537.36","ip":"172.71.147.171","region":"Washington","queryID":"fTYxvbwYluqp","timestamp":1701107920},{"request":{"infants":"0","children":"0","adults":"1","start":"01\/16\/2024","from":"(ATL) Hartsfield-Jackson Atlanta International Airport","end":"undefined","to":"(DTW) Detroit Metro Airport","type":"one-way","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (iPhone; CPU iPhone OS 17_1_2 like Mac OS X) AppleWebKit\/605.1.15 (KHTML, like Gecko) Mobile\/21B101 Instagram 309.1.1.28.108 (iPhone16,1; iOS 17_1_2; en_US; en; scale=3.00; 1179x2556; 537288535)","ip":"172.69.71.84","session_id":"fWH3eNyx0T5t","region":"Georgia","queryID":"fWH3eNyx0T5t","timestamp":1701579734,"selections":[{"offer":"off_0000AcQE1W8c3uAhGPyEIX","amount":"19.89","discountPresented":"0.01","direction":"departure"}]},{"request":{"infants":"0","children":"0","adults":"1","start":"12\/24\/2023","from":"(MSP) Minneapolis-Saint Paul International Airport","end":"12\/31\/2023","to":"(EWR) Newark Liberty International Airport","type":"round-trip","class":"economy"},"uid":"C5HyJVjfOLfsog","agent":"Mozilla\/5.0 (iPhone; CPU iPhone OS 17_0_3 like Mac OS X) AppleWebKit\/605.1.15 (KHTML, like Gecko) Version\/17.0.1 Mobile\/15E148 Safari\/604.1","ip":"172.69.6.193","region":"Illinois","queryID":"fkT9AltV8Tff","timestamp":1700830470},{"request":{"infants":"0","children":"0","adults":"1","start":"09\/01\/2024","from":"(SEA) Seattle-Tacoma International Airport","end":"09\/20\/2024","to":"(HYD) Rajiv Gandhi International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/119.0.0.0 Safari\/537.36 Edg\/119.0.0.0","ip":"162.158.87.155","region":"Hesse","queryID":"gSHlnjuyy4Fi","timestamp":1700504252},{"request":{"infants":"0","children":"0","adults":"1","start":"01\/03\/2024","from":"(LGA) LaGuardia Airport","end":"01\/10\/2024","to":"(YQS) St. Thomas Municipal Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (iPhone; CPU iPhone OS 17_1_2 like Mac OS X) AppleWebKit\/605.1.15 (KHTML, like Gecko) Mobile\/21B101 Instagram 300.0.0.15.103 (iPhone13,3; iOS 17_1_2; en_US; en; scale=3.00; 1170x2532; 514327624)","ip":"172.70.39.161","session_id":"gnNHKQLLR4r4","region":"Virginia","queryID":"gnNHKQLLR4r4","timestamp":1701716952,"noResults":true},{"request":{"infants":"0","children":"0","adults":"1","start":"06\/28\/2024","from":"(DFW) Dallas\/Fort Worth International Airport","end":"undefined","to":"(DAD) Da Nang International Airport","type":"one-way","class":"premium_economy"},"uid":null,"agent":"Mozilla\/5.0 (iPhone; CPU iPhone OS 17_1_1 like Mac OS X) AppleWebKit\/605.1.15 (KHTML, like Gecko) Mobile\/21B91 Instagram 309.1.1.28.108 (iPhone13,2; iOS 17_1_1; en_US; en; scale=3.00; 1170x2532; 537288535)","ip":"172.68.73.58","session_id":"gxbCLWGrF6vr","region":"Tennessee","queryID":"gxbCLWGrF6vr","timestamp":1701573327},{"request":{"infants":"0","children":"0","adults":"1","start":"12\/31\/2023","from":"(ORD) O\'Hare International Airport","end":"01\/07\/2024","to":"(SEA) Seattle-Tacoma International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/119.0.0.0 Safari\/537.36","ip":"172.70.127.83","session_id":"h0pXkWPYNxW9","region":"Illinois","queryID":"h0pXkWPYNxW9","timestamp":1701470702},{"request":{"infants":"0","children":"0","adults":"1","start":"01\/01\/2024","from":"(SFO) San Francisco International Airport","end":"01\/08\/2024","to":"(YVR) Vancouver International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (iPhone; CPU iPhone OS 16_3 like Mac OS X) AppleWebKit\/605.1.15 (KHTML, like Gecko) Mobile\/20D47 Instagram 309.1.1.28.108 (iPhone14,2; iOS 16_3; en_CA; en; scale=3.00; 1170x2532; 537288535)","ip":"172.71.158.7","session_id":"hd9MFCCr2YaD","region":"California","queryID":"hd9MFCCr2YaD","timestamp":1701542280},{"request":{"infants":"0","children":"0","adults":"1","start":"01\/02\/2024","from":"(ATL) Hartsfield-Jackson Atlanta International Airport","end":"01\/09\/2024","to":"(MIA) Miami International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (Linux; Android 12; SM-N970U Build\/SP1A.210812.016; wv) AppleWebKit\/537.36 (KHTML, like Gecko) Version\/4.0 Chrome\/119.0.6045.193 Mobile Safari\/537.36 Instagram 309.1.0.41.113 Android (31\/12; 480dpi; 1080x2046; samsung; SM-N970U; d1q; qcom; en_US; 541635892)","ip":"172.69.71.22","session_id":"hf2iqnnjq9xe","region":"Georgia","queryID":"hf2iqnnjq9xe","timestamp":1701658584},{"request":{"infants":"0","children":"0","adults":"1","start":"02\/29\/2024","from":"(PHX) Phoenix Sky Harbor International Airport","end":"03\/10\/2024","to":"(LHR) Heathrow Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (iPhone; CPU iPhone OS 17_1_1 like Mac OS X) AppleWebKit\/605.1.15 (KHTML, like Gecko) Mobile\/21B91 Instagram 309.1.1.28.108 (iPhone12,1; iOS 17_1_1; en_US; en; scale=2.00; 828x1792; 537288535)","ip":"172.70.215.12","session_id":"hmtbgDf3xH0w","region":"California","queryID":"hmtbgDf3xH0w","timestamp":1701489732},{"request":{"infants":"0","children":"0","adults":"1","start":"03\/14\/2024","from":"(PDX) Portland International Airport","end":"03\/18\/2024","to":"(PHX) Phoenix Sky Harbor International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (Linux; Android 14; Pixel 7a Build\/UP1A.231105.003; wv) AppleWebKit\/537.36 (KHTML, like Gecko) Version\/4.0 Chrome\/119.0.6045.67 Mobile Safari\/537.36[FBAN\/EMA;FBLC\/en_US;FBAV\/382.0.0.11.115;]","ip":"172.69.23.70","session_id":"iFWDR4FbtkKY","region":"California","queryID":"iFWDR4FbtkKY","timestamp":1701154974,"selections":[{"offer":"off_0000AcG2EseepJrDG5fTRy_0","amount":"407.80","direction":"departure"},{"offer":"off_0000AcG2EseepJrDG5fTRy_1","amount":"407.80","direction":"return"}]},{"request":{"infants":"0","children":"2","adults":"2","start":"02\/10\/2024","from":"(MSY) Louis Armstrong New Orleans International Airport","end":"02\/18\/2024","to":"(CDG) Paris Charles de Gaulle Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (iPhone; CPU iPhone OS 16_1_1 like Mac OS X) AppleWebKit\/605.1.15 (KHTML, like Gecko) Mobile\/20B101 Instagram 309.1.1.28.108 (iPhone13,2; iOS 16_1_1; en_US; en; scale=3.00; 1170x2532; 537288535)","ip":"172.71.167.215","session_id":"iKmr8dcdFzgr","region":"Texas","queryID":"iKmr8dcdFzgr","timestamp":1701539967,"selections":[{"offer":"off_0000AcPGsVAKPgBILXCng8_0","amount":"2664.12","discountPresented":"0.9","direction":"departure"},{"offer":"off_0000AcPGsVAKPgBILXCng8_1","amount":"2664.12","discountPresented":"0.9","direction":"return"}]},{"request":{"infants":"0","children":"0","adults":"1","start":"12\/19\/2023","from":"(IAD) Washington Dulles International Airport","end":"undefined","to":"(CDG) Paris Charles de Gaulle Airport","type":"one-way","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/119.0.0.0 Safari\/537.36","ip":"127.0.0.1","region":"none","queryID":"iNQwdexKubbm","timestamp":1700418608},{"request":{"infants":"0","children":"0","adults":"1","start":"03\/04\/2024","from":"(JFK) John F. Kennedy International Airport","end":"03\/08\/2024","to":"(YQS) St. Thomas Municipal Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (iPhone; CPU iPhone OS 17_1_2 like Mac OS X) AppleWebKit\/605.1.15 (KHTML, like Gecko) Mobile\/21B101 Instagram 300.0.0.15.103 (iPhone13,3; iOS 17_1_2; en_US; en; scale=3.00; 1170x2532; 514327624)","ip":"172.70.39.161","session_id":"ieP1efHFj14N","region":"Virginia","queryID":"ieP1efHFj14N","timestamp":1701716981,"noResults":true},{"request":{"infants":"0","children":"0","adults":"1","start":"01\/19\/2024","from":"(AUS) Austin-Bergstrom International Airport","end":"undefined","to":"(LAS) McCarran International Airport","type":"one-way","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (iPhone; CPU iPhone OS 17_1_1 like Mac OS X) AppleWebKit\/605.1.15 (KHTML, like Gecko) Mobile\/21B91 Instagram 309.1.1.28.108 (iPhone15,3; iOS 17_1_1; en_US; en; scale=3.00; 1290x2796; 537288535)","ip":"172.69.67.108","session_id":"inrpgVQW89FR","region":"Texas","queryID":"inrpgVQW89FR","timestamp":1701543827},{"request":{"infants":"0","children":"0","adults":"1","start":"01\/11\/2024","from":"(BDL) Bradley International Airport","end":"01\/17\/2024","to":"(MCO) Orlando International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (iPhone; CPU iPhone OS 17_1_1 like Mac OS X) AppleWebKit\/605.1.15 (KHTML, like Gecko) Mobile\/21B91 [FBAN\/FBIOS;FBAV\/441.0.0.23.105;FBBV\/537255468;FBDV\/iPhone14,2;FBMD\/iPhone;FBSN\/iOS;FBSV\/17.1.1;FBSS\/3;FBID\/phone;FBLC\/en_US;FBOP\/5;FBRV\/538966523]","ip":"172.70.111.156","region":"New Jersey","queryID":"ipStVNchqKfk","timestamp":1700525439},{"request":{"infants":"0","children":"0","adults":"1","start":"11\/30\/2023","from":"(ATL) Hartsfield-Jackson Atlanta International Airport","end":"12\/04\/2023","to":"(SJU) Luis Mu\u00f1oz Mar\u00edn International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (iPhone; CPU iPhone OS 16_6 like Mac OS X) AppleWebKit\/605.1.15 (KHTML, like Gecko) Mobile\/20G75 [FBAN\/FBIOS;FBAV\/441.0.0.23.105;FBBV\/537255468;FBDV\/iPhone14,2;FBMD\/iPhone;FBSN\/iOS;FBSV\/16.6;FBSS\/3;FBID\/phone;FBLC\/en_US;FBOP\/5;FBRV\/539748107]","ip":"172.69.71.60","region":"Georgia","queryID":"ixFviIDtkCeE","timestamp":1700717757},{"request":{"infants":"0","children":"0","adults":"1","start":"04\/19\/2024","from":"(JFK) John F. Kennedy International Airport","end":"04\/26\/2024","to":"(AMS) Amsterdam Airport Schiphol","type":"round-trip","class":"economy"},"uid":"Hw4GZ4XMv4VRGx","agent":"Mozilla\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/118.0.0.0 Safari\/537.36 OPR\/104.0.0.0","ip":"162.158.62.16","region":"New Jersey","queryID":"jDAnElZzO1DD","timestamp":1701055526},{"request":{"infants":"0","children":"0","adults":"1","start":"06\/18\/2024","from":"(JFK) John F. Kennedy International Airport","end":"06\/25\/2024","to":"(HNL) Daniel K. Inouye International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/119.0.0.0 Safari\/537.36","ip":"162.158.62.39","region":"New Jersey","queryID":"jXWbG2tEqjAV","timestamp":1700663882},{"request":{"infants":"0","children":"0","adults":"1","start":"01\/03\/2024","from":"(ABE) Lehigh Valley International Airport","end":"01\/10\/2024","to":"(PDX) Portland International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (iPhone; CPU iPhone OS 16_6_1 like Mac OS X) AppleWebKit\/605.1.15 (KHTML, like Gecko) Mobile\/20G81 Instagram 309.1.1.28.108 (iPhone13,1; iOS 16_6_1; en_US; en; scale=3.00; 1125x2436; 537288535)","ip":"162.158.155.114","session_id":"jsTdRWat2YnJ","region":"New Jersey","queryID":"jsTdRWat2YnJ","timestamp":1701718259},{"request":{"infants":"0","children":"0","adults":"1","start":"02\/29\/2024","from":"(PHX) Phoenix Sky Harbor International Airport","end":"03\/10\/2024","to":"(LHR) Heathrow Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (iPhone; CPU iPhone OS 17_1_1 like Mac OS X) AppleWebKit\/605.1.15 (KHTML, like Gecko) Mobile\/21B91 Instagram 309.1.1.28.108 (iPhone12,1; iOS 17_1_1; en_US; en; scale=2.00; 828x1792; 537288535)","ip":"172.70.215.11","session_id":"jyiHcZSPjXe2","region":"California","queryID":"jyiHcZSPjXe2","timestamp":1701489774},{"request":[],"uid":null,"agent":"Mozilla\/5.0 (compatible; Googlebot\/2.1; +http:\/\/www.google.com\/bot.html)","ip":"108.162.237.35","session_id":"k8XIp6PrKRUn","region":"Georgia","queryID":"k8XIp6PrKRUn","timestamp":1701710544},{"request":{"infants":"0","children":"0","adults":"1","start":"02\/04\/2024","from":"(SFO) San Francisco International Airport","end":"02\/11\/2024","to":"(NRT) Narita International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (Linux; Android 13; SM-G991U Build\/TP1A.220624.014; wv) AppleWebKit\/537.36 (KHTML, like Gecko) Version\/4.0 Chrome\/119.0.6045.163 Mobile Safari\/537.36 Instagram 309.1.0.41.113 Android (33\/13; 480dpi; 1080x2176; samsung; SM-G991U; o1q; qcom; en_US; 541635892)","ip":"172.71.154.117","session_id":"kWeuVSyXBnu5","region":"California","queryID":"kWeuVSyXBnu5","timestamp":1701658363},{"request":{"infants":"0","children":"0","adults":"1","start":"02\/03\/2024","from":"(LGB) Long Beach Airport","end":"02\/11\/2024","to":"(LIH) Lihue Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (Linux; Android 10; K) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/119.0.0.0 Mobile Safari\/537.36","ip":"172.70.211.14","region":"California","queryID":"kxgQCcy3Foc8","timestamp":1700884394},{"request":{"infants":"0","children":"0","adults":"1","start":"01\/02\/2024","from":"(CHS) Charleston International Airport","end":"01\/09\/2024","to":"(SLC) Salt Lake City International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (iPhone; CPU iPhone OS 17_1_2 like Mac OS X) AppleWebKit\/605.1.15 (KHTML, like Gecko) Mobile\/21B101 Instagram 309.1.1.28.108 (iPhone14,3; iOS 17_1_2; en_US; en; scale=3.00; 1284x2778; 537288535)","ip":"172.69.70.242","session_id":"l6tG2eHiozMk","region":"Georgia","queryID":"l6tG2eHiozMk","timestamp":1701605298},{"request":{"infants":"0","children":"0","adults":"1","start":"01\/01\/2024","from":"(EWR) Newark Liberty International Airport","end":"01\/08\/2024","to":"(PHX) Phoenix Sky Harbor International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (iPhone; CPU iPhone OS 17_2 like Mac OS X) AppleWebKit\/605.1.15 (KHTML, like Gecko) Mobile\/21C5054b Instagram 309.1.1.28.108 (iPhone14,2; iOS 17_2; en_US; en; scale=3.00; 1170x2532; 537288535)","ip":"162.158.155.38","session_id":"lNb6yyWMPkZ7","region":"New Jersey","queryID":"lNb6yyWMPkZ7","timestamp":1701524664,"selections":[{"offer":"off_0000AcOu7eaQOIzjnyP32H_0","amount":"247.80","discountPresented":"0.75","direction":"departure"},{"offer":"off_0000AcOu7eaQOIzjnyP32H_1","amount":"247.80","discountPresented":"0.75","direction":"return"}]},{"request":{"infants":"0","children":"0","adults":"1","start":"12\/12\/2023","from":"(LAX) Los Angeles International Airport","end":"12\/15\/2023","to":"(JFK) John F. Kennedy International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (iPhone; CPU iPhone OS 16_6_1 like Mac OS X) AppleWebKit\/605.1.15 (KHTML, like Gecko) Mobile\/20G81 Instagram 300.0.0.15.103 (iPhone11,8; iOS 16_6_1; en_US; en; scale=2.00; 750x1624; 514327624)","ip":"172.70.214.165","session_id":"lajjOaNU5SRy","region":"California","queryID":"lajjOaNU5SRy","timestamp":1701464254},{"request":{"infants":"0","children":"0","adults":"1","start":"04\/13\/2024","from":"(LAX) Los Angeles International Airport","end":"04\/20\/2024","to":"(HND) Haneda Airport","type":"round-trip","class":"economy"},"uid":"XvyZU2r3xKdafL","agent":"Mozilla\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/119.0.0.0 Safari\/537.36 Edg\/119.0.0.0","ip":"162.158.187.38","region":"California","queryID":"lfr8kZtHQdtg","timestamp":1700069300},{"request":{"infants":"0","children":"0","adults":"1","start":"12\/31\/2023","from":"(DCA) Ronald Reagan Washington National Airport","end":"01\/07\/2024","to":"(MCO) Orlando International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (iPhone; CPU iPhone OS 17_1_2 like Mac OS X) AppleWebKit\/605.1.15 (KHTML, like Gecko) Mobile\/21B101 Instagram 309.1.1.28.108 (iPhone13,2; iOS 17_1_2; en_US; en; scale=3.00; 1170x2532; 537288535)","ip":"172.70.42.89","session_id":"lvdsYUHsHvuW","region":"Virginia","queryID":"lvdsYUHsHvuW","timestamp":1701483206},{"request":{"infants":"0","children":"0","adults":"1","start":"07\/27\/2024","from":"(LAX) Los Angeles International Airport","end":"08\/03\/2024","to":"(HND) Haneda Airport","type":"round-trip","class":"economy"},"uid":"ONfSXiBmiWTlti","agent":"Mozilla\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/116.0.0.0 Safari\/537.36","ip":"172.69.135.140","region":"California","queryID":"m37YBm552Vyo","timestamp":1700531829},{"request":{"infants":"0","children":"0","adults":"1","start":"02\/11\/2024","from":"(LAX) Los Angeles International Airport","end":"02\/22\/2024","to":"(NRT) Narita International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (Linux; Android 14; Pixel 6a Build\/UP1A.231105.003; wv) AppleWebKit\/537.36 (KHTML, like Gecko) Version\/4.0 Chrome\/119.0.6045.66 Mobile Safari\/537.36 Instagram 309.0.0.40.113 Android (34\/14; 460dpi; 1080x2130; Google\/google; Pixel 6a; bluejay; bluejay; en_US; 536988435)","ip":"172.70.210.204","session_id":"mf0l1zYhPiWV","region":"California","queryID":"mf0l1zYhPiWV","timestamp":1701235491},{"request":{"infants":"0","children":"0","adults":"1","start":"01\/01\/2024","from":"(LGA) LaGuardia Airport","end":"undefined","to":"(ORD) O\'Hare International Airport","type":"one-way","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (iPhone; CPU iPhone OS 17_1_1 like Mac OS X) AppleWebKit\/605.1.15 (KHTML, like Gecko) Mobile\/21B91 Instagram 309.1.1.28.108 (iPhone14,5; iOS 17_1_1; en_US; en; scale=3.00; 1170x2532; 537288535)","ip":"162.158.159.64","session_id":"mgnOrY9z7wEq","region":"New Jersey","queryID":"mgnOrY9z7wEq","timestamp":1701564229},{"request":{"infants":"0","children":"0","adults":"1","start":"04\/10\/2024","from":"(SFO) San Francisco International Airport","end":"undefined","to":"(CUN) Canc\u00fan International Airport","type":"one-way","class":"economy"},"uid":"n4ID8eelAsUiXl","agent":"Mozilla\/5.0 (X11; CrOS x86_64 14541.0.0) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/119.0.0.0 Safari\/537.36","ip":"172.69.135.93","session_id":"n6sVI2w9yES6","region":"California","queryID":"n6sVI2w9yES6","timestamp":1701570910,"selections":[{"offer":"off_0000AcQ0ttKTfszUJPHxF8","amount":"207.02","discountPresented":"0.75","direction":"departure"}]},{"request":{"infants":"0","children":"0","adults":"1","start":"01\/25\/2024","from":"(CVG) Cincinnati\/Northern Kentucky International Airport","end":"02\/07\/2024","to":"(DFW) Dallas\/Fort Worth International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (Linux; Android 13; moto g 5G - 2023 Build\/T1TPN33.58-42; wv) AppleWebKit\/537.36 (KHTML, like Gecko) Version\/4.0 Chrome\/119.0.6045.163 Mobile Safari\/537.36 [FB_IAB\/FB4A;FBAV\/442.0.0.44.114;]","ip":"172.70.130.56","session_id":"nQhGGLuA8wYW","region":"Illinois","queryID":"nQhGGLuA8wYW","timestamp":1701545460},{"request":{"infants":"0","children":"0","adults":"1","start":"06\/03\/2024","from":"(MCO) Orlando International Airport","end":"06\/11\/2024","to":"(LGA) LaGuardia Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (iPhone; CPU iPhone OS 17_2 like Mac OS X) AppleWebKit\/605.1.15 (KHTML, like Gecko) Mobile\/21C5046c Instagram 309.1.1.28.108 (iPhone13,3; iOS 17_2; en_US; en; scale=3.00; 1170x2532; 537288535)","ip":"108.162.210.197","session_id":"nzFyfNMUWCwP","region":"Florida","queryID":"nzFyfNMUWCwP","timestamp":1701720863,"selections":[{"offer":"off_0000AcTbwqqMoRzQlGvanx_0","amount":"217.80","discountPresented":"0.75","direction":"departure"}]},{"request":{"infants":"0","children":"0","adults":"2","start":"06\/15\/2024","from":"(MCI) Kansas City International Airport","end":"07\/04\/2024","to":"(JFK) John F. Kennedy International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (iPhone; CPU iPhone OS 17_1_1 like Mac OS X) AppleWebKit\/605.1.15 (KHTML, like Gecko) Mobile\/21B91 Instagram 306.0.0.20.118 (iPhone14,5; iOS 17_1_1; en_MX; en; scale=3.00; 1170x2532; 529083166)","ip":"172.70.131.54","session_id":"o15IDycNcXIB","region":"Illinois","queryID":"o15IDycNcXIB","timestamp":1701525834},{"request":{"infants":"0","children":"0","adults":"1","start":"01\/02\/2024","from":"(ATL) Hartsfield-Jackson Atlanta International Airport","end":"01\/04\/2024","to":"(BWI) Baltimore Washington International Thurgood Marshall Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (Linux; Android 12; SM-G973U Build\/SP1A.210812.016; wv) AppleWebKit\/537.36 (KHTML, like Gecko) Version\/4.0 Chrome\/119.0.6045.193 Mobile Safari\/537.36 Instagram 309.1.0.41.113 Android (31\/12; 280dpi; 720x1361; samsung; SM-G973U; beyond1q; qcom; en_US; 541635892)","ip":"172.69.70.243","session_id":"o7Nv0MfB3rHJ","region":"Georgia","queryID":"o7Nv0MfB3rHJ","timestamp":1701646158},{"request":{"infants":"0","children":"0","adults":"1","start":"12\/30\/2023","from":"(ORD) O\'Hare International Airport","end":"01\/06\/2024","to":"(MDW) Chicago Midway International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/119.0.0.0 Safari\/537.36","ip":"172.69.7.11","session_id":"obebFyZA0Tb5","region":"Illinois","queryID":"obebFyZA0Tb5","timestamp":1701383392},{"request":{"infants":"0","children":"0","adults":"1","start":"12\/27\/2023","from":"(LGB) Long Beach Airport","end":"12\/31\/2023","to":"(PDX) Portland International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (iPhone; CPU iPhone OS 17_1_2 like Mac OS X) AppleWebKit\/605.1.15 (KHTML, like Gecko) Mobile\/21B101 Instagram 309.1.1.28.108 (iPhone16,1; iOS 17_1_2; en_US; en; scale=3.00; 1179x2556; 537288535)","ip":"162.158.90.152","session_id":"ohpUZgizYXS4","region":"California","queryID":"ohpUZgizYXS4","timestamp":1701587321,"noResults":true},{"request":{"infants":"0","children":"0","adults":"1","start":"02\/20\/2024","from":"(GRR) Gerald R. Ford International Airport","end":"02\/26\/2024","to":"(FAT) Fresno Yosemite International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (Linux; Android 13; SM-G781U Build\/TP1A.220624.014; wv) AppleWebKit\/537.36 (KHTML, like Gecko) Version\/4.0 Chrome\/119.0.6045.66 Mobile Safari\/537.36 [FB_IAB\/FB4A;FBAV\/440.0.0.31.105;]","ip":"172.70.130.109","region":"Illinois","queryID":"otHinf8bk0EN","timestamp":1700175825},{"request":{"infants":"0","children":"0","adults":"1","start":"12\/25\/2023","from":"(TLV) Ben Gurion International Airport","end":"undefined","to":"(BCN) Barcelona\u2013El Prat Josep Tarradellas Airport","type":"one-way","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (Linux; Android 13; SM-S911U Build\/TP1A.220624.014; wv) AppleWebKit\/537.36 (KHTML, like Gecko) Version\/4.0 Chrome\/119.0.6045.164 Mobile Safari\/537.36 Instagram 309.1.0.41.113 Android (33\/13; 480dpi; 1080x2259; samsung; SM-S911U; dm1q; qcom; en_US; 541635892)","ip":"172.69.150.63","session_id":"pVW7j0FUjiBd","region":"Hesse","queryID":"pVW7j0FUjiBd","timestamp":1701542087,"selections":[{"offer":"off_0000AcPK1e0lC6XeFWmV1y","amount":"270.62","discountPresented":"0.75","direction":"departure"}]},{"request":{"infants":"0","children":"0","adults":"1","start":"12\/31\/2023","from":"(SAN) San Diego International Airport","end":"01\/07\/2024","to":"(BKK) Suvarnabhumi Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (iPad; CPU OS 17_1_2 like Mac OS X) AppleWebKit\/605.1.15 (KHTML, like Gecko) Mobile\/21B101 [FBAN\/FBIOS;FBAV\/442.0.0.57.113;FBBV\/541398377;FBDV\/iPad11,3;FBMD\/iPad;FBSN\/iPadOS;FBSV\/17.1.2;FBSS\/2;FBID\/tablet;FBLC\/en_US;FBOP\/5;FBRV\/542595843]","ip":"172.70.211.35","session_id":"pWUBYCswaGfA","region":"California","queryID":"pWUBYCswaGfA","timestamp":1701487941},{"request":{"infants":"0","children":"0","adults":"1","start":"02\/24\/2024","from":"(SFO) San Francisco International Airport","end":"02\/28\/2024","to":"(SAT) San Antonio International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (iPhone; CPU iPhone OS 17_1_1 like Mac OS X) AppleWebKit\/605.1.15 (KHTML, like Gecko) Mobile\/21B91 Instagram 309.1.1.28.108 (iPhone15,3; iOS 17_1_1; en_US; en; scale=3.00; 1290x2796; 537288535)","ip":"108.162.216.8","session_id":"paVV2SrMuZvM","region":"Illinois","queryID":"paVV2SrMuZvM","timestamp":1701550130},{"request":{"infants":"0","children":"0","adults":"1","start":"02\/16\/2024","from":"(LTX) Cotopaxi International Airport","end":"02\/19\/2024","to":"(LGB) Long Beach Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (iPhone; CPU iPhone OS 15_4_1 like Mac OS X) AppleWebKit\/605.1.15 (KHTML, like Gecko) Mobile\/19E258 Instagram 309.1.1.28.108 (iPhone14,2; iOS 15_4_1; en_US; en; scale=3.00; 1170x2532; 537288535)","ip":"172.69.33.225","session_id":"pkXJoc76MDCc","region":"California","queryID":"pkXJoc76MDCc","timestamp":1701619277,"noResults":true},{"request":{"infants":"0","children":"0","adults":"1","start":"12\/29\/2023","from":"(JFK) John F. Kennedy International Airport","end":"01\/02\/2024","to":"(PHX) Phoenix Sky Harbor International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (Linux; Android 13; Pixel 6 Build\/TQ3A.230901.001.C2; wv) AppleWebKit\/537.36 (KHTML, like Gecko) Version\/4.0 Chrome\/119.0.6045.163 Mobile Safari\/537.36 [FB_IAB\/FB4A;FBAV\/442.0.0.44.114;]","ip":"162.158.159.34","session_id":"pmafchPU5vWS","region":"New Jersey","queryID":"pmafchPU5vWS","timestamp":1701487329},{"request":{"infants":"0","children":"0","adults":"1","start":"12\/27\/2023","from":"(LAX) Los Angeles International Airport","end":"01\/01\/2024","to":"(PUR) Puerto Rico Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (iPhone; CPU iPhone OS 17_1_1 like Mac OS X) AppleWebKit\/605.1.15 (KHTML, like Gecko) Mobile\/21B91 Instagram 309.1.1.28.108 (iPhone14,7; iOS 17_1_1; en_IN; en; scale=3.00; 1170x2532; 537288535)","ip":"172.68.26.93","session_id":"q0PfKqO8x06R","region":"Texas","queryID":"q0PfKqO8x06R","timestamp":1701580672,"noResults":true},{"request":{"infants":"0","children":"0","adults":"1","start":"12\/30\/2023","from":"(ORD) O\'Hare International Airport","end":"01\/06\/2024","to":"(MEX) Benito Juarez International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/119.0.0.0 Safari\/537.36","ip":"172.69.7.11","session_id":"qNOJODu32pEp","region":"Illinois","queryID":"qNOJODu32pEp","timestamp":1701383416},{"request":{"infants":"0","children":"0","adults":"1","start":"03\/04\/2024","from":"(LGA) LaGuardia Airport","end":"03\/08\/2024","to":"(YQS) St. Thomas Municipal Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (iPhone; CPU iPhone OS 17_1_2 like Mac OS X) AppleWebKit\/605.1.15 (KHTML, like Gecko) Mobile\/21B101 Instagram 300.0.0.15.103 (iPhone13,3; iOS 17_1_2; en_US; en; scale=3.00; 1170x2532; 514327624)","ip":"172.70.39.161","session_id":"r0I4JWzyXZh5","region":"Virginia","queryID":"r0I4JWzyXZh5","timestamp":1701716968,"noResults":true},{"request":{"infants":"0","children":"0","adults":"1","start":"03\/29\/2024","from":"(PHX) Phoenix Sky Harbor International Airport","end":"04\/01\/2024","to":"(JFK) John F. Kennedy International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/119.0.0.0 Safari\/537.36","ip":"172.70.206.212","session_id":"r4W1jLyHmOa0","region":"California","queryID":"r4W1jLyHmOa0","timestamp":1701289376},{"request":{"infants":"0","children":"0","adults":"1","start":"01\/03\/2024","from":"(DEN) Denver International Airport","end":"01\/10\/2024","to":"(SFO) San Francisco International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (Linux; Android 11; motorola one 5G ace Build\/RZKS31.Q3-45-16-3-11; wv) AppleWebKit\/537.36 (KHTML, like Gecko) Version\/4.0 Chrome\/119.0.6045.114 Mobile Safari\/537.36 [FB_IAB\/FB4A;FBAV\/442.0.0.44.114;]","ip":"172.68.34.61","session_id":"sfapfSFcq1vd","region":"Colorado","queryID":"sfapfSFcq1vd","timestamp":1701718531},{"request":{"infants":"0","children":"0","adults":"1","start":"11\/17\/2023","from":"(SUB) Juanda International Airport","end":"undefined","to":"(CGK) Soekarno-Hatta International Airport","type":"one-way","class":"economy"},"uid":"BtjWmsjOOvedb9","agent":"Mozilla\/5.0 (Windows NT 10.0; Win64; x64; rv:109.0) Gecko\/20100101 Firefox\/119.0","ip":"172.70.39.115","region":"Virginia","queryID":"ssa0ykvYB5xw","timestamp":1700153414},{"request":{"infants":"0","children":"0","adults":"1","start":"01\/03\/2024","from":"(KUL) Kuala Lumpur International Airport","end":"01\/10\/2024","to":"(DMK) Don Mueang International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (Linux; Android 11; motorola one 5G ace Build\/RZKS31.Q3-45-16-3-11; wv) AppleWebKit\/537.36 (KHTML, like Gecko) Version\/4.0 Chrome\/119.0.6045.114 Mobile Safari\/537.36 [FB_IAB\/FB4A;FBAV\/442.0.0.44.114;]","ip":"172.68.34.61","session_id":"stnE30dEMhGW","region":"Colorado","queryID":"stnE30dEMhGW","timestamp":1701718584,"noResults":true},{"request":{"infants":"0","children":"0","adults":"1","start":"02\/12\/2024","from":"(MDW) Chicago Midway International Airport","end":"undefined","to":"(PHX) Phoenix Sky Harbor International Airport","type":"one-way","class":"economy"},"uid":"MltFTJdfyTdUKA","agent":"Mozilla\/5.0 (Windows NT 10.0; Win64; x64; rv:109.0) Gecko\/20100101 Firefox\/119.0","ip":"172.70.38.80","region":"Virginia","queryID":"t5Q6KXhrlggU","timestamp":1700076422},{"request":{"infants":"0","children":"1","adults":"2","start":"01\/11\/2024","from":"(EWR) Newark Liberty International Airport","end":"01\/18\/2024","to":"(BNA) Nashville International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/119.0.0.0 Safari\/537.36","ip":"162.158.159.13","session_id":"tIkDNwP2Fnhl","region":"New Jersey","queryID":"tIkDNwP2Fnhl","timestamp":1701712973,"selections":[{"offer":"off_0000AcTQCs4MKKrVMzvqWp_0","amount":"215.34","discountPresented":"0.75","direction":"departure"},{"offer":"off_0000AcTQCs4MKKrVMzvqWp_1","amount":"215.34","discountPresented":"0.75","direction":"return"}]},{"request":{"infants":"0","children":"0","adults":"1","start":"09\/29\/2024","from":"(PDX) Portland International Airport","end":"10\/01\/2024","to":"(DTW) Detroit Metro Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (Linux; Android 13; Pixel 6 Pro Build\/TQ3A.230901.001.C2; wv) AppleWebKit\/537.36 (KHTML, like Gecko) Version\/4.0 Chrome\/119.0.6045.163 Mobile Safari\/537.36 [FB_IAB\/FB4A;FBAV\/442.0.0.44.114;]","ip":"172.71.146.36","session_id":"tPzI2JZQ1bf8","region":"Washington","queryID":"tPzI2JZQ1bf8","timestamp":1701493039},{"request":{"infants":"0","children":"0","adults":"1","start":"01\/01\/2024","from":"(YYZ) Toronto Pearson International Airport","end":"01\/05\/2024","to":"(PHX) Phoenix Sky Harbor International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (iPhone; CPU iPhone OS 17_1_1 like Mac OS X) AppleWebKit\/605.1.15 (KHTML, like Gecko) Mobile\/21B91 Instagram 309.1.1.28.108 (iPhone15,4; iOS 17_1_1; en_US; en; scale=3.00; 1179x2556; 537288535)","ip":"172.70.210.204","session_id":"tUMLORbm6Hvh","region":"California","queryID":"tUMLORbm6Hvh","timestamp":1701582766},{"request":{"infants":"0","children":"0","adults":"1","start":"12\/27\/2023","from":"(ORD) O\'Hare International Airport","end":"01\/03\/2024","to":"(CPT) Cape Town International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/119.0.0.0 Safari\/537.36","ip":"172.70.100.82","region":"Illinois","queryID":"tXo4T7OrcP0j","timestamp":1701113627},{"request":{"infants":"0","children":"0","adults":"1","start":"02\/18\/2024","from":"(KUL) Kuala Lumpur International Airport","end":"undefined","to":"(BNE) Brisbane Airport","type":"one-way","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (Linux; Android 14; Pixel 8 Pro Build\/UD1A.231105.004; wv) AppleWebKit\/537.36 (KHTML, like Gecko) Version\/4.0 Chrome\/119.0.6045.67 Mobile Safari\/537.36 Instagram 309.0.0.40.113 Android (34\/14; 360dpi; 1008x2077; Google\/google; Pixel 8 Pro; husky; husky; en_US; 536988435)","ip":"172.71.154.35","session_id":"tuZqR6kpkH1w","region":"California","queryID":"tuZqR6kpkH1w","timestamp":1701467738},{"request":{"infants":"0","children":"0","adults":"1","start":"01\/03\/2024","from":"(BLI) Bellingham International Airport","end":"undefined","to":"(OAK) Oakland International Airport","type":"one-way","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (Linux; Android 13; SM-G991U1 Build\/TP1A.220624.014; wv) AppleWebKit\/537.36 (KHTML, like Gecko) Version\/4.0 Chrome\/119.0.6045.163 Mobile Safari\/537.36 Instagram 309.1.0.41.113 Android (33\/13; 480dpi; 1080x2176; samsung; SM-G991U1; o1q; qcom; en_US; 541635892)","ip":"172.71.155.18","session_id":"uW1rt63sZbHN","region":"California","queryID":"uW1rt63sZbHN","timestamp":1701725063},{"request":{"infants":"0","children":"0","adults":"1","start":"01\/09\/2024","from":"(PHX) Phoenix Sky Harbor International Airport","end":"01\/11\/2024","to":"(LAS) McCarran International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (Linux; Android 10; K) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/119.0.0.0 Mobile Safari\/537.36","ip":"172.69.34.45","session_id":"uXRqy7ltZdbG","region":"California","queryID":"uXRqy7ltZdbG","timestamp":1701494466},{"request":{"infants":"0","children":"0","adults":"1","start":"12\/25\/2023","from":"(PDX) Portland International Airport","end":"undefined","to":"(CUN) Canc\u00fan International Airport","type":"one-way","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/119.0.0.0 Safari\/537.36","ip":"172.68.174.234","session_id":"unNBRIHY5BCZ","region":"Oregon","queryID":"unNBRIHY5BCZ","timestamp":1701197890},{"request":{"infants":"0","children":"1","adults":"4","start":"01\/11\/2024","from":"(EWR) Newark Liberty International Airport","end":"01\/18\/2024","to":"(BNA) Nashville International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/119.0.0.0 Safari\/537.36","ip":"172.70.110.89","session_id":"uszRJFzo8nrd","region":"New Jersey","queryID":"uszRJFzo8nrd","timestamp":1701713982,"selections":[{"offer":"off_0000AcTRhwjJ9K3AktT1RK_0","amount":"358.90","discountPresented":"0.75","direction":"departure"},{"offer":"off_0000AcTRhwjJ9K3AktT1RK_1","amount":"358.90","discountPresented":"0.75","direction":"return"}]},{"request":{"infants":"0","children":"1","adults":"2","start":"01\/11\/2024","from":"(EWR) Newark Liberty International Airport","end":"01\/18\/2024","to":"(BNA) Nashville International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/119.0.0.0 Safari\/537.36","ip":"172.70.110.89","session_id":"v7sZU2A1mVSa","region":"New Jersey","queryID":"v7sZU2A1mVSa","timestamp":1701713933},{"request":{"infants":"0","children":"0","adults":"1","start":"12\/31\/2023","from":"(IAD) Washington Dulles International Airport","end":"01\/07\/2024","to":"(LHR) Heathrow Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (iPhone; CPU iPhone OS 17_1_1 like Mac OS X) AppleWebKit\/605.1.15 (KHTML, like Gecko) Version\/17.1.1 Mobile\/15E148 Safari\/604.1","ip":"172.70.175.54","session_id":"v8U5SJykn6Jb","region":"Virginia","queryID":"v8U5SJykn6Jb","timestamp":1701470947},{"request":{"infants":"0","children":"0","adults":"1","start":"09\/23\/2024","from":"(OAK) Oakland International Airport","end":"09\/30\/2024","to":"(DFW) Dallas\/Fort Worth International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (Linux; Android 13; SM-G986U Build\/TP1A.220624.014; wv) AppleWebKit\/537.36 (KHTML, like Gecko) Version\/4.0 Chrome\/119.0.6045.164 Mobile Safari\/537.36 Instagram 309.1.0.41.113 Android (33\/13; 450dpi; 1080x2192; samsung; SM-G986U; y2q; qcom; en_US; 541635884)","ip":"172.71.158.238","session_id":"vG9j3Iz9BOCd","region":"California","queryID":"vG9j3Iz9BOCd","timestamp":1701714947},{"request":{"infants":"0","children":"0","adults":"2","start":"12\/29\/2023","from":"(RDU) Raleigh\u2013Durham International Airpor","end":"12\/31\/2023","to":"(PHX) Phoenix Sky Harbor International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (X11; CrOS x86_64 14541.0.0) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/118.0.0.0 Safari\/537.36","ip":"172.70.38.80","region":"Virginia","queryID":"vKoQBSLYqloU","timestamp":1700388992},{"request":{"infants":"0","children":"1","adults":"4","start":"01\/11\/2024","from":"(EWR) Newark Liberty International Airport","end":"01\/18\/2024","to":"(BNA) Nashville International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/119.0.0.0 Safari\/537.36","ip":"172.70.110.89","session_id":"vOG541CQM6Uj","region":"New Jersey","queryID":"vOG541CQM6Uj","timestamp":1701713953,"selections":[{"offer":"off_0000AcTRfHXn3cxlZYSDOw_0","amount":"358.90","discountPresented":"0.75","direction":"departure"}]},{"request":{"infants":"0","children":"0","adults":"1","start":"01\/02\/2024","from":"(LGA) LaGuardia Airport","end":"01\/09\/2024","to":"(ORD) O\'Hare International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (Linux; Android 10; K) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/119.0.0.0 Mobile Safari\/537.36","ip":"172.70.111.149","session_id":"vn2cprvSfEe5","region":"New Jersey","queryID":"vn2cprvSfEe5","timestamp":1701583303},{"request":{"infants":"0","children":"0","adults":"1","start":"12\/15\/2023","from":"(TPE) Taiwan Taoyuan International Airport","end":"undefined","to":"(SEA) Seattle-Tacoma International Airport","type":"one-way","class":"economy"},"uid":"ATKOjqj7LqWMvW","agent":"Mozilla\/5.0 (Linux; Android 10; K) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/118.0.0.0 Mobile Safari\/537.36","ip":"172.71.150.15","region":"Washington","queryID":"w07Plo0TVBT7","timestamp":1700069884},{"request":{"infants":"0","children":"0","adults":"1","start":"04\/10\/2024","from":"(SFO) San Francisco International Airport","end":"undefined","to":"(CUN) Canc\u00fan International Airport","type":"one-way","class":"economy"},"uid":"n4ID8eelAsUiXl","agent":"Mozilla\/5.0 (X11; CrOS x86_64 14541.0.0) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/119.0.0.0 Safari\/537.36","ip":"172.71.155.48","session_id":"w513kcBXrKeJ","region":"California","queryID":"w513kcBXrKeJ","timestamp":1701572214},{"request":{"infants":"0","children":"0","adults":"1","start":"01\/29\/2024","from":"(CVG) Cincinnati\/Northern Kentucky International Airport","end":"01\/31\/2024","to":"(BOS) General Edward Lawrence Logan International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (iPhone; CPU iPhone OS 16_3_1 like Mac OS X) AppleWebKit\/605.1.15 (KHTML, like Gecko) Mobile\/15E148 Instagram 271.1.0.11.84 (iPhone15,2; iOS 16_3_1; en_US; en-US; scale=3.00; 1179x2556; 449456748)","ip":"172.69.71.125","session_id":"wDOEqyB2nymT","region":"Georgia","queryID":"wDOEqyB2nymT","timestamp":1701523229,"selections":[{"offer":"off_0000AcOrz1pAm5DuOpNg2k_0","amount":"289.39","discountPresented":"0.75","direction":"departure"},{"offer":"off_0000AcOrz1pAm5DuOpNg2k_1","amount":"289.39","discountPresented":"0.75","direction":"return"}]},{"request":{"infants":"0","children":"0","adults":"1","start":"01\/09\/2024","from":"(BWI) Baltimore Washington International Thurgood Marshall Airport","end":"undefined","to":"(ATL) Hartsfield-Jackson Atlanta International Airport","type":"one-way","class":"economy"},"uid":"MltFTJdfyTdUKA","agent":"Mozilla\/5.0 (Windows NT 10.0; Win64; x64; rv:109.0) Gecko\/20100101 Firefox\/119.0","ip":"172.70.174.201","region":"Virginia","queryID":"wimVX9vOcmaj","timestamp":1700082800},{"request":{"infants":"0","children":"0","adults":"1","start":"12\/16\/2023","from":"(GRR) Gerald R. Ford International Airport","end":"12\/23\/2023","to":"(FAT) Fresno Yosemite International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (Linux; Android 13; SM-G781U Build\/TP1A.220624.014; wv) AppleWebKit\/537.36 (KHTML, like Gecko) Version\/4.0 Chrome\/119.0.6045.66 Mobile Safari\/537.36 [FB_IAB\/FB4A;FBAV\/440.0.0.31.105;]","ip":"172.70.130.109","region":"Illinois","queryID":"xYb5KXSQQOBu","timestamp":1700175787},{"request":{"infants":"0","children":"0","adults":"1","start":"03\/29\/2024","from":"(PHX) Phoenix Sky Harbor International Airport","end":"04\/01\/2024","to":"(EWR) Newark Liberty International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/119.0.0.0 Safari\/537.36","ip":"172.70.211.63","session_id":"xpmlfwnSpYwa","region":"California","queryID":"xpmlfwnSpYwa","timestamp":1701289439,"selections":[{"offer":"off_0000AcJGEczGKOU5HReYKn_0","amount":"535.60","direction":"departure"},{"offer":"off_0000AcJGEdDnSM6u0WS9z3_1","amount":"535.60","direction":"return"}]},{"request":{"infants":"0","children":"0","adults":"1","start":"12\/31\/2023","from":"(ELP) El Paso International Airport","end":"01\/07\/2024","to":"(HOU) William P. Hobby Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (iPhone; CPU iPhone OS 17_2 like Mac OS X) AppleWebKit\/605.1.15 (KHTML, like Gecko) Mobile\/21C5046c Instagram 309.1.1.28.108 (iPhone14,2; iOS 17_2; en_US; en; scale=3.00; 1170x2532; 537288535)","ip":"172.71.174.192","session_id":"xrVR7Z7VwSdo","region":"Texas","queryID":"xrVR7Z7VwSdo","timestamp":1701464641},{"request":{"infants":"0","children":"0","adults":"1","start":"01\/09\/2024","from":"(PHX) Phoenix Sky Harbor International Airport","end":"01\/17\/2024","to":"(LAS) McCarran International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (Linux; Android 10; K) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/119.0.0.0 Mobile Safari\/537.36","ip":"172.69.34.46","session_id":"yBa8BmNQ9oa2","region":"California","queryID":"yBa8BmNQ9oa2","timestamp":1701494534},{"request":{"infants":"0","children":"0","adults":"1","start":"02\/20\/2024","from":"(PDX) Portland International Airport","end":"02\/27\/2024","to":"(SJU) Luis Mu\u00f1oz Mar\u00edn International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (Linux; Android 13; SM-S908U1 Build\/TP1A.220624.014; wv) AppleWebKit\/537.36 (KHTML, like Gecko) Version\/4.0 Chrome\/119.0.6045.193 Mobile Safari\/537.36 Instagram 309.1.0.41.113 Android (33\/13; 560dpi; 1440x2988; samsung; SM-S908U1; b0q; qcom; en_US; 541635884)","ip":"172.68.174.235","session_id":"yK5JR6FwFdhD","region":"Oregon","queryID":"yK5JR6FwFdhD","timestamp":1701537867},{"request":{"infants":"0","children":"0","adults":"1","start":"02\/11\/2024","from":"(LAX) Los Angeles International Airport","end":"02\/22\/2024","to":"(NRT) Narita International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (Linux; Android 10; K) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/119.0.0.0 Mobile Safari\/537.36","ip":"172.69.33.71","session_id":"ybX1fBIWLn3A","region":"California","queryID":"ybX1fBIWLn3A","timestamp":1701235510},{"request":{"infants":"0","children":"0","adults":"1","start":"12\/27\/2023","from":"(LAX) Los Angeles International Airport","end":"12\/31\/2023","to":"(PDX) Portland International Airport","type":"round-trip","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (iPhone; CPU iPhone OS 17_1_2 like Mac OS X) AppleWebKit\/605.1.15 (KHTML, like Gecko) Mobile\/21B101 Instagram 309.1.1.28.108 (iPhone16,1; iOS 17_1_2; en_US; en; scale=3.00; 1179x2556; 537288535)","ip":"162.158.90.152","session_id":"yyVVS0wWl8l9","region":"California","queryID":"yyVVS0wWl8l9","timestamp":1701587329},{"request":{"infants":"0","children":"0","adults":"1","start":"02\/06\/2024","from":"(FLL) Fort Lauderdale-Hollywood International Airport","end":"undefined","to":"(ATL) Hartsfield-Jackson Atlanta International Airport","type":"one-way","class":"economy"},"uid":"MltFTJdfyTdUKA","agent":"Mozilla\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/119.0.0.0 Safari\/537.36","ip":"162.158.90.236","region":"California","queryID":"zC5IBbV9Jv6X","timestamp":1700604374},{"request":{"infants":"0","children":"0","adults":"1","start":"12\/27\/2023","from":"(PHX) Phoenix Sky Harbor International Airport","end":"undefined","to":"(LHR) Heathrow Airport","type":"one-way","class":"economy"},"uid":null,"agent":"Mozilla\/5.0 (iPhone; CPU iPhone OS 16_6 like Mac OS X) AppleWebKit\/605.1.15 (KHTML, like Gecko) CriOS\/119.0.6045.169 Mobile\/15E148 Safari\/604.1","ip":"162.158.91.4","region":"California","queryID":"zgFbcLsSpZPz","timestamp":1701120098}]', true);

        // clean the database

        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $auth = app('firebase.auth');

        

        $searchesDocs = $db->collection("FlightSearches")->documents();

        // truncation

        foreach($searchesDocs as $doc){
            $id = $doc->id();
            $searchesDocs = $db->collection("FlightSearches")->document($id)->delete();
        }

        foreach($searches as $search){
            $db->collection("FlightSearches")->document($search["queryID"])->set($search);
        }
    }

    public function updateName(){
        
        //$uid = "NxyWoVn7XBcbAUcuvxdhovqTrcL2";
        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $auth = app('firebase.auth');

        $properties = [
            'displayName' => "Katherine|Kelly"
        ];

        //$user = $auth->getUserByEmail("kkellylive@gmail.com");
        //$customClaims = $user->customClaims;
        //$customClaims["dob"] = "1978-03-05";



        //$uid = $user->uid;

        //$auth->setCustomUserClaims($user->uid, $customClaims);

        //$updatedUser = $auth->updateUser($uid, $properties);


        $user = $auth->getUserByEmail("montana.mcleod@yahoo.com");
        dd($user);
    }

    public function appPaidTrue(){
        /*
        $uid = "7FUVftKja3Mytt8xmGXNQQZCbyH2";
        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $auth = app('firebase.auth');
        $user = $auth->getUser($uid);

        $customClaims = $user->customClaims;
        $customClaims["appPaid"] = true;

        $auth->setCustomUserClaims($user->uid, $customClaims);
        */
    }

    

    public function importTransactions(){
        $searches = json_decode('', true);

        // clean the database

        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $auth = app('firebase.auth');

        $searches = [];

        $searchesDocs = $db->collection("Transactions")->documents();

        // truncation

        foreach($searchesDocs as $doc){
            $doc->delete();
        }

        foreach($searches as $search){
            
            $db->collection("Transactions")->document($search["search_id"])->set($search);
        }
    }

    public function canPointsPurchasePoints($uid, $db, $transactions = null){
        $transactions = $db->collection("Transactions")
            ->where("type","=","points-deposit")
            ->where("uid","=",$uid)
            ->orderBy("timestamp","DESC")
            ->documents();

        $now = \Carbon\Carbon::now();

        $numTransactions = 0;

        foreach($transactions as $transaction){
            $transaction = $transaction->data();
            if(str_contains($transaction["note"], "Points Purchase") && $transaction["from"] == "Venti"){
                $transactionDif = \Carbon\Carbon::parse($transaction["timestamp"])->diffInDays($now);

                if($transactionDif < 7){
                    return false;
                }
            }
            $numTransactions++;
        }

        return true;
    }

    public function winner(){
        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $auth = app('firebase.auth');

        $dwolla = new Dwolla;

        $customerDocs = $db->collection('Customers')->documents();
        $customers = [];

        $totalEntries = 0;

        foreach($customerDocs as $customer){
            $customer = $customer->data();
            if(!array_key_exists("wallet", $customer)){
                // no wallet url for this person.
                continue;
            }

            if($customer["status"] != "verified"){
                continue;
            }
            $walletDetails = $dwolla->getFundingSourceBalance($customer["wallet"]);
            $balance = (float) $walletDetails->balance->value;

            try{
                $user = $auth->getUser($customer["uid"]);
            }
            catch(FirebaseException $e){
                // user account doesn't exist or is suspended
                
                continue;
            }

            $points = $this->getPoints($user, $db);

            $subscription = $user->customClaims["subscription"] ?? "buddy";
            $entries = getSweepstakeEntries($subscription, $balance, $points);

            $customer["balance"] = $balance;
            $customer["points"] = $points;
            $customer["entries"] = $entries;

            $totalEntries += $entries;

            array_push($customers, $customer);
        }

        // get their balance

        $winner = $this->selectRandomWinner($customers);

        // get the user profile via their UID

        $winner = $auth->getUser($winner);

        echo "<a href='/admin/user/" . $winner->uid . "' target='_blank'>" . $winner->displayName  ."</a><br>";

        echo "Total Entries: " . number_format($totalEntries) . "<br>";

        // get winner based on snapshots?

        dd($winner);

        // get their points
    }

    public function selectRandomWinner($entrants) {
        $weightedEntrants = [];

        // Assign each ticket a unique number in the pool
        foreach ($entrants as $entrant) {
            for ($i = 0; $i < $entrant['entries']; $i++) {
                $weightedEntrants[] = $entrant['uid'];
            }
        }

        // Select a random winner from the weighted pool
        $randomIndex = mt_rand(0, count($weightedEntrants) - 1);
        $winnerUID = $weightedEntrants[$randomIndex];

        // Return the UID of the winner
        return $winnerUID;
    }

    public function getPoints($user, $db){
        $transactions = [];

        $transactionDocs = $db->collection("Transactions");
        $transactionDocs = $transactionDocs
                ->where("uid","=",$user->uid)
                ->where("env","=",env("APP_ENV"))
                ->documents();
            // I need a separate firebase instance for sandboxing

            foreach($transactionDocs as $transactionDoc){
                array_push($transactions,  $transactionDoc->data());
            }

        return getPointsBalance($transactions);
    }

    public function getStatements(){
        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $auth = app('firebase.auth');
        $statements = [];

        $statementList = $db->collection("BalanceSnapshots")->where("statement","=",true)->documents();

        foreach($statementList as $statement){
            $statement = $statement->data();
            array_push($statements, $statement);
        }
        
        $data = [
            "title" => "Admin | Statements",
            "statements" => $statements
        ];

        return view("admin.boardingpass.statements", $data);
    }
    

    public function getStatement($id){
        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $auth = app('firebase.auth');

        $statement = $db->collection("BalanceSnapshots")->document($id)->snapshot()->data();

        $data = [
            "title" => "Admin | Statement $id",
            "statement" => $statement
        ];

        return view("admin.boardingpass.statement", $data);
    }

    public function updateStatement(Request $request){
        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $auth = app('firebase.auth');

        $statement = $db->collection("BalanceSnapshots")->document($request->id);

        $update = $statement->update([
            ["path" => "month","value" => $request->month],
            ["path" => "year","value" => $request->year]
        ]);

        return Redirect::back();
    }

    function getRefund(){
        $firestore = app('firebase.firestore');
        $db = $firestore->database();

        
        $data = [
            "title" => "Admin | Refund",
        ];

        return view("admin.boardingpass.refund", $data);
    }

    function processRefund(Request $request){
        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $auth = app('firebase.auth');

        $order = $request->order;
        $order = $db->collection("Orders")->document($order);

        $orderData = $order->snapshot()->data();
        $type = $orderData["type"];
        $user = $auth->getUser($orderData["uid"]);
        $amountPaid = $request->amount;
        $pointsPaid = $request->points;
        $note = $request->note;
        $dwolla = new Dwolla;
        $customerWallet = "";

        // send customer the funds

        $customer = $dwolla->getCustomer($user, $auth, $db);
        $fundingSources = $dwolla->fundingSources($customer["customerID"]);
        if($fundingSources){
                foreach($fundingSources as $fundingSource){
                    if(!$fundingSource->removed 
                        && isset($fundingSource->bankAccountType)
                        && $fundingSource->status == "verified"
                    ){
                        // this user can now make deposits and withdrawals using their bank account

                        $transactable = true;
                    }
                    if($fundingSource->type == "balance"){
                        $wallet = $fundingSource->_links->self->href;
                        $customerWallet = $fundingSource->_links->self->href;
                    }
                }
            }

        $timestamp = Carbon::now("UTC")->timestamp;
        $booking_reference = $orderData["booking_reference"];
        $refID = $orderData["refID"];
        $date = Carbon::now('UTC')->format("Y-m-d H:i:s");
        
        if($amountPaid != 0){
            $transfer = $dwolla->transferFromVentiWalletToUserWallet($user, $customerWallet, $amountPaid, "$type-refund");

            if($transfer){
                // record the transfers
                
                $transactionID = generateRandomString(12);
                $transferID = explode("transfers/",$transfer)[1];
                

                $data = [
                    "transactionID" => $transactionID,
                    "uid" => $user->uid,
                    "type" => "deposit",
                    "timestamp" => $timestamp,
                    "date" => $date,
                    "amount" => $amountPaid,
                    "speed" => "next-available",
                    "fee" => 0,
                    "status" => "complete",
                    "from" => "Venti",
                    "to" => "My Boarding Pass",
                    "total" => $amountPaid,
                    "note" => ucfirst($type) . " Refund: REF# $booking_reference",
                    "transferID" => $transferID,
                    "transactionUrl" => $transfer,
                    "refID" => $refID,
                    "env" => env('APP_ENV')
                ];

                $saveTransaction = $db->collection("Transactions")->document($transactionID)->set($data);
            }
        }

        if($pointsPaid != 0){
            $transactionID2 = generateRandomString(12);

            $data = [
                "transactionID" => $transactionID2,
                "uid" => $user->uid,
                "type" => "points-deposit",
                "timestamp" => $timestamp,
                "date" => $date,
                "amount" => $pointsPaid ,
                "speed" => "next-available",
                "fee" => 0,
                "status" => "complete",
                "from" => "Venti",
                "to" => "My Boarding Pass",
                "total" => $pointsPaid,
                "note" => ucfirst($type) . " Refund: REF# $booking_reference",
                "transferID" => $transferID ?? null, // it's wise to link this with the previous transfer
                "transactionUrl" => null,
                "env" => env('APP_ENV'),
                "refID" => $refID,
                "taxable" => false // points are not taxable when refunded
            ];

            $saveTransaction = $db->collection("Transactions")->document($transactionID2)->set($data);
        }
        

            /*

            $update = $order->update([
                ["path" => "amountRefunded", "value" => $amountPaid],
                ["path" => "pointsRefunded", "value" => $pointsPaid],
                ["path" => "dateRefunded", "value" => $timestamp],
                ["path" => "noteRefunded", "value" => $note]
            ]);

            */

            return Redirect::back();

    }

    public function mysteryboxOpens(){
        $firestore = app('firebase.firestore');
        $db = $firestore->database();

        $mysteryDocs = $db->collection("MysteryBoxOpens")->documents();
        $boxes = [];

        foreach($mysteryDocs as $mysteryDoc){
            $mysteryDoc = $mysteryDoc->data();
            if($mysteryDoc["uid"] != env('APP_ADMINS_' . strtoupper(env('APP_ENV')))){
                array_push($boxes, $mysteryDoc);    
            }
        }

        $data = [
            "title" => "Admin | Mysterybox Opens",
            "boxes" => $boxes
        ];

        return view("admin.boardingpass.mysterybox", $data);
    }
}
