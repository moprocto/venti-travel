<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Kreait\Firebase\Exception\FirebaseException;
use Illuminate\Support\Facades\Validator;
use Session;
use Redirect;
use Mail;

class AirlockController extends Controller
{
    public function index()
    {
        $data = [
            "title" => "Airlock Flight Protection up to $20,000 per Year",
            "description" => "Cancel your flight for any reason with Airlock Flight Protection. No questions asked, hassle-free cancellation coverage.",
            "image" => "https://venti.co/assets/img/airlock-social.jpg",
            "url" => "https://venti.co/airlock"
        ];

        return view('airlock.landing', $data);
    }

    public function registerPage()
    {
        if(Session::get('user') !== null){
            return Redirect::to('/airlock/home');
        }
        
        $data = [
            "title" => "Airlock | Create Account"
        ];

        return view('airlock.register', $data);
    }

    public function register(Request $request)
    {
        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $auth = app('firebase.auth');
        $photoUrl = '/assets/img/profile.jpg';

        $validator = Validator::make($request->all(), [
            'f_name' => 'required|max:255',
            'l_name' => 'required|max:255',
            'email' => 'required|email|max:255',
            'password' => 'required|min:8',
        ]);

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        try {
            $userProperties = [
                'email' => $request->email,
                'emailVerified' => false,
                'password' => $request->password,
                'displayName' => $request->f_name . "|" . $request->l_name,
                'photoUrl' => $photoUrl,
                'disabled' => false
            ];

            $createdUser = $auth->createUser($userProperties);

            $auth->setCustomUserClaims($createdUser->uid, [
                'smsVerified' => false,
                'timezone' => "America/New_York",
                'createdAt' => now()->timestamp,
                'airlock' => true
            ]);

            Session::put('user', $createdUser);
            Session::put('active', true);

            try{
                // send custom template

                $email = $createdUser->email;

                $link = $auth->getEmailVerificationLink($email);

                $data = [
                    "verificationLink" => $link,
                    "user" => $createdUser,
                    "last" => false
                ];

                $subject = "Welcome to Venti! You Need to Verify Your Email";

                try{
                    Mail::send('emails.onboarding.email-verification', array("data" => $data), function($message) use($email, $subject)
                        {
                            $message
                                ->to($email)
                                ->from("no-reply@venti.co","Venti")
                                ->replyTo("no-reply@venti.co")
                                ->subject($subject);
                    });
                }
                catch(Exception $e){
                    //
                }

            } catch (FirebaseException $e) {
                
            }

            return Redirect::to('/airlock/onboarding/email');


        } catch(FirebaseException $e) {
            return Redirect::back()->withErrors(['email' => 'This email is already in use']);
        }
    }

    public function loginPage()
    {
        if(Session::get('user') !== null){
            return Redirect::to('/airlock/home');
        }
        
        $data = [
            "title" => "Airlock | Login"
        ];

        return view('airlock.login', $data);
    }

    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required',
        ]);

        $loginAttempsRemaining = Session::get('loginAttempsRemaining') ?? 10;

        if($loginAttempsRemaining == 0){
            return Redirect::to('/login')->withErrors(["general" => "Incorrect email or password"])->withInput();
        }

        if ($validator->fails()) {
            return Redirect::to('/login')->withErrors(["general" => "Incorrect email or password"])->withInput();
        }

        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $auth = app('firebase.auth');

        try{
            $signInResult = $auth->signInWithEmailAndPassword($request->email, $request->password);
            if(isset($signInResult)){
                $user = $auth->getUserByEmail($request->email);

                // store previous session vars related to searches

                $priorSearch = Session::get('flight-search');

                Session::flush();
                Session::regenerate();
                Session::put('sessionToken', $signInResult->idToken());
                Session::put('user', $user);
                Session::put('active', false);

                // restore previous session vars

                Session::put('flight-search', $priorSearch);

                return Redirect::to('/airlock/home');
                // check to see if the user is an airlock user
            }
            $customClaims = $auth->getUser($user->uid)->customClaims;
            $airlock = $customClaims["airlock"] ?? false;
            
            Session::put('loginAttempsRemaining',$loginAttempsRemaining);
            
            if($airlock === true){
                return Redirect::to('/airlock/home');
            }
            
            return Redirect::to('/airlock/home');
        }
        catch(Exception $e){
            $loginAttempsRemaining--;
            Session::put('loginAttempsRemaining',$loginAttempsRemaining);
            return Redirect::to('/login')->withErrors(["general" => "Incorrect email or password. $loginAttempsRemaining login attemps remaining"])->withInput();
        }
        
        return Redirect::to('login')->withErrors(["general" => "There was an error signing you in. Please try again!"])->withInput();
    }

    public function home()
    {
        if(Session::get('user') === null){
            return Redirect::to('/airlock/login');
        }



        $user = Session::get('user');

        if($user->emailVerified != true){
            return Redirect::to('/airlock/onboarding/email');    
        }

        $firestore = app('firebase.firestore');
        $db = $firestore->database();

        // Get user's protection plans (if any)
        $plans = $db->collection('AirlockPlans')
            ->where('userId', '=', $user->uid)
            ->documents();

        // Check if user has any plans
        if ($plans->isEmpty()) {
            return Redirect::to('/airlock/plans');
        }

        $data = [
            "title" => "Airlock Dashboard",
            "user" => $user,
            "plans" => $plans
        ];

        return view('airlock.home', $data);
    }

    public function plans(){

    }

    public function purchasePlan(Request $request){

    }

    public function checkPaymentStatus()
    {
        if(Session::get('user') === null){
            return Redirect::to('/airlock/login');
        }

        $user = Session::get('user');
        $customClaims = $user->customClaims;
        
        // Check if user has already paid for a plan
        $hasPlan = isset($customClaims['airlockPlan']);
        
        if (!$hasPlan) {
            return Redirect::to('/airlock/plans');
        }
        
        return Redirect::to('/airlock/home');
    }

    public function showPlans()
    {
        if(Session::get('user') === null){
            return Redirect::to('/airlock/login');
        }

        $data = [
            "title" => "Airlock | Select Protection Plan",
            "plans" => [
                "standard" => [
                    "name" => "Standard",
                    "price" => 500,
                    "coverage" => 5000,
                    "stripe_price_id" => env('AIRLOCK_STANDARD_STRIPE_ID_LOCAL')
                ],
                "pro" => [
                    "name" => "Pro",
                    "price" => 1000,
                    "coverage" => 10000,
                    "stripe_price_id" => env('AIRLOCK_PRO_STRIPE_ID_LOCAL')
                ],
                "jetsetter" => [
                    "name" => "Jetsetter",
                    "price" => 3000,
                    "coverage" => 20000,
                    "stripe_price_id" => env('AIRLOCK_JETSETTER_STRIPE_ID_LOCAL')
                ]
            ]
        ];

        return view('airlock.plans', $data);
    }

    public function processPlanSelection(Request $request)
    {
        if(Session::get('user') === null){
            return Redirect::to('/airlock/login');
        }

        $validator = Validator::make($request->all(), [
            'plan' => 'required|in:standard,pro,jetsetter',
            'frequency' => 'required|in:month,year',
        ]);

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator);
        }

        $user = Session::get('user');
        $stripe = new \Stripe\StripeClient(env('STRIPE_SECRET_' . strtoupper(env("APP_ENV"))));

        try {
            $checkout_session = $stripe->checkout->sessions->create([
                'customer_email' => $user->email,
                'line_items' => [[
                    'price' => env("AIRLOCK_" . strtoupper($request->plan) . "_STRIPE_PRICE_" . strtoupper($request->frequency) . "_" . strtoupper(env("APP_ENV"))),
                    'quantity' => 1,
                ]],
                'mode' => 'subscription',
                'success_url' => url('/airlock/plans/payment/success?session_id={CHECKOUT_SESSION_ID}'),
                'cancel_url' => url('/airlock/plans'),
                'client_reference_id' => $user->uid,
                'metadata' => [
                    'plan' => $request->plan
                ]
            ]);

            return Redirect::to($checkout_session->url);
        } catch (\Exception $e) {
            dd($e->getMessage());
            return Redirect::back()->withErrors(['error' => $e->getMessage()]);
        }
    }

    public function handlePaymentSuccess(Request $request)
    {
        if (!$request->session_id) {
            return Redirect::to('/airlock/plans');
        }

        $stripe = new \Stripe\StripeClient(env('STRIPE_SECRET_' . strtoupper(env("APP_ENV"))));
        $session = $stripe->checkout->sessions->retrieve($request->session_id);
        
        if ($session->payment_status === 'paid') {
            $user = Session::get('user');
            $auth = app('firebase.auth');
            $firestore = app('firebase.firestore');
            $db = $firestore->database();
            
            // Update user's custom claims with their plan
            $customClaims = $user->customClaims ?? [];
            $customClaims['airlockPlan'] = $session->metadata->plan;
            $customClaims['airlockPlanPurchasedAt'] = now()->timestamp;
            
            $auth->setCustomUserClaims($user->uid, $customClaims);
            // Store subscription details in Firestore
            $db->collection('AirlockPlans')->add([
                'userId' => $user->uid,
                'stripeSubscriptionId' => $session->subscription,
                'plan' => $session->metadata->plan,
                'status' => 'active',
                'createdAt' => now()->timestamp,
                'updatedAt' => now()->timestamp
            ]);
            
            // Send receipt email
            $subscription = $stripe->subscriptions->retrieve($session->subscription);
            $emailData = [
                "product" => $session->metadata->plan,
                "fee" => number_format($subscription->plan->amount / 100, 2),
                "frequency" => $subscription->plan->interval,
                "plan_id" => $subscription->id
            ];

            Mail::send('emails.onboarding.subscription-receipt', ['data' => $emailData], function($message) use($user) {
                $message
                    ->to($user->email)
                    ->bcc("admin@venti.co","Venti")
                    ->from("no-reply@venti.co", "Venti")
                    ->replyTo("no-reply@venti.co")
                    ->subject("[Venti] Your Airlock Protection Plan Receipt");
            });
            
            return Redirect::to('/airlock/plans/success');
        }

        return Redirect::to('/airlock/plans')->withErrors(['payment' => 'Payment verification failed']);
    }

    public function showSuccessPage()
    {
        if(Session::get('user') === null){
            return Redirect::to('/airlock/login');
        }

        $user = Session::get('user');
        $customClaims = $user->customClaims;

        $data = [
            "title" => "Success! You're Protected",
            "user" => $user
        ];

        return view('airlock.success', $data);
    }

    public function showClaimPage()
    {
        if(Session::get('user') === null){
            return Redirect::to('/airlock/login');
        }

        $user = Session::get('user');
        $customClaims = $user->customClaims;
        
        if (!isset($customClaims['airlockPlan'])) {
            return Redirect::to('/airlock/plans');
        }

        $data = [
            "title" => "File an Airlock Claim",
            "user" => $user
        ];

        return view('airlock.claim', $data);
    }

    public function submitClaim(Request $request)
    {
        if(Session::get('user') === null){
            return Redirect::to('/airlock/login');
        }

        $validator = Validator::make($request->all(), [
            'confirmation_number' => 'required|string',
            'reason' => 'required|string|max:500'
        ]);

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        $user = Session::get('user');
        $firestore = app('firebase.firestore');
        $db = $firestore->database();

        $customClaims = $user->customClaims;

        try {
            $db->collection('AirlockClaims')->add([
                'userId' => $user->uid,
                'confirmation_number' => $request->confirmation_number,
                'reason' => $request->reason,
                'submitted_on' => now()->timestamp,
                'approved' => null,
                'approved_on' => null,
                'approved_by' => null,
                'airlock_plan' => $customClaims['airlockPlan']
            ]);

            return Redirect::to('/airlock/claim')
                ->with('success', true);
        } catch (\Exception $e) {
            return Redirect::back()
                ->withErrors(['error' => 'Failed to submit claim. Please try again.'])
                ->withInput();
        }
    }

    public function showPlanDetails($id)
    {
        if(Session::get('user') === null){
            return Redirect::to('/airlock/login');
        }

        $user = Session::get('user');
        $firestore = app('firebase.firestore');
        $db = $firestore->database();

        // Get the specific plan
        $plan = $db->collection('AirlockPlans')
            ->document($id)
            ->snapshot();

        if (!$plan->exists()) {
            return Redirect::to('/airlock/home')->withErrors(['error' => 'Plan not found']);
        }

        $planData = $plan->data();

        // Verify the plan belongs to the user
        if ($planData['userId'] !== $user->uid) {
            return Redirect::to('/airlock/home')->withErrors(['error' => 'Unauthorized']);
        }

        // Get the Stripe subscription details
        $stripe = new \Stripe\StripeClient(env('STRIPE_SECRET_' . strtoupper(env("APP_ENV"))));
        $subscription = $stripe->subscriptions->retrieve($planData['stripeSubscriptionId']);
        
        $data = [
            "title" => "Airlock Plan Details",
            "user" => $user,
            "plan" => $planData,
            "subscription" => $subscription,
            "planId" => $id
        ];

        return view('airlock.plan-details', $data);
    }

    public function downloadReceipt($id)
    {
        if(Session::get('user') === null){
            return Redirect::to('/airlock/login');
        }

        $user = Session::get('user');
        $firestore = app('firebase.firestore');
        $db = $firestore->database();

        // Get the specific plan
        $plan = $db->collection('AirlockPlans')
            ->document($id)
            ->snapshot();

        if (!$plan->exists() || $plan->data()['userId'] !== $user->uid) {
            return Redirect::to('/airlock/home')->withErrors(['error' => 'Unauthorized']);
        }

        $planData = $plan->data();
        
        // Get the Stripe invoice
        $stripe = new \Stripe\StripeClient(env('STRIPE_SECRET_' . strtoupper(env("APP_ENV"))));
        $subscription = $stripe->subscriptions->retrieve($planData['stripeSubscriptionId']);
        
        // Redirect to Stripe hosted invoice
        $latestInvoice = $stripe->invoices->retrieve($subscription->latest_invoice);
        
        return Redirect::to($latestInvoice->hosted_invoice_url);
    }

    public function onboardingEmail(){

        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $auth = app('firebase.auth');
        $user = Session::get('user');

        if($user === null){
            // session expired?
            return Redirect::to('/login');
        }

        try{
            $user = $auth->getUserByEmail($user->email);

            $data = [
                "title" => "Venti Onboarding | Email",
                "email" => $user->email,
                "emailVerified" => $user->emailVerified
            ];

            return view("airlock.onboarding.email", $data);
        }
        catch(FirebaseException $e){

        }
    }
}
