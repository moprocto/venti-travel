<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use Str;
use Mail;
use Redirect;

class NavigatorController extends Controller
{
    public function __construct(){
        $this->middleware('firebase.auth');
    }

    public function home(){
        return view("navigator.home");
    }

    public function schedule(){
        return view("navigator.schedule");
    }

    public function agreement(){
        return view("navigator.agreement");
    }

    public function navigatorAgree(Request $request){
        $email = $request->email;
        $uid = Session::get('user')->uid;

        $data = [];

        $subject = "You're Now a Venti Navigator";
        $from = "markus@venti.co";


        $firestore = app('firebase.firestore');
        $db = $firestore->database();


        $docRef = $db->collection("Navigators")->document($uid)->set(
            [
                "f_name" => $request->f_name,
                "l_name" => $request->l_name,
                "address" => $request->address,
                "email" => $email,
                "phone" => $request->phone,
                "signature" => $request->signature,
                "uid" => $uid,
                "createdAt" => Date("Y-m-d")
            ]
        );


        Mail::send('emails.application.invitation', $data, function($message) use($email , $subject, $from)
        {
            $message
                ->to($email)
                ->replyTo($email)
                ->from($from)
                ->subject($subject);
        });


        return Redirect::back()->withErrors(["success" => "Your answer to the math question was incorrect"]);
    }

    public function viewPlanner(){
        return view('navigator.planner');
    }

    public function savePlanner(Request $request){

    }

    public function plannerGPT(Request $request){

        $prompt = "You are a travel advisor. ". $request->prompt  . ". Provide your response in HTML format. For any recommendation, provide short, blog-friendly description.";
        $prompt = ["role" => "user", "content" => $prompt];

        $response = $this->askGPT($prompt);

        $answer = null;

        if(is_null($response)){

            return 501;
        }


        if(!array_key_exists("choices", $response)){
            return 501;
        }

        foreach($response["choices"] as $choice){
            $answer .= $choice["message"]["content"];
        }

        if(is_null($answer)){
            // chatGPT failed to provide a response to the prompt
            return 501;
        }

        $response["answer"] = $answer;


        // store the data for mining purposes

        $auth = app('firebase.auth');
        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $user = Session::get('user');
        
        $planID = Str::random(12);

        $docRef = $db->collection("Plans")->document($planID)->set(
            [          
                "clientID" => $user->uid,   
                "destination" => $request->destination,
                "country" => $request->country,
                "lat" => $request->lat,
                "long" => $request->long,
                "lodging" => $request->lodging,
                "food" => $request->food,
                "airports" => $request->airports,
                "planID" => $planID,
                "answer" => $answer,
                "chatgptID" => $response["id"] ?? null,
                "createdAt" => Date("Y-m-d")
            ]
        );
        $response["planID"] = $planID;

        return json_encode($response);
    }

    public function askGPT($prompt){
        $dataString = json_encode([
            "model" => "gpt-4",
            "messages" => [$prompt],
            "temperature" => 1,
        ]);

        $ch = curl_init('https://api.openai.com/v1/chat/completions');                                                      
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
        curl_setopt($ch, CURLOPT_POSTFIELDS, $dataString);                                                                  
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);     

        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'content-type:application/json',                                                                     
            'Authorization: Bearer ' . env("CHATGPT_API_SECRET"),                                                                                
            'OpenAI-Organization: ' . env("CHATGPT_API_ORGANIZATION")
        )                                                                       
        );                                                                                                                                                                        
        $response = curl_exec($ch);

        $response = json_decode($response, true);

        return $response;
    }
}
