<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use Kreait\Firebase\Database;
use Kreait\Firebase\Auth;
use Carbon\Carbon;
use Google\Cloud\Firestore\FirestoreClient;
use Kreait\Firebase\Exception\FirebaseException;
use Illuminate\Support\Facades\Validator;
use Socialite;
use Session;
use Redirect;
use Throwable;
use Exception;
use Mail;
use Image;

class FirebaseController extends Controller
{
    CONST DRIVER_TYPE = 'facebook';

    public function getLogin(){
        if(Session::get('user') !== null){
            return Redirect::to('/home');
        }
        $data = [
            "title" => "Venti | Log In"
        ];
        
        return view('auth.login', $data);
    }

    public function getRegister(){
        if(Session::get('user') !== null){
            return Redirect::to('/home');
        }
        $data = [
            "title" => "Venti | Create Account"
        ];

        return view('auth.register', $data);
    }

    public function login(Request $request){
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required',
        ]);

        $loginAttempsRemaining = Session::get('loginAttempsRemaining') ?? 10;

        if($loginAttempsRemaining == 0){
            return Redirect::to('/login')->withErrors(["general" => "Incorrect email or password"])->withInput();
        }

        if ($validator->fails()) {
            return Redirect::to('/login')->withErrors(["general" => "Incorrect email or password"])->withInput();
        }

        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $auth = app('firebase.auth');

        try{
            $signInResult = $auth->signInWithEmailAndPassword($request->email, $request->password);
            if(isset($signInResult)){
                $user = $auth->getUserByEmail($request->email);

                // store previous session vars related to searches

                $priorSearch = Session::get('flight-search');

                Session::flush();
                Session::regenerate();
                Session::put('sessionToken', $signInResult->idToken());
                Session::put('user', $user);
                Session::put('active', false);

                // restore previous session vars

                Session::put('flight-search', $priorSearch);


                // check to see if the user is an airlock user

                $customClaims = $auth->getUser($user->uid)->customClaims;
                $airlock = $customClaims["airlock"] ?? false;

                if($airlock === true){
                    return Redirect::to('/airlock/check-payment-status');
                }

                return Redirect::to('/home');
            }
        }
        catch(Exception $e){
            $loginAttempsRemaining--;
            Session::put('loginAttempsRemaining',$loginAttempsRemaining);
            return Redirect::to('/login')->withErrors(["general" => "Incorrect email or password. $loginAttempsRemaining login attemps remaining"])->withInput();
        }
        return Redirect::to('login')->withErrors(["general" => "There was an error signing you in. Please try again!"])->withInput();
    }

    public function logout(Request $request){
        Session::flush();
        Session::regenerate();

        return Redirect::to('/');
    }

    public function register(Request $request){
        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $auth = app('firebase.auth');
        $photoUrl = '/assets/img/profile.jpg';

        // validation

        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'email' => 'required|max:255',
            'username' => 'required|min:6|max:255',
            'password' => 'required|min:8|',
        ]);

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        try{
            $userProperties = [
                'email' => $request->email,
                'emailVerified' => false,
                'password' => $request->password,
                'displayName' => $request->name,
                'photoUrl' => $photoUrl,
                'disabled' => false
            ];

            $createdUser = $auth->createUser($userProperties);

            // create profile in user collection

            $auth->setCustomUserClaims($createdUser->uid, [
                'username' => strtolower($request->username),
                'dob' => $request->dob,
                'smoking' => "none",
                "drinking" => "none",
                "requests" => "all",
                "gender" => "none",
                'genderPref' => "none",
                'sizePref' => "none",
                'agePref' => "none",
                'spendingPref' => "none",
                'smokingPref' => "none",
                'drinkingPref' => "none",
                "smsVerified" => false,
                "religion" => "none",
                "languages" => ["English"],
                "timezone" => "America/New_York",
                "interests" => []
            ]);

            // don't trust the data coming in. fix for later otherwise usernames can get stolen

            $docRef = $db->collection("Usernames")->document($request->username)->set(
                    [
                        "uid" => $createdUser->uid
                    ]
            );

            // try processing the image:

            if($request->file("avatar") !== null){

                //dd($request->file("avatar"));
                $storage =  app('firebase.storage');
                $defaultBucket = $storage->getBucket();
                $file = $request->file("avatar");
                $filename = rand(999,9999999) . "-" . cleanFileName($file->getClientOriginalName());
                $extension = strtolower($request->file("avatar")->getClientOriginalExtension());

                $file = $this->shrinkImage($file, true);

                $uploadResult = $defaultBucket->upload(
                    
                    file_get_contents($file), [
                        'name' => $filename,
                        'uploadType'=> "media",
                        'predefinedAcl' => 'publicRead',
                        "metadata" => ["contentType"=> 'image/' . $extension],
                    ]
                );

                $mediaLink = $uploadResult->info()["mediaLink"];

                $properties = [
                    'photoUrl' => $mediaLink
                ];

                $updatedUser = $auth->updateUser($createdUser->uid, $properties);
            }

            $createdUser = $auth->getUser($createdUser->uid);

            Session::put('user', $createdUser);
            Session::put('active', true);

            try{
                $auth->sendEmailVerificationLink($createdUser->email);
                $this->submitFacebookConversionAPI($createdUser->email);

            } catch (FirebaseException $e) {
                
            }
            
            Session::flash('newuser', 'yes');

            return Redirect::to('/user/profile');
        }
        catch(FirebaseException $e){

            return Redirect::back()->withErrors(["This email is already in use"]);
        }
    }

    public function requestResetPassword(Request $request){
        $user = Session::get('user');
        $auth = app('firebase.auth');

        if($user === null){
            return 500;
        }

        try{
            $user = $auth->getUserByEmail($user->email);

            if(isset($user)){
                try{
                    $auth->sendPasswordResetLink($user->email);
                    return 200;
                }
                catch(FirebaseException $e){
                    // error due to too many attempts
                    return 500;
                }
                    
            }
        }
        catch(FirebaseException $e){
            return 500;
        }
    }

    public function requestResetPasswordAuth(Request $request){
        $auth = app('firebase.auth');

        try{
            $user = $auth->getUserByEmail($request->email);

            if(isset($user)){
                try{
                    $auth->sendPasswordResetLink($user->email);
                    Session::flash('status', 'If an account exists for this email, you will receive a password reset link soon. Check your spam folder if it takes longer than normal to appear in your inbox.');
                    return Redirect::back();
                }
                catch(FirebaseException $e){
                    // error due to too many attempts
                    return Redirect::back();
                }
                    
            }
        }
        catch(FirebaseException $e){
            Session::flash('status', 'If an account exists for this email, you will receive a password reset link soon. Check your spam folder if it takes longer than normal to appear in your inbox.');
            return Redirect::back();
        }
    }

    public function resetPassword(Request $request){
        $auth = app('firebase.auth');
        $token = $request->token;
        $email = $request->email;

        $validator = Validator::make($request->all(), [
            'token' => 'required',
            'password' => 'required|min:8|confirmed',
        ]);

        if ($validator->fails() || $request->token != $token) {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        try{
            $user = $auth->getUserByEmail($email);

            if(isset($user)){
                $updatedUser = $auth->changeUserPassword($user->uid, $request->password);
                
                Session::flush();
                Session::regenerate();
                
                Session::put('active', true);
                Session::put('user', $user);

                Session::flash("reset-success", "Your password as been reset!");

                return Redirect::to('/user/profile');
            }
        }
        catch(FirebaseException $e){
            return Redirect::back()->withErrors(["email" => "This email is not valid"]);
        }

        return Redirect::back()->withErrors(["email" => "There was an error updating your email."]);
    }



    public function handleFacebookRedirect()
    {
        return Socialite::driver(static::DRIVER_TYPE)->fields(['name','email'])->scopes(['email'])->redirect();

    }

    public function handleFacebookCallback()
    {
        // initiate firebase user check
        $auth = app('firebase.auth');
        $firestore = app('firebase.firestore');
        $db = $firestore->database();

        try {
            $user = Socialite::driver(static::DRIVER_TYPE)->fields(['name','email'])->scopes(['email'])->user();

            Session::put('fb', $user);

            if(isset($user->email)){
                try{
                    $existingUser = $auth->getUserByEmail($user->email);

                    if(isset($existingUser->email)){
                        Session::put('user', $existingUser);
                        return Redirect::to('/groups');
                    }
                }
                catch(FirebaseException $e){
                    $dob = null;
                    if(array_key_exists("birthday", $user->user)){
                      $dob = Carbon::createFromFormat('m/d/Y', $user->user["birthday"])->format('Y-m-d');
                    }
                
                    $request = \Kreait\Firebase\Request\CreateUser::new()
                        ->withUid($user->id)
                        ->withUnverifiedEmail($user->email)
                        ->withDisplayName($user->name)
                        ->withPhotoUrl('https://graph.facebook.com/v3.3/' . $user->id . '/picture?width=400');

                    $createdUser = $auth->createUser($request);

                    $username = $this->findOpenUsername($user->email);

                    $docRef = $db->collection("Usernames")->document($username)->set(
                        [
                            "uid" => $createdUser->uid
                        ]
                    );

                    $auth->setCustomUserClaims($createdUser->uid, [
                        'username' => $username,
                        'smoking' => "none",
                        "drinking" => "none",
                        "requests" => "all",
                        "dob" => $dob,
                        "gender" => "none",
                        'genderPref' => "none",
                        'sizePref' => "none",
                        'agePref' => "none",
                        'spendingPref' => "none",
                        'smokingPref' => "none",
                        'drinkingPref' => "none",
                        "smsVerified" => false,
                        "religion" => "none",
                        "languages" => ["English"],
                        "interests" => []
                    ]);

                    $createdUser = $auth->getUser($createdUser->uid);

                    Session::put('user', $createdUser);
                    Session::put('active', true);

                    try{
                        $auth->sendEmailVerificationLink($user->email);
                        $this->submitFacebookConversionAPI($user->email);
                    }
                    catch (FirebaseException $e) {
                
                    }

                    
                    
                    Session::flash('newuser', 'yes');

                    return Redirect::to('/user/profile');
                }
            }
            return Redirect::to('register')->withErrors(["fb" => "There was an error. Please try again or select a different sign-up method"]);
        } catch (Exception $e) {
            Session::flush();
            Session::regenerate();
            
            return Redirect::to('register')->withErrors(["fb" => "There was an error. Please try again or select a different sign-up method"]);
        }

    }

    public function facebookLogin(){
        $fb = Session::get('fb');
        $dob = Carbon::createFromFormat('m/d/Y', $fb["birthday"])->format('Y-m-d');
    }

    public function findOpenUsername($email){
        $username = explode("@",$email)[0];
        $username = strtolower(substr($username,0,9));
        $username = str_replace("-","", $username);
        $username = str_replace(".","", $username);
        $username = str_replace("_","", $username);
        $username = $username . rand(99,9999);

        return $username;
    }

    public function shrinkImage($OGimage, $crop = false){
        $image = Image::make($OGimage)->orientate();
        if($crop){
            $image->fit(300);
        } else{
            $image->resize(300, null, function ($constraint) {
                $constraint->aspectRatio();
            });
        }

        $extension = $OGimage->getClientOriginalExtension();
    
        $thumbnailPath = public_path() .'/thumbnail/';
        $fileName = $thumbnailPath.time(). ".jpg";
        $image->save($fileName);
    
        return $fileName;
    }

    public function submitFacebookConversionAPI($email){
        $data = [ // main object
            "data" => [ // data array
                [
                    "event_name" => "CompleteRegistration",
                    "event_time" => time(),
                    "user_data" => [
                        "em" => hash('sha256', strtolower($email))
                    ],
                    "action_source" => "website"
               ],
            ],
            
            "access_token" => env('FACEBOOK_CONVERSION_TOKEN')
        ];          
    
        $dataString = json_encode($data);
        // pixel ID: 1169331916805166
        $ch = curl_init('https://graph.facebook.com/v11.0/1169331916805166/events');                                                                      
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
        curl_setopt($ch, CURLOPT_POSTFIELDS, $dataString);                                                                  
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
            'Content-Type: application/json',                                                                                
            'Content-Length: ' . strlen($dataString))                                                                       
        );                                                                                                                                                                       
        $response = curl_exec($ch);
    }
}
