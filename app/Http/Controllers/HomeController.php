<?php

namespace App\Http\Controllers;

use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use Kreait\Firebase\Database;
use Kreait\Firebase\Auth;
use Google\Cloud\Firestore\FirestoreClient;
use Kreait\Firebase\Exception\FirebaseException;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Illuminate\Support\Facades\Http;

use App\Models\Airport as Airports;
use App\Models\Country as Countries;

use Illuminate\Http\Request;
use Redirect;
use Dwolla;
use Session;

use \Stripe\Stripe as Stripe;
use \Stripe\StripeClient as StripeClient;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('firebase.auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function index(){

        // check if maintenance mode is engaged.

        if(env('APP_DOWN') == "true"){
            return view('errors.300');
        }

        // we need to check to see if they have any onboarding steps

        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $auth = app('firebase.auth');
        $user = Session::get('user');

        if(!array_key_exists("timezone", $user->customClaims)){
            // set timezone to new york
            $customClaims = $user->customClaims;
            $customClaims["timezone"] = "America/New_York";
            try{
                $auth->setCustomUserClaims($user->uid, $customClaims);
            }
            catch(FirebaseException $e){

            }

        }

        try{
            $user = $auth->getUserByEmail($user->email);

            if(!$user->emailVerified){
                return Redirect::to('/boardingpass/onboarding/email');
            }

            $customClaims = $user->customClaims ?? false;

            if(!$customClaims["smsVerified"]){
                return Redirect::to('/boardingpass/onboarding/phone');
            } else{

                // check if the current session has 2FA'd

                $active = Session::get('active') ?? false;
                if(env("APP_ENV") == "production"){
                    if(!$active){
                        return Redirect::to('/auth/mfa');
                    }
                }
            }

            // the user has completed all the basic steps to create a Venti Account
        }
        catch(FirebaseException $e){
            // error page for when we cannot find the account
        }

        // get customer data

        $subscription = $user->customClaims["subscription"] ?? "buddy";

        $pro = false;

        if($subscription == "pro"){
            $pro = true;
        }

        if($pro){
            return Redirect::to('/pro');
        }

        $dwolla = new Dwolla;

        $customer = $dwolla->getCustomer($user, $auth, $db);

        $fundingSources = [];
        $fundingSourcesCount = 0;

        $transactable = $wallet = $secret = $appPaid = false;



        if($customer){
            $fundingSources = [];  //$dwolla->fundingSources($customer["customerID"]);

            $appPaid = $user->customClaims["appPaid"] ?? false;

            if($fundingSources){
                foreach($fundingSources as $fundingSource){
                    if(!$fundingSource->removed 
                        && isset($fundingSource->bankAccountType)
                        && $fundingSource->status == "verified"
                    ){
                        // this user can now make deposits and withdrawals using their bank account

                        $transactable = true;
                        $fundingSourcesCount++;
                    }
                    if($fundingSource->type == "balance"){
                        $wallet = explode("funding-sources/",$fundingSource->_links->self->href)[1];
                    }
                    if(!$fundingSource->removed 
                        && isset($fundingSource->bankAccountType)
                        && $fundingSource->status == "unverified"
                    ){
                        $fundingSourcesCount++;
                    }
                }
                // account balances
            }
        }

        else{
            // we need to see if the end user has already paid their application fee

            $appPaid = $user->customClaims["appPaid"] ?? false;

            if(!$appPaid){
                // the customer has not paid their fee

                // we have not created a customer profile for them yet. We will need to collect their fee.

                // skip this if the type is first class or buddy pass


                if($subscription != "buddy"){
                    $secret = env("STRIPE_SECRET_" . strtoupper(env("APP_ENV")));
                    $stripe = new StripeClient($secret);



                    $intent = $stripe->paymentIntents->create([
                      'amount' => env("VENTI_" . strtoupper($subscription) . "_ANNUAL_FEE"), // 9.99 USD
                      'currency' => 'usd',
                      'statement_descriptor_suffix' => 'Venti',
                      'statement_descriptor' => 'Account Verification',
                      'automatic_payment_methods' => ['enabled' => true],
                    ]);

                    $secret = $intent->client_secret;
                }

                
            }
        }

        // see if warp drive is available

        $canPointsPurchasePoints = $this->canPointsPurchasePoints($user->uid, $db);


        // we are pulling the subscription value from the customClaims record in firebase
        // for those that signed up before Jan 1, they will not have a subscription attribute
        // by default, we will treat them as interested in buddy pass

        // get age of the account

        $createdAt = $user->metadata->createdAt->format("Y-m-d");

        $creationDate = new Carbon($createdAt);

        $accountAge = $creationDate->diffInDays(Carbon::now());

        $ranking = $user->customClaims["leaderboard"] ?? "X";

        $data = [
            "title" => "Boarding Pass",
            "description" => "Traveling for free just got whole lot easier.",
            "image" => "https://venti.co/assets/img/boarding-pass-social.jpg",
            "url" => "https://venti.co/boardingpass",
            "user" => $user,
            "customer" => $customer,
            "fundingSources" => $fundingSources,
            "transactable" => $transactable,
            "transactions" => [],
            "wallet" => $wallet,
            "points" => 0,
            "secret" => $secret,
            "appPaid" => $appPaid ? "true" : "false",
            "canPointsPurchasePoints" => $canPointsPurchasePoints,
            "subscription" => $subscription,
            "upgrade" => "false",
            "accountAge" => $accountAge,
            "fundingSourcesCount" => $fundingSourcesCount,
            "ranking" => $ranking
        ];

        return view('boardingpass.home', $data);
    }

    public function browse()
    {
        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $auth = app('firebase.auth');
        $docRef = $db->collection("Ideas");
        $ideas = $docRef->listDocuments();
        $results = [];

        foreach($ideas as $idea){
            array_push($results, $idea->snapshot()->data());
        }

        $data = [
            "ideas" => $results
        ];

        return view('dashboard', $data);
    }

    public function read($trip)
    {
        
    }

    public function dashboard(){
        
    }

    public function create(){
        return view('dashboard.new');
    }

    public function add(Request $request){

        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        
        $foodBudget  = $request->foodBudget; 
        $lodgingBudget = $request->lodgingBudget; 
        $transportationBudget = $request->transportationBudget; 
        $activityBudget = $request->activityBudget; 
        $budgetOverall = $foodBudget + $lodgingBudget + $transportationBudget + $activityBudget;

        $tags = [];

        if($request->tag != null && $request->tag != ""){
            $tags = $request->tag;
        }

        $departure = Carbon::createFromFormat('Y-m-d', $request->departure)->format("m/d/Y");

        $docRef = $db->collection("Ideas")->document($request->tripID)->set(
            [             
                "tripID" => $request->tripID,
                "archived" => $request->archived,
                "budgetOverall" => $budgetOverall,
                "country" => $request->country,
                "latitude" => $request->latitude,
                "longitude" => $request->longitude,
                "destination" => $request->destination,
                "title" => $request->title,
                "departure" => $departure,
                "days" => $request->days,
                "foodBudget" => $foodBudget,
                "lodgingBudget" => $lodgingBudget,
                "transportationBudget" => $transportationBudget,
                "activityBudget" => $activityBudget,
                "tags" => $request->tags,
                "imageURL" => $request->imageURL,
                "imageAuthor" => $request->imageAuthor,
                "imageAuthorProfile" => $request->imageAuthorProfile,
                "personalizationCost" => $request->personalizationCost,
                "purchasePrice" => $request->purchasePrice,
                "purchaseLink" => $request->purchaseLink,
                "itineraryURL" => $request->itineraryURL,
                "previewURL" => $request->previewURL,
                "pdf" => $request->pdf,
                "itineraryNotes" => $request->itineraryNotes,
                "author" => $request->author,
                "authorBio" => $request->authorBio,
                "authorImage" => $request->authorImage,
                "description" => $request->description,
                "worldee" => $request->worldee_key,
                "itinerary" => []
            ]
        );

        return Redirect::to('/dashboard/edit/' . $request->tripID);
    }

    public function builder(){
        return view('dashboard.builder');
    }

    public function pointsBalance(){
        
    }

    public function customerDetails(Request $request){

        if($request->wallet == false || $request->wallet == "false"){
            // customer account does not exist
            return json_encode([
                "cashBalance" => 0,
                "cashHTML" => '<h1 style=" color:white; font-size: 275%; margin: 0">$0</h1><span style="font-weight: 500; color: white;">Cash Balance</span>',
                "spendingPowerHTML" => '<h1 style=" color:white; font-size: 275%; margin: 0">$0</h1><span style="font-weight: 500; color: white;">Spending Power</span>',
                "pointsHTML" => '<h1 style=" color:white; font-size: 275%; margin: 0">0</h1><span style="font-weight: 500; color: white;">Points</span>',
                "withdrawButtonHTML" => '<a href="#" data-bs-toggle="modal"><h1><img src="/assets/img/icons/bank.png" style="width: 100%; max-width:55px;"></h1><span style="font-weight: 500;">Withdraw</span>',
                "pointsBalance" => 0,
                "spendingPower" => 0
            ]);
        }

        

        $wallet = env("DWOLLA_URL_" . strtoupper(env('APP_ENV'))) . "/funding-sources/" . $request->wallet;
        // wallet URL


        

        /*
            

            @if(!$wallet)
                            <h1 style="color:white; font-size: 275%; margin:0;">0.00</h1>
                        @else
                            <h1 style="color:white; font-size: 275%; margin:0">${{ $wallet["balance"] }}</h1>
                        @endif<span style="font-weight: 500; color: white;">Cash Balance</span>


            <h1 style=" color:white; font-size: 275%; margin: 0">{{ number_format($points,2) }}</h1><span style="font-weight: 500; color: white;">Points</span>

            @if(!$wallet || $wallet['balance'] <= 0) disabled @endif

            
        */

            $dwolla = new Dwolla;

            $walletDetails = $dwolla->getFundingSourceBalance($wallet);

            $wallet = $transactions = [];

            $wallet["balance"] = (float) $walletDetails->balance->value;
            $wallet["last_updated"] = \Carbon\Carbon::parse($walletDetails->last_updated)->format("m-d-Y H:i:s");
        
            $points = 0;

            $firestore = app('firebase.firestore');
            $db = $firestore->database();
            $user = Session::get('user');

            $transactionDocs = $db->collection("Transactions");
            $transactionDocs = $transactionDocs
                ->where("uid","=",$user->uid)
                ->documents();
            // I need a separate firebase instance for sandboxing

            foreach($transactionDocs as $transactionDoc){
                array_push($transactions,  $transactionDoc->data());
            }

            $points = getPointsBalance($transactions);

            $spendingPowerHTML = '<h1 style=" color:white; font-size: 275%; margin: 0">$' . number_format($wallet["balance"] + $points,2) . '</h1><span style="font-weight: 500; color: white;">Spending Power</span>';

            $cashHTML = '<h1 style=" color:white; font-size: 275%; margin: 0">$' . number_format($wallet["balance"],2) . '</h1><span style="font-weight: 500; color: white;">Cash Balance</span>';
            $pointsHTML = '<h1 style=" color:white; font-size: 275%; margin: 0">' . number_format($points,2) . '</h1><span style="font-weight: 500; color: white;">Points</span>';

            $withdrawButtonHTML = '<a href="#" data-bs-toggle="modal" data-bs-target="#withdrawFunds"><h1><img src="/assets/img/icons/bank.png" style="width: 100%; max-width:55px;"></h1><span style="font-weight: 500;">Withdraw</span>
                            </a>';

            return json_encode([
                "cashBalance" => $wallet["balance"],
                "cashHTML" => $cashHTML,
                "spendingPowerHTML" => $spendingPowerHTML,
                "pointsHTML" => $pointsHTML,
                "withdrawButtonHTML" => $withdrawButtonHTML,
                "pointsBalance" => $points,
                "spendingPower" => (float) $wallet["balance"] + $points
            ]);

    }

    public function transactionsTable(Request $request){
        $user = Session::get('user');
        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $auth = app('firebase.auth');

        // check to see if the logged in user matches the user id
        // or if the user is an admin

        if(!isAdmin($user)){
            if($user->uid != $request->user){
                return json_encode(500);
            }
        }



        $transactionDocs = $db->collection("Transactions");
        $transactionDocs = $transactionDocs
        ->where("uid","=",$request->user)
        ->documents();
        // I need a separate firebase instance for sandboxing

        $transactions = ["data" => []];

        foreach($transactionDocs as $transactionDoc){
            $data = $transactionDoc->data();

            $timestamp = \Carbon\Carbon::parse($data["timestamp"])->timezone($user->customClaims["timezone"])->format("M. d, Y");
            $ampm = \Carbon\Carbon::parse($data["timestamp"])->timezone($user->customClaims["timezone"])->format("h:i A");
            $strtotime = \Carbon\Carbon::parse($data["timestamp"])->timezone($user->customClaims["timezone"])->timestamp;
            $category = "Transfer";

            $type = "";
            $negative = false;

            if($data["type"] == "points-deposit" || $data["type"] == "points-withdraw"){
                $category = "Points";
            }

            if($data["type"] == "withdraw" || $data["type"] == "points-withdraw"){
                $negative = true;
            }

            $total = number_format($data["total"],2);

            if($category != "Points"){
                $type = "<br> Type: ACH " . ucfirst($data["type"]);
                $total = "$" . $total;
            }

            if($negative){
                // we show a negative sign for withdraw transactions
                $total = "-$total";
            }

            $status = ucfirst($data["status"]);

            $transactionID = explode("transfers/",$data['transactionUrl'])[1] ?? "";

            $btnClass = "";

            if($status == "Pending"){
                if(!str_contains($data['note'], "Fee")){
                    $btnClass = "text-warning";
                    
                }else{
                    $btnClass = "text-success";
                    $status = 'Complete';
                }
                
            }
            if($status == "Complete" || $status == "Canceled"){
                $btnClass = "text-success";
            }

            array_push($transactions["data"], [
                "description" => $data["note"] . '<br>ID: ' . $data["transactionID"],
                "category" => $category,
                "from" => $data["from"],
                "to" => $data["to"],
                "total" => $total,
                "timestamp" => "<span style='display:none'>" . $strtotime  . "</span>" . $timestamp . "<br> $ampm",
                "status" => $status,
                "action" => '<button 
                                title="View Receipt" 
                                class="btn btn-white btn-generate-receipt-preview ' . $btnClass . '" 
                                style="background:transparent;"
                                href=";javascript"
                                type="button"
                                data-bs-toggle="modal"
                                data-bs-target="#receiptModal"
                                data-category="' . $category . '"
                                data-amount="' . $data["amount"] . '"
                                data-transaction-id="' . $data['transactionID'] . '"
                                data-note="' . $data['note'] . '"
                                data-from="' . $data['from'] . '"
                                data-to="' . $data['to'] . '"
                                data-type="' . ucfirst($data['type']) . '"
                                data-total="' . $data['total'] . '"
                                data-fee="' . number_format($data['fee'],2) . '"
                                data-timestamp="' . $timestamp . '"
                                data-ampm="' . $ampm . '"
                                data-status="' . ucfirst($data['status']) . '"
                                ><i class="fa fa-file-invoice"></i></button>'
            ]);
        }

        return json_encode($transactions);
    }

    public function snapshotsTable(Request $request){
        $user = Session::get('user');
        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $auth = app('firebase.auth');

        
        // check to see if the logged in user matches the user id
        // or if the user is an admin

        if(!isAdmin($user)){
            if($user->uid != $request->user){
                return json_encode(500);
            }
        }

        $transactionDocs = $db->collection("BalanceSnapshots");
        $transactionDocs = $transactionDocs
        ->where("uid","=",$request->user)
        ->where("env","=",env("APP_ENV"))
        ->where("reconciled","=",true)
        ->where("statement","=",true)
        ->documents();
        // I need a separate firebase instance for sandboxing

        $transactions = ["data" => []];

        foreach($transactionDocs as $transactionDoc){
            $data = $transactionDoc->data();

            $timestamp = \Carbon\Carbon::parse($data["timestamp"])->timezone($user->customClaims["timezone"])->format("M. d, Y");

            $ampm = \Carbon\Carbon::parse($data["timestamp"])->timezone($user->customClaims["timezone"])->format("h:i A");

            $strtotime = \Carbon\Carbon::parse($data["timestamp"])->timezone($user->customClaims["timezone"])->timestamp;

            $points = $data["points"] ?? 0;

            array_push($transactions["data"], [
                "description" => "<strong>" . $data["month"] . " " .  $data["year"] . " Account Statement</strong><br>Statement ID: " . $data["snapshotID"],
                "balance" => "$" . number_format((float) $data["balance"],2),
                "points" => number_format((float) $points,2),
                "timestamp" => "<span style='display:none'>" . $strtotime  . "</span>" . $timestamp,
                "action" => '<a href="/home/statements/snapshot/' . $data["snapshotID"] . '" class="btn" target="_blank"><i class="fa fa-file-invoice"></i></a>'
            ]);
        }

        return json_encode($transactions);
    }

    public function customerShare(){
        $user = Session::get('user');

        // we check to see if they're in the crm

        $subscribers =  $this->subscribers();
        $me = $id = null;

        foreach($subscribers as $contact){
            $fields = $contact["fields"];
            // only push those that have a source in their fields

            if(array_key_exists("Source", $fields) && $fields["Source"] != null){

                if($user->email == $contact["email"]){
                    $me = $contact;
                    $id = getEmailID($contact["id"]);
                    break;
                }
            }
        }

        if(is_null($me)){
            // create and set a random invite key
            $code = $this->userReferralKey($user, null);
        }
        else {
            // 
            // set the existing email octopus key to the user record if the customclaim object doesn't exist
            $customClaims = $user->customClaims;

            if(!array_key_exists("referralKey", $customClaims)){
                $this->userReferralKey($user, $id);
            }

            $code = $id;
        }

        return json_encode(env("APP_URL") . "/s/" . $code);
    }

    public function subscribers(){
        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $auth = app('firebase.auth');

        $subscribersList = $db->collection("VIPS")->documents();

        $subscribers = [];

        foreach($subscribersList as $contact){
            $contact = $contact->data();
            $fields = $contact["fields"];

            array_push($subscribers, [
                "email" => $contact["email_address"],
                "fields" => $fields,
                "id" => $contact["id"]
            ]);
        }

        return $subscribers;
    }

    public function userReferralKey($user, $key = null){
        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $auth = app('firebase.auth');
        $user = $auth->getUserByEmail($user->email);
        $customClaims = $user->customClaims;


        if(!$key){
            // we're creating a new key
            $key = generateRandomString(12);
            
        }
        // add key to customClaims
        $customClaims["referralKey"] = $key;
        $auth->setCustomUserClaims($user->uid, $customClaims);

        return $key;
    }

    public function viewStatement($id){
        $user = Session::get('user');

        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $auth = app('firebase.auth');


        $transactionDocs = $db->collection("BalanceSnapshots");
        $transactionDocs = $transactionDocs
        ->where("uid","=",$user->uid)
        ->where("snapshotID","=",$id)
        ->documents();

        $data = "";

        foreach($transactionDocs as $transactionDoc){
            $data = $transactionDoc->data();
        }

        $userTimezone = $user->customClaims["timezone"] ?? "America/New_York";

        $timestamp = \Carbon\Carbon::parse($data["timestamp"])
        ->timezone($userTimezone)->format("M. d, Y");

        $ampm = \Carbon\Carbon::parse($data["timestamp"])->timezone($userTimezone)->format("h:i A");

        $strtotime = \Carbon\Carbon::parse($data["timestamp"])->timezone($userTimezone)->timestamp;

        $year = \Carbon\Carbon::parse($data["timestamp"])->timezone($userTimezone)->format("Y");
        
        $month = \Carbon\Carbon::parse($data["timestamp"])->timezone($userTimezone)->format("m");

        // get all transaction within the statement period

        //dd([$timestamp, $strtotime, $ampm, $year, $month]);

        $lastMonth = \Carbon\Carbon::parse($data["timestamp"])->timezone($userTimezone)->subMonth()->timestamp;

        $transactions = $db->collection("Transactions");
        $transactions = $transactions
        ->where("uid","=", $user->uid)
        ->orderBy("timestamp", "DESC")
        ->documents();
        //

        $statementTransactions = [];

        $transactionValue = $pointsValue = 0;

        foreach($transactions as $transaction){
            $transaction = $transaction->data();

            if($transaction["timestamp"] >= $lastMonth && $transaction["timestamp"] <= $strtotime){
                array_push($statementTransactions, $transaction);

                $negative = false;

                if($transaction["type"] == "deposit" || $transaction["type"] == "withdraw"){
                    $amount = $transaction["amount"];

                    if($transaction["type"] == "withdraw"){
                        $amount *= -1;
                    }

                    $transactionValue += $amount;
                }

                if($transaction["type"] == "points-deposit" || $transaction["type"] == "points-withdraw"){

                    $amount = $transaction["amount"];

                    if($transaction["type"] == "points-withdraw"){
                        $amount *= -1;
                    }

                    $pointsValue += $amount;
                }
            }
        }

        usort($statementTransactions, function($a, $b) {
            return $a['timestamp'] <=> $b['timestamp'];
        });

        // get a sum of the deposits/withdraws



        $data = [
            "id" => $month,
            "data" => $data,
            "transactions" => $statementTransactions,
            "transactionValue" => number_format($transactionValue,2),
            "pointsValue" => number_format($pointsValue,3),
            "timezone" => $userTimezone,
            "month" => $data["month"],
            "year" => $data["year"]
        ];

        return view("boardingpass.panels.transactions.statement", $data);
    }

    public function canPointsPurchasePoints($uid, $db, $transactions = null){
        $transactions = $db->collection("Transactions")
            ->where("type","=","points-deposit")
            ->where("uid","=",$uid)
            ->orderBy("timestamp","DESC")
            ->documents();

        $now = \Carbon\Carbon::now();

        $numTransactions = 0;

        foreach($transactions as $transaction){
            $transaction = $transaction->data();
            if(str_contains($transaction["note"], "Points Purchase") && $transaction["from"] == "Venti"){
                $transactionDif = \Carbon\Carbon::parse($transaction["timestamp"])->diffInDays($now);

                if($transactionDif < 7){
                    return false;
                }
            }
            $numTransactions++;
        }

        return true;
    }

    public function viewUpgrade($subscription){
        $user = Session::get('user');

        $currentSubscription = $user->customClaims["subscription"] ?? "buddy";

        if($subscription == $currentSubscription){
            // can't upgrade to your own subscription
            return Redirect::back();
        }

        $secret = env("STRIPE_SECRET_" . strtoupper(env("APP_ENV")));
        $stripe = new StripeClient($secret);

        $intent = $stripe->paymentIntents->create([
          'amount' => env("VENTI_" . strtoupper($subscription) . "_ANNUAL_FEE"), // 9.99 USD
          'currency' => 'usd',
          'statement_descriptor_suffix' => 'Venti',
          'statement_descriptor' => 'Account Verification',
          'automatic_payment_methods' => ['enabled' => true],
        ]);

                    $secret = $intent->client_secret;

        $data = [
            "user" => $user,
            "subscription" => $subscription,
            "retry" => false,
            "customer" => false,
            "appPaid" => "false",
            "secret" => $secret,
            "fundingSources" => [],
            "wallet" => false,
            "upgrade" => "true"
        ];

        return view("boardingpass.upgrade", $data);
    }
}
