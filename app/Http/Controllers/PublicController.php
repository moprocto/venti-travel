<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use Redirect;

class PublicController extends Controller
{
    public function index(Request $request){
        if($request->fbid == "fb"){
            Session::put('referer', "facebook");
        }
        
        return view('tailwind.index');
    }

    public function referral($referral){
        return Redirect::to('/boardingpass?fbid=' . $referral);
    }

    public function twentyCountries(){
        $data = [
            "title" => "20 Countries for \$20,000",
            "description" => "A single payment plan that unlocks the world for a small monthly fee",
            "image" => "https://venti.co/assets/img/navigator-backdrop-sm.jpg",
            "url" => "https://venti.co/20-countries"
        ];
        
        return view('20countries', $data);
    }

    public function navigatorProgram(){

        return Redirect::to('/');

        $data = [
            "title" => "Get Paid to Travel",
            "description" => "Earn up to $500+ per trip when you organize trips as a Venti Navigator.",
            "image" => "https://venti.co/assets/img/navigator-backdrop-sm.jpg",
            "url" => "https://venti.co/navigator"
        ];

        return view('navigator.index', $data);
    }

    public function excursionProgram(){
        $data = [
            "title" => "Networking Meets Travel",
            "description" => "Connect with accomplished professionals in your industry while exploring new places.",
            "image" => "https://venti.co/assets/img/excursion-backdrop-sm.jpg",
            "url" => "https://venti.co/excursions"
        ];

        return view('excursion.index', $data);
    }
}
