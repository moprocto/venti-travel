<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

use Session;
use Location;
use Redirect;
use Mail;
use Exception;
use Validator;

class BusinessController extends Controller
{
    public function index(){
        $data = [
            "title" => "AI-Driven Rewards for Financial Institutions",
            "description" => "Financial institutions and lenders use Venti to offer their members 30+ different ways to earn rewards.",
            "url" => "https://venti.co/enterprise",
            "image" => "https://venti.co/assets/img/navigator-backdrop-t7.jpg",
        ];

        return view('business.enterprise', $data);
    }

    public function success(Request $request){

        $email = $request->email;
        $to = $request->name;

        if($email == null && $email == ""){
            return json_encode(500);
        }

        $email = $email;
        $subject = "[AI Rewards Automation] Inquiry Confirmation";
        $from = "no-reply@venti.co";
        $message = "";
        $data = [
            "company" => $request->company,
            "name" => $to,
            "email" => $email,
            "website" => $request->website
        ];

        Mail::send('emails.application.business', $data, function($message) use($email , $subject, $from, $to)
        {
            $message
                ->to($email, $to)
                ->bcc("markus@venti.co", "Markus Proctor")
                ->from($from, "Venti")
                ->subject($subject); 
        });

        return json_encode(200);

        $source = Session::get('referrer') ?? "direct";
        $ip = \Request::ip();
        $location = Location::get($ip);
        $agent = $request->header('User-Agent');
        $region = "none";

        if(isset($location) && !is_bool($location)){
            $region = $location->regionName;
        }

        $fields = [
            "Source" => $source,
            "IP" => $ip ?? "none",
            "Location" => $location ? $region : "none",
            "Company" => $request->company,
            "Hires" => $request->hires,
            "Points" => $request->points,
            "Preference" => $request->preference
        ];

        $result = Http::post("https://emailoctopus.com/api/1.6/lists/6c5be602-0aef-11ef-9067-5f3bee8c335a/contacts", [
            'api_key' => env('EMAIL_OCTOPUS_API_KEY'),
            'email_address' => $email,
            "fields" => $fields
        ]);

        $result = json_decode($result->body(),true);


        if(!array_key_exists("error",$result)){
            // add contact to email octopus
            // send the success message to facebook
            if(env('APP_ENV') != "local"){
                $this->submitFacebookConversionAPI($email, $ip, $location, $agent);
            } else {
                //$this->submitFacebookConversionAPITEST($email, $ip, $location, $agent);
            }

            $id = getEmailID($result["id"]);

            $data = [
                "title" => "AI-Driven Rewards for Financial Institutions",
                "description" => "Financial institutions and lenders use Venti to offer their members 30+ different ways to earn rewards.",
                "url" => "https://venti.co/enterprise",
                "image" => "https://venti.co/assets/img/navigator-backdrop-t7.jpg",
                "referral" => $id
            ];

            // send email

            try{

                // try adding to firebase

                $firestore = app('firebase.firestore');
                $db = $firestore->database();
                $auth = app('firebase.auth');

                $db->collection("VIPS")->document($id)->set($result);

            }
            catch(Exception){

            }

            return json_encode(200);
        }
        
        return json_encode(500);
    }

    public function submitFacebookConversionAPI($email, $ip = null, $location = null, $agent = null){
        

        $userdata = [
            "em" => hash('sha256', strtolower($email))
        ];

        if($ip != "none" && !is_null($ip)){
            $userdata["client_ip_address"] = $ip;
        }

        if(!is_null($agent)){
            $userdata["client_user_agent"] = $agent;
        }

        if(!is_null($location)){
            $userdata["country"] = hash('sha256', strtolower($location->countryName));
            $userdata["st"] = hash('sha256', strtolower($location->regionName));
            $userdata["zp"] = hash('sha256', strtolower($location->zipCode));
        }

        if(Session::get('fbclid') !== null){
            // add the fbclick id to the pixel conversion
            $userdata["fbc"] = "fb.1." . (int) (microtime(true) * 1000) . "." . Session::get('fbclid');
        }

        $data = [ // main object
            "data" => [ // data array
                [
                    "event_name" => "Lead",
                    "event_time" => time(),
                    "user_data" => $userdata,
                    "action_source" => "website"
               ],
            ],
            
            "access_token" => env('FACEBOOK_CONVERSION_TOKEN')
        ];
    
        $dataString = json_encode($data);
        // pixel ID: 1169331916805166
        $ch = curl_init('https://graph.facebook.com/v11.0/1169331916805166/events');                                                                      
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
        curl_setopt($ch, CURLOPT_POSTFIELDS, $dataString);                                                                  
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
            'Content-Type: application/json',                                                                                
            'Content-Length: ' . strlen($dataString))                                                                       
        );                       


        // clear fbclic

        Session::flush();                                                                                                                                                
        $response = curl_exec($ch);

    }

    public function playground(){

        $data = [
            "title" => "Playground"
        ];
       return view('errors.404');
        //return view("business.playground.index", $data);
    }
}
