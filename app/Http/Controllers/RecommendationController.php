<?php

namespace App\Http\Controllers;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use SKAgarwal\GoogleApi\PlacesApi;
use Redirect;

class RecommendationController extends Controller
{

    public function tripadvisorSearch(Request $request){
        $results = [];
        $headers = ['Accept' => 'application/json'];

        $activities = Http::withHeaders($headers)->get(env("TRIPADVISOR_URL") . 'location/search',
            [
                "searchQuery" => $request->search,
                "latLong" => $request->latitude . "," . $request->longitude,
                "radius" => 10,
                "radiusUnit" => "mi",
                "category" => $request->category,
                "language" => "en",
                "key" => env("TRIPADVISOR_KEY")
            ])->getBody();

        $activities = json_decode($activities)->data;

        foreach($activities as $activity){
            $locationID =  $activity->location_id;

            $details = Http::withHeaders($headers)->get(env("TRIPADVISOR_URL") . "location/$locationID/details",
                [
                    "currency" => "USD",
                    "language" => "en",
                    "key" => env("TRIPADVISOR_KEY")
                ])->getBody();

            $details = json_decode($details);

            if($details->num_reviews > 3 && $details->photo_count > 5 && (int) $details->rating >= 3.5){

                $response = [
                    "name" => $details->name,
                    "description" => "",
                    "distance" => $activity->distance,
                    "rating" => $details->rating,
                    "reviews" => $details->num_reviews,
                    "latitude" => $details->latitude,
                    "longitude" => $details->longitude,
                    "address" => $details->address_obj->address_string,
                    "url" => $details->web_url,
                    "locationID" => $details->location_id
                ];

                if(isset($details->description)){
                    $response["description"] = substr($details->description, 0, 100);
                }

                if(isset($details->groups)){
                    if(array_key_exists(0, $details->groups)){
                        $response["subcategory"] = $details->groups[0]->name;
                    }
                }

                // get photo

                $photos = Http::withHeaders($headers)->get(env("TRIPADVISOR_URL") . "location/$locationID/photos",
                [
                    "currency" => "USD",
                    "language" => "en",
                    "key" => env("TRIPADVISOR_KEY")
                ])->getBody();

                $photos = json_decode($photos)->data;

                foreach($photos as $photo){
                    $response["image"] = $photo->images->original->url;
                }

                if($request->category == "attractions"){
                    array_push($results, $response);
                }

                if($request->category == "hotels" || $request->category == "restaurants"){
                    if(isset($details->price_level)){
                        $response["price_level"] = $details->price_level;
                        if(isset($details->amenities)){
                            $response["amenities"] = $details->amenities;
                        }
                        array_push($results, $response);
                    }
                }
            }
        }

        return json_encode($results);
    }

    public function googlePlaces($placeName){
        $googlePlaces = new PlacesApi(env('GOOGLE_API_KEY'));

        $response = $googlePlaces->findPlace($placeName, "textquery"); # line 2

        if(isset($response["candidates"])){
            foreach($response["candidates"] as $prediction){
                if(strlen($prediction["place_id"]) > 120){
                    return view('errors.410');
                }
                return Redirect::to('https://www.google.com/maps/place/?q=place_id:' . $prediction["place_id"]);
            }
        }
    }

    public function googleMaps($origin, $destination){

        $googlePlaces = new PlacesApi(env('GOOGLE_API_KEY'));

        $response = $googlePlaces->findPlace($destination, "textquery"); # line 2

        if(isset($response["candidates"])){
            foreach($response["candidates"] as $prediction){
                if(strlen($prediction["place_id"]) > 120){
                    return view('errors.410');
                }
                $destinationID = $prediction["place_id"];
            }
        }

        return Redirect::to("https://www.google.com/maps/dir/?api=1&origin=$origin&destination=$destination&destination_place_id=$destinationID");

    }
}
