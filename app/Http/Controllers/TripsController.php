<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use Dwolla;
use Validator;
use Redirect;
use Mail;
use Carbon\Carbon as Carbon;


class TripsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('firebase.auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function index(){
        // get all orders for the user

        $user = Session::get('user');
        $dwolla = new Dwolla;

        // get the customer

        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $auth = app('firebase.auth');

        $orderDocs = $db->collection("Orders")
            ->where("uid","=", $user->uid)
            ->documents();


        $flights = [];
        $hotels = [];
        $orders = 0;

        foreach($orderDocs as $order){
            $order = $order->data();

            $orderData = json_decode($order["order"], true);
            $order["order"] = $orderData["data"] ?? $orderData;
            if($order["type"] == "flight"){
                array_push($flights, $order);
                $orders++;
            }
            if($order["type"] == "hotel"){
                array_push($hotels, $order);
                $orders++;
            }
            
        }

        if(sizeof($flights) > 0){
            usort($flights, function($a, $b) {
                return $b['timestamp'] <=> $a['timestamp'];
            });
        }   
        
        if(sizeof($hotels) > 0){
            usort($hotels, function($a, $b) {
                return $b['timestamp'] <=> $a['timestamp'];
            });
        }

        // calc points

        $points = $this->getPoints($user, $db);

        // get trackers

        $trackers = [];

        $trackersList = $db->collection("Trackers")->where("uid","=", $user->uid)
            ->documents();

        foreach($trackersList as $tracker){
            $tracker = $tracker->data();
            array_push($trackers, $tracker);
        }

        $data = [
            "title" => "My Trips",
            "flights" => $flights,
            "hotels" => $hotels,
            "orders" => $orders,
            "user" => $user,
            "points" => $points,
            "trackedFlights" => $trackers
        ];

        return view("trips.index", $data);
    }

    public function tripDetail($order){
        $user = Session::get('user');
        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $auth = app('firebase.auth');

        $order = $db->collection("Orders")->document($order)->snapshot()->data();

        // check permission of user to get details about it

        if($order["uid"] != $user->uid){
            // permission denied
            if(!isAdmin($user)){
                return json_encode(501);
            }   
        }

        $type = $order["type"];

        $url ="https://api.duffel.com/air/orders/" . $order["refID"];

        if($type == "hotel"){
            $url ="https://api.duffel.com/stays/bookings/" . $order["refID"];
        }

        // query duffel to get the latest data about the order

        $headers = ['Application-Type:application/json','Accept-Encoding:gzip','Accept:application/json','Duffel-Version:v1','Content-Type:application/json',"Authorization:Bearer " . env('DUFFEL_API_KEY')];

        $crl = curl_init($url);
        curl_setopt($crl, CURLOPT_ENCODING , "gzip");
        curl_setopt($crl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($crl, CURLINFO_HEADER_OUT, true);
        curl_setopt($crl, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($crl, CURLOPT_HTTPHEADER, $headers);

        // Submit the POST request
        $result = curl_exec($crl);

        $result = json_decode($result,true)["data"] ?? false;

        $user = $auth->getUser($order["uid"]);

        // report the status back to the user

        if($type == "flight"){
            $originData = getTripOriginData($result["slices"]);
            $changeable = getTripCancelationDetails($result["conditions"]);
            $cancelable = getTripChangeDetails($result["conditions"]);
            $order["bagUpgrades"] = $order["bagUpgrades"] ?? [];
            $order["seatUpgrades"] = $order["seatUpgrades"] ?? [];
            $purchaseData = json_decode($order["order"], true);

            $airline_initiated_changes = $result["airline_initiated_changes"] ?? false;

            $data = [
                "title" => "My Trip to " . $originData["destination"]["city_name"] . " | " . $order["booking_reference"],
                "duffelData" => $result,
                "order" => $order,
                "originData" => $originData,
                "user" => $user,
                "changeable" => $changeable,
                "cancelable" => $cancelable,
                "airline_initiated_changes" => $airline_initiated_changes,
                "type" => "flight"
            ];

            return view("trips.detail-flight", $data);
        }

        if($type == "hotel"){

            $hotel = $result["accommodation"];

            $cancelable = $result["accommodation"]["rooms"][0]["rates"][0]["cancellation_timeline"];

            $adults = 0;

            foreach($result["guests"] as $guest){

                
            }

            $data = [
                "title" => "My Stay at " . $hotel["name"] . " | " . $order["booking_reference"],
                "duffelData" => $order,
                "order" => $result,
                "user" => $user,
                "changeable" => false,
                "cancelable" => $cancelable,
                "canceledOn" => false,
                "type" => "hotel",
                "adults" => sizeof($result["guests"]),
                "guests" => sizeof($result["guests"])
            ];

            return view("trips.detail-hotel", $data);


        }
    }

    public function getTripAvailableServices(Request $request){
        $user = Session::get('user');
        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $auth = app('firebase.auth');

        $url ="https://api.duffel.com/air/orders/" . $request->order . "/available_services";

        $order = $db->collection("Orders")->document($request->order)->snapshot()->data();

        // check permission of user to get details about it

        if($order["uid"] != $user->uid){
            // permission denied
            return json_encode(501);
        }

        // query duffel to get the latest data about the order

        $headers = ['Application-Type:application/json','Accept-Encoding:gzip','Accept:application/json','Duffel-Version:v1','Content-Type:application/json',"Authorization:Bearer " . env('DUFFEL_API_KEY')];

        $crl = curl_init($url);
        curl_setopt($crl, CURLOPT_ENCODING , "gzip");
        curl_setopt($crl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($crl, CURLINFO_HEADER_OUT, true);
        curl_setopt($crl, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($crl, CURLOPT_HTTPHEADER, $headers);

        // Submit the POST request
        $result = curl_exec($crl);

        $result = json_decode($result,true)["data"] ?? false;

        return json_encode($result);
    }

    public function submitSupportRequest(Request $request){

        $validator = Validator::make($request->all(), [
            'supportReason' => 'required',
            'refID' => 'required'
        ]);

        if ($validator->fails()) {
            return json_encode(500);
        }

        //

        $user = Session::get('user');
        $email = $user->email;

        // get the trip info
        $firestore = app('firebase.firestore');
        $db = $firestore->database();

        $order = $db->collection("Orders")->document($request->refID)->snapshot()->data();

        // check permission of user to get details about it

        if(!$order){
            // order not found
            return json_encode(501);
        }

        if($order["uid"] != $user->uid){
            // permission denied
            return json_encode(502);
        }



        $today = Carbon\Carbon::now()->timezone($user->customClaims["timezone"])->format("m-d-Y g:i A");

        $topics = implode(',', $request->supportReason);

        $timezone = str_replace("_"," ",$user->customClaims["timezone"]);

$content =
"To: Venti Admin Support
From: $user->displayName
Date: $today $timezone
Order ID: $request->refID
Topics: $topics
";

        if($request->supportMessage != "" && $request->supportMessage != "undefined"){
            $content .= "Message: " . $request->supportMessage;
        }

        $subject = "Booking Support Request " . $order["booking_reference"];

        Mail::raw($content, function ($message) use ($subject, $email) {
            $message->subject($subject);
            $message->from("admin@venti.co");
            $message->to('admin@venti.co')->cc($email);
        });

        return json_encode(200);
    }

    public function tripInvoice($order){
        $user = Session::get('user');
        $firestore = app('firebase.firestore');
        $db = $firestore->database();

        $order = $db->collection("Orders")->document($order)->snapshot()->data();

        // check permission of user to get details about it

        if($order["uid"] != $user->uid){
            // permission denied
            return json_encode(501);
        }

        if($order["type"] == "flight"){
            $duffelData = json_decode($order["order"],true)["data"];

            $destination = getTripDestinationData($duffelData["slices"]);

            $data = [
                "order" => $order,
                "duffelData" => $duffelData,
                "destination" => $destination,
                "user" => $user
            ];

            return view('trips.panels.invoice', $data);
        }
        if($order["type"] == "hotel"){
            $duffelData = json_decode($order["order"],true);
            $data = [
                "order" => $order,
                "duffelData" => $duffelData,
                "destination" => "",
                "user" => $user
            ];

            return view('trips.panels.hotel.invoice', $data);
        }

        
    }

    public function sendReceipt(Request $request){
        $user = Session::get('user');
        $firestore = app('firebase.firestore');
        $db = $firestore->database();

        $order = $db->collection("Orders")->document($request->order)->snapshot()->data();

        // check permission of user to get details about it

        if($order["uid"] != $user->uid){
            // permission denied
            return json_encode(501);
        }

        $email = $user->email;

        if($request->type == "flight"){
            $duffelData = json_decode($order["order"],true)["data"];
            $destination = getTripDestinationData($duffelData["slices"]);

            $timezone = Session::get('user')->customClaims["timezone"] ?? "America/New_York";

            $date = \Carbon\Carbon::parse($order["timestamp"])->format("Y-m-d H:i:s");
            $owner = $duffelData["owner"]["name"];
            $booking_reference = $order["booking_reference"];

            $data = [
                "cfar" => $order["airlock"],
                "user" => $user,
                "base_amount" => $duffelData["base_amount"],
                "tax_amount" => $duffelData["base_amount"],
                "seatUpgrades" => $order["seatUpgrades"] ?? [],
                "bagUpgrades" => $order["bagUpgrades"] ?? [],
                "airlock" => $order["airlock"] ?? [],
                "airlockPrice" => $order["airlockPrice"] ?? 0,
                "paidAmount" => $order["amount"],
                "pointsApplied" => $order["points"],
                "order" => $duffelData,
                "passengers" => $duffelData["passengers"],
                "booking_reference" => $booking_reference,
                "timestamp" => $order["timestamp"],
                "date" => $date,
                "timezone" => $timezone,
                "iata_destination" => $destination["destinationArrival"]["iata_code"],
                "destination" => $destination["destinationArrival"]["city_name"],
                "refID" => $order["refID"],
                "from" => $order["from"]
            ];

            $subject = "[Boarding Pass] Booking $booking_reference Confirmed with $owner";
                
            try{
                Mail::send('emails.transactions.flight-purchase', array("data" => $data), function($message) use($email, $subject)
                    {
                        $message
                            ->to($email)
                            ->from("no-reply@venti.co","Venti")
                            ->replyTo("no-reply@venti.co")
                            ->subject($subject);
                });
            }
            catch(Exception $e){
                // the email was not fired.
                
            }
        }

        if($request->type == "hotel"){

            $result = json_decode($order["order"], true);

            $rate = $result["accommodation"]["rooms"][0]["rates"][0];
            $guests = $result["guests"];
            $owner = $result["accommodation"]["name"];
            $address = $result["accommodation"]["location"]["address"];
            $booking_reference = $result["reference"];

            $addressString = "";

            foreach($address as $key => $value){
                $addressString .= $value . "<br>";
            }
            $conditionsString = "";
            $cancelationTimelines = [];
            $cancelation = $rate["cancellation_timeline"] ?? false;
            $conditions = $rate["conditions"] ?? [];

            if(is_array($cancelation)){
                $cancelationTimelines = array_merge($cancelationTimelines, $rate["cancellation_timeline"]);
            }

            foreach($conditions as $condition){
                if($condition["description"] != null){
                    $conditionsString .= "<h3>" . $condition["title"] . "</h3><p>" . $condition["description"] . "</p>";
                }
            }
            
            if($cancelation == null){
                $cancelation = [];
            }

            $timezone = Session::get('user')->customClaims["timezone"] ?? "America/New_York";



            $data = [
                "total_amount" => $rate["total_amount"],
                "user" => $user,
                "paidAmount" => $order["amount"],
                "pointsApplied" => $order["points"],
                "order" => $order["refID"],
                "booking_reference" => $result["reference"],
                "guests" => $result["guests"],
                "timestamp" => $order["timestamp"],
                "timezone" => $timezone,
                "date" => $order["date"],
                "refID" => $order["refID"],
                "base_amount" => $rate["base_amount"],
                "tax_amount" => $rate["tax_amount"],
                "fee_amount" => $rate["fee_amount"],
                "due_at_accommodation_amount" => $rate["due_at_accommodation_amount"],
                "name" => $owner,
                "address" => $address,
                "addressString" => $addressString,
                "adults" => sizeof($result["guests"]),
                "rooms" => $result["rooms"],
                "accommodation" => $result["accommodation"],
                "check_in_date" => $result["check_in_date"],
                "check_out_date" => $result["check_out_date"],
                "id" => $order["refID"],
                "showPricing" => false,
                "imgBlock" => false,
                "accommodation_special_requests" => $order["accommodation_special_requests"],
                "conditionsString" => $conditionsString,
                "from" => $order["from"]
            ];

            
            $subject = "[Boarding Pass] Booking $booking_reference Confirmed with $owner";
                
            try{
                Mail::send('emails.transactions.hotel-purchase', array("data" => $data), function($message) use($email, $subject)
                    {
                        $message
                            ->to($email)
                            ->from("no-reply@venti.co","Venti")
                            ->replyTo("no-reply@venti.co")
                            ->subject($subject);
                });
            }
            catch(Exception $e){
                // the email was not fired.
                
            }
        }

        return json_encode(200);
    }

    public function cancelTrip(Request $request){
        $user = Session::get('user');

        $validator = Validator::make($request->all(), [
            'order' => 'required',
            'cancelationReason' => 'required'
        ]);

        if ($validator->fails()) {
            return json_encode(500);
        }

        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $auth = app('firebase.auth');

        $orderDoc = $db->collection("Orders")->document($request->order);
        $order = $orderDoc->snapshot()->data();

        // check permission of user to get details about it

        if(!$order["uid"] == $user->uid){
            // permission denied
            return json_encode(501);
        }

        // check if the order has airlock
        $duffelData = json_decode($order["order"],true)["data"];

        $destination = getTripDestinationData($duffelData["slices"]);

        $originData = $destination["originData"];

        if($order["airlock"] == true){

            // check if today's date is after the departure date

            $airlockExpiration = \Carbon\Carbon::parse($destination["originData"]["departing_at"])->format("F d, Y-g:i A");
            $airlockExpiry = explode('-', $airlockExpiration);

            $hasExpiryPassed = false;

            $now = \Carbon\Carbon::now()->timezone($originData["origin"]["time_zone"]);

            $remainingTime = (int) $now->diffInMinutes($airlockExpiration, false);

            if($remainingTime < 0){
                // the expiry has passed
                return json_encode(502);
            }

            // by this point, we can activate the airlock policy

            $now = \Carbon\Carbon::now()->timezone($user->customClaims["timezone"])->timestamp;

            $orderDoc->update([
                ["path" => "canceled","value" => "true"],
                ["path" => "canceledOn","value" => $now],
                ["path" => "cancelReason","value" => $request->cancelationReason],
            ]);

            // email the user

            $data = $order;
            $data["timezone"] = $user->customClaims["timezone"];

            $email = $user->email;

            $subject = "[Boarding Pass] Flight Canceled with Airlock: " . $duffelData["owner"]["name"] . " REF: " . $order["booking_reference"];

            $data["order"] = $duffelData;
            $data["canceledOn"] = $now;

            try{
                Mail::send('emails.transactions.flight-canceled', array("data" => $data), function($message) use($email, $subject)
                    {
                        $message
                            ->to($email)
                            ->bcc("admin@venti.co")
                            ->from("no-reply@venti.co","Venti")
                            ->replyTo("no-reply@venti.co")
                            ->subject($subject);
                });
            }
            catch(Exception $e){
                // the email was not fired.
                
            }

            // log cancelation with duffel

            $this->duffelCancelRequest($order["id"]);

            return json_encode(200);
        }

        // this order cannot be canceled due to an error

        return json_encode(500);
    }

    public function duffelCancelRequest($order){

        $headers = ['Application-Type:application/json','Accept-Encoding:gzip','Accept:application/json','Duffel-Version:v1','Content-Type:application/json',"Authorization:Bearer " . env('DUFFEL_API_KEY')];

        $url = "https://api.duffel.com/air/order_cancellations";

        $post_data = [
            "data" => [
                "order_id" => $order
            ]
        ];

        $crl = curl_init($url);
        curl_setopt($crl, CURLOPT_ENCODING , "gzip");
        curl_setopt($crl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($crl, CURLINFO_HEADER_OUT, true);

        curl_setopt($crl, CURLOPT_POST, true);
        curl_setopt($crl, CURLOPT_POSTFIELDS, json_encode($post_data));
        curl_setopt($crl, CURLOPT_HTTPHEADER, $headers);

        // Submit the POST request
        $result = curl_exec($crl);

        $result = json_decode($result,true)["data"] ?? false;

        // update firebase
        $firestore = app('firebase.firestore');
        $db = $firestore->database();

        $orderDoc = $db->collection("Orders")->document($order);

        $cancelID = $result["id"];

        // cancel the order through duffel

        $cancelResult = $this->duffelCancelSubmit($cancelID);

        $orderDoc->update([
            ["path" => "duffelCancelRequest","value" => $result],
            ["path" => "duffelCancelResult","value" => $cancelResult]
        ]);

        return true;

    }

    public function duffelCancelSubmit($cancelID){
        $headers = ['Application-Type:application/json','Accept-Encoding:gzip','Accept:application/json','Duffel-Version:v1','Content-Type:application/json',"Authorization:Bearer " . env('DUFFEL_API_KEY')];

        $url = "https://api.duffel.com/air/order_cancellations/$cancelID/actions/confirm";

        $crl = curl_init($url);
        curl_setopt($crl, CURLOPT_ENCODING , "gzip");
        curl_setopt($crl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($crl, CURLINFO_HEADER_OUT, true);

        curl_setopt($crl, CURLOPT_POST, true);
        curl_setopt($crl, CURLOPT_HTTPHEADER, $headers);

        // Submit the POST request

        $result = curl_exec($crl);

        $result = json_decode($result,true)["data"] ?? false;

        return $result;
    }

    public function trackers(Request $request){
        $user = Session::get('user');
        $firestore = app('firebase.firestore');
        $db = $firestore->database();

        $trackerDocs = $db->collection("Trackers")->where("uid","=",$user->uid)->documents();
        $trackers = [];

        foreach($trackerDocs as $tracker){
            $tracker = $tracker->data();
            array_push($trackers, $tracker);
        }

        return json_encode($trackers);
    }

    public function createTracker(Request $request){
        $user = Session::get('user');

        // validate submission

        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $trackerID = generateRandomString("12");

        $iata_origin = str_replace("(","",explode(")", $request->origin)[0]);
        $iata_destination = str_replace("(","",explode(")", $request->destination)[0]);


        // check points balance

        $points = $this->getPoints($user, $db);

        if($points < 1){
            // insufficient funds
            return json_encode(400);
        }

        // deduct points

        $transactionID2 = generateRandomString(12);
        $timestamp = \Carbon\Carbon::now('UTC')->timestamp; // UTC
        $date = \Carbon\Carbon::now('UTC')->format("Y-m-d H:i:s");

        $data = [
            "transactionID" => $transactionID2,
            "uid" => $user->uid,
            "type" => "points-withdraw",
            "timestamp" => $timestamp,
            "date" => $date,
            "amount" => 1,
            "speed" => "next-available",
            "fee" => 0,
            "status" => "complete",
            "from" => "My Boarding Pass",
            "to" => "Venti",
            "total" => 1,
            "note" => "Flight Tracking: $iata_origin to $iata_destination",
            "transferID" => $transactionID2, // it's wise to link this with the previous transfer
            "transactionUrl" => null,
            "env" => env('APP_ENV'),
            "refID" => $trackerID,
            "taxable" => true // points are taxable when converted to cash equivalent value
        ];

        $pointsDeduction = $db->collection("Transactions")->document($transactionID2)->set($data);

        $data = [
            "uid" => $user->uid,
            "iata_origin" => $iata_origin, // IATA
            "iata_destination" => $iata_destination, // IATA
            "origin" => explode(")", $request->origin)[1],
            "destination" => explode(")", $request->destination)[1],
            "class" => $request->flight_class,
            "adults" => $request->adults,
            "children" => $request->children,
            "strike" => $request->strike,
            "start" => $request->departure,
            "expiry" => Carbon::parse($request->departure)->timezone('UTC')->timestamp,
            "end" => $request->return,
            "status" => "active",
            "points" => 1,
            "createdAt" => Carbon::now()->timezone("UTC")->timestamp,
            "type" => $request->flight_type,
            "trackerID" => $trackerID
        ];

        $tracker = $db->collection("Trackers")->document($trackerID)->set($data);

        return json_encode(200);
    }

    public function getTracker($id){
        $user = Session::get('user');

        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $tracker = $db->collection("Trackers")->document($id)->snapshot()->data();

        if(!$tracker){

            return view("errors.404");
        }

        if($tracker["uid"] != $user->uid){

           return view("errors.404"); 
        }

        // get all prices we've collected thus far

        $priceLists = $db->collection("TrackedFlights")->where("trackerID","=",$id)->documents();

        $prices = [];

        foreach($priceLists as $price){
            array_push($prices, $price);
        }

        $data = [
            "title" => "Tracking Flight to " . $tracker["destination"],
            "tracker" => $tracker,
            "prices" => $prices
        ];

        return view("trips.panels.tracker", $data);
    }

    public function editTracker(Request $request){
        $user = Session::get('user');
        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $trackerDoc = $db->collection("Trackers")->document($request->id);
        $tracker = $trackerDoc->snapshot()->data();

        if($tracker){
            if($tracker["uid"] == $user->uid){
                $trackerDoc->update([
                    //iata_home
                    //iata_destination
                    //
                    ["path" => "cityName", "value" => $request->cityName],
                    ["path" => "class", "value" => $request->class],
                    ["path" => "adults", "value" => $request->adults],
                    ["path" => "children", "value" => $request->children],
                    ["path" => "strike", "value" => $request->strike],
                    ["path" => "departure", "value" => $request->departure],
                    ["path" => "return", "value" => $request->return],
                    ["path" => "status", "value" => $request->status],
                    ["path" => "expiry", "value" => Carbon::parse($request->departure)->timezone('UTC')->timestamp],
                ]);

                return json_encode(200);
            }
        }

        return json_encode(404);
    }

    public function getPoints($user, $db){
        $transactions = [];

        $transactionDocs = $db->collection("Transactions");
        $transactionDocs = $transactionDocs
                ->where("uid","=",$user->uid)
                ->where("env","=",env("APP_ENV"))
                ->documents();
            // I need a separate firebase instance for sandboxing

            foreach($transactionDocs as $transactionDoc){
                array_push($transactions,  $transactionDoc->data());
            }

        return getPointsBalance($transactions);
    }

    public function updateTrackerStatus(Request $request){
        $user = Session::get('user');
        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $trackerDoc = $db->collection("Trackers")->document($request->tracker);
        $tracker = $trackerDoc->snapshot()->data();

        if($tracker){
            if($tracker["uid"] == $user->uid){
                $status = $request->status == "true" ? "active" : "inactive";
                $trackerDoc->update([
                    ["path" => "status", "value" => $status],
                ]);

                return json_encode(200);
            }
        }

        return json_encode(404);
    }
}
