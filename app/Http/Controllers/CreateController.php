<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Http;
use Barryvdh\DomPDF\Facade\Pdf;
use View;

class CreateController extends Controller
{
    // chatGPT manager

    public function index(){
        $data = [
            "title" => "Plan Your Trip With ChatGPT",
            "description" => "Create a travel itinerary in seconds with our new itinerary generation tool made with ChatGPT.",
            "image" => "https://venti.co/assets/img/travelplanner.jpg"
        ];

        return view("builder.create", $data);
    }

    public function create(Request $request){
        if((str_replace("VENTI","",$request->random)/2) != $request->answer){
            // failed math problem
            return 500; 
        }

        $destination = $request->destination;

        $days = $request->days . "-day";

        /*

        $example = sampleItinerary($request->days);
        $hotel = "";

        

        



        $prompt = "Create a unique and detailed itinerary for a $days trip to $destination. Use this itinerary as a guideline for formatting purposes only: '$example'. 
            For each <a> html tag, replace the hyperlink with a new link that follows this format 'https://venti.co/place/name of place'.
            $hotel
            For each <time> html tag, hyperlink the content with a url that follows this format 'https://venti.co/map/origin/destination'.
            Only recommend activities within 40-minute or 30-mile driving distance of downtown $destination.";
        */
            if($request->days > 1){
            $hotel = "For the first day of the itinerary, provide instructions on how to get to my hotel from the airport.
            On the first day, do not schedule hotel check in before 3 pm in the afternoon. Make sure check in is listed as an activity.
            On the last day of the trip, do not schedule hotel check out after 11 am in the morning. Make sure checkout is listed as an activity.";
        }

        $example = sampleJSONItinerary($request->destination, $request->country, $request->days);

        $prompt = "Create a unique and detailed itinerary for a $days trip to $destination that follows this format `$example`.
            $hotel
            Provide a lunch and dinner recommendation for each day of the trip except the last day.
            If there is a huge gap in between activities, make a note that there is free time to explore.
            For each activity, provide the url for the destination and approximate the distance for each mode of travel to that activity.
            The url pattern for a place is https://venti.co/place/destination and the url pattern for distance is https://venti.co/map/previous/next.
            For each activity, consider the logistics required get there when recommending the start time. For example, if the destination is 30 minutes away, schedule the start time to be at least 45 minutes from the previous activity's end time. Please
            Make sure the response is valid JSON without any HTML.";


        $prompt = ["role" => "user", "content" => $prompt];

        $response = $this->askGPT($prompt);

        $answer = "";

        if(is_null($response)){

            return 501;
        }


        if(!array_key_exists("choices", $response)){
            return 501;
        }



        foreach($response["choices"] as $choice){
            $answer .= $choice["message"]["content"];
        }

        //$answer = explode('{"title',$answer);

        


        //$answer = '{"title' . $answer[1];
        // now we format the answer into html using a Blade view
        //$answer = str_replace("\n", '', $answer);
        
        
        $output = json_decode($answer, true);

$response["answer"] = $output;
        if(is_null($output)){
            // chatGPT failed to provide a response in JSON smh
            dd($output);
            return 501;
        }



        if(is_null($output)){
            return 501;
        }
// 
        if(!array_key_exists("description", $output)){
            $output["description"] = "This is a sample itinerary. Please contact admin@venti.co if you need it customized by a travel advisor.";
        }

        $data = $output;

        $view = View::make("builder.itinerary", compact('data'))->render();

        $response["answer"] = $view;


        // store the data for mining purposes

        $auth = app('firebase.auth');
        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        
        $planID = Str::random(12);

        $background = $this->getBackgroundImage($request->destination, $request->country);

        $docRef = $db->collection("Plans")->document($planID)->set(
            [             
                "destination" => $request->destination,
                "country" => $request->country,
                "lat" => $request->lat,
                "long" => $request->long,
                "lodging" => $request->lodging,
                "food" => $request->food,
                "airports" => $request->airports,
                "planID" => $planID,
                "answer" => $answer,
                "chatgptID" => $response["id"] ?? null,
                "createdAt" => Date("Y-m-d"),
                "imageUrl" => $background[0] ?? null,
                "imageAuthor" => $background[1] ?? null,
                "imageAuthorProfile" => $background[2] ?? null
            ]
        );
        $response["image"] = $background[0] ?? null;
        $response["planID"] = $planID;

        return json_encode($response);
        
    }

    public function askGPT($prompt){
        $dataString = json_encode([
            "model" => "gpt-3.5-turbo",
            "messages" => [$prompt],
            "temperature" => 1,
        ]);

        $ch = curl_init('https://api.openai.com/v1/chat/completions');                                                      
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
        curl_setopt($ch, CURLOPT_POSTFIELDS, $dataString);                                                                  
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);     

        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'content-type:application/json',                                                                     
            'Authorization: Bearer ' . env("CHATGPT_API_SECRET"),                                                                                
            'OpenAI-Organization: ' . env("CHATGPT_API_ORGANIZATION")
        )                                                                       
        );                                                                                                                                                                        
        $response = curl_exec($ch);

        $response = json_decode($response, true);

        return $response;
    }

    public function read($planID){
        $auth = app('firebase.auth');
        $firestore = app('firebase.firestore');
        $db = $firestore->database();

        $result = $db->collection("Plans")->document($planID)->snapshot()->data();

        if($result == null){
            return view('errors.404');
        }

        $title = getStringBetween("<h1>","</h1>",$result["answer"]);
        $description = getStringBetween("<h6>","</h6>",$result["answer"]);

        $data = [
            "title" => $title,
            "description" => $description,
            "plan" => $result,
            "url" => "https://venti.co/plans/$planID",
            "image" => $result["imageUrl"],
            "pdf" => false
        ];

        return view("builder.read", $data);
    }

    public function edit($planID){
        $auth = app('firebase.auth');
        $firestore = app('firebase.firestore');
        $db = $firestore->database();

        $result = $db->collection("Plans")->document($planID)->snapshot()->data();

        if($result == null){
            return view('errors.404');
        }

        $title = getStringBetween("<h1>","</h1>",$result["answer"]);
        $description = getStringBetween("<h6>","</h6>",$result["answer"]);
        
        $data = [
            "title" => $title,
            "description" => $description,
            "plan" => $result,
            "url" => "https://venti.co/plans/$planID",
            "image" => $result["imageUrl"]
        ];

        return view("builder.edit", $data);
    }

    public function update(Request $request){
        $auth = app('firebase.auth');
        $firestore = app('firebase.firestore');
        $db = $firestore->database();

        $result = $db->collection("Plans")->document($request->planID);

        if($result == null){
            return 500;
        }

        $update = $result->update([
            ["path" => "answer","value" => str_replace('h4>','h6>',$request->answer)],
            ["path" => "destination","value" => $request->destination],
            ["path" => "country","value" => $request->country],
            ["path" => "imageUrl","value" => $request->imageUrl],
            ["path" => "imageAuthor","value" => $request->imageAuthor],
            ["path" => "imageAuthorProfile","value" => $request->imageAuthorProfile],
        ]);

        return 200;
    }

    function getTripLocation($destination, $country){
        if($destination == $country){
            return $destination;
        }

        return "$destination, $country";
    }

    function savePDF($planID){
        $auth = app('firebase.auth');
        $firestore = app('firebase.firestore');
        $db = $firestore->database();

        $result = $db->collection("Plans")->document($planID)->snapshot()->data();

        if($result == null){
            return view('errors.404');
        }

        $title = getStringBetween("<h1>","</h1>",$result["answer"]);
        $description = getStringBetween("<h6>","</h6>",$result["answer"]);
        
        $data = [
            "title" => $title,
            "description" => $description,
            "plan" => $result,
            "url" => "https://venti.co/plans/$planID",
            "image" => $result["imageUrl"],
            "pdf" => true
        ];

        $pdf = Pdf::loadView('builder.pdf', $data);
            
        return $pdf->download(str_replace(" ","_", $title) . '.pdf');
    }

    function getBackgroundImage($destination, $country){

        $name = $this->getTripLocation($destination, $country, $random = false);

        $details = Http::get("https://api.unsplash.com/search/photos?client_id=" . env('UNSPLASH_KEY'). "&per_page=10&query=scenery of " . $name)->getBody();

        $details = json_decode($details);
        $rand = rand(0,3);
        if($random){
            $rand = rand(0,10);
        }

        if(isset($details->results)){
            $i = 0;
            foreach($details->results as $detail){
                if($i == $rand){
                    return [
                        0 => $detail->urls->regular,
                        1 => $detail->user->name,
                        2 => $detail->user->links->html
                    ];
                }
                $i++;
            }
        }
    }
}
