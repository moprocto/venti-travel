<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;

class ChatController extends Controller
{
    public function __construct(){
        $this->middleware('firebase.auth');
    }

    public function token(Request $request){
        dd($request->all());
    }

    public function send(Request $request){
        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $auth = app('firebase.auth');
        $timestamp = strtotime("now");
        $user = Session::get('user');
        $group = $ideaDoc = $db->collection("Ideas")->document($request->tripID)->snapshot()->data();

        $myRole = getGroupRole($user->uid, $group);

        // check if in the group

        if($myRole == "owner" || $myRole == "viewer") {
            $docRef = $db->collection("Messages/$request->tripID/$timestamp")->document("message")->set(
                [
                    "uid" => $user->uid,
                    "timestamp" => $timestamp,
                    "message" => $request->msg,
                    "photoUrl" => $user->photoUrl,
                    "username" => $user->customClaims['username'],
                    "displayName" => getFirstName($user->displayName)
                ]
            );

            return json_encode(200);
        }
    }

    public function pull(Request $request){
        $firestore = app('firebase.firestore');
        $db = $firestore->database();
        $auth = app('firebase.auth');
        $user = Session::get('user');
        $group = $db->collection("Ideas")->document($request->tripID)->snapshot()->data();

        $myRole = getGroupRole($user->uid, $group);

        // check if in the group

        if($myRole == "owner" || $myRole == "viewer") {
            $html = "";
            $messages = $db->collection("Messages")->document("$request->tripID")->collections();

            foreach($messages as $message){
                $items = $message->listDocuments();
                foreach($items as $item){
                    $data = $item->snapshot()->data();
                    $displayName = $data["username"];
                    if(array_key_exists("displayName", $data)){
                        $displayName = getFirstName($data["displayName"]);
                    }
                    if($user->uid == $data["uid"]){

                        $html .= "<li class='send'><div class='chat-message'><p class='chat-text'>". displayTextWithLinks($data["message"]) . "<span class='username'>" . $displayName . "</span></p><img src='" . $data["photoUrl"] . "' class='avatar' onclick=\"window.location.href='/u/" . $data["username"] ."'\"></div></li>";

                    } else{

                        $html .= "<li class='receive'><div class='chat-message'><img src='" . $data["photoUrl"] . "' class='avatar' onclick=\"window.location.href='/u/" . $data["username"] ."'\"><p class='chat-text'>". displayTextWithLinks($data["message"]) . "<span class='username'>" . $displayName . "</span></p></div></li>";
                    }
                }   
            }

            return json_encode($html);
        }
    }
}
