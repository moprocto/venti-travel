<?php

namespace app\Facades\Mailchimp;

use Illuminate\Support\ServiceProvider;

class MailchimpServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->singleton('Mailchimp', function($app) {
            return new Mailchimp();
        });
    }
}