<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon as Carbon;

class Group extends Model
{
    use HasFactory;

    public function member(){
        return $this->belongsTo('\App\Models\Member','uid','uid');
    }

    public function connections(){
    	return $this->hasMany('\App\Models\Connection','tripID','tripID');
    }

    public function groupMemberUIDs(){
        $uidList = [];
        $connections = $this->connections;

        foreach($connections as $connection){
            array_push($uidList, $connection->uid);
        }
        
        return $uidList;
    }

    public function numMembers(){
        $connections = $this->connections->where("role", "viewer")->count();
        return $connections;
    }

    public function isExpired(){
        $createdAt = Carbon::parse($this->departure)->diffInDays();

        if($createdAt > 1){
            return true;
        }

        return false;
    }
}