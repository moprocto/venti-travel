<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Dwolla;
use Session;
use InvalidArgumentException;
use App\Models\BoardingPass as BoardingPass;

class Customer 
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array<string>
     */

    public $wallet = false;
    public $status;
    public $customerID;
    public $boardingpass = false;

    public function __construct($uid){
    	$dwolla = new Dwolla;
    	$db = app('firebase.firestore')->database();
    	$customer = $this->getCustomer($uid, $dwolla);
    	$this->getWallet($customer["wallet"] ?? null);
        $this->getBoardingPass($uid, $db, $dwolla);
    }

    public function getCustomer(string $uid, $dwolla){
        $customer = $dwolla->getCustomer($uid);
        $this->status = $customer["status"];
        $this->customerID = $customer["customerID"];
        return $customer;
    }

    public function getWallet($wallet){
    	$walletAddress = $wallet ?? null;
    	if($walletAddress){
			$this->wallet = explode("funding-sources/",$walletAddress)[1];

    	}
    }

    public function getBoardingPass($uid, $db, $dwolla){
        $this->boardingpass = new BoardingPass($this->wallet, $uid, $this->customerID, $dwolla, $db);
    }
}