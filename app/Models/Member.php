<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Member extends Model
{
    use HasFactory;

    public function groups(){
        return $this->hasMany('\App\Models\Group','uid','uid');
    }

    public function connections(){
        return $this->hasMany('\App\Models\Connection','uid','uid');
    }

    public function customClaims(){
        $claims = $this->hasOne('\App\Models\Claim','uid','uid');
        $this->attributes['customClaims'] = $claims->first()->toArray();
        return $claims;
    }

    public function men(){

    }

    public function women(){
        
    }

    public function isComplete(){
        $completed = false;
        $claims = $this->customClaims;

        if(
            $this->emailVerified == 1 &&
            $claims["dob"] != null &&
            $claims["dob"] != 0 &&
            $this->hasPhoto() == true
        ){
            $completed = true;
        }

        return $completed;
    }

    public function interests(){
        return $this->hasMany('\App\Models\Interest','uid','uid');
    }

    public function languages(){
        return $this->hasMany('\App\Models\Language','uid','uid');
    }

    public function wishlist(){
        return $this->hasMany('\App\Models\Wishlist','uid','uid')->where("archived", 0);
    }

    public function settings(){
        return $this->hasMany('\App\Models\Setting','uid','uid');
    }

    public function token(){
        return $this->hasOne('\App\Models\Token','uid','uid');
    }

    public function hasPhoto(){
        if($this->photoUrl != "/assets/img/profile.jpg" && $this->photoUrl != "/assets/img/profile-2.jpg"){
            return true;
        }
       return false;
    }

    public function isRecommendable(){
        // a recommendable completed user has all of the necessary fields filled out to 
        // 1. is not disabled
        // 2. languages spoken
        // 3. has at least a bio or occupation
        // 4. has interests
        // if not interests, they atleast have a bio

        if(!$this->isComplete()){
            // a complete profile can use the platform
            // but more detail is needed for them to be recommendable
            return false;
        }
        if($this->hasBio == "N"){
                // okat they don't have a bio. do they at least have an occupation listed... 
            if($this->hasOccupation == "N" && $this->hasWishlist == "N" && $this->hasInterests == "N"){
                // they have no material for me to work with
                return false;
            }
        }
        


        if($this->customClaims["requests"] != "all"){
            return false;
        }

        if(
            $this->disabled == 1 ||
            sizeof($this->languages) == 0
        ){
            return false;
        }

        return true;
    }
}
