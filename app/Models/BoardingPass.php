<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use App\Models\Customer as Customer;

class BoardingPass extends Customer
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array<string>
     */

    // Public variables initialized at 0
    public float $cash = 0.0;
    public float $points = 0.0;
    public bool $transactable = false;
    public $fundingSources;

    private $transactions = [];
    /**
     * Create a new instance of the BoardingPass class.
     *
     * @param string $owner Owner of the boarding pass
     */
    public function __construct($wallet, $uid, $customerID, $dwolla, $db)
    {
        if(!$wallet){
            return false;
        }

        $this->cashBalance($wallet, $dwolla);
        $this->pointsBalance($uid, $db); 
        $this->fundingSources($customerID, $dwolla);
    }

    public function spendingPower(){
        return $this->cash + $this->points;
    }

    public function cashBalance($wallet, $dwolla){
        $source = $dwolla->getFundingSourceBalance($wallet);
        $this->cash = (float) $source->balance->value;
    }

    public function addFunds(float $amount){
        // dwolla add funds
        $this->cash += $amount;
        // return instance of self so transactions can be chained (hamburger method)
        return $this;
    }

    public function withdrawFunds(float $amount){

        $this->cash -= $amount;
        // return instance of self so transactions can be chained (hamburger method)
        return $this;
    }

    public function addPoints(float $amount){
        $this->points += $amount;
        return $this;
    }

    public function withdrawPoints(float $amount){
        $this->points -= $amount;
        return $this;
    }

    public function pointsBalance($uid, $db){
        // get the transactions from firebase

        $transactionDocs = $db->collection("Transactions");
        $transactionDocs = $transactionDocs
            ->where("uid","=",$uid)
            ->documents();
        // I need a separate firebase instance for sandboxing

        foreach($transactionDocs as $transactionDoc){
            array_push($this->transactions,  $transactionDoc->data());
        }

        $this->points = getPointsBalance($this->transactions);
    }

    public function fundingSources($customerID, $dwolla){

        $this->fundingSources = collect($dwolla->fundingSources($customerID));

        if($this->fundingSources->where("status", "verified")->count() > 0){
            $this->transactable = true;
        }
        
    }
}
