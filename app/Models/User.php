<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Session;
use Carbon\Carbon as Carbon;
use Dwolla;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    public $auth;
    public $firebase;
    public $db;
    public $user;
    public $uid;
    public $transactions = [];
    public $claims;
    public $customer;
    public $dwolla;
    public $wallet;
    public float $balance;

    public function __construct($user){
        $this->auth = app('firebase.auth');
        try{
            $this->user = $this->auth->getUser($user);
            $this->firestore = app('firebase.firestore');
            $this->db = $this->firestore->database();
            $this->uid = $this->user->uid;
            $this->claims = $this->user->customClaims;
        }
        catch(FirebaseException $e){
            // throw fatal error
            return false;
        }
    }

    public function customClaims(){
        return $this->claims;
    }

    public function subscription(){
        return strtoupper($this->claims["subscription"] ?? "BUDDY");
    }

    public function customer(){
        $this->dwolla = new Dwolla;
        $this->customer = $this->dwolla->getCustomer($this->user);
        return $this;
    }

    public function wallet(){
        // can only be called after customer is defined
        $this->wallet = $this->dwolla->getWallet($this->customer["customerID"]);
        return $this;
    }


    public function getCashBalance(){
        $wallet = $this->customer()->wallet();
        $walletDetails = $this->dwolla->getFundingSourceBalance($this->wallet->_links->self->href);
        $this->balance = (float) $walletDetails->balance->value;
        return $this->balance;
    }

    public function getPointsBalance(){
        
    }
    
}
