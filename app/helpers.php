<?php

use Carbon\Carbon;

function isAdmin($user){
    $admins = explode(",",env('APP_ADMINS_' . strtoupper(env('APP_ENV'))));

    if(in_array($user->uid, $admins)){
        // the user is an admin
        return true;
    }
}

function getMemberID($uid, $customerUrl){
    $uid = substr($uid, -5);
    $cuid = substr($customerUrl, -4);
    return strtoupper($uid . "" . $cuid);
}

function toPennies($integer){
    return ($integer < 10) ? "0.0$integer" : "0.$integer";
}

function ordinal($number) {
    $ends = array('th','st','nd','rd','th','th','th','th','th','th');
    if ((($number % 100) >= 11) && (($number%100) <= 13))
        return 'th';
    else
        return $ends[$number % 10];
}

function isDisposableEmail($email) {
    $blocklist_path = public_path() . '/assets/disposable_email_blocklist.conf';
    $disposable_domains = file($blocklist_path, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
    $domain = mb_strtolower(explode('@', trim($email))[1]);
    return in_array($domain, $disposable_domains);
}



function isLoyaltyAvailable($id){
    switch ($id) {
        case 'arl_00009VME7DDSWSIAygCT1z':
            return true;
            break;
        case 'arl_00009VME7DDoV8ZkzmMkaN':
            return true;
            break;
        case 'arl_00009VME7DAch01WpssCbI':
            return true;
            break;
        case 'arl_00009VME7DBKeMags5CliQ':
            return true;
            break;
        case 'arl_00009VME7DAGiJjwomhv32':
            return true;
            break;
        case 'arl_00009VME7DBgd2sGtBN3Gy':
            return true;
            break;
        case 'arl_00009VME7DC2bj9quHXKoQ':
            return true;
            break;
        case 'arl_00009VME7DBKeMags5Cliz':
            return true;
            break;
        case 'arl_00009VME7DAch01WpssCcW':
            return true;
            break;
        case 'arl_00009VME7DAGiJjwomhv3K':
            return true;
            break;
        case 'arl_00009VME7DD6Xm0axa2BU8':
            return true;
            break;
        default:
            return false;
            break;
    }
}

function findMatchingSegments($firstArray, $secondArray) {
    $matchingItems = [];
    $segmentID = $firstArray['segment_id'];

    foreach ($secondArray as $item) {
        if ($item['segment_id'] == $segmentID) {
            $matchingItems = $item;
            $matchingItems["passenger_name"] = $firstArray["passenger_name"];
            $matchingItems["passenger_id"] = $firstArray["passenger_id"];
            $matchingItems["origin"] = $firstArray["origin"];
            $matchingItems["destination"] = $firstArray["destination"];
            $matchingItems["origin_iata"] = $firstArray["origin_iata"];
            $matchingItems["destination_iata"] = $firstArray["destination_iata"];
        }
    }

    return $matchingItems;
}

function getPassengerFirstNameById($items, $searchId) {
    foreach ($items as $item) {
        if (isset($item['id']) && $item['id'] === $searchId) {
            return isset($item['given_name']) ? $item['given_name'] : null;
        }
    }
    return null; // Return null if no matching ID is found
}

function getBaggageIdByPassengerID($items, $passengerId){
    foreach ($items as $item) {
        if (isset($item['passenger_ids']) && $item['passenger_ids'] === $passengerId) {
            return isset($item['id']) ? $item['id'] : null;
        }
    }
    return null;
}

function getSegmentsByID($slices, $segmentID) {
    // Check if the slices array is valid
    if (!is_array($slices) || empty($slices)) {
        return null;
    }

    // Iterate through slices
    foreach ($slices as $slice) {
        // Check if 'segments' key exists and is an array
        if (isset($slice['segments']) && is_array($slice['segments'])) {
            // Iterate through segments
            foreach ($slice['segments'] as $segment) {
                // Check for the matching segment ID
                if (isset($segment['id']) && $segment['id'] === $segmentID) {
                    // Check for destination city name
                    $destinationCity = isset($slice['destination']['city_name']) ? $slice['destination']['city_name'] : null;
                    // Add the destinationCity to the segment array
                    $segment['destinationCity'] = $destinationCity;
                    // Return the matching segment array with destinationCity
                    return $segment;
                }
            }
        }
    }

    // If no matching segment is found, return null
    return null;
}

function getPassengerTicketNumber($documents, $passengerId){
    foreach($documents as $document){
        if(in_array($passengerId, $document["passenger_ids"]) && $document["type"] == "electronic_ticket"){
            return $document["unique_identifier"];
        }
    }
}

function getBaggageLabelBySegmentID($items, $segmentId){
    foreach ($items as $item) {
        if ($item['segment']["id"] === $segmentId) {
            $label = $item["segment"]["origin"]["city_name"] . " -> " . $item["segment"]["destination"]["city_name"];
            return $label;
        }
    }
    return null;
}

function getBaggagesByPassengerId($array, $passengerId) {
    // Check if the necessary structure exists in the array
    if (isset($array[0]['segments'][0]['passengers'])) {
        // Loop through the passengers array
        foreach ($array[0]['segments'][0]['passengers'] as $passenger) {
            // Check if the passenger_id matches
            if (isset($passenger['passenger_id']) && $passenger['passenger_id'] === $passengerId) {
                // Return the baggages array for the matching passenger_id
                return isset($passenger['baggages']) ? $passenger['baggages'] : [];
            }
        }
    }
    // Return an empty array if no match is found
    return [];
}


function calcVentiPlatformFee($total,$protection = true){
    $markUp = $total * 0.045;
    $totalWithMarkUp = $total + (float) $markUp;
    $protectionFee = 0;

    if($protection){
        $protectionFee = $totalWithMarkUp * 0.2;
    }

    $subtotal = $totalWithMarkUp + $protectionFee;

    $ccFee = ($subtotal * .029) + 0.30;

    $platformFee = $subtotal + $ccFee;

    return $platformFee;
}

function calVentiDiscount($total,$protection = true){
    // members pay a smaller mark-up fee 3.5% insteaf of 4%
    $markUp = $total * 0.035;
    $totalWithMarkUp = $total + $markUp;
    $protectionFee = 0;

    if($protection){
        $protectionFee = $totalWithMarkUp * 0.15;
    }

    $subtotal = $totalWithMarkUp + $protectionFee;

    $ccFee = 0;

    return $subtotal;
}

function getDurationinMinutes($duration){
    $duration = str_replace("PT","",$duration);
    $durationHours = explode("H", $duration);
    $durationHours = (int) $durationHours[0] * 60;

    $minutes = explode("H", $duration)[1] ?? explode("H", $duration)[0];

    
    
    $durationMinutes = $durationHours + ((int) str_replace("M","",trim($minutes))) * 1;

    return $durationMinutes;

}

function parseShortCode($string){
      $words = explode(" ", $string);
      $i = 0;
      foreach($words as $word){
        if (str_contains($word, '[link')) {
            $href = str_replace("url=",'',$words[$i + 1]);
            $href = str_replace("'",'',$href);
            $label = str_replace("label=",'',$words[$i + 2]);
            $label = str_replace("'",'',$label);
            $label = str_replace("]",'',$label);
            $words[$i] = "<a href='$href' target='_blank' class='text-success'>$label</a>";
            unset($words[$i + 1]);
            unset($words[$i + 2]);
        }

        if (str_contains($word, '[image')) {
            $href = str_replace("url=",'',$words[$i + 1]);
            $href = str_replace("'",'',$href);
            $href = str_replace("]",'',$href);
            $words[$i] = "<br><img src='$href' style='width:100%; max-width:300px;' />";
            unset($words[$i + 1]);
        }
        if (str_contains($word, '[gif')) {
            $href = str_replace("url=",'',$words[$i + 1]);
            $href = str_replace("'",'',$href);
            $href = str_replace("]",'',$href);
            $words[$i] = "<br><img src='$href' style='width:100%; max-width:300px;' class='append-gif' />";
            unset($words[$i + 1]);
        }

        if (str_contains($word, '[reel')) {
            $href = str_replace("id=",'',$words[$i + 1]);
            $href = str_replace("'",'',$href);
            $href = str_replace("]",'',$href);
            $words[$i] = ReelsBlockQuote($href);
            unset($words[$i + 1]);
        }

        if (str_contains($word, '[tiktok')) {
            $href = str_replace("id=",'',$words[$i + 1]);
            $href = str_replace("'",'',$href);
            $href = str_replace("]",'',$href);
            $words[$i] = TikTokBlockQuote($href);
            unset($words[$i + 1]);
        }

        if (str_contains($word, '[youtube')) {
            $href = str_replace("id=",'',$words[$i + 1]);
            $href = str_replace("'",'',$href);
            $href = str_replace("]",'',$href);
            $words[$i] = YouTubeEmbed($href);
            unset($words[$i + 1]);
        }

        if (str_contains($word, '[fbvideo')) {
            $href = str_replace("url=",'',$words[$i + 1]);
            $href = str_replace("'",'',$href);
            $href = str_replace("]",'',$href);
            $words[$i] = FacebookVideoEmbed($href);
            unset($words[$i + 1]);
        }

        $i++;
      }

      return implode(" ", $words);
   }

function ReelsBlockQuote($id){
   	 return '<div style="text-align:center; margin: 20px 0;"><blockquote class="instagram-media" data-instgrm-permalink="https://www.instagram.com/reel/'.$id.'/?utm_source=ig_embed&amp;utm_campaign=loading" data-instgrm-version="14" style=" background:#FFF; border:0; border-radius:3px; box-shadow:0 0 1px 0 rgba(0,0,0,0.5),0 1px 10px 0 rgba(0,0,0,0.15); margin: 1px; max-width:540px; min-width:326px; padding:0; width:99.375%; width:-webkit-calc(100% - 2px); width:calc(100% - 2px);"><div style="padding:16px;"> <a href="https://www.instagram.com/reel/CgmpbBsDCU3/?utm_source=ig_embed&amp;utm_campaign=loading" style=" background:#FFFFFF; line-height:0; padding:0 0; text-align:center; text-decoration:none; width:100%;" target="_blank"> <div style=" display: flex; flex-direction: row; align-items: center;"> <div style="background-color: #F4F4F4; border-radius: 50%; flex-grow: 0; height: 40px; margin-right: 14px; width: 40px;"></div> <div style="display: flex; flex-direction: column; flex-grow: 1; justify-content: center;"> <div style=" background-color: #F4F4F4; border-radius: 4px; flex-grow: 0; height: 14px; margin-bottom: 6px; width: 100px;"></div> <div style=" background-color: #F4F4F4; border-radius: 4px; flex-grow: 0; height: 14px; width: 60px;"></div></div></div><div style="padding: 19% 0;"></div> <div style="display:block; height:50px; margin:0 auto 12px; width:50px;"><svg width="50px" height="50px" viewBox="0 0 60 60" version="1.1" xmlns="https://www.w3.org/2000/svg" xmlns:xlink="https://www.w3.org/1999/xlink"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><g transform="translate(-511.000000, -20.000000)" fill="#000000"><g><path d="M556.869,30.41 C554.814,30.41 553.148,32.076 553.148,34.131 C553.148,36.186 554.814,37.852 556.869,37.852 C558.924,37.852 560.59,36.186 560.59,34.131 C560.59,32.076 558.924,30.41 556.869,30.41 M541,60.657 C535.114,60.657 530.342,55.887 530.342,50 C530.342,44.114 535.114,39.342 541,39.342 C546.887,39.342 551.658,44.114 551.658,50 C551.658,55.887 546.887,60.657 541,60.657 M541,33.886 C532.1,33.886 524.886,41.1 524.886,50 C524.886,58.899 532.1,66.113 541,66.113 C549.9,66.113 557.115,58.899 557.115,50 C557.115,41.1 549.9,33.886 541,33.886 M565.378,62.101 C565.244,65.022 564.756,66.606 564.346,67.663 C563.803,69.06 563.154,70.057 562.106,71.106 C561.058,72.155 560.06,72.803 558.662,73.347 C557.607,73.757 556.021,74.244 553.102,74.378 C549.944,74.521 548.997,74.552 541,74.552 C533.003,74.552 532.056,74.521 528.898,74.378 C525.979,74.244 524.393,73.757 523.338,73.347 C521.94,72.803 520.942,72.155 519.894,71.106 C518.846,70.057 518.197,69.06 517.654,67.663 C517.244,66.606 516.755,65.022 516.623,62.101 C516.479,58.943 516.448,57.996 516.448,50 C516.448,42.003 516.479,41.056 516.623,37.899 C516.755,34.978 517.244,33.391 517.654,32.338 C518.197,30.938 518.846,29.942 519.894,28.894 C520.942,27.846 521.94,27.196 523.338,26.654 C524.393,26.244 525.979,25.756 528.898,25.623 C532.057,25.479 533.004,25.448 541,25.448 C548.997,25.448 549.943,25.479 553.102,25.623 C556.021,25.756 557.607,26.244 558.662,26.654 C560.06,27.196 561.058,27.846 562.106,28.894 C563.154,29.942 563.803,30.938 564.346,32.338 C564.756,33.391 565.244,34.978 565.378,37.899 C565.522,41.056 565.552,42.003 565.552,50 C565.552,57.996 565.522,58.943 565.378,62.101 M570.82,37.631 C570.674,34.438 570.167,32.258 569.425,30.349 C568.659,28.377 567.633,26.702 565.965,25.035 C564.297,23.368 562.623,22.342 560.652,21.575 C558.743,20.834 556.562,20.326 553.369,20.18 C550.169,20.033 549.148,20 541,20 C532.853,20 531.831,20.033 528.631,20.18 C525.438,20.326 523.257,20.834 521.349,21.575 C519.376,22.342 517.703,23.368 516.035,25.035 C514.368,26.702 513.342,28.377 512.574,30.349 C511.834,32.258 511.326,34.438 511.181,37.631 C511.035,40.831 511,41.851 511,50 C511,58.147 511.035,59.17 511.181,62.369 C511.326,65.562 511.834,67.743 512.574,69.651 C513.342,71.625 514.368,73.296 516.035,74.965 C517.703,76.634 519.376,77.658 521.349,78.425 C523.257,79.167 525.438,79.673 528.631,79.82 C531.831,79.965 532.853,80.001 541,80.001 C549.148,80.001 550.169,79.965 553.369,79.82 C556.562,79.673 558.743,79.167 560.652,78.425 C562.623,77.658 564.297,76.634 565.965,74.965 C567.633,73.296 568.659,71.625 569.425,69.651 C570.167,67.743 570.674,65.562 570.82,62.369 C570.966,59.17 571,58.147 571,50 C571,41.851 570.966,40.831 570.82,37.631"></path></g></g></g></svg></div><div style="padding-top: 8px;"> <div style=" color:#3897f0; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:550; line-height:18px;">View this post on Instagram</div></div><div style="padding: 12.5% 0;"></div> <div style="display: flex; flex-direction: row; margin-bottom: 14px; align-items: center;"><div> <div style="background-color: #F4F4F4; border-radius: 50%; height: 12.5px; width: 12.5px; transform: translateX(0px) translateY(7px);"></div> <div style="background-color: #F4F4F4; height: 12.5px; transform: rotate(-45deg) translateX(3px) translateY(1px); width: 12.5px; flex-grow: 0; margin-right: 14px; margin-left: 2px;"></div> <div style="background-color: #F4F4F4; border-radius: 50%; height: 12.5px; width: 12.5px; transform: translateX(9px) translateY(-18px);"></div></div><div style="margin-left: 8px;"> <div style=" background-color: #F4F4F4; border-radius: 50%; flex-grow: 0; height: 20px; width: 20px;"></div> <div style=" width: 0; height: 0; border-top: 2px solid transparent; border-left: 6px solid #f4f4f4; border-bottom: 2px solid transparent; transform: translateX(16px) translateY(-4px) rotate(30deg)"></div></div><div style="margin-left: auto;"> <div style=" width: 0px; border-top: 8px solid #F4F4F4; border-right: 8px solid transparent; transform: translateY(16px);"></div> <div style=" background-color: #F4F4F4; flex-grow: 0; height: 12px; width: 16px; transform: translateY(-4px);"></div> <div style=" width: 0; height: 0; border-top: 8px solid #F4F4F4; border-left: 8px solid transparent; transform: translateY(-4px) translateX(8px);"></div></div></div> <div style="display: flex; flex-direction: column; flex-grow: 1; justify-content: center; margin-bottom: 24px;"> <div style=" background-color: #F4F4F4; border-radius: 4px; flex-grow: 0; height: 14px; margin-bottom: 6px; width: 224px;"></div> <div style=" background-color: #F4F4F4; border-radius: 4px; flex-grow: 0; height: 14px; width: 144px;"></div></div></a><p style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; line-height:17px; margin-bottom:0; margin-top:8px; overflow:hidden; padding:8px 0 7px; text-align:center; text-overflow:ellipsis; white-space:nowrap;"><a href="https://www.instagram.com/reel/CgmpbBsDCU3/?utm_source=ig_embed&amp;utm_campaign=loading" style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:normal; line-height:17px; text-decoration:none;" target="_blank">A post shared by Plating with Perel (@platingwithperel)</a></p></div></blockquote> <script async src="//www.instagram.com/embed.js"></script></div>';
}

function getWishlistString($uid){
    $wishlists = \App\Models\Wishlist::where('uid', $uid)->where("archived", 0)->get();
    $string = "I'd like to visit ";

    $i = 1;
    foreach($wishlists as $wishlist){
        if($i == sizeof($wishlists) && sizeof($wishlists) > 1){
            $string .= "and $wishlist->destination";
        }
        if($i == 1 && sizeof($wishlists) <= 2){
            $string .= "$wishlist->destination ";
        }
        if($i > 1 && sizeof($wishlists) > 2){
            $string .= "$wishlist->destination, ";
        }
        $i++;
    }
    if($string == "I'd like to visit "){
        return "";
    }
    return $string;
}

function parseShortCodeStory($string){
    $words = explode(" ", $string);
    $i = 0;

    foreach($words as $word){
        if (str_contains($word, '[link')) {
            $href = str_replace("url=",'',$words[$i + 1]);
            $href = str_replace("'",'',$href);
            $label = str_replace("label=",'',$words[$i + 2]);
            $label = str_replace("'",'',$label);
            $label = str_replace("]",'',$label);
            $words[$i] = ""; //"<a href='$href' target='_blank' class='text-success'>$label</a>";
            unset($words[$i + 1]);
            unset($words[$i + 2]);
        }

        if (str_contains($word, '[gif')) {
            $href = str_replace("url=",'',$words[$i + 1]);
            $href = str_replace("'",'',$href);
            $href = str_replace("]",'',$href);
            $words[$i] = "";
            unset($words[$i + 1]);
        }

        if (str_contains($word, '[reel')) {
            $href = str_replace("id=",'',$words[$i + 1]);
            $href = str_replace("'",'',$href);
            $href = str_replace("]",'',$href);
            $words[$i] = ""; //"<a href='https://instagram.com/reels/$href' target='_blank' class='text-success'>REEL</a>";
            unset($words[$i + 1]);
        }

        if (str_contains($word, '[tiktok')) {
            $href = str_replace("id=",'',$words[$i + 1]);
            $href = str_replace("'",'',$href);
            $href = str_replace("]",'',$href);
            $words[$i] = ""; //"<a href='https://tiktok.com/$href' target='_blank' class='text-success'>TIK TOK</a>";
            unset($words[$i + 1]);
        }

        if (str_contains($word, '[youtube')) {
            $href = str_replace("id=",'',$words[$i + 1]);
            $href = str_replace("'",'',$href);
            $href = str_replace("]",'',$href);
            $words[$i] = ""; //YouTubeEmbed($href);
            unset($words[$i + 1]);
        }
    }

    return implode(" ", $words);
}

function getFirstName($full_name){
    $name = explode("|", $full_name);
    if(!str_contains($full_name, "|")){
        $name = explode(" ", $full_name);
    }
    $name = $name[0];
    return $name;
}

function getLastName($full_name){
    $name = explode("|", $full_name);
    if(!str_contains($full_name, "|")){
        $name = explode(" ", $full_name);
    }
    $name = $name[1] ?? "";
    return $name;
}

function cleanName($full_name){
    $name = str_replace("|"," ",$full_name);
    return $name;
}

function getTripLocation($destination, $country){
    if($destination == $country){
        return $destination;
    }

    return "$destination, $country";
}

function getTripDate($departure){
    return \Carbon\Carbon::parse($departure)->format("M. d, Y");
}

function getTripTitle($title, $days, $destination){
    if($title == "" || trim($title) == "" || is_null($title) || strlen($title) <= 11){
        return $days . " Days in " . $destination;
    }
    return $title;
}

function getGroupRole($uid, $group){
    $key = array_search($uid, array_column($group["group"], 'uid'));
    $role = $group["group"][$key]["role"];
    return $role;
}

function getGroupSize($groupMembers){
    $size = 0;

    foreach($groupMembers as $groupMember){
        if($groupMember["role"] == "viewer" || $groupMember["role"] == "owner"){
            $size++;
        }
    }

    return $size;
}

function cleanFileName($string){
    return str_replace(" ", "-", $string);
}

function cleanString($string){
    $string = str_replace("'","",$string);
    return strtolower(str_replace(" ", "_", $string));
}

function calculateAge($dob){
    return Carbon::parse($dob)->diff(Carbon::now())->format('%y');
}

function calculateOccupation($occupation){
    if($occupation != ""){
        return "<span style='font-size:14px;'>$occupation</span>";
    }
}

function calculateGender($gender){
    switch ($gender) {
        case 'men':
            return '<li>Male</li>';
            break;
        case 'women':
            return '<li>Woman</li>';
        default:
            // code...
            break;
    }
}

function calculateSmoking($smoking){
    switch ($smoking) {
        case 'none':
            return "<li>Non-Smoker</li>";
            break;
        case 'socially':
            return "<li>Smokes Socially</li>";
            break;
        case 'often':
            return "<li>Smokes Often</li>";
            break;
        default:
            // code...
            break;
    }
}

function calculateDrinking($drinking){
    switch ($drinking) {
        case 'none':
            return "<li>Non-Drinker</li>";
            break;
        case 'socially':
            return "<li>Drinks Socially</li>";
            break;
        case 'often':
            return "<li>Drinks Often</li>";
            break;
        default:
            // code...
            break;
    }
}

function validateUsername($username, $db){

    try{
        $username = $db->collection('Usernames')->document($username)->snapshot()->data();
        if($username === null){
            return false;
        } else {
            return $username;
        }
    } catch(Exception $e){
        return false; // username is unavailable
    }
}

function calcCompatibility($match, $matchee){
    
    if(!isProfileComplete($match)[0] || !isProfileComplete((object) $matchee)[0]){
        // terminate calculation because both profiles are not complete
        return "nocompat 0";
    }

    // define vars for comparison. 
    $mid = $match->uid;
    $match = $match->customClaims; // Person A


    if(is_array($matchee)){
        $mide = $matchee["uid"];
        $matchee = $matchee["customClaims"]; 
    } else {
        $mide = $matchee->uid;
        $matchee = $matchee->customClaims;
     // Person B
    }

    

    $totalPoints = $points = 0; // By default, we assume no compatibility. Max Points are calculated based on Person A. Points are accrued by Person B



    $matchGenderPref = $match["genderPref"];

    if($matchGenderPref == "No Preference"){
        $matchGenderPref = "none";
    }

    $matcheeGenderPref = $matchee["genderPref"];

    if($matcheeGenderPref == "No Preference"){
        $matcheeGenderPref = "none";
    }

    $matchGender = $match["gender"];


    if($matchGender == "Male"){
        $matchGender = "men";
    }
    if($matchGender == "Female"){
        $matchGender = "women";
    }
    if($matchGender == "Prefer Not to Say"){
        $matchGender = "none";
    }

    $matcheeGender = $matchee["gender"];

    if($matcheeGender == "Male"){
        $matcheeGender = "men";
    }
    if($matcheeGender == "Female"){
        $matcheeGender = "women";
    }
    if($matcheeGender == "Prefer Not to Say"){
        $matcheeGender = "none";
    }

    $matchSmokingPref = $match["smokingPref"];

    if($matchSmokingPref == "No Preference"){
        $matchSmokingPref = "none";
    }
    if($matchSmokingPref == "Non-Smokers Only"){
        $matchSmokingPref = "no";
    }
    if($matchSmokingPref == "Smokers Okay"){
        $matchSmokingPref = "okay";
    }

    $matcheeSmokingPref = $matchee["smokingPref"];

    if($matcheeSmokingPref == "No Preference"){
        $matcheeSmokingPref = "none";
    }
    if($matcheeSmokingPref == "Non-Smokers Only"){
        $matcheeSmokingPref = "no";
    }
    if($matcheeSmokingPref == "Smokers Okay"){
        $matcheeSmokingPref = "okay";
    }

    $matchSmoking = $match["smoking"];

    if($matchSmoking == "I Don't Smoke"){
        $matchSmoking = "none";
    }
    if($matchSmoking == "I Smoke Socially"){
        $matchSmoking = "socially";
    }
    if($matchSmoking == "I Smoke Often"){
        $matchSmoking = "often";
    }

    $matcheeSmoking = $matchee["smoking"];

    if($matcheeSmoking == "I Don't Smoke"){
        $matcheeSmoking = "none";
    }
    if($matcheeSmoking == "I Smoke Socially"){
        $matcheeSmoking = "socially";
    }
    if($matcheeSmoking == "I Smoke Often"){
        $matcheeSmoking = "often";
    }

    $matchDrinkingPref = $match["drinkingPref"];

    if($matchDrinkingPref == "No Preference"){
        $matchDrinkingPref = "none";
    }
    if($matchDrinkingPref == "Non-Drinkers Only"){
        $matchDrinkingPref = "no";
    }
    if($matchDrinkingPref == "Drinkers Okay"){
        $matchDrinkingPref = "okay";
    }

    $matcheeDrinkingPref = $matchee["drinkingPref"];

    if($matcheeDrinkingPref == "No Preference"){
        $matcheeDrinkingPref = "none";
    }
    if($matcheeDrinkingPref == "Non-Drinkers Only"){
        $matcheeDrinkingPref = "no";
    }
    if($matcheeDrinkingPref == "Drinkers Okay"){
        $matcheeDrinkingPref = "okay";
    }

    $matchDrinking = $match["drinking"];

    if($matchDrinking == "I Don't Drink"){
        $matchDrinking = "none";
    }
    if($matchDrinking == "I Drink Socially"){
        $matchDrinking = "socially";
    }
    if($matchDrinking == "I Drink Often"){
        $matchDrinking = "often";
    }

    $matcheeDrinking = $matchee["drinking"];

    if($matcheeDrinking == "I Don't Drink"){
        $matcheeDrinking = "none";
    }
    if($matcheeDrinking == "I Drink Socially"){
        $matcheeDrinking = "socially";
    }
    if($matcheeDrinking == "I Drink Often"){
        $matcheeDrinking = "often";
    }

    $matchSizePref = $match["sizePref"];

    if($matchSizePref == "No Preference"){
        $matchSizePref = "none";
    }
    if($matchSizePref == "Four Or Less"){
        $matchSizePref = "small";
    }
    if($matchSizePref == "No More Than 10"){
        $matchSizePref = "medium";
    }
    if($matchSizePref == "No More Than 20"){
        $matchSizePref = "large";
    }

    $matcheeSizePref = $matchee["sizePref"];

    if($matcheeSizePref == "No Preference"){
        $matcheeSizePref = "none";
    }
    if($matcheeSizePref == "Four Or Less"){
        $matcheeSizePref = "small";
    }
    if($matcheeSizePref == "No More Than 10"){
        $matcheeSizePref = "medium";
    }
    if($matcheeSizePref == "No More Than 20"){
        $matcheeSizePref = "large";
    }

    $matchSpendingPref = $match["spendingPref"];

    if($matchSpendingPref == "No Preference"){
        $matchSpendingPref = "none";
    }
    if($matchSpendingPref == "Budget Conscience"){
        $matchSpendingPref = "small";
    }
    if($matchSpendingPref == "Enjoys Comfort and Experience"){
        $matchSpendingPref = "medium";
    }
    if($matchSpendingPref == "Prefers Luxury"){
        $matchSpendingPref = "large";
    }

    $matcheeSpendingPref = $matchee["spendingPref"];

    if($matcheeSpendingPref == "No Preference"){
        $matcheeSpendingPref = "none";
    }
    if($matcheeSpendingPref == "Budget Conscience"){
        $matcheeSpendingPref = "small";
    }
    if($matcheeSpendingPref == "Enjoys Comfort and Experience"){
        $matcheeSpendingPref = "medium";
    }
    if($matcheeSpendingPref == "Prefers Luxury"){
        $matcheeSpendingPref = "large";
    }

    $matchAgePref = $match["agePref"];

    if($matchAgePref == "No Preference"){
        $matchAgePref = "none";
    }

    if($matchAgePref == "Within 4 Years"){
        $matchAgePref = "small";
    }

    if($matchAgePref == "Within 8 Years"){
        $matchAgePref = "medium";
    }

    if($matchAgePref == "Within 6 Years"){
        $matchAgePref = "large";
    }

    $matcheeAgePref = $matchee["agePref"];

    if($matcheeAgePref == "No Preference"){
        $matcheeAgePref = "none";
    }

    if($matcheeAgePref == "Within 4 Years"){
        $matcheeAgePref = "small";
    }

    if($matcheeAgePref == "Within 8 Years"){
        $matcheeAgePref = "medium";
    }

    if($matcheeAgePref == "Within 6 Years"){
        $matcheeAgePref = "large";
    }
    
    // Convert mobile app to web app vars for uniformity
    
    

    // gender is a deal breaker. If someone is not interested in traveling with the opposite sex, return nothing

    // For each category, we have to base it on 

    if($matcheeGenderPref != "none" || $matchGenderPref != "none"){
        if($matchGender != $matcheeGender){
            return "nocompat 0";
        }
    }

    // if I don't want any smokers, and they smoke, deal break

    if($matchSmokingPref == "no" && $matcheeSmoking != "none"){
        return "nocompat 0";
    }
    // vice-versa, if I smoke and they don't want a smoker
    if($matcheeSmokingPref == "no" && $matchSmoking != "none"){
        return "nocompat 0";
    }


    // same with drinking

    if($matchDrinkingPref == "no" && $matcheeDrinking != "none"){
       return "nocompat 0";
    }
    if($matcheeDrinkingPref == "no" && $matchDrinking != "none"){
        return "nocompat 0";
    }

    // we calculate the maximum number of points Person A could impose evaluation on Person B

    if($matchGenderPref == "none"){
        $points += 4;
        $totalPoints += 4;
    }
    if($matchSmokingPref == "none"){
        // people who don't smoke may have a much lower tolerance compared to drinking
        $points += 8;
        $totalPoints += 8;
    }
    if($matchDrinkingPref == "none"){
        $points += 4;
        $totalPoints += 4;
    }
    if($matchSpendingPref == "none"){
        // different spending styles can really divide a group!
        $points += 10;
        $totalPoints += 10;
    }
    if($matchSizePref == "none"){
        // slightly more critical than if someone drinks or not
        $points += 6;
        $totalPoints += 6;
    }

    // if the Person A doesn't care who they match with, the totalPoints by now is 32
    // now we allow Person B to accrue points
    // first we tackle the low-hanging fruit
    // if there is an exact match, they get all available points for the category

    if($matchGenderPref == $matcheeGenderPref){
        $points += 4;
        $totalPoints += 4;
    }

    if($matchSmokingPref == $matcheeSmokingPref){
        $points += 4;
        $totalPoints += 4;
    }

    if($matchDrinkingPref == $matcheeDrinkingPref){
        $points += 4;
        $totalPoints += 4;
    }

    if($matchSpendingPref == $matcheeSpendingPref){
        $points += 10;
        $totalPoints += 10;
    }

    if($matchSizePref == $matcheeSizePref){
        $points += 6;
        $totalPoints += 6;
    }

    // new 28 points brings total to 60
    // even if they're not looking for the same gender, we award some points for being the same

    if($matchGender == $matcheeGender){
        $totalPoints += 4;
        $points += 4;
    }

    // 64

    // smoke buddies

    if($matchSmoking == $matcheeSmoking){
        $totalPoints += 4;
        $points += 4;
    }

    // drinking level buddies

    if($matchDrinking == $matcheeDrinking){
        $points += 4;
        $totalPoints += 4;
    }

    // spiritual buddies

    if(array_key_exists("religion",$match) && array_key_exists("religion", $matchee)){
        if(strtolower($match["religion"]) == strtolower($matchee["religion"])){
            $points += 4;
            $totalPoints += 4;
        }
    }

    

    // +12 for both sides. 
    // low hanging fruit done.
    // by now the max accruable points is 76

    if(array_key_exists("dob", $match) && array_key_exists("dob", $matchee)){
        $matchAge = calculateAge($match["dob"]);
        $matcheeAge = calculateAge($matchee["dob"]);
        $ageDif = abs($matcheeAge - $matchAge);

        if($matchAge == $matcheeAge){
            // if we are the same age, it's a big plus
            $points += 6;
            $totalPoints += 6;
        } else {
            $points += 4;
            $totalPoints += 6;
            // lose some points for not being exact
        }

        // 82 points total

        switch ($matchAgePref) {
            case 'small':
                if(isNumberBetween($matchAge, 4, $matcheeAge)){
                    // we're not the same age
                    $points += 2;
                    $totalPoints += 2;
                } else {
                    $totalPoints += 2;
                    $points = $points * 0.75;
                }
                break;
            case 'medium':
                if(isNumberBetween($matchAge, 8, $matcheeAge)){
                    $points += 2;
                    $totalPoints += 2;
                } else {
                    $totalPoints += 2;
                    $points = $points * 0.85;
                }
                break;
            
            case 'large';
                if(isNumberBetween($matchAge, 16, $matcheeAge)){
                    $points += 2;
                    $totalPoints += 2;
                } else {
                    $totalPoints += 2;
                    $points = $points * 0.95;
                }
                break;
            default:
                // code...
                break;
        }

        // 84 total

        switch ($matcheeAgePref) {
            case 'small':
                if(isNumberBetween($matcheeAge, 4, $matchAge)){
                    $points += 2;
                    $totalPoints += 2;
                } else {
                    $points = $points * 0.75;
                    $totalPoints += 2;
                }
                break;
            case 'medium':
                if(isNumberBetween($matcheeAge, 8, $matchAge)){
                    $points += 2;
                    $totalPoints += 2;
                }else {
                    $points = $points * 0.85;
                    $totalPoints += 2;
                }
                break;
            
            case 'large';
                if(isNumberBetween($matcheeAge, 16, $matchAge)){
                    $points += 2;
                    $totalPoints += 2;
                } else {
                    $points = $points * 0.95;
                    $totalPoints += 2;
                }
                break;
            default:
                // code...
                break;
        }

        // 86 total
    }

    // now for hard disconnects

    if($matchSizePref == "small" && $matcheeSizePref == "large"){
        $points = $points * 0.75;
    }
    if($matchSizePref == "medium" && $matcheeSizePref != "medium"){
        $points = $points * 0.9;
    }
    if($matcheeSizePref == "small" && $matchSizePref == "large"){
        $points = $points * 0.75;
    }
    if($matcheeSizePref == "medium" && $matchSizePref != "medium"){
        $points = $points * 0.9;
    }

    $score = round($points/$totalPoints,2) * 100;

    switch ($score) {
        case $score >= 90:
            return "good " . $score;
            break;
        case $score < 90 && $score >= 72:
            return "workable " . $score;
            break;
        case $score == 0:
            return "nocompat 0";
            break;
        default:
            return "nocompat " . $score;
            break;
    }
    
}

function organizeMembersList($members){

    $finalMembers = [];

    if(!array_key_exists(0, $members)){
        array_push($finalMembers, $members);
        return $finalMembers;
    }

    for($i = 0; $i <= sizeof($members); $i++){
        if($i == 0){
            array_push($finalMembers, $members[$i]);
            unset($members[$i]);
        } else{
            if($members[$i]["role"] == "viewer"){
                array_push($finalMembers, ["role" => "viewer", "uid" => $members[$i]["uid"]]);
                unset($members[$i]);
            }
        }
    }
    $finalMembers = array_merge($finalMembers, $members);
    return $finalMembers;
}

function isProfileComplete($user){

    if(array_key_exists("dob", $user->customClaims)){
        if($user->customClaims["dob"] == "" || calculateAge($user->customClaims["dob"]) < 18){
            return [false, "Venti users must be at least 18 years of age"];
        }
    } else{
        return [false,'Add your date of birth to your <a href="/user/profile">your profile</a>'];
    }

    if($user->photoUrl == "" || $user->photoUrl == "/assets/img/profile.jpg" || $user->photoUrl == "/assets/img/profile-2.jpg"){
        return [false, 'Add a headshot to <br><a href="/user/profile">your profile</a>'];
    }

    if(!$user->emailVerified){
        return [false,'<a href="/user/profile">Verify your email</a>'];
    }

    return [true, ""];
}

function isNumberBetween($init, $range, $test){
    $upper = $init + $range;
    $lower = $init - $range;

    if($test >= $lower && $test <= $upper){
        return true;
    }

    return false;
}

function cleanCountry($country){
    $country = trim(str_replace(" ", "", $country));
    return $country;
}

function displayTextWithLinks($s) {
  return preg_replace('@(https?://([-\w\.]+[-\w])+(:\d+)?(/([\w/_\.#-]*(\?\S+)?[^\.\s])?)?)@', '<a href="$1" target="_blank">$1</a>', $s);
}

function getRandomColor(){
    $letters = '0123456789ABCDEF';
    $color = '#';
    for ($i = 0; $i < 6; $i++) {
        $color .= $letters[rand(0, 15)];
    }
    return $color;
}

function generateRandomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

function move_to_top(&$array, $key) {
    $temp = array($key => $array[$key]);
    unset($array[$key]);
    $array = $temp + $array;
    return array_values($array);
}

function convertReligion($religion){
    switch ($religion) {
        case null:
            return null;
            break;
        case "none":
            return "No Affiliation";
            break;
        case 1:
            return "No Affiliation";
            break;
        case 2:
            return "Christian";
            break;
        case 3:
            return "Catholic";
            break;
        case 4:
            return "Sikh";
            break;
        case 5:
            return "Jewish";
            break;
        case 6:
            return "Muslim";
            break;
        case 7:
            return "Hindu";
            break;
        case 8:
            return "Buddhist";
            break;
        case 9:
            return "Spiritual";
            break;
        case 10:
            return "Atheist";
            break;
        case 11:
            return "Agnostic";
            break;
    }
}

function interestEmoji($interest){
    switch ($interest) {
        case "Hiking":
            return "🥾";
            break;
        case "Camping":
            return "🏕️";
            break;
        case "Surfing":
            return "🌊";
            break;
        case "Scuba":
            return "🤿";
            break;
        case "Kayaking/Rafting":
            return "🛶";
            break;
        case "Beach":
            return "🏖️";
            break;
        case "Dancing":
            return "🕺💃";
            break;
        case "Museums":
            return "🏛️";
            break;
        case "Tours":
            return "🗽";
            break;
        case "Foodie":
            return "🍱";
            break;
        case "Photography":
            return "📸";
            break;
        case "Music":
            return "🎸";
            break;
        case "Backpacking":
            return "🎒";
            break;
        case "Gambling":
            return "🎲";
            break;
        case "Workouts":
            return "💪";
            break;
        case "Drinks":
            return "🥃";
            break;
        case "Smoking":
            return "💨";
            break;
        case "Golfing":
            return "⛳";
            break;
        case "Meeting Locals":
            return "👋";
            break;
        case "Shopping":
            return "🛍️";
            break;
        case "Relaxing":
            return "💆‍♀️";
            break;
        case "Wine Tasting":
            return "🍷";
            break;
        case "Wildlife":
            return "🐒";
            break;
    }
}

function interestsToObject($interestSelections){

    $result = [];

    $interests = [
        ["key" => "1", "value" => "🥾 Hiking"],
        ["key" => "2", "value" => "🏕️ Camping"],
        ["key" => "3", "value" => "🌊 Surfing"],
        ["key" => "4", "value" => "🤿 Scuba"],
        ["key" => "5", "value" => "🛶 Kayaking/Rafting"],
        ["key" => "6", "value" => "🏖️ Beach"],
        ["key" => "7", "value" => "🕺💃 Dancing"],
        ["key" => "8", "value" => "🏛️ Museums"],
        ["key" => "9", "value" => "🗽 Tours"],
        ["key" => "10", "value" => "🍱 Foodie"],
        ["key" => "11", "value" => "📸 Photography"],
        ["key" => "12", "value" => "🎸 Music"],
        ["key" => "13", "value" => "🎒 Backpacking"],
        ["key" => "14", "value" => "🎲 Gambling"],
        ["key" => "15", "value" => "💪 Workouts"],
        ["key" => "16", "value" => "🥃 Drinks"],
        ["key" => "17", "value" => "💨 Smoking"],
        ["key" => "18", "value" => "⛳ Golfing"],
        ["key" => "19", "value" => "👋 Meeting Locals"],
        ["key" => "20", "value" => "🛍️ Shopping"],
        ["key" => "21", "value" => "💆‍♀️ Relaxing"],
        ["key" => "22", "value" => "🍷 Wine Tasting"],
        ["key" => "23", "value" => "🐒 Wildlife"]
    ];

    if($interestSelections == null){
        return $result;
    }

    foreach($interestSelections as $interestSelection){
        foreach ($interests as $interest){
            if($interestSelection == $interest["value"]){
                $interest["isChecked"] = true;
                array_push($result, $interest);
            }
        }
    }

    return $result;
}

function getQuizEmoji($category){

    
    switch($category){
        case $category == "classic_spending" || $category == "spending_classic":
            return ["Boujie 🍷","You like to stick to familiar activities but are willing to spend a bit more money for new, quality experiences. Whether traveling solo or with others, you prefer luxury and comfort when given a chance."];
            break;
        case $category == "classic_adventure" || $category == "adventure_classic":
            return ["Sightseer 🥾","You prefer to spend as much time as possible outside of your room. You want to see as much as possible during your trip and keep things moving constantly."];
            break;
        case $category == "classic_taboo" || $category == "taboo_classic":
            return ["Playful 🐒","You're sometimes the funny one or that wants to have a good time in almost any environment. You don't mind going off the beaten path and trying new things even if they seem a bit scary at first."];
            break;
        case $category == "classic_peaceful" || $category == "peaceful_classic":
            return ["Peacekeeper 😇","You like to play it safe, keep things organized, and stay far away from drama as possible. You like to stick to familiar activites and experiences even in new places."];
            break;
        case $category == "spending_adventure" || $category == "adventure_spending":
            return ["Adventurer 🛶","You embody the \"you only live once\" mantra and seek truly unforgettable experiences and will spend what it takes to get them. You've likely traveled to more countries than most of your friends and have the coolest travel stories." ];
            break;
        case $category == "spending_taboo" || $category == "taboo_spending":
            return ["The Partier 🎸","You're the one that's always looking to find the party or the next fun thing to do. Sitting around absolutely bores you and you prefer to keep it moving!"];
            break;
        case $category == "spending_peaceful" || $category == "peaceful_spending":
            return ["Relaxed 🏖️","You're always ready to bum out by a beach with a beverage in hand. You prefer calmness and tranquility and will spend the extra dollar for comfort and convenience"];
            break;
        case $category == "adventure_taboo" || $category == "taboo_adventure":
            return ["Daredevil 😈","You're the wild and fun one. You like to be the life of the party and get everyone to have a blast... even if it means taking some questionable risks. "];
            break;
        case $category == "adventure_peaceful" || $category == "peaceful_adventure":
            return ["Zen 💆‍♀️","You like new experiences that promote your overall wellbeing. You already have enough bad energy to deal with on a daily basis, you want you trip to recharge your soul while. You prefer to have fun but without the drama."];
            break;
        case $category == "taboo_peaceful" || $category == "peaceful_taboo":
            return ["Smooth 🥃","You like to keep it chill and can be somewhat mysterious. You tend to blend well into just about any vibe, as long as it's not crazy or weird."];
            break;
    }
}

function getQuizProfiles($category){

}

function isRecommendable($user){
    if($user->disabled == true || $user->customClaims == null || sizeof($user->customClaims) == 0){
        return false;
    }
    if(!array_key_exists("interests", $user->customClaims) || 
        !array_key_exists("bio", $user->customClaims) ||
        sizeof($user->customClaims["interests"]) == 0 ||
        strlen($user->customClaims["bio"]) < 4
    ){
        return false;
    }

    return true;
}

function languagesToObject($languageSelections){

    $result = [];

    $languages = [
        ["key" => 1, "value" =>'All'],
        ["key" => 2, "value" =>'English'],
        ["key" => 3, "value" =>'Spanish'],
        ["key" => 4, "value" =>'Mandarin'],
        ["key" => 5, "value" =>'Hindi'],
        ["key" => 6, "value" =>'French'],
        ["key" => 7, "value" =>'Arabic'],
        ["key" => 8, "value" =>'Russian'],
        ["key" => 9, "value" =>'Portuguese'],
        ["key" => 10, " value" => 'Bengali'],
        ["key" => 11, " value" => 'Indonesian'],
        ["key" => 12, " value" => 'Urdu'],
        ["key" => 13, " value" => 'German']
    ];

    if($languageSelections == null){
        return $result;
    }

    foreach($languageSelections as $languageSelection){
        foreach ($languages as $language){
            if(array_key_exists("value", $language)){
                if($languageSelection == $language["value"]){
                    $language["isChecked"] = true;
                    array_push($result, $language);
                }
            }
            
        }
    }

    return $result;
}

function tagsToObject($tagSelections){

    $result = [];

    $tags = [
        ["key" => 1, "label" => "Plenty of Exercise", "value" => "Plenty of Exercise",  "isChecked" => true],
        ["key" => 2, "label" => "Light Activity", "value" => "Light Activity",  "isChecked" => true],
        ["key" => 3, "label" => "Nature Adventure", "value" => "Nature Adventure",  "isChecked" => true],
        ["key" => 4, "label" => "Major Savings", "value" => "Major Savings",  "isChecked" => true],
        ["key" => 5, "label" => "Ecotourism", "value" => "Ecotourism",  "isChecked" => true],
        ["key" => 6, "label" => "Educational/Cultural", "value" => "Educational/Cultural",  "isChecked" => true],
        ["key" => 7, "label" => "Animals/Ecological", "value" => "Animals/Ecological",  "isChecked" => true],
        ["key" => 8, "label" => "Foodie", "value" => "Foodie",  "isChecked" => true],
        ["key" => 9, "label" => "Relaxing", "value" => "Relaxing",  "isChecked" => true],
        ["key" => 10, "label" => "Water Fun", "value" => "Water Fun",  "isChecked" => true],
        ["key" => 11, "label" => "Artsy", "value" => "Artsy",  "isChecked" => true],
        ["key" => 12, "label" => "Backpacker", "value" => "Backpacker",  "isChecked" => true],
        ["key" => 13, "label" => "For Couples", "value" => "For Couples",  "isChecked" => true],
        ["key" => 14, "label" => "For Friends", "value" => "For Friends",  "isChecked" => true],
        ["key" => 15, "label" => "Quick Getaway", "value" => "Quick Getaway",  "isChecked" => true],
        ["key" => 16, "label" => "Multiple Cities", "value" => "Multiple Cities",  "isChecked" => true],
        ["key" => 17, "label" => "Holiday", "value" => "Holiday",  "isChecked" => true],
        ["key" => 18, "label" => "Girls Trip", "value" => "Girls Trip",  "isChecked" => true],
        ["key" => 19, "label" => "Guys Trip", "value" => "Guys Trip",  "isChecked" => true],
        ["key" => 20, "label" => "Concerts/Music Festivals", "value" => "Concerts/Music Festivals",  "isChecked" => true]
    ];

    foreach($tagSelections as $tagSelection){
        foreach ($tags as $tag){
            if($tagSelection == $tag["value"]){
                array_push($result, $tag);
            }
        }
    }

    return $result;
}

function declineOptions($myRole, $type){
    if($type == "travel"){
        return "<option value='This invitation feels spammy'>This invitation feels spammy</option><option value='I don't want to visit this destination'>I don't want to visit this destination</option><option value='I'm not available during the travel dates'>I'm not available during the travel dates</option><option value='I'm not comfortable with this group'>I'm not comfortable with this group</option><option value='I have nothing in common with this group'>I have nothing in common with this group</option><option value='I'm not open to recommendations right now'>I'm not open to recommendations right now</option><option value='Simply not interested'>Simply not interested</option>";
    } else {
        switch ($myRole) {
            case "invited":
                return "<option value='This invitation feels spammy'>This invitation feels spammy</option><option value='I'm not comfortable with this group'>I'm not comfortable with this group</option><option value='I have nothing in common with this group'>I have nothing in common with this group</option><option value='I'm not open to recommendations right now'>I'm not open to recommendations right now</option><option value='Simply not interested'>Simply not interested</option>";
                break;
            case "recommended":
                return "<option value='I'm not comfortable with this group'>I'm not comfortable with this group</option><option value='I have nothing in common with this group'>I have nothing in common with this group</option><option value='I'm not open to recommendations right now'>I'm not open to recommendations right now</option><option value='Simply not interested'>Simply not interested</option>";
                break;
        }
    }
    
}

function countInvited($groupMembers){
    $invited = 0;

    foreach($groupMembers as $member){
        if($member["role"] == "invited"){
            $invited++;
        }
    }

    return $invited;
}

function getStringBetween(string $from, string $to, string $haystack): string
{
    $fromPosition = strpos($haystack, $from) + strlen($from);
    $toPosition = strpos($haystack, $to, $fromPosition);
    $betweenLength = $toPosition - $fromPosition;
    return substr($haystack, $fromPosition, $betweenLength);
}

function sampleItinerary($length){

    $titles = ["Escape to", "Highlights of", "Journey through", "Essential", "Adventure in", "Retreat to", "Cultural exploration of", "Unforgettable"];

    $randomTitle = $titles[rand(0,7)];

    switch ($length) {
        case 1:
            return "<h1>Day $randomTitle Dublin: Discovering the Best of Ireland
            </h1><h6>Are you ready for the best day trip to the stunning city of Dublin, Ireland? This itinerary will cover all the must-see destinations, amazing food, and unforgettable experiences in the heart of Ireland. From the buzz of Temple Bar to the taste of Guinness, this itinerary will help you make every moment count!
            </h6><h3>Day 1: Discover the Heart of Dublin
            </h3><p>We will start the day by exploring Dublin's historic landmarks and famous neighborhoods. Be prepared to walk a lot!
            </p><ul><li><strong>9:00 am - 10:00 am:
            </strong> Begin by visiting the impressive <place><a href='https://venti.co/place/St. Patrick's Cathedral in Dublin'>St. Patrick's Cathedral</a>
        </place>, one of Ireland's most iconic landmarks.
        </li><li><strong>10:30 am - 12:00 pm:
            </strong> Take a tour of <place><a href='https://venti.co/place/Dublin Castle in Dublin' target='_blank'>Dublin Castle
            </a></place>, the ancient fortress of the city, now a government office with beautiful state rooms.
        </li><li><strong>12:30 pm - 2:00 pm:
            </strong> Experience a traditional Irish pub for lunch in the heart of <food><a href='https://venti.co/place/Temple Bar in Dublin' target='_blank'>Temple Bar</a></food>, and enjoy live music and delicious traditional dishes. <time><a href='https://venti.co/map/Dublin Castle in Dublin/Temple Bar in Dublin' target='_blank'>~4 min. away from Dublin Castle</a></time>.
        </li><li><strong>2:30 pm - 4:00 pm:
            </strong> Take a stroll by the river Liffey and visit the museum of Dublin's most famous literary figure, James Joyce, at the <place><a href='https://venti.co/place/Dublin Writers Museum in Dublin' target='_blank'>Dublin Writers Museum</a></place>. <time><a href='https://venti.co/map/Temple Bar in Dublin/Dublin Writers Museum in Dublin' target='blank'> ~5 min from Templte Bar</a></time>
        </li><li><strong>4:30 pm - 6:30 pm:
            </strong> Immerse yourself in the best shopping spots at <place><a href='https://venti.co/place/Grafton Street in Dublin' target='_blank'>Grafton Street
            </a></place>, home to Ireland's famous businesses and high street stores.<time><a href='https://venti.co/map/Dublin Writers Museum in Dublin/Grafton Street in Dublin' target='_blank'>~5 min from Dublin Writers Museum</a></time>
        </li><li><strong>7:00 pm - 8:30 pm:
            </strong> Enjoy a pint of Guinness while learning the perfect pour at the famous <place><a href='https://venti.co/place/Guinness Storehouse in Dublin' target='_blank'>Guinness Storehouse</a></place>. Don't forget a panoramic view from the top-floor bar overlooking Dublin. <time><a href='https://venti.co/map/Grafton Street in Dublin/Guinness Storehouse in Dublin' target='_blank'>~10 min. from Grafton Street</a></time> 
        </li>
        </ul>";
            break;
        case 2:
            return "<h1>2-Day $randomTitle Dublin: Discovering the Best of Ireland
            </h1><h6>Are you ready for a 2-day trip to the stunning city of Dublin, Ireland? This itinerary will cover all the must-see destinations, amazing food, and unforgettable experiences in the heart of Ireland. From the buzz of Temple Bar to the taste of Guinness, this itinerary will help you make every moment count!
            </h6><h3>Day 1: Arrival and Settle In
            </h3><p>Start your Dublin adventure by arriving at <place><a href='https://venti.co/place/Dublin Airport in Dublin Ireland' target='_blank'>Dublin Airport
            </a>
        </place>. Dublin Airport is one of Europe's busiest airports and is located about 10 km north of Dublin City. We suggest taking a taxi or the Airlink Express bus, which takes about 30 minutes, to your hotel. Check-in starts at 3 pm, so feel free to take your time exploring before heading to your hotel. 
        </p><ul><li><strong>3:00 pm - 4:30 pm:
            </strong> Check-in to your hotel and take your time to settle in.
        </li><li><strong>5:00 pm - 6:30 pm:
            </strong> Stroll through <place><a href='https://venti.co/place/Trinity College' target='_blank'>Trinity College in Dublin
            </a>
        </place> and see one of the most impressive libraries in the world. 
        </li><li><strong>7:00 pm - 9:00 pm:
            </strong> Try classic Irish food at <food><a href='https://venti.co/place/The Lobster Pot restaurant in Dublin' target='_blank'>The Lobster Pot</a>
        </food> one of Dublin's oldest family-owned restaurants, serving traditional dishes since 1904. <time><a href='https://venti.co/map/Trinity College/The Lobster Pot restaurant in Dublin' target='_blank'>~15 min. from Trinity College
            </a>
        </time>.
        </li>
        </ul><h3>Day 2: Discover the Heart of Dublin
            </h3><p>We will start the day by exploring Dublin's historic landmarks and famous neighborhoods. Be prepared to walk a lot!
            </p><ul><li><strong>9:00 am - 10:00 am:
            </strong> Begin by visiting the impressive <place><a href='https://venti.co/place/St. Patrick's Cathedral in Dublin'>St. Patrick's Cathedral</a>
        </place>, one of Ireland's most iconic landmarks.
        </li><li><strong>10:30 am - 12:00 pm:
            </strong> Take a tour of <place><a href='https://venti.co/place/Dublin Castle in Dublin' target='_blank'>Dublin Castle
            </a></place>, the ancient fortress of the city, now a government office with beautiful state rooms. <time><a href='https://venti.co/map/St. Patrick's Cathedral in Dublin/Dublin Castle in Dublin' target='_blank'>~1 min. away from the Cathedral
            </a>
        </li><li><strong>12:30 pm - 2:00 pm:
            </strong> Experience a traditional Irish pub for lunch in the heart of <food><a href='https://venti.co/place/Temple Bar in Dublin' target='_blank'>Temple Bar</a></food>, and enjoy live music and delicious traditional dishes. <time><a href='https://venti.co/map/Dublin Castle in Dublin/Temple Bar in Dublin' target='_blank'>~4 min. away from Dublin Castle</a></time>
        </li><li><strong>2:30 pm - 4:00 pm:
        </strong> Take a stroll by the river Liffey and visit the museum of Dublin's most famous literary figure, James Joyce, at the <place><a href='https://venti.co/place/Dublin Writers Museum in Dublin' target='_blank'>Dublin Writers Museum</a></place>.
        </li><li><strong>4:30 pm - 6:30 pm:
            </strong> Immerse yourself in the best shopping spots at <place><a href='https://venti.co/place/Grafton Street in Dublin' target='_blank'>Grafton Street
            </a></place>, home to Ireland's famous businesses and high street stores.<time><a href='https://venti.co/map/Dublin Writers Museum in Dublin/Grafton Street in Dublin' target='_blank'>~5 min from Dublin Writers Museum</a></time> 
        </li><li><strong>7:00 pm - 8:30 pm:
            </strong> Enjoy a pint of Guinness while learning the perfect pour at the famous <place><a href='https://venti.co/place/Guinness Storehouse in Dublin' target='_blank'>Guinness Storehouse</a></place>. Don't forget a panoramic view from the top-floor bar overlooking Dublin. <time><a href='https://venti.co/map/Grafton Street in Dublin/Guinness Storehouse in Dublin' target='_blank'>~10 min. from Grafton Street</a></time> 
        </li>
        </ul><h3>Day 3: Departure
            </h3><p>It's time to wrap up your $randomTitle Dublin and start your journey home.
            </p><ul><li><strong>8:00 am - 9:00 am:
            </strong> Grab breakfast from the hotel lounge/cafe.
        </li><li><strong>10:00 am - 11:am pm
            </strong> Check out of your hotel. Depart from the hotel and take a taxi back to <place><a href='https://venti.co/place/Dublin Airport in Dublin Ireland' target='_blank'>Dublin Airport</a></place>, which is around 30 minutes drive.
        </li>
        </ul>";
            break;
        case 3:
            return "<h1>3-Day $randomTitle Dublin: Discovering the Best of Ireland
            </h1><h6>Are you ready for a 3-Day trip to the stunning city of Dublin, Ireland? This itinerary will cover all the must-see destinations, amazing food, and unforgettable experiences in the heart of Ireland. From the buzz of Temple Bar to the taste of Guinness, this itinerary will help you make every moment count!
            </h6><h3>Day 1: Arrival and Settle In
            </h3><p>Start your Dublin adventure by arriving at <place><a href='https://venti.co/place/Dublin Airport in Dublin Ireland' target='_blank'>Dublin Airport
            </a>
        </place>. Dublin Airport is one of Europe's busiest airports and is located about 10 km north of Dublin City. We suggest taking a taxi or the Airlink Express bus, which takes about 30 minutes, to your hotel. Check-in starts at 3 pm, so feel free to take your time exploring before heading to your hotel. 
        </p><ul><li><strong>3:00 pm - 4:30 pm:
            </strong> Check-in to your hotel and take your time to settle in.
        </li><li><strong>5:00 pm - 6:30 pm:
            </strong> Stroll through <place><a href='https://venti.co/place/Trinity College' target='_blank'>Trinity College in Dublin
            </a>
        </place> and see one of the most impressive libraries in the world. 
        </li><li><strong>7:00 pm - 9:00 pm:
            </strong> Try classic Irish food at <food><a href='https://venti.co/place/The Lobster Pot restaurant in Dublin' target='_blank'>The Lobster Pot</a>
        </food> one of Dublin's oldest family-owned restaurants, serving traditional dishes since 1904. <time><a href='https://venti.co/map/Trinity College/The Lobster Pot restaurant in Dublin' target='_blank'>~15 min. from Trinity College
            </a>
        </time>.
        </li>
        </ul><h3>Day 2: Discover the Heart of Dublin
            </h3><p>We will start the day by exploring Dublin's historic landmarks and famous neighborhoods. Be prepared to walk a lot!
            </p><ul><li><strong>9:00 am - 10:00 am:
            </strong> Begin by visiting the impressive <place><a href='https://venti.co/place/St. Patrick's Cathedral in Dublin'>St. Patrick's Cathedral</a>
        </place>, one of Ireland's most iconic landmarks.
        </li><li><strong>10:30 am - 12:00 pm:
            </strong> Take a tour of <a href='https://venti.co/place/Dublin Castle in Dublin'>Dublin Castle
            </a>, the ancient fortress of the city, now a government office with beautiful state rooms. <time><a href='https://venti.co/map/St. Patrick's Cathedral in Dublin/Dublin Castle in Dublin' target='_blank'>~1 min. away from the Cathedral
            </a>
        </time>.
        </li><li><strong>12:30 pm - 2:00 pm:
            </strong> Experience a traditional Irish pub for lunch in the heart of <food><a href='https://venti.co/place/Temple Bar in Dublin' target='_blank'>Temple Bar</a></food>, and enjoy live music and delicious traditional dishes. <time><a href='https://venti.co/map/Dublin Castle in Dublin/Temple Bar in Dublin' target='_blank'>~4 min. away from Dublin Castle</a></time>.
        </li><li><strong>2:30 pm - 4:00 pm:
            </strong> Take a stroll by the river Liffey and visit the museum of Dublin's most famous literary figure, James Joyce, at the <place><a href='https://venti.co/place/Dublin Writers Museum in Dublin' target='_blank'>Dublin Writers Museum</a></place>. <time><a href='https://venti.co/map/Temple Bar in Dublin/Dublin Writers Museum in Dublin' target='blank'> ~5 min from Templte Bar</a></time>
        </li><li><strong>4:30 pm - 6:30 pm:
            </strong> Immerse yourself in the best shopping spots at <place><a href='https://venti.co/place/Grafton Street in Dublin' target='_blank'>Grafton Street
            </a></place>, home to Ireland's famous businesses and high street stores.<time><a href='https://venti.co/map/Dublin Writers Museum in Dublin/Grafton Street in Dublin' target='_blank'>~5 min from Dublin Writers Museum</a></time>
        </li><li><strong>7:00 pm - 8:30 pm:
            </strong> Enjoy a pint of Guinness while learning the perfect pour at the famous <place><a href='https://venti.co/place/Guinness Storehouse in Dublin' target='_blank'>Guinness Storehouse</a></place>. Don't forget a panoramic view from the top-floor bar overlooking Dublin. <time><a href='https://venti.co/map/Grafton Street in Dublin/Guinness Storehouse in Dublin' target='_blank'>~10 min. from Grafton Street</a></time> 
        </li>
        </ul><h3>Day 3: Exploring the Surrounding Areas
            </h3><p>On day 3, venture areas near Dublin to enjoy the natural beauty of Ireland learn about Ireland's fascinating history.
            </p><ul><li><strong>9:00 am - 10:00 am:
            </strong> Take a ride out to the seaside town of Howth for breakfast followed by a scenic cliff walk to admire the stunning views and sea breeze. <time><a href='https://venti.co/map/Downtown Dublin/Howth in Dublin' target='_blank'>~40 min. away from Downtown Dublin</a></time>
        </li><li><strong>11:00 am - 12:00 pm:
            </strong> Discover the history of <place><a href='https://venti.co/place/Malahide Castle in Dublin' target='_blank'>Malahide Castle
            </a>
        </place>, by wandering through the ornate rooms that date back to the 12th century.
        </li><li><strong>12:30 pm - 1:30 pm:
            </strong> Take a break for lunch and enjoy the cozy <food><a href='https://venti.co/place/FishShackCafé Malahide' target='_blank'>FishShackCafé Malahide</a></food> with Ireland's finest seafood. <time><a href='https://venti.co/map/Malahide Castle in Dublin/FishShackCafé Malahide in Dublin' target='_blank'>~5 min. from Malahide Castle</a></time>.
        </li><li><strong>2:00 pm - 4:00 pm:
            </strong> Gain insight into Ireland's history with the earliest Christian society at the <place><a href='https://venti.co/place/National Museum of Archaeology in Dublin' target='_blank'>National Museum of Archaeology
            </a></place>. <time><a href='https://venti.co/map/FishShackCafé Malahide in Dublin/National Museum of Archaeology in Dublin'>~33 min. away from FishShackCafé Malahide</a></time>. 
        </li><li><strong>4:30 pm - 6:00 pm:
            </strong> Visit the 13th-century Dublin landmark, <place><a href='https://venti.co/place/St. Michans Church in Dublin' target='_blank'>St. Michan's Church in Dublin
            </a></place>, famous for its crypt, home to the mummified bodies of wealthy families. <time><a href='https://venti.co/map/National Museum of Archaeology in Dublin/St Michans Church in Dublin' target='_blank'> ~5 min from National Museum of Archaeology</a></time> 
        </li><li><strong>6:30 pm - 9:00 pm:
            </strong> For a closing dinner, experience modern Irish cuisine in beautiful surroundings at one of Dublin's Michelin-starred restaurants.
        </li>
        </ul><h3>Day 4: Departure
            </h3><p>It's time to wrap up your $randomTitle Dublin and start your journey home.
            </p><ul><li><strong>8:00 am - 9:00 am:
            </strong> Grab breakfast from the hotel lounge/cafe.
        </li><li><strong>10:00 am - 11:am pm
            </strong> Check out of your hotel. Depart from the hotel and take a taxi back to <place><a href='https://venti.co/place/Dublin Airport in Dublin Ireland' target='_blank'>Dublin Airport</a></place>, which is around 30 minutes drive.
        </li>
        </ul>";
            break;
        case 4:
            return "<h1>4-Day $randomTitle Dublin: Discovering the Best of Ireland
            </h1><h6>Are you ready for a 4-Day trip to the stunning city of Dublin, Ireland? This itinerary will cover all the must-see destinations, amazing food, and unforgettable experiences in the heart of Ireland. From the buzz of Temple Bar to the taste of Guinness, this itinerary will help you make every moment count!
            </h6><h3>Day 1: Arrival and Settle In
            </h3><p>Start your Dublin adventure by arriving at <place><a href='https://venti.co/place/Dublin Airport in Dublin Ireland' target='_blank'>Dublin Airport
            </a>
        </place>. Dublin Airport is one of Europe's busiest airports and is located about 10 km north of Dublin City. We suggest taking a taxi or the Airlink Express bus, which takes about 30 minutes, to your hotel. Check-in starts at 3 pm, so feel free to take your time exploring before heading to your hotel. 
        </p><ul><li><strong>3:00 pm - 4:30 pm:
            </strong> Check-in to your hotel and take your time to settle in.
        </li><li><strong>5:00 pm - 6:30 pm:
            </strong> Stroll through <place><a href='https://venti.co/place/Trinity College' target='_blank'>Trinity College in Dublin
            </a>
        </place> and see one of the most impressive libraries in the world. 
        </li><li><strong>7:00 pm - 9:00 pm:
            </strong> Try classic Irish food at <food><a href='https://venti.co/place/The Lobster Pot restaurant in Dublin' target='_blank'>The Lobster Pot</a>
        </food> one of Dublin's oldest family-owned restaurants, serving traditional dishes since 1904. <time><a href='https://venti.co/map/Trinity College/The Lobster Pot restaurant in Dublin' target='_blank'>~15 min. from Trinity College
            </a>
        </time>.
        </li>
        </ul><h3>Day 2: Discover the Heart of Dublin
            </h3><p>We will start the day by exploring Dublin's historic landmarks and famous neighborhoods. Be prepared to walk a lot!
            </p><ul><li><strong>9:00 am - 10:00 am:
            </strong> Begin by visiting the impressive <place><a href='https://venti.co/place/St. Patrick's Cathedral in Dublin'>St. Patrick's Cathedral</a>
        </place>, one of Ireland's most iconic landmarks.
        </li><li><strong>10:30 am - 12:00 pm:
            </strong> Take a tour of <place><a href='https://venti.co/place/Dublin Castle in Dublin' target='_blank'>Dublin Castle
            </a></place>, the ancient fortress of the city, now a government office with beautiful state rooms.
        </li><li><strong>12:30 pm - 2:00 pm:
            </strong> Experience a traditional Irish pub for lunch in the heart of <food><a href='https://venti.co/place/Temple Bar in Dublin' target='_blank'>Temple Bar</a></food>, and enjoy live music and delicious traditional dishes. <time><a href='https://venti.co/map/Dublin Castle in Dublin/Temple Bar in Dublin' target='_blank'>~4 min. away from Dublin Castle</a></time>
        </li><li><strong>2:30 pm - 4:00 pm:
            </strong> Take a stroll by the river Liffey and visit the museum of Dublin's most famous literary figure, James Joyce, at the <place><a href='https://venti.co/place/Dublin Writers Museum in Dublin' target='_blank'>Dublin Writers Museum</a></place>. <time><a href='https://venti.co/map/Temple Bar in Dublin/Dublin Writers Museum in Dublin' target='blank'> ~5 min from Templte Bar</a></time>
        </li><li><strong>4:30 pm - 6:30 pm:
            </strong> Immerse yourself in the best shopping spots at <place><a href='https://venti.co/place/Grafton Street in Dublin' target='_blank'>Grafton Street
            </a></place>, home to Ireland's famous businesses and high street stores.<time><a href='https://venti.co/map/Dublin Writers Museum in Dublin/Grafton Street in Dublin' target='_blank'>~5 min from Dublin Writers Museum</a></time>
        </li><li><strong>7:00 pm - 8:30 pm:
            </strong> Enjoy a pint of Guinness while learning the perfect pour at the famous <place><a href='https://venti.co/place/Guinness Storehouse in Dublin' target='_blank'>Guinness Storehouse</a></place>. Don't forget a panoramic view from the top-floor bar overlooking Dublin. <time><a href='https://venti.co/map/Grafton Street in Dublin/Guinness Storehouse in Dublin' target='_blank'>~10 min. from Grafton Street</a></time> 
        </li>
        </ul><h3>Day 3: Exploring the Surrounding Areas
            </h3><p>On day 3, venture areas near Dublin to enjoy the natural beauty of Ireland learn about Ireland's fascinating history.
            </p><ul><li><strong>9:00 am - 10:00 am:
            </strong> Take a ride out to the seaside town of Howth for breakfast followed by a scenic cliff walk to admire the stunning views and sea breeze. <time><a href='https://venti.co/map/Downtown Dublin/Howth in Dublin' target='_blank'>~40 min. away from Downtown Dublin</a></time>
        </li><li><strong>11:00 am - 12:00 pm:
            </strong> Discover the history of <place><a href='https://venti.co/place/Malahide Castle in Dublin' target='_blank'>Malahide Castle
            </a>
        </place>, by wandering through the ornate rooms that date back to the 12th century.
        </li><li><strong>12:30 pm - 1:30 pm:
            </strong> Take a break for lunch and enjoy the cozy <food><a href='https://venti.co/place/FishShackCafé Malahide' target='_blank'>FishShackCafé Malahide</a></food> with Ireland's finest seafood. <time><a href='https://venti.co/map/Malahide Castle in Dublin/FishShackCafé Malahide in Dublin' target='_blank'>~5 min. from Malahide Castle</a></time>.
        </li><li><strong>2:00 pm - 4:00 pm:
            </strong> Gain insight into Ireland's history with the earliest Christian society at the <place><a href='https://venti.co/place/National Museum of Archaeology in Dublin' target='_blank'>National Museum of Archaeology
            </a></place>. <time><a href='https://venti.co/map/FishShackCafé Malahide in Dublin/National Museum of Archaeology in Dublin'>~33 min. away from FishShackCafé Malahide</a></time>
        </li><li><strong>4:30 pm - 6:00 pm:
            </strong> Visit the 13th-century Dublin landmark, <place><a href='https://venti.co/place/St. Michans Church in Dublin' target='_blank'>St. Michan's Church in Dublin
            </a></place>, famous for its crypt, home to the mummified bodies of wealthy families. <time><a href='https://venti.co/map/National Museum of Archaeology in Dublin/St Michans Church in Dublin' target='_blank'> ~5 min from National Museum of Archaeology</a></time>
        </li><li><strong>6:30 pm - 9:00 pm:
            </strong> For a closing dinner, experience modern Irish cuisine in beautiful surroundings at one of Dublin's Michelin-starred restaurants.
        </li>
        </ul><h3>Day 4: Exploring the Surrounding Areas
            </h3><p>On day 3, venture outside of Dublin to enjoy the natural beauty of Ireland along the coastline and learn about Ireland's fascinating history.
            </p><ul><li><strong>9:00 am - 10:00 am:
            </strong> Take a ride out to the seaside town of Howth for breakfast followed by a scenic cliff walk to admire the stunning views and sea breeze. <time><a href='https://venti.co/map/Downtown Dublin/Howth in Dublin' target='_blank'>~40 min. away from Downtown Dublin</a></time>
        </li><li><strong>11:00 am - 12:00 pm:
            </strong> Discover the history of <place><a href='https://venti.co/place/Malahide Castle in Dublin' target='_blank'>Malahide Castle
            </a>
        </place>, by wandering through the ornate rooms that date back to the 12th century.
        </li><li><strong>12:30 pm - 1:30 pm:
            </strong> Take a break for lunch and enjoy the cozy <food><a href='https://venti.co/place/FishShackCafé Malahide' target='_blank'>FishShackCafé Malahide</a></food> with Ireland's finest seafood. <time><a href='https://venti.co/map/Malahide Castle in Dublin/FishShackCafé Malahide in Dublin' target='_blank'>~5 min. from Malahide Castle</a></time>.
        </li><li><strong>2:00 pm - 4:00 pm:
            </strong> Gain insight into Ireland's history with the earliest Christian society at the <place><a href='https://venti.co/place/National Museum of Archaeology in Dublin' target='_blank'>National Museum of Archaeology
            </a></place>. <time><a href='https://venti.co/map/FishShackCafé Malahide in Dublin/National Museum of Archaeology in Dublin'>~33 min. away from FishShackCafé Malahide</a></time> 
        </li><li><strong>4:30 pm - 6:00 pm:
            </strong> Visit the 13th-century Dublin landmark, <place><a href='https://venti.co/place/St. Michans Church in Dublin' target='_blank'>St. Michan's Church in Dublin
            </a></place>, famous for its crypt, home to the mummified bodies of wealthy families. <time><a href='https://venti.co/map/National Museum of Archaeology in Dublin/St Michans Church in Dublin' target='_blank'> ~5 min from National Museum of Archaeology</a></time>
        </li><li><strong>6:30 pm - 9:00 pm:
            </strong> For a closing dinner, experience modern Irish cuisine in beautiful surroundings at one of Dublin's Michelin-starred restaurants.
        </li>
        </ul><h3>Day 5: Departure
            </h3><p>It's time to wrap up your $randomTitle Dublin and start your journey home.
            </p><ul><li><strong>8:00 am - 9:00 am:
            </strong> Grab breakfast from the hotel lounge/cafe.
        </li><li><strong>10:00 am - 11:00 am
            </strong> Check out of your hotel. Depart from the hotel and take a taxi back to <place><a href='https://venti.co/place/Dublin Airport in Dublin Ireland' target='_blank'>Dublin Airport</a></place>, which is around 30 minutes drive.
        </li>
        </ul>";
        default:
            break;
    }
}

function sampleJSONItinerary($destination, $country, $days){
    $titles = ["Escape to", "Highlights of", "Journey through", "Essential", "Adventure in", "Retreat to", "Cultural exploration of", "Unforgettable"];

    $randomTitle = $titles[rand(0,7)];

    $itinerary = [
        "title" => "$days-Day $randomTitle $destination: Exploring the Best of $country",
        "description" => "</h1><h6>Are you ready for a 3-Day trip to the stunning city of $destination, $country? This itinerary will cover all the must-see destinations, amazing food, and unforgettable experiences in the heart of $country. From the buzz of Temple Bar to the taste of Guinness, this itinerary will help you make every moment count!",
        "destination" => "$destination",
        "country" => "$country",
        "duration" => $days,
        "itinerary" => []
    ];

    for($i = 0; $i <= $days; $i++){
        $itinerary["itinerary"][$i] = sampleDay($i, $days);
    }

    return json_encode($itinerary);
}

function sampleDay($day, $days){
    $dayCounter = $day + 1;
    if($day == 0){
        // first day
        
        $day = [
            "day" => $dayCounter,
            "subtitle" => "Day $dayCounter: Arrival and Settle In",
            "description" => "We will start the day by exploring Dublin's historic landmarks and famous neighborhoods.",
            "hotel" => "Westin Dublin",
            "activities" => [
                0 => [
                    "destination" => "Westin Dublin Hotel",
                    "category" => "Lodging",
                    "start" => "3:00 pm",
                    "end" => "4:00 pm",
                    "description" => "Start your Dublin adventure by arriving at Dublin Airport, which is one of Europe's busiest airports and is located about 10 km north of Dublin City. We suggest taking a taxi or the Airlink Express bus, which takes about 30 minutes, to our recommended hotel: Westin Dublin. Check-in starts at 3 pm, so feel free to take your time exploring before heading to your hotel.",
                    "url" => "https://venti.co/place/Westin Dublin Hotel in Dublin Ireland",
                    "previousDestination" => "Dublin Airport",
                    "nextDestination" => "Trinity College",
                    "distanceToDestination" => [
                        //"walking" => null,
                        "taxi" => "Approx. 30 min",
                        //"train" => "Approx. 30 min"
                    ],
                    "distanceUrl" => "https://venti.co/map/Dublin Airport in Dublin Ireland/Westin Dublin Hotel in Dublin Ireland"
                ],
                1 => [
                    "destination" => "Trinity College",
                    "category" => "Landmarks",
                    "start" => "5:00 pm",
                    "end" => "6:40 pm",
                    "description" => "Stroll through Trinity College in Dublin and see one of the most impressive libraries in the world.",
                    "url" => "https://venti.co/place/Trinity College in Dublin Ireland",
                    "previousDestination" => "Westin Dublin Hotel",
                    "nextDestination" => "The Lobster Pot",
                    "distanceToDestination" => [
                        //"walking" => "Approx. 2 min",
                        "taxi" => "Approx. 3 min",
                        //"train" => null
                    ],
                    "distanceUrl" => "https://venti.co/map/Westin Dublin Hotel in Dublin Ireland/Trinity College in Dublin Ireland/"
                ],
                2 => [
                    "destination" => "The Lobster Pot",
                    "category" => "Food",
                    "start" => "7:00 pm",
                    "end" => "9:00 pm",
                    "description" => "Try classic Irish food at The Lobster Pot one of Dublin's oldest family-owned restaurants, serving traditional dishes since 1904.",
                    "url" => "https://venti.co/place/The Lobster Pot in Dublin Ireland",
                    "previousDestination" => "Trinity College",
                    "nextDestination" => null,
                    "distanceToDestination" => [
                        //"walking" => "Approx. 34 min",
                        "taxi" => "Approx. 16 min",
                        //"train" => "Approx 16 min."
                    ],
                    "distanceUrl" => "https://venti.co/map/Trinity College in Dublin Ireland/The Lobster Pot in Dublin Ireland/"
                ]
            ]
        ];
    }
    if($day > 0 && $day < $days){
        $day = [
            "day" => $dayCounter,
            "subtitle" => "Day $dayCounter: Exploring the Surrounding Areas",
            "description" => "We will start the day by exploring Dublin's historic landmarks and famous neighborhoods.",
            "activities" => [
                0 => [
                    "destination" => "Malahide Castle",
                    "category" => "Food",
                    "start" => "11:00 am",
                    "end" => "12:00 pm",
                    "description" => "Discover the history of Malahide Castle, by wandering through the ornate rooms that date back to the 12th century.",
                    "url" => "https://venti.co/place/Malahide Castle in Dublin Ireland",
                    "previousDestination" => "Westin Dublin Hotel",
                    "nextDestination" => "FishShackCafé Malahide",
                    "distanceToDestination" => [
                        //"walking" => null,
                        "taxi" => "Approx. 30 min",
                        //"train" => "Approx. 1 hr"
                    ],
                    "distanceUrl" => "https://venti.co/map/Westin Dublin Hotel in Dublin Ireland/Malahide Castle in Dublin Ireland"
                ],
                1 => [
                    "destination" => "FishShackCafé Malahide",
                    "category" => "Food",
                    "start" => "12:30 pm",
                    "end" => "1:30 pm",
                    "description" => "Take a break for lunch and enjoy the cozy with Ireland's finest seafood.",
                    "url" => "https://venti.co/place/FishShackCafé Malahide in Dublin Ireland",
                    "previousDestination" => "Malahide Castle",
                    "nextDestination" => "National Museum of Archaeology",
                    "distanceToDestination" => [
                        //"walking" => "Approx. 21 min",
                        "taxi" => "Approx. 3 min",
                        //"train" => null
                    ],
                    "distanceUrl" => "https://venti.co/map/Malahide Castle in Dublin Ireland/FishShackCafé Malahide in Dublin Ireland"
                ],
                2 => [
                    "destination" => "National Museum of Archaeology",
                    "category" => "Museums",
                    "start" => "2:00 pm",
                    "end" => "4:00 pm",
                    "description" => "Gain insight into Ireland's history with the earliest Christian society at the National Museum of Archaeology.",
                    "url" => "https://venti.co/place/National Museum of Archaeology in Dublin Ireland",
                    "previousDestination" => "FishShackCafé Malahide",
                    "nextDestination" => "St. Michans Church",
                    "distanceToDestination" => [
                        //"walking" => null,
                        "taxi" => "Approx. 40 min",
                        //"train" => "Approx. 43 min"
                    ],
                    "distanceUrl" => "https://venti.co/map/FishShackCafé Malahide in Dublin Ireland/National Museum of Archaeology in Dublin Ireland"
                ],
                3 => [
                    "destination" => "St. Michans Church",
                    "category" => "Landmarks",
                    "start" => "4:30 pm",
                    "end" => "6:00 pm",
                    "description" => "Visit the 13th-century Dublin landmark, St. Michan's Church in Dublin, famous for its crypt, home to the mummified bodies of wealthy families.",
                    "url" => "https://venti.co/place/St. Michans Church in Dublin Ireland",
                    "previousDestination" => "National Museum of Archaeology",
                    "nextDestination" => "Guinness Storehouse",
                    "distanceToDestination" => [
                        //"walking" => "Approx. 25 min",
                        "taxi" => "Approx. 19 min",
                        //"train" => "Approx. 20 min"
                    ],
                    "distanceUrl" => "https://venti.co/map/National Museum of Archaeology in Dublin Ireland/St. Michans Church in Dublin Ireland"
                ],
                4 => [
                        "destination" => "Guinness Storehouse",
                        "category" => "Food",
                        "start" => "6:30 pm",
                        "end" => "7:30 pm",
                        "description" => "Enjoy a pint of Guinness while learning the perfect pour at the famous Guinness Storehouse. Don't forget a panoramic view from the top-floor bar overlooking Dublin. ",
                        "url" => "https://venti.co/place/Guinness Storehouse in Dublin Ireland",
                        "previousDestination" => "St. Michans Church",
                        "nextDestination" => null,
                        "distanceToDestination" => [
                            //"walking" => "Approx. 18 min",
                            "taxi" => "Approx. 8 min",
                            //"train" => "Approx. 18 min"
                        ],
                        "distanceUrl" => "https://venti.co/map/St. Michans Church in Dublin Ireland/Guinness Storehouse in Dublin Ireland"
                ]
            ]
        ];
    }
    if($day == $days){
        // last day
        $day = [
            "day" => $dayCounter,
            "subtitle" => "Day $dayCounter: Goodbyes and Departures",
            "description" => "We will start the day by exploring Dublin's historic landmarks and famous neighborhoods.",
            "activities" => [
                0 => [
                    "destination" => "Westin Dubline Hotel Cafe/Lounge",
                    "category" => "Food",
                    "start" => "7:00 am",
                    "end" => "9:00 am",
                    "description" => "Enjoy your last breakfast within your hotel's lounge/cade.",
                    "url" => "https://venti/co/place/Westin Dublin Hotel Cafe in Dublin Ireland",
                    "previousDestination" => null,
                    "nextDestination" => null,
                    "distanceUrl" => null
                ],
                1 => [
                    "destination" => "Westin Dublin Hotel",
                    "category" => "Hotel",
                    "start" => "11:00 am",
                    "end" => "12:00 pm",
                    "description" => "Check out of your hotel. Depart from the hotel and take a taxi back to Dublin Airport, which is around 30 minutes drive.",
                    "url" => "https://venti/co/place/Westin Dublin Hotel in Dublin Ireland",
                    "previousDestination" => "Hotel Cafe/Lounge",
                    "nextDestination" => "Dublin Airport",
                    "distanceUrl" => "https://venti.co/map/Westin Dublin Hotel in Dublin Ireland/Dublin Airport in Dublin Ireland"
                ]
            ]
        ];
    }

    return $day;
}

function extractJSONfromText($text){
    $matches = [];
    $pattern = '
    /
    \{              # { character
        (?:         # non-capturing group
            [^{}]   # anything that is not a { or }
            |       # OR
            (?R)    # recurses the entire pattern
        )*          # previous group zero or more times
    \}              # } character
    /x
    ';
    preg_match_all($pattern, $text, $matches);
    return $matches;
}

function getContinent($countryCode){

    $continents = [
      "AD" => "Europe",
      "AE" => "Asia",
      "AF" => "Asia",
      "AG" => "North America",
      "AI" => "North America",
      "AL" => "Europe",
      "AM" => "Asia",
      "AN" => "North America",
      "AO" => "Africa",
      "AQ" => "Antarctica",
      "AR" => "South America",
      "AS" => "Australia",
      "AT" => "Europe",
      "AU" => "Australia",
      "AW" => "North America",
      "AZ" => "Asia",
      "BA" => "Europe",
      "BB" => "North America",
      "BD" => "Asia",
      "BE" => "Europe",
      "BF" => "Africa",
      "BG" => "Europe",
      "BH" => "Asia",
      "BI" => "Africa",
      "BJ" => "Africa",
      "BM" => "North America",
      "BN" => "Asia",
      "BO" => "South America",
      "BR" => "South America",
      "BS" => "North America",
      "BT" => "Asia",
      "BW" => "Africa",
      "BY" => "Europe",
      "BZ" => "North America",
      "CA" => "North America",
      "CC" => "Asia",
      "CD" => "Africa",
      "CF" => "Africa",
      "CG" => "Africa",
      "CH" => "Europe",
      "CI" => "Africa",
      "CK" => "Australia",
      "CL" => "South America",
      "CM" => "Africa",
      "CN" => "Asia",
      "CO" => "South America",
      "CR" => "North America",
      "CU" => "North America",
      "CV" => "Africa",
      "CX" => "Asia",
      "CY" => "Asia",
      "CZ" => "Europe",
      "DE" => "Europe",
      "DJ" => "Africa",
      "DK" => "Europe",
      "DM" => "North America",
      "DO" => "North America",
      "DZ" => "Africa",
      "EC" => "South America",
      "EE" => "Europe",
      "EG" => "Africa",
      "EH" => "Africa",
      "ER" => "Africa",
      "ES" => "Europe",
      "ET" => "Africa",
      "FI" => "Europe",
      "FJ" => "Australia",
      "FK" => "South America",
      "FM" => "Australia",
      "FO" => "Europe",
      "FR" => "Europe",
      "GA" => "Africa",
      "GB" => "Europe",
      "GD" => "North America",
      "GE" => "Asia",
      "GF" => "South America",
      "GG" => "Europe",
      "GH" => "Africa",
      "GI" => "Europe",
      "GL" => "North America",
      "GM" => "Africa",
      "GN" => "Africa",
      "GP" => "North America",
      "GQ" => "Africa",
      "GR" => "Europe",
      "GS" => "Antarctica",
      "GT" => "North America",
      "GU" => "Australia",
      "GW" => "Africa",
      "GY" => "South America",
      "HK" => "Asia",
      "HN" => "North America",
      "HR" => "Europe",
      "HT" => "North America",
      "HU" => "Europe",
      "ID" => "Asia",
      "IE" => "Europe",
      "IL" => "Asia",
      "IM" => "Europe",
      "IN" => "Asia",
      "IO" => "Asia",
      "IQ" => "Asia",
      "IR" => "Asia",
      "IS" => "Europe",
      "IT" => "Europe",
      "JE" => "Europe",
      "JM" => "North America",
      "JO" => "Asia",
      "JP" => "Asia",
      "KE" => "Africa",
      "KG" => "Asia",
      "KH" => "Asia",
      "KI" => "Australia",
      "KM" => "Africa",
      "KN" => "North America",
      "KP" => "Asia",
      "KR" => "Asia",
      "KW" => "Asia",
      "KY" => "North America",
      "KZ" => "Asia",
      "LA" => "Asia",
      "LB" => "Asia",
      "LC" => "North America",
      "LI" => "Europe",
      "LK" => "Asia",
      "LR" => "Africa",
      "LS" => "Africa",
      "LT" => "Europe",
      "LU" => "Europe",
      "LV" => "Europe",
      "LY" => "Africa",
      "MA" => "Africa",
      "MC" => "Europe",
      "MD" => "Europe",
      "ME" => "Europe",
      "MG" => "Africa",
      "MH" => "Australia",
      "MK" => "Europe",
      "ML" => "Africa",
      "MM" => "Asia",
      "MN" => "Asia",
      "MO" => "Asia",
      "MP" => "Australia",
      "MQ" => "North America",
      "MR" => "Africa",
      "MS" => "North America",
      "MT" => "Europe",
      "MU" => "Africa",
      "MV" => "Asia",
      "MW" => "Africa",
      "MX" => "North America",
      "MY" => "Asia",
      "MZ" => "Africa",
      "NA" => "Africa",
      "NC" => "Australia",
      "NE" => "Africa",
      "NF" => "Australia",
      "NG" => "Africa",
      "NI" => "North America",
      "NL" => "Europe",
      "NO" => "Europe",
      "NP" => "Asia",
      "NR" => "Australia",
      "NU" => "Australia",
      "NZ" => "Australia",
      "OM" => "Asia",
      "PA" => "North America",
      "PE" => "South America",
      "PF" => "Australia",
      "PG" => "Australia",
      "PH" => "Asia",
      "PK" => "Asia",
      "PL" => "Europe",
      "PM" => "North America",
      "PN" => "Australia",
      "PR" => "North America",
      "PS" => "Asia",
      "PT" => "Europe",
      "PW" => "Australia",
      "PY" => "South America",
      "QA" => "Asia",
      "RE" => "Africa",
      "RO" => "Europe",
      "RS" => "Europe",
      "RU" => "Europe",
      "RW" => "Africa",
      "SA" => "Asia",
      "SB" => "Australia",
      "SC" => "Africa",
      "SD" => "Africa",
      "SE" => "Europe",
      "SG" => "Asia",
      "SH" => "Africa",
      "SI" => "Europe",
      "SJ" => "Europe",
      "SK" => "Europe",
      "SL" => "Africa",
      "SM" => "Europe",
      "SN" => "Africa",
      "SO" => "Africa",
      "SR" => "South America",
      "ST" => "Africa",
      "SV" => "North America",
      "SY" => "Asia",
      "SZ" => "Africa",
      "TC" => "North America",
      "TD" => "Africa",
      "TF" => "Antarctica",
      "TG" => "Africa",
      "TH" => "Asia",
      "TJ" => "Asia",
      "TK" => "Australia",
      "TM" => "Asia",
      "TN" => "Africa",
      "TO" => "Australia",
      "TR" => "Asia",
      "TT" => "North America",
      "TV" => "Australia",
      "TW" => "Asia",
      "TZ" => "Africa",
      "UA" => "Europe",
      "UG" => "Africa",
      "US" => "North America",
      "UY" => "South America",
      "UZ" => "Asia",
      "VC" => "North America",
      "VE" => "South America",
      "VG" => "North America",
      "VI" => "North America",
      "VN" => "Asia",
      "VU" => "Australia",
      "WF" => "Australia",
      "WS" => "Australia",
      "YE" => "Asia",
      "YT" => "Africa",
      "ZA" => "Africa",
      "ZM" => "Africa",
      "ZW" => "Africa"
    ];

    return $continents[$countryCode];
}

function allAirports(){
    return '["A.P. Hill Army Airfield","Al Minhad Air Base","Al Najaf International Airport","Al Qaisumah-Hafr Al Batin Airport","Al Taqaddum Air Base","Al Thaurah Airport","Al Udeid Air Base","Al Wajh Domestic Airport","Almer\u00eda Airport","Almirante Padilla Airport","Along Airport","Alor Island Airport","Alowe Airport","Alpena County Regional Airport","Alpes\u2013Is\u00e8re Airport","Alpha Airport","Alpine\u2013Casparis Municipal Airport","Alroy Downs Airport","Alta Airport","Alta Floresta Airport","Altai Airport","Altamira Airport","Altay Airport","Altenburg-Nobitz Airport","Alto Molocue Airport","Alto Palena Airport","Alto Parna\u00edba Airport","Alto R\u00edo Senguer Airport","Alton Downs Airport","Altoona-Blair County Airport","Altus Air Force Base","Altus-Quartz Mountain Regional Airport","Alula Aba Nega Airport","Alula Airport","Alverca Airport","Alxa Left Banner Bayanhot Airport","Alxa Right Banner Airport","Alykel Airport","Am Timan Airport","Ama Airport","Amahai Airport","Amakusa Airport","Amalfi Airport","Amami Airport","Amanab Airport","Amasya Merzifon Airport","Amata Airport","Amazon Bay Airport","Ambalabe Airport","Ambatolahy Airport","Ambatomainty Airport","Ambatondrazaka Airport","Ambilobe Airport","Ambler Airport","Ambohibary Airport","Amboin Airport","Amborovy Airport","Amboseli Airport","Ambriz Airport","Ambunti Airport","Amderma Airport","Amedee Army Airfield","American River Airport","Amerigo Vespucci Airport","Amery Municipal Airport","Ames Municipal Airport","Amgu Airport","Amman Civil Airport","Ammaroo Airport","Amook Bay Seaplane Base","Amos-Magny Airport","Ampampamena Airport","Ampanihy Airport","Ampara Airport","Amsterdam Airport Schiphol","Am\u00edlcar Cabral International Airport","Anaa Airport","Anaco Airport","Anacortes Airport","Anadolu Airport","Anahim Lake Airport","Anaktuvuk Pass Airport","Analalava Airport","Anapa Airport","Anatom Airport","Ancona Falconara Airport","Andahuaylas Airport","Andakombe Airport","Andamooka Airport","Andapa Airport","Andavadoaka Airport","Andersen Air Force Base","Anderson Municipal Airport","Anderson Regional Airport","Andes Airport","Andi Jemma Airport","Andizhan Airport","Andorra\u2013La Seu d\'Urgell Airport","Andrau Airpark Airport","Andravida Air Base","Andre Maggi Airport","Andriamena Airport","Andros Town Airport","Andr\u00e9s Miguel Salazar Marcano Airport","Andulo Airport","And\u00f8ya Airport","Anfa Airport","Angads Airport","Angama Airport","Angel Fire Airport","Angelina County Airport","Angers - Loire Airport","Anggi Airport","Anglesey Airport","Angoche Airport","Angoon Seaplane Base","Angoram Airport","Angoul\u00eame \u2013 Brie \u2013 Champniers Airport","Anguganak Airport","Angus Downs Airport","Aniak Airport","Animas Air Park","Aniwa Airport","Ankang Wulipu Airport","Ankavandra Airport","Ankazoabo Airport","Ankokoambo Airport","Ann Airport","Ann Arbor Municipal Airport","Annai Airport","Annanberg Airport","Annecy \u2013 Haute-Savoie \u2013 Mont Blanc Airport","Annette Island Airport","Anniston Regional Airport","Annob\u00f3n Airport","Anqing Tianzhushan Airport","Anshan Teng\'ao Airport","Anshun Huangguoshu Airport","Antalya Airport","Anthony Lagoon Airport","Anthony Municipal Airport","Antlers Municipal Airport","Antofagasta Airport","Antoine de Saint Exup\u00e9ry Airport","Antonio B. Won Pat International Airport","Antonio Jos\u00e9 de Sucre Airport","Antonio Maceo International Airport","Antonio Narino Airport","Antonio Nery Juarbe Pol Airport","Antonio Rivera Rodr\u00edguez Airport","Antonio Rold\u00e1n Betancourt Airport","Antrim County Airport","Antsalova Airport","Antsirabato Airport","Antsirabe Airport","Antsoa Airport","Antwerp International Airport","Anuradhapura Airport","Anvik Airport","Anyang Airport","An\u00e1polis Airport","Aomori Airport","Aosta Airport","Apalachicola Regional Airport","Apalapsili Airport","Apataki Airport","Apiay Air Base Airport","Apolo Airport","Apple Valley Airport","Appleton International Airport","April River Airport","Apucarana Airport","Ar Horqin Airport","Aracati - Drag\u00e3o do Mar Regional Airport","Arad International Airport","Aragar\u00e7as Airport","Aragip Airport","Aragua\u00edna Airport","Arak Airport","Aramac Airport","Arandis Airport","Aransas County Airport","Aranuka Airport","Arapahoe Municipal Airport","Arapiraca Airport","Arapongas Airport","Arapoti Airport","Arar Domestic Airport","Araracuara Airport","Araraquara Airport","Ararat Airport","Arathusa Safari Lodge Airport","Aratika-Nord Airport","Arauca Santiago Perez Quiroz Airport","Arawa Airport","Araxos Airport","Arax\u00e1 Airport","Ara\u00e7atuba Airport","Arba Minch Airport","Arboletes Airport","Arcata Eureka Airport","Arctic Bay Airport","Arctic Village Airport","Ardabil Airport","Ardmore Airport","Ardmore Downtown Executive Airport","Ardmore Municipal Airport","Arenal Airport","Argyle Airport","Argyle International Airport","Aribinda Airport","Arica Airport","Aripuan\u00e3 Airport","Ariquemes Airport","Aristides Pereira International Airport","Arkalyk Airport","Arkansas International Airport","Arlit Airport","Arly Airport","Armando Schwarck Airport","Armidale Airport","Armstrong Airport","Arnold Palmer Regional Airport","Aroa Airport","Arona Airport","Aropa Airport","Arorae Island Airport","Arrabury Airport","Arrachart Airport","Arraias Airport","Arroyo Barril International Airport","Arso Airport","Artesia Municipal Airport","Arthur N. Neu Airport","Arthur Napoleon Raymond Robinson International Airport","Arthur\'s Town Airport","Artigas International Airport","Arturo Michelena International Airport","Arua Airport","Arugam Bay Seaplane Base Airport","Arusha Airport","Arutua Airport","Arvaikheer Airport","Arviat Airport","Arvidsjaur Airport","Arxan Yi\'ershi Airport","Asaba International Airport","Asahikawa Airport","Asapa Airport","Asau Airport","Ascenci\u00f3n de Guarayos Airport","Aseki Airport","Asella Airport","Ashburton Aerodrome","Asheville Regional Airport","Ashgabat International Airport","Ashland Municipal Airport","Ashley Municipal Airport","Asirim Airport","Asmara International Airport","Asosa Airport","Aspen-Pitkin County Airport","Assab International Airport","Assis Airport","Assiut Airport","Astana International Airport","Astoria Regional Airport","Astrakhan Airport","Asturias Airport","Astypalaia Island National Airport","Aswan International Airport","Ataq Airport","Atar International Airport","Atauro Airport","Atbara Airport","Atbasar Airport","Athens Ben Epps Airport","Athens International Airport","Ati Airport","Atikokan Municipal Airport","Atizapan De Zaragoza Airport","Atka Airport","Atkamba Airport","Atkinson Municipal Airport","Atlantic City International Airport","Atlantic Municipal Airport","Atmautluak Airport","Atqasuk Edward Burnell Sr. Memorial Airport","Atsinanana Airport","Atsugi Naval Air Facility Airport","Attawapiskat Airport","Attopeu Airport","Atung Bungsu Airport","Atuona Airport","Atyrau Airport","Aua Island Airport","Aubenas-Ard\u00e8che M\u00e9ridional Airport","Auburn Municipal Airport","Auburn University Regional Airport","Auburn-Lewiston Municipal Airport","Auckland Airport","Augsburg Airport","Augusta Regional Airport","Augusta State Airport","Auguste George Airport","Augusto Cesar Sandino International Airport","Augustus Downs Airport","Auki Gwaunaru\'u Airport","Aumo Airport","Aupaluk Airport","Aur Island Airport","Aurangabad Airport","Aurillac \u2013 Tronqui\u00e8res Airport","Aurora Municipal Airport","Aurukun Airport","Austin Airport","Austin Municipal Airport","Austin-Bergstrom International Airport","Austral Downs Airport","Auvergne Airport","Auxerre-Branches Airport","Avalon Airport","Avaratra Airport","Avenger Field","Aviador Carlos Campos Airport","Aviano Air Base","Avignon Provence Airport","Avon Park Executive Airport","Avram Iancu Cluj International Airport","Avu Avu Airport","Awaba Airport","Awang Airport","Awar Airport","Awasa Airport","Axum Airport","Ayacucho Airport","Ayapel Airport","Ayawasi Airport","Ayers Rock Airport","Ayr Airport","Azaza Airport","Babo Airport","Bacacheri Airport","Bacha Khan International Airport","Bachigualato Federal International Airport","Baco Airport","Bacolod-Silay International Airport","Bac\u0103u International Airport","Badajoz Airport","Bade Airport","Badu Island Airport","Bafoussam Airport","Baganga Airport","Bagani Airport","Bagdad Airport","Bagdogra Airport","Baghdad International Airport","Baglung Airport","Bagram Airfield Airport","Bahawalpur Airport","Bahir Dar Airport","Bahja Airport","Bahrain International Airport","Bahregan Airport","Bah\u00eda Cupica Airport","Bah\u00eda Negra Airport","Bah\u00eda Pi\u00f1a Airport","Bah\u00eda de los \u00c1ngeles Airport","Bah\u00edas de Huatulco International Airport","Baia Mare Airport","Baibara Airport","Baicheng Chang\'an Airport","Baidoa Airport","Baie-Comeau Airport","Baie-Johan-Beetz Seaplane Base Airport","Baikal International Airport","Bailing Airport","Baillif Airport","Baimuru Airport","Baindoung Airport","Bairnsdale Airport","Baise Bama Airport","Baitadi Airport","Bajawa Soa Airport","Bajhang Airport","Bajone Airport","Bajura Airport","Bakalalan Airport","Bakel Airport","Baker City Municipal Airport","Baker Lake Airport","Bakkafj\u00f6r\u00f0ur Airport","Bakouma Airport","Balakovo Airport","Balalae Airport","Balcanoona Airport","Baldwin County Airport","Baleela Airport","Balesin Island Airport","Balgo Hill Airport","Bali Airport","Balikesir Airport","Balimo Airport","Balkanabat Airport","Balkhash Airport","Ballera Airport","Ballina Byron Gateway Airport","Ballykelly Airport","Balmaceda Airport","Balranald Airport","Balsas Airport","Baltimore Washington International Thurgood Marshall Airport","Baltrum Airport","Balurghat Airport","Bam Airport","Bamako-S\u00e9nou International Airport","Bamarni Airport","Bambari Airport","Bambu Airport","Bamburi Airport","Bamenda Airport","Bamfield Airport","Bamu Airport","Bamyan Airport","Ban Huoeisay Airport","Banaina Airport","Bancasi Airport","Bandanaira Airport","Bandar Abbas International Airport","Bandar Lengeh Airport","Bandaranaike International Airport","Bandon State Airport","Bandundu Airport","Band\u0131rma Airport","Banff Airport","Banfora Airport","Bangalore Kempegowda International Airport","Bangassou Airport","Bangoka International Airport","Bangor International Airport","Bangui M\'Poko International Airport","Baniyala Airport","Banja Luka International Airport","Banjul International Airport","Bankstown Airport","Banning Municipal Airport","Bannu Airport","Bantry Aerodrome","Banz Airport","Baoshan Yunrui Airport","Baotou Airport","Bapi Airport","Bar River Airport","Bar Yehuda Airfield","Baracoa Airport","Barakoma Airport","Baramita Airport","Barcaldine Airport","Barcelona\u2013El Prat Josep Tarradellas Airport","Barcelonnette - Saint-Pons Airport","Barcelos Airport","Bardera Airport","Bardufoss Airport","Bareilly Airport","Bari Karol Wojty\u0142a Airport","Barimunya Airport","Barin Naval Outlying Field Airport","Barinas Airport","Bario Airport","Barisal Airport","Barking Sands Airport","Barkley Regional Airport","Barkly Downs Airport","Barksdale Air Force Base","Barnaul Airport","Barnstable Municipal Airport","Barnwell Regional Airport","Barora Airport","Barra Airport","Barra del Colorado Airport","Barra do Corda Airport","Barra do Gar\u00e7as Airport","Barranca De Upia Airport","Barranco Minas Airport","Barreiras Airport","Barreirinhas Airport","Barriles Airport","Barrow County Airport","Barrow Island Airport","Barrow-Walney Island Airport","Barstow-Daggett Airport","Barter Island Airport","Barth Airport","Bartica Airport","Bartlesville Municipal Airport","Bartlett Cove Seaplane Base","Bartow Municipal Airport","Baruun-Urt Airport","Basango Mboliasa Airport","Basankusu Airport","Basco Airport","Base A\u00e9rea de Santos Airport","Basongo Airport","Basrah International Airport","Bassatine Airport","Bassel Al-Assad International Airport","Bastia \u2013 Poretta Airport","Bata Airport","Batagay Airport","Batajnica Air Base","Batangafo Airport","Batavia Downs Airport","Bateen Airport","Batesville Regional Airport","Bathinda Airport","Bathpalathang Airport","Bathurst Airport","Bathurst Island Airport","Batman Airport","Batom Airport","Baton Rouge Metropolitan Airport","Batouri Airport","Battambang Airport","Batticaloa Airport","Battle Mountain Airport","Batu Licin Airport","Batumi International Airport","Batuna Airport","Baubau Airport","Bauchi State Airport","Baudette International Airport","Bauerfield International Airport","Baures Airport","Bauru Arealva Airport","Bawan Airport","Bawean Airport","Bay City Municipal Airport","Bayankhongor Airport","Bayannur Tianjitai Airport","Baytown Airport","Bazaruto Island Airport","Bazhong Enyang Airport","Beagle Bay Airport","Bealanana Airport","Beale Air Force Base","Bear Creek 3 Airport","Bearskin Lake Airport","Beatrice Municipal Airport","Beatty Airport","Beaufort County Airport","Beaumont Municipal Airport","Beauregard Regional Airport","Beaver Airport","Beaver County Airport","Beaver Creek Airport","Bedford Downs Airport","Bedourie Airport","Beech Factory Airport","Beersheba Airport","Begishevo Airport","Begumpet Airport","Beica Airport","Beihai Fucheng Airport","Beihan Airport","Beijing Capital International Airport","Beijing Daxing International Airport","Beijing Nanyuan Airport","Beira Airport","Beira Lake Seaplane Base Airport","Beirut-Rafic Hariri International Airport","Beja International Airport","Bekily Airport","Belaga Airport","Belaya Gora Airport","Belbek Airport","Beles Airport","Beletwene Airport","Belfast International Airport","Belgaum Airport","Belgorod International Airport","Belgrade Nikola Tesla Airport","Belize City Municipal Airport","Bell Island Hot Springs Seaplane Base","Bella Bella","Bella Coola Airport","Bella Uni\u00f3n Airport","Bella Yella Airport","Bellary Airport","Bellburn Airport","Bellingham International Airport","Bellona-Anua Airport","Belluno Airport","Belmonte Airport","Belmullet Aerodrome","Belo sur Tsiribihina Airport","Beloretsk Airport","Beloyarsky Airport","Beluga Airport","Bel\u00e9m-Val de Cans\u2013J\u00falio Cezar Ribeiro International Airport","Bembridge Airport","Bemichi Airport","Bemidji Regional Airport","Bemolanga Airport","Ben Bruce Memorial Airpark","Ben Gurion International Airport","Ben Slimane Airport","Ben Ya\'akov Airport","Benalla Airport","Benbecula Airport","Bendigo Airport","Bengbu Airport","Benguela Airport","Benguerra Island Airport","Beni Airport","Beni Mellal Airport","Benin Airport","Benina International Airport","Benito Juarez International Airport","Benito Salas Airport","Benjam\u00edn Rivera Noriega Airport","Benjina Airport","Bensbach Airport","Benson Municipal Airport","Bento Gon\u00e7alves Airport","Benton Field","Bentota River Airport","Bequia Airport","Berane Airport","Berbera Airport","Berb\u00e9rati Airport","Berdyansk Airport","Bereina Airport","Berens River Airport","Berezovo Airport","Bergen Airport","Bergerac Dordogne P\u00e9rigord Airport","Beringin Airport","Berlev\u00e5g Airport","Berlin Brandenburg Airport","Berlin Regional Airport","Berlin Tegel Airport","Berlin-Sch\u00f6nefeld Airport","Bermejo Airport","Bermuda Dunes Airport","Bern Airport","Bert Mooney Airport","Bertoua Airport","Beru Airport","Besakoa Airport","Besalampy Airport","Beslan Airport","Bethel Airport","Bethel Seaplane Base","Betioky Airport","Betoota Airport","Betou Airport","Bettles Airport","Beverley Springs Airport","Beverly Municipal Airport","Bewani Airport","Bezmer Air Base","Bhadrapur Airport","Bhagatanwala Airport","Bhamo Airport","Bharatpur Airport","Bhavnagar Airport","Bhisho Airport","Bhojpur Airport","Bhuj Airport","Bialla Airport","Biangabip Airport","Biarritz Pays Basque Airport","Biaru Airport","Biawonque Airport","Bia\u0142a Podlaska Airport","Bickerton Island Airport","Bicycle Lake Army Airfield","Bielefeld Airport","Big Bay Marina Airport","Big Bay Yacht Club Airport,Sanhe Airport","Big Bear City Airport","Big Creek Airport","Big Lake Airport","Big Mountain Airport","Big Spring McMahon\u2013Wrinkle Airport","Big Trout Lake Airport","Biggs Army Airfield","Bijie Feixiong Airport","Biju Patnaik Airport","Bikini Atoll Airport","Bilaspur Airport","Bilbao Airport","Bildudalur Airport","Biliau Airport","Bill and Hillary Clinton National Airport","Billiluna Airport","Billings Logan International Airport","Billund Airport","Billy Bishop Toronto City Airport","Billy Mitchell Airport","Biloela Airport","Bilogai-Sugapa Airport","Bima Sultan Muhammad Salahudin Airport","Bimin Airport","Binaka Airport","Bindlacher Berg Airport","Bing\u00f6l Airport","Biniguni Airport","Bintulu Airport","Birao Airport","Biratnagar Airport","Birch Creek Airport","Birchwood Airport","Bird Island Airport","Birdsville Airport","Birjand Airport","Birmingham Airport","Birmingham-Shuttlesworth International Airport","Birsa Munda Airport","Bisbee Municipal Airport","Bisbee-Douglas International Airport","Bisha Domestic Airport","Bishe Kola Air Base","Bishkek Manas International Airport","Bishop International Airport","Biskra Airport","Bislig Airport","Bismarck Municipal Airport","Bitam Airport","Bitburg Airport","Bizant Airport","Black Hills Airport","Black Tickle Airport","Blackall Airport","Blackbushe Airport","Blackpool Airport","Blackstone Army Airfield","Blackwater Airport","Blackwell\u2013Tonkawa Municipal Airport","Blaise Diagne International Airport","Blakely Island Airport","Blanding Municipal Airport","Blangpidie Airport","Blimbingsari Airport","Block Island State Airport","Bloodvein River Airport","Bloomfield Airport","Blosser Municipal Airport","Blubber Bay Airport","Blue Canyon\u2013Nyack Airport","Blue Grass Airport","Blue Lagoon Seaplane Base","Bluefields Airport","Blumenau Airport","Blythe Airport","Blytheville Municipal Airport","Bl\u00f6ndu\u00f3s Airport","Bo Airport","Boa Vista-Atlas Brasil Cantanhede International Airport","Boana Airport","Boang Airport","Bob Baker Memorial Airport","Bob Quinn Lake Airport","Bob Sikes Airport","Bobo Dioulasso Airport","Boca Raton Airport","Bocas del Toro \"Isla Col\u00f3n\" International Airport","Bodaybo Airport","Bodinumu Airport","Bodrum-Imsik Airport","Bod\u00f8 Airport","Boende Airport","Bogande Airport","Bogashevo Airport","Bogorodskoye Airport","Boigu Island Airport","Boise Airport","Bojnord Airport","Bokondini Airport","Bokoro Airport","Bokpyin Airport","Boku Airport","Bok\u00e9 Baralande Airport","Bol-B\u00e9rim Airport","Bolaang Airport","Bole Alashankou Airport","Bollon Airport","Bologna Guglielmo Marconi Airport","Bolovip Airport","Bolwarra Airport","Bolzano Airport","Bom Jesus da Lapa Airport","Boma Airport","Bomai Airport","Bonaventure Airport","Bongo Airport","Bongor Airport","Bonito Airport","Bonnyville Airport","Bonriki International Airport","Bontang Airport","Boolgeeda Airport","Boone County Airport","Boone Municipal Airport","Booue Airport","Bora Bora Airport","Boraldai Airport","Borama Airport","Borba Airport","Bordeaux-M\u00e9rignac Airport","Bordj Mokhtar Airport","Borg El Arab Airport","Borgarfj\u00f6r\u00f0ur Eystri Airport","Boridi Airport","Borkum Airport","Borl\u00e4nge Airport","Bornholm Airport","Borrego Valley Airport","Borroloola Airport","Boryspil International Airport","Bosaso Airport","Bossangoa Airport","Bosset Airport","Bost Airport","Boston Harbor Seaplane Base","Boswell Bay Airport","Botopasi Airport","Bou Saada Airport","Bouak\u00e9 Airport","Bouar Airport","Bouarfa Airport","Bouca Airport","Boulder City Municipal Airport","Boulder Municipal Airport","Boulia Airport","Boulsa Airport","Bouna Airport","Boundary Airport","Boundary Bay Airport","Boundiali Airport","Boundji Airport","Bounneau Airport","Bourges Airport","Bourke Airport","Bournemouth Airport","Bousso Airport","Boutilimit Airport","Bovanenkovo Airport","Bowen Airport","Bowerman Airport","Bowers Field","Bowling Green-Warren County Regional Airport","Bowman Field","Bowman Municipal Airport","Bozeman Yellowstone International Airport","Bozoum Airport","Brack Airport","Brackett Field","Bradford Regional Airport","Bradley International Airport","Bradshaw Army Airfield","Braga Airport","Bragan\u00e7a Airport","Bragan\u00e7a Paulista Airport","Brahman Airport","Brainerd Lakes Regional Airport","Bram Fischer International Airport","Brampton Island Airport","Brandon Municipal Airport","Branson Airport","Brasilia International Airport","Bratislava Airport","Bratsk Airport","Braunschweig Wolfsburg Airport","Brawley Municipal Airport","Bra\u010d Airport","Brei\u00f0dalsv\u00edk Airport","Bremen Airport","Bremerhaven Airport","Bremerton National Airport","Brescia \"Gabriele D\'Annunzio\" Airport","Brest Airport","Brest Bretagne Airport","Breves Airport","Brevig Mission Airport","Brewarrina Airport","Brewster Field","Bria Airport","Brig. Lysias Augusto Rodrigues Airport","Brigadier Hector Eduardo Ruiz Airport","Brigham City Airport","Bright Airport","Brighton Downs Airport","Brindisi Airport","Brisbane Airport","Bristol Airport","Bristol Filton Airport","Britton Municipal Airport","Brive-Souillac Airport","Brno-Tu\u0159any Airport","Broadus Airport","Brochet Airport","Brockville Regional Tackaberry Airport","Broken Bow Municipal Airport","Broken Hill Airport","Brookhaven Airport","Brookings Airport","Brookings Regional Airport","Broome International Airport","Brown Field Municipal Airport","Browns Airport","Brownsville-South Padre Island International Airport","Brownwood Regional Airport","Bruce Campbell Field","Brunei International Airport","Brunette Downs Airport","Brunswick Executive Airport","Brunswick Golden Isles Airport","Brus Laguna Airport","Brussels Airport","Brussels South Charleroi Airport","Bryansk Airport","Bryant Army Heliport","Bryce Canyon Airport","Br\u00f8nn\u00f8ysund Airport","Bubaque Airport","Buchanan Airport","Buchanan Field Airport","Bucholz Army Airfield","Buckeye Municipal Airport","Buckhorn Ranch Airport","Buckland Airport","Buckley Air Force Base","Budapest Ferenc Liszt International Airport","Buenos Aires Airport","Buffalo Narrows Airport","Buffalo Niagara International Airport","Buffalo Range Airport","Bugulma Airport","Buin Airport","Bujumbura International Airport","Buka Airport","Bukhara International Airport","Bukhovtsi Airfield","Bukoba Airport","Bulagtai Resort Airport","Bulchi Airport","Bulgan Airport","Bulgan Sum Airport","Buli Airport","Bulimba Airport","Bull Harbour Airport","Bullfrog Basin Airport","Bulolo Airport","Bumba Airport","Bumi Hills Airport","Bunbury Airport","Bundaberg Airport","Bundi Airport","Bunia Airport","Buno Bedelle Airport","Bunsil Airport","Bunyu Airport","Buochs Airport","Buon Ma Thuot Airport","Buraimi Airport","Burao Airport","Burevestnik Airport","Burg Feuerstein Airport","Burgas Airport","Burgos Airport","Buriram Airport","Burketown Airport","Burley Municipal Airport","Burlington International Airport","Burnie Airport","Burns Lake Airport","Burns Municipal Airport","Burwash Airport","Bushehr Airport","Busselton Regional Airport","Buta Zega Airport","Butare Airport","Butaritari Atoll Airport","Butembo Airport","Butler County Regional Airport","Butler Memorial Airport","Butterworth Airport","Buttonville Municipal Airport","Butts Army Airfield","Buzwagi Airport","Bydgoszcz Ignacy Jan Paderewski Airport","B\u00e5tsfjord Airport","B\u00e9char Boudghene Ben Ali Lotfi Airport","B\u00e9ziers Cap d\'Agde Airport","B\u0103l\u021bi International Airport","B\u0103neasa International Airport","Cabaniss Field Naval Outlying Field Airport","Cabinda Airport","Cable Airport","Cabo Frio International Airport","Cabo Rojo Airport","Cache Creek Airport","Cachoeiro de Itapemirim Airport","Cacique Aramare Airport","Cacoal Airport","Cadjehoun Airport","Caen - Carpiquet Airport","Cafunfo Airport","Cagayan North International Airport","Cagayan de Sulu Airport","Cagliari Elmas Airport","Cahors-Lalbenque Airport","Caia Airport","Caicara del Orinoco Airport","Caiguna Airport","Cairns Airport","Cairns Army Airfield Airport","Cairo International Airport","Cairo Regional Airport","Cakung Airport","Calabozo Airport","Calais-Dunkerque Airport","Calbayog Airport","Caldas Novas Airport","Caledonia County Airport","Caleta Olivia Airport","Calexico International Airport","Calgary International Airport","Calgary-Springbank Airport","Calicut International Airport","Caloundra Airport","Calverton Executive Airpark","Calvi \u2013 Sainte-Catherine Airport","Cam Ranh International Airport","Camballin Airport","Cambridge Airport","Cambridge Bay Airport","Cambridge-Dorchester Airport","Camden Airport","Camet\u00e1 Airport","Camfield Airport","Camiguin Airport","Camilo Daza International Airport","Camiri Airport","Camocim Airport","Camooweal Airport","Camp Bastion Airport","Campbell Army Airfield","Campbell River Airport","Campbell River Harbour Airport","Campbeltown Airport","Campeche International Airport","Campina Grande Airport","Campo Grande International Airport","Campo Mour\u00e3o Airport","Campos\u2013Bartolomeu Lysandro Airport","Can Tho International Airport","Canadian Forces Base Bagotville","Canadian Forces Base Cold Lake Airport","Canadian Forces Base Greenwood Airport","Canadian Forces Base Trenton Airport","Canaima Airport","Cananea Airport","Canarana Airport","Canas Airport","Canberra Airport","Canc\u00fan International Airport","Candala Airport","Candilejas Airport","Candle 2 Airport","Canefield Airport","Cangamba Airport","Cangapara Airport","Cangyuan Washan Airport","Cannes \u2013 Mandelieu Airport","Cannon Air Force Base","Canobie Airport","Canouan Airport","Canton Island Airport","Canton Municipal Airport","Canyonlands Field","Cap Skirring Airport","Cap-Ha\u00eftien International Airport","Capanda Airport","Cape Barren Island Airport","Cape Cod Coast Guard Airport","Cape Dorset Airport","Cape Dyer Airport","Cape Flattery Airport","Cape Girardeau Regional Airport","Cape Gloucester Airport","Cape Lisburne LRRS Airport","Cape May County Airport","Cape Newenham LRRS Airport","Cape Orford Airport","Cape Palmas Airport","Cape Pole Seaplane Base","Cape Rodney Airport","Cape Romanzof LRRS Airport","Cape Sarichef Airport","Cape Town International Airport","Cape Vogel Airport","Capital City Airport","Capital Region International Airport","Capitan D Daniel Vazquez Airport","Capitan FAP Carlos Martinez De Pinillos International Airport","Capitan Nicolas Rojas Airport","Capitan Oriel Lea Plaza Airport","Capit\u00e1n Av. Juan Cochamanidis Airport","Capit\u00e1n Av. Vidal Villagomez Toledo Airpor","Capit\u00e1n FAP Pedro Canga Rodr\u00edguez Airport","Capit\u00e1n Fuentes Mart\u00ednez Airport","Capit\u00e1n Germ\u00e1n Quiroga Guardia Airport","Captain Ramon Xatruch Airport","Captain Rogelio Castillo National Airport","Capurgan\u00e1 Airport","Caquetania Airport","Car Nicobar Air Force Base Airport","Caraj\u00e1s Airport","Caransebe\u0219 Airport","Carauari Airport","Caravelas Airport","Carbon County Airport","Carcassonne Airport","Cardiff Airport","Caribou Municipal Airport","Carimagua Airport","Carlisle Lake District Airport","Carlos Manuel de C\u00e9spedes Airport","Carlos Miguel Jimenez Airport","Carlos Ruhl Airport","Carlton Hill Airport","Carmelita Airport","Carmen de Patagones Airport","Carnarvon Airport","Carnot Airport","Carora Airport","Carosue Dam Airport","Carpentaria Downs Airport","Carrasco International Airport","Carson Airport","Carti Airport","Cartwright Airport","Caruaru Airport","Carur\u00fa Airport","Carutapera Airport","Casa Grande Municipal Airport","Cascade Airport","Cascade Locks State Airport","Cascavel Airport","Casco Cove Coast Guard Station","Casigua El Cubo Airport","Casiguran Airport","Casino Airport","Casper-Natrona County International Airport","Cassidy International Airport","Cassil\u00e2ndia Airport","Castaway Island Airport","Castell\u00f3n-Costa Azahar Airport","Castle Airport","Castlereigh Reservoir Seaplane Base Airport","Castres-Mazamet Airport","Casuarito Airport","Cat Bi International Airport","Cat Cay Airport","Cat Lake Airport","Catacamas Airport","Catalina Airport","Catal\u00e3o Airport","Catamarca International Airport","Catania-Fontanarossa Airport","Cataratas Del Iguazu International Airport","Catarman National Airport","Cattaraugus County Olean Airport","Cattle Creek Airport","Catumbela Airport","Cauayan Airport","Caucay\u00e1 Airport","Cauquira Airport","Cavern City Air Terminal","Caviahue Airport","Caxias do Sul Airport","Cayana Airstrip","Caye Caulker Airport","Caye Chapel Airport","Cayenne \u2013 F\u00e9lix Ebou\u00e9 Airport","Cazombo Airport","Ca\u00e7ador Airport","Cecil Airport","Cedar City Regional Airport","Ceduna Airport","Cengiz Topel Airport","Centennial Airport","Center Island Airport","Centerville Municipal Airport","Central Airport","Central Illinois Regional Airport","Central Jersey Regional Airport","Central Maine Regional Norridgewock Airport","Central Nebraska Regional Airport","Central Wisconsin Airport","Centralia - James T. Field Memorial Aerodrome","Centralia Municipal Airport","Ceres Airport","Cerro Largo International Airport","Cesar Lim Rodriguez Airport","Cessna Aircraft Field","Cessnock Airport","Chacalluta Airport","Chacarita Airport","Chachapoyas Airport","Chacho\u00e1n Airport","Chadron Municipal Airport","Chafei Amsei Airport","Chaghcharan Airport","Chait\u00e9n Airport","Chalkyitsik Airport","Challis Airport","Chamb\u00e9ry Airport","Chan Gurney Municipal Airport","Chandalar Lake Airport","Chandigarh Airport","Chandler Field","Changbaishan Airport","Changchun Longjia International Airport","Changde Taohuayuan Airport","Changhai Airport","Changsha Huanghua International Airport","Changuinola \"Capitan Manuel Ni\u00f1o\" International Airport","Changzhi Wangcun Airport","Changzhou Benniu Airport","Chania International Airport","Chanute Martin Johnson Airport","Chaoyang Airport","Chapec\u00f3 Airport","Chapleau Airport","Charata Airport","Charles B. Wheeler Downtown Airport","Charles M. Schulz Sonoma County Airport","Charleston International Airport","Charleville Airport","Charlevoix Airport","Charlo Airport","Charlotte Amalie Harbor Seaplane Base","Charlotte Douglas International Airport","Charlottesville Albemarle Airport","Charlottetown Airport","Charters Towers Airport","Chase Field Industrial Airport","Chatham Islands-Tuuta Airport","Chatham Seaplane Base","Chatham-Kent Airport","Chattanooga Metropolitan Airport","Chaudhary Charan Singh International Airport","Chautauqua County-Dunkirk Airport","Chautauqua County-Jamestown Airport","Chaves Airport","Cha\u00f1aral Airport","Cheadle Airport","Cheboksary Airport","Cheddi Jagan International Airport","Chefornak Airport","Chehalis-Centralia Airport","Cheikh Larbi T\u00e9bessi Airport","Chelinda Airport","Chelyabinsk Airport","Chena Hot Springs Airport","Chenega Bay Airport","Chengde Puning Airport","Chengdu Shuangliu International Airport","Chennai International Airport","Chennault International Airport","Cheongju International Airport","Cheraw Municipal Airport","Cherbourg \u2013 Maupertus Airport","Cherepovets Airport","Cherif Al Idrissi Airport","Cherkasy International Airport","Chernivtsi International Airport","Chernofski Harbor Seaplane Base","Cherokee County Airport","Cherrabah Airport","Cherrabun Airport","Cherry Capital Airport","Cherskiy Airport","Chester County G. O. Carlson Airport","Chesterfield Inlet Airport","Chetumal International Airport","Chetwynd Airport","Chevak Airport","Chevery Airport","Cheyenne Regional Airport","Chhatrapati Shivaji International Airport","Chi Mei Airport","Chiang Mai International Airport","Chiang Rai International Airport","Chiayi Airport","Chibougamau-Chapais Airport","Chicago Executive Airport","Chicago Midway International Airport","Chicago Rockford International Airport","Chichen Itza International Airport","Chichester-Goodwood Airport","Chickasha Municipal Airport","Chicken Airport","Chiclayo International Airport","Chico Municipal Airport","Chicoutimi-Saint-Honor\u00e9 Aerodrome","Chifeng Yulong Airport","Chignik Bay Airport","Chignik Fisheries Airport","Chignik Lagoon Airport","Chignik Lake Airport","Chigorod\u00f3 Airport","Chihuahua International Airport","Chilas Airport","Childress Municipal Airport","Chile Chico Airport","Chileka International Airport","Chilko Lake Airport","Chillagoe Airport","Chilliwack Airport","Chilonzuine Island Airport","Chiloquin State Airport","Chimbu Airport","Chimoio Airport","Chimore Airport","China Bay Airport","China Strait Airstrip","Chinchilla Airport","Chinde Airport","Chinggis Khaan International Airport","Chingozi Airport","Chinguetti Airport","Chino Airport","Chios Island National Airport","Chipata Airport","Chipinge Airport","Chippewa County International Airport","Chippewa Valley Regional Airport","Chiquimula Airport","Chisana Airport","Chisasibi Airport","Chistochina Airport","Chita-Kadala International Airport","Chitato Airport","Chitina Airport","Chitral Airport","Chitre Alonso Valderrama Airport","Chivolo Airport","Chi\u0219in\u0103u International Airport","Chkalovsky Airport","Chlef International Airport","Choibalsan Airport","Choiseul Bay Airport","Chokurdakh Airport","Chokw\u00e9 Airport","Cholet Le Pontreau Airport","Chongqing Jiangbei International Airport","Chongqing Wushan Airport","Chos Malal Airport","Christchurch International Airport","Christiansted Harbor Seaplane Base","Christmas Creek Airport","Christmas Creek Mine Airport","Christmas Island Airport","Chu Lai International Airport","Chuathbaluk Airport","Chub Cay International Airport","Chubu Centrair International Airport","Chulman Neryungri Airport","Chumphon Airport","Chungribu Airport","Churchill Airport","Churchill Falls Airport","Chute-Des-Passes-Lac Margane Airport","Chuuk International Airport","Ch\u00e2lons Vatry Airport","Ch\u00e2teauroux-Centre \"Marcel Dassault\" Airport","Ciampino-G. B. Pastine International Airport","Cibao International Airport","Cicia Airport","Cimitarra Airport","Cincinnati Municipal Airport","Cincinnati-Northern Kentucky International Airport","Circle City Airport","Circle Hot Springs Airport","City of Derry Airport","Ciudad Acuna International Airport","Ciudad Constituci\u00f3n Airport","Ciudad Ju\u00e1rez International Airport","Ciudad Mante National Airport","Ciudad Obreg\u00f3n International Airport","Ciudad Real Central Airport","Ciudad Victoria International Airport","Ciudad de Catamayo Airport","Ciudad del Carmen International Airport","Claremont Municipal Airport","Clarence A. Bain Airport","Clark International Airport","Clarks Point Airport","Clarksville-Montgomery County Regional Airport","Clayton J. Lloyd International Airport","Clayton Municipal Airpark","Clearwater Air Park","Clermont Airport","Clermont-Ferrand Auvergne Airport","Cleve Airport","Cleveland Burke Lakefront Airport","Cleveland Hopkins International Airport","Cliff Hatfield Memorial Airport","Clifton Hills Airport","Clinton Creek Airport","Clinton Municipal Airport","Clinton Regional Airport","Clinton Sherman Airport","Clintonville Municipal Airport","Cloncurry Airport","Clorinda Airport","Clovis Municipal Airport","Club Makokola Airport","Cluff Lake Airport","Cluny Airport","Clyde River Airport","Coari Airport","Coastal Carolina Regional Airport","Coatepeque Airport","Coban Airport","Cobar Airport","Cobija Capitan Anibal Arab Airport","Cochin International Airport","Cochise County Airport","Cochrane Airport","Coconut Island Airport","Cocos Islands Airport","Codazzi Airport","Codrington Airport","Coen Airport","Coeur d\'Alene Airport","Coffeyville Municipal Airport","Coffman Cove Airport","Coffs Harbour Airport","Cognac \u2013 Ch\u00e2teaubernard Air Base Airport","Coimbatore International Airport","Coimbra Airport","Colac Airport","Colby Municipal Airport","Cold Bay Airport","Coldfoot Airport","Coleman A. Young Municipal Airport","Coleman Municipal Airport","Coles County Memorial Airport","Colima Airport","Coll Airport","Collarenebri Airport","College Park Airport","Collie Airport","Collins Bay Airport","Collinsville Airport","Colmar-Houssen Airport","Cologne Bonn Airport","Colonel Carlos Concha Torres Airport","Colonel Hill Airport","Colonia Catriel Airport","Colonia Sarmiento Airport","Colonsay Airport","Colorado Creek Airport","Colorado Plains Regional Airport","Colorado Springs Airport","Columbia Airport","Columbia County Airport","Columbia Gorge Regional Airport","Columbia Metropolitan Airport","Columbia Regional Airport","Columbus Air Force Base","Columbus Airport","Columbus Municipal Airport","Columbus-Lowndes County Airport","Colville Lake Airport","Comandante Espora Airport","Comandante FAP Germ\u00e1n Arias Graziani Airport","Comandante Gustavo Kraemer Airport","Combolcha Airport","Comilla Airport","Comiso Airport","Comodoro Arturo Merino Ben\u00edtez International Airport","Comodoro D.R. Salom\u00f3n Airport","Comodoro Pedro Zanni Airport","Comodoro Pierrestegui Airport","Comodoro Rivadavia Airport","Comox Valley Airport","Compton-Woodley Airport","Con Dao Airport","Conakry International Airport","Concei\u00e7\u00e3o do Araguaia Airport","Concepci\u00f3n Airport","Concord Municipal Airport","Concord Regional Airport","Conc\u00f3rdia Airport","Condobolin Airport","Condron Army Airfield","Confresa Airport","Connemara Regional Airport","Conroe-North Houston Regional Airport","Constanza Dom Re Airport","Contadora Airport","Converse County Airport","Coober Pedy Airport","Cooch Behar Airport","Cooinda Airport","Cooktown Airport","Coolah Airport","Coolawanyah Station Airport","Coolibah Airport","Cooma Snowy Mountains Airport","Coonabarabran Airport","Coonamble Airport","Coondewanna Airport","Cooperstown-Westville Airport","Coorabie Airport","Cootamundra Airport","Copalar Airport","Copenhagen Airport","Copiapo Desierto de Atacama Airport","Coposa Airport","Copper Center 2 Airport","Cop\u00e1n Ruinas Airport","Coral Harbour Airport","Coraz\u00f3n de Jes\u00fas Airport","Corcoran Airport","Cordillo Downs Airport","Cordova Municipal Airport","Corfu International Airport","Corisco International Airport","Cork Airport","Corn Island Airport","Cornwall Airport","Cornwall Regional Airport","Corn\u00e9lio Proc\u00f3pio Airport","Coromandel Aerodrome","Coron Airport","Coronation Airport","Coronel FAP Alfredo Mend\u00edvil Duarte Airport","Coronel FAP Carlos Ciriani Santa Rosa International Airport","Coronel FAP Francisco Secada Vignetta International Airport","Corowa Airport","Corozal Airport","Corpus Christi International Airport","Corrientes International Airport","Corryong Airport","Corsicana Airport","Cortes Bay Airport","Cortez Municipal Airport","Cortland County Airport","Corumb\u00e1 International Airport","Corvallis Municipal Airport","Corvo Airport","Costa Esmeralda Airport","Costa Marques Airport","Coto 47 Airport","Cotopaxi International Airport","Cotswold Airport","Cottbus-Drewitz Airport","Cottonwood Airport","Cotulla-La Salle County Airport","Coulter Field","Council Airport","Council Bluffs Municipal Airport","Courchevel Airport","Courtenay Airpark","Coventry Airport","Cove\u00f1as Airport","Cowarie Airport","Cowell Airport","Cowley Airport","Cowra Airport","Cox Field","Cox\'s Bazar Airport","Coyoles Airport","Cozumel International Airport","Cradock Airport","Craig Cove Airport","Craig Field","Craig Seaplane Base","Craig-Moffat Airport","Craiova Airport","Cram Field","Cranbrook-Canadian Rockies International Airport","Crane County Airport","Crane Island Airport","Crater Lake-Klamath Regional Airport","Cravo Norte Airport","Creech Air Force Base","Creil Airport","Cresswell Downs Airport","Creston Airport","Creston Municipal Airport","Croker Island Airport","Crooked Creek Airport","Crookston Municipal Airport","Cross City Airport","Cross Lake","Crossville Memorial Whitson Field","Crotone Airport","Croydon Airport","Cruzeiro do Sul International Airport","Crystal Airport","Cuamba Airport","Cudal Airport","Cuddihy Field Airport","Cue Airport","Cuito Cuanavale Airport","Culberson County Airport","Culion Airport","Cuneo International Airport","Cunnamulla Airport","Cupul Airport","Cura\u00e7ao International Airport","Currillo Airport","Curtis Field","Cururupu Airport","Curuzu Cuatia Airport","Cushing Municipal Airport","Cut Bank International Airport","Cut Nyak Dhien Airport","Cutral-Co Airport","Cuyahoga County Airport","Cuyo Airport","Cyril E. King Airport","Cz\u0119stochowa-Rudniki Airport","C\u00e0 Mau Airport","C\u00e1ceres Airport","C\u00f3rdoba Airport","Da Nang International Airport","Dabo Airport","Dabra Airport","Dade-Collier Training and Transition Airport","Dadu Airport","Daegu International Airport","Daet Airport","Dahl Creek Airport","Dahlgren Naval Surface Warfare Center Airport","Dakhla Airport","Dakhla Oasis Airport","Dalaman Airport","Dalanzadgad Airport","Dalbandin Airport","Dalbertis Airport","Dalby Airport","Dalgaranga Gold Mine Airport","Dalhart Municipal Airport","Dali Airport","Dalian Zhoushuizi International Airport","Dallas Executive Airport","Dallas Love Field","Dallas-Fort Worth International Airport","Dalnegorsk Airport","Dalnerechensk Airport","Daloa Airport","Dalton Municipal Airport","Daly River Airport","Daly Waters Airport","Dama Airport","Daman Airport","Damascus International Airport","Damazin Airport","Dambulu Oya Tank Seaplane Base","Danbury Municipal Airport","Dandong Langtou Airport","Dandugama Seaplane Base","Dane County Regional Airport","Dangriga Airport","Daniel Field","Daniel K. Inouye International Airport","Daniel Oduber Quir\u00f3s International Airport","Daniel Z. Romualdez Airport","Danilo Atienza Air Base","Dansville Municipal Airport","Danville Regional Airport","Daocheng Yading Airport","Daporijo Airport","Daqing Sartu Airport","Darchula Airport","Dare County Regional Airport","Dargaville Aerodrome","Darnley Island Airport","Daru Airport","Darwaz Airport","Darwin International Airport","Dasht-e Naz Airport","Datadawai Airport","Dathina Airport","Datong Yungang Airport","Daugavpils International Airport","Daup Airport","Davao International Airport","Davenport Downs Airport","Davenport Municipal Airport","David Wayne Hooks Memorial Airport","Davis-Monthan Air Force Base","Davison Army Airfield","Dawadmi Domestic Airport","Dawei Airport","Dawson City Airport","Dawson Community Airport","Dawson Creek Airport","Dayrestan Airport","Dayton International Airport","Dayton-Wright Brothers Airport","Daytona Beach International Airport","Dazhou Heshi Airport","Dazu Airport","Da\u015foguz Airport","De Kooy Airport","Deadhorse Airport","Deadman\'s Cay Airport","Dean River Airport","Dease Lake Airport","Deauville \u2013 Normandie Airport","Debepare Airport","Debra Marcos Airport","Debre Tabor Airport","Debrecen International Airport","Decatur Airport","Decatur County Industrial Air Park","Decatur Shores Airport","Decimomannu Air Base","Decorah Municipal Airport","Dedu Airport","Deer Harbor Seaplane Base Airport","Deer Lake Airport","Deer Lake Regional Airport","Deering Airport","Defiance Memorial Airport","Degahbur Airport","Dehong Mangshi Airport","Dehradun Airport","Deir ez-Zor Airport","Dekalb Peachtree Airport","Del Baj\u00edo International Airport","Del Norte County Regional Airport","Del Norte International Airport","Del Rio International Airport","Delaware County Regional Airport","Delingha Airport","Delissaville Airport","Delma Island Airport","Delta County Airport","Delta Downs Airport","Delta Junction Airport","Delta Municipal Airport","Dembidolo Airport","Deming Municipal Airport","Denham Airport","Deniliquin Airport","Denis Island Airport","Denison Municipal Airport","Denizli \u00c7ardak Airport","Denver International Airport","Depati Amir Airport","Deputatsky Airport","Dera Ghazi Khan International Airport","Dera Ismail Khan Airport","Derby Airport","Derby Field","Derim Airport","Des Moines International Airport","Desert Rock Airport","Desroches Airport","Destin Executive Airport","Destin-Fort Walton Beach Airport","Detroit Lakes Airport","Detroit Metro Airport","Devi Ahilya Bai Holkar Airport","Devils Lake Regional Airport","Devonport Airport","Dewadaru Airport","Dezful Airport","Dhaalu Atoll Airport","Dhaka Shahjalal International Airport","Dhala Airport","Dhamar Airport","Dhanbad Airport","Dhangadhi Airport","Dharamsala Gaggal Kangra Airport","Dharavandhoo Airport","Diamantina Airport","Diamantina Lakes Airport","Diamantino Airport","Dian\u00f3polis Airport","Diapaga Airport","Diavik Airport","Dibba Airport","Dibrugarh Airport","Dickinson Theodore Roosevelt Regional Airport","Diebougou Airport","Diego Jimenez Torres Airport","Dien Bien Phu Airport","Digby - Annapolis Regional Airport","Dijon-Bourgogne Airport","Dikson Airport","Dilasag Airport","Dillant-Hopkins Airport","Dillingham Airfield","Dillingham Airport","Dillon Airport","Dillon County Airport","Dillon\'s Bay Airport","Dimapur Airport","Dimbokro Airport","Dimmit County Airport","Dinangat Airport","Dinard\u2013Pleurtuit\u2013Saint-Malo Airport","Dinwiddie County Airport","Diom\u00edcio Freitas Airport","Diori Hamani International Airport","Dios Airport","Dipolog Airport","Diqing Shangri-La Airport","Dirico Airport","Dirranbandi Airport","Diu Airport","Divin\u00f3polis Airport","Divo Airport","Dixie Airport","Diyarbak\u0131r Airport","Diyawanna Oya Seaplane Base Airport","Djambala Airport","Djanet Inedbirene Airport","Djerba-Zarzis International Airport","Djibo Airport","Djibouti-Ambouli International Airport","Djoemoe Airport","Djougou Airport","Dj\u00fapivogur Airport","Dnipropetrovsk International Airport","Do Palmar Airport","Doany Airport","Dobbins Air Reserve Base","Docker River Airport","Dodge City Regional Airport","Dodge County Airport","Dodoima Airport","Dodoma Airport","Doha International Airport","Doini Airport","Dolbeau-Saint-F\u00e9licien Airport","Dole-Jura Airport","Dolisie Airport","Dolpa Airport","Domine Eduard Osok Airport","Domingo Faustino Sarmiento Airport","Domodedovo International Airport","Don Mueang International Airport","Donaldson Field Airport","Doncaster Sheffield Airport","Donegal Airport","Donetsk International Airport","Dong Hoi Airport","Dong Tac Airport","Dongara Airport","Dongola Airport","Dongsha Island Airport","Dongying Shengli Airport","Donnelly Airport","Donoi Airport","Doomadgee Airport","Doongan Airport","Door County Cherryland Airport","Dori Airport","Doris Lake Airport","Dornoch Airport","Dorobisoro Airport","Dortmund Airport","Dorunda Airport","Dos Lagunas Airport","Dothan Regional Airport","Douala International Airport","Douglas Lake Airport","Douglas Municipal Airport","Douglas-Charles Airport","Dourados Airport","Dover Air Force Base","Doylestown Airport","Dr Juan Plate Airport","Dr. Antonio Nicol\u00e1s Brice\u00f1o Airport","Dr. Arturo Umberto Illia Airport","Dr. Augusto Roberto Fuster International Airport","Dr. Babasaheb Ambedkar International Airport","Dr. Ferdinand Lumban Tobing Airport","Dr. Juan C. Angara Airport","Dr. Luis Mar\u00eda Arga\u00f1a International Airport","Drake Bay Airport","Drake Field","Draughon-Miller Central Texas Regional Airport","Drayton Valley Industrial Airport","Dresden Airport","Drietabbetje Airport","Drift River Airport","Drumduff Airport","Drummond Airport","Drummond Island Airport","Dryden Regional Airport","Drysdale River Airport","Dschang Airport","DuBois Regional Airport","DuPage Airport","Dubai Creek Seaplane Base Airport","Dubai International Airport","Dubbo City Regional Airport","Dublin Airport","Dubois Municipal Airport","Dubrovnik Airport","Dubuque Regional Airport","Duke Field","Dukuduk Airport","Dulkaninna Airport","Duluth International Airport","Dumpu Airport","Dunbar Airport","Duncan Airport","Duncan Town Airport","Dundee Airport","Dundo Airport","Dunedin International Airport","Dunhuang Airport","Dunk Island Airport","Dunwich Airport","Duqm International Airport","Durango International Airport","Durango-La Plata County Airport","Durant Regional Airport","Durham Downs Airport","Durham Tees Valley Airport","Durrie Airport","Dushanbe International Airport","Dutchess County Airport","Dwangwa Airport","Dyess Air Force Base","Dysart Airport","Dywer Airbase Airport","Dzaoudzi Pamandzi International Airport","D\u00e9dougou Airport","D\u00e9line Airport","D\u00fcsseldorf Airport","Eagle Airport","Eagle County Regional Airport","Eagle Lake Airport","Eagle River Union Airport","Eareckson Air Station","Earlton","East Hampton Airport","East Kimberley Regional Airport","East London Airport","East Midlands Airport","East Texas Regional Airport","Eastern Oregon Regional Airport","Eastern Sierra Regional Airport","Eastern Slopes Regional Airport","Eastern West Virginia Regional Airport","Easterwood Airport","Eastland Municipal Airport","Eastmain River Airport","Easton Airport","Easton State Airport","Ebadon Airport","Ebolowa Airport","Ebon Airport","Echuca Airport","Ed Daein Airport","Ed-Air Airport","Eday Airport","Edinburgh Airport","Edmonton City Centre","Edmonton International Airport","Edna Bay Airport","Edremit Bal\u0131kesir Koca Seyit Airport","Edson Airport","Eduardo Falla Solano Airport","Eduardo Gomes International Airport","Edwaki Airport","Edward Bodden Airfield","Edward F. Knapp State Airport","Edward G. Pitka Sr. Airport","Edward River Airport","Edwards Air Force Base","Eek Airport","Efogi Airport","Egegik Airport","Egilssta\u00f0ir Airport","Eielson Air Force Base","Eil Airport","Eilat Airport","Ein Yahav Airfield","Einasleigh Airport","Eindhoven Airport","Eirunep\u00e9 Airport","Eisenach-Kindel Airport","Ejin Banner Taolai Airport","Ekati Airport","Ekereku Airport","Ekibastuz Airport","Ekuk Airport","Ekwok Airport","El Alamein International Airport","El Alcarav\u00e1n Airport","El Alto International Airport","El Arish International Airport","El Array\u00e1n Airport","El Bagre Airport","El Bayadh Airport","El Bolson Airport","El Borma Airport","El Calafate International Airport","El Cara\u00f1o Airport","El Centro Naval Air Facility Airport","El Charco Airport","El Debba Airport","El Dorado Airport","El Dorado International Airport","El Ed\u00e9n International Airport","El Embrujo Airport","El Encanto Airport","El Fasher Airport","El Golea Airport","El Hierro Airport","El Kharga Airport","El Lencero Airport","El Loa Airport","El Maiten Airport","El Minya Airport","El Mirador Airport","El Naranjo Airport","El Nido Airport","El Nouzha Airport","El Obeid Airport","El Palomar Airport","El Paso International Airport","El Plumerillo International Airport","El Porvenir Airport","El Real Airport","El Recreo Airport","El Taj\u00edn National Airport","El Tari Airport","El Tehuelche Airport","El Tepual Airport","El Tigre Airport","El Tor Airport","El Trompillo Airport","El Troncal Airport","El Tuqui Airport","Elaz\u0131\u011f Airport","Elcho Island Airport","Eldoret International Airport","Elelim Airport","Elenak Airport","Elfin Cove Seaplane Base","Elim Airport","Eliptamin Airport","Elista Airport","Eliye Springs Airport","Elizabeth City Regional Airport","Elizabeth Field","Elizabethtown Regional Airport","Elk City Regional Business Airport","Elkedra Airport","Elkhart Municipal Airport","Elkins-Randolph Co-Jennings Randolph Field Airport","Elko Regional Airport","Ellamar Seaplane Base","Ellington Airport","Ellinikon International Airport","Elliot Lake Municipal Airport","Ellisras Matimba Airport","Ellsworth Air Force Base","Elmendorf Air Force Base","Elmira Corning Regional Airport","Elorza Airport","Eloy Alfaro International Airport","Elrose Mine Airport","Ely Airport","Ely Municipal Airport","Emalamo Airport","Embessa Airport","Emden Airport","Emerald Airport","Emirau Airport","Emmen Air Base","Emmonak Airport","Emo River Airstrip","Empangeni Airport","Emporia Municipal Airport","En Nahud Airport","Enarotali Airport","Eneabba Airport","Enejit Airport","Enewetak Auxiliary Airfield","Enfidha \u2013 Hammamet International Airport","Engati Airport","Enid Woodring Regional Airport","Enonteki\u00f6 Airport","Enrique Adolfo Jimenez Airport","Enrique Malek International Airport","Enrique Olaya Herrera Airport","Enschede Twente Airport","Ensenada Airport","Enshi Xujiaping Airport","Entebbe International Airport","Enterprise Municipal Airport","Entrance Island Seaplane Base","Enua Airport","Epena Airport","Ephrata Municipal Airport","Eppley Airfield","Erandique Airport","Erave Airport","Erbil International Airport","Ercan International Airport","Erdenet Airport","Erechim Airport","Erenhot Saiwusu International Airport","Erfurt-Weimar Airport","Erie International Airport","Erigavo Airport","Erldunda Airport","Ernabella Airport","Ernest A. Love Field","Ernesto Cortissoz International Airport","Eros Airport","Errol Airport","Erume Airport","Erzincan Airport","Erzurum Airport","Esa\'ala Airport","Esbjerg Airport","Esenbo\u011fa International Airport","Eskilstuna Airport","Eski\u015fehir Air Base Airport","Esler Regional Airport","Esperadinha Airport","Esperance Airport","Espinosa Airport","Esquel Airport","Essaouira-Mogador Airport","Essen-M\u00fclheim Airport","Essex County Airport","Estevan Airport","Estherville Municipal Airport","Etadunna Airport","Etimesgut Air Base","Eucla Airport","Eugene Airport","Eugenio Mar\u00eda de Hostos Airport","Eunice Airport","Eureka Airport","Eurico de Aguiar Salles Airport","EuroAirport Basel Mulhouse Freiburg","EuroAirport Basel-Mulhouse-Freiburg","Eva Downs Airport","Evans Head Airport","Evanston-Uinta County Burns Field","Evansville Regional Airport","Eveleth-Virginia Municipal Airport","Evelio Javier Airport","Everett-Stewart Regional Airport","Ewer Airport","Ewo Airport","Excursion Inlet Seaplane Base","Exeter Airport","Exmouth Airport","Exuma International Airport","F. D. Roosevelt Airport","Faaite Airport","Fabio Alberto Le\u00f3n Bentley Airport","Fada N\'gourma Airport","Fagali\'i Airport","Fagurh\u00f3lsm\u00fdri Airport","Fahud Airport","Fair Isle Airport","Fairbanks International Airport","Fairbury Municipal Airport","Fairchild Air Force Base","Fairfield Municipal Airport","Fairmont Hot Springs Airport","Fairmont Municipal Airport","Fairview Airport","Faisalabad International Airport","Fakahina Airport","Fakarava Airport","Fakfak Torea Airport","Falcone-Borsellino Airport","Faleolo International Airport","Fallon Municipal Airport","Fallon Naval Air Station","Falls International Airport","False Island Seaplane Base","False Pass Airport","Fane Airport","Fangatau Airport","Farafangana Airport","Farah Airport","Faranah Airport","Farewell Airport","Faribault Municipal Airport","Farmington Regional Airport","Farnborough Airport","Faro Airport","Fasa Airport","Fascene Airport","Fatmawati Soekarno Airport","Faya-Largeau Airport","Fayetteville Municipal Airport","Fayetteville Regional Airport","Fayzabad Airport","Fazenda Piraguassu Airport","Fazenda Tucunar\u00e9 Airport","Fderik Airport","Federico Fellini International Airport","Federico Garcia Lorca Airport","Feij\u00f3 Airport","Feira de Santana Airport","Felker Army Airfield","Felts Field","Fera Island Airport","Feramin Airport","Fergana International Airport","Fergus Falls Municipal Airport","Ferkess\u00e9dougou Airport","Fernando Luis Ribas Dominicci Airport","Fernando de Noronha Airport","Ferry Port","Fetlar Airport","Fianarantsoa Airport","Ficksburg Sentraoes Airport","Figari-Sud-Corse Airport","Filadelfia Airport","Fillmore Municipal Airport","Fincha Airport","Findlay Airport","Finke Airport","Finley Airport","Finschhafen Airport","First Flight Airport","Fitiuta Airport","Fitzroy Crossing Airport","Flabob Airport","Flagstaff Pulliam Airport","Flamingo International Airport","Flat Airport","Flensburg-Sch\u00e4ferhaus Airport","Fletcher Field","Flin Flon Airport","Flinders Island Airport","Flora Valley Airport","Florence Municipal Airport","Florence Regional Airport","Flores Airport","Florida Keys Marathon International Airport","Flor\u00f8 Airport","Flotta Isle Airport","Floyd Bennett Memorial Airport","Flying Cloud Airport","Foggia \"Gino Lisa\" Airport","Fond du Lac County Airport","Fond-du-Lac Airport","Fontanges Airport","Fonte Boa Airport","Foothills Regional Airport","Forbes Airport","Ford Airport","Forest City Municipal Airport","Forestville Airport","Forl\u00ec International Airport","Formosa International Airport","Forrest Airport","Forrest City Municipal Airport","Forrest River Airport","Forster Airport","Fort Albany Airport","Fort Bragg Airport","Fort Bridger Airport","Fort Chipewyan Airport","Fort Dodge Regional Airport","Fort Frances Municipal Airport","Fort Good Hope Airport","Fort Hope Airport","Fort Lauderdale Executive Airport","Fort Lauderdale-Hollywood International Airport","Fort Liard Airport","Fort MacKay-Albian Aerodrome","Fort MacKay-Firebag Aerodrome","Fort Mackay - Horizon Airport","Fort Madison Municipal Airport","Fort McMurray International Airport","Fort McMurray-Mildred Lake Airport","Fort McPherson Airport","Fort Reliance Airport","Fort Resolution Airport","Fort Scott Municipal Airport","Fort Severn Airport","Fort Simpson Airport","Fort Smith Airport","Fort Smith Regional Airport","Fort St. John Airport","Fort Stockton Pecos County Airport","Fort Sumner Municipal Airport","Fort Wayne International Airport","Fort Worth Alliance Airport","Fort Worth Meacham International Airport","Fort Yukon Airport","Fortescue Dave Forrest Airport","Foshan Shadi Airport","Fossil Downs Airport","Fougamou Airport","Foula Airport","Foumban Nkounja Airport","Four Corners Regional Airport","Foya Airport","Foz do Igua\u00e7u-Cataratas International Airport","Franca Airport","Francis S. Gabreski Airport","Francisco B. Reyes Airport","Francisco Beltr\u00e3o Airport","Francisco Carle Airport","Francisco Primo de Verdad National Airport","Francisco de Assis Airport","Francisco de Orellana Airport","Francistown Airport","Franco Bianco Airport","Frank Pais International Airport","Frankfurt am Main Airport","Frankfurt-Hahn Airport","Franklin County Airport","Franklin Municipal-John Beverly Rose Airport","Frans Kaisiepo Airport","Frans Sales Lega Airport","Frans Xavier Seda Airport","Franz Josef Glacier Airport","Frederick Municipal Airport","Frederick Regional Airport","Fredericton International Airport","Freeman Municipal Airport","Fremont County Airport","Fremont Municipal Airport","French Lick Municipal Airport","French Valley Airport","Fresno Chandler Executive Airport","Fresno Yosemite International Airport","Fria Airport","Friday Harbor Airport","Friday Harbor Seaplane Base","Frieda River Airport","Friedman Memorial Airport","Friedrichshafen Airport","Fritzlar Airport","Front Royal-Warren County Airport","Frutillar Airport","Fr\u00e9gate Island Airport","Fua\u02bbamotu International Airport","Fuerte Olimpo Airport","Fuerteventura Airport","Fujairah International Airport","Fukue Airport","Fukui Airport","Fukuoka Airport","Fukushima Airport","Fulleborn Airport","Fullerton Municipal Airport","Fulton County Airport","Fuma Airport","Funafuti International Airport","Funter Bay Seaplane Base","Furnace Creek Airport","Futaleuf\u00fa Airport","Futuna Airport","Fuvahmulah Airport","Fuyang Xiguan Airport","Fuyuan Dongji Airport","Fuyun Koktokay Airport","Fuzhou Changle International Airport","F\u00e1skr\u00fa\u00f0sfj\u00f6r\u00f0ur Airport","F\u00e8s\u2013Sa\u00efss Airport","F\u00f8rde Airport","F\u00fcrstenfeldbruck Air Base","Gabbs Airport","Gabriel Vargas Santos Airport","Gab\u00e8s \u2013 Matmata International Airport","Gachsaran Airport","Gafsa Ksar International Airport","Gag Island Airport","Gage Airport","Gagnoa Airport","Gainesville Municipal Airport","Gainesville Regional Airport","Galatina Air Base Airport","Galbraith Lake Airport","Galcaio Airport","Galegu Airport","Galesburg Municipal Airport","Galion Municipal Airport","Gallup Municipal Airport","Galway Airport","Gamar Malamo Airport","Gamarra Airport","Gamba Airport","Gambela Airport","Gambell Airport","Gamboa Airport","Gamboma Airport","Gamboola Airport","Gam\u00e8t\u00ec-Rae Lakes Airport","Gan International Airport","Gandajika Airport","Gander International Airport","Gangaw Airport","Ganges Harbor Airport","Gangneung Airport","Ganja International Airport","Gannan Xiahe Airport","Gansner Field","Ganzhou Huangjin Airport","Gao International Airport","Gaoua Airport","Gap-Tallard Airport","Garachin\u00e9 Airport","Garaina Airport","Garasa Airport","Garbaharey Airport","Gardabya Airport","Garden City Regional Airport","Garden County Airport","Garden Point Airport","Gardez Airport","Gardner Municipal Airport","Gardo Airport","Garfield County Regional Airport","Garissa Airport","Garner Field","Garoua International Airport","Garowe Airport","Garrett County Airport","Garuahi Airport","Garze Gesar Airport","Gascoyne Junction Airport","Gasmata Island Airport","Gasuke Airport","Gatineau-Ottawa Executive Airport","Gatlinburg-Pigeon Forge Airport","Gatokae Airport","Gatwick Airport","Gau Airport","Gaua Island Airport","Gautam Buddha Airport","Gaya Airport","Gaylord Regional Airport","Gayndah Airport","Gaziantep O\u011fuzeli International Airport","Gazipa\u015fa-Alanya Airport","Gbadolite Airport","Gbangbatok Airport","Gda\u0144sk Lech Wa\u0142\u0119sa Airport","Gebe Airport","Gecitkale Airport","Geelong Airport","Geilenkirchen Airport","Geita Airport","Geladi Airport","Gelendzhik Airport","Gelephu Airport","Gemena Airport","Genda Wuha Airport","Geneina Airport","General Bartolom\u00e9 Salom Airport","General Bernardo O\'Higgins Airport","General Edward Lawrence Logan International Airport","General Jos\u00e9 Antonio Anzo\u00e1tegui International Airport","General Jos\u00e9 Francisco Berm\u00fadez Airport","General Jos\u00e9 Mar\u00eda Y\u00e1\u00f1ez International Airport","General Juan N. \u00c1lvarez International Airport","General Lucio Blanco International Airport","General Manuel Serrano Airport","General Mariano Matamoros Airport","General Mitchell International Airport","General Navas Pardo Airport","General Pico Airport","General Santos International Airport","General Sudirman Airport","General Ulpiano Paez Airport","General Villamil Airport","General Villegas Airport","General Wayne A. Downing Peoria International Airport","General William J. Fox Airfield","Geneva Airport","Genoa Cristoforo Colombo Airport","George Airport","George Best Belfast City Airport","George Bush Intercontinental Airport","George Downer Airport","George F. L. Charles Airport","George R Carr Memorial Airfield","George T. Lewis Airport","George Town Airport","Georgetown Airport","Georgetown County Airport","Gerald R. Ford International Airport","Geraldton Airport","Geraldton Greenstone Regional Airport","Gerardo Tobar L\u00f3pez Airport","German Olano Air Base","German Olano Airport","Gettysburg Regional Airport","Geva Airport","Gewayantana Airport","Gewoia Airport","Ghadames Airport","Ghanzi Airport","Ghat Airport","Ghazni Airport","Ghinnir Airport","Ghriss Airport","Gibb River Airport","Gibraltar International Airport","Giebelstadt Airport","Gilgal Airport","Gilgit Airport","Gillam Airport","Gillespie Field","Gillette-Campbell County Airport","Gillies Bay Airport","Gilze-Rijen Air Base","Gimhae International Airport","Gimli Industrial Park Airport","Gimpo International Airport","Ginbata Airport","Girdwood Airport","Girona-Costa Brava Airport","Girua Airport","Gisborne Airport","Gisenyi Airport","Gitega Airport","Giyani Airport","Gjoa Haven Airport","Gj\u00f6gur Airport","Glacier Creek Airport","Glacier Park International Airport","Gladstone Airport","Gladwin Zettel Memorial Airport","Glasgow International Airport","Glasgow Municipal Airport","Glasgow Prestwick Airport","Glasgow Valley County Airport","Glen Innes Airport","Glendale Fokker Field Airport","Glengyle Airport","Glenormiston Airport","Glentanner Airport","Glenwood Springs Municipal Airport","Gloucestershire Airport","Gnarowein Airport","Goa Airport","Gobabis Airport","Gobernador Edgardo Castello Airport","Gobernador Gregores Airport","Gobernador Horacio Guzman International Airport","Gode Airport","Godman Army Airfield","Godofredo P. Ramos Airport","Gods Lake Narrows Airport","Gods River Airport","Gogebic\u2013Iron County Airport","Gol Airport","Gold Beach Municipal Airport","Gold Coast Airport","Golden Airport","Golden Horn Lodge Seaplane Base","Golden Triangle Regional Airport","Goldsworthy Airport","Golfito Airport","Golfo de Morrosquillo Airport","Golmud Airport","Golog Maqin Airport","Goloson International Airport","Golovin Airport","Goma International Airport","Gombe Lawanti International Airport","Gomel Airport","Gonaili Airport","Gondar Airport","Gonzalo Mej\u00eda Airport","Gooding Municipal Airport","Goodland Municipal Airport","Goodnews Airport","Goondiwindi Airport","Goose Bay Airport","Gora Airport","Gorakhpur Airport","Gordil Airport","Gordon Downs Airport","Gordon K. Bush Airport","Gordon Municipal Airport","Gordonsville Municipal Airport","Gore Airport","Gore Bay-Manitoulin Airport","Gorgan Airport","Gorna Oryahovitsa Airport","Gorno-Altaysk Airport","Goroka Airport","Gorom-Gorom Airport","Gosford Airport","Goshen Municipal Airport","Gothenburg City Airport","Goulburn Airport","Goulimime Airport","Gounda Airport","Goundam Airport","Gov. Dix-Sept Rosado Airport","Gove Airport","Governador Valadares Airport","Governor\'s Harbour Airport","Goya Airport","Gracias Airport","Graciosa Airport","Grafton Airport","Gran Canaria Airport","Grand Bahama International Airport","Grand Canyon Caverns Airport","Grand Canyon National Park Airport","Grand Canyon West Airport","Grand Case-Esp\u00e9rance Airport","Grand Central Airport","Grand Cess Airport","Grand Coulee Dam Airport","Grand Forks Air Force Base Airport","Grand Forks Airport","Grand Forks International Airport","Grand Geneva Resort Airport","Grand Junction Regional Airport","Grand Lake Regional Airport","Grand Marais-Cook County Airport","Grand Rapids\u2013Itasca County Airport","Grand Strand Airport","Grand-Santi Airport","Grande Cache Airport","Grande Prairie Airport","Granite Mountain Air Station","Grant County Airport","Grant County International Airport","Grant County Regional Airport","Grantley Adams International Airport","Grants Pass Airport","Grants-Milan Municipal Airport","Grantsburg Municipal Airport","Granville Airport","Gratiot Community Airport","Gray Army Airfield","Grayling Airport","Graz Airport","Great Barrier Aerodrome","Great Bear Lake Airport","Great Bend Municipal Airport","Great Falls International Airport","Great Harbour Cay Airport","Great Keppel Island Airport","Greater Binghamton Airport","Greater Cumberland Regional Airport","Greater Kankakee Airport","Greater Moncton Rom\u00e9o LeBlanc International Airport","Greater Portsmouth Regional Airport","Greater Rochester International Airport","Greeley-Weld County Airport","Green Bay\u2013Austin Straubel International Airport","Green River Airport","Green River Municipal Airport","Greenbrier Valley Airport","Greene County Airport","Greeneville-Greene County Municipal Airport","Greenlee County Airport","Greenvale Airport","Greenville Airport","Greenville Downtown Airport","Greenville Sinoe Airport","Greenville-Spartanburg International Airport","Greenway Sound Airport","Greenwood County Airport","Greenwood-Leflore Airport","Gregorio Luperon International Airport","Gregory Downs Airport","Gregory Lake Seaplane Base Airport","Grenfell Airport","Greymouth Airport","Grider Field Airport","Griffing Sandusky Airport","Griffiss International Airport","Griffith Airport","Grise Fiord Airport","Grissom Air Reserve Base","Groningen Airport Eelde","Groote Eylandt Airport","Grootfontein Airport","Grosseto Airport","Groton\u2013New London Airport","Grozny Airport","Grumeti Airstrip Airport","Grundarfj\u00f6r\u00f0ur Airport","Gr\u00edmsey Airport","Guacamayas Airport","Guadalupe Airport","Guajar\u00e1-Mirim Airport","Gualeguaychu Airport","Guamal Airport","Guanaja Airport","Guanambi Airport","Guanare Airport","Guanghan Airport","Guangyuan Panlong Airport","Guangzhou Baiyun International Airport","Guangzhou MR Air Base Airport","Guapiles Airport","Guaran\u00ed International Airport","Guarapari Airport","Guaratinguet\u00e1 Airport","Guardiamarina Za\u00f1artu Airport","Guari Airport","Guasdualito Airport","Guasopa Airport","Guayaramer\u00edn Airport","Guemar Airport","Guerima Airport","Guernsey Airport","Guerrero Negro Airport","Guiglo Airport","Guilin Liangjiang International Airport","Guillermo Le\u00f3n Valencia Airport","Guimar\u00e3es Airport","Guiria Airport","Guiyang Longdongbao International Airport","Gujrat Airport","Gulfport\u2013Biloxi International Airport","Gulgubip Airport","Gulkana Airport","Gulu Airport","Guna Airport","Gunnedah Airport","Gunnison-Crested Butte Regional Airport","Gunsan Airport","Gunung Batin Airport","Gurayat Domestic Airport","Guriaso Airport","Guriel Airport","Gurney Airport","Gurue Airport","Gurupi Airport","Gusap Airport","Gush Katif Airport","Gustaf III Airport","Gustavo Artunduaga Paredes Airport","Gustavo Rizo Airport","Gustavo Rojas Pinilla International Airport","Gustavus Airport","Guthrie County Regional Airport","Guthrie-Edmond Regional Airport","Guymon Municipal Airport","Guyuan Liupanshan Airport","Gwa Airport","Gwadar International Airport","Gwalior Airport","Gwangju Airport","Gwinnett County Airport","Gympie Airport","G\u00e4llivare Airport","G\u00e4vle-Sandviken Airport","G\u00f6k\u00e7eada Airport","G\u00f6teborg Landvetter Airport","H. H. Coffield Regional Airport","H. Hasan Aroeboesman Airport","H.A.S. Hanandjoeddin Airport","Ha\'il Regional Airport","Habi Airport","Hachijojima Airport","Hachinohe Airport","Haelogo Airport","Hagerstown Regional Airport","Hagfors Airport","Haibei Qilian Airport","Haifa Airport","Haikou Meilan International Airport","Haines Airport","Haines Junction Airport","Haivaro Airport","Haji Aroeppala Airport","Hakai Pass Airport","Hakkari Y\u00fcksekova Airport","Hakodate Airport","Halali Airport","Hale County Airport","Half Moon Bay Airport","Halifax County Airport","Halifax Stanfield International Airport","Halim Perdanakusuma International Airport","Haliwen Airport","Hall Beach Airport","Halli Airport","Halliburton Field","Halls Creek Airport","Halmstad Airport","Hamad International Airport","Hamadan Airport","Hamburg Airport","Hamburg-Finkenwerder Airport","Hami Airport","Hamid Karzai International Airport","Hamilton Airport","Hamilton Field Airport","Hamilton Island Airport","Hammerfest Airport","Hampton Municipal Airport","Hana Airport","Hanamaki Airport","Hancock County-Bar Harbor Airport","Handan Airport","Haneda Airport","Hang Nadim International Airport","Hangzhou Xiaoshan International Airport","Hanimaadhoo International Airport","Hanksville Airport","Hannover Airport","Hanzhong Chenggu Airport","Hao Airport","Harare International Airport","Harbin Taiping International Airport","Hargeisa Airport","Harrell Field","Harriet Alexander Field","Harris County Airport","Harrisburg International Airport","Harrisburg-Raleigh Airport","Harrismith Airport","Harrison County Airport","Harry Clever Field","Harry Mwanga Nkumbula International Airport","Harry P. Williams Memorial Airport","Harry Stern Airport","Harstad-Narvik Airport, Evenes","Hartford-Brainard Airport","Hartley Bay Airport","Hartness State Airport","Hartsfield-Jackson Atlanta International Airport","Hartsville Regional Airport","Hassan I Airport","Hassi Messaoud Airport","Hassi R\'Mel Airport","Hastings Airport","Hastings Municipal Airport","Hasvik Airport","Hat Yai International Airport","Hatay Airport","Hatchet Lake Airport","Hateruma Airport","Hato Corozal Airport","Hattiesburg Bobby L Chain Municipal Airport","Hattiesburg-Laurel Regional Airport","Hatzfeldhaven Airport","Haugesund Airport, Karm\u00f8y","Havadarya Airport","Haverfordwest Airport","Havre City-County Airport","Havre St Pierre Airport","Hawabango Airport","Hawarden Airport","Hawk Inlet Seaplane Base Airport","Hawke\'s Bay Airport","Hawkins Field","Hawthorne Industrial Airport","Hawthorne Municipal Airport","Hay Airport","Hay River Merlyn Carter Airport","Hayfields Airport","Hays Regional Airport","Hayward Executive Airport","Hazleton Municipal Airport","Hazyview Airport","Headingly Airport","Healy Lake Airport","Hearst Ren\u00e9 Fontaine Municipal Airport","Heathlands Airport","Heathrow Airport","Hechi Jinchengjiang Airport","Hector International Airport","Hefei Xinqiao International Airport","Heho Airport","Heide-B\u00fcsum Airport","Heidelberg Airport","Heihe Airport","Heiweni Airport","Helena Regional Airport","Helenvale Airport","Heligoland Airport","Helmuth Baungartem Airport","Helsinki Airport","Helsinki-Malmi Airport","Hemavan T\u00e4rnaby Airport","Hemet-Ryan Airport","Henbury Airport","Henderson Executive Airport","Henderson Field","Hendrik Van Eck Airport","Hengchun Airport","Hengyang Nanyue Airport","Henri Coand\u0103 International Airport","Henry County Airport","Henry Post Army Airfield","Henry Tift Myers Airport","Heraklion International Airport","Herat International Airport","Herc\u00edlio Luz International Airport","Herendeen Bay Airport","Heriberto G\u00edl Mart\u00ednez Airport","Heringsdorf Airport","Heritage Field","Hermannsburg Airport","Hermanos Ameijeiras Airport","Hermes Quijada International Airport","Hermiston Municipal Airport","Hermosillo International Airport","Herrera Airport","Hervey Bay Airport","Hesa Airport","Hesler-Noble Field","Hewanorra International Airport","Heydar Aliyev International Airport","Hickory Regional Airport","High Level Airport","High Prairie Airport","Highbury Airport","Higuerote Airport","Hihifo Airport","Hikueru Airport","Hill Air Force Base","Hill City Municipal Airport","Hillenbrand Industries Airport","Hillside Airport","Hilo International Airport","Hilton Head Airport","Hingurakgoda Airport","Hinkles Ferry Airport","Hinthada Airport","Hinton-Jasper-Hinton Airport","Hiroshima Airport","Hiroshimanishi Airport","Hisar Airport","Hluhluwe Airport","Hobart International Airport","Hobart Regional Airport","Hodeidah International Airport","Hoedspruit Airport","Hof-Plauen Airport","Hog River Airport","Hohenems-Dornbirn Airport","Hohhot Baita International Airport","Hokitika Airport","Hola Airport","Holbrook Municipal Airport","Hole\u0161ov Airport","Holingol Huolinhe Airport","Hollis Clark Bay Seaplane Base","Hollister Municipal Airport","Holloman Air Force Base","Hollywood Burbank Airport","Holt Airport","Holy Cross Airport","Homalin Airport","Homer Airport","Homestead Air Reserve Base Airport","Hon Airport","Hong Kong International Airport","Honiara International Airport","Honinabi Airport","Honningsv\u00e5g Airport","Hood Army Airfield","Hooker County Airport","Hooker Creek Airport","Hoonah Airport","Hooper Bay Airport","Hope Airport","Hope Bay Airport","Hope Vale Airport","Hopedale Airport","Hopetoun Airport","Horizontina Airport","Horn Island Airport","Hornafj\u00f6r\u00f0ur Airport","Hornepayne Municipal Airport","Horsham Airport","Horta Airport","Hosea Kutako International Airport","Hoskins Airport","Hot Springs County Airport","Hotan Airport","Houari Boumediene Airport","Houghton County Memorial Airport","Houlton International Airport","Houma-Terrebonne Airport","Hoy Island Airport","Hpa-An Airport","Hpapun Airport","Hrodna Airport","Hsinchu Air Base","Hua Hin Airport","Huacaraje Airport","Huahine \u2013 Fare Airport","Huai\'an Lianshui Airport","Huaihua Zhijiang Airport","Hualien Airport","Huallaga Airport","Huangshan Tunxi International Airport","Huanuco Airpor","Huatugou Airport","Hubli Airport","Hudiksvall Airport","Hudson Bay Airport","Hudson\'s Hope Airport","Huehuetenango Airport","Huesca\u2013Pirineos Airport","Hughenden Airport","Hughes Airport","Huizhou Pingtan Airport","Hukuntsi Airport","Hultsfred Airport","Hulunbuir Hailar Airport","Humacao Airport","Humait\u00e1 Airport","Humberside Airport","Humbert River Airport","Humboldt Municipal Airport","Humen Airport","Humera Airport","Hunt Field","Hunter Army Airfield","Huntingburg Airport","Huntsville International Airport","Huntsville Regional Airport","Hurghada International Airport","Huron Regional Airport","Husein Sastranegara International Airport","Huslia Airport","Husum Schwesing Airport","Hutchinson County Airport","Hutchinson Municipal Airport","Hwange National Park Airport","Hwange Town Airport","Hydaburg Seaplane Base","Hyder Seaplane Base","Hyderabad Airport","Hyvink\u00e4\u00e4 Airfield","H\u00e9v\u00edz\u2013Balaton Airport","H\u00f3lmav\u00edk Airport","H\u00fasav\u00edk Airport","Iamalele Airport","Iaura Airport","Ia\u0219i International Airport","Ibadan Airport","Ibaraki Airport","Iberia Airport","Ibiza Airport","Ibo Airport","Iboki Airport","Icabar\u00fa Airport","Iconi Airport","Icy Bay Airport","Ida Grove Municipal Airport","Idaho County Airport","Idaho Falls Regional Airport","Idiofa Airport","Idre Airport","Iejima Airport","Iffley Airport","Ifuru Airport","Igarka Airport","Igiugig Airport","Igloolik Airport","Ignace Municipal Airport","Ignacio Agramonte International Airport","Ignatyevo Airport","Igor I Sikorsky Memorial Airport","Igrim Airport","Ihosy Airport","Ihu Airport","Iju\u00ed Airport","Ikaria Island National Airport","Ikela Airport","Iki Airport","Il Caravaggio International Airport","Ilam Airport","Ilebo Airport","Ileg Airport","Ilford Airport","Ilh\u00e9us-Bahia-Jorge Amado Airport","Iliamna Airport","Illaga Airport","Illesheim Air Base Airport","Illinois Valley Regional Airport","Ilo Airport","Iloilo International Airport","Ilopango International Airport","Ilorin International Airport","Ilu Airport","Ilulissat Airport","Imam Ali Air Base Airport","Imane Airport","Imbaimadai Airport","Immokalee Regional Airport","Imonda Airport","Imperatriz-Prefeito Renato Moreira Airport","Imperial Beach Naval Outlying Landing Field Airport","Imperial County Airport","Imperial Municipal Airport","Impfondo Airport","Imphal Airport","In Am\u00e9nas Airport","In Guezzam Airport","In Salah Airport","Inagua Airport","Inanwatan Airport","Inca Manco C\u00e1pac International Airport","Incheon International Airport","Indagen Airport","Independence Airport","Independence Municipal Airport","Indian Mountain LRRS Airport","Indiana County\u2013Jimmy Stewart Airport","Indianapolis International Airport","Indigo Bay Lodge Airport","Indira Gandhi International Airport","Indulkana Airport","Industrial Airpark","Ine Airport","Ingalls Field Airport","Ingeniero Aeron\u00e1utico Ambrosio L.V. Taravella International Airport","Ingeniero Jacobacci Airport","Ingham Airport","Ingolstadt Manching Airport","Inhaca Airport","Inhambane Airport","Inhaminga Airport","Inisheer Airport","Inishmaan Aerodrome","Inishmore Aerodrome","Injune Airport","Inkerman Airport","Innamincka Airport","Innisfail Airport","Innsbruck Airport","Inongo Airport","Inta Airport","Inukjuak Airport","Inus Airport","Inuvik Mike Zubko Airport","Invercargill Airport","Inverell Airport","Inverness Airport","Inverway Airport","Inyati Airport","Inyokern Airport","In\u00e1cio Lu\u00eds do Nascimento Airport","Ioannina National Airport","Iokea Airport","Ioma Airport","Iosco County Airport","Iowa City Municipal Airport","Iowa County Airport","Iowa Falls Municipal Airport","Ipia\u00fa Airport","Ipil Airport","Ipiranga Airport","Ipota Airport","Iqaluit Airport","Iquique Diego Aracena International Airport","Iraan Municipal Airport","Iranamadu Seaplane Base Airport","Iranshahr Airport","Irec\u00ea Airport","Ireland West Airport Knock Airport","Iringa Airport","Iriona Airport","Irkutsk International Airport","Iscuande Airport","Isfahan International Airport","Ishurdi Airport","Isisford Airport","Iskandar Airport","Isla Mujeres Airport","Islamabad International Airport","Island Lake Airport","Islay Airport","Isle of Man Airport","Isparta S\u00fcleyman Demirel Airport","Issyk-Kul International Airport","Istanbul Airport","Istanbul Atat\u00fcrk Airport","Itabuna Airport","Itacoatiara Airport","Itaituba Airport","Itaperuna Airport","Itaqui Airport","Ithaca Tompkins Regional Airport","Itokama Airport","Ituber\u00e1 Airport","Itumbiara Airport","Iturup Airport","Ivalo Airport","Ivano-Frankivsk International Airport","Ivanof Bay Seaplane Base","Ivanovo Yuzhny Airport","Ivato International Airport","Ivujivik Airport","Iwakuni Kintaikyo Airport","Iwami Airport","Iwo Jima Airport","Ixtapa Zihuatanejo International Airport","Ixtepec Airport","Izhevsk Airport","Izumo Airport","I\u011fd\u0131r Airport","J Lynn Helms Sevier County Airport","JA Douglas McCurdy Sydney Airport","JAGS McCartney International Airport","Ja\'Aluni Airport","Jabalpur Airport","Jabiru Airport","Jabot Airport","Jacareacanga Airport","Jacinto Lara International Airport","Jack Brooks Regional Airport","Jack Edwards Airport","Jackpot Airport","Jackson County Airport","Jackson Hole Airport","Jackson Municipal Airport","Jacksons International Airport","Jacksonville Executive","Jacksonville International Airport","Jacksonville Municipal Airport","Jacksonville Naval Air Station Airport","Jackson\u2013Medgar Wiley Evers International Airport","Jacmel Airport","Jacobina Airport","Jacqueline Cochran Regional Airport","Jacquinot Bay Airport","JadeWeser Airport","Jaffrey Silver Ranch Airport","Jagdalpur Airport","Jaguaruna Regional Airport","Jahrom Airport","Jaime Gonzalez Airport","Jaipur International Airport","Jaisalmer Airport","Jajao Airport","Jalalabad Airport","Jalaluddin Airport","Jales Airport","Jaluit Airport","Jam Airport","Jamba Airport","Jamestown Regional Airport","Jammu Airport","Jamnagar Airport","Janakpur Airport","Jandakot Airport","Janu\u00e1ria Airport","Japura Airport","Jaqu\u00e9 Airport","Jardines del Rey Airport","Jask Airport","Jasper Airport","Jasper County Airport","Jasper County-Bell Field Airport","Jata\u00ed Airport","Ja\u00e9n Airport","Jebel Ali Seaplane Base Airport","Jefferson City Memorial Airport","Jefferson County International Airport","Jefferson Municipal Airport","Jeh Airport","Jeju International Airport","Jeki Airport","Jember Airport","Jenpeg Airport","Jeongseok Airport","Jeonju Airport","Jequi\u00e9 Airport","Jerez Airport","Jericoacoara Airport","Jerry Tyler Memorial Airport","Jersey Airport","Jerusalem Airport","Jessore Airport","Jeypore Airport","Jharsuguda Veer Surendra Sai Airport","Ji-Paran\u00e1 Airport","Jiagedaqi Airport","Jiamusi Dongjiao Airport","Jiansanjiang Airport","Jiayuguan Airport","Jieyang Chaoshan International Airport","Jijel Ferhat Abbas Airport","Jijiga Airport","Jilin Ertaizi Airport","Jim Hamilton-L.B. Owens Airport","Jim Kelly Field","Jimma Airport","Jinan Yaoqiang International Airport","Jinchang Jinchuan Airport","Jingdezhen Luojia Airport","Jinggangshan Airport","Jinhae Airport","Jining Qufu Airport","Jinja Airport","Jinnah International Airport","Jinzhou Bay Airport","Jipijapa Airport","Jiri Airport","Jiroft Airport","Jiujiang Lushan Airport","Jiuzhai Huanglong Airport","Jiwani Airport","Jixi Xingkaihu Airport","Jizan Regional Airport","Jodhpur Airport","Joensuu Airport","Johan Adolf Pengel International Airport","Johan Pienaar Airport","Johannesburg airport","John A. Osborne Airport","John C. Munro Hamilton International Airport","John F. Kennedy International Airport","John F. Kennedy Memorial Airport","John Glenn Columbus International Airport","John H. Batten Airport","John Murtha Johnstown\u2013Cambria County Airport","John Wayne Airport","Johnson County Airport","Johnson County Executive Airport","Joint Base Andrews Airport","Joinville-Lauro Carneiro de Loyola Airport","Joliet Regional Airport","Jolo Airport","Jomo Kenyatta International Airport","Jomsom Airport","Jonesboro Municipal Airport","Joplin Regional Airport","Jordan Airport","Jorge Ch\u00e1vez International Airport","Jorge Isaacs Airport","Jorge Newbery Airfield","Jorge Wilstermann International Airport","Jorhat Airport","Jos Orno Imsula Airport","Jose De San Martin Airport","Josefa Camejo International Airport","Josephstaal Airport","Joshua Mqabuko Nkomo International Airport","Jos\u00e9 Aponte de la Torre Airport","Jos\u00e9 Celestino Mutis Airport","Jos\u00e9 Joaqu\u00edn de Olmedo International Airport","Jos\u00e9 Leonardo Chirino Airport","Jos\u00e9 Mart\u00ed International Airport","Jos\u00e9 Mar\u00eda Velasco Ibarra Airport","Jo\u00e3o Paulo II Airport","Jo\u00e3o Sim\u00f5es Lopes Neto International Airport","Juan De Ayolas Airport","Juan Gualberto G\u00f3mez Airport","Juan H. White Airport","Juan Jos\u00e9 Rond\u00f3n Airport","Juan Manuel G\u00e1lvez International Airport","Juan Mendoza Airport","Juan Pablo Perez Alfonso Airport","Juan Santamaria International Airport","Juan Simons Vela Airport","Juan Vicente G\u00f3mez International Airport","Juana Azurduy de Padilla International Airport","Juancho E. Yrausquin Airport","Juanda International Airport","Juanju\u00ed Airport","Juazeiro do Norte Airport","Juba International Airport","Juist Airport","Julia Creek Airport","Julius Nyerere International Airport","Jumandy Airport","Jumla Airport","Jundah Airport","Juneau International Airport","Jungwon Air Base Airport","Junin Airport","Jurado Airport","Jurien Bay Airport","Juruena Airport","Juscelino Kubitscheck Airport","Juticalpa Airport","Juwata Airport","Ju\u00edna Airport","Jwaneng Airport","Jyvaskyla Airport","J\u00e9r\u00e9mie Airport","J\u00f6nk\u00f6ping Airport","Kaadedhdhoo Airport","Kabala Airport","Kabalega Falls Airport","Kabalo Airport","Kaben Airport","Kabri Dehar Airport","Kabwum Airport","Kadanwari Airport","Kadapa Airport","Kadena Air Base","Kadhdhoo Airport","Kadugli Airport","Kaduna Airport","Kaghau Airport","Kagi Airport","Kagoshima Airport","Kagua Airport","Kahramanmara\u015f Airport","Kahului Airport","Kaieteur International Airport","Kaikohe Airport","Kaikoura Airport","Kailashahar Airport","Kaili Huangping Airport","Kaimana Airport","Kaintiba Airport","Kairuku Airport","Kaitaia Airport","Kajaani Airport","Kakamega Airport","Kake Airport","Kalabo Airport","Kalaeloa Airport","Kalakaket Creek Air Station","Kalaleh Airport","Kalamata International Airport","Kalamazoo-Battle Creek International Airport","Kalaupapa Airport","Kalaymyo Airport","Kalbarri Airport","Kalemie Airport","Kalgoorlie-Boulder Airport","Kalibo International Airport","Kalimarau Airport","Kalkgurung Airport","Kalmar Airport","Kalokol Airport","Kalpowar Airport","Kalskag Airport","Kaltag Airport","Kaluga Grabtsevo Airport","Kalumburu Airport","Kalymnos Island National Airport","Kamalpur Airport","Kamaran Airport","Kamaran Downs Airport","Kamarang Airport","Kamarata Airport","Kambalda Airport","Kamberatoro Airport","Kambuaya Airport","Kamembe Airport","Kamianets-Podilskiy Airport","Kamileroi Airport","Kamina Airport","Kamiraba Airport","Kamishly Airport","Kamloops Airport","Kamphaeng Saen Airport","Kampong Chhnang Airport","Kampot Airport","Kamulai Airport","Kamur Airport","Kamusi Airport","Kanab Municipal Airport","Kanabea Airport","Kanainj Airport","Kananga Airport","Kanas Airport","Kandahar International Airport","Kandep Airport","Kandi Airport","Kandla Airport","Kandrian Airport","Kaneohe Bay Marine Corps Air Station","Kangding Airport","Kangerlussuaq Airport","Kangiqsualujjuaq","Kangiqsujuaq","Kangirsuk Airport","Kaniama Airport","Kankan Airport","Kankesanturai Airport","Kannur International Airport","Kanpur Airport","Kansai International Airport","Kansas City International Airport","Kantchari Airport","Kao Airport","Kaohsiung International Airport","Kaolack Airport","Kaoma Airport","Kapal Airport","Kapalua Airport","Kapanga Airport","Kapit Airport","Kapiti Coast Airport","Kapuskasing Airport","Kar Airport","Kar Kar Airport","Karamay Airport","Karanambo Airport","Karara Airport","Karasabai Airport","Karasburg Airport","Karato Airport","Karawari Airport","Karel Sadsuitubun Airport","Kariba Airport","Karimui Airport","Karlovy Vary Airport","Karlskoga Airport","Karlsruhe-Baden-Baden Airport","Karlstad Airport","Karluk Airport","Karluk Lake Seaplane Base","Karonga Airport","Karoola Airport","Karpathos Island National Airport","Karratha Airport","Kars Harakani Airport","Karshi Airport","Karubaga Airport","Karumba Airport","Kasaan Seaplane Base","Kasaba Bay Airport","Kasabonika Airport","Kasama Airport","Kasane Airport","Kasanombe Airport","Kasba Lake Airport","Kasenga Airport","Kasese Airport","Kashan Airport","Kashechewan Airport","Kashgar Airport","Kasigluk Airport","Kasiguncu Airport","Kasompe Airport","Kasongo-Lunda Airport","Kasos Island Public Airport","Kassala Airport","Kassel Airport","Kastamonu Airport","Kastellorizo Island Public Airport","Kastoria National Airport","Kasungu Airport","Katanning Airport","Katherine Tindal Airport","Katima Mulilo Airport","Katiola Airport","Katiu Airport","Kato Airport","Katowice International Airport","Katsina Airport","Kattiniq-Donaldson Airport","Katugastota Airport","Katukurunda Airport","Kauehi Airport","Kaufana Airport","Kauhajoki Airport","Kauhava Airport","Kaukura Airport","Kaunas International Airport","Kavadja Airport","Kavala International Airport","Kavalerovo Airport","Kavanay\u00e9n Airport","Kavieng Airport","Kavumu Airport","Kawama Airport","Kawito Airport","Kawthaung Airport","Kaya Airport","Kayenta Airport","Kayes Airport","Kayseri Erkilet Airport","Kazan International Airport","Kazi Nazrul Islam Airport","Ka\u00e9di Airport","Ka\u00e9l\u00e9 Airport","Kearney Regional Airport","Kebar Airport","Keekorok Airport","Keesler Air Force Base","Keetmanshoop Airport","Keewaywin Airport","Kefalonia Cephalonia International Airport","Keflav\u00edk International Airport","Kegaska Airport","Kegelman Air Force Auxiliary Field","Keglsugl Airport","Keibane Airport","Kelafo Airport","Kelaniya River Airport","Kelanoa Airport","Kelila Airport","Kelle Airport","Kelowna International Airport","Kelsey Airport","Keluang Airport","Kemerovo Airport","Kemi-Tornio Airport","Kemmerer Municipal Airport","Kempegowda International Airport","Kempsey Airport","Ken Jones Airport","Kenai Municipal Airport","Kendari Haluoleo Airport","Kenema Airport","Kengtung Airport","Kenieba Airport","Keningau Airport","Kenitra Airport","Kenmore Air Harbor Airport","Kenmore Air Harbor Seaplane Base","Kenneth Kaunda International Airport","Kennett Memorial Airport","Kenora Airport","Kenosha Regional Airport","Kent International Airport","Kentland Municipal Airport","Keokuk Municipal Airport","Keperveyem Airport","Kepi Airport","Kerama Airport","Kerang Airport","Kerau Airport","Kerch Airport","Kerema Airport","Kericho Airport","Kerikeri Airport","Kerinci Airport","Kerio Valley Airport","Kerman Airport","Kerrville Municipal Airport","Kerry Airport","Kertajati International Airport","Kerteh Airport","Keshod Airport","Ketapang Airport","Ketchikan Harbor Seaplane Base","Ketchikan International Airport","Key Lake Airport","Key West International Airport","Key West Naval Air Station","Khabarovsk-Novy Airport","Khajuraho Airport","Khamti Airport","Khaneh Airport","Khanty-Mansiysk Airport","Khark Island Airport","Kharkhorin Airport","Kharkiv International Airport","Khartoum International Airport","Khasab Airport","Khashm el Girba Airport","Khatanga Airport","Khatgal Airport","Kherson International Airport","Khmelnytskyi Airport","Khok Kathiam Airport","Khoka Moya Airport","Khon Kaen Airport","Khong Airport","Khorramabad Airport","Khost Airport","Khovd Airport","Khowai Airport","Khoy Airport","Khrabrovo Airport","Khudzhand Airport","Khujirt Airport","Khuzdar Airport","Khwahan Airport","Khwai River Lodge Airport","Kibuli Airport","Kichwa Tembo Airport","Kickapoo Downtown Airport","Kiel Airport","Kiffa Airport","Kigali International Airport","Kigoma Airport","Kihihi Airport","Kikai Airport","Kikinonda Airport","Kikori Airport","Kikwit Airport","Kilaguni Airport","Kili Airport","Kilimanjaro International Airport","Kilkenny Airport","Killeen-Fort Hood Regional Airport","Kilwa Airport","Kilwa Masoko Airport","Kimam Airport","Kimberley Airport","Kimberley Downs Airport","Kimble County Airport","Kimmirut Airport","Kincardine Municipal Airport","Kindamba Airport","Kindersley Regional Airport","Kindu Airport","King Abdulaziz Air Base","King Abdulaziz International Airport","King County International Airport","King Cove Airport","King Fahd International Airport","King Hussein Air College Airport","King Hussein International Airport","King Island Airport","King Khaled Air Base","King Khaled Military City Airport","King Khalid International Airport","King Mswati III International Airport","King Salmon Airport","King Shaka International Airport","Kingaroy Airport","Kingfisher Lake Airport","Kingman Airport","Kings Canyon Airport","Kings Creek Airport","Kingscote Airport","Kingston Airport","Kingsville Naval Air Station","Kinkungwa Airport","Kinmen Airport","Kinshasa N\'Djili Airport","Kinston Regional Jetport","Kipnuk Airport","Kira Airport","Kirakira Airport","Kirensk Airport","Kirk Field","Kirkenes Airport","Kirkimbie Station Airport","Kirkland Lake Airport","Kirksville Regional Airport","Kirkuk Air Base","Kirkwall Airport","Kirovsk-Apatity Airport","Kirsch Municipal Airport","Kiruna Airport","Kirundo Airport","Kiryat Shmona Airport","Kisengam Airport","Kish International Airport","Kishangarh Airport","Kisimayu Airport","Kissidougou Airport","Kissimmee Gateway Airport","Kisumu International Airport","Kitadaito Airport","Kitakyushu Airport","Kitale Airport","Kitava Airport","Kitee Airport","Kithira Island National Airport","Kitkatla Airport","Kitoi Bay Seaplane Base","Kittil\u00e4 Airport","Kitzingen Airport","Kiunga Airport","Kivalina Airport","Kiwai Island Airport","Kiwayu Airport","Klagenfurt Airport","Klawock Airport","Kleinsee Airport","Klemtu Water Aerodrome","Klerksdorp Airport","Knee Lake Airport","Kneeland Airport","Knox County Regional Airport","Kobe Airport","Kobuk Airport","Kodiak Airport","Kodiak Municipal Airport","Kogalym International Airport","Koggala Airport","Koh Kong Airport","Kohat Airport","Koinambe Airport","Koingnaas Airport","Kokhanok Airport","Kokkola-Pietarsaari Airport","Kokoda Airport","Kokomo Municipal Airport","Kokonao Airport","Kokoro Airport","Kokshetau Airport","Kol Airport","Kolda North Airport","Kolhapur Airport","Koliganek Airport","Koltsovo Airport","Kolwezi Airport","Komaio Airport","Komako Airport","Komatipoort Airport","Komatsu Airport","Komo-Manda Airport","Komodo Airport","Kompiam Airport","Komsomolsk-on-Amur Airport","Kona International Airport","Konarak Airport","Konawaruk Airport","Kondavattavan Tank Airport","Kondinskoye Airport","Kondubol Airport","Konge Airport","Kongiganak Airport","Kongo Boumba Airport","Kongolo Airport","Kontum Airport","Konya Airport","Kon\u00e9 Airport","Kooddoo Airport","Koolatah Airport","Koolburra Airport","Kopiago Airport","Korhogo Airport","Korla Airport","Kornasoren Airport","Koro Island Airport","Koroba Airport","Korolevu Airport","Kos International Airport","Kosciusko-Attala County Airport","Kosipe Airport","Kosrae International Airport","Kostanay Airport","Kostroma Airport","Koszalin-Zegrze Pomorskie Airport","Kota Airport","Kota Kinabalu International Airport","Kotabangun Airport","Kotabaru Airport","Kotakoli Airport","Kotlas Airport","Kotlik Airport","Kotoka International Airport","Koulamoutou Airport","Koumac Airport","Koumala Airport","Koutiala Airport","Kowanyama Airport","Koyuk Alfred Adams Airport","Koyukuk Airport","Kozani National Airport","Ko\u0161ice International Airport","Krabi Airport","Krakor Airport","Krak\u00f3w John Paul II International Airport","Kramatorsk Airport","Kramfors-Sollefte\u00e5 Airport","Krasnodar International Airport","Krasnoselkup Airport","Krasnoyarsk International Airport","Kratie Airport","Krayniy Airport","Kremenchuk Airport","Kribi Airport","Kristiansand Kjevik Airport","Kristianstad Airport","Kristiansund Airport, Kvernberget","Kropyvnytskyi Airport","Kruger Mpumalanga International Airport","Krymska Airport","Kryvyi Rih International Airport","Kuala Lumpur International Airport","Kuala Pembuang Airport","Kualanamu International Airport","Kubin Airport","Kuching International Airport","Kudat Airport","Kufra Airport","Kugaaruk Airport","Kugluktuk Airport","Kuini Lavenia Airport","Kuito Airport","Kukundu Airport","Kulgera Airport","Kulik Lake Airport","Kullu Manali Airport","Kulob Airport","Kulusuk Airport","Kumamoto Airport","Kumasi Airport","Kumejima Airport","Kunduz Airport","Kungim Airport","Kunming Changshui International Airport","Kunovice Airport","Kunua Airport","Kuopio Airport","Kupiano Airport","Kuqa Qiuci Airport","Kuressaare Airport","Kurgan Airport","Kuri Airport","Kuria Airport","Kursk Vostochny Airport","Kurumoch International Airport","Kurundi Airport","Kurupung Airport","Kurwina Airport","Kushiro Airport","Kushok Bakula Rimpochee Airport","Kutaisi International Airport","Kuujjuaq Airport","Kuujjuarapik Airport","Kuusamo Airport","Kuwait International Airport","Kuyol Airport","Kwai Harbour Airport","Kwailabesi Airport","Kwethluk Airport","Kwigillingok Airport","Kyaukpyu Airport","Kyauktu Airport","Kyiv International Airport","Kyzyl Airport","Kyzylorda Airport","K\u00e4rdla Airport","K\u00e9dougou Airport","K\u00f3pasker Airport","K\u00f6then Airport","K\u014dchi Airport","L\'Aquila\u2013Preturo Airport","L\'Esp\u00e9rance Airport","L\'Mekrareg Airport","L\'alpe D\'huez Airport","L.F. Wade International Airport","L.O. Simenstad Municipal Airport","LBJ Ranch Airport","La Araucan\u00eda Airport","La Aurora International Airport","La Baule-Escoublac Airport","La Chinita International Airport","La Chorrera Airport","La Coloma Airport","La Crosse Regional Airport","La Cumbre Airport","La D\u00e9sirade Airport","La Esperanza Airport","La Florida Airport","La Fria Airport","La Gomera Airport","La Grande Rivi\u00e8re Airport","La Grande-3 Airport","La Grande-4 Airport","La Grande-Union County Airport","La Isabela International Airport","La Macarena Airport","La M\u00f4le Airport","La Nubia Airport","La Palma Airport","La Paz International Airport","La Pedrera Airport","La Plata Airport","La Porte Municipal Airport","La Primavera Airport","La Rioja Airport","La Roche-sur-Yon Airport","La Rochelle-\u00cele de R\u00e9 Airport","La Romaine Airport","La Romana International Airport","La Ronge Airport","La Sarre Airport","La Tabati\u00e8re Airport","La Tontouta International Airport","La Tuque Airport","La Uni\u00f3n Airport","La Vanguardia Airport","LaGrange Callaway Airport","LaGuardia Airport","Lab Lab Airport","Labasa Airport","Labo Airport","Labuan Airport","Lac Brochet Airport","Lac La Biche Airport","Lackland Air Force Base","Laconia Municipal Airport","Ladd Army Airfield","Laduani Airport","Lady Elliot Island Airport","Ladysmith Airport","Lae Island Airport","Lae Nadzab Airport","Lafayette Regional Airport","Lages Airport","Lago Agrio Airport","Lague Airport","Laguindingan Airport","Laguna Army Airfield","Laguna de Los Patos International Airport","Lagunillas Airport","Lahad Datu Airport","Lahr Airport","Laiagam Airport","Lajes Airport","Lake Baringo Airport","Lake Brooks Airport","Lake Charles Regional Airport","Lake City Gateway Airport","Lake County Airport","Lake Cumberland Regional Airport","Lake Evella Airport","Lake Gregory Airport","Lake Havasu City Airport","Lake Macquarie Airport","Lake Manyara Airport","Lake Murray Airport","Lake Nash Airport","Lake Placid Airport","Lake Simcoe Regional Airport","Lake Tahoe Airport","Lakeba Island Airport","Lakefield Airport","Lakefront Airport","Lakehurst Maxfield Field Airport","Lakeland Airport","Lakeland Downs Airport","Lakeland Linder International Airport","Lakselv Banak Airport","Lal Bahadur Shastri Airport","Lalibela Airport","Lamar Municipal Airport","Lamassa Airport","Lambarene Airport","Lamen Bay Airport","Lamerd Airport","Lamezia Terme International Airport","Lamidanda Airport","Lampang Airport","Lampedusa Airport","Lampson Field","Lanai Airport","Lancang Jingmai Airport","Lancaster Airport","Land\'s End Airport","Landivisiau Airport","Langebaanweg Airport","Langeoog Airport","Langimar Airport","Langkawi International Airport","Langley Air Force Base","Langley Regional Airport","Langtang Airport","Lankaran International Airport","Lannion Airport","Lansdowne Airport","Lansdowne House Airport","Lanseria International Airport","Lanyu Airport","Lanzarote Airport","Lanzhou Zhongchuan International Airport","Laoag International Airport","Lappeenranta Airport","Laramie Regional Airport","Laredo International Airport","Larestan International Airport","Larissa National Airport","Larnaca International Airport","Larsen Bay Airport","Las Am\u00e9ricas International Airport","Las Breas Airport","Las Brujas Airport","Las Cruces Airport","Las Cruces International Airport","Las Flecheras Airport","Las Flores Airport","Las Gaviotas Airport","Las Heras Airport","Las Higueras Airport","Las Khoreh Airport","Las Malvinas Airport","Las Vegas Airport","Las Vegas Municipal Airport","Lashio Airport","Lastourville Airport","Latrobe Airport","Latrobe Valley Airport","Latur Airport","Laucala Island Airport","Laughlin Air Force Base Airport","Laughlin-Bullhead International Airport","Launceston Airport","Laura Airport","Laurence G Hanscom Field","Laurie River Airport","Laurinburg\u2013Maxton Airport","Lauriston Airport","Lauro Kurtz Airport","Laval-Entrammes Airport","Lavan Island Airport","Laverton Airport","Lawas Airport","Lawn Hill Airport","Lawrence County Airpark","Lawrence J. Timmerman Airport","Lawrence Municipal Airport","Lawrenceville Brunswick Municipal Airport","Lawrenceville-Vincennes International Airport","Lawson Army Airfield","Lawton-Fort Sill Regional Airport","Layang-Layang Airport","La\u00ef Airport","Le Castellet Airport","Le Havre Octeville Airport","Le Mans-Arnage Airport","Le Mars Municipal Airport","Le Puy-Loudes Airport","Le Touquet \u2013 C\u00f4te d\'Opale Airport","Lea County Regional Airport","Leaf Rapids Airport","Learmonth Airport","Lebakeng Airport","Lebanon Municipal Airport","Lebel-sur-Quevillon Airport","Lee Airport","Lee C Fine Memorial Airport","Lee Gilmer Memorial Airport","Leeds Bradford Airport","Leesburg International Airport","Leeuwarden Air Base","Leeward Point Field","Legazpi Airport","Lehigh Valley International Airport","Lehu Airport","Leigh Creek Airport","Leinster Airport","Leipzig-Halle Airport","Leirin Airport","Leismer Airport","Leite Lopes Airport","Leitre Airport","Lekana Airport","Lekhwair Airport","Leknes Airport","Lekoni Airport","Lelystad Airport","Lemhi County Airport","Lemmon Municipal Airport","Lemnos International Airport","Lemoore Naval Air Station","Lemwerder Airport","Lenawee County Airport","Lengbati Airport","Lengpui Airport","Lensk Airport","Len\u00e7\u00f3is Airport","Leo Airport","Leonard M. Thompson International Airport","Leonardo da Vinci-Fiumicino Airport","Leonora Airport","Leopoldina Airport","Lereh Airport","Leribe Airport","Leron Plains Airport","Leros Municipal Airport","Lerwick Tingwall Airport","Les Cayes Airport","Les Sables-d\'Olonne Talmont Airport","Lese Airport","Leshukonskoye Airport","Lesobeng Airport","Letfotar Airport","Lethbridge Airport","Lethem Airport","Letterkenny Airport","Letung Airport","Levelock Airport","Levuka Airfield","Lewis University Airport","Lewiston-Nez Perce County Airport","Lewistown Municipal Airport","Lewoleba Airport","Le\u00f3n Airport","Lhasa Gonggar Airport","Lhok Sukon Airport","Liangping Airport","Lianhuashan Port Airport","Lianshulu Airport","Lianyungang Baitabu Airport","Libenge Airport","Liberal Mid-America Regional Airport","Libo Airport","Liboi Airport","Libreville Airport","Licenciado Gustavo D\u00edaz Ordaz International Airport","Lichinga Airport","Lidk\u00f6ping-Hovby Airport","Lien Khuong Airport","Liep\u0101ja International Airport","Lieutenant Colonel Rafael Pab\u00f3n Airport","Lifuka Island Airport","Lightning Ridge Airport","Lihir Island Airport","Lihue Airport","Lijiang Sanyi Airport","Likiep Airport","Likoma Island Airport","Lilabari Airport","Lille Airport","Lilongwe International Airport","Lima Allen County Airport","Limbang Airport","Limbunya Airport","Lime Acres Finsch Mine Airport","Lime Village Airport","Limoges \u2013 Bellegarde Airport","Limon Airport","Limon Municipal Airport","Lim\u00f3n International Airport","Linate Airport","Lincang Airport","Lincoln Airport","Linda Downs Airport","Lindeman Island Airport","Linden Airport","Lindi Airport","Linfen Qiaoli Airport","Linga Linga Airport","Link\u00f6ping City Airport","Lins Airport","Linton-on-Ouse Airport","Linyi Shubuling International Airport","Linz Airport","Lipetsk Airport","Liping Airport","Lisala Airport","Lisbon Cascais-Tejo Regional Airport","Lisbon Portela Airport","Lismore Airport","Lissadell Airport","Little Grand Rapids Airport","Little Rock Air Force Base","Liupanshui Yuezhao Airport","Liuzhou Bailian Airport","Livengood Camp Airport","Livermore Municipal Airport","Liverpool John Lennon Airport","Lizard Island Airport","Li\u00e8ge Airport","Ljubljana Jo\u017ee Pu\u010dnik Airport","Lleida-Alguaire Airport","Lloydminster Airport","Loakan Airport","Loani Airport","Lobatse Airport","Lobito Airport","Lock Airport","Lockhart River Airport","Lodar Airport","Lodja Airport","Lodwar Airport","Loei Airport","Loen Airport","Logan-Cache Airport","Logro\u00f1o-Agoncillo Airport","Loikaw Airport","Lokichogio Airport","Lokpriya Gopinath Bordoloi International Airport","Lolland Falster Airport","Lombok International Airport","Lompoc Airport","Lom\u00e9-Tokoin Airport","Loncopue Airport","Londolozi Airport","London Biggin Hill Airport","London City Airport","London International Airport","London Luton Airport","London Southend Airport","London Stansted Airport","London-Corbin Airport","Londrina Airport","Lonesome Pine Airport","Long Akah Airport","Long Apung Airport","Long Banga Airport","Long Bawan Airport","Long Beach Airport","Long Island Airport","Long Island MacArthur Airport","Long Lama Airport","Long Lellang Airport","Long Pasia Airport","Long Semado Airport","Long Seridan Airport","Long Sukang Airport","Long Xuy\u00ean Airport","Longana Airport","Longnan Chengzhou Airport","Longreach Airport","Longyan Guanzhishan Airport","Lonorore Airport","Lopez Island Airport","Lopez de Micay Airport","Loralai Airport","Lord Howe Island Airport","Lordsburg Municipal Airport","Loreto International Airport","Lorica Airport","Lorient South Brittany Airport","Loring Airport","Loring International Airport","Lorraine Airport","Los Alamos Airport","Los Angeles International Airport","Los Banos Municipal Airport","Los Cabos International Airport","Los Chiles Airport","Los Colonizadores Airport","Los Garzones Airport","Los Menucos Airport","Los Perales Airport","Los Roques Airport","Los Tablones Airport","Losuia Airport","Lotus Vale Station Airport","Louis Armstrong New Orleans International Airport","Louis Trichardt Airport","Louisa County Airport","Louisville International Airport","Louisville Winston County Airport","Lourdes-de-Blanc-Sablon Airport","Lowai Airport","Lowcountry Regional Airport","Loyengalani Airport","Lo\u0161inj Airport","Lt Col W.G. Billy Barker Airport","Lt. Warren Eaton Airport","Luabo Airport","Luang Namtha Airport","Luang Prabang International Airport","Luau Airport","Lubang Airport","Lubango Airport","Lubbock Preston Smith International Airport","Lublin Airport","Lubumbashi International Airport","Lucapa Airport","Lucas do Rio Verde Airport","Lucca-Tassignano Airport","Luderitz Airport","Ludhiana Airport","Luena Airport","Lugano Airport","Lugh Ganane Airport","Luhansk International Airport","Luis Mu\u00f1oz Mar\u00edn International Airport","Luiza Airport","Luke Air Force Base","Lukulu Airport","Lule\u00e5 Airport","Lumbala Airport","Lumberton Municipal Airport","Lumbo Airport","Lumi Airport","Lumid Pau Airport","Lungi International Airport","Lunyuk Airport","Luoyang Beijiao Airport","Luozi Airport","Lusambo Airport","Lusanga Airport","Lusk Municipal Airport","Lutselk\'e Airport","Lutsk Airport","Luwuk Bubung Airport","Luxembourg Airport","Luxor International Airport","Luzamba Airport","Luzhou Yunlong Airport","Lu\u010denec Airport","Lviv Danylo Halytskyi International Airport","Lwbak Airport","Lyall Harbour Seaplane Base","Lycksele Airport","Lydd Airport","Lynchburg Regional Airport","Lynden Pindling International Airport","Lyndhurst Airport","Lynn Lake Airport","Lyon-Bron Airport","Lyon-Saint-Exup\u00e9ry Airport","Lyons-Rice County Municipal Airport","Lyudao Airport","L\u00e1brea Airport","L\u00e1zaro C\u00e1rdenas Airport","L\u00e6s\u00f8 Airport","L\u00e9opold S\u00e9dar Senghor International Airport","L\u00fcbeck Blankensee Airport","L\u00fcliang Airport","M\'Boki Airport","M\'Vengue El Hadj Omar Bongo Ondimba International Airport","M. Graham Clark Downtown Airport","MBS International Airport","Maamigili Airport","Maan Airport","Maastricht Aachen Airport","Mabaruma Airport","Mabuiag Island Airport","Mac Dill Air Force Base Airport","Macagua Airport","Macanal Airport","Macapa International Airport","Macas Airport","Macau International Airport","Maca\u00e9 Airport","Macdonald Downs Airport","Macei\u00f3 Zumbi dos Palmares International Airport","Macenta Airport","Mackall Army Airfield","Mackay Airport","Mackinac Island Airport","Macksville Airport","Macmillan Pass Airport","Macomb Municipal Airport","Macon Downtown Airport","Mactan-Cebu International Airport","Madang Airport","Madeira Airport","Madera Municipal Airport","Madirovalo Airport","Madison Municipal Airport","Madras Municipal Airport","Madrid\u2013Torrej\u00f3n Airport","Madurai Airport","Mae Hong Son Airport","Mae Sot Airport","Maewo-Naone Airport","Mafeteng Airport","Mafia Airport","Magan Airport","Maganja da Costa Airport","Magaruque Airport","Magas Airport","Magdagachi Airport","Magdalena Airport","Magdeburg\u2013Cochstedt Airport","Magic Valley Regional Airport","Magnitogorsk International Airport","Magnolia Municipal Airport","Magway Airport","Mahana Airport","Mahanoro Airport","Maharana Pratap Airport","Mahdia Airport","Mahendranagar Airport","Mahenye Airport","Mahshahr Airport","Maiana Airport","Maiduguri International Airport","Maikwak Airport","Maimana Airport","Maimun Saleh Airport","Mainoru Airport","Maintirano Airport","Maio Airport","Maitland Airport","Majkin Airport","Major Brigadeiro Trompowsky Airport","Majors Airport","Makabana Airport","Makemo Airport","Makin Island Airport","Makini Airport","Makkovik Airport","Makokou Airport","Makoua Airport","Maku International Airport","Makung Airport","Makurdi Airport","Mal Island Airport","Mala Mala Airport","Malabang Airport","Malabo International Airport","Malacca Airport","Malad City Airport","Malaimbandy Airport","Malakal Airport","Malalaua Airport","Malam Airport","Malanje Airport","Malatya Erha\u00e7 Airport","Malcolm McKinnon Airport","Malda Airport","Malden Regional Airport","Malekolon Airport","Malekoula Airport","Malelane Airport","Malevo Airport","Maliana Airport","Malikus Saleh Airport","Malindi Airport","Mallacoota Airport","Mallam Aminu Kano International Airport","Malm\u00f6 Airport","Maloelap Airport","Malolo Lailai Island Airport","Malone-Dufort Airport","Malta Airport","Malta International Airport","Mamai Airport","Mamburao Airport","Mamfe Airport","Mamitupo Airport","Mammoth Yosemite Airport","Mampikony Airport","Man Airport","Mana Island Airport","Manakara Airport","Mananara Nord Airport","Manang Airport","Mananjary Airport","Manapouri Airport","Manari Airport","Manassas Regional Airport","Manatee Airport","Manaung Airport","Manchester Airport","Manchester-Boston Regional Airport","Manda Airport","Mandabe Airport","Mandalay Chanmyathazi Airport","Mandalay International Airport","Mandalgovi Airport","Mandera Airport","Mandinga Airport","Mandora Airport","Mandritsara Airport","Manega Airport","Manetai Airport","Manga Airport","Mangaia Island Airport","Mangalore International Airport","Mangla Airport","Mangochi Airport","Manguna Airport","Manhattan Regional Airport","Manicor\u00e9 Airport","Manihi Airport","Manihiki Island Airport","Maniitsoq Airport","Manila Municipal Airport","Maningrida Airport","Manistee County Blacker Airport","Manitoulin East Municipal Airport","Manitouwadge Airport","Manitowoc County Airport","Maniwaki Airport","Manja Airport","Manjimup Airport","Mankato Regional Airport","Manley Hot Springs Airport","Manners Creek Airport","Mannheim-City Airport","Mano Dayak International Airport","Manokotak Airport","Manono Airport","Mansa Airport","Mansfield Lahm Regional Airport","Mansons Landing Seaplane Base Airport","Manti-Ephraim Airport","Manuel Crescencio Rej\u00f3n International Airport","Manumu Airport","Manzhouli Xijiao Airport","Mao Airport","Maota Airport","Maple Bay Airport","Mapoda Airport","Mapua Airport","Maputo International Airport","Maquehue Airport","Maquinchao Airport","Mar Del Plata Airport","Mara North Airport","Mara Serena Airport","Maraba Airport","Maradi Airport","Marakei Airport","Maramag Airport","Maramuni Airport","Marana Regional Airport","Maranggo Airport","Marathon Airport","Marau Airport","Marawaka Airport","Marble Bar Airport","Marble Canyon Airport","March Air Reserve Base Airport","Marco Island Airport","Mardin Airport","Marechal Cunha Machado International Airport","Marechal Rondon International Airport","Mareeba Airport","Marfa Municipal Airport","Margaret Ekpo International Airport","Margaret River Airport","Margaret River Airport","Margarima Airport","Margate Airport","Maria Cristina Airport","Maria Montez International Airport","Maria Reiche Neuman Airport","Mariana Grajales Airport","Marib Airport","Maribor Edvard Rusjan Airport","Marie-Galante Airport","Mariehamn Airport","Marilia Airport","Marin County Airport","Marina Municipal Airport","Marina di Campo Airport","Marinda Airport","Marinduque Airport","Maringa Regional Airport","Marion County Airport","Marion County Regional Airport","Marion County \u2013 Rankin Fite Airport","Marion Downs Airport","Marion Municipal Airport","Maripasoula Airport","Mariposa-Yosemite Airport","Mariquita Airport","Mariscal Lamar International Airport","Mariscal Sucre Airport","Mariscal Sucre International Airport","Mariupol International Airport","Mari\u00e1nsk\u00e9 L\u00e1zn\u011b Airport","Markovo Airport","Marla Airport","Marlboro County Jetport","Marlborough Airport","Marmul Airport","Maroantsetra Airport","Maron Island Airport","Marqua Airport","Marrakesh Menara Airport","Marree Airport","Marromeu Airport","Marsa Alam International Airport","Marsa Brega Airport","Marsabit Airport","Marseille Provence Airport","Marshall Army Airfield","Marshall Don Hunter Sr. Airport","Marshall Islands International Airport","Marshall Memorial Municipal Airport","Marshalltown Municipal Airport","Marshfield Municipal Airport","Martha\'s Vineyard Airport","Martin State Airport","Martinique Aim\u00e9 C\u00e9saire International Airport","Martubah Airport","Mart\u00edn Miguel de G\u00fcemes International Airport","Marudi Airport","Mary Airport","Mary River Aerodrome","Mary\'s Harbour Airport","Maryborough Airport","Mar\u00e9 Airport","Mar\u00eda Dolores Airport","Masa Airport","Masalembo Airport","Masasi Airport","Mashhad International Airport","Masi-Manimba Airport","Masindi Airport","Masirah Air Base","Mason City Municipal Airport","Mason County Airport","Massawa International Airport","Massena International Airport","Masset Airport","Masterton Airport","Masvingo Airport","Mata\'aho Airport","Matagami Airport","Matagorda Island Air Force Base Airport","Matahora Airport","Mataiva Airport","Matamata Airport","Matamoros International Airport","Matane Airport","Matari Airport","Mataveri International Airport","Mateca\u00f1a International Airport","Matei Airport","Mati Airport","Mato Grosso Airport","Matsaile Airport","Matsapha Airport","Matsu Beigan Airport","Matsu Nangan Airport","Matsumoto Airport","Matsuyama Airport","Mattala Rajapaksa International Airport","Matthew Spain Airport","Matthews Ridge Airport","Matup\u00e1 Airport","Maturin International Airport","Mauke Airport","Maun Airport","Maupiti Airport","Maurice Bishop International Airport","Maury County Airport","Mau\u00e9s Airport","Maverick County Memorial International Airport","Mawella Lagoon Airport","Mawlamyine Airport","Maximo Gomez Airport","Maxson Airfield","Maxwell Air Force Base","May Creek Airport","May River Airport","Maya-Maya Airport","Mayaguana Airport","Mayfa\'ah Airport","Maygatka Airport","Mayo Airport","Mayor Buenaventura Vivas Airport","Mayor General FAP Armando Revoredo Iglesias Airport","Mayumba Airport","Mazar-e Sharif International Airport","Mazatl\u00e1n International Airport","Mbala Airport","Mbambanakira Airport","Mbandaka Airport","Mbanza Congo Airport","Mbarara Airport","Mbeya Airport","Mbigou Airport","Mbout Airport","Mbuji Mayi Airport","Mc Allen Miller International Airport","Mc Clellan Airfield","McAlester Regional Airport","McArthur River Mine Airport","McCall Municipal Airport","McCarran International Airport","McCarthy Airport","McChord Field Airport","McClellan-Palomar Airport","McComb\u2013Pike County Airport","McConnell Air Force Base Airport","McCook Ben Nelson Regional Airport","McEntire Joint National Guard Base Airport","McGhee Tyson Airport","McGrath Airport","McGuire Air Force Base Airport","McKellar-Sipes Regional Airport","McKinley National Park Airport","McMinn County Airport","McNary Field","McPherson Airport","Meadow Lake Airport","Meadows Field","Mecheria Airport","Medellin Airport","Medfra Airport","Medicine Hat Airport","Medina Airport","Meekatharra Airport","Mefford Field Airport","Meghauli Airport","Meg\u00e8ve Airport","Mehamn Airport","Mehrabad International Airport","Meixian Airport","Mejit Atoll Airport","Mekambo Airport","Mekane Selam Airport","Mekoryuk Airport","Melangguane Airport","Melbourne Airport","Melbourne Essendon Airport","Melfi Airport","Melilla Airport","Melinda Airport","Memanbetsu Airport","Memmingen Airport","Memorial Field Airport","Memphis International Airport","Mena Airport","Mena Intermountain Municipal Airport","Mende Brenoux Airport","Mendi Airport","Menominee\u2013Marinette Twin County Airport","Menongue Airport","Menorca Airport","Menyamya Airport","Merced Regional Airport","Mercedes Airport","Mercedita Airport","Mercer County Airport","Merdey Airport","Meribel Airport","Meridian Regional Airport","Merimbula Airport","Merle K. (Mudhole) Smith Airport","Merluna Airport","Merowe Airport","Merrill Field Airport","Merrill Municipal Airport","Merritt Airport","Merritt Island Airport","Mersa Matruh Airport","Mersing Airport","Merty Merty Airport","Merville \u2013 Calonne Airport","Mesa Del Rey Airport","Mesa Falcon Field Airport","Meselia Airport","Mesquite Airport","Messina Airport","Metlakatla Seaplane Base Airport","Metro Field Airport","Metropolitan Airport","Mettel Field","Metz-Frescaty Air Base Airport","Metz-Nancy-Lorraine Airport","Mevang Airport","Mexicali International Airport","Meyers Chuck Seaplane Base Airport","Mfuwe Airport","Miami Executive Airport","Miami International Airport","Miami Municipal Airport","Miami Seaplane Base","Miami University Airport","Miami-Opa Locka Executive Airport","Miandrivazo Airport","Mianwali Airport","Mianyang Nanjiao Airport","Michael Army Airfield","Michel-Pouliot Gasp\u00e9 Airport","Michigan City Municipal Airport","Mid Delta Regional Airport","Mid State Regional Airport","Mid-Carolina Regional Airport","Mid-Ohio Valley Regional Airport","MidAmerica St. Louis Airport","MidCoast Regional Airport","Middle Caicos Airport","Middle Georgia Regional Airport","Middlemount Airport","Middleton Island Airport","Middletown Regional Airport","Midgard Airport","Midland Airpark","Midland International Airport","Midtjyllands Airport","Miele Mimbale Airport","Mifflin County Airport","Migalovo Air Base","Miguel Hidalgo y Costilla Guadalajara International Airport","Mihail Kog\u0103lniceanu International Airport","Miho Yonago Airport","Mikkeli Airport","Milan\u2013Malpensa Airport","Milas-Bodrum Airport","Mildura Airport","Miles Airport","Miles City Airport","Miley Memorial Field","Milford Municipal Airport","Milford Sound Airport","Mili Island Airport","Milingimbi Airport","Millard Airport","Miller Field","Millicent Airport","Millington-Memphis Airport","Millinocket Municipal Airport","Millville Executive Airport","Milos Island National Airport","Min\'hat Hashnayim Airport","Minami Torishima Airport","Minami-Daito Airport","Minangkabau International Airport","Minatitl\u00e1n-Coatzacoalcos International Airport","Mina\u00e7u Airport","Minchumina Airport","Minden-Tahoe Airport","Mindik Airport","Mindiptana Airport","Mine Site Airport","Mineral Wells Airport","Mineralnye Vody Airport","Miners Lake Airport","Mingan Airport","Minhas Airbase","Ministro Pistarini International Airport","Minj Airport","Minlaton Airport","Minna Airport","Minneapolis-Saint Paul International Airport","Minnipa Airport","Minot Air Force Base","Minot International Airport","Minsk National Airport","Minsk-1 Airport","Minto Al Wright Airport","Minute Man Airfield","Minvoul Airport","Miquelon Airport","Miracema do Tocantins Airport","Miraflores Airport","Miramar Airport","Miramar Marine Corps Air Station - Mitscher Field","Miramichi Airport","Miranda Downs Airport","Miri Airport","Mirit\u00ed-Paran\u00e1 Airport","Mirny Airport","Misawa Airport","Misima Island Airport","Miskolc Airport","Misrata International Airport","Mission Field","Missoula International Airport","Mitchell Airport","Mitchell Municipal Airport","Mitchell Plateau Airport","Mitiaro Island Airport","Mitiga International Airport","Mittiebah Airport","Mitzic Airport","Mitzpe Ramon Airport","Miyakejima Airport","Miyako Airport","Miyanmin Airport","Miyazaki Airport","Mizan Teferi Airport","Mkambati Airport","Mkuze Airport","Mmabatho International Airport","Mo i Rana Airport, R\u00f8ssvoll","Moabi Airport","Moala Airport","Moanamani Airport","Moanda Airport","Moba Airport","Mobile Downtown Airport","Mobile Regional Airport","Mobridge Municipal Airport","Mocopulli Airport","Moc\u00edmboa da Praia Airport","Modesto City\u2013County Airport","Moengo Airstrip","Moenjodaro Airport","Moffett Federal Airfield","Mogilev Airport","Mohamed Boudiaf International Airport","Mohammed V International Airport","Mohe Gulian Airport","Moh\u00e9li Bandar Es Eslam Airport","Moi International Airport","Moises R. Espinosa Airport","Mois\u00e9s Benzaqu\u00e9n Rengifo Airport","Mojave Airport","Mokhotlong Airport","Moki Airport","Mokuti Lodge Airport","Molde Airport","Molokai Airport","Moma Airport","Momeik Airport","Momote Airport","Mompos Airport","Monastir Habib Bourguiba International Airport","Monbetsu Airport","Monclova International Airport","Mondell Field","Mondulkiri Airport","Monfort Airport","Mong Hsat Airport","Mong Tong Airport","Mongo Airport","Mongu Airport","Monkey Bay Airport","Monkey Mountain Airport","Monkira Airport","Monmouth Executive Airport","Mono Airport","Monroe County Airport","Monroe Regional Airport","Monse\u00f1or \u00d3scar Arnulfo Romero International Airport","Mont Tremblant International Airport","Mont-Dauphin - Saint-Cr\u00e9pin Airport","Mont-Joli Airport","Montagne Harbor Airport","Montague Airport","Montauk Airport","Monte Alegre Airport","Monte Caseros Airport","Monte Dourado Airport","Monteagudo Airport","Montelibano Airport","Montepuez Airport","Monterey Regional Airport","Monterrey Airport","Monterrey International Airport","Montes Claros Airport","Montevideo-Chippewa County Airport","Montgomery County Airpark","Montgomery Regional Airport","Montgomery-Gibbs Executive Airport","Monticello Airport","Monticello Regional Airport","Montlu\u00e7on \u2013 Gu\u00e9ret Airport","Monto Airport","Montpellier-M\u00e9diterran\u00e9e Airport","Montrose Regional Airport","Montr\u00e9al - Saint-Hubert Airport","Montr\u00e9al\u2013Mirabel International Airport","Montr\u00e9al\u2013Trudeau International Airport","Monument Valley Airport","Monywa Airport","Moody Air Force Base","Moolawatana Airport","Moomba Airport","Moorabbin Airport","Mooraberree Airport","Moore County Airport","Moore-Murrell Airport","Moorea Airport","Moose Jaw Airport","Moose Lake Airport","Moosonee Airport","Mopah Airport","Mopti Airport","Morafenobe Airport","Moramba Airport","Moranbah Airport","Morawa Airport","Mora\u2013Siljan Airport","Moree Airport","Morehead Airport","Morelia International Airport","Moreton Airport","Morgantown Municipal Airport","Morichal Airport","Morlaix Ploujean Airport","Morney Airport","Mornington Island Airport","Moro Airport","Moroak Airport","Morobe Airport","Morombe Airport","Moron Air Base","Morondava Airport","Morowali Airport","Morris Municipal Airport","Morristown Municipal Airport","Morrisville\u2013Stowe State Airport","Moruya Airport","Moser Bay Seaplane Base","Moses Point Airport","Moshoeshoe I International Airport","Mosj\u00f8en Airport, Kj\u00e6rstad","Mosquera Airport","Moss Airport, Rygge","Mossel Bay Airport","Mossendjo Airport","Mostaganem Airport","Mostar International Airport","Mosteiros Airport","Most\u00e9pha Ben Boulaid Airport","Mosul International Airport","Mota Airport","Mota Lava Airport","Motswari Airport","Motueka Airport","Moucha Airport","Mougulu Airport","Mouilla Ville Airport","Moulay Ali Cherif Airport","Mould Bay Airport","Moultrie Municipal Airport","Moundou Airport","Mount Aue Airport","Mount Barnett Airport","Mount Buffalo Airport","Mount Cavenagh Airport","Mount Cook Airport","Mount Etjo Airport","Mount Full Stop Airport","Mount Gambier Airport","Mount Gordon Airport","Mount Gunson Airport","Mount Hagen Airport","Mount Hotham Airport","Mount House Airport","Mount Isa Airport","Mount Keith Airport","Mount Magnet Airport","Mount Pleasant Airport","Mount Pleasant Municipal Airport","Mount Pleasant Regional Airport","Mount Sandford Station Airport","Mount Shasta Airport","Mount Swan Airport","Mount Valley Airport","Mount Vernon Airport","Mount Washington Regional Airport","Mountain Home Air Force Base","Mountain Village Airport","Mouyondzi Airport","Moyale Airport","Moyo Airport","Moyobamba Airport","Mpanda Airport","Mthatha Airport","Mtwara Airport","Muan International Airport","Muanda Airport","Muara Bungo Airport","Muccan Station Airport","Mucuri Airport","Mudanjiang Hailang International Airport","Mudgee Airport","Mueda Airport","Mueo Airport","Mui Airport","Muir Army Airfield","Mukah Airport","Mukalla International Airport","Mukeiras Airport","Mukhaizna Airport","Muko-Muko Airport","Mulatos Airport","Mulatupo Airport","Muleg\u00e9 Airstrip","Mulga Park Airport","Mulia Airport","Mulika Lodge Airport","Mulka Airport","Mull Airport","Mullewa Airport","Multan International Airport","Mulu Airport","Munbil Airport","Munda Airport","Mundo Maya International Airport","Munduku Airport","Mungeranie Airport","Munich Airport","Municipal Jos\u00e9 Figueiredo Airport","Murmansk Airport","Murray Field","Murray Island Airport","Murray-Calloway County Airport","Murrin Murrin Airport","Murtala Muhammed International Airport","Mururoa Atoll Airport","Muscat International Airport","Muscatine Municipal Airport","Musgrave Airport","Mushaf Air Base Airport","Muskegon County Airport","Muskogee-Davis Regional Airport","Muskoka Airport","Muskrat Dam Airport","Musoma Airport","Mussau Island Airport","Mustique Airport","Mutare Airport","Muting Airport","Muttaburra Airport","Muzaffarabad Airport","Muzaffarnagar Airport","Muzaffarpur Airport","Mu\u015f Airport","Mvuu Camp Airport","Mwadui Airport","Mwanza Airport","Mweka Airport","Myeik Airport","Myitkyina Airport","Mykolaiv International Airport","Mykonos International Airport","Myrhorod Airport","Myrtle Beach International Airport","Mys-Kamenny Airport","Mysore Airport","Mytilene International Airport","Mzuzu Airport","M\u00e1laga Airport","M\u00e9douneu Airport","M\u00e9ndez Airport","M\u00f6nchengladbach Airport","M\u00f6r\u00f6n Airport","M\u00fcnster Osnabr\u00fcck International Airport","N\'Djamena International Airport","N\'Dolo Airport","N\'D\u00e9l\u00e9 Airport","N\'dalatando Airport","N\'zeto Airport","NAS Fort Worth JRB-Carswell Field","NASA Crows Landing Airport","Na-San Airport","Nabire Airport","Nacala Airport","Nachingwea Airport","Nadi International Airport","Nador International Airport","Nadunumu Airport","Nadym Airport","Nafoora Airport","Naga Airport","Nagasaki Airport","Nagoya Airport","Naha Airport","Nain Airport","Najran Regional Airport","Nakashibetsu Airport","Nakhchivan International Airport","Nakhon Phanom Airport","Nakhon Ratchasima Airport","Nakhon Si Thammarat Airport","Nakina Airport","Naknek Airport","Nakuru Airport","Nal Airport","Nalchik Airport","Namangan Airport","Namatanai Airport","Nambaiyufa Airport","Nambucca Heads Airport","Namdrik Airport","Namibe Airport","Namlea Airport","Nampula Airport","Namrole Airport","Namsang Airport","Namsos Airport","Namtu Airport","Namu Airport","Namudi Airport","Namutoni Airport","Nan Nakhon Airport","Nana Airport","Nanaimo Airport","Nanaimo Harbour Water Airport","Nanchang Changbei International Airport","Nanchong Gaoping Airport","Nancy-Essey Airport","Nanded Airport","Nanga Pinoh Airport","Nangade Airport","Nanjing Lukou International Airport","Nanki-Shirahama Airport","Nankina Airport","Nanning Wuxu International Airport","Nanping Wuyishan Airport","Nantes Atlantique Airport","Nantong Xingdong Airport","Nantucket Memorial Airport","Nanuque Airport","Nanwalek Airport","Nanyang Jiangying Airport","Nanyuki Airport","Naoro Airport","Napa County Airport","Napakiak Airport","Napaskiak Airport","Naples International Airport","Naples Municipal Airport","Nappa Merrie Airport","Napperby Airport","Napuka Airport","Naracoorte Airport","Narathiwat Airport","Nargan\u00e1 Airport","Narita International Airport","Narrabri Airport","Narrandera Airport","Narrogin Airport","Narsarsuaq Airport","Nartron Field","Narvik Framnes Airport","Naryan-Mar Airport","Nashik International Airport","Nashua Airport","Nashville International Airport","Natadola Airport","Natal International Airport","Natashquan Airport","Natchez\u2013Adams County Airport","Natitingou Airport","Natuashish Airport","Naujaat Airport","Naukati Bay Seaplane Base","Nauru International Airport","Nausori International Airport","Naval Air Station Corpus Christi Airport","Naval Air Station Sigonella Airport","Naval Station Mayport","Naval Station Norfolk Airport","Navegantes Airport","Navoi International Airport","Nawabshah Airport","Naxos Island National Airport","Naypyidaw Airport","Ndende Airport","Ndjol\u00e9 Ville Airport","Nea Anchialos National Airport","Necochea Airport","Necocli Airport","Needles Airport","Neftekamsk Airport","Nefteyugansk Airport","Negage Airport","Negarbo Airport","Negele Airport","Negril Airport","Neil Armstrong Airport","Nejjo Airport","Nekemte Airport","Nellis Air Force Base","Nelson Airport","Nelson Lagoon Airport","Nelson Mandela International Airport","Nelspruit Airport","Nemiscau Airport","Nenana Municipal Airport","Nenjiang Melgen Airport","Neom Bay Airport","Neosho Hugh Robinson Airport","Nepalgunj Airport","Nephi Municipal Airport","Nerlerit Inaat Airport","Nero-Mer Airport","Nervino Airport","Netaji Subhas Chandra Bose International Airport","Neubrandenburg Airport","Neum\u00fcnster Airport","Nevada Municipal Airport","Nevatim Airbase","Nevers-Fourchambault Airport","Nev\u015fehir Kapadokya Airport","New Bedford Regional Airport","New Bight Airport","New Castle Airport","New Century Aircenter Airport","New Chitose Airport","New Coalinga Municipal Airport","New Halfa Airport","New Ishigaki Airport","New Laura Airport","New Moon Airport","New Orleans NAS JRB-Alvin Callender Field","New Plymouth Airport","New Richmond Regional Airport","New River Valley Airport","New Stuyahok Airport","New Tanegashima Airport","New Ulaanbaatar International Airport","New Ulm Municipal Airport","New York Skyports Seaplane Base Airport","New York Stewart International Airport","Newark Liberty International Airport","Newcastle Airport","Newman Airport","Newport Municipal Airport","Newport News-Williamsburg International Airport","Newport State Airport","Newry Airport","Newtok Airport","Newton City-County Airport","Newton Municipal Airport","Neyveli Airport","Ngala Airport","Ngaound\u00e9r\u00e9 Airport","Ngari Gunsa Airport","Ngloram Airport","Ngoma Airport","Ngukurr Airport","Ngurah Rai International Airport","Nha Trang Air Base","Nhon Co Airport","Niagara District Airport","Niagara Falls International Airport","Niamtougou International Airport","Niau Airport","Nice-C\u00f4te d\'Azur Airport","Nicholson Airport","Nicosia International Airport","Nicoya Guanacaste Airport","Nieuw Nickerie Airport","Nifty Airport","Nightmute Airport","Niigata Airport","Nikolai Airport","Nikolayevsk-on-Amur Airport","Nikolski Air Station","Nikunau Airport","Nimba Airport","Ningbo Lishe International Airport","Ningerum Airport","Ninglang Luguhu Airport","Ninilchik Airport","Ninoy Aquino International Airport","Nioki Airport","Niokolo-Koba Airport","Nioro du Sahel Airport","Niort-Souch\u00e9 Airport","Nipa Airport","Niquel\u00e2ndia Airport","Nissan Island Airport","Niue International Airport","Nizhnevartovsk Airport","Nizhny Novgorod International Airport","Ni\u0161 Constantine the Great Airport","Njombe Airport","Nkan Airport","Nkaus Airport","Nkolo Fuma Airport","Nkongsamba Airport","Nnamdi Azikiwe International Airport","Noatak Airport","Nogales International Airport","Nogliki Airport","Noi Bai International Airport","Nojeh Airport","Nomad River Airport","Nomane Airport","Nome Airport","Nondalton Airport","Nonouti Airport","Noonkanbah Airport","Noosa Airport","Nop Goliat Dekai Airport","Norden-Norddeich Airport","Norderney Airport","Nordholz Naval Airbase Airport","Norfolk International Airport","Norfolk Island Airport","Norfolk Regional Airport","Norman Manley International Airport","Norman University of Oklahoma Westheimer Airport","Norman Wells Airport","Norman\'s Cay Airport","Normanton Airport","Norrk\u00f6ping Airport","Norseman Airport","Norsup Airport","North Battleford Airport","North Bay Airport","North Bimini Airport","North Caicos Airport","North Central State Airport","North Central West Virginia Airport","North Eleuthera Airport","North Fork Valley Airport","North Island Naval Air Station-Halsey Field Airport","North Las Vegas Airport","North Perry Airport","North Platte Regional Airport","North Ronaldsay Airport","North Spirit Lake Airport","North Texas Regional Airport","North Whale Seaplane Base","Northeast Alabama Regional Airport","Northeast Florida Regional Airport","Northeast Iowa Regional Airport","Northeast Ohio Regional Airport","Northeast Philadelphia Airport","Northeastern Regional Airport","Northern Aroostook Regional Airport","Northern Colorado Regional Airport","Northern Maine Regional Airport","Northern Peninsula Airport","Northern Rockies Regional Airport","Northway Airport","Northwest Alabama Regional Airport","Northwest Arkansas Regional Airport","Northwest Florida Beaches International Airport","Northwest Regional Airport","Norway House Airport","Norwich International Airport","Norwood Memorial Airport","Nor\u00f0fj\u00f6r\u00f0ur Airport","Nosara Airport","Noshahr Airport","Noto Airport","Notodden Airport","Nottingham Airport","Nouadhibou International Airport","Nouakchott\u2013Oumtounsy International Airport","Noum\u00e9a Magenta Airport","Noum\u00e9rat - Moufdi Zakaria Airport","Nouna Airport","Nova Xavantina Airport","Novgorod Airport","Novo Aripuan\u00e3 Airport","Novo Campo Airport","Novo Progresso Airport","Novy Urengoy Airport","Nowata Airport","Nowra Airport","Noyabrsk Airport","Nueva Guinea Airport","Nueva Hesperides International Airport","Nuevo Casas Grandes Airport","Nuevo Laredo International Airport","Nuguria Airstrip","Nuiqsut Airport","Nuku Airport","Nuku Hiva Airport","Nukus Airport","Nukutavake Airport","Nukutepipi Airport","Nulato Airport","Nullabor Motel Airport","Nullagine Airport","Numbulwar Airport","Nunapitchuk Airport","Nunchia Airport","Nunukan Airport","Nuremberg Airport","Nusatupe Airport","Nusawiru Airport","Nushki Airport","Nutuve Airport","Nutwood Downs Airport","Nuuk Airport","Nyac Airport","Nyagan Airport","Nyala Airport","Nyaung U Airport","Nyeri Airport","Nyingchi Mainling Airport","Nyngan Airport","Nyurba Airport","Nzagi Airport","Nz\u00e9r\u00e9kor\u00e9 Airport","N\u00e9ma Airport","N\u00eemes\u2013Al\u00e8s\u2013Camargue\u2013C\u00e9vennes Airport","O\'Hare International Airport","O\'Neill Municipal Airport","Oakdale Airport","Oakey Airport","Oakland County International Airport","Oakland International Airport","Oamaru Airport","Oaxaca International Airport","Oban Airport","Obando Airport","Obano Airport","Obbia Airport","Oberpfaffenhofen Airport","Obo Airport","Obock Airport","Obre Lake-North of Sixty Airport","Ocala International Airport","Ocean City Municipal Airport","Ocean County Airport","Ocean Falls Seaplane Base Airport","Ocean Reef Club Airport","Oceana Naval Air Station","Oceanside Municipal Airport","Ocho Rios Airport","Oconee County Regional Airport","Odate Noshiro Airport","Odense Airport","Odessa International Airport","Odienne Airport","Oecussi Airport","Oenpelli Airport","Oesman Sadik Airport","Offutt Air Force Base","Ofu Airport","Ogden-Hinckley Airport","Ogdensburg International Airport","Ogeranang Airport","Ogle Airport","Ogoki Post Airport","Ohio State University Airport","Ohkay Owingeh Airport","Ohrid Airport","Oiapoque Airport","Oita Airport","Okaba Airport","Okadama Airport","Okao Airport","Okaukuejo Airport","Okayama Airport","Okeechobee County Airport","Okha Airport","Okhotsk Airport","Oki Airport","Okierabu Airport","Okmulgee Regional Airport","Okondja Airport","Okoyo Airport","Oksapmin Airport","Oksibil Airport","Oktyabrsky Airport","Okushiri Airport","Ol Seki Airport","Olare Airport","Olavarria Airport","Olbia Costa Smeralda Airport","Old Crow Airport","Old Harbor Airport","Old Town Municipal Airport","Olenyok Airport","Olga Bay Seaplane Base","Olive Branch Airport","Olkiombo Airport","Olney Municipal Airport","Olney-Noble Airport","Olomouc Airport","Olpoi Airport","Olsobip Airport","Olsztyn-Mazury Airport","Olympia Regional Airport","Olympic Dam Airport","Olyokminsk Airport","Omak Airport","Omar N. Bradley Airport","Omboue-Hopital Airport","Omega Airport","Omidiyeh Airport","Omkalai Airport","Omora Airport","Omsk Tsentralny Airport","Ondangwa Airport","Ondjiva Pereira Airport","Oneonta Municipal Airport","Onepusu Airport","Ongava Game Reserve Airport","Ono-i-Lau Airport","Ononge Airport","Onotoa Airport","Onslow Airport","Ontario International Airport","Ontario Municipal Airport","Ontong Java Airport","Oodnadatta Airport","Oostmalle Airfield Airport","Opapimiskan Lake Airport","Open Bay Airport","Opinaca Aerodrome","Opuwo Airport","Oradea International Airport","Oral Ak Zhol Airport","Oram Airport","Oran International Airport","Orang Airport","Orange Airport","Orange County Airport","Orange Walk Airport","Orangeburg Municipal Airport","Oranjemund Airport","Orapa Airport","Orbost Airport","Orcas Island Airport","Orchid Beach Airport","Ord River Airport","Ordos Ejin Horo Airport","Ordu-Giresun Airport","Orenburg Tsentralny Airport","Orestes Acosta Airport","Oria Airport","Orientos Airport","Orinduik Airport","Oristano-Fenosu Airport","Oriximin\u00e1 Airport","Orlando Executive Airport","Orlando International Airport","Orlando Melbourne International Airport","Orlando Sanford International Airport","Orl\u00e9ans-Bricy","Ormara Airport","Ormoc Airport","Oro Negro Airport","Orocue Airport","Oroville Municipal Airport","Orsk Airport","Oryol Yuzhny Airport","Or\u00e1n Airport","Osage Beach Airport","Osaka International Airport","Osan Air Base","Osborne Mine Airport","Oscoda-Wurtsmith Airport","Osh Airport","Oshakati Airport","Oshawa Executive Airport","Oshima Airport","Osijek Airport","Oskaloosa Municipal Airport","Oskarshamn Airport","Oskemen Airport","Oslo Airport, Gardermoen","Osmani International Airport","Osorno Airport","Ossima Airport","Ostafyevo International Business Airport","Ostend-Bruges International Airport","Ostrava Leos Jan\u00e1\u010dek Airport","Osvaldo Vieira International Airport","Oswaldo Guevara Mujica Airport","Otjiwarongo Airport","Ottawa - Rockcliffe Airport","Ottawa Macdonald-Cartier International Airport","Ottumwa Regional Airport","Otu Airport","Ouadda Airport","Ouagadougou Airport","Ouahigouya Airport","Ouanaham Airport","Ouanda Djall\u00e9 Airport","Ouanga Airport","Ouango Fitini Airport","Ouani Airport","Ouarzazate Airport","Oudomsay Airport","Oudtshoorn Airport","Ouesso Airport","Oulu Airport","Oum Hadjer Airport","Ouril\u00e2ndia do Norte Airport","Ourinhos Airport","Ouro Sogui Airport","Outer Skerries Airport","Ouv\u00e9a Airport","Ouyen Airport","Ouzinkie Airport","Ovda Airport","Overberg Airport","Owando Airport","Owatonna Degner Regional Airport","Owen Roberts International Airport","Owen Sound Billy Bishop Regional Airport","Owendo Airport","Owensboro-Daviess County Regional Airport","Oxford Airport","Oxford House Airport","Oxnard Airport","Oyem Airport","Oyo Ollombo Airport","Ozark Regional Airport","Ozona Municipal Airport","P.R. Mphephu Airport","Paama Airport","Paamiut Airport","Pablo L. Sidar Airport","Pacific City State Airport","Pacific Harbour Airport","Paderborn Lippstadt Airport","Padre Aldamiz International Airport","Pagadian Airport","Page Field","Page Municipal Airport","Pago Pago International Airport","Pai Airport","Paiela Airport","Paine Field Airport","Pajala Airport","Pakokku Airport","Pakse International Airport","Pakuba Airport","Pakyong Airport","Pala Airport","Palacios Airport","Palacios Municipal Airport","Palanga International Airport","Palenque International Airport","Palestine Municipal Airport","Palm Beach County Glades Airport","Palm Beach County Park Airport","Palm Beach International Airport","Palm Beach Seaplane Base Airport","Palm Island Airport","Palm Springs International Airport","Palma Airport","Palma de Mallorca Airport","Palmar Sur Airport","Palmarito Airport","Palmas Airport","Palmdale Regional Airport","Palmer Municipal Airport","Palmerston North Airport","Palmyra Airport","Palo Alto Airport","Palo Verde Airport","Paloich Airport","Palonegro International Airport","Palopo Lagaligo Airport","Palu Mutiara Airport","Palungtar Airport","Pama Airport","Pambwa Airport","Pamol Airport","Pampa Guanaco Airport","Pamplona Airport","Panama City International Airport","Panama Pacifico International Airport","Pandie Pandie Airport","Panev\u0117\u017eys Air Base","Pangborn Memorial Airport","Pangia Airport","Pangkor Airport","Pangnirtung Airport","Pangoa Airport","Pangsuma Airport","Panguilemo Airport","Panguitch Municipal Airport","Panjgur Airport","Pantelleria Airport","Pantnagar Airport","Panzhihua Bao\'anying Airport","Papa Stour Airport","Papa Westray Airport","Papeete Airport","Paphos International Airport","Paraburdoo Airport","Parachinar Airport","Paradise River Airport","Paradise Skypark Airport","Parai-tepu\u00ed Airport","Parakou Airport","Param Airport","Paramakatoi Airport","Paramillo Airport","Paranagu\u00e1 Airport","Paranava\u00ed Airport","Parana\u00edba Airport","Paran\u00e1 Airport","Parasi Airport","Paratebueno Airport","Pardoo Airport","Pardubice Airport","Parintins Airport","Paris Charles de Gaulle Airport","Paris-Beauvais Airport","Paris-Orly Airport","Paris\u2013Le Bourget Airport","Park Falls Municipal Airport","Park Rapids Municipal Konshok Field Airport","Park Township Airport","Parker County Airport","Parkes Airport","Parlin Field","Parma Airport","Parna\u00edba International Airport","Parndana Airport","Paro International Airport","Paros National Airport","Parry Sound Area Municipal Airport","Parsabade Moghan Airport","Paruima Airport","Pasighat Airport","Pasir Gudang Port Airport","Pasir Pangaraan Airport","Pasni Airport","Paso Robles Municipal Airport","Paso de los Libres Airport","Passikudah Helipad Airport","Pathankot Airport","Pathein Airport","Patna Airport","Pato Branco Airport","Patos de Minas Airport","Patreksfj\u00f6r\u00f0ur Airport","Patrick Air Force Base","Pattani Airport","Pattimura Airport","Patuxent River Naval Air Station","Pau Pyr\u00e9n\u00e9es Airport","Pauk Airport","Paulatuk","Paulo Afonso Airport","Pavlodar Airport","Paya Lebar Air Base","Payam International Airport","Payson Airport","Pay\u00e1n Airport","Paz de Ariporo Airport","Peace River Airport","Pearl River County Airport","Peawanuck Airport","Pebane Airport","Pechora Airport","Pecos Municipal Airport","Pedas\u00ed Airport","Pedernales Airport","Pedro Bay Airport","Peenem\u00fcnde Airport","Pelaneng Airport","Pelican Seaplane Base Airport","Pellston Regional Airport","Pemba Airport","Pembina Municipal Airport","Pembroke Airport","Penang International Airport","Pender Harbour Water Airport","Pendopo Airport","Penggung Airport","Penglai Shahekou Airport","Penn Valley Airport","Penneshaw Airport","Penong Airport","Pensacola International Airport","Pensacola Naval Air Station-Forrest Sherman Field","Penticton Regional Airport","Penza Airport","Peppimenarti Airport","Perales Airport","Perito Moreno Airport","Perm International Airport","Perpignan\u2013Rivesaltes Airport","Perry Island Seaplane Base","Perry Lefors Field","Perry Municipal Airport","Perry Stokes Airport","Perry-Foley Airport","Perryville Airport","Persian Gulf Airport","Perth Airport","Perth-Scone Airport","Perugia San Francesco d\'Assisi \u2013 Umbria International Airport","Petawawa Airport","Peter O. Knight Airport","Peterborough Airport","Petersburg James A. Johnson Airport","Petit Jean Park Airport","Petrolina Airport","Petropavl Airport","Petropavlovsk-Kamchatsky Airport","Petrozavodsk Airport","Pevek Airport","Phan Rang Airport","Phaplu Airport","Phetchabun Airport","Phifer Airfield","Philadelphia International Airport","Philadelphia Seaplane Base","Philip Airport","Philip Billard Municipal Airport","Philip S. W. Goldson International Airport","Phillips Army Airfield","Phitsanulok Airport","Phnom Penh International Airport","Phoenix Deer Valley Airport","Phoenix Goodyear Airport","Phoenix Sky Harbor International Airport","Phoenix-Mesa-Gateway Airport","Phrae Airport","Phu Bai International Airport","Phu Cat Airport","Phu Quoc International Airport","Phuket International Airport","Phuoc Vinh Airport","Phuoclong Airport","Piarco International Airport","Pichoy Airport","Pickens County Airport","Pickle Lake Airport","Pico Airport","Picos Airport","Picton Aerodrome","Piedmont Triad International Airport","Piedras Negras International Airport","Pierce County Airport","Pierre Regional Airport","Pierrefonds Airport","Pietermaritzburg Airport","Pie\u0161\u0165any Airport","Pikangikum Airport","Pike County Airport","Pikwitonei Airport","Pilanesberg International Airport","Pilot Point Airport","Pilot Station Airport","Piloto Civil Norberto Fern\u00e1ndez International Airport","Pimaga Airport","Pimenta Bueno Airport","Pinal Airpark","Pinang Kampai Airport","Pincher Creek Airport","Pindiu Airport","Pine Cay Airport","Pine Island Airport","Pine Ridge Airport","Pinehouse Lake Airport","Pingtung Airport","Pinheiro Airport","Pinto Martins International Airport","Pipillipai Airport","Pirapora Airport","Pisa International Airport","Pisco Airport","Pitalito Airport","Pitt-Greenville Airport","Pitts Town Airport","Pittsburgh International Airport","Pittsburgh-Butler Regional Airport","Pittsfield Municipal Airport","Pitu Airport","Piura Airport","Placencia Airport","Placerville Airport","Plaine Corail Airport","Plan de Guadalupe International Airport","Planadas Airport","Planeta Rica Airport","Plastun Airport","Platinum Airport","Plato Airport","Plattsburgh International Airport","Playa Baracoa Airport","Playa Grande Airport","Playa Samara-Carrillo Airport","Playa de Oro International Airport","Playa del Carmen Airport","Play\u00f3n Chico Airport","Pleiku Airport","Plettenberg Bay Airport","Plovdiv Airport","Plymouth City Airport","Plymouth Municipal Airport","Po Airport","Pobedilovo Airport","Pocahontas Municipal Airport","Pocatello Regional Airport","Pocono Mountains Municipal Airport","Podgorica Airport","Podkamennaya Tunguska Airport","Podor Airport","Pogogul Airport","Pohang Airport","Pohnpei International Airport","Point Baker Seaplane Base","Point Hope Airport","Point Lay LRRS Airport","Point Lonely Short Range Radar Site Airport","Point Mugu Naval Air Station","Pointe Noire Airport","Pointe Vele Airport","Pointe-\u00e0-Pitre International Airport","Points North Landing Airport","Poitiers Biard Airport","Pokhara Airport","Polacca Airport","Polk Army Airfield","Polokwane International Airport","Poltava International Airport","Polyarny Airport","Pomala Airport","Pompano Beach Airpark","Ponca City Regional Airport","Pond Inlet Airport","Pondicherry Airport","Pongtiku Airport","Ponikve Airport","Ponta Grossa Airport","Ponta Pelada Airport","Ponta Por\u00e3 International Airport","Ponta do Ouro Airport","Pontes e Lacerda Airport","Pontoise - Cormeilles-en-Vexin Airport","Pope Field","Poplar Bluff Municipal Airport","Poplar Hill Airport","Poplar River Airport","Popondetta Airport","Poprad-Tatry Airport","Popt\u00fan Airport","Porbandar Airport","Pore Airport","Porgera Airport","Pori Airport","Porlamar International Airport","Port Alexander Seaplane Base","Port Alfred Airport","Port Alice Seaplane Base","Port Allen Airport","Port Alsworth Airport","Port Augusta Airport","Port Bailey Seaplane Base","Port Berg\u00e9 Airport","Port Bouet Airport","Port Clarence Coast Guard Station","Port Elizabeth Airport","Port Graham Airport","Port Harcourt City Airport","Port Harcourt International Airport","Port Hardy Airport","Port Hawkesbury Airport","Port Hedland International Airport","Port Heiden Airport","Port Hope Simpson Airport","Port Kaituma Airport","Port Keats Airport","Port Lincoln Airport","Port Lions Airport","Port Macquarie Airport","Port McNeill Airport","Port Meadville Airport","Port Moller Airport","Port Nelson Airport","Port Pirie Airport","Port Protection Seaplane Base","Port Said Airport","Port Simpson Airport","Port St Johns Airport","Port Stanley Airport","Port Sudan New International Airport","Port Walter Seaplane Base Airport","Port Williams Seaplane Base","Port of Poulsbo Marina Moorage Seaplane Base","Port-Gentil International Airport","Port-Menier Airport","Port-de-Paix Airport","Portage Creek Airport","Portage la Prairie-Southport Airport","Porter County Regional Airport","Porterville Municipal Airport","Portim\u00e3o Airport","Portland Airport","Portland International Airport","Portland International Jetport Airport","Portland\u2013Hillsboro Airport","Portland\u2013Troutdale Airport","Porto Airport","Porto Amboim Airport","Porto Cheli Airport","Porto Nacional Airport","Porto Santo Airport","Porto Seguro Airport","Porto Velho Airport","Porto de Moz Airport","Porto dos Ga\u00fachos Airport","Portoroz Airport","Portsmouth International Airport","Posadas Airport","Postville Airport","Potchefstroom Airport","Poum Airport","Pouso Alegre Airport","Powell Lake Seaplane Base Airport","Powell Municipal Airport","Powell River Airport","Pozna\u0144-\u0141awica Airport","Po\u00e7os de Caldas Airport","Prado Airport","Prairie du Chien Municipal Airport","Praslin Island Airport","Pratt Regional Airport","Preah Vinhear Airport","Prefeito Oct\u00e1vio de Almeida Neves Airport","Pregui\u00e7a Airport","Prentice Airport","Pres. Gral. \u00d3scar D. Gestido International Airport","President Obiang Nguema International Airport","Presidente Castro Pinto International Airport","Presidente Jos\u00e9 Sarney Airport","Presidente Nicolau Lobato International Airport","Presidente Per\u00f3n International Airport","Presidente Prudente Airport","Pre\u0161ov Air Base","Prieska Airport","Primrose Airport","Prince Abdul Majeed bin Abdulaziz Domestic Airport","Prince Albert Glass Field Airpor","Prince George Airport","Prince Mangosuthu Buthelezi Airport","Prince Mohammad Bin Abdulaziz Airport","Prince Nayef bin Abdulaziz International Airport","Prince Rupert Airport","Prince Rupert-Seal Cove Seaplane Base","Prince Said Ibrahim International Airport","Prince Sultan Air Base","Princess Juliana International Airport","Princeton Airport","Princeton Municipal Airport","Princeville Airport","Principe Airport","Prineville Airport","Pristina International Airport","Prominent Hill Airport","Propriano Airport","Prospect Creek Airport","Providenciales International Airport","Provideniya Bay Airport","Provincetown Municipal Airport","Provo Municipal Airport","Pryor Field Regional Airport","Pskov Airport","Pu\'er Simao Airport","Puas Airport","Pucallpa International Airport","Puc\u00f3n Airport","Puebla International Airport","Pueblo Memorial Airport","Puerto Ais\u00e9n Airport","Puerto Armuelles Airport","Puerto Barrios Airport","Puerto Berrio Airport","Puerto Cabezas Airport","Puerto Deseado Airport","Puerto Escondido International Airport","Puerto Jimenez Airport","Puerto La Victoria Airport","Puerto Leda Airport","Puerto Lempira Airport","Puerto Nare Airport","Puerto Obaldia Airport","Puerto Ordaz International Airport","Puerto Paez Airport","Puerto Pe\u00f1asco International Airport","Puerto Princesa International Airport","Puerto Rico Airport","Puerto Suarez Airport","Puka Puka Airport","Pukaki Airport","Pukapuka Island Airport","Pukarua Airport","Pukatawagan Airport","Pula Airport","Pulau Panjang Airport","Pulkovo Airport","Pullman-Moscow Regional Airport","Pumani Airport","Pune Airport","Punia Airport","Punta Abreojos Airfield","Punta Arenas Airport","Punta Cana International Airport","Punta Chivato Airport","Punta Colorada Airport","Punta Gorda Airport","Punta Islita Airport","Punta de Maisi Airport","Punta del Este Airport","Pupelde Airport","Purdue University Airport","Pureni Airport","Putao Airport","Putumayo Airport","Puvirnituq Airport","Pweto Airport","Pyay Airport","Pyongyang International Airport","P\u00e4rnu Airport","P\u00e9cs-Pog\u00e1ny International Airport","P\u00e9rigueux-Bassillac Airport","P\u0159erov Airport","Qaanaaq Airport","Qaarsut Airport","Qabala International Airport","Qacha\'s Nek Airport","Qala-I-Naw Airport","Qamdo Bamda Airport","Qarn Alam Airport","Qasigiannguit Heliport","Qayyarah West Airport","Qazvin Airport","Qianjiang Wulingshan Airport","Qiemo Airport","Qikiqtarjuaq Airport","Qingdao Liuting International Airport","Qingyang Airport","Qinhuangdao Beidaihe Airport","Qionghai Bo\'ao Airport","Qiqihar Sanjiazi Airport","Qishn Airport","Quad City International Airport","Quadra Island Airport","Quakertown Airport","Qualicum Beach Airport","Quang Ngai Airport","Quantico Marine Corps Airfield","Quanzhou Jinjiang International Airport","Quaqtaq Airport","Quartz Creek Airport","Quartz Hill Airport","Quatro de Fevereiro Airport","Queen Alia International Airport","Queen Beatrix International Airport","Queen Charlotte Island Airport","Queenstown Airport","Quelimane Airport","Quepos La Managua Airport","Quer\u00e9taro Intercontinental Airport","Quesnel Airport","Quetta International Airport","Quetzaltenango Airport","Quich\u00e9 Airport","Quillayute Airport","Quilpie Airport","Quimper\u2013Bretagne Airport","Quince Mil Airport","Quincy Regional Airport","Quinhagak Airport","Quirindi Airport","Quoin Hill Airport","Quonset State Airport","Qurghonteppa International Airport","Quthing Airport","Quzhou Airport","Qu\u00e9bec City Jean-Lesage International Airport","RNAS Yeovilton Airport","RNZAF Base Ohakea Airport","Raba Raba Airport","Rabah Bitat Airport","Rabak Airport","Rabat-Sal\u00e9 Airport","Rabaul Airport","Rabi Island Airport","Rach Gia Airport","Radin Inten II Airport","Radom Airport","Raduzhny Airport","Rafael Cabrera Mustelier Airport","Rafael N\u00fa\u00f1ez International Airport","Rafaela Airport","Rafa\u00ef Airport","Rafha Domestic Airport","Rafsanjan Airport","Raglan Airfield","Raiatea Airport","Rainbow Lake Airport","Raivavae Airport","Raja Bhoj Airport","Raja Haji Fisabilillah International Airport","Rajahmundry Airport","Rajbiraj Airport","Rajiv Gandhi International Airport","Rajkot Airport","Rajouri Airport","Rakanda Airport","Raleigh County Memorial Airport","Raleigh\u2013Durham International Airpor","Ralph M. Calhoun Memorial Airport","Ralph Wenz Field","Ralph Wien Memorial Airport","Ramagundam Airport","Ramata Airport","Ramechhap Airport","Ramingining Airport","Ramon Airport","Rampart Airport","Ramsar Airport","Ramstein Air Base","Ram\u00f3n Villeda Morales International Airport","Ranai Airport","Ranau Airport","Rand Airport","Randolph Air Force Base","Range Regional Airport","Rangely Airport","Ranger Municipal Airport","Rangiroa Airport","Rankin Inlet Airport","Ranong Airport","Rapid City Regional Airport","Rar Gwamar Airport","Raroia Airport","Rarotonga International Airport","Ras Al Khaimah International Airport","Rasht Airport","Ratanakiri Airport","Ratmalana Airport","Ratnagiri Airport","Raton Municipal Airport","Raudha Airport","Raufarh\u00f6fn Airport","Ravenna Airport","Ravensthorpe Airport","Rawalakot Airport","Rawlins Municipal Airport","Razer Airport","Reading Regional Airport","Reales Tamarindos Airport","Reao Airport","Rebun Airport","Rechlin\u2013L\u00e4rz Airfield","Recife Guararapes International Airport","Reconquista Airport","Red Bluff Municipal Airport","Red Deer Regional Airport","Red Devil Airport","Red Dog Airport","Red Lake Airport","Red Sucker Lake Airport","Redang Airport","Redcliffe Airport","Redding Municipal Airport","Reden\u00e7\u00e3o Airport","Redstone Army Airfield","Redwood Falls Municipal Airport","Reese Airpark","Reggio Calabria Airport","Regina Airport","Regina International Airport","Reginaldo Hammer Airport","Region of Waterloo International Airport","Regi\u00f3n de Murcia International Airport","Reid\u2013Hillview Airport","Reims \u2013 Champagne Air Base Airport","Reivilo Airport","Reko Diq Airport","Relais de la Reine Airport","Rendani Airport","Rene Mouawad Air Base","Renmark Airport","Rennell-Tingoa Airport","Rennes-Saint-Jacques Airport","Reno\u2013Tahoe International Airport","Renton Municipal Airport","Republic Airport","Resende Airport","Resistencia International Airport","Resolute Bay Airport","Retalhuleu Airport","Reus Airport","Revelstoke Airport","Rewa Airport","Rexburg Madison County Airport","Reyes Airport","Reyes Murillo Airport","Reykh\u00f3lar Airport","Reykjahl\u00ed\u00f0 Airport","Reykjav\u00edk Airport","Rhinelander-Oneida County Airport","Rhodes International Airport","Riberalta Airport","Ricardo Garc\u00eda Posada Airport","Rice Lake Regional Airport","Richard B. Russell Airport","Richard I Bong Airport","Richard Lloyd Jones Jr Airport","Richard Pearse Airport","Richard Toll Airport","Richards Bay Airport","Richfield Municipal Airport","Richland Airport","Richmond Airport","Richmond International Airport","Richmond Municipal Airport","Rick Husband Amarillo International Airport","Rickenbacker International Airport","Riesa-G\u00f6hlis Airport","Rif Airport","Riga International Airport","Rigolet Airport","Rijeka Airport","Rimatara Airport","Rimouski Airport","Rincon De Los Sauces Airport","Ringi Cove Airport","Rinkenberger Airport","Rio Amazonas Airport","Rio Branco International Airport","Rio Frio - Progreso Airport","Rio Grande Regional Airport","Rio Mayo Airport","Rio Turbio Airport","Rio Verde Airport","Rio de Janeiro-Gale\u00e3o International Airport","Rishiri Airport","River Cess Airport","Rivers Airport","Rivers Inlet Airport","Riverside Municipal Airport","Riverton Regional Airport","Riviera Airport","Rivi\u00e8re-du-Loup Airport","Rivne International Airport","Rizhao Shanzihe Airport","Road Town Airport","Roanne-Renaison Airport","Roanoke-Blacksburg Regional Airport","Robe Airport","Roben-Hood Airport","Robert","Robert Atty Bessing Airport","Robert F. Swinnie Airport","Robert L. Bradshaw International Airport","Robert S. Kerr Airport","Roberts Field","Roberts International Airport","Robertson Airport","Roberval Airport","Robinhood Airport","Robins Air Force Base","Robinson River Airport","Robinvale Airport","Robor\u00e9 Airport","Roche Harbor Airport","Rochefort \u2013 Saint-Agnant Airpor","Rochester Airport","Rochester International Airport","Rock Hill - York County Airport","Rock Sound International Airport","Rock Springs-Sweetwater County Airport","Rockhampton Airport","Rockhampton Downs Airport","Rockwood Municipal Airport","Rocky Mount-Wilson Regional Airport","Rocky Mountain House Airport","Rocky Mountain Metropolitan Airport","Rodelillo Airport","Rodez\u2013Aveyron Airport","Rodr\u00edguez Ball\u00f3n International Airport","Roebourne Airport","Rogers Executive Airport","Rogue Valley International\u2013Medford Airport","Roi Et Airport","Rokeby Airport","Rokot Airport","Roland Garros Airport","Roland-D\u00e9sourdy Airport","Rolla National Airport","Rollang Field","Rolpa Airport","Roma Airport","Roman Tmetuchl International Airport","Rome State Airport","Ronald Reagan Washington National Airport","Rondonopolis Airport","Rongelap Island Airport","Ronneby Airport","Rooke Field","Roosevelt Municipal Airport","Roper Bar Airport","Roper Valley Airport","Rosario Islas Malvinas International Airport","Rosario Seaplane Base","Roscoe Turner Airport","Roscommon County\u2013Blodgett Memorial Airport","Rose Bay Water Airport","Roseau Municipal Airport","Roseberth Airport","Roseburg Regional Airport","Rosecrans Memorial Airport","Rosella Plains Airport","Roshchino International Airport","Rosita Airport","Roskilde Airport","Ross River Airport","Rostock-Laage Airport","Rostov-on-Don Airport","Roswell International Air Center Airport","Rota International Airport","Rota Naval Station Airport","Roti Airport","Rotorua Regional Airport","Rotterdam The Hague Airport","Rottnest Island Airport","Rotuma Airport","Rouen Airport","Round Lake","Roundup Airport","Rourkela Airport","Rouses Point Seaplane Base","Rouyn-Noranda Airport","Rovaniemi Airport","Roxas Airport","Roy Hill Station Airport","Roy Hurd Memorial Airport","Roy Otten Memorial Airfield","Royal Air Force Station G\u00fctersloh Airport","Royal Air Forces Akrotiri Airport","Royal Air Forces Ascension Island Airport","Royal Air Forces Benson Airport","Royal Air Forces Brize Norton Airport","Royal Air Forces Coningsby Airport","Royal Air Forces Cottesmore Airport","Royal Air Forces Fairford Airport","Royal Air Forces Honington Airport","Royal Air Forces Kinloss Airport","Royal Air Forces Lakenheath Airport","Royal Air Forces Leuchars Airport","Royal Air Forces Lossiemouth Airport","Royal Air Forces Lyneham Airport","Royal Air Forces Marham Airport","Royal Air Forces Mildenhall Airport","Royal Air Forces Northolt Airport","Royal Air Forces Odiham Airport","Royal Air Forces Scampton Airport","Royal Air Forces Waddington Airport","Royal Air Forces Wyton Airport","Royal Airstrip","Royal Australian Air Force Base Curtin Airport","Royal Australian Air Force Base Richmond Airport","Royan-M\u00e9dis Airport","Rubelsanto Airport","Rubem Berta International Airport","Ruben Cantu Airport","Ruby Airport","Rugao Air Base","Ruhengeri Airport","Rukumkot Airport","Rumbek Airport","Rumginae Airport","Rumjatar Airport","Rundu Airport","Ruoqiang Loulan Airport","Rupsi Airport","Rurrenabaque Airport","Rurutu Airport","Ruse Airport","Russell Municipal Airport","Russian Mission Airport","Rustaq Airport","Ruston Regional Airport","Ruti Airport","Rutland Plains Airport","Rutland\u2013Southern Vermont Regional Airport","Rzesz\u00f3w-Jasionka Airport","Rzhevka Airport","R\u00f8ros Airport","R\u00f8rvik Airport, Ryum","R\u00f8st Airport","R\u00fcgen Airport","Saarbr\u00fccken Airport","Sabah Airport","Sabetta International Airport","Sabha Airport","Sabi Sabi Airport","Sabiha G\u00f6k\u00e7en International Airport","Sable Island Airport","Sabzevar Airport","Sacheon Airport","Sachigo Lake Airport","Sachs Harbour","Sacramento Executive Airport","Sacramento International Airport","Sacramento Mather Airport","Sadah Airport","Sadiq Abubakar III International Airport","Sado Airport","Safford Regional Airport","Safia Airport","Saga Airport","Sagarai Airport","Saginaw Seaplane Base","Sahabat Airport","Sahand Airport","Sahiwal Airport","Saibai Island Airport","Saidor Airport","Saidpur Airport","Saidu Sharif Airport","Saih Rawl Airport","Saint Helena Airport","Saint John Airport","Saint Louis Airport","Saint-Augustin Airport","Saint-Brieuc \u2013 Armor Airport","Saint-Georges-de-l\'Oyapock Airport","Saint-Laurent-du-Maroni Airport","Saint-L\u00e9onard Airport","Saint-Nazaire-Montoir Airport","Saint-Pierre Airport","Saint-Yan Airport","Saint-\u00c9tienne-Bouth\u00e9on Airport","Sainte Marie Airport","Saipan International Airport","Sakkyryr Airport","Sakon Nakhon Airport","Salak Airport","Salalah Airport","Salamanca Airport","Salamo Airport","Salekhard Airport","Salem Airport","Salem-Leckrone Airport","Salerno Costa d\'Amalfi Airport","Salerno Landing Zone Airport","Salgado Filho International Airport","Salima Airport","Salina Cruz Airport","Salina Regional Airport","Salina-Gunnison Airport","Salinas Municipal Airport","Salisbury-Ocean City-Wicomico Regional Airport","Salluit Airport","Salmon Arm Airport","Salt Cay Airport","Salt Lake City International Airport","Salton Sea Airport","Salvador-Deputado Lu\u00eds Eduardo Magalh\u00e3es International Airport","Salzburg Airport","Sam Mbakwe International Airport","Sam Neua Airport","Sam Ratulangi International Airport","Samangoky Airport","Saman\u00e1 El Catey International Airport","Samarinda Temindung Airport","Samarkand International Airport","Sambailo Airport","Sambava Airport","Sambu Airport","Samburu Airport","Samedan Airport","Samjiyon Airport","Samos International Airport","Sampit Airport","Sampson County Airport","Samsun-\u00c7ar\u015famba Airport","Samuels Field","Samui Airport","San Andros Airport","San Angelo Regional Airport","San Antonio International Airport","San Bernardino International Airport","San Blas Airport","San Carlos Airport","San Carlos Apache Airport","San Carlos de Bariloche Airport","San Crist\u00f3bal Airport","San Diego International Airport","San Esteban Airport","San Felipe International Airport","San Fernando Airport","San Francisco International Airport","San Gabriel Valley Airport","San Ignacio Airfield","San Ignacio Town Airstrip","San Ignacio de Moxos Airport","San Isidro de El General Airport","San Javier Airport","San Joaqu\u00edn Airport","San Jose Airport","San Jose International Airport","San Jos\u00e9 Airport","San Jos\u00e9 Island Airport","San Jos\u00e9 de Chiquitos Airport","San Jos\u00e9 del Guaviare Airport","San Juan De Uraba Airport","San Juan de Marcona Airport","San Juli\u00e1n Air Base","San Luis Airport","San Luis Obispo County Regional Airport","San Luis Potos\u00ed International Airport","San Luis R\u00edo Colorado Airport","San Luis Valley Regional Airport","San Luis de Palenque Airport","San Marcos Airport","San Mat\u00edas Airport","San Miguel Airport","San Nicolas Airport","San Pedro Airport","San Pedro de Jagua Airport","San Pedro de Uraba Airport","San Quint\u00edn Military Airstrip","San Rafael Airport","San Ram\u00f3n Airport","San Salvador Airport","San Salvador de Paul Airport","San Sebasti\u00e1n Airport","San Tom\u00e9 Airport","San Vicente Airport","San Vito De Java Airport","Sana\'a International Airport","Sanandaj Airport","Sancti Spiritus Airport","Sand Creek Airport","Sand Point Airport","Sandakan Airport","Sandane Airport","Sanday Airport","Sandefjord Airport, Torp","Sanderson Field","Sandnessj\u00f8en Airport","Sandringham Airport","Sandspit Airport","Sandstone Airport","Sandy Lake Airport","Sanfebagar Airport","Sanford Seacoast Regional Airport","Sanga-Sanga Airport","Sangapi Airport","Sanggata Airport","Sangster International Airport","Sania Ramel Airport","Sanikiluaq Airport","Sankt Peter-Ording Airport","Sanming Shaxian Airport","Sans Souci Airport","Santa Ana Airport","Santa Ana del Yacuma Airport","Santa Barbara Municipal Airport","Santa Bernardina International Airport","Santa B\u00e1rbara de Barinas Airport","Santa Carolina Airport","Santa Catalina Airport","Santa Cruz Air Force Base","Santa Cruz Airport","Santa Cruz Island Airport","Santa Cruz do Sul Airport","Santa Cruz-Graciosa Bay-Luova Airport","Santa Elena de Uairen Airport","Santa Fe Do Sul Airport","Santa Fe Municipal Airport","Santa Genoveva Airport","Santa Isabel do Morro Airport","Santa Luc\u00eda Airport","Santa Maria Airport","Santa Maria Public Airport","Santa Monica Municipal Airport","Santa Paula Airport","Santa Rosa Airport","Santa Rosa International Airport","Santa Rosa de Cop\u00e1n Airport","Santa Rosalia Airport","Santa Teresita Airport","Santa Terezinha Airport","Santa Ynez Airport","Santana Ramos Airport","Santana do Araguaia Airport","Santana do Livramento Airport","Santander Airport","Santarem Maestro Wilson Fonseca Airport","Santiago Vila Airport","Santiago de Compostela Airport","Santo-Pekoa International Airport","Santorini (Thira) International Airport","Santorini National Airport","Santos Dumont Airport","Sanya Phoenix International Airport","Sao Jose Do Rio Preto Airport","Sao Vicente Cesaria Evora Airport","Sapmanga Airport","Saposoa Airport","Saqani Airport","Sara Airport","Sarajevo International Airport","Sarakhs Airport","Saransk Airport","Sarasota\u2013Bradenton International Airport","Saratov Gagarin Airport","Saratov Tsentralny Airport","Saravane Airport","Sardar Vallabhbhai Patel International Airport","Sardeh Band Airport","Sarh Airport","Sarmi Orai Airport","Sarnia Chris Hadfield Airport","Sarteneja Airport","Sary Su Airport","Sary-Arka Airport","Sasereme Airport","Saskatoon John G. Diefenbaker International Airport","Saskylakh Airport","Sassandra Airport","Sasstown Airport","Satna Airport","Satu Mare International Airport","Satwag Airport","Sauce Viejo Airport","Saufley Field Airport","Sault Ste. Marie Airport","Saumlaki Olilit Airport","Sauren Airport","Saurimo Airport","Sau\u00f0\u00e1rkr\u00f3kur Airport","Savannah-Hilton Head International Airport","Savannakhet Airport","Savo Airport","Savonlinna Airport","Savoonga Airport","Savusavu Airport","Savuti Airport","Sav\u00e9 Airport","Sawan Airport","Sawu Airport","Sawyer County Airport","Sawyer International Airport","Sayaboury Airport","Sa\u00fcl Airport","Scammon Bay Airport","Scandinavian Mountains Airport","Scarlett Martinez International Airport","Scatsta Airport","Schefferville Airport","Schenck Field","Schenectady County Airport","Schleswig Air Base","Scholes International Airport","Schoolcraft County Airport","Schwerin Parchim International Airport","Scone Airport","Scottsdale Airport","Scribner State Airport","Scusciuban Airport","Sde Dov Airport","Seal Bay Seaplane Base","Searcy Municipal Airport","Searle Field","Seattle-Tacoma International Airport","Sebba Airport","Sebring Regional Airport","Sechelt Airport","Secunda Airport","Sedalia Regional Airport","Sedona Airport","Seghe Airport","Seguela Airport","Sehonghong Airport","Sehulea Airport","Sehwan Sharif Airport","Sei Bati Airport","Sei Pakning Airport","Sein\u00e4joki Airport","Seiyun Hadhramaut Airport","Sekake Airport","Selaparang Airport","Selawik Airport","Selbang Airport","Seldovia Airport","Selebi-Phikwe Airport","Seletar Airport","Selfridge Air National Guard Base Airport","Selfs Airport","Semarang Airport","Sematan Airport","Semera Airport","Semey Airport","Semnan Municipal Airport","Semonkong Airport","Semporna Airport","Sena Madureira Airport","Senadora Eunice Micheles Airport","Senai International Airport","Senanga Airport","Sendai Airport","Senggeh Airport","Senggo Airport","Sentani Airport","Seosan Air Base","Seoul Air Base","Sepik Plains Airport","Sept-\u00celes Airport","Sepulot Airport","Sep\u00e9 Tiaraju Airport","Sequim Valley Airport","Seronera Airstrip","Serui Airport","Sesheke Airport","Seshutes Airport","Sesriem Airport","Severo-Evensk Airport","Severodonetsk Airport","Seville Airport","Seward Airport","Seychelles International Airport","Seymour Airport","Seymour Johnson Air Force Base","Sfax-Thyna International Airport","Shafter Airport","Shageluk Airport","Shah Amanat International Airport","Shah Makhdum Airport","Shahbaz Air Base Airport","Shahid Ashrafi Esfahani Airport","Shahid Sadooghi Airport","Shahrekord Airport","Shahroud International Airport","Shaikh Zayed International Airport","Shakawe Airport","Shakhtyorsk Airport","Shakiso Airport","Shaktoolik Airport","Shamattawa Airport","Shamshernagar Airport","Shanghai Hongqiao International Airport","Shanghai Pudong International Airport","Shangjie Airport","Shangrao Sanqingshan Airport","Shank Air Base","Shannon Airport","Shanshan Airport","Shaoguan Guitou Airport","Shaoyang Wugang Airport","Sharana Airstrip","Sharjah International Airport","Shark Bay Airport","Sharm El Sheikh International Airport","Sharp County Regional Airport","Sharpe Field","Sharq El Owainat Airport","Sharurah Domestic Airport","Shashi Airport","Shaw Air Force Base","Shaw River Airport","Shawnee Regional Airport","Shay Gap Airport","Shearwater Airport","Sheboygan County Memorial Airport","Sheep Mountain Airport","Sheghnan Airport","Shelby Airport","Shelbyville Municipal Airport","Sheldon Point Airport","Shellharbour Airport","Shenandoah Valley Regional Airport","Shennongjia Hongping Airport","Shenyang Taoxian International Airport","Shenzhen Bao\'an International Airport","Shepparton Airport","Sher-Wood Airport","Sherbro International Airport","Sherbrooke Airport","Sheremetyevo International Airport","Sheridan County Airport","Sherman Army Airfield","Shigatse Peace Airport","Shihezi Huayuan Airport","Shijiazhuang Zhengding International Airport","Shilavo Airport","Shillong Airport","Shimla Airport","Shimojishima Airport","Shindand Air Base Airport","Shinyanga Airport","Shirak Airport","Shiraz International Airport","Shirdi Airport","Shire Airport","Shiringayoc Airport","Shishmaref Airport","Shively Field Airport","Shiyan Wudangshan Airport","Shizuoka Airport","Shonai Airport","Shoreham Airport","Show Low Regional Airport","Shreveport Downtown Airport","Shreveport Regional Airport","Shungnak Airport","Shymkent International Airport","Sialkot International Airport","Sialum Airport","Siargao Airport","Siasi Airport","Siassi Airport","Sibi Airport","Sibisa Airport","Sibiti Airport","Sibiu International Airport","Sibu Airport","Sibulan Airport","Sicogon Airport","Sidi Barrani Airport","Sidi Bel Abbes Airport","Sidi Ifni Airport","Sidi Mahdi Airport","Sidney Municipal Airport","Sidney-Richland Regional Airport","Siegerland Airport","Siem Reap International Airport","Siena-Ampugnano Airport","Sierra Blanca Regional Airport","Sierra Grande Airport","Sierra Maestra Airport","Sierra Vista Municipal Airport","Sigiriya Airport","Siglufj\u00f6r\u00f0ur Airport","Siguanea Airport","Siguiri Airport","Sihanoukville International Airport","Siirt Airport","Sikasso Airport","Sikeston Memorial Municipal Airport","Sila Airport","Silampari Airport","Silangit Airport","Silchar Airport","Silgadi Doti Airport","Silistra Polkovnik Lambrinovo Airfield","Silur Airport","Silva Bay Seaplane Base Airport","Silver Creek Airport","Silver Plains Airport","Silvio Pettirossi International Airport","Sim Airport","Simara Airport","Simbai Airport","Simberi Airport","Simenti Airport","Simferopol International Airport","Simikot Airport","Simmons Army Airfield","Simon Mwansa Kapwepwe International Airport","Sim\u00f3n Bol\u00edvar International Airport","Sinak Airport","Sindal Airport","Sindhri Airport","Sines Airport","Singapore Changi Airport","Singaua Airport","Singita Safari Lodge Airport","Singleton Airport","Sinop Airport","Sintang Airport","Siocon Airport","Sion Airport","Sioux Falls Regional Airport","Sioux Gateway Airport","Sioux Lookout Airport","Sipitang Airport","Sir Bani Yas Airport","Sir Captain Charles Kirkconnell International Airport","Sir Seewoosagur Ramgoolam International Airport","Sir Seretse Khama International Airport","Sirjan Airport","Sirri Island Airport","Sishen Airport","Sisimiut Airport","Siskiyou County Airport","Sissano Airport","Sitia Public Airport","Sitiawan Airport","Sitka Rocky Gutierrez Airport","Sittwe Airport","Siuna Airport","Sivas Nuri Demirag Airport","Siwa Oasis North Airport","Siwea Airport","Siwo Airport","Skagit Regional Airport","Skagway Airport","Skardu Airport","Skeldon Airport","Skellefte\u00e5 Airport","Skiathos Island National Airport","Skien Airport","Skikda Airport","Skive Airport","Skopje International Airport","Skorpion Mine Airport","Skukuza Airport","Skwentna Airport","Skye Bridge Ashaig Airport","Skylark Field","Skypark Airport","Skyros Island National Airport","Sk\u00f6vde Airport","Slave Lake Airport","Slayton Municipal Airport","Sleetmute Airport","Slia\u010d Airport","Sligo Airport","Sloulin Field International Airport","Smara Airport","Smith Field","Smith Point Airport","Smith Reynolds Airport","Smithers Airport","Smiths Falls-Montague Airport","Smithton Airport","Smolensk North Airport","Smyrna Airport","Snake Bay Airport","Snap Lake Airport","Soalala Airport","Sochi International Airport","Socorro Municipal Airport","Socotra Airport","Sodankyla Airport","Soddu Airport","Soekarno-Hatta International Airport","Soesterberg Air Base","Soewondo Air Force Base","Sofia Airport","Sogndal Haukasen Airport","Sohag International Airport","Sohar Airport","Soko Airport","Sokol Airport","Sola Airport","Solano Airport","Solapur Airport","Soldotna Airport","Solenzara Airport","Solidarity Szczecin\u2013Goleni\u00f3w Airport","Solita Airport","Solomon Airport","Solomon State Field","Solovki Airport","Solwezi Airport","Sonari Airport","Sondok Airport","Songea Airport","Songkhla Airport","Songyuan Chaganhu Airport","Sopu Airport","Soroako Airport","Sorocaba Airport","Soroti Airport","Sorriso Airport","Soto Cano Air Base Airport","Souank\u00e9 Airport","Soummam \u2013 Abane Ramdane Airport","Soure Airport","South Andros Airport","South Arkansas Regional Airport","South Bend International Airport","South Big Horn County Airport","South Bimini Airport","South Caicos Airport","South Cariboo Regional Airport","South Galway Airport","South Goulburn Island Airport","South Indian Lake Airport","South Jersey Regional Airport","South Lewis County Airport","South Naknek Airport","South West Bay Airport","South Wood County Airport","Southampton Airport","Southdowns Airport","Southeast Iowa Regional Airport","Southern California Logistics Airport","Southern Cross Airport","Southern Illinois Airport","Southern Seaplane Airport","Southern Wisconsin Regional Airport","Southport Airport","Southwest Florida International Airport","Southwest Georgia Regional Airport","Southwest Michigan Regional Airport","Southwest Minnesota Regional Airport","Southwest Oregon Regional Airport","Southwest Washington Regional Airport","Sovetsky Airport","Soyo Airport","Space Coast Regional Airport","Spangdahlem Air Base","Sparrevohn LRRS Airport","Sparta Community-Hunter Field Airport","Sparta-Fort McCoy Airport","Spartanburg Downtown Memorial Airport","Sparti Airport","Spence Airport","Spencer Municipal Airport","Sphinx International Airport","Spichenkovo Airport","Spirit Lake Municipal Airport","Spirit of St Louis Airport","Split Airport","Spokane International Airport","Spriggs Payne Airport","Spring Creek Airport","Spring Point Airport","Springbok Airport","Springdale Municipal Airport","Springfield-Beckley Municipal Airport","Springfield-Branson National Airport","Springvale Airport","Squamish Airport","Srednekolymsk Airport","Sri Guru Ram Dass Jee International Airport","Sri Sathya Sai Airport","Srinagar Airport","St Angelo Airport","St Aubin Airport","St Clair County International Airport","St George Airport","St Helens Airport","St John Island Airport","St Lucie County International Airport","St-Fran\u00e7ois Airport","St-Jean Airport","St. Anthony Airport","St. Catherine International Airport","St. Clair County Airport","St. Cloud Regional Airport","St. Gallen\u2013Altenrhein Airport","St. George Airport","St. George Regional Airport","St. John\'s International Airport","St. Johns Industrial Air Park","St. Landry Parish Airport","St. Lewis","St. Louis Downtown Airport","St. Louis Lambert International Airport","St. Louis Regional Airport","St. Mary\'s Airport","St. Mary\'s County Regional Airport","St. Marys Municipal Airport","St. Michael Airport","St. Paul Downtown Airport","St. Paul Island Airport","St. Paul\'s Mission Airport","St. Pete\u2013Clearwater International Airport","St. Theresa Point Airport","St. Thomas Municipal Airport","Stafsberg Airport","Stan Stamper Municipal Airport","Staniel Cay Airport","Stanthorpe Airport","Stanton Airfield","Stara Zagora Airport","Staroselye Airport","Statesboro Bulloch County Airport","Statesville Regional Airport","Stauning Airport","Stavanger Airport, Sola","Stavropol Shpakovskoye Airport","Stawell Airport","Steamboat Bay Seaplane Base Airport","Steamboat Springs Airport","Stebbins Airport","Stella Maris Airport","Stenkol Airport","Stephens County Airport","Stephens Island Airport","Stephenville Clark Regional Airport","Stephenville International Airport","Sterling Municipal Airport","Stevens Field","Stevens Point Municipal Airport","Stevens Village Airport","Stewart Airport","Stewart Island Airport","Stillwater Regional Airport","Stinson Municipal Airport","Stockholm Airport","Stockholm Skavsta Airport","Stockholm V\u00e4ster\u00e5s Airport","Stockholm-Arlanda Airport","Stockholm-Bromma Airport","Stockton Metropolitan Airport","Stoelmanseiland Airport","Stokmarknes Skagen Airport","Stony Rapids Airport","Stony River Airport","Stord S\u00f8rstokken Airport","Storm Lake Municipal Airport","Stornoway Airport","Storuman Airport","Strahan Airport","Strasbourg Airport","Strathmore Airport","Straubing Wallm\u00fchle Airport","Streaky Bay Airport","Strezhevoy Airport","Stronsay Airport","Strother Field","Stroud Municipal Airport","Stroudsburg-Pocono Airport","Stuart Island Airpark","Stuart Island Airport","Stung Treng Airport","Sturt Creek Airport","Stuttgart Airport","Stuttgart Municipal Airport","Stykkish\u00f3lmur Airport","Sua Pan Airport","Suabi Airport","Suai Airport","Suavanao Airport","Sub Teniente Nestor Arias Airport","Subic Bay International Airport","Suceava International Airport","Sucua Airport","Sudbury Airport","Sugar Land Regional Airport","Sugimanuru Airport","Sugraly Airport","Sui Airport","Suia-Missu Airport","Sukhothai Airport","Sukhumi Babushara Airport","Suki Airport","Sukkur Airport","Sulaco Airport","Sulaimaniyah International Airport","Sulayel Airport","Sule Airport","Sullivan Bay Airport","Sullivan County Airport","Sullivan County International Airport","Sulphur Springs Municipal Airport","Sultan Abdul Aziz Shah Airport","Sultan Abdul Halim Airport","Sultan Aji Muhammad Sulaiman Sepinggan International Airport","Sultan Azlan Shah Airport","Sultan Bantilan Airport","Sultan Haji Ahmad Shah Airport","Sultan Hasanuddin International Airport","Sultan Iskandar Muda International Airport","Sultan Ismail Petra Airport","Sultan Mahmud Airport","Sultan Mahmud Badaruddin II International Airport","Sultan Muhammad Kaharuddin III Airport","Sultan Syarif Kasim II International Airport","Sultan Thaha Airport","Sumbawanga Airport","Sumbe Airport","Sumburgh Airport","Summer Beaver Airport","Summerside Airport","Summit Airport","Sumter Airport","Sumy Airport","Sunan Shuofang International Airport","Sunchales Aeroclub Airport","Sundsvall-Timr\u00e5 Airport","Sungai Tiang Airport","Sunriver Airport","Sunshine Coast Airport","Suntar Airport","Sunyani Airport","Supadio Airport","Sur Airport","Surat Airport","Surat Thani Airport","Surgut International Airport","Suria Airport","Surigao Airport","Surin Airport","Surkhet Airport","Susanville Municipal Airport","Sussex County Airport","Suvarnabhumi Airport","Suwon Airport","Suzhou Guangfu Airport","Svalbard Airport, Longyear","Sveg Airport","Svetlaya Airport","Svolv\u00e6r Airport","Swakopmund Airport","Swami Vivekananda Airport","Swan Hill Airport","Swan River Airport","Swansea Airport","Swift Current Airport","Syamsudin Noor Airport","Syangboche Airport","Sydney Airport","Syktyvkar Airport","Sylt Airport","Sylvester Airport","Syracuse Hancock International Airport","Syros Island National Airport","Sywell Aerodrome","S\u00e3o Carlos International Airport","S\u00e3o Filipe Airport","S\u00e3o F\u00e9lix do Araguaia Airport","S\u00e3o F\u00e9lix do Xingu Airport","S\u00e3o Gabriel da Cachoeira Airport","S\u00e3o Jorge Airport","S\u00e3o Jos\u00e9 dos Campos Airport","S\u00e3o Louren\u00e7o Airport","S\u00e3o Louren\u00e7o do Sul Airport","S\u00e3o Mateus Airport","S\u00e3o Miguel do Araguaia Airport","S\u00e3o Miguel do Oeste Airport","S\u00e3o Paulo \u2013 Congonhas Airport","S\u00e3o Paulo \u2013 Guarulhos International Airport","S\u00e3o Tom\u00e9 International Airport","S\u00e9gou Airport","S\u00e9libaby Airport","S\u00f3c Tr\u0103ng Airport","S\u00f3crates Mariani Bittencourt Airport","S\u00f3crates Rezende Airport","S\u00f6derhamn Airport","S\u00f8nderborg Airport","S\u00f8rkjosen Airport","T. F. Green International Airport","TSTC Waco Airport","Ta\'izz International Airport","Taba International Airport","Tabal Airport","Tabarka\u2013A\u00efn Draham International Airport","Tabas Airport","Tabatinga International Airport","Tabibuga Airport","Tabiteuea North Airport","Tabiteuea South Airport","Tableland Homestead Airport","Tabl\u00f3n de Tamar\u00e1 Airport","Tabora Airport","Tabou Airport","Tabriz International Airport","Tabuaeran Island Airport","Tabubil Airport","Tabuk Regional Airport","Tacheng Airport","Tachilek Airport","Tacoma Narrows Airport","Tacuarembo Airport","Tadji Airport","Tadjoura Airport","Tadoule Lake Airport","Tafaraoui Airport","Taftan Airport","Taganrog Yuzhny Airport","Tagbilaran Airport","Tagbita Airport","Tagula Airport","Taharoa Aerodrome","Tahoua Airport","Tahsis Seaplane Base Airport","Taichung International Airport","Tainan Airport","Taipei Songshan Airport","Taiping Airport","Taisha Airport","Taitung Airport","Taiwan Taoyuan International Airport","Taiyuan Wusu International Airport","Taizhou Luqiao Airport","Tajima Airport","Tak Airport","Takaka Airport","Takamatsu Airport","Takapoto Airport","Takaroa Airport","Takengon Rembele Airport","Takhamalt Airport","Takhli Airport","Takoradi Airport","Takotna Airport","Taku Lodge Seaplane Base Airport","Takume Airport","Talagi Airport","Talakan Airport","Talara International Airport","Talasea Airport","Taldykorgan Airport","Talhar Airport","Taliabu Airport","Talkeetna Airport","Talladega Municipal Airport","Tallahassee International Airport","Tallinn Airport","Taloqan Airport","Taloyoak Airport","Taltheilei Narrows Airport","Tam K\u1ef3 Airport","Tamale Airport","Tamana Island Airport","Tamarindo Airport","Tambacounda Airport","Tambao Airport","Tambillos Airport","Tambohorano Airport","Tambolaka Airport","Tambor Airport","Tambov Donskoye Airport","Tamchakett Airport","Tampa International Airport","Tampa North Aero Park","Tampa Padang Airport","Tampere-Pirkkala Airport","Tampico International Airport","Tamu\u00edn National Airport","Tamworth Airport","Tan Son Nhat International Airport","Tan Tan Airport","Tanacross Airport","Tanah Grogot Airport","Tanah Merah Airport","Tanbar Airport","Tancredo Neves International Airport","Tancredo Thomas de Faria Airport","Tanda Tula Airport","Tandag Airport","Tandil Airport","Tanga Airport","Tangalooma Airport","Tangar\u00e1 da Serra Airport","Tangier Ibn Battuta Airport","Tangshan Sann\u00fche Airport","Tangwangcheng Airport","Tanjung Api Airport","Tanjung Harapan Airport","Tanjung Manis Airport","Tanjung Pelepas Port Airport","Tanjung Santan Airport","Tanna Airport","Taos Regional Airport","Tapachula International Airport","Tapeta Airport","Tapini Airport","Taplejung Airport","Tapuruquara Airport","Tara Airport","Tarabo Airport","Tarakbits Airport","Tarama Airport","Taranto-Grottaglie Airport","Tarapac\u00e1 Airport","Tarapaina Airport","Tarapoa Airport","Tarapoto Airport","Tarauac\u00e1 Airport","Taraz Airport","Tarbela Dam Airport","Tarbes\u2013Lourdes\u2013Pyr\u00e9n\u00e9es Airport","Tarcoola Airport","Taree Airport","Tarempa Airport","Tarfaya Airport","Tari Airport","Tarin Kowt Airport","Tarko-Sale Airport","Taroom Airport","Tartagal Airport","Tartu Airport","Tashkent International Airport","Tasikmalaya Wiriadinata Airport","Tasiujaq Airport","Taskul Airport","Tasu Water Aerodrome","Tata Airport","Tatakoto Airport","Tatalina LRRS Airport","Tatitlek Airport","Tau Airport","Taupo Airport","Tauramena Airport","Tauranga Airport","Tauta Airport","Tawa Airport","Tawau Airport","Taylor Airport","Taylor County Airport","Tazadit Airport","Ta\u2019if Regional Airport","Tbilisi International Airport","Tchibanga Airport","Tchien Airport","Tchongorove Airport","Ted Stevens Anchorage International Airport","Tef\u00e9 Airport","Tehachapi Municipal Airport","Tehran Imam Khomeini International Airport","Tehuac\u00e1n Airport","Teixeira de Freitas Airport","Tekadu Airport","Tekin Airport","Tekirda\u011f \u00c7orlu Airport","Tela Airport","Telefomin Airport","Telegraph Creek Airport","Telegraph Harbour Seaplane Base","Telfair-Wheeler Airport","Telfer Airport","Telida Airport","Teller Airport","Telluride Regional Airport","Telupid Airport","Tel\u00eamaco Borba Airport","Teminabuan Airport","Temora Airport","Tenakee Seaplane Base","Tenerife North Airport","Tenerife South Airport","Tengah Air Base","Tengchong Tuofeng Airport","Teniente Amin Ayub Gonzalez Airport","Teniente Coronel Carmelo Peralta Airport","Teniente Coronel Luis A. Mantilla International Airport","Teniente FAP Jaime Montreuil Morales Airport","Teniente Jorge Henrich Arauz Airport","Teniente Julio Gallardo Airport","Teniente Rodolfo Marsh Martin Airport","Teniente Vidal Airport","Tenkodogo Airport","Tennant Creek Airport","Tenzing-Hillary Airport","Tepic Airport","Teply Klyuch Airport","Tepoe Airstrip","Teptep Airport","Teraina Airport","Terapo Airport","Teresina\u2013Senador Petr\u00f4nio Portella Airport","Termal Airport","Termas de Rio Hondo Airport","Termez Airport","Ternate Airport","Terney Airport","Ternopil International Airport","Terrace Bay Airport","Terrance B. Lettsome International Airport","Terre Haute Regional Airport","Terre-de-Haut Airport","Terrell Municipal Airport","Teruel Airport","Teseney Airport","Teslin Airport","Tetebedi Airport","Teterboro Airport","Tetiaroa Airport","Tetlin Airport","Teuku Cut Ali Airport","Tewantin Airport","Texarkana Regional Airport","Texas Gulf Coast Regional Airport","Tezpur Airport","Tezu Airport","Thaba Nchu Airport","Thaba-Tseka Airport","Thakhek Airport","Thakurgaon Airport","Thames Airport","Thandwe Airport","Thangool Airport","Thanjavur Air Force Station Airport","Thargomindah Airport","The Eastern Iowa Airport","The Granites Airport","The Monument Airport","The Pas Airport","Theda Station Airport","Theodore Airport","Thessaloniki Airport \"Makedonia\"","Thicket Portage Airport","Thief River Falls Regional Airport","Thimarafushi Airport","Thisted Airport","Tho Xuan Airport","Thomas C Russell Field","Thomasville Regional Airport","Thompson Airport","Thompson Falls Airport","Thompson-Robbins Airport","Thorne Bay Seaplane Base","Thornhill Air Base","Thorshofn Airport","Three Rivers Municipal Dr Haines Airport","Thule Air Base","Thumrait Air Base Airport","Thunder Bay International Airport","Thylungra Airport","Tianjin Binhai International Airport","Tianshui Maijishan Airport","Tibooburra Airport","Tib\u00fa Airport","Ticantiqu\u00ed Airport","Tichitt Airport","Tidjikja Airport","Tifalmin Airport","Tiga Airport","Tijuana International Airport","Tikapur Airport","Tikchik Lodge Seaplane Base","Tikehau Airport","Tiko Airport","Tiksi Airport","Tilin Airport","Tillamook Airport","Timbedra Airport","Timber Creek Airport","Timbiqui Airport","Timbuktu Airport","Timbunke Airport","Timika Airport","Timimoun Airport","Timi\u0219oara Traian Vuia International Airport","Timmins-Victor M. Power Airport","Tin City Airport","Tinak Airport","Tinboli Airport","Tindouf Airport","Tingo Maria Airport","Tingwon Airport","Tinian International Airport","Tinker Air Force Base","Tinson Pen Airport","Tioga Municipal Airport","Tiom Airport","Tioman Airport","Tippi Airport","Tipton Airport","Tiputini Airport","Tirana International Airport","Tiree Airport","Tiruchirappalli International Airport","Tirupati Airport","Tisdale Airport","Tissa Tank Waterdrome Airport","Tivat Airport","Tjilik Riwut Airport","Tlokoeng Airport","Toamasina Airport","Tobermorey Airport","Tobolsk Airport","Tobruk Airport","Tob\u00edas Bola\u00f1os International Airport","Toccoa Airport","Tocoa Airport","Tocumen International Airport","Tocumwal Airport","Tofino Harbour Seaplane Base Airport","Tofino-Long Beach Airport","Togiak Airport","Tok Airport","Tokachi-Obihiro Airport","Tokat Airport","Tokeen Seaplane Base","Tokoroa Airfield","Toksook Bay Airport","Tokunoshima Airport","Tokushima Airport","Tol Airport","Toledo Airport","Toledo Executive Airport","Toledo Express Airport","Toliara Airport","Tolmachevo Airport","Toluca International Airport","Tom Price Airport","Tomanggong Airport","Tom\u00e1s de Heres Airport","Toncont\u00edn International Airport","Tongareva Airport","Tonghua Sanyuanpu Airport","Tongliao Airport","Tongoa Airport","Tongren Fenghuang Airport","Tonopah Airport","Tonopah Test Range Airport","Tonu Airport","Toowoomba Airport","Toowoomba Wellcamp Airport","Topeka Regional Airport","Torembi Airport","Torokina Airport","Toronto Pearson International Airport","Tororo Airport","Torres Airport","Torre\u00f3n Airport","Torrington Municipal Airport","Torsby Airport","Tortola West End Seaplane Base Airport","Tortol\u00ec Airport","Tortuguero Airport","Torwood Airport","Tosontsengel Airport","Totegegie Airport","Totness Airport","Tottori Airport","Touat Cheikh Sidi Mohamed Belkebir Airport","Tougan Airport","Touho Airport","Toulon-Hy\u00e8res Airport","Toulouse-Blagnac Airport","Tours Val de Loire Airport","Toussaint Louverture International Airport","Toussus-le-Noble Airport","Townsville Airport","Toyama Airport","Tozeur-Nefta International Airport","Trabzon Airport","Tradewind Airport","Trail Airport","Trang Airport","Trat Airport","Travis Air Force Base","Treasure Cay Airport","Treinta y Tres Airport","Trelew International Airport","Trent Lott International Airport","Trenton Municipal Airport","Trenton-Mercer Airport","Trepell Airport","Tres Arroyos Airport","Tres Esquinas Airport","Tres Lagoas Airport","Tres de Mayo Airport","Treviso Airport","Tri-Cities Airport","Tri-Cities Regional Airport","Tri-City Airport","Tri-County Regional Airport","Tri-State Airport","Tri-State Steuben County Airport","Triangle North Executive Airport","Tribhuvan International Airport","Trieste\u2013Friuli Venezia Giulia Airport","Trincomalee Harbour Seaplane Base Airport","Trinidad Airport","Tripoli International Airport","Trivandrum International Airport","Trois-Rivi\u00e8res Airport","Trollh\u00e4ttan\u2013V\u00e4nersborg Airport","Trombetas Airport","Trompeteros Airport","Troms\u00f8 Airport","Trona Airport","Trondheim Airport","Troy Municipal Airport","Truckee Tahoe Airport","Trujillo Airport","Tynda Airport","Tyndall Air Force Base","Tyonek Airport","Tzaneen Airport","T\u00e2rgu Mure\u0219 International Airport","T\u00eate-\u00e0-la-Baleine Airport","T\u00f4lanaro Airport","U-Tapao International Airport","Ua Huka Airport","Ua Pou Airport","Uaxactun Airport","Ubari Airport","Ubatuba Airport","Uberaba Airport","Uberl\u00e2ndia Airport","Ubon Ratchathani Airport","Ubrub Airport","Udine-Campoformido Air Base Airport","Udon Thani International Airport","Ufa International Airport","Uganik Island Airport","Ugashik Airport","Ugashik Bay Airport","Ugnu-Kuparuk Airport","Ugolny Airport","Uige Airport","Uiju Airfield","Ujae Airport","Ukhta Airport","Ukiah Municipal Airport","Ukunda Airport","Ulaangom Airport","Ulanhot Airport","Ulanqab Jining Airport","Ulawa Airport","Ulei Airport","Ulithi Airport","Uljin Airport","Ulsan Airport","Ulukhaktok Holman Airport","Ulusaba Airport","Ulyanovsk Baratayevka Airport","Ulyanovsk Vostochny Airport","Umba Airport","Umberto Modiano Airport","Umbu Mehang Kunda Airport","Ume\u00e5 Airport","Umiat Airport","Umiujaq Airport","Umuarama Airport","Una-Comandatuba Airport","Unalakleet Airport","Unalaska Airport","Undara Airport","Unguia Airport","Union County Airport","Union Island Airport","United States Air Force Academy Airfield","University Park Airport","University of Illinois Willard Airport","University-Oxford Airport","Unst Airport","Upala Airport","Upavon Airport","Upernavik Airport","Upiara Airport","Upington Airport","Upolu Airport","Urad Middle Banner Airport","Uranium City Airport","Uray Airport","Urengoy Airport","Urgench International Airport","Urgun Airport","Uribe Airport","Uriman Airport","Urmia Airport","Uroubi Airport","Urrao Airport","Uru Harbour Airport","Uruapan International Airport","Urubupunga Airport","Uruzgan Airport","Urzhar Airport","Useless Loop Airport","Usharal Airport","Ushuaia - Malvinas Argentinas International Airport","Usiminas Airport","Usino Airport","Usinsk Airport","Ust-Ilimsk Airport","Ust-Kut Airport","Ust-Kuyga Airport","Ust-Maya Airport","Ust-Nera Airport","Ust-Tsilma Airport","Ustupo Airport","Ustupo Ogobsucum Airport","Utila Airport","Utirik Airport","Utti Airport","Uvol Airport","Uytash Airport","Uyuni Airport","Uzhhorod International Airport","U\u015fak Airport","V. C. Bird International Airport","Vaasa Airport","Vadodara Airport","Vads\u00f8 Airport","Vahitahi Airport","Val-d\'Or Airport","Valcheta Airport","Valdez Airport","Valdosta Regional Airport","Valence-Chabeuil Airport","Valencia Airport","Valen\u00e7a Airport","Valesdir Airport","Valladolid Airport","Valle Airport","Valle de La Pascua Airport","Valle del Conlara Airport","Vallenar Airport","Valley International Airport","Van Don International Airport","Van Ferit Melen Airport","Van Nuys Airport","Vance Air Force Base","Vance W. Amory International Airport","Vancouver Harbour Flight Centre Airport","Vancouver International Airport","Vandalia Municipal Airport","Vandenberg Air Force Base","Vangaindrano Airport","Vanimo Airport","Vannes-Meucon Airport","Vanrook Station Airport","Vanuabalavu Airport","Varandey Airport","Vard\u00f8 Airport, Svartnes","Varkaus Airport","Varna Airport","Varrelbusch Airport","Vatomandry Airport","Vatukoula Airport","Vatulele Airport","Vava\'u International Airport","Veer Savarkar International Airport","Velana International Airport","Velasquez Airport","Velikiy Ustyug Airport","Velikiye Luki Airport","Venango Regional Airport","Venetie Airport","Venice Marco Polo Airport","Venice Municipal Airport","Ventspils International Airport","Veracruz International Airport","Verkhnevilyuysk Airport","Vermilion Airport","Vermilion Regional Airport","Vernal Regional Airport","Vernon Regional Airport","Vero Beach Regional Airport","Verona Villafranca Airport","Vestmannaeyjar Airport","Vicecomodoro \u00c1ngel de la Paz Aragon\u00e9s Airport","Vicenza Airport","Vichadero Airport","Vichy-Charmeil Airport","Vicksburg Municipal Airport","Victoria Airport","Victoria Falls International Airport","Victoria Inner Harbour Airport","Victoria International Airport","Victoria Regional Airport","Victoria Resevour Kandy Airport","Victoria River Downs Airport","Vidalia Regional Airport","Videira Airport","Vidyanagar Airport","Vienna International Airport","Vigo Airport","Vijayawada Airport","Vila Real Airport","Vila Rica Airport","Vilankulo Airport","Vilhelmina Airport","Vilhena Airport","Villa Constituci\u00f3n Airport","Villa Dolores Airport","Villa Garz\u00f3n Airport","Villa Gesell Airport","Villa Reynolds Airport","Villahermosa International Airport","Vilnius International Airport","Vilo Acu\u00f1a International Airport","Vilyuysk Airport","Vincent Fayks Airport","Vincenzo Florio Airport Trapani-Birgi","Vinh Airport","Vinh Long Airport","Vinnytsia International Airport","Vipingo Airport","Viqueque Airport","Virac Airport","Viracopos International Airport","Virgil I. Grissom Municipal Airport","Virgin Gorda Airport","Virginia Airport","Virginia Highlands Airport","Virginia Tech Montgomery Executive Airport","Viru Harbour Airstrip","Viru Viru International Airport","Visakhapatnam Airport","Visalia Municipal Airport","Visby Airport","Viseu Airport","Vitebsk Vostochny Airport","Vitoria Airport","Vittel Champ De Course Airport","Vit\u00f3ria da Conquista Airport","Viveros Island Airport","Vivigani Airfield","Vi\u00f1a del Mar Airport","Vladivostok International Airport","Vnukovo International Airport","Vodochody Airport","Vohimarina Airport","Voinjama Airport","Vojens Airport","Volgodonsk Airport","Volgograd International Airport","Volk Field","Volkel Air Base","Vologda Airport","Vopnafj\u00f6r\u00f0ur Airport","Vorkuta Airport","Voronezh International Airport","Votuporanga Airport","Vredendal Airport","Vryburg Airport","Vryheid Airport","Vung Tau Airport","Vunisea Airport","V\u00e1clav Havel Airport Prague","V\u00e1gar Airport","V\u00e4stervik Airport","V\u00e4xj\u00f6 Sm\u00e5land Airport","V\u00e9lizy \u2013 Villacoublay Air Base Airport","V\u00edctor Laf\u00f3n Airport","W. K. Kellogg Airport","W.H. Bud Barron Airport","Wabag Airport","Wabo Airport","Wabush Airport","Waca Airport","Waco Kungo Airport","Waco Regional Airport","Wad Medani Airport","Wadi Ain Airport","Wadi Halfa Airport","Wadi al Jandali Airport","Wadi al-Dawasir Domestic Airport","Wagau Airport","Wageningen Airstrip","Wagga Wagga Airport","Waghete Airport","Wagny Airport","Wahai Airport","Waiheke Island Airport","Waimea-Kohala Airport","Wainwright Airport","Wairoa Airport","Wajir Airport","Wakaya Island Airport","Wake Island Airfield","Wakkanai Airport","Wakunai Airport","Walaha Airport","Walcha Airport","Waldronaire Airport","Wales Airport","Walgett Airport","Walker\'s Cay Airport","Walla Walla Regional Airport","Wallal Airport","Wallops Flight Facility Airport","Walnut Ridge Regional Airport","Walt Disney World Airport","Walter J. Koladza Airport","Walvis Bay Airport","Wamena Airport","Wana Airport","Wanaka Airport","Wang-an Airport","Wanganui Airport","Wangaratta Airport","Wangerooge Airport","Wanigela Airport","Wantoat Airport","Wanuma Airport","Wanzhou Wuqiao Airport","Wapekeka Airport","Wapenamanda Airport","Wapolu Airport","Warangal Airport","Warder Airport","Ware Airport","Waris Airport","Warm Spring Bay Seaplane Base Airport","Warraber Island Airport","Warracknabeal Airport","Warrawagine Airport","Warren County Memorial Airport","Warren Field","Warri Airport","Warrnambool Airport","Warroad International Memorial Airport","Warsaw Chopin Airport","Warsaw Modlin Airport","Warton Airport","Warukin Airport","Warwick Airport","Washabo Airport","Washington County Airport","Washington Dulles International Airport","Wasilla Airport","Wasior Airport","Waskaganish Airport","Waspam Airport","Wasu Airport","Wasua Airport","Wasum Airport","Waterbury-Oxford Airport","Waterfall Seaplane Base","Waterford Airport","Waterkloof Air Force Base","Waterloo Airport","Waterloo Regional Airport","Waterport Airport","Watertown International Airport","Watertown Regional Airport","Waterville Robert Lafleur Airport","Watson Lake Airport","Watsonville Municipal Airport","Wattay International Airport","Wau Airport","Wauchope Airport","Waukegan National Airport","Waukesha County Airport","Waukon Municipal Airport","Wausau Downtown Airport","Wave Hill Airport","Wawa Airport","Wawoi Falls Airport","Waycross-Ware County Airport","Wayne County Airport","Waynesville-St. Robert Regional Airport","Weam Airport","Weasua Airport","Webequie Airport","Webster City Municipal Airport","Wedau Airport","Wee Waa Airport","Weedon Field","Weerawila Airport","Weeze Airport","Weifang Airport","Weihai Dashuibo Airport","Weipa Airport","Wekwe\u00e8t\u00ec Airport","Welkom Airport","Wellington International Airport","Wells Municipal Airport","Wellsville Municipal Airport","Welshpool Airport","Wemindji Airport","Wendover Airport","Wenshan Puzhehei Airport","Wenzhou Longwan International Airport","West Angelas Airport","West Bend Municipal Airport","West Branch Community Airport","West End Airport","West Houston Airport","West Kootenay Regional Airport","West Kutai Melalan Airport","West Memphis Municipal Airport","West Point Village Seaplane Base Airport","West Sale Airport","West Woodward Airport","West Wyalong Airport","Westchester County Airport","Westerly State Airport","Western Nebraska Regional Airport","Westfield-Barnes Regional Airport","Westover Metropolitan Airport","Westport Airport","Westray Airport","Westsound Seaplane Base Airport","Wevelgem Airport","Wewak Airport","Wexford County Airport","Whakatane Airport","Whale Cove Airport","Whalsay Airport","Whangarei Airport","Wharton Regional Airport","What\u00ec Airport","Wheeler Army Airfield","Wheeling Ohio County Airport","Whidbey Island Naval Air Station Airport","Whistler Airport","White Mountain Airport","White River Airport","Whitecourt Airport","Whitehorse International Airport","Whitehouse Naval Outlying Field Airport","Whiteman Air Force Base","Whiteman Airport","Whiteriver Airport","Whiteside County Airport","Whitianga Airport","Whiting Field Naval Air Station - North","Whitsunday Airport","Whitsunday Coast Airport","Whyalla Airport","Wiarton Airport","Wichita Dwight D. Eisenhower National Airport","Wichita Falls Regional Airport","Wick Airport","Wiesbaden Army Airfield","Wilbarger County Airport","Wilcannia Airport","Wild Coast Sun Airport","Wiley Post Airport","Wiley Post-Will Rogers Memorial Airport","Wilgrove Air Park","Wilkes County Airport","Wilkes-Barre Wyoming Valley Airport","Wilkes-Barre-Scranton International Airport","Will Rogers World Airport","William P. Hobby Airport","William R Fairchild International Airport","William T. Piper Memorial Airport","Williams Harbour Airport","Williams Lake Airport","Williamson County Regional Airport","Williamsport Regional Airport","Williston Basin International Airport","Willmar Municipal Airport","Willoughby Lost Nation Municipal Airport","Willow Airport","Willow Run Airport","Willows-Glenn County Airport","Wilmington Air Park Airport","Wilmington International Airport","Wilpena Pound Airport","Wilson Airport","Wiluna Airport","Winchester Regional Airport","Windarling Airport","Windarra Airport","Windom Municipal Airport","Windorah Airport","Windsor International Airport","Wings Field","Winkler County Airport","Winnemucca Municipal Airport","Winnipeg James Armstrong Richardson International Airport","Winnipeg-St. Andrews Airport","Winona Municipal Airport","Winslow-Lindbergh Regional Airport","Winston Field","Winter Haven\'s Gilbert Airport","Winton Airport","Wipim Airport","Wiscasset Airport","Wiseman Airport","Witham Field","Wittenoom Airport","Wittman Regional Airport","Witu Airport","Woensdrecht Air Base","Woitape Airport","Woja Airport","Wolf Point L. M. Clayton Airport","Wollaston Lake Airport","Wollogorang Airport","Wondai Airport","Wonderboom Airport","Wondoola Airport","Wonenara Airport","Wonju Airport","Wonken Airport","Wonsan Kalma International Airport","Woodbourne Airport","Woodford Airport","Woodgreen Airport","Woodie Woodie Airport","Woodward Field","Woomera Airfield Airport","Wora na Yeno Airport","Worcester Regional Airport","Worland Municipal Airport","Worthington Municipal Airport","Wotho Island Airport","Wotje Airport","Wrangell Airport","Wright-Patterson Air Force Base","Wrigley Airport","Wroc\u0142aw Copernicus Airport","Wrotham Park Airport","Wudinna Airport","Wuhai Airport","Wuhan Tianhe International Airport","Wuhu Wanli Airport","Wunnumin Lake Airport","Wuvulu Island Airport","Wuzhou Xijiang Airport","Wycombe Air Park","Wyk auf F\u00f6hr Airport","Wyndham Airport","Xai-Xai Airport","Xangongo Airport","Xanxer\u00ea Airport","Xi\'an Xianyang International Airport","Xi\'an Xiguan Airport","Xiamen Gaoqi International Airport","Xiangyang Liuji Airport","Xichang Qingshan Airport","Xieng Khouang Airport","Xienglom Airport","Xilinhot Airport","Xinbarag Youqi Baogede Airport","Xingcheng Airport","Xingning Airport","Xingtai Dalian Airport","Xinguara Municipal Airport","Xingyi Wanfenglin Airport","Xining Caojiabao International Airport","Xinyang Minggang Airport","Xinyuan Nalati Airport","Xinzhou Wutaishan Airport","Xishuangbanna Gasa Airport","Xuzhou Guanyin Airport","Yacuiba Airport","Yagoua Airport","Yaguara Airport","Yakataga Airport","Yakima Air Terminal Airport","Yakubu Gowon Airport","Yakushima Airport","Yakutat Airport","Yakutsk Airport","Yalata Mission Airport","Yalgoo Airport","Yalinga Airport","Yalumet Airport","Yam Island Airport","Yamagata Airport","Yamaguchi Ube Airport","Yamoussoukro Airport","Yampa Valley Airport","Yan\'an Ershilipu Airport","Yanbu Airport","Yancheng Nanyang International Airport","Yandicoogina Airport","Yandina Airport","Yangambi Airport","Yangon International Airport","Yangyang International Airport","Yangzhou Taizhou Airport","Yanji Chaoyangchuan Airport","Yankisa Airport","Yantai Penglai International Airport","Yaound\u00e9 Airport","Yaound\u00e9 Nsimalen International Airport","Yap International Airport","Yapsiei Airport","Yari Airport","Yarigu\u00edes Airport","Yarmouth Airport","Yas Island Seaplane Base Airport","Yasawa Island Airport","Yasser Arafat International Airport","Yasuj Airport","Yasuru Airport","Yavarate Airport","Yaviza Airport","Ye Airport","Yeager Airport","Yecheon Airbase Airport","Yedinka Airport","Yeelirrie Airport","Yeerqiang Airport","Yegepa Airport","Yellowknife Airport","Yellowstone Airport","Yellowstone Regional Airport","Yengema Airport","Yeniseysk Airport","Yeni\u015fehir Airport","Yeosu Airport","Yerbogachen Airport","Yerington Municipal Airport","Yes Bay Lodge Seaplane Base Airport","Yeva Airport","Yevlakh Airport","Yeysk Airport","Yibin Caiba Airport","Yichang Sanxia Airport","Yichun Lindu Airport","Yichun Mingyueshan Airport","Yilan Airport","Yinchuan Hedong International Airport","Yinchuan Helanshan Airport","Yingkou Lanqi Airport","Yining Airport","Yiwu Airport","Ylivieska Airfield","Yogyakarta International Airport","Yokangassi Airport","Yokota Air Base","Yola Airport","Yonaguni Airport","Yongai Airport","Yongphulla Airport","Yongzhou Lingling Airport","York Airport","York Landing Airport","Yorke Island Airport","Yorketown Airport","Yorkton Municipal Airport","Yoro Airport","Yoron Airport","Yoshkar-Ola Airport","Yotvata Airfield","Young Airport","Youngstown\u2013Warren Regional Airport","Yuanmou Airport","Yuba County Airport","Yucca Airstrip","Yuendumu Airport","Yulin Yuyang Airport","Yuma International Airport","Yuncheng Guangong Airport","Yuruf Airport","Yushu Batang Airport","Yuzhno-Kurilsk Mendeleyevo Airport","Yuzhno-Sakhalinsk Airport","Y\u00e9liman\u00e9 Airport","Z.M. Jack Stell Field Airport","Zabol Airport","Zabrat Airport","Zabreh Ostrava Airport","Zabr\u00e9 Airport","Zacatecas International Airport","Zachar Bay Seaplane Base","Zadar Airport","Zafer Airport","Zagora Airport","Zagreb Airport","Zahedan International Airport","Zakouma Airport","Zakynthos International Airport","Zalingei Airport","Zambezi Airport","Zamboanga International Airport","Zamora Airport","Zamperini Field","Zamzama Heliport","Zanaga Airport","Zanesville Municipal Airport","Zanjan Airport","Zapala Airport","Zapatoca Airport","Zaporizhia International Airport","Zaqatala International Airport","Zaragoza Airport","Zaranj Airport","Zaria Airport","Zaysan Airport","Zemio Airport","Zenag Airport","Zenata - Messali El Hadj Airport","Zephyrhills Municipal Airport","Zero Airport","Zhalantun Chengjisihan Airport","Zhangjiajie Hehua International Airport","Zhangjiakou Ningyuan Airport","Zhangye Ganzhou Airport","Zhanjiang Airport","Zhaotong Airport","Zhengzhou Xinzheng International Airport","Zhezkazgan Airport","Zhigansk Airport","Zhob Airport","Zhongwei Shapotou Airport","Zhoushan Putuoshan Airport","Zhuhai Jinwan Airport","Zhukovsky International Airport","Zhytomyr Airport","Zielona Gora Babimost Airport","Zigong Fengming General Airport","Ziguinchor Airport","Zilfi Airport","Zinder Airport","Zintan Airport","Zona da Mata Regional Airport","Zonalnoye Airport","Zonguldak Airport","Zorg en Hoop Airport","Zulu Inyala Airport","Zunyi Maotai Airport","Zunyi Xinzhou Airport","Zuwarah Airport","Zvartnots International Airport","Zweibr\u00fccken Airport","Zyryanka Airport","Z\u00fcrich Airport","anta B\u00e1rbara del Zulia Airport","\u00c1ngel Albino Corzo International Airport","\u00c4ngelholm-Helsingborg Airport","\u00c5re \u00d6stersund Airport","\u00c7anakkale Airport","\u00c7i\u011fli Airport","\u00c7\u0131ld\u0131r Airport","\u00c9pinal \u2013 Mirecourt Airport","\u00c9vreux-Fauville Air Base Airport","\u00cdsafj\u00f6r\u00f0ur Airport","\u00cele Art - Waala Airport","\u00cele d\'Yeu Airport","\u00cele des Pins Airport","\u00celes-de-la-Madeleine Airport","\u00d3bidos Airport","\u00d3lafsfj\u00f6r\u00f0ur Airport","\u00d6lgii Airport","\u00d6nd\u00f6rkhaan Airport","\u00d6rebro Airport","\u00d6rnsk\u00f6ldsvik Airport","\u00d8rland Airport","\u00d8rsta-Volda Airport, Hovden","\u00dcr\u00fcmqi Diwopu International Airport","\u00deingeyri Airport","\u0130ncirlik Air Base","\u0141\u00f3d\u017a W\u0142adys\u0142aw Reymont Airport","\u014cmura Airport","\u015eanl\u0131urfa Airport","\u015eanl\u0131urfa GAP Airport","\u015e\u0131rnak Airport","\u0160iauliai International Airport","\u017dabljak Airport","\u017dilina Airport"]';
}

function getEmailID($id){
    $id = explode("-", $id);
    $id = $id[3] . "" . $id[4];

    return $id;
}

function getPointsBalance($transactions){
    $total = 0;
    foreach($transactions as $transaction){
        if($transaction["type"] == "points-deposit" || $transaction["type"] == "points-withdraw"){
            if(strtolower($transaction["status"]) == "complete"){
                $amount = (float) $transaction["amount"];

                if($transaction["type"] == "points-withdraw"){
                    $amount *= -1;
                }

                $total += $amount;
            }
        } 
    }

    return $total;
}

function search_array ( $array, $key, $value )
{
   return array_search($value,array_column($array,$key));
}

function seatableAirline($airline){

    $airlines = [
        "Air France",
        "American Airlines",
        "Australian Airlines",
        "British Airways",
        "Brussels Airlines",
        "easyJet",
        "Eurowings Discover",
        "Hawaiian Airlines",
        "Jetstar",
        "KLM",
        "Lufthansa",
        "Qantas",
        "Swiss",
        "United Airlines",
        "Duffel Airways"
    ];

    if(in_array($airline, $airlines)){
        return true;
    }

    return false;
}

function baggableAirline($airline){
    $airlines = [
        "Aegean Airlines",
        "Air France",
        "Austrian Airlines",
        "British Airways",
        "Brussels Airlines",
        "easyJet",
        "Eurowings Discover",
        "Jetstar",
        "KLM",
        "LEVEL",
        "Lufthansa",
        "Olympic Air",
        "Qantas",
        "Spirit",
        "Swiss",
        "Transavia",
        "United Airlines",
        "Volaris",
        "Vueling Airlines"
    ];

    if(in_array($airline, $airlines)){
        return true;
    }

    return false;
}

function getSweepstakeEntries($subscription, $cashBalance, $pointsBalance){
    $entries = 0;
    $entries += $cashBalance;

    switch ($subscription) {
        case 'buddy':
            $pointsBalance *= 1;
            break;
        case 'priority':
            $pointsBalance *= 2;
            break;
        case 'business':
            $pointsBalance *= 4;
            break;
        case 'first':
            $pointsBalance *= 8;
            break;
    }

    $entries += $pointsBalance;

    //16949.270675033 / 8 = 2,118.6588343791

    return round($entries, 0);
}

function getDiscountPercent($productClass, $msrp, $currency = "USD"){

    if($currency != "USD"){
        $msrp = convertCurrency($msrp, $currency);
    }
    if($productClass == "airfare"){
        $new = $msrp - 250;
    }

    switch ($productClass) {
        case 'airfare':
            switch ($msrp) {
                case $msrp > 5000:
                    return ($new/$msrp);
                    break;
                case ($msrp < 5000 && $msrp >= 2000 ):
                    return ($new/$msrp);
                    break;
                case ($msrp < 2000 && $msrp >= 1000):
                    return ($new/$msrp);
                    break;
                case ($msrp < 1000 && $msrp > 250):
                    return ($new/$msrp);
                    break;
                case ($msrp <= 250):
                    return 0;
                    break;
                default:
                    return 1;
                    break;
            }
            return 1;
            break;
        case 'hotel':
            switch ($msrp) {
                case $msrp > 5000:
                    // 5 percent off top-tier/extended stays
                    return 0.95;
                    break;
                case ($msrp < 5000 && $msrp > 2000):
                    return 0.85;
                    break;
                case ($msrp < 2000 && $msrp > 500):
                    return 0.825;
                    break;
                case ($msrp < 500 && $msrp > 200):
                    return 0.8;
                    break;
                case ($msrp < 200 && $msrp > 120):
                    return ($msrp - 40)/$msrp;
                    break;
                case ($msrp <= 120):
                    return 0;
                    // 99 percent off short trip 2-star stays
                    break;
                default:
                    return 1;
                    break;
            }
            return 1;
            break;
        default:
            return 1;
            break;
    }
}

function getMarkupMultiplier($productClass, $msrp){
    switch ($productClass) {
        case 'baggage':
            // code...

            switch ($msrp) {
                case $msrp > 150:
                    // add 10 percent to price when the baggage fee is above 150
                    return 1.1;
                    break;
                case ($msrp < 150 && $msrp > 100):
                    return 1.2;
                    // add 20
                    break;
                case ($msrp < 100 && $msrp > 75):
                    return 1.25;
                    // add 25
                    break;
                case ($msrp < 75 && $msrp > 50):
                    return 1.33;
                    // add 33
                    break;
                case ($msrp < 50 && $msrp > 0):
                    return 1.6;
                    // add 60% markup
                    break;
                case ($msrp == 0):
                    return 50; // $50 min for bags
                default:
                    return 1.25;
                    break;
            }
            return false;
            break;
        case 'seat':
            // code...

            switch ($msrp) {
                case $msrp > 150:
                    // add 10 percent to price when the seat fee is above 150
                    return 1.1;
                    break;
                case ($msrp < 150 && $msrp > 100):
                    return 1.2;
                    // add 20
                    break;
                case ($msrp < 100 && $msrp > 75):
                    return 1.25;
                    // add 25
                    break;
                case ($msrp < 75 && $msrp > 50):
                    return 1.33;
                    // add 33
                    break;
                case ($msrp < 50 && $msrp > 0):
                    return 1.6;
                    // add 60% markup
                    break;
                case ($msrp == 0):
                    return 25; // $50 min for bags
                default:
                    return 1.25;
                    break;
            }
            return false;
            break;

        default:
            return false;
            break;
    }
}

function baseMarkUpNonZero($price){
    if($price < 15){
        return 15;
    }
    return $price;
}

function convertCurrency($amount, $currency){
    if($currency == "USD"){
        return $amount;
    }

    $firestore = app('firebase.firestore');
    $db = $firestore->database();

    $rates = $db->collection("Currency")->document("rates")->snapshot()->data();

    $amount = ($amount / ($rates[$currency] ?? 1)); 

    return (float) round($amount * 1.0275 * $rates["USD"],2); // 2.75% conversion fee
}

function preventNameDuplication($displayName){
    $name = $displayName;
    try{
        $displayName = explode(" ", $name);

        if(sizeof($displayName) > 1){
            // we detected a improver name value
            $first_name = $displayName[0];
            $last_name = $displayName[1];

            return $first_name . " " . $last_name;
        }
    }
    catch(Exception $e){

    }
    
    return $name;
}

function getTripOriginData($slices){
    // we need to communicate data about where the traveler is leaving from

    if(array_key_exists("segments", $slices[0])){
        $origin = $slices[0]["segments"][0];
    } else {
        $origin = $slices[0];
    }

    $data = [
        "origin" => $slices[0]["origin"],
        "destination" => $slices[0]["destination"],
        "details" => $origin
    ];

    return $data;
}

function getTripDestinationData($slices){
    // we need to communicate data about where the traveler is leaving from

    if(array_key_exists("segments", $slices[0])){
        $origin = $slices[0]["segments"][0];
    } else {
        $origin = $slices[0];
    }

    $data = [
        "departingOrigin" => $slices[0]["origin"],
        "destinationArrival" => $slices[0]["destination"],
        "originData" => $origin
    ];

    return $data;
}

function getTripCancelationDetails($conditions){
    if(is_null($conditions)){
        return false;
    }
    if(array_key_exists("refund_before_departure", $conditions)){
        if($conditions["refund_before_departure"] == null){
            return false;
        }
        if(array_key_exists("allowed", $conditions["refund_before_departure"])){
            if($conditions["refund_before_departure"]["allowed"] == true){
                // refund is allowed
                return [
                    "currency" => $conditions["refund_before_departure"]["penalty_currency"],
                    "amount" => $conditions["refund_before_departure"]["penalty_amount"]
                ];
            }
        }
    }
    // the trip cannot be canceled
    return false;
}

function getTripChangeDetails($conditions){
    if(is_null($conditions)){
        return false;
    }
    if(array_key_exists("change_before_departure", $conditions)){
        if($conditions["change_before_departure"] == null){
            return false;
        }
        if(array_key_exists("allowed", $conditions["change_before_departure"])){
            if($conditions["change_before_departure"]["allowed"] == true){
                // refund is allowed
                return [
                    "currency" => $conditions["change_before_departure"]["penalty_currency"],
                    "amount" => $conditions["change_before_departure"]["penalty_amount"]
                ];
            }
        }
    }
    // the trip cannot be canceled
    return false;
}

function searchUpgrades(array $availableUpgrades, $selectedItems, $typeToMatch) {
    $matchingUpgrades = [];

    // Loop through each selected item
    if($selectedItems == null){
        return [];
    }
    foreach ($selectedItems as $item) {
        // Make sure the 'id' key exists to prevent errors
        $itemId = isset($item['id']) ? $item['id'] : null;

        // Search for matching upgrades in the availableUpgrades array
        foreach ($availableUpgrades as $upgrade) {
            if ($upgrade['type'] === $typeToMatch && isset($upgrade['id']) && $upgrade['id'] === $itemId) {
                $price = $upgrade["total_amount"];
                $newPrice = round( baseMarkUpNonZero($price * getMarkupMultiplier($upgrade["type"] , $price)) ,0);
                $upgrade["newPrice"] = $newPrice;
                $matchingUpgrades[] = $upgrade;
                // get the markup value here

                break; // Break out of the inner loop once a match is found
            }
        }
    }

    return $matchingUpgrades;
}

function findRateById($rooms, $id) {
    foreach ($rooms as $room) {
        // If the current value is an array, recurse into it
        foreach($room["rates"] as $rate){
            // If the current key is 'id' and it matches the provided id, return the rate
            if($rate["id"] == $id){
                return $rate;
            }
        }
    }
    
    // Return null if nothing is found
    return null;
}

function compareSegmentTimes($data) {
    // Initialize an array to hold the comparison results
    $results = [];

    // Retrieve the 'removed' and 'added' segments from the changes
    $removedSegments = $data['changes']['removed'];
    $addedSegments = $data['changes']['added'];

    // Iterate over the removed segments
    foreach ($removedSegments as $index => $removed) {
        // Extract the corresponding added segment using the same index
        $added = $addedSegments[$index] ?? null;

        // Ensure there is a corresponding added segment
        if ($added) {
            // Compare each corresponding segment
            foreach ($removed['segments'] as $segIndex => $removedSegment) {
                $addedSegment = $added['segments'][$segIndex] ?? null;

                // Ensure the added segment exists
                if ($addedSegment) {
                    // Check for the existence of 'departureDatetime' and 'arrivalDatetime'
                    $removedDepTime =  $removedSegment['departure_datetime'] ?? $removedSegment['departureDatetime'];
                    $addedDepTime = $addedSegment['departure_datetime'] ?? $addedSegment['departureDatetime'];
                    $removedArrTime = $removedSegment['arrival_datetime'] ?? $removedSegment['arrivalDatetime'];
                    $addedArrTime = $addedSegment['arrival_datetime'] ?? $addedSegment['arrivalDatetime'];

                    // Check if there is a change in the departure or arrival datetime
                    $departureTimeChanged = ($removedDepTime && $addedDepTime) && ($removedDepTime !== $addedDepTime);
                    $arrivalTimeChanged = ($removedArrTime && $addedArrTime) && ($removedArrTime !== $addedArrTime);

                    // If there is a change, add it to the results
                    if ($departureTimeChanged || $arrivalTimeChanged) {
                        // Create segment label from origin to destination city names

                        $segmentLabel = ($removedSegment['origin']['city_name'] ?? '') . " to " . ($removedSegment['destination']['city_name'] ?? '');

                        $results[] = [
                            'segment_label' => $segmentLabel,
                            'removed_segment_id' => $removedSegment['id'],
                            'added_segment_id' => $addedSegment['id'],
                            'removed_departure_datetime' => $removedDepTime,
                            'added_departure_datetime' => $addedDepTime,
                            'removed_arrival_datetime' => $removedArrTime,
                            'added_arrival_datetime' => $addedArrTime,
                            'departure_time_changed' => $departureTimeChanged,
                            'arrival_time_changed' => $arrivalTimeChanged,
                        ];
                    }
                }
            }
        }
    }

    return $results;
}

function isBankBlacklisted($routing){
    $blocked = ["324173626"];

    if(in_array($routing, $blocked)){
        return true;
    }
    return false;
}