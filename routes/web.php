<?php

use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function(){
    return view('offline');
});

// Add this catch-all route at the end to redirect everything else to home
Route::any('{any}', function(){
    return redirect('/');
})->where('any', '.*');

/*

Route::get('/', [App\Http\Controllers\BusinessController::class, 'index'])->name('boarding-index');


Route::get('/s/{referral}', [App\Http\Controllers\PublicController::class, 'referral']);
Route::get('/20-countries', [App\Http\Controllers\PublicController::class, 'twentyCountries']);
Route::get('/20countries', [App\Http\Controllers\PublicController::class, 'twentyCountries']);


Route::get('demo', function(){
    return Redirect::to('https://youtu.be/MCgbZYyK3PQ?si=4D3qYen7vozZHGr1');
});


require __DIR__.'/auth.php';

Auth::routes();


Route::get('/download', function(){
    return view('auth.download');
});

Route::get('/privacy/deletion', function () {
    
    $data = [
        "title" => "Venti Delete Account"
    ];

    return view('privacy', $data);
});

Route::get('/privacy/do-not-sell', function () {
    
    $data = [
        "title" => "Venti Do Not Sell Info"
    ];

    return view('contact', $data);
});

Route::get('/fb', function () {
    dd(Session::get('fb'));
});

Route::get('/verified', function () {
    return view('verified');
});

Route::get('/privacy', function () {
    $data = [
        "title" => "Venti Privacy Policy"
    ];

    return view('privacy', $data);
});

Route::get('/contest', function(){
    return Redirect::to("https://form.typeform.com/to/oboDXYCt");
});

Route::get('/feedback', function(){
    return view('feedback');
});

Route::get('/contest/terms', function () {
    return view('contest');
});

Route::get('/policy', function () {

     $data = [
        "title" => "Venti Privacy Policy"
    ];

    return view('privacy', $data);
});

Route::get('/terms', function () {

    $data = [
        "title" => "Venti Terms & Conditions"
    ];

    return view('privacy', $data);
});

Route::get('/insurance', function(){
    $data = [
        "title" => "Travel Insurance"
    ];

    return view('insurance', $data);
});

Route::get('/sweepstakes', function(){
    $data = [
        "title" => "Venti Sweepstakes",
        "description" => "Venti members can win free flights, hotel room nights, and paid for vacations simply by building up their savings on our platform."
    ];

    return view('sweepstakes.index', $data);
});

Route::get('/raffle', function(){
    $data = [
        "title" => "Venti Sweepstakes"
    ];

    return view('sweepstakes.index', $data);
});

Route::get('/schedule', function () {
    return Redirect::to('https://calendly.com/markus-venti/chat-about-venti-50?month=2022-03');
});

Route::get('/success/waitlist', function () {
    return view('layouts.success', ["goal" => "waitlisted"]);
});

Route::get('/success/gold', function () {
    return view('layouts.success', ["goal" => "gold"]);
});

Route::get('/builder', function () {
    return view('builder.index');
});

Route::get('/success/black', function () {
    return view('layouts.success', ["goal" => "black"]);
});

Route::get('/success', function () {
    return view('layouts.success', ["goal" => "complete"]);
});

Route::get('/waitlisted', function () {
    return view('waitlisted');
});

Route::get('/contact', [App\Http\Controllers\FormController::class, 'contact']);
Route::get('/contact/delete', [App\Http\Controllers\FormController::class, 'contactDelete']);
Route::post('/contact', [App\Http\Controllers\FormController::class, 'submitFeedback']);
Route::post('/contact/delete', [App\Http\Controllers\FormController::class, 'submitDelete']);

Route::get('/survey',function(){
    return Redirect::to('https://forms.microsoft.com/r/Nn9kW5GKUW');
});

Route::get('/itinerary/{tripID}/{userID}/{stampID}/{month?}/{day?}/{year?}', [App\Http\Controllers\ItineraryController::class, 'secureView']);


Route::post('/validate/username', [App\Http\Controllers\FormController::class, 'validateUsername'])->name('validate-username');
Route::post('/validate/email', [App\Http\Controllers\FormController::class, 'validateEmail'])->name('validate-email');

Route::get('/place/{placeName}', [App\Http\Controllers\RecommendationController::class, 'googlePlaces']);
Route::get('/map/{origin}/{destination}', [App\Http\Controllers\RecommendationController::class, 'googleMaps']);

include("about.php");

include("navigator.php");

include("tailwind.php");

include("webhooks.php");

include("flights.php");

include("hotels.php");

include("create.php");

include("dashboard.php");

include("shop.php");

include("test.php");

include("user.php");

include("trips.php");

include("admin.php");

include("facebook.php");

include("boarding.php");

include("home.php");

include("jetbridge.php");

include('schools.php');

include("business.php");

include("pro.php");

include("crm.php");

include("airlock.php");


*/

