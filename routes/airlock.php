<?php

Route::prefix('airlock')->group(function () {
    Route::get('/', [App\Http\Controllers\AirlockController::class, 'index'])->name('airlock-landing');
    Route::get('/register', [App\Http\Controllers\AirlockController::class, 'registerPage'])->name('airlock-register');
    Route::get('/login', [App\Http\Controllers\AirlockController::class, 'loginPage'])->name('airlock-login');
    Route::get('/home', [App\Http\Controllers\AirlockController::class, 'home'])->name('airlock-home');
    
    // POST Requests
    Route::post('/register', [App\Http\Controllers\AirlockController::class, 'register']);
    Route::post('/login', [App\Http\Controllers\AirlockController::class, 'login']);

    Route::prefix('onboarding')->group(function () {
        Route::get('/email', [App\Http\Controllers\AirlockController::class, 'onboardingEmail'])->name('airlock-onboarding-email');
    });


    Route::prefix('claim')->group(function () {
        Route::get('/', [App\Http\Controllers\AirlockController::class, 'showClaimPage']);
        Route::post('/', [App\Http\Controllers\AirlockController::class, 'submitClaim']);
    });

    // Plans

    Route::prefix('plans')->group(function () {
        Route::get('/', [App\Http\Controllers\AirlockController::class, 'showPlans']);
        Route::get('/check-payment-status', [App\Http\Controllers\AirlockController::class, 'checkPaymentStatus']);
        Route::post('/process', [App\Http\Controllers\AirlockController::class, 'processPlanSelection']);
        Route::get('/payment/success', [App\Http\Controllers\AirlockController::class, 'handlePaymentSuccess']);
        Route::get('/success', [App\Http\Controllers\AirlockController::class, 'showSuccessPage']);
    });

    Route::prefix('plan')->group(function () {
        Route::get('/{id}', [App\Http\Controllers\AirlockController::class, 'showPlanDetails']);
        Route::get('/{id}/receipt', [App\Http\Controllers\AirlockController::class, 'downloadReceipt']);
    });
});