<?php


Route::prefix('test')->group(function () {
	Route::prefix('email')->group(function () {
		Route::get('/preview', [App\Http\Controllers\TestController::class, 'preview']);
		Route::get('/previewHotelBooking', [App\Http\Controllers\TestController::class, 'previewHotelBooking']);

		Route::get('/previewTrackerEmail', [App\Http\Controllers\TestController::class, 'previewTrackerEmail']);
		
		
		Route::get('/dobErrors', [App\Http\Controllers\TestController::class, 'dobErrors']);
		Route::get('/reverify', [App\Http\Controllers\TestController::class, 'sendCustomEmailVerficationLink']);
	});

	Route::prefix('sync')->group(function () {
		Route::get('/', [App\Http\Controllers\TestController::class, 'dbSync']);
		Route::get('/octopus', [App\Http\Controllers\TestController::class, 'octopusSync']);  
	});

	Route::prefix('pushNotification')->group(function () {
		Route::get('/', [App\Http\Controllers\TestController::class, 'pushNotification']);
	});

	Route::prefix('/scrape')->group(function() {
		Route::get('/instagram', [App\Http\Controllers\TestController::class, 'instagram']);
	});

	Route::prefix('/facebook')->group(function() {
		Route::get('/conversion', [App\Http\Controllers\TestController::class, 'instagram']);
	});

	Route::prefix('recommendations')->group(function () {
		Route::get('/', [App\Http\Controllers\TestController::class, 'sendGroupRecommendation']);
		Route::get('/clean', [App\Http\Controllers\TestController::class, 'cleanRecommendations']);
	});

	Route::prefix("user")->group(function(){
		Route::get('/updateAvatar', [App\Http\Controllers\TestController::class, 'updateAvatar']);
		Route::get('manualSMSVerify',[App\Http\Controllers\TestController::class, 'manualSMSVerify']);
		Route::get('/testRecommendationEmail', [App\Http\Controllers\TestController::class, 'createFakeRecommendation']);
		Route::get('/nudgeToComplete',[App\Http\Controllers\TestController::class, 'nudgeToComplete']);
	});

	Route::prefix("plans")->group(function(){
		Route::get('/remove/{planID}', [App\Http\Controllers\TestController::class, 'deletePlan']);
	});

	Route::get("stats", [App\Http\Controllers\TestController::class, 'statistics']);

	Route::prefix("duffel")->group(function(){
		Route::get('/createlink', [App\Http\Controllers\TestController::class, 'duffelCreateLink']);
	});

	Route::get('/top100', [App\Http\Controllers\BoardingPassController::class, 'top100']);
});