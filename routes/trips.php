<?php

Route::prefix('trips')->group(function () {
	Route::get('/', [App\Http\Controllers\TripsController::class, 'index'])->name('trips');
	Route::get('/{order}', [App\Http\Controllers\TripsController::class, 'tripDetail'])->name('trip-detail');
	Route::get('/{order}/invoice', [App\Http\Controllers\TripsController::class, 'tripInvoice'])->name('trip-invoice');

	Route::post('/receipt', [App\Http\Controllers\TripsController::class, 'sendReceipt'])->name('trip-resend-receipt');

	Route::post('/services', [App\Http\Controllers\TripsController::class, 'getTripAvailableServices'])->name('trip-services');

	Route::post('/support', [App\Http\Controllers\TripsController::class, 'submitSupportRequest'])->name('trip-support');

	Route::post('/cancel', [App\Http\Controllers\TripsController::class, 'cancelTrip'])->name('cancel-trip');

	Route::prefix('tracker')->group(function () {
		Route::get('/view/{id}', [App\Http\Controllers\TripsController::class, 'getTracker']);
		Route::post('/view/{id}', [App\Http\Controllers\TripsController::class, 'editTracker']);
		Route::post('/', [App\Http\Controllers\TripsController::class, 'createTracker'])->name('trip-create-tracker');
		Route::post('/status', [App\Http\Controllers\TripsController::class, 'updateTrackerStatus'])->name('trip-update-status');
	});
});