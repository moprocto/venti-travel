<?php

Route::prefix('connect')->group(function () {
	Route::get('/', [App\Http\Controllers\ConnectController::class, 'index'])->name('browse-connections');

});

Route::prefix('u')->group(function () {
	Route::get('/{username}', [App\Http\Controllers\ConnectController::class, 'read'])->name('read-connection');
});