<?php

Route::prefix('/tailwind')->group(function () {
	Route::get('/', [App\Http\Controllers\TailwindController::class, 'index'])->name('tailwind-index');
	Route::post('/', [App\Http\Controllers\TailwindController::class, 'index'])->name('tailwind-join-waitlist');
});