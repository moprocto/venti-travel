<?php

/*

BREAD
	Browse
	Read
	Edit
	Add
	Delete

CRUMB
	Browse => 
	Read => Report
	Add => Create
	Edit => Update
	Delete => Recycle
*/

Route::prefix('dashboard')->group(function () {
	Route::get('/', [App\Http\Controllers\DashboardController::class, 'home'])->name('dashboard-home');

	Route::prefix('catalog')->group(function () {
		Route::get('/', [App\Http\Controllers\DashboardController::class, 'catalog'])->name('dashboard-catalog');
		Route::get('/read/{trip}', [App\Http\Controllers\DashboardController::class, 'read'])->name('dashboard-catalog-read');

		Route::get('/preview/{trip}', [App\Http\Controllers\ItineraryController::class, 'preview'])->name('dashboard-catalog-preview');

		Route::get('/create', [App\Http\Controllers\DashboardController::class, 'create'])->name('dashboard-catalog-create')->middleware('superadmin');

		Route::post('/add', [App\Http\Controllers\DashboardController::class, 'add'])->middleware('superadmin');
		Route::post('/edit', [App\Http\Controllers\DashboardController::class, 'edit'])->middleware('superadmin');
	});

	include("clients.php");

	include("profile.php");
	
	Route::get('/reports/{type}/{id}', [App\Http\Controllers\DashboardController::class, 'report'])->middleware('superadmin');
	Route::get('/stripe/transaction', [App\Http\Controllers\DashboardController::class, 'getStripeTransaction'])->middleware('superadmin');
});
