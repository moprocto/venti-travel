<?php

Route::prefix('/login/facebook')->group(function () {
	Route::get('/', [App\Http\Controllers\FirebaseController::class, 'facebookLogin'])->name('facebook-login');
	Route::get('/redirect', [App\Http\Controllers\FirebaseController::class, 'handleFacebookRedirect'])->name('facebook-redirect');
	Route::get('/callback', [App\Http\Controllers\FirebaseController::class, 'handleFacebookCallback'])->name('facebook-callback');
});