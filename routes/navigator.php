<?php

Route::prefix('navigator')->group(function () {
	// GETS
	// Anyone can access this route
	Route::get('/', [App\Http\Controllers\PublicController::class, 'navigatorProgram']);

	// Venti account required for these routes
	Route::get('/schedule', [App\Http\Controllers\NavigatorController::class, 'schedule']);
	Route::get('/agreement', [App\Http\Controllers\NavigatorController::class, 'agreement']);

	// Navigator certifiction required for these routes
	Route::middleware(['navigator'])->group(function () {
		Route::get('/home', [App\Http\Controllers\NavigatorController::class, 'home'])->name('navigator-home');
		
		Route::prefix('tools')->group(function () {
			Route::get('planner', [App\Http\Controllers\NavigatorController::class, 'viewPlanner'])->name('navigator-view-planner');
			Route::post('planner', [App\Http\Controllers\NavigatorController::class, 'savePlanner'])->name('navigator-save-planner');
			// reserved route for chatGPT integration
			Route::post('plannerGPT', [App\Http\Controllers\NavigatorController::class, 'plannerGPT'])->name('navigator-plannerGPT');
		});
	});
	

	// POSTS
	// Anyone can access these routes
	Route::post('/', [App\Http\Controllers\FormController::class, 'apply']);
	Route::post('/agreement', [App\Http\Controllers\NavigatorController::class, 'navigatorAgree']);

	Route::prefix('pro')->group(function () {
		Route::get('/', [App\Http\Controllers\ProController::class, 'index'])->name('pro-index');
		Route::get('/manage', [App\Http\Controllers\ProController::class, 'index'])->name('pro-management');
	});
});

