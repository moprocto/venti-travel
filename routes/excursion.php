<?php

Route::prefix('excursions')->group(function () {
	// GETS
	// Anyone can access this route
	Route::get('/', [App\Http\Controllers\PublicController::class, 'excursionProgram']);
	Route::post('/', [App\Http\Controllers\FormController::class, 'excursionProgramApply']);
});

