<?php

Route::prefix('home')->group(function () {
	Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
	
	Route::get('/change/{subscription}', [App\Http\Controllers\BoardingPassController::class, 'changeSubscription'])->name('change-subscription');

	Route::prefix('transactions')->group(function () {
		Route::post('/', [App\Http\Controllers\HomeController::class, 'transactionsTable'])->name('transactions-table');
		Route::post('/snapshots', [App\Http\Controllers\HomeController::class, 'snapshotsTable'])->name('snapshots-table');
		Route::post('/cancel/request', [App\Http\Controllers\WalletController::class, 'cancelRequest'])->name('transaction-cancel-request');
		Route::post('/cancel/confirm', [App\Http\Controllers\WalletController::class, 'cancelConfirm'])->name('transaction-cancel-confirm');
		Route::post('/purchase/points', [App\Http\Controllers\WalletController::class, 'purchasePoints'])->name('purchase-points');
	});

	Route::prefix('customer')->group(function () {
		Route::post('/details', [App\Http\Controllers\WalletController::class, 'customerDetails'])->name('home-customer-details');
		
		Route::post('/share', [App\Http\Controllers\HomeController::class, 'customerShare'])->name('generate-share-data');
	});

	Route::prefix('statements')->group(function () {
		Route::get('/snapshot/{id}', [App\Http\Controllers\HomeController::class, 'viewStatement'])->name('home');
		Route::get('/tax/{year}', [App\Http\Controllers\HomeController::class, 'viewTaxStatement'])->name('home');
	});

	Route::prefix('upgrade')->group(function () {
		Route::get('/{subscription}', [App\Http\Controllers\HomeController::class, 'viewUpgrade'])->name('home');
		Route::post('/', [App\Http\Controllers\HomeController::class, 'upgradeSubscription'])->name('home');
	});	
});