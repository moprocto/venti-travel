<?php

Route::prefix('/hotels')->group(function () {
	Route::get('/', [App\Http\Controllers\HotelsController::class, 'index'])->name('hotels');
	Route::prefix('/search')->group(function () {
		Route::get('/', [App\Http\Controllers\HotelsController::class, 'searchResults']);
		Route::post('/results', [App\Http\Controllers\HotelsController::class, 'search']);
		Route::get('/result/{id}/offer/{hotel}', [App\Http\Controllers\HotelsController::class, 'getHotel']);
		Route::post('/result', [App\Http\Controllers\HotelsController::class, 'getHotelAjax']);

		Route::get('/result/{id}/checkout/{rate}', [App\Http\Controllers\HotelsController::class, 'getCheckout']);
		Route::post('/result/{id}/checkout/{rate}', [App\Http\Controllers\HotelsController::class, 'submitOrder']);
		Route::post('/result/verify', [App\Http\Controllers\HotelsController::class, 'verifyHotel']);
		Route::post('/update/selection', [App\Http\Controllers\HotelsController::class, 'updateSelection']);
	});
});