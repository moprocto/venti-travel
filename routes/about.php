<?php

Route::prefix('/about')->group(function () {
    Route::get('/', function () {
        $data = [
            "title" => "About Venti"
        ];

        return view('about.index', $data);
    });

    Route::get('/giving-tuesday', function () {
        $data = [
            "title" => "About Venti Giving Tuesday"
        ];

        return view('about.giving-tuesday', $data);
    });

    Route::get('/points', function () {

        $data = [
            "title" => "About Venti Boarding Pass Points"
        ];

        return view('about.points', $data);
    });

    Route::get('/price-match', function () {

        $data = [
            "title" => "About Venti Price Matching"
        ];

        return view('about.price-match', $data);
    });

    Route::get('/airlock', function () {

        $data = [
            "title" => "About Venti Airlock"
        ];

        return view('about.airlock', $data);
    });

    Route::get('/mysterybox', function () {

        $data = [
            "title" => "About Venti Mystery Box"
        ];

        return view('about.mystery', $data);
    });

    Route::get('/pro', function () {

        $data = [
            "title" => "About Pro"
        ];

        return view('about.pro', $data);
    });
});