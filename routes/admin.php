<?php

Route::prefix('admin')->group(function () {
	Route::get('/', [App\Http\Controllers\AdminController::class, 'index'])->name('admin-home');
	Route::get('/winner', [App\Http\Controllers\AdminController::class, 'winner'])->name('admin-winner');
	Route::prefix('transactions')->group(function () {
		Route::get('/',[App\Http\Controllers\AdminController::class, 'getTransactions']);
		Route::get('/create',[App\Http\Controllers\AdminController::class, 'getTransactScreen']);
		Route::get('/autosaves',[App\Http\Controllers\AdminController::class, 'getAutosavesScreen']);

		Route::prefix('orders')->group(function () {
			Route::get('/',[App\Http\Controllers\AdminController::class, 'getOrders']);
			Route::get('/refund',[App\Http\Controllers\AdminController::class, 'getRefund']);
			Route::post('/refund',[App\Http\Controllers\AdminController::class, 'processRefund'])->name('admin-process-refund');
		});

		Route::prefix('statements')->group(function () {
			Route::get('/',[App\Http\Controllers\AdminController::class, 'getStatements']);
			Route::get('/{id}',[App\Http\Controllers\AdminController::class, 'getStatement']);
			Route::post('/{id}',[App\Http\Controllers\AdminController::class, 'updateStatement']);
		});

		Route::get('/mysterybox',[App\Http\Controllers\AdminController::class, 'mysteryboxOpens']);
	});

	Route::get('/snapshots',[App\Http\Controllers\AdminController::class, 'getSnapshots']);
	Route::get('/snapshots/delete/{id}',[App\Http\Controllers\AdminController::class, 'deleteSnapshot']);
	Route::get('/transactions/delete/{id}',[App\Http\Controllers\AdminController::class, 'deleteTransaction']);
	Route::get('/sources',[App\Http\Controllers\AdminController::class, 'getFundingSources']);
	Route::get('/users',[App\Http\Controllers\AdminController::class, 'users']);
	Route::get('/searches',[App\Http\Controllers\AdminController::class, 'flightSearches']);

	Route::post('/transact',[App\Http\Controllers\AdminController::class, 'postTransact'])->name('admin-transact');

	Route::post('/profile',[App\Http\Controllers\AdminController::class, 'getProfile']);
	Route::post('/profile/dwolla',[App\Http\Controllers\AdminController::class, 'getDwolla']);

	Route::get('/sendBoardingPassEmails', [App\Http\Controllers\AdminController::class, 'sendBoardingPassEmails'])->name('admin-sendBoardingPassEmails');
	Route::get('/mailchimpStatus/{email}',[App\Http\Controllers\AdminController::class, 'mailchimpStatus']);

	Route::prefix('functions')->group(function () {
		Route::get('/resetPhotoUrl/{uid}',[App\Http\Controllers\AdminController::class, 'resetPhotoUrl']);
		Route::get('/sendVerificationEmail/{email}',[App\Http\Controllers\AdminController::class, 'sendVerificationEmail']);
		Route::get('/scoresheet/{match}/{matchee}',[App\Http\Controllers\AdminController::class, 'scoresheet']);
	});

	Route::prefix('user')->group(function () {
		Route::get('/{uid}',[App\Http\Controllers\AdminController::class, 'user']);
		Route::get('/{uid}/login',[App\Http\Controllers\AdminController::class, 'loginAs']);
	});

	Route::prefix('boardingpass')->group(function () {
		Route::get('/',[App\Http\Controllers\Admin\BoardingAdminController::class, 'index']);
	});
	
	Route::prefix('data')->group(function () {
		Route::prefix('export')->group(function () {
			Route::get('/users',[App\Http\Controllers\AdminController::class, 'exportUsers']);
			Route::get('/customers',[App\Http\Controllers\AdminController::class, 'exportCustomers']);
			Route::get('/banks',[App\Http\Controllers\AdminController::class, 'exportBanks']);
			Route::get('/cards',[App\Http\Controllers\AdminController::class, 'exportCards']);
			Route::get('/transactions',[App\Http\Controllers\AdminController::class, 'exportTransactions']);
			Route::get('/orders',[App\Http\Controllers\AdminController::class, 'exportOrders']);
			Route::get('/mysterybox',[App\Http\Controllers\AdminController::class, 'exportMysterbox']);
			Route::get('/snapshots',[App\Http\Controllers\AdminController::class, 'exportBalanceSnapshots']);
			Route::get('/autosaves',[App\Http\Controllers\AdminController::class, 'exportAutosaves']);
			Route::get('/flight-searches',[App\Http\Controllers\AdminController::class, 'exportFlightSearch']);
			Route::get('/hotel-searches',[App\Http\Controllers\AdminController::class, 'exportHotelSearch']);
		});
		Route::get('/import-flight-searches',[App\Http\Controllers\AdminController::class, 'importFlightSearch']);
	});

	Route::get('/name',[App\Http\Controllers\AdminController::class, 'updateName']);
	
});