<?php

Route::prefix('/flights')->group(function () {
	Route::get('/', [App\Http\Controllers\FlightsController::class, 'index'])->name('flights');
	Route::get('/data/airports', [App\Http\Controllers\FlightsController::class, 'airports'])->name('flights-airports');

	Route::get('/data/airports/nearby', [App\Http\Controllers\FlightsController::class, ''])->name('flights-airports-nearby');

	Route::prefix('/search')->group(function () {
		Route::get('/',[App\Http\Controllers\FlightsController::class, 'searchResults'])->name('flights-search');
		
		Route::post('/update/selection',[App\Http\Controllers\FlightsController::class, 'updateSearchSelection'])->name('flights-search-update');
		
		Route::post('/results',[App\Http\Controllers\FlightsController::class, 'search'])->name('flights-search');

		Route::prefix('/offer')->group(function () {
			Route::post('/status',[App\Http\Controllers\FlightsController::class, 'offerStatus'])->name('flight-offer-status');

			Route::prefix('/order')->group(function () {
				Route::post('/',[App\Http\Controllers\FlightsController::class, 'submitOrder'])->name('flight-order');
				Route::post('/services',[App\Http\Controllers\FlightsController::class, 'submitOrder'])->name('flight-order-services');
				Route::post('/status',[App\Http\Controllers\FlightsController::class, 'orderStatus'])->name('flight-order-status');

				// produces the HTML for checkout
				Route::post('/preview',[App\Http\Controllers\FlightsController::class, 'orderPreview'])->name('flight-order-preview');
			});


			

			Route::get('/{offer}/{iata_destination}/checkout',[App\Http\Controllers\FlightsController::class, 'checkout'])->name('flight-checkout');

			Route::post('/pax',[App\Http\Controllers\FlightsController::class, 'updateOfferPax'])->name('flight-update-offer-pax');

			Route::get('/{search}/{offer}/{return}/{iata_destination}/{type}/{amount}',[App\Http\Controllers\FlightsController::class, 'offer'])->name('flight-offer');
			Route::post('/seats',[App\Http\Controllers\FlightsController::class, 'seatMap'])->name('flight-seatmap');
		});
	});
});