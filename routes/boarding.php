<?php

Route::prefix('boardingpass')->group(function () {
    Route::get('/', [App\Http\Controllers\BoardingPassController::class, 'index'])->name('boarding-index');

    Route::post('/register', [App\Http\Controllers\BoardingPassController::class, 'register'])->name('boarding-register');

    Route::post('/success', [App\Http\Controllers\BoardingPassController::class, 'success'])->name('boarding-success');

    Route::prefix('onboarding')->group(function () {
        Route::get('/continue', [App\Http\Controllers\BoardingPassController::class, 'continueCheck']);
        Route::get('/email', [App\Http\Controllers\BoardingPassController::class, 'onboardingEmail']);
        Route::get('/phone', [App\Http\Controllers\BoardingPassController::class, 'onboardingPhone']);
        Route::get('/bank', [App\Http\Controllers\BoardingPassController::class, 'onboardingBank']);
    });

    Route::prefix('bank')->group(function () {
        Route::get('{id}', [App\Http\Controllers\PlaidController::class, 'getBank']);

    });


    Route::prefix('customer')->group(function () {
        Route::post('/stripe', [App\Http\Controllers\BoardingPassController::class, 'createCustomerPaymentIntent'])->name('boarding-create-customer-intent');

        Route::prefix('status')->group(function () {
            Route::post('/', [App\Http\Controllers\BoardingPassController::class, 'checkCustomerStatus'])->name('boarding-status-customer');
            Route::post('/marketing', [App\Http\Controllers\BoardingPassController::class, 'getMarketingStatus'])->name('boarding-status-customer-marketing');
            Route::post('/marketing/tags', [App\Http\Controllers\BoardingPassController::class, 'updateMarketingStatus'])->name('boarding-status-customer-marketing-update');
        });

        Route::post('/upload-document', [App\Http\Controllers\BoardingPassController::class, 'uploadCustomerDocument'])->name('boarding-status-customer-upload-document');

        Route::prefix('fundingSource')->group(function () {

            Route::post('/', [App\Http\Controllers\BoardingPassController::class, 'createFundingSource'])->name('boarding-create-funding-source');

            Route::post('/remove', [App\Http\Controllers\BoardingPassController::class, 'removeFundingSource'])->name('boarding-remove-funding-source');

            Route::prefix('bank')->group(function () {
                Route::post('/detail', [App\Http\Controllers\BoardingPassController::class, 'bankDetails'])->name('bank-details');
                Route::post('/verify', [App\Http\Controllers\BoardingPassController::class, 'verifyBank'])->name('boarding-verify-bank');
            });
            Route::prefix('card')->group(function () {
                Route::post('/list', [App\Http\Controllers\BoardingPassController::class, 'getCards'])->name('boarding-get-cards');
                Route::post('/save', [App\Http\Controllers\BoardingPassController::class, 'saveCard'])->name('boarding-save-card');
                Route::post('/delete', [App\Http\Controllers\BoardingPassController::class, 'deleteCard'])->name('boarding-delete-card');
            });
        });

        Route::post('/balances', [App\Http\Controllers\WalletController::class, 'getBalances'])->name('wallet-get-balances');
        Route::post('/balances/points', [App\Http\Controllers\WalletController::class, 'getPoints'])->name('wallet-get-points-balance');

        Route::post('/reset-password', [App\Http\Controllers\FirebaseController::class, 'requestResetPassword'])->name('boarding-reset-password');

        Route::post('/', [App\Http\Controllers\BoardingPassController::class, 'createCustomer'])->name('boarding-create-customer');

        Route::post('/update', [App\Http\Controllers\BoardingPassController::class, 'updateCustomer'])->name('boarding-update-customer');
    });

    Route::post('/withdraw', [App\Http\Controllers\BoardingPassController::class, 'withdrawToBank'])->name('withdraw-to-bank');

    Route::prefix('deposit')->group(function () {
        Route::post('/', [App\Http\Controllers\BoardingPassController::class, 'depositToBoardingPass'])->name('deposit-to-boarding-pass');

        Route::prefix('recurring')->group(function () {
            Route::post('/', [App\Http\Controllers\BoardingPassController::class, 'recurringDepositToBoardingPass'])->name('recurring-deposit-to-boarding-pass');
            Route::post('/status', [App\Http\Controllers\BoardingPassController::class, 'recurringDepositStatus'])->name('recurring-deposit-status');
            Route::post('/delete', [App\Http\Controllers\BoardingPassController::class, 'recurringDepositDelete'])->name('recurring-deposit-delete');
        });
    });

    Route::get('/share', [App\Http\Controllers\BoardingPassController::class, 'share'])->name('boarding-share');
    Route::post('/share/points', [App\Http\Controllers\WalletController::class, 'sharePoints'])->name('share-points');

    Route::prefix('mystery')->group(function () {
        Route::get('/', [App\Http\Controllers\MysteryController::class, 'index'])->name('mystery-box-closed');
        Route::post('/open', [App\Http\Controllers\MysteryController::class, 'open'])->name('mystery-box-open');
        Route::post('/opens', [App\Http\Controllers\MysteryController::class, 'opens'])->name('mystery-box-opens');
    });

    
    Route::get('/track/{code}', [App\Http\Controllers\BoardingPassController::class, 'track'])->name('boarding-track');

    Route::get('/email/preview/{email}', [App\Http\Controllers\BoardingPassController::class, 'preview'])->name('boarding-preview');


});



Route::prefix('buddypass')->group(function () {
    Route::get('/', [App\Http\Controllers\BoardingPassController::class, 'buddyIndex'])->name('buddy-index');
});

Route::get('/auth/mfa', [App\Http\Controllers\BoardingPassController::class, 'authMFA'])->name('auth-mfa');