<?php

Route::prefix('creator')->group(function () {
	Route::get('/{clientID}', [App\Http\Controllers\ShopController::class, 'profile'])->name('creator-profile');
});