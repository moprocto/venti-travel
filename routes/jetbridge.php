<?php

Route::prefix('jetbridge')->group(function () {
	Route::get('/', [App\Http\Controllers\JetBridgeController::class, 'index'])->name('jetbridge-index');

	Route::post('/join', [App\Http\Controllers\JetBridgeController::class, 'success'])->name('jetbridge-join-waitlist');
});