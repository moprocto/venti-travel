<?php

Route::prefix('clients')->group(function () {
	Route::get('/', [App\Http\Controllers\ClientController::class, 'index'])->name('dashboard-clients');
	Route::get('/add', [App\Http\Controllers\ClientController::class, 'add'])->name('dashboard-clients-add');
	Route::get('/read/{client}', [App\Http\Controllers\ClientController::class, 'read'])->name('dashboard-clients-read');

	Route::post('/add', [App\Http\Controllers\ClientController::class, 'create'])->name('dashboard-clients-create');
	Route::post('/edit/{client}', [App\Http\Controllers\ClientController::class, 'read'])->name('dashboard-clients-edit');
});