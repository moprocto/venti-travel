<?php

Route::prefix('group')->group(function () {

	Route::prefix('{tripID}')->group(function () {
		Route::get('/', [App\Http\Controllers\GroupsController::class, 'readGroup'])->name('read-group');
		Route::post('/', [App\Http\Controllers\GroupsController::class, 'editGroup'])->name('edit-group');

		Route::get('/studio',[App\Http\Controllers\GroupsController::class, 'getStudio'])->name('group-studio');
		Route::post('/studio/refresh',[App\Http\Controllers\GroupsController::class, 'studioRefresh'])->name('studio-refresh');

		Route::get('/studio/pdf',[App\Http\Controllers\GroupsController::class, 'studioPDF'])->name('studio-pdf');

		Route::get('/join', [App\Http\Controllers\GroupsController::class, 'readGroupJoin'])->name('read-group-join');
		Route::get('/leave', [App\Http\Controllers\GroupsController::class, 'readGroupLeave'])->name('read-group-leave');
		Route::get('/archive', [App\Http\Controllers\GroupsController::class, 'archiveTrip'])->name('group-archive');
		Route::get('/republish', [App\Http\Controllers\GroupsController::class, 'republishTrip'])->name('group-republish');

		Route::post('/planner', [App\Http\Controllers\GroupsController::class, 'editPlanner'])->name('group-planner');

		Route::prefix('chat')->group(function () {
			Route::post('/token', [App\Http\Controllers\ChatController::class, 'token'])->name('chat-token');
			Route::post('/send', [App\Http\Controllers\ChatController::class, 'send'])->name('chat-send');
			Route::post('/pull', [App\Http\Controllers\ChatController::class, 'pull'])->name('chat-pull');
		});
	});
	
	Route::post('/members', [App\Http\Controllers\GroupsController::class, 'getMembers'])->name('group-members');
});

Route::prefix('groups')->group(function () {
	Route::get('/', [App\Http\Controllers\GroupsController::class, 'browse'])->name('browse-groups');
	
	
	Route::get('/new', [App\Http\Controllers\GroupsController::class, 'createGroup'])->name('create-group');

	Route::post('/new', [App\Http\Controllers\GroupsController::class, 'addGroup'])->name('add-group');
	Route::post('/invite', [App\Http\Controllers\GroupsController::class, 'groupInvite'])->name('group-invite');
	Route::post('/invite/reply', [App\Http\Controllers\GroupsController::class, 'groupInviteReply'])->name('group-invite-reply');

	
	Route::post('/remove/member', [App\Http\Controllers\GroupsController::class, 'groupRemoveMember'])->name('group-member-remove');
	Route::post('/{limit}/{cache}', [App\Http\Controllers\GroupsController::class, 'browseAJAX'])->name('groups-browse-ajax');
});