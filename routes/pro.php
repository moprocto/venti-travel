<?php

Route::prefix('pro')->group(function () {
	Route::get('/', [App\Http\Controllers\ProController::class, 'index'])->name('pro-home');
	Route::post('/list', [App\Http\Controllers\ProController::class, 'getBanks'])->name('list-plaid-banks');

	Route::prefix('plaid')->group(function () {
	    Route::get('/link', [App\Http\Controllers\PlaidController::class, 'createLinkToken']);
	    Route::post('/exchange-token', [App\Http\Controllers\PlaidController::class, 'exchangeToken']);
	    Route::post('/get-balance', [App\Http\Controllers\PlaidController::class, 'getBalance']);
	});

	Route::get('/link/bank', [App\Http\Controllers\PlaidController::class, 'index'])->name('link-new-bank');

	Route::prefix('accounts')->group(function () {
		Route::get('{vid}', [App\Http\Controllers\ProController::class, 'getAccount']);
		Route::post('/delete/bank', [App\Http\Controllers\ProController::class, 'deleteBank'])->name('delete-bank');
		Route::post('/delete/subaccount', [App\Http\Controllers\ProController::class, 'deleteSubaccount'])->name('delete-subaccount');
	});
});