<?php

Route::prefix('crm')->group(function () {
    Route::get('/', [App\Http\Controllers\CRMController::class, 'index'])->name('crm-index');
    Route::get('/mass', [App\Http\Controllers\CRMController::class, 'massMailer'])->name('crm-index-mass');
    Route::post('/email', [App\Http\Controllers\CRMController::class, 'sendEmail'])->name('crm-send-email');
    Route::post('/email/mass', [App\Http\Controllers\CRMController::class, 'massMailerSend'])->name('crm-send-mass');
});