<?php

Route::prefix('shop')->group(function () {
	Route::get('/', [App\Http\Controllers\ShopController::class, 'index'])->name('shop');
	Route::get('/product/{productID}/{filter?}', [App\Http\Controllers\ShopController::class, 'product']);
});


