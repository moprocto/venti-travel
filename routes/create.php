<?php

Route::prefix('create')->group(function () {
    Route::get('/', [App\Http\Controllers\CreateController::class, 'index'])->name('create-index');
    Route::post('/', [App\Http\Controllers\CreateController::class, 'create'])->name('create-plan');
});

Route::get('/plans/{planID}', [App\Http\Controllers\CreateController::class, 'read'])->name('plan-read');
Route::get('/plan/edit/{planID}', [App\Http\Controllers\CreateController::class, 'edit'])->name('plan-edit');
Route::post('/plan/update/{planID}', [App\Http\Controllers\CreateController::class, 'update'])->name('plan-update');
Route::get('/plan/pdf/{planID}', [App\Http\Controllers\CreateController::class, 'savePDF'])->name('plan-update');
