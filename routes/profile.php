<?php


Route::prefix('profile')->group(function () {
	Route::get('/{clientID}', [App\Http\Controllers\ProfileController::class, 'read'])->name('profile-read');
	Route::post('/timezone', [App\Http\Controllers\ProfileController::class, 'updateTimezone'])->name('update-timezone');
	Route::post('/{clientID}', [App\Http\Controllers\ProfileController::class, 'update'])->name('profile-update');


});