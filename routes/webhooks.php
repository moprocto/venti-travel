<?php

Route::prefix('/webhooks')->group(function () {
	Route::prefix('dwolla')->group(function () {
		Route::post('/', [App\Http\Controllers\WebhooksController::class, 'listenDwolla']);
		Route::prefix('subscriptions')->group(function () {
			Route::get('/create', [App\Http\Controllers\WebhooksController::class, 'createSubscription']);
			Route::get('/retrieve', [App\Http\Controllers\WebhooksController::class, 'retrieveSubscription']);
			Route::get('/list', [App\Http\Controllers\WebhooksController::class, 'listSubscription']);
			Route::get('/delete', [App\Http\Controllers\WebhooksController::class, 'deleteWebhook']);
		});
	});

	Route::prefix('stripe')->group(function () {
		Route::post('/', [App\Http\Controllers\WebhooksController::class, 'listenStripe']);
	});

	Route::prefix('duffel')->group(function () {
		Route::post('/', [App\Http\Controllers\WebhooksController::class, 'listenDuffel']);
	});
});