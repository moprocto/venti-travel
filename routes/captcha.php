<?php

Route::get('/refresh-captcha', [App\Http\Controllers\CaptchaController::class, 'refresh'])->name('refresh-captcha');

Route::prefix('captcha')->group(function () {

	

	Route::post('my-captcha', 'CaptchaController@submit')->name('post-captcha');
});