<?php
Route::get('/login', [App\Http\Controllers\FirebaseController::class, 'getLogin'])->name('login');
Route::get('/register', [App\Http\Controllers\FirebaseController::class, 'getRegister'])->name('register');
Route::post('/login', [App\Http\Controllers\FirebaseController::class, 'login'])->name('create-login');

Route::prefix('user')->group(function () {
	Route::get('/register', [App\Http\Controllers\FirebaseController::class, 'getRegister'])->name('add-user');
	Route::post('/register', [App\Http\Controllers\FirebaseController::class, 'register'])->name('create-user');

	

	Route::get('/logout', [App\Http\Controllers\FirebaseController::class, 'logout'])->name('logout');

	Route::post('/request-reset-password', [App\Http\Controllers\FirebaseController::class, 'requestResetPassword'])->name('request-reset-password');
	Route::post('/request-reset-password-auth', [App\Http\Controllers\FirebaseController::class, 'requestResetPasswordAuth'])->name('request-reset-password-auth');
	Route::post('/reset-password', [App\Http\Controllers\FirebaseController::class, 'resetPassword'])->name('reset-user-password');

	Route::prefix('profile')->group(function () {
		Route::get('/', [App\Http\Controllers\ProfileController::class, 'getProfile'])->name('user-profile');
		Route::get('/invites', [App\Http\Controllers\ProfileController::class, 'getInvitations'])->name('user-invitations');
		Route::get('/wishlist', [App\Http\Controllers\ProfileController::class, 'getWishlist'])->name('user-wishlist');
		Route::post('/wishlist', [App\Http\Controllers\ProfileController::class, 'updateWishlist'])->name('user-wishlist-update');

		
		Route::post('/wishlist/create', [App\Http\Controllers\ProfileController::class, 'createWishlist'])->name('user-wishlist-create');

		Route::post('/wishlist/remove', [App\Http\Controllers\ProfileController::class, 'removeWishlist'])->name('user-wishlist-remove');

		

		Route::get('/groups/json/{uid}', [App\Http\Controllers\ProfileController::class, 'getUserGroups'])->name('user-profile-groups');

		Route::post('/', [App\Http\Controllers\ProfileController::class, 'editProfile'])->name('edit-profile');
		Route::post('/verify-email', [App\Http\Controllers\ProfileController::class, 'verifyEmail'])->name('send-email-verification');
		Route::post('/verify-sms', [App\Http\Controllers\ProfileController::class, 'sendVerifySMS'])->name('send-sms-verification');
		Route::post('/verify-sms-code', [App\Http\Controllers\ProfileController::class, 'verifySMSCode'])->name('verify-sms-code');
		
		Route::post('/verify-sms-code-mfa', [App\Http\Controllers\ProfileController::class, 'verifySMSCodeMFA'])->name('verify-sms-code-mfa');

		Route::post('/verify-email-validation', [App\Http\Controllers\ProfileController::class, 'verifyEmailValidation'])->name('verify-email-validation');

		Route::post('/send-verification-email', [App\Http\Controllers\ProfileController::class, 'sendEmailValidation'])->name('send-verification-email');
	});

	Route::prefix('settings')->group(function () {
		Route::get('/', [App\Http\Controllers\ProfileController::class, 'getSettings'])->name('user-settings');
		Route::post('/', [App\Http\Controllers\ProfileController::class, 'updateSettings'])->name('user-settings-update');
	});
});