<?php

Route::get('/blog/comparing-venti-savings-to-travel-credit-cards', function () {
	$data = [
		"title" => "Comparing Venti High-Yield Savings to Travel Credit Cards"
	];
	
    return view("blog.comparing-venti-savings-to-travel-credit-cards", $data);
});

Route::get('/tools/apy-calculator', function () {
	$data = [
		"title" => "APY Calculator With Chart",
		"description" => "Calculate and visualze your earnings with precision using our APY Calculator. Perfect for savers and investors, get accurate Annual Percentage Yield comparisons for your investments and savings accounts. Try it now to optimize your financial planning!",
		"image" => "https://venti.co/assets/img/apy-calculator-example.png",
		"url" => "https://venti.co/tools/apy-calculator"
	];
	
    return view("tools.apy-calculator", $data);
});

Route::get('/tools/apr-calculator', function () {
	$data = [
		"title" => "APR Calculator With Chart",
		"description" => "Calculate and visualze APR with precision using our APR Calculator. Perfect for the money conscious, get accurate Annual Percentage Rate comparisons for your loans and other credit accounts. Try it now to optimize your financial planning!",
		"image" => "https://venti.co/assets/img/apy-calculator-example.png",
		"url" => "https://venti.co/tools/apr-calculator"
	];
	
    return view("tools.apr-calculator", $data);
});