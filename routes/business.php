<?php

    Route::prefix('business')->group(function () {
        Route::get('/', [App\Http\Controllers\BusinessController::class, 'index'])->name('business-index');
        Route::post('/business', [App\Http\Controllers\BusinessController::class, 'success'])->name('business-contact');
    });

    Route::prefix('enterprise')->group(function () {
        Route::get('/', [App\Http\Controllers\BusinessController::class, 'index'])->name('business-index');
    });

    Route::prefix('playground')->group(function () {
        Route::get('/', [App\Http\Controllers\BusinessController::class, 'playground'])->name('business-playground');
    });

