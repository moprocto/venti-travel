<?php

Route::prefix('schools')->group(function () {
	Route::get('/', [App\Http\Controllers\SchoolsController::class, 'index'])->name('schools-home');
	Route::post('/', [App\Http\Controllers\SchoolsController::class, 'success'])->name('schools-waitlist');
	Route::get('/stats', [App\Http\Controllers\SchoolsController::class, 'getSchoolStats'])->name('schools-stats');
});

