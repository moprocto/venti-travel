@extends('layouts.app')

@section('css')

@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8" style="margin-bottom: 20px">
            <div class="card">
                <div class="card-header">Functions</div>
                <div class="card-body">
                    <p>Admin Functions for Data Entry. </p>
                    <div class="row">
                        <div class="col-md-3">
                            <a href="/dashboard/create" class="btn btn-main btn-success">Create Trip</a>
                        </div>
                        <div class="col-md-3">
                            <a href="https://www.efultimatebreak.com/explore" target="_blank" class="btn btn-main btn-info">EF Ultimate Break</a>
                        </div>
                        <div class="col-md-3">
                            <a href="https://www.gadventures.com/search/" target="_blank" class="btn btn-main btn-info">G Adventures</a>
                        </div>
                        <div class="col-md-3">
                            <a href="https://www.intrepidtravel.com/us/top-destinations" target="_blank" class="btn btn-main btn-info">Intrepid</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @foreach($groups as $group)
    <?php
        $agenda = $group;
        unset($group["photos"]);
        unset($group["itinerary"]);
        unset($group["tags"]);

        $departure = Carbon\Carbon::createFromFormat('m-d-Y', str_replace("/","-",$group["departure"]))->timezone('America/New_York');
        $return = Carbon\Carbon::createFromFormat('m-d-Y', str_replace("/","-",$group["departure"]))->addDays($group["days"] - 1)->timezone('America/New_York');

        $schedule = null;
        $itineraryList = [];

        try{
                    $schedules = Carbon\CarbonPeriod::create($departure, $return);

                    foreach($schedules as $schedule){
                        array_push($itineraryList, $schedule->format("D, M j"));
                    }

                } catch(Exception $e){

                }

        $firstDay = $departure->format("m-d-Y");
        $key = 0;
    ?>
    <div class="row justify-content-center">
        <div class="col-md-8" style="margin-bottom: 20px">
            <div class="card">
                <div class="card-header">{{ $group["title"] }}</div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-4">
                            <a href="{{ $group['imageURL'] }}" target="_blank">
                                <img src="{{ $group['imageURL'] }}" style="width:100%">
                            </a>
                            <br><br>
                            <a href="/dashboard/edit/{{ $group['tripID'] }}" class="btn btn-primary" style="width:100%">Edit Trip</a>
                            <br><br>
                            <a href="" class="btn btn-warning" style="width:100%">Archive Trip</a>
                        </div>
                        <div class="col-md-8">
                            <pre>
                                {{ print_r($group) }}
                            </pre>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <h4>Itinerary</h4>
                        </div>
                        <div class="col-md-12" style="max-height:400px; overflow-y:scroll;">
                            <ul>
                                
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endforeach
</div>
@endsection