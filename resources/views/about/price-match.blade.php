@extends('layouts.curator-master')

@section('css')
    <style type="text/css">
        @media(max-width: 1368px ){
            .tb-full-width{
                width: 100%;
            }
        }
        #footer{
            margin-top: 100px;
        }
        .about-body{
            justify-content: center;
        }
        body{
        	background: #F2F2F6 !important
        }
        .hamburger-menu-list li a{
            color: white;
        }
        .mobile-nav-open .hamburger-menu-list li a{
            color: black;
        }
        .btn-white{
            color: black !important;
        }
        .mobile-nav-open #backdrop{
            margin-top: 0 !important;
        }
    </style>
@endsection

@section('content')
    <div id="backdrop" style="margin-top: -130px; width: 100%; z-index: 1; display: flex; align-items: center; min-height: 500px; background-position: center;  background-size: cover;  background-image:url('/assets/img/navigator-backdrop-t5.jpg'); background-color: #F2F2F6 !important;">
        <div class="gradient-wrap" style="background: transparent; width:100%;">
            <!--  HERO -->
            <div class="section-wrapper black position-relative">
                <div class="row" style="align-items:center;">
                    <div class="col-md-5 col-sm-12 col-12 top-header" style="display: flex; align-items:center;">
                        <div id="hero-text-wrapper" class="mt-4" style="max-width:900px; margin-top:25px;">
                        	<br><br><br>
                            <h1 class="mb-3 section-title" style="line-height: 3rem; color: white; font-weight:600 !important; font-size: 3.8rem;">
                            About</sup>
                            <span style="font-size: 0.45em; display:block;">Price Matching</span>
                            </h1>
                            <h2 class="section-subtitle mb-4" style="color: white; max-width:900px; ">
                            Boarding Pass holders can request price matching for most flights, hotels, or tours for sale on the Venti platform. Price matching is how we ensure you're not missing out on great deals.</h2>
                            <p><strong>We do not price match Hawaiian Airlines or TAP Air Portugal</strong></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
        <div class="gradient-wrap mt-4" style="background:#F2F2F6 !important;">
	        <div class="container hero-section mb-4 mt-4">
	            <div class="row mb-4">
	            	<div class="col-md-8">
		            	<div class="card bg-white">
		            		<div class="card-body">
                                <p>Venti's price matching differs by category, and our team evaluates each request on a case-by-case basis. The deal under consideration must be:</p>
                                    <ul>
                                        <li>Discoverable via a public-facing search tool that is not behind a paywall or membership portal: Examples of what we will not price match include hopper, SkyScanner, and travel portals offered via Travel Credit Card providers.</li>
                                        <li>Non-Clearance in nature. Examples of what we will not consider are price bundling, discounts for new customers, coupons, rebates, mistake fares, or artificial price cutting hidden behind excessive fees. We will consider limited-time specials offered by carriers (not re-sellers or aggregators) if the travel details match.</li>
                                        <li>Provided by a vendor that has been in business for at least five years.</li>
                                    </ul>

                                <p class="text-small">Price matching must be requested at least 10 days before the date you intend to purchase your flight. Buddy Pass account holders are not eligible for price matching. This page was last updated on April 3, 2024.</p>
			            	</div>
		            	</div>
		            </div>
	            </div>
				<div class="row mb-4">
	            	<div class="col-md-8">
	            		<h5>Price Matching Flights</h5>
	            		<div class="card bg-white">
		            		<div class="card-body">
				           		<p>For a flight to be price matched, the following details must be exactly the same:</p>
                                    <ul>
                                        <li>Origin airport</li>
                                        <li>Destination airport</li>
                                        <li>Flight type (one-way, round trip, direct, # of connections)</li>
                                        <li>Departure date</li>
                                        <li>Return date</li>
                                        <li>Origin departure time (+/- 10 minutes)</li>
                                        <li>Destination arrival time (+/- 10 minutes)</li>
                                        <li>Destination departure time (+/- 10 minutes)</li>
                                        <li>Return to origin arrival time (+/- 10 minutes)</li>
                                        <li>Seat Class (economy, business, first)</li>
                                        <li>Ticket Type (standard, flex, premium, plus)</li>
                                        <li>Number of passengers and passenger types (adult, children)</li>
                                        <li>Carrier(s) (operator or marketing/subsidiary)</li>
                                        <li>Flight Number</li>
                                        <li>Baggage Allotment</li>
                                    </ul>
                                    <p>When price matching a flight, Venti will use Google Flights as the primary aggregation tool. When a flight has been approved for price matching, you must still book the flight at the listing price on Venti. You Boarding Pass will be credited the fare difference in the form of Points after your booking.</p>
                                    <p>To request price matching, please provide screenshots of the competing offer in an email to and a screenshot of the flight you wish to book on Venti. If approved, you will receive a written approval notice from a Venti team member with the offer expiry.</p>
                                <p><strong>We do not price match Hawaiian Airlines or TAP Air Portugal</strong></p>
				           	</div>
				        </div>
				    </div>
				</div>
				<div class="row mb-4">
	            	<div class="col-md-8">
	            		<h5>Price Matching Hotels</h5>
	            		<div class="card bg-white">
		            		<div class="card-body">
				           		<p>For a hotel to be price matched, the following details must be exactly the same:</p>
                                <ul>
                                    <li>Location</li>
                                    <li>Check-in date</li>
                                    <li>Check-out date</li>
                                    <li>Number of rooms</li>
                                    <li>Number of guests and guest types (adult, children)</li>
                                    <li>Room type and size</li>
                                    <li>Number of beds and room configuration</li>
                                    <li>Room code</li>
                                    <li>Resort fees/Check-in fee</li>
                                    <li>Amenity access (includes food) and refund terms</li>
                                </ul>
                                <p>When price matching a hotel, Venti will use Google Hotels as the primary aggregation tool. In some cases, we will also call the hotel to confirm pricing. Venti does not offer bookings for short-term rentals, such as AirBnb and VRBO, and will not price match listings on similar sites. When a hotel has been approved for price matching, you must still book the flight at the listing price on Venti. You Boarding Pass will be credited the reservation difference in the form of Points after your booking. </p>
                                <p>To request price matching, please provide screenshots of the competing offer in an email to and a screenshot of the hotel you wish to book on Venti. If approved, you will receive a written approval notice from a Venti team member with the offer expiry.</p>
				           	</div>
				        </div>
				    </div>
				</div>
				<div class="row mb-4">
					<div class="col-md-8">
						<h5>What About Price Protection?</h5>
						<div class="card bg-white">
							<div class="card-body">
				           		<p>At this time, Venti does not offer price protection in the case that your trip's fare decreases after booking.</p>
			            	</div>
		            	</div>
	            	</div>
	            </div>
	       	</div>
	    </div>
<!-- Cover End -->
@endsection


    
@section("js")
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/typeahead.js/0.10.4/typeahead.bundle.min.js"></script>
    @if(env('APP_ENV') != "local")
        <!-- Meta Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s)
            {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};
            if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
            n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t,s)}(window, document,'script',
            'https://connect.facebook.net/en_US/fbevents.js');
            fbq('init', '1169331916805166');
            fbq('track', 'PageView');
        </script>
        <noscript><img height="1" width="1" style="display:none"
        src="https://www.facebook.com/tr?id=1169331916805166&ev=PageView&noscript=1"
        /></noscript>
        <!-- End Meta Pixel Code -->
    @endif
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/11.0.2/bootstrap-slider.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $.ajaxSetup({
                 headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                 }
              });

            toggleLogo("black","white");

            function toggleLogo(color, replaced){
                var logoSRC = $(".logo").attr("src");
                logoSRC = logoSRC.replace(color, replaced);
                console.log(logoSRC)
                $(".logo").attr("src", logoSRC);
            }

            $("#hamburger-menu").click(function(){
                if($("#page_home__show").hasClass("mobile-nav-open")){
                    toggleLogo("white","black");
                } else {
                    toggleLogo("black","white");
                }
                
            });

        });

    </script>
@endsection