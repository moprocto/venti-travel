@extends('layouts.curator-master')

@section('css')
    <style type="text/css">
        @media(max-width: 1368px ){
            .tb-full-width{
                width: 100%;
            }
        }
        #footer{
            margin-top: 100px;
        }
        .about-body{
            justify-content: center;
        }
        body{
        	background: #F2F2F6 !important
        }
        .hamburger-menu-list li a{
            color: white;
        }
        .mobile-nav-open .hamburger-menu-list li a{
            color: black;
        }
        .btn-white{
            color: black !important;
        }
        .mobile-nav-open #backdrop{
            margin-top: 0 !important;
        }
        .border-black{
            border: 1px solid black;
        }
    </style>
@endsection

@section('content')
    <div id="backdrop" style="margin-top: -130px; width: 100%; z-index: 1; display: flex; align-items: center; min-height: 500px; background-position: center top;  background-size: cover;  background-image:url('/assets/img/buddypass-bg.jpg'); background-color: #F2F2F6 !important;">
        <div class="gradient-wrap" style="background: transparent; width:100%;">
            <!--  HERO -->
            <div class="section-wrapper black position-relative">
                <div class="row" style="align-items:center;">
                    <div class="col-md-5 col-sm-12 col-12 top-header" style="display: flex; align-items:center;">
                        <div id="hero-text-wrapper" class="mt-4" style="max-width:900px; margin-top:25px;">
                        	<br><br><br>
                            <h1 class="mb-3 section-title" style="line-height: 3rem; color: white; font-weight:600 !important; font-size: 3.8rem;">
                            About</sup>
                            <span style="font-size: 0.45em; display:block;">Venti Pro</span>
                            </h1>
                            <h2 class="section-subtitle mb-4" style="color: white; max-width:900px; ">Earn cash and Points to help you achieve your travel goals faster. We're accepting Pro account registrations on January 3, 2025.</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
        <div class="gradient-wrap mt-4" style="background:#F2F2F6 !important;">
	        <div class="container hero-section mb-4 mt-4">
	            <div class="row mb-4">
	            	<div class="col-md-8">
		            	<div class="card bg-white">
		            		<div class="card-body">
                                <p>Venti is helping Credit Unions build a presence within the travel community. Now, you'll be able to earn cash interest and Points when you create a savings account with one of the credit unions on our favorites list.</p>
			            	</div>
		            	</div>
		            </div>
	            </div>
				<div class="row mb-4">
	            	<div class="col-md-8">
	            		<h5>Our Favorites List</h5>
	            		<div class="card bg-white">
		            		<div class="card-body">
				           		<p>When you create a savings account with one of the below credit unions, we'll credit your Venti Pro account with Points at a rate of 3% APY on the first $25,000. Points are calculated and rewarded by Venti each week based on the average daily cash balance of your savings account. </p>
                                <br>
                                <div class="row text-center align-items-center">
                                    <div class="col-md-4 text-center d-flex justify-content-center" style="flex-direction:column;">
                                        <a href="https://www.wingsfinancial.com/" target="_blank" style="align-self:center;">
                                            <img src="/assets/img/wings-logo.png" style="height:75px;" class="d-block mb-3">
                                        </a>
                                        <a href="https://www.wingsfinancial.com/savings/platinum-savings" class="btn btn-white text-primary d-block border-black" target="_blank">Up to 4.75% on Deposits</a>
                                    </div>
                                    <div class="col-md-4 text-center d-flex justify-content-center" style="flex-direction:column;">
                                        <a href="https://www.meriwest.com/" target="_blank" style="align-self:center;">
                                            <img src="/assets/img/meriwest-logo.png" style="height: 75px;" class="d-block mb-3">
                                        </a>
                                        <a href="https://www.meriwest.com/personal/savings-accounts/premier-savings-for-new-members" class="btn btn-white text-primary d-block border-black" target="_blank">Up to 5.50% on Deposits</a>
                                    </div>

                                    <div class="col-md-4 text-center d-flex justify-content-center" style="flex-direction:column;">
                                        <a href="https://www.msufcu.org/" target="_blank" style="align-self:center;">
                                            <img src="/assets/img/msufcu-logo.png" style="height: 75px;" class="d-block mb-3">
                                        </a>
                                        <a href="https://www.msufcu.org/savingsbuilder" class="btn btn-white text-primary d-block border-black" target="_blank">Up to 5.25% on Deposits</a>
                                    </div>
                                </div>
                                <br>
                                <p>Cash interest is not calculated or disbursed by Venti. Points disbursed by Venti are in addition to savings account APY. All credit unions on Venti's favorites list are members of the National Credit Union Administration (NCUA) with deposits federally insured up to $250,000. Venti is not responsible for the accuracy of external institutions' APY.</p>
				           	</div>
				        </div>
				    </div>
				</div>
				<div class="row mb-4">
	            	<div class="col-md-8">
	            		<h5>How to Get 120 Points</h5>
	            		<div class="card bg-white">
		            		<div class="card-body">
				           		<p>Below is our step-by-step guide to earning $120 Points when you create a savings account with one of the credit unions on our favorites list. This promotional offer is limited to <strong>new credit union accounts</strong> created within the last 30 days of your Venti account. Must enable transaction history for eligibility.</p>
                                <ol>
                                    <li>Create a Venti account and select "Pro" as your preferred membership tier.</li>
                                    <li>Select one of the credit unions on our favorites list and create a savings account directly with them.</li>
                                    <li>Log in to Venti, and link your newly created savings account with your Venti profile. You'll need to enable permissions for Venti to read your balance and transaction history. These permissions can be turned off at any time but will result in your account being no longer eligible to earn rewards.
                                        <ul>
                                            <li>You'll receive confirmation that you've completed all necessary steps to earn the signup bonus.</li>
                                        </ul>
                                    </li>
                                    <li>Once you've completed everything, our team will reward your account with $120 worth in Points. You can use these Points to bring down the cost of any flight or hotel on Venti by $120. Your balance at the credit union must be greater than $0 to earn this reward and subsequent weekly payouts of Points.</li>
                                    <li>Earn Points every Saturday based on snapshots we take of the balance within your savings account at your new credit union account.</li>
                                    <li>Book your flight directly on Venti. Link a credit card to purchase flights or hotels that exceed your Points balance.</li>
                                </ol>
				           	</div>
				        </div>
				    </div>
				</div>
                <div class="row mb-4">
                    <div class="col-md-8">
                        <h5>Fine Print</h5>
                        <div class="card bg-white">
                            <div class="card-body">
                                <p>Venti is not an affiliate or subsidiary of the credit unions on our favorites list. The above credit unions are not involved in managing your Venti account or responsible for rewards distribution. Funds you deposit within your savings account at any credit union are held with them and not with Venti. Venti is not responsible for activities or other account related matters conducted with your credit union. Credit unions reserve the right to change their saving account APYs, requirements, savings account status, and fees at any time. The APYs listed on this page are accurate as of July 12, 2024.</p>
                            </div>
                        </div>
                    </div>
                </div>
	       	</div>
	    </div>
<!-- Cover End -->
@endsection


    
@section("js")
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/typeahead.js/0.10.4/typeahead.bundle.min.js"></script>
    @if(env('APP_ENV') != "local")
        <!-- Meta Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s)
            {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};
            if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
            n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t,s)}(window, document,'script',
            'https://connect.facebook.net/en_US/fbevents.js');
            fbq('init', '1169331916805166');
            fbq('track', 'PageView');
        </script>
        <noscript><img height="1" width="1" style="display:none"
        src="https://www.facebook.com/tr?id=1169331916805166&ev=PageView&noscript=1"
        /></noscript>
        <!-- End Meta Pixel Code -->
    @endif
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/11.0.2/bootstrap-slider.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $.ajaxSetup({
                 headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                 }
              });

            toggleLogo("black","white");

            function toggleLogo(color, replaced){
                var logoSRC = $(".logo").attr("src");
                logoSRC = logoSRC.replace(color, replaced);
                console.log(logoSRC)
                $(".logo").attr("src", logoSRC);
            }

            $("#hamburger-menu").click(function(){
                if($("#page_home__show").hasClass("mobile-nav-open")){
                    toggleLogo("white","black");
                } else {
                    toggleLogo("black","white");
                }
                
            });

        });

    </script>
@endsection