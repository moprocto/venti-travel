@extends('layouts.curator-master')

@section('css')
    <style type="text/css">
        @media(max-width: 1368px ){
            .tb-full-width{
                width: 100%;
            }
        }
        #footer{
            margin-top: 100px;
        }
        .about-body{
            justify-content: center;
        }
        body{
        	background: #F2F2F6 !important
        }
        .hamburger-menu-list li a{
            color: white;
        }
        .mobile-nav-open .hamburger-menu-list li a{
            color: black;
        }
        .btn-white{
            color: black !important;
        }
        .mobile-nav-open #backdrop{
            margin-top: 0 !important;
        }
        table > *{
            font-size: 13px;
        }
        
        @media(max-width: 500px ){
            table > *{
                font-size: 9px;
            }
        }

        .card .header-img{
            width: 100%;
            border-top-left-radius: 0.375rem;
            border-top-right-radius: 0.375rem;
        }
        .text-small{
            font-size: 12px;
        }
    </style>
@endsection

@section('content')
    <div id="backdrop" style="margin-top: -130px; width: 100%; z-index: 1; display: flex; align-items: center; min-height: 500px; background-position: center;  background-size: cover;  background-image:url('/assets/img/mystery-about-bg.jpg'); background-color: #F2F2F6 !important;">
        <div class="gradient-wrap" style="background: transparent; width:100%;">
            <!--  HERO -->
            <div class="section-wrapper black position-relative">
                <div class="row" style="align-items:center;">
                    <div class="col-md-5 col-sm-12 col-12 top-header" style="display: flex; align-items:center;">
                        <div id="hero-text-wrapper" class="mt-4" style="max-width:900px; margin-top:25px;">
                        	<br><br><br>
                            <h1 class="mb-3 section-title" style="line-height: 3rem; color: white; font-weight:600 !important; font-size: 3.8rem;">
                            About</sup>
                            <span style="font-size: 0.45em; display:block;">Mystery Box</span>
                            </h1>
                            <h2 class="section-subtitle mb-4" style="color: white; max-width:900px; ">
                            Try your luck at winning cool prizes and vacation packages</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
        <div class="gradient-wrap mt-4" style="background:#F2F2F6 !important;">
	        <div class="container hero-section mb-4 mt-4">
	            <div class="row mb-4">
	            	<div class="col-md-8">
		            	<div class="card bg-white">
		            		<div class="card-body">
		            			<p>
                                    To make the Venti experience just a bit more fun, we're allowing members to spend a limited amount of their Points for the chance to win cool prizes ranging from free flights, hotels, and vacation packages. Must have an active Venti account with at least 1 Point. <strong>Prizes are not redeemable for cash.</strong>
                                </p> 
                                <br>
                                <a href="/boardingpass/mystery" target="_blank" class="btn btn-black">Open Mystery Box</a>
			            	</div>
		            	</div>
		            </div>
	            </div>
                
                @include('about.panels.mystery-table')
                @include('about.panels.prizes')
                @include('about.panels.past-winners')

                <div class="row mb-4">
                    <div class="col-md-8">
                        <h5>Claiming Your Prize</h5>
                        <p>Congrats! This is an easy and stress-free process. When you win a non-Points prize, you'll receive an email containing your confirmation number and award date.</p>
                        <div class="card bg-white">
                            <div class="card-body">
                                <p>If you win a Dragon, Captain, High Tide, Low Tide, Gold, or Silver prize, you have up to two years to activate your prize. All other hotel and flight prizes must be activated within one year.</p>
                                <p><strong>Your name must be on bookings for flights and hotel prizes.</strong> </p>
                                <p>When you're ready to start the planning process, you can send an email to admin@venti.co, and our team will help you plan your trip.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mb-4">
                    <div class="col-md-8">
                        <h5>Terms and Conditions</h5>
                        <p>By opening our mystery box, you agree to the below terms.</p>
                        <div class="card bg-white">
                            <div class="card-body">
                                <p>Points are required to open the Mystery Box. You cannot use cash. Your chances of winning certain prizes increase when using more points to open the box. Winning a prize is not guaranteed. Play at your own risk. Points used to open the Mystery Box are non-refundable.</p>
                                <p>Restricted to U.S. residents with a valid Venti account. Automated entries are prohibited, and any use of automated devices will cause disqualification. Entrants may not enter with multiple accounts nor may entrants use any other device or artifice to enter multiple times or as multiple entrants. Any entrant who attempts to enter with accounts, under multiple identities, or uses any device or artifice to enter multiple times will be disqualified and forfeits any and all prizes won, in Venti’s sole discretion. Winners of certain prizes will be required to show proof of being the Authorized Account Holder, or other proof of identity, as applicable. Any prize that includes flights and hotels will require a completed tax form. We reserve the right to deny prizes if a tax form is not completed.</p>
                                <p>Venti reserves the right to deny prizes to accounts that use ill-gotten or illegitmiately garnered Points. We reserve the right to conduct an audit of all account transactions to determine eligibility to receive prizes. It is against our policy to arrange or conduct a sale of your winnings to cash. Offer void where prohibited. Offer is subject to all applicable federal, state and local laws.</p>
                                <p>Neither Venti Financial, Inc., nor its parent, affiliates, subsidiaries, and advertising and promotion agencies nor any of their respective officers, directors, employees, representatives or agents (collectively, “Released Parties”) are responsible for lost, late, incomplete, damaged, delayed, inaccurate, stolen, misdirected, undelivered, or garbled orders, email, mail, or other communications of any kind; or for errors or difficulties of any kind whether human, mechanical, electronic, computer, network, typographical, printing or otherwise relating to or in connection with the offer, including, without limitation, errors or difficulties which may occur in connection with the administration of the offer, the processing of orders, the functionality of the website, or in any offer-related materials. Released Parties will have no liability whatsoever for, and shall be held harmless against, any liability, for any injuries, losses or damages of any kind, including death, to persons, or property resulting in whole or in part, directly or indirectly, from participation in this offer or use of prizes won.</p>
                                <p>Venti reserves the right to substitute and limit prizes based on availability, safety, cancellations, and travel restrictions. We reserve the right to ban accounts from opening the mystery box for any reason. We also reserve the right to restrict the number of times you can open the mystery box in a given day, week, month, or year.</p>
                                <p>Winners of flights and hotels agree to have their first name, state of residence, and Member ID posted on Venti's website.</p>
                            </div>
                        </div>
                    </div>
                </div>
	       	</div>
	    </div>
<!-- Cover End -->
@endsection


    
@section("js")
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/typeahead.js/0.10.4/typeahead.bundle.min.js"></script>
    @if(env('APP_ENV') != "local")
        <!-- Meta Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s)
            {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};
            if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
            n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t,s)}(window, document,'script',
            'https://connect.facebook.net/en_US/fbevents.js');
            fbq('init', '1169331916805166');
            fbq('track', 'PageView');
        </script>
        <noscript><img height="1" width="1" style="display:none"
        src="https://www.facebook.com/tr?id=1169331916805166&ev=PageView&noscript=1"
        /></noscript>
        <!-- End Meta Pixel Code -->
    @endif
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/11.0.2/bootstrap-slider.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $.ajaxSetup({
                 headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                 }
              });

            toggleLogo("black","white");

            function toggleLogo(color, replaced){
                var logoSRC = $(".logo").attr("src");
                logoSRC = logoSRC.replace(color, replaced);
                console.log(logoSRC)
                $(".logo").attr("src", logoSRC);
            }

            $("#hamburger-menu").click(function(){
                if($("#page_home__show").hasClass("mobile-nav-open")){
                    toggleLogo("white","black");
                } else {
                    toggleLogo("black","white");
                }
                
            });

        });

    </script>
@endsection