@extends('layouts.curator-master')

@section('css')
    <style type="text/css">
        @media(max-width: 1368px ){
            .tb-full-width{
                width: 100%;
            }
        }
        #footer{
            margin-top: 100px;
        }
        .about-body{
            justify-content: center;
        }
        body{
        	background: #F2F2F6 !important
        }
        .hamburger-menu-list li a{
            color: white;
        }
        .mobile-nav-open .hamburger-menu-list li a{
            color: black;
        }
        .btn-white{
            color: black !important;
        }
        .mobile-nav-open #backdrop{
            margin-top: 0 !important;
        }
    </style>
@endsection

@section('content')
    <div id="backdrop" style="margin-top: -130px; width: 100%; z-index: 1; display: flex; align-items: center; min-height: 500px; background-position: center;  background-size: cover;  background-image:url('/assets/img/navigator-backdrop-t6.png'); background-color: #F2F2F6 !important;">
        <div class="gradient-wrap" style="background: transparent; width:100%;">
            <!--  HERO -->
            <div class="section-wrapper black position-relative">
                <div class="row" style="align-items:center;">
                    <div class="col-md-5 col-sm-12 col-12 top-header" style="display: flex; align-items:center;">
                        <div id="hero-text-wrapper" class="mt-4" style="max-width:900px; margin-top:25px;">
                        	<br><br><br>
                            <h1 class="mb-3 section-title" style="line-height: 3rem; color: white; font-weight:600 !important; font-size: 3.8rem;">
                            About</sup>
                            <span style="font-size: 0.45em; display:block;">Airlock Flight Protection</span>
                            </h1>
                            <h2 class="section-subtitle mb-4" style="color: white; max-width:900px; ">
                            Embarking on any journey brings a sense of excitement and anticipation, but the unpredictable nature of travel demands prudent planning—and that's where flight protection becomes indispensable. </h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
        <div class="gradient-wrap mt-4" style="background:#F2F2F6 !important;">
	        <div class="container hero-section mb-4 mt-4">
	            <div class="row mb-4">
	            	<div class="col-md-8">
		            	<div class="card bg-white">
		            		<div class="card-body">
		            			<p>
                                    Consider the myriad of unforeseen circumstances that could affect your trip: weather disruptions, sudden illness, personal emergencies, or you simply don't want to go anymore. Regardless of your need to cancel, it can come with significant financial repercussions.
                                </p> 
                                <p>With Airlock, you safeguard your flight investment and rest easy knowing that if your plans go awry, you're covered, allowing you to book your next adventure with confidence and peace of mind.
                                </p>
			            	</div>
		            	</div>
		            </div>
	            </div>
				<div class="row mb-4">
	            	<div class="col-md-8">
	            		<h5>What About Cancelable Fares?</h5>
	            		<div class="card bg-white">
		            		<div class="card-body">
				           		<p>
                                    While airlines do offer cancellation options, they often come with restrictions, fees, and a maze of policies that can leave you with vouchers instead of refunds or with credits that have expiration dates. Our flight protection plan is designed to minimize the stress and uncertainty associated with trip cancellations. It provides comprehensive coverage that goes beyond the airline's limited offers, ensuring that you are reimbursed for non-refundable expenses imposed by the airlines.
                                </p>
                                <p>
                                    What sets our protection plan apart is the simplicity and ease of the claim process. We understand that dealing with cancellations is stressful enough without the added hassle of paperwork, lengthy phone calls, arguing with a chat bot, or the need to provide explanations. That's why our hassle-free claim process is fully digital, requiring no direct contact with a person unless you request it. You can manage and submit your request with convenience, without the need to talk to a person. Our straightforward, user-friendly approach ensures a smooth, quick, and efficient resolution and speedy refunds.
                                </p>
				           	</div>
				        </div>
				    </div>
				</div>
				<div class="row mb-4">
	            	<div class="col-md-8">
	            		<h5>The Bottom Line</h5>
	            		<div class="card bg-white">
		            		<div class="card-body">
				           		<p>
                                    Every flight and trip is an investment in experience, and our flight protection plan acts as your safety net. So, whether you're traveling for business, pleasure, or adventure, consider the small step of adding our protection to your itinerary. It's a decision that grants you the ultimate luxury in travel: peace of mind. With our commitment to hassle-free service, you can focus on what truly matters: creating unforgettable memories around the globe. Embrace each opportunity to explore the world, secure in the knowledge that we've got your back, every step of the way.
                                </p>
				           	</div>
				        </div>
				    </div>
				</div>
				<div class="row mb-4">
					<div class="col-md-8">
						<h5>Terms & Conditions</h5>
						<div class="card bg-white">
							<div class="card-body">
				           		<p>
                                Venti Airlock ("Flight Protection") - is an additional charge which provides a less restrictive fare and allows you to cancel your flight before departure. Airlock is not travel insurance and does not cover purchases made elsewhere. Airlock is strictly applicable to flights purchased on Venti. If you require insurance for your overall trip, please email admin@venti.co or purchase insurance through our <a href="/insurance">affiliate portal</a>. Airlock quotes are determined on an order-by-order basis and are subject to change prior to purchase. To activate your Airlock coverage, you can send an email to admin@venti.co or log into your account and "cancel" your flight via your trip's detail page.
                                </p>
                                <h5>Additional Restrictions and Considerations:</h5>
                                <p>
                                 Airlock cannot be activated after departure. Airlock expiry will update if your initial departing flight is delayed.</p>
                                <p>In the instance that your flight was canceled, you must work with the airline to receive a refund for your fare. Airlock does not reimburse for airline iniated cancelations or delays.</p>
                                <p>Airlock cannot be applied to a single passenger, and can only be applied to the entire order.</p>
                                <p>Airlock may not cover add-ons such as additional seats and checked bags in all instances.</p>
			            	</div>
		            	</div>
	            	</div>
	            </div>
                <div class="row">
                    <div class="col-md-8">
                        <h5>How do I get my refund?</h5>
                        <div class="card bg-white">
                            <div class="card-body">
                                <p>
                                    Before you confirm your cancellation, you will be presented with a refund offer that shows how much cash and Points will be sent to you. You must accept the offer prior to submission. Once you submit your cancellation, your trip will be marked canceled with the airline immediately, which impacts all passengers associated with your booking. This is non-reversible.
                                </p>
                                <p>After you submit your cancellation, our team will begin processing your refund immediately but it may take up to three business days for your funds and Points (if applicable) are reflected in your account. The amount you receive in the form of a refund may be lower that the initial estimate if partial refunds or price matching credits were made to the order.</p>
                            </div>
                        </div>
                    </div>
                </div>
	       	</div>
	    </div>
<!-- Cover End -->
@endsection


    
@section("js")
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/typeahead.js/0.10.4/typeahead.bundle.min.js"></script>
    @if(env('APP_ENV') != "local")
        <!-- Meta Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s)
            {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};
            if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
            n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t,s)}(window, document,'script',
            'https://connect.facebook.net/en_US/fbevents.js');
            fbq('init', '1169331916805166');
            fbq('track', 'PageView');
        </script>
        <noscript><img height="1" width="1" style="display:none"
        src="https://www.facebook.com/tr?id=1169331916805166&ev=PageView&noscript=1"
        /></noscript>
        <!-- End Meta Pixel Code -->
    @endif
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/11.0.2/bootstrap-slider.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $.ajaxSetup({
                 headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                 }
              });

            toggleLogo("black","white");

            function toggleLogo(color, replaced){
                var logoSRC = $(".logo").attr("src");
                logoSRC = logoSRC.replace(color, replaced);
                console.log(logoSRC)
                $(".logo").attr("src", logoSRC);
            }

            $("#hamburger-menu").click(function(){
                if($("#page_home__show").hasClass("mobile-nav-open")){
                    toggleLogo("white","black");
                } else {
                    toggleLogo("black","white");
                }
                
            });

        });

    </script>
@endsection