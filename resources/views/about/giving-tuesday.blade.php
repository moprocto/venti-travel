@extends("layouts.curator-master")

@section("css")

@endsection

@section("content")
	<div id="page-content">
	    <div class="gradient-wrap">
	        <div class="container hero-section mb-4 mt-4">
	           	<div class="row">
	           		<div class="col-md-10 mt-4">
	                    <h1>About | Giving Tuesday</h1>
	                    <h5>We're Helping the World See</h5>
	                    <div class="card bg-white">
			            	<div class="card-body">
			            		<div class="row">
						           	<div class="col-md-10">
						           		<h4>We donated $179.26</h4>
						           		<p style="font-weight:bold;">Confirmation ID: 96877319</p>
						           		<p>
						           			The Tuesday following Black Friday was traditionally reserved for Giving Tuesday, a day to celebrate and appreciate amazing charitable organizations making our planet and lives better. Unfortunately, Giving Tuesday is being slowly overshadowed by the travel industry making one more desperate push to sell travel packages and vacations.</p>
										<p>
										In this email you won't find a "blow-out" discount. Instead, we're re-affirming Giving Tuesday by donating 100% of our application fees to our favorite charitable organization, Cure Blindness' Himalayan Cataract Project (HCP) between Nov. 27 and Nov. 28.</p>

										<strong>About the Project:</strong>
										<p>
										HCP Cure Blindness is working to eradicate avoidable blindness in under-resourced areas of the world by helping people retain and regain their sight through inexpensive procedures. Since 1995, HCP has performed 1.5 million procedures, trained over 19,000 professionals in developing countries, and screened over 14.5 million patients. In our opinion, HCP is one of the most effective charitable organizations in the world with real, measurable impact. Watch the below video to learn more:</p>
										<p>
											<iframe width="560" height="315" src="https://www.youtube.com/embed/cCRyQHmsfbs?si=W_Yp7B4OCHhJ_DfQ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
										</p>
										<p>
											<strong>Double the Impact:</strong>
										</p>
										<p>
										During Giving Tuesday, partners of HCP will be matching all contributions dollar-for-dollar. To accelerate HCP's mission to restore sight to the world, we'll also be donating $100 for every $10,000 in deposits made onto Boarding Passes (initiated and cleared) by 11:59 p.m. EST tomorrow. It's a win-win. You get closer to your savings goals and help the world become just a little bit better.</p>

										<p>
										Funds for charitable contributions do NOT originate from customer deposits but rather a separate operating account. See how much we contributed by tracking our Giving Tuesday page.</p>

										<p>
										Seeing the world is an incredible privilege that we often do not appreciate until our ability to do is either hindered or gone. If you're not interested in a Venti Boarding Pass at this time, please consider making a <a href="https://cureblindness.org/givingtuesday" target="_blank">direct donation to HCP</a>.</p>
										</p>
						           	</div>
						        </div>
			            	</div>
			            </div>
	                </div>
	            </div>
	       	</div>
	    </div>
	</div>
@endsection

@section("js")

@endsection