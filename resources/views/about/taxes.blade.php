@extends('layouts.curator-master')

@section('css')
    <style type="text/css">
        @media(max-width: 1368px ){
            .tb-full-width{
                width: 100%;
            }
        }
        #footer{
            margin-top: 100px;
        }
        .about-body{
            justify-content: center;
        }
        body{
        	background: #F2F2F6 !important
        }
        .hamburger-menu-list li a{
            color: white;
        }
        .mobile-nav-open .hamburger-menu-list li a{
            color: black;
        }
        .btn-white{
            color: black !important;
        }
        .mobile-nav-open #backdrop{
            margin-top: 0 !important;
        }
    </style>
@endsection

@section('content')
    <div id="backdrop" style="margin-top: -130px; width: 100%; z-index: 1; display: flex; align-items: center; min-height: 500px; background-position: bottom;  background-size: cover;  background-image:url('/assets/img/navigator-backdrop-t4.jpg'); background-color: #F2F2F6 !important;">
        <div class="gradient-wrap" style="background: transparent; width:100%;">
            <!--  HERO -->
            <div class="section-wrapper black position-relative">
                <div class="row" style="align-items:center;">
                    <div class="col-md-5 col-sm-12 col-12 top-header" style="display: flex; align-items:center;">
                        <div id="hero-text-wrapper" class="mt-4" style="max-width:900px; margin-top:25px;">
                        	<br><br><br>
                            <h1 class="mb-3 section-title" style="line-height: 3rem; color: white; font-weight:600 !important; font-size: 3.8rem;">
                            About</sup>
                            <span style="font-size: 0.45em; display:block;">Taxes & Verification</span>
                            </h1>
                            <h2 class="section-subtitle mb-4" style="color: white; max-width:900px; ">
                            Let's face it, taxes are everywhere, and Boarding Pass Points are subject to taxes when you use them to make a purchase or send to a friend.</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
        <div class="gradient-wrap mt-4" style="background:#F2F2F6 !important;">
	        <div class="container hero-section mb-4 mt-4">
	            <div class="row mb-4">
	            	<div class="col-md-8">
		            	<div class="card bg-white">
		            		<div class="card-body">
		            			<p>Earning points is easy! Below are examples that allow anyone to earn points:</p>
		            			<ul>
                                    <li>You achieved VIP status on our platform and completed your first deposit.</li>
                                    <li>You invited someone to join Venti, and they completed their first deposit.</li>
                                    <li>You maintained a Cash Balance on your Boarding Pass greater than $0 during a statement period.</li>
                                    <li>You purchased points from your dashboard using Warp Drive.</li>
                                    <li>Someone on Venti sent you Points</li>
                                </ul>
                                <p><strong>Remember: 1 Point = $1 toward travel</strong></p>
				           		<p>
                                    Each activity rewards a different amount of points. You can track how many points you have from within your Venti account. Points do not expire and can be transferred to others on Venti.
                                </p>
                                
			            	</div>
		            	</div>
		            </div>
	            </div>
				<div class="row mb-4">
	            	<div class="col-md-8">
	            		<h5>What Are the Restrictions?</h5>
	            		<div class="card bg-white">
		            		<div class="card-body">
				           		<p>
                                    Points can only be used <strong>towards a purchase</strong> on the Venti platform. You cannot redeem points for cash and you cannot spend your points to fulfill off-platform transactions. Selling your points is against our <a href="/terms" target="_blank">Terms of Use</a> and may result in an account audit. Purchases on our platform cannot be made solely with points. Our proprietary algorithm will determine how many points you can use towards a transaction, which can be up to 100%. Lastly, your account will not be rewarded Points via interest disbursement if your Cash Balance was $0 for the statement period.</p>

                                    <p>We impose these limitations to <strong>prevent platform abuse</strong>.</p>
				           	</div>
				        </div>
				    </div>
				</div>
				<div class="row mb-4">
	            	<div class="col-md-8">
	            		<h5>How Do I Use My Points?</h5>
	            		<div class="card bg-white">
		            		<div class="card-body">
				           		<p>Just like your home screen, you will see the following numbers on the checkout screen when making a purchase:</p>
                                <br>
                                <img src="/assets/img/balance-breakdown.png" class="d-block mb-2" style="margin: 0 auto; width:100%; max-width:400px;">
                                <br>
                                <ul>
                                    <li>Spending Power: The total amount of cash and cash-equivalent Points you have to make a purchase with.</li>
                                    <li>Cash Balance: The amount of cash you have on your Boarding Pass, which excludes pending transactions.</li>
                                    <li>Points: The total amount of points you have available to use towards a purchase.</li>
                                </ul>
                                
                                <p>You can decide how many Points you wish to apply to the purchase's subtotal. Prior to payment, you will be presented with a breakdown of how much will be deducted from your Cash and Points balances to complete the transaction.</p>

                                <br>
                                <img src="/assets/img/points-applied.png" class="d-block mb-2" style="margin: 0 auto; width:100%; max-width:400px;">
                                <br>
				           	</div>
				        </div>
				    </div>
				</div>
				<div class="row mb-4">
					<div class="col-md-8">
						<h5>Example: Flight to Miami from New York</h5>
						<div class="card bg-white">
							<div class="card-body">
				           		<p>Assuming you are a single passenger flying from JFK Airport to Miami International on American Airlines. The subtotal presented by the airline is $192.80 for a round trip. Your Cash Balance is $1,000 and your Points balance is 500.</p>
                                <p>The Venti Algorithm determines that you can use 100 points towards this flight. During the checkout screen, you decide to use the full 100 points allowed and are presented with a new total of $92.80. After booking the flight, your new Cash Balance is $907.20, and your Points balance is 400. You scored the Miami flight for a total discount of about 52%.</p>
			            	</div>
		            	</div>
	            	</div>
	            </div>
                <div class="row">
                    <div class="col-md-8">
                        <h5>What About Taxes?</h5>
                        <div class="card bg-white">
                            <div class="card-body">
                                <p>We consider a taxable event to be created when you apply Points toward a purchase, which is when they are  converted into "cash equivalent value." Thus, if you received 10,000 points from a friend or family member, we will not classify the transfer as a tax event in our system, and it will not be included in your 1099-K from our ACH payments provider, Dwolla. At the beginning of each year, you'll be able to download a statement from your Boarding Pass that will provide a summary of all events deemed taxable by Venti. Venti does not offer tax advice. You must seek the consultation of a licensed tax professional to review your individual circumstances.</p>
                            </div>
                        </div>
                    </div>
                </div>
	       	</div>
	    </div>
<!-- Cover End -->
@endsection


    
@section("js")
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/typeahead.js/0.10.4/typeahead.bundle.min.js"></script>
    @if(env('APP_ENV') != "local")
        <!-- Meta Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s)
            {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};
            if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
            n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t,s)}(window, document,'script',
            'https://connect.facebook.net/en_US/fbevents.js');
            fbq('init', '1169331916805166');
            fbq('track', 'PageView');
        </script>
        <noscript><img height="1" width="1" style="display:none"
        src="https://www.facebook.com/tr?id=1169331916805166&ev=PageView&noscript=1"
        /></noscript>
        <!-- End Meta Pixel Code -->
    @endif
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/11.0.2/bootstrap-slider.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $.ajaxSetup({
                 headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                 }
              });

            toggleLogo("black","white");

            function toggleLogo(color, replaced){
                var logoSRC = $(".logo").attr("src");
                logoSRC = logoSRC.replace(color, replaced);
                console.log(logoSRC)
                $(".logo").attr("src", logoSRC);
            }

            $("#hamburger-menu").click(function(){
                if($("#page_home__show").hasClass("mobile-nav-open")){
                    toggleLogo("white","black");
                } else {
                    toggleLogo("black","white");
                }
                
            });

        });

    </script>
@endsection