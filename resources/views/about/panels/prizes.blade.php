<div class="row mb-4">
   <div class="col-md-8">
      <h5>Dragon Level Prizes</h5>
      <p>Seven incredible nights in the best destinations for you and the family (or friends). Prizes only include flights and accommodaton.</p>
      <div class="row">
         <div class="col-md-3 mb-3">
            <div class="card bg-white">
               <div class="card-body text-center" style="padding:0">
                  <img class="header-img" src="/assets/img/maldives-dragon.jpg">
                  <p style="padding: 5px 10px; padding-bottom:0">Maldives</p>
               </div>
            </div>
         </div>
         <div class="col-md-3 mb-3">
            <div class="card bg-white">
               <div class="card-body text-center" style="padding:0">
                  <img class="header-img"  src="/assets/img/bora-bora-dragon.jpg">
                  <p style="padding: 5px 10px; padding-bottom:0">Bora Bora</p>
               </div>
            </div>
         </div>
         <div class="col-md-3 mb-3">
            <div class="card bg-white">
               <div class="card-body text-center" style="padding:0">
                  <img class="header-img"  src="/assets/img/switzerland-dragon.jpg">
                  <p style="padding: 5px 10px; padding-bottom:0">Switzerland</p>
               </div>
            </div>
         </div>
         <div class="col-md-3 mb-3">
            <div class="card bg-white">
               <div class="card-body text-center" style="padding:0">
                  <img class="header-img"  src="/assets/img/greece-dragon.jpg">
                  <p style="padding: 5px 10px; padding-bottom:0">Greece</p>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>