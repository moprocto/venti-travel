<div class="row mb-4">
   <div class="col-md-8">
      <h5>Past Winners</h5>
      <p>Recipients of prizes containing a flight, hotel, or both are listed below.</p>
      <div class="card bg-white">
         <div class="card-body">
            <table class="table">
               <thead>
                  <th>Winner</th>
                  <th>Member ID</th>
                  <th>Date Won</th>
                  <th>Prize</th>
               </thead>
               <tbody>
                  <tr>
                     <td>Wilson
                        <span class="d-block text-small">Arizona</span>
                     </td>
                     <td>OKW22586C</td>
                     <td>June 11, 2024</td>
                     <td>Bronze</td>
                  </tr>
                  <tr>
                     <td>Binod
                        <span class="d-block text-small">Arizona</span>
                     </td>
                     <td>REEG33D09</td>
                     <td>August 2, 2024</td>
                     <td>Economy Plus</td>
                  </tr>
                   <tr>
                     <td>Wilson
                        <span class="d-block text-small">Arizona</span>
                     </td>
                     <td>OKW22586C</td>
                     <td>August 12, 2024</td>
                     <td>Main Cabin</td>
                  </tr>
                  <tr>
                     <td>Misha
                        <span class="d-block text-small">Texas</span>
                     </td>
                     <td>4TZ83F8F9</td>
                     <td>November 1, 2024</td>
                     <td>Bronze</td>
                  </tr>
                  <tr>
                     <td>Misha
                        <span class="d-block text-small">Texas</span>
                     </td>
                     <td>4TZ83F8F9</td>
                     <td>November 3, 2024</td>
                     <td>Economy Plus</td>
                  </tr>
               </tbody>
            </table>
         </div>
      </div>
   </div>
</div>