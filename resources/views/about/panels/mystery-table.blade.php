<div class="row mb-4">
      <div class="col-md-8">
         <h5>Prizes and Winning Odds</h5>
         <p>Each time you open the Mystery Box, you are given <strong>up to 12 chances to win</strong> a prize. There is no limit on the number of prizes in each tier. Flights and hotels are booked on your behalf by Venti staff. Need help planning your trip? Our team will be happy to assist! Prizes listed as "10X", "5X", "2X", and "1X" are credited immediately to your account in the form of Points.</p>
         <br>
         <p>Prize odds updated on Aug 14, 2024 at 9:45 A.M. EDT</p>
         <div class="card bg-white">
            <div class="card-body">
               <table class="table">
                  <thead>
                      <td></td>
                      <td></td>
                      <td colspan="3" style="text-align: center;"><strong>Odds of Winning</strong></td>
                  </thead>
                  <thead>
                      <th>Tier</th>
                      <th>Example Prizes</th>
                      <th>1 Points</th>
                      <th>3 Points</th>
                      <th>5 Points</th>
                  </thead>
                 <tbody>
                    <tr>
                       <td>Dragon</td>
                       <td>7 Nights in the Maldives<sup>*</sup> up to four people. Business Class flights available. Flights and accommodation cannot exceed $20,000.</td>
                       <td class="text-center">1:500000</td>
                       <td class="text-center">1:166666</td>
                       <td class="text-center">1:125000</td>
                    </tr>
                    <tr>
                       <td>Captain</td>
                       <td>7 Nights in the Italy<sup>*</sup> up to four people. Business Class flights available. Flights and accommodation cannot exceed $10,000.</td>
                       <td class="text-center">1:250000</td>
                       <td class="text-center">1:83333</td>
                       <td class="text-center">1:62500</td>
                    </tr>
                    <tr>
                       <td>High Tide</td>
                       <td>7 Nights in the Cancún<sup>*</sup> up to four people. Flights and accommodation cannot exceed $5,000.</td>
                       <td class="text-center">1:125000</td>
                       <td class="text-center">1:41666</td>
                       <td class="text-center">1:31250</td>
                    </tr>
                    <tr>
                       <td>Low Tide</td>
                       <td>6 hotel nights anywhere. Flights included. Flights and accommodation cannot exceed $2,500.</td>
                       <td class="text-center">1:62500</td>
                       <td class="text-center">1:20833</td>
                       <td class="text-center">1:15625</td>
                    </tr>
                    <tr>
                       <td>Gold</td>
                       <td>3 hotel nights anywhere. Flights included. Flights and accommodation cannot exceed $2,000.</td>
                       <td class="text-center">1:31250</td>
                       <td class="text-center">1:10416</td>
                       <td class="text-center">1:7813</td>
                    </tr>
                    <tr>
                       <td>Silver</td>
                       <td>3 hotel nights anywhere. Flights included for domestic travel. Flights and accommodation cannot exceed $1,000.</td>
                       <td class="text-center">1:15625</td>
                       <td class="text-center">1:5208</td>
                       <td class="text-center">1:3906</td>
                    </tr>
                    <tr>
                       <td>Bronze</td>
                       <td>3 hotel nights anywhere or a roundtrip flight. Cannot exceed $500.</td>
                       <td class="text-center">1:7812</td>
                       <td class="text-center">1:2604</td>
                       <td class="text-center">1:1954</td>
                    </tr>
                    <tr>
                       <td>Main Cabin</td>
                       <td>A roundtrip domestic flight. Cannot exceed $250.</td>
                       <td class="text-center">1:3906</td>
                       <td class="text-center">1:1302</td>
                       <td class="text-center">1:976</td>
                    </tr>
                    <tr>
                       <td>Economy Plus</td>
                       <td>A one-way domestic flight. Cannot exceed $200.</td>
                       <td class="text-center">1:1953</td>
                       <td class="text-center">1:651</td>
                       <td class="text-center">1:439</td>
                    </tr>
                    <tr>
                       <td>10X</td>
                       <td>Receive 10x the Points spent opening the box.</td>
                       <td class="text-center">1:256</td>
                       <td class="text-center">1:192</td>
                       <td class="text-center">1:160</td>
                    </tr>
                    <tr>
                       <td>5X</td>
                       <td>Receive 5x the Points spent opening the box.</td>
                       <td class="text-center">1:64</td>
                       <td class="text-center">1:48</td>
                       <td class="text-center">1:40</td>
                    </tr>
                    <tr>
                       <td>2x</td>
                       <td>Receive 2x the Points spent opening the box.</td>
                       <td class="text-center">1:16</td>
                       <td class="text-center">1:12</td>
                       <td class="text-center">1:10</td>
                    </tr>
                    <tr>
                       <td>1X</td>
                       <td>Receive your Points back. Good luck next time!</td>
                       <td class="text-center">1:4</td>
                       <td class="text-center">1:3</td>
                       <td class="text-center">1:2.5</td>
                    </tr>
                 </tbody>
               </table>

               <sup>*</sup><span style="font-size:12px;">Destinations subject to change based on availability. Our team will help you identify destinations that is approprate for each tier.</span>
               </div>
        </div>
    </div>
</div>