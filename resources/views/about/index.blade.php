@extends('layouts.curator-master')

@section('css')
    <style type="text/css">
        @media(max-width: 1368px ){
            .tb-full-width{
                width: 100%;
            }
        }
        #footer{
            margin-top: 100px;
        }
        .about-body{
            justify-content: center;
        }
        body{
        	background: #F2F2F6 !important
        }
        .hamburger-menu-list li a{
            color: white;
        }
        .mobile-nav-open .hamburger-menu-list li a{
            color: black;
        }
        .btn-white{
            color: black !important;
        }
        .mobile-nav-open #backdrop{
            margin-top: 0 !important;
        }
    </style>
@endsection

@section('content')
    <div id="backdrop" style="margin-top: -130px; width: 100%; z-index: 1; display: flex; align-items: center; min-height: 500px; background-position: center;  background-size: cover;  background-image:url('/assets/img/navigator-backdrop-t2.jpg'); background-color: #F2F2F6 !important;">
        <div class="gradient-wrap" style="background: transparent; width:100%;">
            <!--  HERO -->
            <div class="section-wrapper black position-relative">
                <div class="row" style="align-items:center;">
                    <div class="col-md-5 col-sm-12 col-12 top-header" style="display: flex; align-items:center;">
                        <div id="hero-text-wrapper" class="mt-4" style="max-width:900px; margin-top:25px;">
                        	<br><br><br>
                            <h1 class="mb-3 section-title" style="line-height: 3rem; color: white; font-weight:600 !important; font-size: 3.8rem;">
                            About</sup>
                            </h1>
                            <h2 class="section-subtitle mb-4" style="color: white; max-width:900px; ">
                            We're on a mission to help everyone enjoy their passion for exploring the world and experiencing new cultures by providing them with the tools to make their trips more affordable.</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
        <div class="gradient-wrap mt-4" style="background:#F2F2F6 !important;">
	        <div class="container hero-section mb-4 mt-4">
	            <div class="row mb-4">
	            	<div class="col-md-8">
		            	<div class="card bg-white">
		            		<div class="card-body">
		            			<p>Venti Financial, Inc. is a U.S.-based travel finance company created by travelers for travelers. On Venti, you can save up to 100% on:</p>
		            			<ul>
		            				<li>Flights</li>
		            				<li>Hotels</li>
		            				<li>Tours & Travel Packages</li>
		            				<li>Luggage</li>
		            			</ul>
		            			<p>and so much more!</p>
				           		<p>On November 1, 2023, we launched our first product, <a href="/boardingpass">Boarding Pass</a>: the world's first high-yield travel savings account to reward travelers for building good financial habits. Since then, we've become the fastest growing platform for Millennial and Gen Z travelers.</p>
                                <p>To keep customer funds safe, we've partnered with <a href="https://www.dwolla.com/legal/about-our-financial-institution-partners/" target="_blank">Dwolla</a>, a leading ACH payments service provider, to handle verification, deposits, and withdrawals. With Dwolla as a partner, your funds are stored at <a href="https://www.veridiancu.org/" target="_blank">Veridian Credit Union</a>, a member NCUA bank. Learn more about <a href="https://www.veridiancu.org/faq/12331/is-my-money-insured-through-ncua" target="_blank">deposit insurance here</a>.</p>
			            	</div>
		            	</div>
		            </div>
	            </div>
				<div class="row mb-4">
	            	<div class="col-md-8">
	            		<h5>About Boarding Pass</h5>
	            		<div class="card bg-white">
		            		<div class="card-body">
				           		<p>A Venti Boarding Pass serves as your travel savings account. Without it, you cannot make purchases on the Venti Platform. We do not accept credit cards as payment for flights, hotels, and other travel-related products on our platform.</p>
				           		<p>To be eligible for a Boarding Pass, you must be at least 18 years of age and a U.S. resident with a valid Social Security Number (SSN).</p>
				           		<a href="/boardingpass" class="btn btn-white text-primary border-primary">Learn More <i class="fa fa-arrow-right"></i></a>
				           	</div>
				        </div>
				    </div>
				</div>
				<div class="row mb-4">
	            	<div class="col-md-8">
	            		<h5>About our Points</h5>
	            		<div class="card bg-white">
		            		<div class="card-body">
				           		<p>Points are a form of loyalty program currency offered by Venti that account holders use to buy down the cost of travel on our platform and participate in <a href="/sweepstakes">sweepstakes</a>.</p>
				           		<a href="/about/points" class="btn btn-white text-primary border-primary">Learn More <i class="fa fa-arrow-right"></i></a>
				           	</div>
				        </div>
				    </div>
				</div>
                <div class="row mb-4" style="display:none;">
                    <div class="col-md-8">
                        <h5>About Jet Bridge</h5>
                        <div class="card bg-white">
                            <div class="card-body">
                                <p>For travelers that need maximum flexibility, we will offer a $100,000 travel line of credit to support travel ideas of all shapes and sizes.</p>
                                <a href="/jetbridge" class="btn btn-white text-primary border-primary">Learn More <i class="fa fa-arrow-right"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mb-4">
                    <div class="col-md-8">
                        <h5>About Tailwind</h5>
                        <div class="card bg-white">
                            <div class="card-body">
                                <p>A travel checking account allows you to earn Points every time you earn, spend, and save when used alongside our rewards card.</p>
                                 <a href="/tailwind" class="btn btn-white text-primary border-primary">Learn More <i class="fa fa-arrow-right"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mb-4">
                    <div class="col-md-8">
                        <h5>About Mystery Box</h5>
                        <div class="card bg-white">
                            <div class="card-body">
                                <p>Everyone has a chance to win free flights, hotels, and vacations.</p>
                                 <a href="/about/mysterybox" class="btn btn-white text-primary border-primary">Learn More <i class="fa fa-arrow-right"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
				<div class="row">
					<div class="col-md-8">
						<h5>What's the meaning behind the name?</h5>
						<div class="card bg-white">
							<div class="card-body">
				           		<p>Most people are familiar with "venti" when they order their favorite frappe or latte at Starbucks. Venti means "twenty" in Italian, and we picked this name because we aspire to enable every person to visit at least 20 countries during their lifetime. We believe a good life is one well-traveled.</p>
				           		<br>
				           		<h5>Our Mailing Address:</h5>
				           		<p>Venti Financial, Inc.<br>
		      						800 N King Street <br>
		      						Suite 304 1552 <br>
		      						Wilmington, DE 19801
		      					<br>
		      					</p>
			            	</div>
		            	</div>
	            	</div>
	            </div>
	       	</div>
	    </div>
<!-- Cover End -->
@endsection


    
@section("js")
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/typeahead.js/0.10.4/typeahead.bundle.min.js"></script>
    @if(env('APP_ENV') != "local")
        <!-- Meta Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s)
            {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};
            if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
            n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t,s)}(window, document,'script',
            'https://connect.facebook.net/en_US/fbevents.js');
            fbq('init', '1169331916805166');
            fbq('track', 'PageView');
        </script>
        <noscript><img height="1" width="1" style="display:none"
        src="https://www.facebook.com/tr?id=1169331916805166&ev=PageView&noscript=1"
        /></noscript>
        <!-- End Meta Pixel Code -->
    @endif
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/11.0.2/bootstrap-slider.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $.ajaxSetup({
                 headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                 }
              });

            toggleLogo("black","white");

            function toggleLogo(color, replaced){
                var logoSRC = $(".logo").attr("src");
                logoSRC = logoSRC.replace(color, replaced);
                console.log(logoSRC)
                $(".logo").attr("src", logoSRC);
            }

            $("#hamburger-menu").click(function(){
                if($("#page_home__show").hasClass("mobile-nav-open")){
                    toggleLogo("white","black");
                } else {
                    toggleLogo("black","white");
                }
                
            });

        });

    </script>
@endsection