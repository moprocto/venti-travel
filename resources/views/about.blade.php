@extends("layouts.curator-master")

@section("css")

@endsection

@section("content")
	<div id="page-content">
	    <div class="gradient-wrap">
	        <div class="container hero-section mb-4 mt-4">
	           	<div class="row">
	           		<div class="col-md-10 mt-4">
	                    <h1>About</h1>
	                </div>
	            </div>
	            <div class="card bg-white">
	            	<div class="card-body">
	            		<div class="row">
				           	<div class="col-md-10">
				           		<br>
				           		<p>Venti Financial, Inc. is a U.S. based travel finance company created by travelers for travelers. We're on a mission to help everyone enjoy their passion for exploring the world and experiencing new cultures by providing them with the financial tools to do so.</p>
				           		<br><br>
				           		<p>On November 1, 2023, we launched our first product: <a href="/boardingpass">Boarding Pass</a>: the world's first high-yield travel savings account to reward travelers for building good financial habits.</p>
				           		<p>To keep customer funds safe, we've partnered with <a href="https://www.dwolla.com/legal/about-our-financial-institution-partners/" target="_blank">Dwolla</a>, a reputable ACH payments service provider, to handle verification, deposits, and withdrawals. With Dwolla as a partner, your funds are stored at <a href="https://www.veridiancu.org/" target="_blank">Veridian Credit Union</a>, a member NCUA bank. Learn more about <a href="https://www.veridiancu.org/faq/12331/is-my-money-insured-through-ncua" target="_blank">deposit insurance here</a>.</p>
				           		</ul>
				           		<br>
				           		<h5>What's the meaning behind the name?</h5>
				           		<p>Most people are familiar with "venti" when they order their favorite frappe or latte at Starbucks. Venti means "twenty" in Italian, and we picked this name because we aspire to enable every person to visit at least 20 countries during their lifetime. We believe a good life is one well-traveled.</p>
				           		<br>
				           		<h5>Our Mailing Address:</h5>
				           		<p>Venti Financial, Inc.<br>
		      						800 N King Street <br>
		      						Suite 304 1552 <br>
		      						Wilmington, DE 19801
		      					<br><br><br>
		      					</p>	
				           	</div>
		        		</div>
	            	</div>
	            </div>
	       	</div>
	    </div>
	</div>
@endsection

@section("js")

@endsection