<section style=" padding: 100px 20px;">
    <div class="container full-width">
        <div class="row">
            <div class="col-lg-6 offset-lg-3 col-md-8 offset-md-2 section-head text-center">
                <div class="section-head-tag">Our mission is to help every person visit 20 countries during their lifetime</div>
                <h2 class="section-head-title">Your Feedback is Valuable</h2>
                <p>How can we make your travel journey 10x better? <br> Responses are 100% anonymous. No email required.</p>
                <br>
                <form action="/feedback" method="POST" class="form text-center" id="feedback-form">
					@CSRF
					<div class="form-group">
						<textarea name="feedback" id="feedback-text" class="form-control" style="max-width: 330px; margin: 0 auto;" placeholder="venti should include..."></textarea> 
					</div>		
					<button type="button" class="btn btn-main" id="submit-feedback">Send Feedback</button>
				</form>
                <br>
                <p>Prefer a zoom call? <a href="https://calendly.com/markus-venti/" target="_blank">Schedule a chat</a> with our team!</p>
            </div>
        </div>
    </div>
</section>