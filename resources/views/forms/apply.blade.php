<form action="/navigator" method="POST" class="text-left" style="display:block;">
	@CSRF
	<input type="hidden" name="applied" value="{{ Date('m-d-Y') }}">
	@php
		$random = ( rand(10, 100) );

		if($random % 2 !== 0) {
		    if($random == 100) {
		        $random = $random - 2;
		    } else {
		        $random = $random + 1;
		    }
		}

		$solution = $random / 2;
		echo "<input type='hidden' name='ran' value='VENTI$random'>";
	@endphp
	<div class="row">
		<div class="col-md-4">
			<div class="form-group">
				<label class="text-left text-dark">Your Name:</label>
				<input type="text" class="form-control text-dark" name="name" required value="{{ old('name') }}" placeholder="Not Ratatouille">
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<label class="text-left text-dark">Your Email:</label>
				<input type="email" class="form-control text-dark" name="email" required value="{{ old('email') }}" placeholder="bestcontact@email.com">
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<label class="text-left text-dark">Website/Social Handle</label>
				<input type="text" class="form-control text-dark" name="website" value="{{ old('website') }}" placeholder="https://yoursite.com OR @instagram" required="">
				<span style="font-size:12px;">Please make sure your page is public.</span>
			</div>
		</div>
		
	</div>
	<div class="row">
		<div class="col-md-4">
			<div class="form-group">
				<label class="text-left text-dark">Interested in becoming a <a href="#pro">Pro Navigator</a>?<br>(This is optional)</label>
				<select class="form-control text-dark" name="pro" required>
					<option value="">—Please Select—</option>
					<option value="Yes" @if(old('pro') == "Yes") selected @endif >Yes</option>
					<option value="No" @if(old('pro') == "No") selected @endif >No</option>
				</select>
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<label class="text-left text-dark">Number of Countries Traveled<br>(Airports Don't Count 😉)</label>
				<input type="number" class="form-control text-dark" name="countries" value="{{ old('countries') }}" placeholder="10" required="" min="0" step="1">
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<label class="text-left text-dark">Which of the following best describes you?<br><br></label>
				<select class="form-control text-dark" name="experience" required>
					<option value="">—Please Select—</option>
					<option @if(old('experience') == "I am not great at planning, but I love traveling") selected @endif value="I am not great at planning, but I love traveling">I'm not great at planning, but I love traveling</option>
					<option @if(old('experience') == "I like planning trips mainly for myself") selected @endif value="I like planning trips mainly for myself">I like planning trips mainly for myself</option>
					<option @if(old('experience') == "I'm very organized and like to plan trips for my friends and family") selected @endif value="I'm very organized and like to plan trips for my friends and family">I'm very organized and like to plan trips for my friends and family</option>
					<option @if(old('experience') == "I am currently a tour guide/operator") selected @endif value="I am currently a tour guide/operator">I am currently a tour guide/operator</option>
					<option @if(old('experience') == "I am a travel agent or travel advisor") selected @endif value="I am a travel agent or travel advisor">I am a travel agent or travel advisor</option>
				</select>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-4">
			<div class="form-group">
				<label class="text-left text-dark">Are you fluent in English?</label>
				<select class="form-control text-dark" name="fluent">
					<option value="">—Please Select—</option>
					<option value="Yes" @if(old('fluent') == "Yes") selected @endif >Yes</option>
					<option value="No" @if(old('fluent') == "No") selected @endif >No</option>
				</select>
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<label class="text-left text-dark">What country do you primarily live and work?</label>
				<input type="text" name="country" class="form-control text-dark" placeholder="Example: Brazil" required="" value="{{ old('country') }}" />
			</div>
		</div>
		
		<div class="col-md-4">
			<div class="form-group">
				<label class="text-left text-dark">Do you prefer to stay in touch with travelers after each trip (as friends or acquintances)?</label>
				<select class="form-control text-dark" name="friends" required>
					<option value="">—Please Select—</option>
					<option value="Yes, if we're compatible" @if(old('friends') == "Yes, if we're compatible") selected @endif >Yes, if we're compatible</option>
					<option value="Not really" @if(old('friends') == "Not really") selected @endif >Not really</option>
				</select>
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<label class="text-left text-dark">How many trips per year do you think you could guide?</label>
				<input type="number" class="form-control text-dark" name="numtrips" value="{{ old('numtrips') }}" placeholder="10" required="" min="0" step="1">
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<label class="text-left text-dark">What type of trips do you think you are best suited for?</label>
				<select class="form-control text-dark" name="triptype" required>
					<option value="">—Please Select—</option>
					<option value="Month long voyages" @if(old('triptype') == "Month long voyages") selected @endif >Month long voyages</option>
					<option value="Week long adventures" @if(old('triptype') == "Week long adventures") selected @endif >Week long adventures</option>
					<option value="Weekend explorations" @if(old('triptype') == "Weekend explorations") selected @endif >Weekend explorations</option>
					<option value="Willing to do all the above" @if(old('triptype') == "Willing to do all the above") selected @endif >Willing to do all the above</option>
					<option value="Not sure yet" @if(old('triptype') == "Not sure yet") selected @endif >Not sure yet</option>
				</select>
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<label>Quick math test: <strong>What is {{ $random }} / 2?</strong><br>(We just need to make sure you are human)</label>
				@error('incorrectAnswer')
					<div class="alert alert-danger">
						<span>Your answer was incorrect.</span>
					</div>
				@enderror
				<input type="number" class="form-control" required min="5" max="50"  name="answer">
			</div>
		</div>
	</div>
	<div class="row" style="padding-top:50px;">

		<div class="col-md-12 text-center" id="submitApp">
			<input type="submit" value="Submit Application" class="btn btn-primary">
			<p class="text-dark" style="margin-top:25px;">Need help submitting? Send an email to <a href="mailto:markus@venti.co">markus@venti.co</a></p>
		</div>
	</div>
</form>