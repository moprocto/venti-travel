<div id="mc_embed_signup">
    <form action=https://venti.us1.list-manage.com/subscribe/post?u=c56dc9bc23a7cc9e1255ba4b6&amp;id=198a1db3ac method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validater" target="_blank" novalidate>
        <div id="mc_embed_signup_scroll" class="text-center">
            <input type="email" value="" name="EMAIL" class="form-control required" id="mce-EMAIL" placeholder="Your Email" style="box-shadow: 0 3px 6px rgba(0,0,0,0.02), 0 3px 6px rgba(0,0,0,0.06);  max-width: 250px; margin: 0 auto; margin-bottom: 20px;" required>

            <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_c56dc9bc23a7cc9e1255ba4b6_198a1db3ac" tabindex="-1" value=""></div>

            <div class="clear">
                <input type="submit" value="Join Waitlist" name="subscribe" id="mc-embedded-subscribe" class="btn btn-main">
            </div>
            <br><strong>No fee or credit card necessary to join our waitlist.</strong>
        </div>
    </form>
    <br>
    <p class="text-center text-small text-light">We hate spam, too. We'll only contact you when it's important.</p>
</div>