<form action="/excursions" method="POST" class="text-left" style="display:block;">
	@CSRF
	<input type="hidden" name="applied" value="{{ Date('m-d-Y') }}">
	@php
		$random = ( rand(10, 100) );

		if($random % 2 !== 0) {
		    if($random == 100) {
		        $random = $random - 2;
		    } else {
		        $random = $random + 1;
		    }
		}
		$solution = $random / 2;
		echo "<input type='hidden' name='ran' value='VENTI$random'>";
	@endphp
	<div class="row">
		<div class="col-md-4">
			<div class="form-group">
				<label class="text-left text-dark">Your Name:</label>
				<input type="text" class="form-control text-dark" name="name" required value="{{ old('name') }}" placeholder="Not Ratatouille">
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<label class="text-left text-dark">Your Email:</label>
				<input type="email" class="form-control text-dark" name="email" required value="{{ old('email') }}" placeholder="bestcontact@email.com">
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<label class="text-left text-dark">LinkedIn URL:</label>
				<input type="text" class="form-control text-dark" name="linkedin" value="{{ old('linkedin') }}" placeholder="https://www.linkedin.com/in/username" required="">
				<span style="font-size:12px;">Please make sure your page is public.</span>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-4">
			<div class="form-group">
				<label class="text-left text-dark">Your Job Title:</label>
				<input type="text" class="form-control text-dark" name="title" required value="{{ old('title') }}" placeholder="Vice President, Engineering">
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<label class="text-left text-dark">Your Industry:</label>
				<input type="text" class="form-control text-dark" name="industry" value="{{ old('industry') }}" placeholder="Enterprise Software" required="">
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<label class="text-left text-dark">Are you fluent in English?</label>
				<select class="form-control text-dark" name="fluent" required="">
					<option value="">—Please Select—</option>
					<option value="Yes" @if(old('fluent') == "Yes") selected @endif >Yes</option>
					<option value="No" @if(old('fluent') == "No") selected @endif >No</option>
				</select>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-4">
			<div class="form-group">
				<label class="text-left text-dark">How many years of professional (post-college) experience do you have in your industry?</label>
				<input type="text" class="form-control text-dark" name="years" value="{{ old('years') }}" placeholder="10" required="">
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<label class="text-left text-dark">Which of the following trips resonate with you the most?<br></label>
				<select class="form-control text-dark" name="experience" required>
					<option value="">—Please Select—</option>
					<option @if(old('experience') == "Finance in Switzerland") selected @endif value="Finance in Switzerland">Finance in Switzerland</option>
					<option @if(old('experience') == "Medicine in Egypt") selected @endif value="Medicine in Egypt">Medicine in Egypt</option>
					<option @if(old('experience') == "Black Tech Founders in South Africa") selected @endif value="Black Tech Founders in South Africa">Black Tech Founders in South Africa</option>
					<option @if(old('experience') == "Women in Law in France") selected @endif value="Women in Law in France">Women in Law in France</option>
					<option @if(old('experience') == "Game Developers in Japan") selected @endif value="Game Developers in Japan">Game Developers in Japan</option>
					<option @if(old('experience') == "Engineers in Germany") selected @endif value="Engineers in Germany">Engineers in Germany</option>
					<option @if(old('experience') == "Architects in Dubai") selected @endif value="Architects in Dubai">Architects in Dubai</option>
					<option @if(old('experience') == "Venture Capital in Iceland") selected @endif value="Venture Capital in Iceland">Venture Capital Investors in Iceland</option>
					<option @if(old('experience') == "Real Estate in Brazil") selected @endif value="Real Estate in Brazil">Real Estate Professionals in Brazil</option>
					<option @if(old('experience') == "Marketers in Canada") selected @endif value="Marketers in Canada">Marketers in Canada</option>
					<option @if(old('experience') == "None, my profession is not represented") selected @endif value="None, my profession is not represented">None, my profession is not represented</option>
					<option @if(old('experience') == "None, my preferred destination is not listed") selected @endif value="None, my preferred destination is not listed">None, my preferred destination is not listed</option>
					<option @if(old('experience') == "None, neither my profession or preferred destination are listed") selected @endif value="None, neither my profession or preferred destination are listed">None, neither my profession or preferred destination are listed</option>
				</select>
			</div>
		</div>


		<div class="col-md-4">
			<div class="form-group">
				<label class="text-left text-dark">Which of the following do you agree with most?<br><br></label>
				<select class="form-control text-dark" name="agree" required>
					<option value="">—Please Select—</option>
					<option @if(old('agree') == "I'm growing my network to find new employment opportunities") selected @endif value="I'm growing my network to find new employment opportunities">I'm growing my network to find new employment opportunities</option>
					<option @if(old('agree') == "I'm recruiting potential hires in my industry") selected @endif value="I'm recruiting potential hires in my industry">I'm recruiting potential hires in my industry</option>
					<option @if(old('agree') == "I'm looking to receive mentorship and career guidance") selected @endif value="I'm looking to receive mentorship and career guidance">I'm looking to receive mentorship and career guidance</option>
					<option @if(old('agree') == "I'm looking for support for a new venture or project") selected @endif value="I'm looking for support for a new venture or project">I'm looking for support for a new venture or project</option>
					<option @if(old('agree') == "I'd like to show appreciation to a valued client/customer") selected @endif value="I'd like to show appreciation to a valued client/customer">I'd like to show appreciation to a valued client/customer</option>
					<option @if(old('agree') == "I'm simply trying to make friends and professional connections.") selected @endif value="I'm simply trying to make friends and professional connections.">I'm simply trying to make friends and professional connections.</option>
					<option @if(old('agree') == "None") selected @endif value="None">None of the above</option>
					<option @if(old('agree') == "Seeking all program benefits") selected @endif value="Seeking all program benefits">All the above</option>
				</select>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-4">
			<div class="form-group">
				<label class="text-left text-dark">Prefer to travel somewhere else? List below</label>
				<input type="text" name="preference" class="form-control text-dark" placeholder="Example: Peru" value="{{ old('preference') }}" />
				<span style="font-size:12px;">(Optional)</span>
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<label class="text-left text-dark">What country do you primarily live and work?</label>
				<input type="text" name="country" class="form-control text-dark" placeholder="Example: Brazil" required="" value="{{ old('country') }}" />
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<label>Quick math test: <strong>What is {{ $random }} / 2?</strong></label>
				@error('incorrectAnswer')
					<div class="alert alert-danger">
						<span>Your answer was incorrect.</span>
					</div>
				@enderror
				<input type="number" class="form-control" required min="5" max="50"  name="answer">
				<span style="font-size:12px;">(We just need to make sure you are human)</span>
			</div>
		</div>
	</div>
	<div class="row" style="padding-top:50px;">

		<div class="col-md-12 text-center" id="submitApp">
			<input type="submit" value="Join Waitlist" class="btn btn-primary">
			<p class="text-dark" style="margin-top:25px;">Need help submitting? Send an email to <a href="mailto:markus@venti.co">markus@venti.co</a></p>
		</div>
	</div>
</form>