<!--
<div id="mc_embed_signup" style="text-align:right ; justify-content:flex-end; align-items:flex-end; padding-bottom:40px">
    <form action=https://venti.us1.list-manage.com/subscribe/post?u=c56dc9bc23a7cc9e1255ba4b6&amp;id=198a1db3ac method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validater" target="_blank" novalidate>
        <div id="mc_embed_signup_scroll" class="@if($dark ?? '') text-dark @endif" >
            <input type="email" value="" name="EMAIL" class="form-control required data-hj-allow" id="mce-EMAIL" placeholder="Your Email" style="margin-left: auto; box-shadow: 0 3px 6px rgba(0,0,0,0.02), 0 3px 6px rgba(0,0,0,0.06);  max-width: 450px; margin-bottom: 20px;" required>

            <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_c56dc9bc23a7cc9e1255ba4b6_198a1db3ac" tabindex="-1" value=""></div>

            <div class="clear text-center">
                <input type="submit" value="JOIN WAITLIST" name="subscribe" id="mc-embedded-subscribe" class="btn btn-success">
                <p style="font-size:12px; margin-top:5px; color:white;">We'll only contact you when it's important<br></p>
            </div>
        </div>
    </form>
    <br>
</div>
<script async src="https://eocampaign1.com/form/348237c8-3b19-11ee-8875-b18a8bc19714.js" data-form="348237c8-3b19-11ee-8875-b18a8bc19714"></script>
-->

<form method="POST" action="{{ route('boarding-success') }}" style="margin-top: 40px;" class="text-center">
@CSRF

<input type="hidden" name="deposit" id="deposit">
<input type="hidden" name="contribution" id="contribution">
<input type="hidden" name="balanceFinal" id="balanceFinal">
<input type="hidden" name="pointsFinal" id="pointsFinal">

<div class="form-group">
    <input type="text" name="email" required="" class="form-control" placeholder="your@email.com" style="min-width: 325px;">
</div>
<div class="form-group">
    <input type="submit" class="btn btn-success btn-wide" name="submit" value="SUBMIT" style="min-width: 225px;">
</div>
</form>