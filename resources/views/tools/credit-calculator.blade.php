<div class="row">
    <div class="col-md-12">
        <p class="section-head-body sign-up-box" style="font-size:1.3em; font-weight: bold; margin-bottom:0; padding-bottom:0">How Much Can You Borrow?<sup>**</sup></p>
        <p class="section-head-body" style="">Use our calculator below<br></p>
    </div>
    <div class="col-md-8 offset-md-2"> 
        <div class="text-left">
            <div class="col-md-12 mb-4 ">
                <label>Credit Score Range</label>
                <input type="text" id="pool-amount" class="calculate" 
                  data-provide="slider"
                  data-slider-ticks="[0, 1, 2, 3, 4, 5]"
                  data-slider-ticks-labels='["580-620", "621-660", "661 - 700", "701-740", "741-780","781+"]'
                  data-slider-min="1"
                  data-slider-max="6"
                  data-slider-step="1"
                  data-slider-value="5"
                  data-slider-tooltip="hide" />
            </div>
        </div>
    </div>
    <div class="col-md-8 offset-md-2">
        <div class="text-left">
            <div class="col-md-12 mb-4 ">
                <label>Average Savings Balance</label>
                <input type="text" id="pool-deposits" class="calculate" 
                  data-provide="slider"
                  data-slider-ticks="[0, 1, 2, 3, 4, 5]"
                  data-slider-ticks-labels='["< $10,000", "$20,000", "$40,000", "$60,000", "$80,000", "$100,000+"]'
                  data-slider-min="1"
                  data-slider-max="6"
                  data-slider-step="1"
                  data-slider-value="1"
                  data-slider-tooltip="hide" />
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <p class="section-head-body" style="font-size:1.3em; font-weight: bold; margin-bottom:0; padding-bottom:0">Our Estimate</p>
        <br>
    </div>
    <div class="col-md-6"><div style="background-color:rgba(49, 127, 131, 0.2); padding:20px; border-radius: 20px; font-weight:bold; margin-bottom: 10px;"><span style="font-size:36px">$<span id="balance" >20,000</span></span><br><span style="font-size:12px;">Est. Line of Credit</span></div>
    </div>
    <div class="col-md-6"><div style="background-color:rgba(252, 166, 76, 0.2); padding:20px; border-radius: 20px; font-weight:bold; ">
        <span style="font-size: 36px;">$<span id="points">14,000</span></span><br><span style="font-size:12px; line-height: 0.9"> Cash Reserve Minimum</span></div>
    </div>
</div>