@extends("layouts.curator-master")

@section("css")
<style type="text/css">
   label.form-label{
      font-weight: bold;
      margin: 0;
   }
   .text-small{
      padding: 0;
      font-size: 12px;
   }
   #apr-equivalent{
      font-size: 13px;
   }
   .black .btn-white{
      width: 52px;
      border: 1px solid #ced4da;
      padding-top: 10px;
      cursor: inherit !important;
   }
   .btn-white:hover{
      border: 1px solid #ced4da !important;
   }
   .btn-wide{
      width: 100%;
   }
   .required.error,.input-group.error .btn-white{
      border-color: #dc3545;
   }
   .card-success{
      background-color:  rgba(25,135,84,0.2);
   }
   .card-body h2{
      font-size: 150%;
   }
   @media(max-width:  500px){
      .col-md-6{
         width: 49%;
      }
   }
</style>
   
@endsection

@section("content")

<div id="page-content">
   <div class="gradient-wrap">
      <!--  HERO -->
      <div class="black position-relative hero-section mb-4" style="margin: 20px 4em;">
         <div class="row">
            <div class="col-xl-5 col-lg-12 col-md-12 mb-4">
               <h1>APY Calculator</h1>
               <h2 style="font-size:14px; font-weight: inherit;">Use the below APY calculator to determine how much money you'll earn in interest.</h2>
               <div class="card">
                  <div class="card-body" id="apy-calculator-input">
                     <div class="form-group">
                        <label class="form-label">Annual Percentage Yield (APY)</label>
                        <p class="text-small">Commonly used to compare saving accounts and other financial products that pay you.</p>
                        <div class="input-group">
                           <input type="number" inputmode="numeric" class="form-control required" placeholder="4" step=".05" min="0" max="100" maxlength="5" id="apy">
                           <span class="btn btn-white"><i class="fa fa-percent"></i></span>
                        </div>
                        <strong id="apr-equivalent"></strong>
                     </div>
                     <div class="form-group">
                        <label class="form-label">Initial Deposit Amount</label>
                        <p class="text-small">Our calculator works best with no prior balance.</p>
                        <div class="input-group">
                           <span class="btn btn-white"><i class="fa fa-dollar"></i></span><input type="number" inputmode="numeric" class="form-control required" placeholder="1000" min="1" step="1"  id="deposit" maxlength="6" oninput="this.value=this.value.slice(0,this.maxLength)">
                           <span class="btn btn-white">.00</span>
                        </div>
                     </div>
                     <div class="form-group">
                        <label class="form-label">Average Monthly Contributions</label>
                        <p class="text-small">Starting in month #2</p>
                        <div class="input-group">
                           <span class="btn btn-white"><i class="fa fa-dollar"></i></span><input type="number" inputmode="numeric" class="form-control" placeholder="100" min="1" step="1"  id="contribution" maxlength="6" oninput="this.value=this.value.slice(0,this.maxLength)">
                           <span class="btn btn-white">.00</span>
                        </div>
                     </div>
                     <div class="form-group">
                        <label class="form-label">Number of Years</label>
                        <p></p>
                        <div class="input-group">
                           <input type="number" inputmode="numeric" class="form-control required" placeholder="2" min="1" id="duration" step="1" maxlength="2" oninput="this.value=this.value.slice(0,this.maxLength)">
                           <span class="btn btn-white"><i class="fa fa-calendar"></i></span>
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="alert alert-danger hidden">
                           Missing required info or the info provided is incorrectly formatted.
                        </div>
                        <button type="submit" class="btn btn-success btn-wide btn-lg" id="calculate">Calculate</button>
                     </div>
                  </div>
               </div>
               <br>
               <p>This APY calculator is designed for deposits that compound monthly.</p>
            </div>
            <div class="col-xl-7 col-lg-12 col-md-12 hidden" id="apy-results">
               <h2>After <span id="years"></span> Year(s)</h2>
               <div class="row mb-3">
                  <div class="col-lg-6 mb-2">
                     <h5>Without Venti</h5>
                     <div class="row">
                        <div class="col-md-6">
                           <div class="card">
                              <div class="card-body text-center">
                                 <h2 id="endingBalance">$0.00</h2>
                                 <span>Spending Power</span>
                              </div>
                           </div>
                        </div>
                        <div class="col-md-6">
                           <div class="card">
                              <div class="card-body text-center">
                                 <h2 id="earnedInterest">$0.00</h2>
                                 <span>Earned Interest</span>
                              </div>
                           </div>
                        </div>
                     </div>
                     
                  </div>
                  <div class="col-lg-6 mb-2">
                     <h5>Venti's <a href="https://venti.co/boardingpass" target="_blank">9% APY<sup>*</sup> Boarding Pass</a></h5>
                     <div class="row">
                        <div class="col-md-6">
                           <div class="card card-success">
                              <div class="card-body text-center">
                                 <h2 id="endingVentiBalance">$0.00</h2>
                                 <span>Spending Power</span>
                              </div>
                           </div>
                        </div>
                        <div class="col-md-6">
                           <div class="card card-success">
                              <div class="card-body text-center">
                                 <h2 id="earnedVentiInterest">$0.00</h2>
                                 <span>Earned Interest</span>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-md-12">
                     <p id="teaser"></p>
                     <div class="card">
                        <div class="card-body" id="chart-container"></div>
                     </div>
                  </div>
                  <div class="col-md-12">
                  </div>
               </div>
            </div>
         </div>
         <div class="row" style="padding-top:100px">

            <h5>What is a Boarding Pass?</h5>
            <div class="card">
               <div class="card-body">
            <p style="margin-bottom:0">It's the world's first High-Yield Travel Savings Account. Earn interest in the form of points that you can use to get up to 100% off on travel and travel-related products on Venti. Explore the benefits of having a Boarding Pass by searching for <a href="/flights" target="_blank">discounted flights</a>.</p>
         </div>
            </div>
            <p style="padding-top:10px">
               * Annual Percentage Yield (APY) is accurate as of 11/15/2023. Select markets only. APY is compounded and credited monthly. Fees may reduce earnings. Rates are variable and subject to change before and after account opening.
               <br>
               ** Calculated values assume principal and interest remain on deposit and are rounded to the nearest dollar. In addition, calculated values use the current APY, which is variable and may change before and after account opening; your savings will be based on the actual APY, so savings could be less. While there is no limit on interest earned, calculator is designed with certain limits and is for illustrative purposes only.
            </p>
         </div>
      </div>
   </div>
</div>

@endsection

@section("js")
   <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
   <script type="text/javascript">
      $("#calculate").click(function(){
         var apy = parseFloat($("#apy").val())/100;
         var deposit = parseInt($("#deposit").val());
         var frequency = $("#frequency").val();
         var duration = parseInt($("#duration").val());
         var contribution = parseInt($("#contribution").val());
         var passable = false;

         passable = checkRequiredFields();

         if(passable){
            // start computation
            $(".alert-danger").hide();
            $("#chart-container").html("");
            $("#chart-container").append('<canvas id="apy-chart"></canvas>');
            return renderChart(apy, deposit, frequency, duration, contribution);
         } else {
            $(".alert-danger").show();
         }
      });

      function checkRequiredFields(){
         var allClear = true;

         $("#apy-calculator-input .required").each(function(){
            if($(this).val() == "" || $(this).val() == 0 || $(this).val() == undefined){
               $(this).addClass("error");
               $(this).parent().addClass("error");
               allClear = false;
            } else {
               $(this).removeClass("error");
               $(this).parent().removeClass("error");
            }
         });

         return allClear;
      }

      $("#deposit").on("keyup", function(){
         var deposit = parseInt($(this).val());
         $(this).val('');
         $(this).val(deposit);
      });

      $("#contribution").on("keyup", function(){
         var contribution = parseInt($(this).val());
         $(this).val('');
         $(this).val(contribution);
      });

      function renderChart(apy, deposit, frequency, duration, contribution){
         $("#apy-results").show();
         $("#endingBalance").html("0.00");
         $("#netReturn").html("0.00");
         $("#years").text(duration);

         // compute APR
         // Periodic Rate = (1 + APY)1/n - 1
         // always use n = 12 for one-to-one comparison

         var apr = (Math.pow((1 + apy), (1/12)) - 1) * 12;
         apr = Math.round(apr * 10000, 2) / 100;

         $("#apr-equivalent").text("Approximately equal to an APR of " + apr + "%");

         var ctx = document.getElementById('apy-chart');
         var chart = $("#apy-chart");

         ctx = document.getElementById('apy-chart');

         var labels = [];
         var data = [];
         var ventiData = [];

         var principal = deposit;
         var ventiPrincipal = deposit;
         const rate = apr/100; // APR
         const ventiRate = 0.0865; // 9% APY
         durationN = duration * 12; // convert years to months


         const time = 1/12; // 1 year
         const n = 1; // compounded monthly

         for(i = 1; i <= durationN; i++){
            labels.push(i);

            var compoundInterestAdded = compoundInterest(principal, time, rate, n);
            principal += compoundInterestAdded;

            if(i > 1){
               // after the initial month we factor in additional contributions
               principal += contribution;
            }

            data.push(Math.round(principal,2));

            compoundInterestAdded = compoundInterest(ventiPrincipal, time, ventiRate, n);
            ventiPrincipal +=  compoundInterestAdded;

            if(i > 1){
               ventiPrincipal += contribution;
            }

            ventiData.push(Math.round(ventiPrincipal));
         }

         // update totals

         var earnedInterestDifference = (ventiPrincipal - deposit) - (principal - deposit);
         if(earnedInterestDifference > 0){
            earnedInterestDifference =  earnedInterestDifference.toLocaleString(undefined, {
                  minimumFractionDigits: 2,
                  maximumFractionDigits: 2
                });
            $("#teaser").html('With a 9% APY <a href="https://venti.co/boardingpass" target="_blank">Boarding Pass</a>, you\'d earn $' + earnedInterestDifference + " more in purchasing power over " + duration + " year(s).");
         }

         $("#endingBalance").text("$" + principal.toLocaleString(undefined, {
                  minimumFractionDigits: 2,
                  maximumFractionDigits: 2
                }));
         $("#earnedInterest").text("$" + (principal - deposit).toLocaleString(undefined, {
                  minimumFractionDigits: 2,
                  maximumFractionDigits: 2
                }));
         $("#endingVentiBalance").text("$" + ventiPrincipal.toLocaleString(undefined, {
                  minimumFractionDigits: 2,
                  maximumFractionDigits: 2
                }));
         $("#earnedVentiInterest").text("$" + (ventiPrincipal - deposit).toLocaleString(undefined, {
                  minimumFractionDigits: 2,
                  maximumFractionDigits: 2
                }));

         new Chart(ctx, {
          type: 'line',
          data: {
            labels: labels,
            datasets: [
            {
              label: 'Your Balance',
              data: data,
              borderWidth: 1
            },
            {
              label: 'With Venti',
              data: ventiData,
              borderWidth: 1
            },
            ]
          },

          options: {
            tooltips: {
             callbacks: {
                   label: function(tooltipItem, data) {
                     console.log(data);
                       var value = data.datasets[0].data[tooltipItem.index];
                       if(parseInt(value) >= 1000){
                         return '$' + value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                      } else {
                         return '$' + value;
                      }
                   }
             } // end callbacks:
           }, //end tooltips 
            scales: {
              y: {
                beginAtZero: false,

                title: {
                  display:true,
                  text:"Balance"
                },
                ticks:{
                  callback: function(value, index, ticks) {
                        return '$' + value.toLocaleString();
                  }
                }
              },
              x:{


                title: {
                  display:true,
                  text:"Months"
                }
              }
            }
          }
        });
      }

      function compoundInterest(p, t, r, n){
         const amount = p * (Math.pow((1 + (r / n)), (n * t)));
         const interest = amount - p;
         return interest;
      };
   </script>
@endsection