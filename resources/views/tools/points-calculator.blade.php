<div class="row">
    <div class="col-md-12">
        <p class="section-head-body sign-up-box" style="font-size:1.3em; font-weight: bold; margin-bottom:0; padding-bottom:0">OUR POINTS CALCULATOR<sup>**</sup></p>
        <p class="section-head-body" style="">No minimum deposit required.<br></p>
    </div>
    <div class="col-md-8 offset-md-2"> 
        <div class="text-left">
            <div class="col-md-12 mb-4 ">
                <label>Initial Deposit</label>
                <input type="text" id="pool-amount" class="calculate" 
                  data-provide="slider"
                  data-slider-ticks="[0, 1, 2, 3, 4, 5]"
                  data-slider-ticks-labels='["$0", "$2,000", "$4,000", "$6,000", "$8,000", "$10,000"]'
                  data-slider-min="1"
                  data-slider-max="6"
                  data-slider-step="1"
                  data-slider-value="1"
                  data-slider-tooltip="hide" />
            </div>
        </div>
    </div>
    <div class="col-md-8 offset-md-2">
        <div class="text-left">
            <div class="col-md-12 mb-4 ">
                <label>Additional Monthly Deposits</label>
                <input type="text" id="pool-deposits" class="calculate" 
                  data-provide="slider"
                  data-slider-ticks="[0, 1, 2, 3, 4, 5]"
                  data-slider-ticks-labels='["$0", "$50", "$100", "$150", "$200", "$250"]'
                  data-slider-min="1"
                  data-slider-max="6"
                  data-slider-step="1"
                  data-slider-value="0"
                  data-slider-tooltip="hide" />
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <p class="section-head-body" style="font-size:1.3em; font-weight: bold; margin-bottom:0; padding-bottom:0">After 12 Months, You'll Have:</p>
        <br>
    </div>
    <div class="col-md-6"><div style="background-color:rgba(49, 127, 131, 0.2); padding:20px; border-radius: 20px; font-weight:bold; margin-bottom: 10px;"><span style="font-size:36px">$<span id="balance" >2,000</span></span><br><span style="font-size:12px;">Withdrawable To Your Bank</span></div>
    </div>
    <div class="col-md-6"><div style="background-color:rgba(252, 166, 76, 0.2); padding:20px; border-radius: 20px; font-weight:bold; ">
        <span style="font-size: 36px;">$<span id="points">154</span></span><br><span style="font-size:12px; line-height: 0.9"> In Travel Points</span></div>
    </div>
</div>