@extends('layouts.loan-master')

@section('content')
	<div class="form-group">
		<label>Do you have a savings account?</label>
		<select name="savingsAccount" class="form-control required" required="">
			<option value="">-- Please Select --</option>
			<option value="yes">Yes</option>
			<option value="no">No</option>
			<option value="nobank">I do not have a bank account</option>
		</select>
	</div>

	<div class="form-group">
		<label>On average, how many times per month do you deposit into your savings account?</label>
		<select name="depositRate" class="form-control required" required="">
			<option value="">-- Please Select --</option>
			<option value="once_a_week">Weekly</option>
			<option value="twice_a_month">Twice a month</option>
			<option value="once_a_month">Once a month</option>
			<option value="infrequently">Infrequently throughout the year</option>
			<option value="no_deposits">I never deposit into it</option>
			<option value="nobank">I do not have a savings account</option>
		</select>
	</div>

	<div class="form-group">
		<label>Have you used a budgeting or savings app before?</label>
		<select name="budgetAppUse" class="form-control required" required="">
			<option value="">-- Please Select --</option>
			<option value="yes">Yes</option>
			<option value="no">No</option>
		</select>
	</div>

	<div class="form-group">
		<label for="surveycode">What is your level of interest in using a budgeting/savings app?</label>
		<select class="form-control required" name="interestInBudgetApp" required="">
			<option value="">-- Please Select One--</option>
			<option value="extremely">6 - Extremely Interested</option>
			<option value="very">5 - Very Interested</option>
			<option value="some">4 - Somewhat Interested</option>
			<option value="neutral">3 - Neutral Interest</option>
			<option value="indifferent">2 - Indifferent</option>
			<option value="not">1 - Not Interested</option>
		</select>
	</div>

	<div class="form-group">
		<label>On average, how many international trips do you take per year</label>
		<input type="number" min="0" max="99999" required="" class="form-control" name="tripsAbroad">
	</div>

	<div class="form-group">
		<label>On average, how many domestic trips do you take per year</label>
		<input type="number" min="0" max="99999" required="" class="form-control" name="tripsDomestic">
	</div>

	<div class="form-group">
		<label>Is cost the primary reason you do not travel more often?</label>
		<select name="isCostPrimaryReason" class="form-control required" required="">
			<option value="">-- Please Select --</option>
			<option value="yes">Yes</option>
			<option value="no">No</option>
		</select>
	</div>

	<div class="form-group">
		<label for="surveycode">What is your level of interest in using a free app to help you budget for travel?</label>
		<select class="form-control required" name="interestInTravelApp" required="">
			<option value="">-- Please Select One--</option>
			<option value="extremely">6 - Extremely Interested</option>
			<option value="very">5 - Very Interested</option>
			<option value="some">4 - Somewhat Interested</option>
			<option value="neutral">3 - Neutral Interest</option>
			<option value="indifferent">2 - Indifferent</option>
			<option value="not">1 - Not Interested</option>
		</select>
	</div>

	<div class="form-group">
		<label for="surveycode">What is your level of interest in using a free app to help you find travel and flight deals?</label>
		<select class="form-control required" name="interestInTravelDeals" required="">
			<option value="">-- Please Select One--</option>
			<option value="extremely">6 - Extremely Interested</option>
			<option value="very">5 - Very Interested</option>
			<option value="some">4 - Somewhat Interested</option>
			<option value="neutral">3 - Neutral Interest</option>
			<option value="indifferent">2 - Indifferent</option>
			<option value="not">1 - Not Interested</option>
		</select>
	</div>

	<div class="form-group">
		<label>Marital/Relationship Status</label>
		<select name="maritalStatus" class="form-control required" required="">
			<option value="">-- Please Select --</option>
			<option value="married">Married</option>
			<option value="engaged">Engaged</option>
			<option value="single">Single</option>
			<option value="separated">Recently Separated/Divorced</option>
		</select>
	</div>

	<div class="form-group">
		<label>What is your highest level of education?</label>
		<select name="levelOfEducation" class="form-control required" required="">
			<option value="">-- Please Select --</option>
			<option value="highschool">High School</option>
			<option value="vocational">Vocational or Some College</option>
			<option value="associates">Associates</option>
			<option value="bachelor">Bachelor's</option>
			<option value="masters">Master's</option>
			<option value="professional">Professional (JD, MD)</option>
			<option value="doctorate">Doctorate</option>
		</select>
	</div>

	<div class="form-group">
		<label>What is your household income?</label>
		<select name="income" class="form-control required" required="">
			<option value="">-- Please Select --</option>
			<option value="25000">Less than $25,000</option>
			<option value="50000">$25,000 - $50,000</option>
			<option value="100000">$50,000 - $100,000</option>
			<option value="200000">$100,000 - $200,000</option>
			<option value="300000">More than $200,000</option>
		</select>
	</div>

	<div class="form-group">
		<label>How many children do you have?</label>
		<input type="number" min="0" max="99999" required="" class="form-control" name="numChildren">
	</div>


	<div class="form-group">
		<label>Which social media app do you use the most?</label>
		<select name="socialMedia" class="form-control required" required="">
			<option value="">-- Please Select --</option>
			<option value="twitter">Twitter</option>
			<option value="facebook">Facebook</option>
			<option value="instagram">Instagram</option>
			<option value="tiktok">TikTok</option>
			<option value="snapchat">Snapchat</option>
			<option value="reddit">Reddit</option>
			<option value="none">None of the above</option>
		</select>
	</div>

	<div class="form-group">
		<label>Please enter your zip code</label>
		<input type="number" min="1000" max="99999" required="" class="form-control" name="zipcode">
	</div>
@endsection
