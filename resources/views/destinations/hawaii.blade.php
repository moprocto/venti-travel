@extends("layouts.destination")

@section('hero')
	<!-- Cover Start -->
		<section id="cover">
		    <div class="container">
		        <div class="row">
		            <div class="col-md-7 cover-image align-self-center">
		                <img src="/assets/loan/img/maldives-bg-square.jpg" class="img-fluid" alt="product">
		            </div>
		            <div class="col-md-5 align-self-center cover-body">
		                <h1>The Black Card<br>of Travel</h1>
		                <p></p>
		                <h5 class="font-light">Travel now. Pay later. Earn rewards. We turn your dream vacation into an affordable, all-inclusive trip. </h5>
		                <p class="section-head-body">We're launching in 2022. Early birds may get 0% APR for life with all the perks.</p>
		                @include('forms.waitlist')
		            </div>
		        </div>
		    </div>
		</section>
	<!-- Cover End -->
@endsection