@extends('layouts.curator-master')

@section('css')
    <style type="text/css">
        html{
            overflow-x: hidden;
        }
        @media(max-width: 1368px ){
            .tb-full-width{
                width: 100%;
            }
        }
        .hamburger-menu-list, .footer-lists, .right-nav, footer .text{
            display: none !important;
        }
        .about-body{
            justify-content: center;
        }
        @media(min-width: 1367px){
            {
                margin-left: 22px;
            }
            .row.about-body{
                margin-bottom: 22px;
            }
            .desktop-hide{
                display: none;
            }
        }
        @media(max-width: 800px){
            .mb-pd-20{
                padding-bottom: 20px;
            }
        }
        @media(max-width: 1368px){
            .mobile-block{
                display: block !important;
            }
            .mobile-hide{
                display: none;
            }
            #about .about-body .about-body-item .about-body-icon-cnt{
                justify-content: left !important;
                margin-bottom: 10px;
            }
            .pills-item{
                background-position-y: inherit !important;
            }
            .sign-up-box{
                padding-top: 80px;
                padding-bottom:  0;
            }
        }
        .about-body-icon-cnt{
            border: 1px solid black;
            border-radius: 20px;
            padding: 5px 10px;
        }
        

        .font-light{
            font-weight: 300 !important;
        }
        .full-width{
            max-width: 9999px !important;
            padding-left:  0;
            padding-right: 0;
        }
        #about{
            padding-top: 0;
        }
        .pd-20{
            padding: 80px 40px;
        }
        i.text-small{
            font-size: 16px;
        }
        .about-body-icon-cnt, .about-body-text-head{
            padding-left: 14px;
        }
        .about-body-text-head{
            padding-right: 15px;
            font-weight: bold;
        }
        @media(min-width: 1367px){
            {
                margin-left: 22px;
            }
            .row.about-body{
                margin-bottom: 22px;
            }
            .desktop-hide{
                display: none;
            }
        }
        @media(max-width: 768px){
            .mobile-hide{
                display: none;
            }


        }
        @media(max-width: 468px){
            #minutes, #seconds{
                display: none;
            }
        }   
        @media(max-width: 1368px){
            .mh-500-sm{
                max-height: 500px;
            }
            .mobile-block{
                display: block !important;
            }
            .mobile-hide{
                display: none;
            }
            #about .about-body .about-body-item .about-body-icon-cnt{
                justify-content: left !important;
                margin-bottom: 10px;
            }
            .pills-item{
                background-position-y: inherit !important;
            }
            .sign-up-box{
                padding-top: 80px;
                padding-bottom:  0;
            }
            .preview-mh{
                width: 143% !important;
                margin-left: -73px;
            }
        }

        .text-gold{
            color: #9D8A7C !important;
        }

        .text-large{
            font-size: 20px;
        }

        ol.pills-list{
            display: block;
            padding: 0 0 0 26px;
            list-style: none;
            overflow: hidden;
        }

        .pills-list li{
            display: flex;
            justify-content: space-between;
            align-items: center;
            margin-bottom: 10px;
            padding: 10px 20px 5px 20px;
        }
        .sand-badge{
            background-color: rgba(157, 138, 124, 0.7);
            opacity: 0.8;
            color: white;
            font-size: 22px;
            border-radius: 50%;
            width: 42px;
            height: 40px;
            letter-spacing: 1;
        }
        .pills-item{
            padding: 7px 20px;
            border-radius: 10px;
            margin-bottom: 20px;
            background-color: whitesmoke;
        }
        .pills-item div{
            padding-top: 10px;
        }
        .pills-item p{
            padding-top: 3px;
        }
        .pills-item-1{
            background-position-y: 0;
        }

        .pills-item-2{
            background-position-y: -90px;
        }
        .pills-item-3{
            background-position-y: -180px;
        }
        .pills-item-4{
            background-position-y: -220px;
        }
        li.pills-item-3{
            background: #d7d0ca;
        }
#mc_embed_signup .text-small{
    font-size: 12px !important;
}

#subscribe .text-small{
    margin-top: 15px;
    
}
.about-body-item{
    margin-bottom: 10px;
}.slider-horizontal{
            width: 100% !important;
          }

          #faqs
{
padding-bottom: 100px;
}
#faqs .faqs-container .faq
{
    border-top: 1px solid var(--color-border);
    padding: 15px;
}
#faqs .faqs-container .faq .faq-head
{
    padding: 15px 0px;
    position: relative;
}
#faqs .faqs-container .faq .faq-head h5
{
    width: 90%;
    cursor: pointer;
}
#faqs .faqs-container .faq .faq-head h5::before
{
    content: 'keyboard_arrow_down';
    text-align: center;
    font-size: 20px;
    line-height: 27px;
    font-family: 'Material Icons Round';
    height: 25px;
    width: 25px;
    color: #fefefe;
    background: var(--color-sec);
    position: absolute;
    top: 50%;
    right: 0;
    transform: translateY(-50%);
    border-radius: 50%;
    transition: 0.5s;
}
#faqs .faqs-container .faq .faq-head h5[aria-expanded=true]::before
{
    content: 'keyboard_arrow_up';
}
.form-container .emailoctopus-form-wrapper .emailoctopus-success-message{
    color:  orange !important;
    font-size: 18px;
    padding-bottom: 20px;
}
.accordion-item{
    margin-bottom: 10px;
}
#countdown{
    position: fixed;
    z-index: 999;
    height: 50px;
    background: white;
    width: 100%;
    bottom: 0;
    left: 0;
    display: flex;
    flex-direction: row;
    justify-content: center;
    padding: 10px 20px;
    font-weight: bold;
    font-size: 0.9em;
    align-items: center;
}
#countdown div.descrtiption{
    text-align: center;
    font-size: 0.8em;
    padding: 10px;
}
#timer{
    font-weight: bold;
    display: flex;
    justify-content: space-between;
}
#timer *{
    padding-right: 4px;
}
.right-nav{
    display: none;
}
    </style>
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/11.0.2/css/bootstrap-slider.min.css">

<link href="https://fonts.googleapis.com/icon?family=Material+Icons+Round" rel="stylesheet">
            <!-- Main CSS -->

            <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>

@endsection

@php
    $vips = 2132;
    $vip = number_format($vips, 0);
    $counter = $vips * 20;
@endphp

@section('content')
<div id="backdrop" style="margin-top: -130px; width: 100%; z-index: 1; display: flex; align-items: center; min-height: 800px; background-position: center;  background-size: cover; margin-bottom: -64px; background-image:url('/assets/img/boarding-pass-bg2.jpg')">
        <div class="gradient-wrap" style="background: transparent; width:100%;">
            <!--  HERO -->
            <div class="section-wrapper black position-relative">
                <div class="row" style="align-items:center;">
                    <div class="col-lg-5 col-md-12 col-sm-12 col-12 top-header" style="display: flex; align-items:center;">
                        <div id="hero-text-wrapper" style="max-width:900px;">
                            <h1 class="mb-3 section-title" style="line-height: 4rem; color: white; font-weight:600 !important; font-size: 3.8rem;">
                            Earn 8% APY<sup>*</sup>
                            <span style="font-size: 0.45em; display:block;">With a Venti Boarding Pass</span>
                            </h1>
                            <h2 class="section-subtitle mb-4" style="color: white; max-width:900px;">
                            The highest paying travel savings program. Ready for you. Ready for the world.</h2>
                         <p class="section-head-body" style="color:white;">We're launching in October. Join our VIP list to pay no annual fee for the first two years.</p>
                         <a href="#waitlist" class="btn btn-primary">JOIN VIP LIST</a>
                         <span style="display:block; color:white; font-size:10px; font-weight: bold;margin-top: 5px;">{{ $vip }} VIPS AND COUNTING</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<!-- Cover End -->
 <!-- About Start -->

 <!-- Start FAQs -->
    <section id="" style="padding-top: 100px; padding-bottom: 50px;">
        <div class="container full-width">
            <div class="row">
                <div class="col-md-12  text-center pd-20 text-center" style="padding-bottom: 0; margin-bottom: -3px; padding:40px 20px;" >
                    <h5 style="font-weight: 800; font-size: 16px">WE'RE ON A MISSION TO SAVE TRAVELERS</h5>
                    <h2 class="section-head-title text-dark mb-3 section-title" style="margin-top:0.5em; font-size: 5rem;">$1 Billion</h2>
                    <h3 class="section-head-body">With a savings rate that beats every bank</h3>

                    <img src="/assets/img/apy-comparison.png" style="width:100%; max-width:600px; margin-top:25px; margin-bottom:25px">
                    <br><br>
                    <img src="/assets/img/save-big.jpg" style="width:100%; max-width: 800px; margin-bottom:-87px;">
                </div>
            </div>
        </div>
    </section>
    <!-- End FAQs -->

    <section id="about" style="padding-bottom: 0;">
        <div class="container full-width">
            <div class="row" style="background-color: #f7f9ff;">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 text-center pd-20" style="padding-bottom: 0; margin-bottom: -3px; padding:40px;" >
                    <h2 class="section-head-title text-dark mb-3 section-title" style="margin:40px; margin-top:20px">How it Works</h2>
                    <div class="row text-dark text-left pills-item pills-item-1">
                        <div class="col-md-1 text-center">
                            <span class="badge sand-badge">1</span>
                        </div>
                        <div class="col-md-9">
                            <p class="text-dark text-large">You deposit funds into a secure, FDIC insured account completely controlled by you. With no lock-up period, you can withdraw from your account at any time.</p>
                        </div>
                        <div class="col-md-2 text-center mb-pd-20" style="display: flex; align-items: center; justify-content:center;">
                            <img src="/assets/img/virtual-wallet.png" style="width:50px;">
                        </div>
                    </div>
                    <div class="row text-dark text-left pills-item pills-item-2">
                        <div class="col-md-1 text-center">
                            <span class="badge sand-badge">2</span>
                        </div>
                        <div class="col-md-9">
                            <p class="text-dark text-large">Interest accrued by your account is awarded as points. <strong>1 Point = $1 towards travel</strong>. The more you save, the more points you earn. </p>
                        </div>
                        <div class="col-md-2 text-center mb-pd-20" style="display: flex; align-items: center; justify-content:center;">
                            <img src="/assets/img/earn-rewards.png" style="width:50px;">
                        </div>
                    </div>
                    <div class="row text-dark text-left pills-item pills-item-3">
                        <div class="col-md-1 text-center">
                            <span class="badge sand-badge">3</span>
                        </div>
                        <div class="col-md-9">
                            <p class="text-dark text-large">Points can only be used for travel-related purchases on the Venti platform. Points never expire and can be transferred to friends and family.</p>
                        </div>
                        <div class="col-md-2 text-center mb-pd-20" style="display: flex; align-items: center; justify-content:center;">
                            <img src="/assets/img/tourism.png" style="width:50px;">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <p><a href="#waitlist">Become a VIP</a> before Jan. 1, 2024, and we'll waive our $40 annual fee for two years. Otherwise, your account can be effectively free if you maintain a Boarding Pass balance of at least $500.</p>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 full-width">
                    <img src="/assets/img/how-it-works-m.jpg" style="width: 100%;" class="">
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 full-width mobile-hide">

                    <img src="/assets/img/pays-to-save.jpg" style="width: 100%; height:100%;">
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 text-center" style="padding-bottom: 0; margin-bottom: -3px; padding:40px;" >
                    <h2 class="section-head-title text-dark mb-3 section-title" style="margin:40px; margin-top:20px">It Pays to Save</h2>
                    <p class="section-head-body" style="font-size:1.2em">Redeem Boarding Pass points for a wide range of travel-related purchases.</p>

                    <div class="row about-body">
                        <div class="about-body-item d-flex mobile-block align-items-center">
                            <div class="tb-full-width">
                                <div class="about-body-icon-cnt">
                                    <i class="material-icons-round about-body-icon fas fa-plane text-small"></i>
                                    <span class="about-body-text-head">AIRFARE</span>
                                </div>
                            </div>
                        </div>
                        <div class="about-body-item d-flex mobile-block align-items-center">
                            <div class="tb-full-width">
                                <div class="about-body-icon-cnt">
                                    <i class="material-icons-round about-body-icon fas fa-ship text-small"></i>
                                    <span class="about-body-text-head">CRUISES</span>
                                </div>
                            </div>
                        </div>
                        <div class="about-body-item d-flex mobile-block align-items-center">
                            <div class="tb-full-width">
                                <div class="about-body-icon-cnt">
                                    <i class="material-icons-round about-body-icon fas fa-hotel text-small"></i>
                                    <span class="about-body-text-head">LODGING</span>
                                </div>
                            </div>
                        </div>
                        <div class="about-body-item d-flex mobile-block align-items-center">
                            <div class="tb-full-width">
                                <div class="about-body-icon-cnt">
                                    <i class="material-icons-round about-body-icon fas fa-car text-small"></i>
                                    <span class="about-body-text-head">RENTALS</span>
                                </div>
                            </div>
                        </div>
                        <div class="about-body-item d-flex mobile-block align-items-center">
                            <div class="tb-full-width">
                                <div class="about-body-icon-cnt">
                                    <i class="material-icons-round about-body-icon fas fa-binoculars text-small"></i>
                                    <span class="about-body-text-head">TOURS</span>
                                </div>
                            </div>
                        </div>
                        <div class="about-body-item d-flex mobile-block align-items-center">
                            <div class="tb-full-width">
                                <div class="about-body-icon-cnt">
                                    <i class="material-icons-round about-body-icon fas fa-skiing text-small"></i>
                                    <span class="about-body-text-head">PACKAGES</span>
                                </div>
                            </div>
                        </div>
                        <div class="about-body-item d-flex mobile-block align-items-center">
                            <div class="tb-full-width">
                                <div class="about-body-icon-cnt">
                                    <i class="material-icons-round about-body-icon fas fa-stethoscope text-small"></i>
                                    <span class="about-body-text-head">INSURANCE</span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <p class="section-head-body sign-up-box" style="font-size:1.3em; font-weight: bold; margin-bottom:0; padding-bottom:0">Calculate Your Savings<sup>**</sup></p>
                            <p class="section-head-body" style="">No minimum deposit required. Interest compounded monthly.<br></p>
                        </div>
                        <div class="col-md-8 offset-md-2"> 
                            <div class="text-left">
                                <div class="col-md-12 mb-4 ">
                                    <label>Initial Deposit</label>
                                    <input type="text" id="pool-amount" class="calculate" 
                                      data-provide="slider"
                                      data-slider-ticks="[0, 1, 2, 3, 4, 5]"
                                      data-slider-ticks-labels='["$0", "$2,000", "$4,000", "$6,000", "$8,000", "$10,000"]'
                                      data-slider-min="1"
                                      data-slider-max="6"
                                      data-slider-step="1"
                                      data-slider-value="1"
                                      data-slider-tooltip="hide" />
                                </div>
                            </div>
                        </div>
                        <div class="col-md-8 offset-md-2">
                            <div class="text-left">
                                <div class="col-md-12 mb-4 ">
                                    <label>Additional Monthly Deposits</label>
                                    <input type="text" id="pool-deposits" class="calculate" 
                                      data-provide="slider"
                                      data-slider-ticks="[0, 1, 2, 3, 4, 5]"
                                      data-slider-ticks-labels='["$0", "$50", "$100", "$150", "$200", "$250"]'
                                      data-slider-min="1"
                                      data-slider-max="6"
                                      data-slider-step="1"
                                      data-slider-value="0"
                                      data-slider-tooltip="hide" />
                                </div>
                            </div>
                        </div>
                        
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <p class="section-head-body" style="font-size:1.3em; font-weight: bold; margin-bottom:0; padding-bottom:0">After 12 Months, You'll Have:</p>
                            <br>
                        </div>
                        <div class="col-md-6"><div style="background-color:rgba(49, 127, 131, 0.2); padding:20px; border-radius: 20px; font-weight:bold; margin-bottom: 10px;"><span style="font-size:36px">$<span id="balance" >2,000</span></span><br><span style="font-size:12px;">Withdrawable To Your Bank</span></div></div>
                        <div class="col-md-6"><div style="background-color:rgba(252, 166, 76, 0.2); padding:20px; border-radius: 20px; font-weight:bold; "><span style="font-size: 36px;"><span id="points">154</span> POINTS</span><br><span style="font-size:12px; line-height: 0.9">1 Point = $1 Towards Travel</span></div></div>
                        </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 desktop-hide full-width">
                    <img src="/assets/img/pays-to-save.jpg" style="width: 100%;">
                </div>
            </div>
            
        </div>
    </section>
    <!-- About End -->

    <!-- End Preview -->

    <section id="waitlist">
        <div class="container full-width" style="background-image:url('/assets/img/rio-bg.jpg'); background-position: center;">
            <div class="row" >
                <div class="col-md-12  text-center pd-20 full-width" style="margin-bottom: -3px; padding:40px; justify-content: center; padding-bottom: 0; " >
                    <h2 class="section-head-title text-dark mb-3 section-title" style="font-size: 4rem; margin-top:20px; color:white !important">$<span class="" id="currency">10</span></h2>
                    <h5 class="section-head-body" style="color:white; max-width: 800px; justify-content:center; margin: 0 auto;">in points already pre-claimed by {{ $vip }}+ VIPs so far<br><br>Join our fast-growing list by December 31, 2023, and we'll credit your Boarding Pass with 20 Points when you make your first deposit.</h5>
                </div>
            </div>
            <div class="row" style="justify-content: center;">
                <form method="POST" action="{{ route('boarding-test') }}" style="margin-top: 40px;" class="text-center">
                    @CSRF

                    <input type="hidden" name="deposit" id="deposit">
                    <input type="hidden" name="contribution" id="contribution">
                    <input type="hidden" name="balanceFinal" id="balanceFinal">
                    <input type="hidden" name="pointsFinal" id="pointsFinal">

                    <div class="form-group">
                        <input type="text" name="email" required="" class="form-control" placeholder="your@email.com" style="min-width: 325px;">
                    </div>
                    <div class="form-group">
                        <input type="submit" class="btn btn-success btn-wide" name="submit" value="SUBMIT" style="min-width: 225px;">
                    </div>
                </form>
            </div>
            <br>
        </div>
    </section>

    <section id="preview" style="padding-bottom: 0;">
        <div class="container full-width">
            <div class="row" style="background-color: #f7f9ff;">
                <div class="col-md-6 text-center pd-20" style="flex-direction: column; display: flex; align-self: center; padding-bottom: 0; margin-bottom: -3px; padding:40px;" >
                    <h2 class="section-head-title text-dark mb-3 section-title" style="margin:40px; margin-top:20px">Track. Book. Save.</h2>
                    <h5 style="font-weight: 800; font-size: 16px; text-transform:uppercase;">Always on the go. Just like you.</h5>
                    <div class="row text-dark text-center pills-item pills-item-1" style="max-width: 500px; margin: 0 auto; margin-top: 20px;background-color:rgba(157, 138, 124, 0.2)">
                        <div class="col-md-12">
                            <p class="text-dark text-large">Track your savings progress, redeem points, and send/receive funds right from a convenient and secure mobile app. <br><br> <span style="font-weight:bold">Coming soon to iOS and Android</span></p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 full-width">
                    <img src="/assets/img/app-preview.png" style="width: 100%;" class="preview-mh">
                </div>
            </div>
            
        </div>
    </section>
    <!-- About End -->

    <section id="press" style="padding-bottom:40px; background:white">
        <div class="container">
            <div class="row">
                <div class="col-md-12  text-center pd-20 text-center" style="padding-bottom: 0; margin-bottom: -3px; padding:40px;" >
                    <h2 class="section-head-title text-dark mb-3 section-title" style="margin:40px; margin-top:20px">WE'VE BEEN FEATURED</h2>
                    <h5 style="font-weight: 800; font-size: 16px; text-transform:uppercase;">SPECIAL THANKS TO OUR FRIENDS AND FELLOW SAVERS</h5>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 text-center">
                    <img src="/assets/img/doctorofcredit.jpg" style="width: 100%; max-width:200px;">
                </div>
                <div class="col-md-4 text-center">
                    <img src="/assets/img/pointsbuzz.jpg" style="width: 100%;  max-width:200px;">
                </div>
                <div class="col-md-4 text-center">
                    <img src="/assets/img/frugalflyer.jpg" style="width: 100%;  max-width:200px;">
                </div>
            </div>
        </div>
    </section>

    <!-- Start FAQs -->
    <section id="faqs" style="padding-bottom:0">
        <div class="container">
            <div class="row">
                <div class="col-md-12  text-center pd-20 text-center" style="padding-bottom: 0; margin-bottom: -3px; padding:40px;" >
                    <h2 class="section-head-title text-dark mb-3 section-title" style="margin:40px; margin-top:20px">FAQs</h2>
                    <h5 style="font-weight: 800; font-size: 16px; text-transform:uppercase;">It's in our DNA to be as transparent as possible</h5>
                </div>
            </div>

            <div class="accordion accordion-flush" id="faqs" style="padding-bottom:0">
              
                <div class="accordion-item">
                <h2 class="accordion-header" id="flush-headingFive">
                  <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseFive" aria-expanded="false" aria-controls="flush-collapseFive">
                    Where exactly are my funds kept? Are my funds FDIC insured?
                  </button>
                </h2>
                <div id="flush-collapseFive" class="accordion-collapse collapse" aria-labelledby="flush-headingFive" data-bs-parent="#accordionFlushExample">
                  <div class="accordion-body"><p>When you make a deposit onto your Boarding Pass, the funds are stored with a trusted third-party custodian that has accounts with <strong>Veridian Credit Union</strong>, <strong>Evolve Bank & Trust</strong>, and <strong>Cross River Bank</strong>. All three banks are based in the U.S. and are FDIC insured. Venti never has direct access to your funds.</p></div>
                </div>
              </div>
              <div class="accordion-item">
                <h2 class="accordion-header" id="flush-headingEight">
                  <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseEight" aria-expanded="false" aria-controls="flush-collapseEight">
                    Why use this over a travel-focused credit card?
                  </button>
                </h2>
                <div id="flush-collapseEight" class="accordion-collapse collapse" aria-labelledby="flush-headingEight" data-bs-parent="#accordionFlushExample">
                  <div class="accordion-body"><p>We get this question all the time, especially since we're competing with popular credit cards for your travel purchases. Essentially, this is an apples vs. oranges debate and cannot be compared directly. We philosophically oppose the idea that paying nearly 30% interest to get "2% cashback" is the best way to accrue travel benefits or discounted travel. If the big banks have you strongly convinced that their math helps you achieve your financial goals, then Venti might not be for you. Regardless of their advertised benefits, we will never promote high-interest credit cards to our customers.</p></div>
                </div>
              </div>
              <div class="accordion-item">
                <h2 class="accordion-header" id="flush-headingOne">
                  <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseOne" aria-expanded="false" aria-controls="flush-collapseOne">
                    How does Venti make money?
                  </button>
                </h2>
                <div id="flush-collapseOne" class="accordion-collapse collapse" aria-labelledby="flush-headingOne" data-bs-parent="#accordionFlushExample">
                  <div class="accordion-body"><p>We generate revenue by selling flights, hotel reservations, car rentals, and other travel-related products and services on our platform.</p></div>
                </div>
              </div>
              
              <div class="accordion-item">
                <h2 class="accordion-header" id="flush-headingThree">
                  <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseThree" aria-expanded="false" aria-controls="flush-collapseThree">
                    What are the fees for this program?
                  </button>
                </h2>
                <div id="flush-collapseThree" class="accordion-collapse collapse" aria-labelledby="flush-headingThree" data-bs-parent="#accordionFlushExample">
                  <div class="accordion-body"><p>For those that join after our promotional period (ending December 31, 2023), we require a $39.99 a year subscription to maintain a Venti Boarding Pass. This fee can be paid via 40 points if you retain a balance of at least $500 for 12 months.</p><p>Deposits and withdrawals from your bank account via ACH are free.</p><p>Instant withdrawals from your Boarding Pass to your bank via debit card are subject to a 3% transaction fee not to exceed $50 per transaction and no lesser than $1.</p></div>
                </div>
              </div>
              
              <div class="accordion-item">
                <h2 class="accordion-header" id="flush-headingSix">
                  <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseSix" aria-expanded="false" aria-controls="flush-collapseSix">
                    Can I get reimbursed for transactions that occur off the Venti Platform?
                  </button>
                </h2>
                <div id="flush-collapseSix" class="accordion-collapse collapse" aria-labelledby="flush-headingSix" data-bs-parent="#accordionFlushExample">
                  <div class="accordion-body"><p>No, we do not allow rebates or reimbursements for off-platform transactions. Exceptions may apply in certain circumstances.</p></div>
                </div>
              </div>
              <div class="accordion-item">
                <h2 class="accordion-header" id="flush-headingSeven">
                  <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseSeven" aria-expanded="false" aria-controls="flush-collapseSeven">
                    Can my child have an account in their name?
                  </button>
                </h2>
                <div id="flush-collapseSeven" class="accordion-collapse collapse" aria-labelledby="flush-headingSeven" data-bs-parent="#accordionFlushExample">
                  <div class="accordion-body"><p>No, the primary account holder must be at least 18 years of age. As a parent, you can create an account in your name and make purchases on behalf of your child(ren), such as airfare.</p></div>
                </div>
              </div>
              <div class="accordion-item">
                <h2 class="accordion-header" id="flush-headingTwo">
                  <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseTwo" aria-expanded="false" aria-controls="flush-collapseTwo">
                    Is there a limit on how much I can deposit onto my Boarding Pass?
                  </button>
                </h2>
                <div id="flush-collapseTwo" class="accordion-collapse collapse" aria-labelledby="flush-headingTwo" data-bs-parent="#accordionFlushExample">
                  <div class="accordion-body"><p>Yes, the maximum balance you can have is $25,000. You will continue to accrue points even if you've maxed out your Boarding Pass.</p></div>
                </div>
              </div>
              
              <div class="accordion-item">
                <h2 class="accordion-header" id="flush-headingNine">
                  <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseNine" aria-expanded="false" aria-controls="flush-collapseNine">
                    How do you handle international currencies?
                  </button>
                </h2>
                <div id="flush-collapseNine" class="accordion-collapse collapse" aria-labelledby="flush-headingNine" data-bs-parent="#accordionFlushExample">
                  <div class="accordion-body"><p>All funds stored on your Boarding Pass must first be converted to USD. We will process non-USD transfer requests but will charge a conversion fee of 2%.</p></div>
                </div>
              </div>

              
            </div>
            <div class="row">
                <div class="col-md-12 text-center" style="padding:90px 20px; padding-bottom:0;">
                    <h3></h3>
                    <h2 class="section-head-title text-dark mb-3 section-title" style="margin:40px; margin-top:20px">You Made It This Far</h2>
                    <p style="max-width:800px; margin:0 auto; font-size:1.2em">Might as well claim your $20 in points. Receive $10 in points for each person you invite that makes a deposit with your referral link.***</p>
                    <br>
                    <a href="#waitlist" class="btn btn-primary">JOIN VIP LIST</a>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12" style="padding-top:100px; padding-bottom:0">
                    <p>
                        * Annual Percentage Yield (APY) is accurate as of 8/15/2023. Select markets only. APY is compounded and credited monthly. Fees may reduce earnings. Rates are variable and subject to change before and after account opening.
                        <br>
                        ** Calculated values assume principal and interest remain on deposit and are rounded to the nearest dollar. In addition, calculated values use the current APY, which is variable and may change before and after account opening; your savings will be based on the actual APY, so savings could be less. While there is no limit on interest earned, calculator is designed with certain limits and is for illustrative purposes only.<br>
                        *** Maximum referral credit is $250 per year with a lifetime cap of $1,000.</p>
                </div>
            </div>
        </div>
    </section>
    <!-- End FAQs -->

    <div id="countdown">
        <div id="timer">
          <div id="days"></div>
          <div id="hours"></div>
          <div id="minutes"></div>
          <div id="seconds"></div>
        </div>
        <div class="description">
            LEFT TO <a href="#waitlist">BECOME VIP</a>
        </div>
    </div>

@endsection


    
@section("js")
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/typeahead.js/0.10.4/typeahead.bundle.min.js"></script>
    @if(env('APP_ENV') != "local")
        <!-- Meta Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s)
            {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};
            if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
            n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t,s)}(window, document,'script',
            'https://connect.facebook.net/en_US/fbevents.js');
            fbq('init', '1169331916805166');
            fbq('track', 'PageView');
        </script>
        <noscript><img height="1" width="1" style="display:none"
        src="https://www.facebook.com/tr?id=1169331916805166&ev=PageView&noscript=1"
        /></noscript>
        <!-- End Meta Pixel Code -->
    @endif
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/11.0.2/bootstrap-slider.min.js"></script>
    <script>
        


        $(document).ready(function() {
            $.ajaxSetup({
                 headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                 }
              });

            var currency = $("#currency"); //[make sure this is a unique variable name]

            

            
             
            /**
             * Format the number, so it will be seperated by comma
             */
            function commaSeparateNumber(val){
                while (/(\d+)(\d{3})/.test(val.toString())){
                  val = val.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
                }
                return val;
            }

            toggleLogo("black","white");

            function toggleLogo(color, replaced){
                var logoSRC = $(".logo").attr("src");
                logoSRC = logoSRC.replace(color, replaced);
                console.log(logoSRC)
                $(".logo").attr("src", logoSRC);
            }

            $.fn.isInViewport = function() {
              var elementTop = $(this).offset().top;
              var elementBottom = elementTop + $(this).outerHeight();

              var viewportTop = $(window).scrollTop();
              var viewportBottom = viewportTop + $(window).height();

              return elementBottom > viewportTop && elementTop < viewportBottom;
            };

            


            function isElementVisible($elementToBeChecked)
            {
                var TopView = $(window).scrollTop();
                var BotView = TopView + $(window).height();
                var TopElement = $elementToBeChecked.offset().top;
                var BotElement = TopElement + $elementToBeChecked.height();
                return ((BotElement <= BotView) && (TopElement >= TopView));
            }

            $({someValue: 0}).delay(3000).animate({someValue: {{ $counter }} }, {

                duration: 4500,
                easing:'swing', // can be anything
                step: function() { // called on every step
                  // Update the element's text with rounded-up value:
                  currency.text(commaSeparateNumber(Math.round(this.someValue)));
                },
                done: function() {

                }

            });


            $(".nav-icon").each(function(){
                var src = $(this).attr("src");
                console.log(src)
                src = src.replace('.png', '-white-boarding.png');
                $(this).attr("src", src);
              });

            $("#hamburger-menu").click(function(){
                if($("#page_home__show").hasClass("mobile-nav-open")){
                    toggleLogo("white","black");
                } else {
                    toggleLogo("black","white");
                }
                
            });

            $(".calculate").on("change", function(){
                var principal = $("#pool-amount").val() * 2000;

                const time = 1/12; // 1 year
                const rate = 0.077; // 8% APY
                const n = 1; // compounded monthly
                var withdrawable = principal;
                var points = 0;
                var deposits = ($("#pool-deposits").val() * 50);

                for(i = 0; i < 12; i++){
                    principal += deposits
                    var compoundInterestAdded = compoundInterest(principal, time, rate, n);
                    principal += compoundInterestAdded;
                    points += compoundInterestAdded;
                    withdrawable += deposits;
                }

                $("#balance").text(withdrawable.toLocaleString(undefined, {
                  minimumFractionDigits: 0,
                  maximumFractionDigits: 0
                }));

                $("#points").text(points.toLocaleString(undefined, {
                  minimumFractionDigits: 0,
                  maximumFractionDigits: 0
                }));

                $("#deposit").val($("#pool-amount").val() * 2000);
                $("#contribution").val(deposits);
                $("#balanceFinal").val(withdrawable);
                $("#pointsFinal").val(points);
                
            });

            function compoundInterest(p, t, r, n){
               const amount = p * (Math.pow((1 + (r / n)), (n * t)));
               const interest = amount - p;
               return interest;
            };


          function calculatePayout(poolAmount, poolSize, poolRate){
            console.log("size " + poolSize);
            console.log("amount " + poolAmount);
            console.log("rate " + poolRate);

            var top = poolSize * poolAmount;
            var bottom = poolSize * poolRate;

            console.log("top " + top);
            console.log("bottom " + bottom);

            return top/bottom;
          }

          @if(Session::get('subscribeError') !== null)
                swal({
                      icon: 'error',
                      title: 'Oops!',
                      text: 'We ran into a problem and could not add you to our list. Try again or send an email to markus@venti.co',
                    })
            @endif

            makeTimer();

            function makeTimer() {

    //      var endTime = new Date("29 April 2018 9:56:00 GMT+01:00");  
        var endTime = new Date("31 December 2023 23:59:49 GMT-04:00");          
            endTime = (Date.parse(endTime) / 1000);

            var now = new Date();
            now = (Date.parse(now) / 1000);

            var timeLeft = endTime - now;

            var days = Math.floor(timeLeft / 86400); 
            var hours = Math.floor((timeLeft - (days * 86400)) / 3600);
            var minutes = Math.floor((timeLeft - (days * 86400) - (hours * 3600 )) / 60);
            var seconds = Math.floor((timeLeft - (days * 86400) - (hours * 3600) - (minutes * 60)));
  
            if (hours < "10") { hours = "0" + hours; }
            if (minutes < "10") { minutes = "0" + minutes; }
            if (seconds < "10") { seconds = "0" + seconds; }

            $("#days").html(days + "<span> DAYS</span>");
            $("#hours").html(hours + "<span> HOURS</span>");
            $("#minutes").html(minutes + "<span> MINUTES</span>");
            $("#seconds").html(seconds + "<span style='padding-right:0'> SECONDS</span>");       

    }

    setInterval(function() { makeTimer(); }, 1000);

        });
    </script>

@endsection