@extends('layouts.curator-master')

@section('css')
	<style>
		p{
			font-size: 16px;
		}
		tbody td{
			font-size: 20px;
			text-align: center;
			vertical-align: middle;
		}
		thead th{
			vertical-align: middle;
		}
		.text-bold{
			font-weight: 700;
		}
		th img{
			padding: 10%;
		}
		ul.text-small{
			font-size: 12px;
			text-align: right;
			list-style: none;
			padding-left: 0;
			margin-left: 0;
		}
		.text-left{
			font-size: 16px;
		}
		.form-label{
			font-weight: 500;
		}
	</style>
@endsection

@section('content')
	<div id="page-content">
	    <div class="gradient-wrap">
	        <div class="container hero-section mb-4 mt-4">
	           	<div class="row">
	           		<div class="col-md-10 mt-4">
	                    <h1>Comparing High-Interest Travel Credit Cards</h1>
	                    <h4>with Venti's Boarding Pass: High-Yield Travel Savings Account</h4>
	                </div>
	                <div class="col-md-10 mb-4">
		                <div class="card bg-white">
			            	<div class="card-body">
			            		<div class="row">
				           			<div class="col-md-12">
				           				<p>Travel credit cards promise a convenient and rewarding way to finance your trips. This article is intended to provide an object comparison between the most popular travel card products and Venti's Boarding Pass: A High-Yield Travel Savings Account.</p>
				           				<h5>Before "Perks" Are Considered</h5>
				           				<p>let's utline the basics of what each program costs you.</p>
				           				<table class="table" style="width:100%">
				           					<thead>
				           						<th style="width:25%;"></th>
				           						<th class="text-center"><img src="/assets/img/chase-sapphire-reserve-card.png" style="width:100%; max-width:200px;"></th>
				           						<th class="text-center"><img src="/assets/img/capital-one-venture-x-card.png" style="width:100%; max-width:200px;"></th>
				           						<th class="text-center"><img src="/assets/img/citi-premier-card.png" style="width:100%; max-width: 200px;"></th>
				           						<th class="text-center"><img src="/assets/img/venti-trademark-logo-black-boarding.png" style="width:100%; max-width: 200px;"></th>
				           					</thead>
				           					<tbody>
				           						<tr>
				           							<td class="text-bold text-left">Annual Fee</td>
				           							<td>$550</td>
				           							<td>$395</td>
				           							<td>$95</td>
				           							<td>$72</td>
				           						</tr>
				           						<tr>
				           							<td class="text-bold text-left">APR Cost</td>
				           							<td>22.49%-29.49%</td>
				           							<td>22.24%-29.24%</td>
				           							<td>21.24%-29.24%</td>
				           							<td>0</td>
				           						</tr>
				           						<tr>
				           							<td class="text-bold text-left">Minimum Credit Score</td>
				           							<td>720</td>
				           							<td>720</td>
				           							<td>680</td>
				           							<td>N/A</td>
				           						</tr>
				           					</tbody>
				           				</table>
				           				<p>As a high-yield travel savings, Venti does not charge you interest for maintaining a balance. Instead, your earn interest on your deposits. Strong credit is not required, and Venti does not run a credit check to allow you to start saving.</p>
				           			</div>
				           		</div>
				           	</div>
				        </div>
				    </div>
				    <div class="col-md-10 mb-4">
		                <div class="card bg-white">
			            	<div class="card-body">
			            		<div class="row">
				           			<div class="col-md-12">
				           				<h5>Let's Compare the Perks</h5>
				           				<p>Sign-up bonuses are how credit card companies "compete" with each other so you select their card program vs. their counterparts. Sign-up bonuses are usually generous and the credit card companies generally take a loss at first in order to garner your business.</p>
				           				<p>
				           					Key Takeaways:
				           					<ul>
				           						<li>Credit card sign-up bonuses require excessive and rapid spending, which is done on purpose to get you to start building a high-credit card balance to start.</li>
				           						<li>The advertised credit card cashback/reward reward rates can be complicated for one-to-one comparisons. All three providers require you to book travel through their internal travel booking platforms, which often lack diversity of options.</li>
				           						<li>Annual Credits are only useful when shopping with internal travel booking platforms.</li>
				           					</ul>
				           				</p>

				           				<p>For fair comparison, we've converted points to cash equivalent value. Take credit card bonus points and divide by 100.</p>
				           				<table class="table" style="width:100%">
				           					<thead>
				           						<th style="width:25%;"></th>
				           						<th class="text-center"><img src="/assets/img/chase-sapphire-reserve-card.png" style="width:100%; max-width:200px;"></th>
				           						<th class="text-center"><img src="/assets/img/capital-one-venture-x-card.png" style="width:100%; max-width:200px;"></th>
				           						<th class="text-center"><img src="/assets/img/citi-premier-card.png" style="width:100%; max-width: 200px;"></th>
				           						<th class="text-center"><img src="/assets/img/venti-trademark-logo-black-boarding.png" style="width:100%; max-width: 200px;"></th>
				           					</thead>
				           					<tbody>
				           						<tr>
				           							<td class="text-bold text-left">Bonus Points</td>
				           							<td>$600<br><p style="margin-top: 10px; font-size:10px; line-height: 1;">Must spend $4,000 in the first 3 months</p></td>
				           							<td>$750<br><p style="margin-top: 10px; font-size:10px; line-height: 1;">Must spend $4,000 in the first 3 months</p></td>
				           							<td>$600<br><p style="margin-top: 10px; font-size:10px; line-height: 1;">Must spend $4,000 in the first 3 months</p></td>
				           							<td>$20<br><p style="margin-top: 10px; font-size:10px; line-height: 1;">Join before Dec. 31 or be invited by existing member.</p></td>
				           						</tr>
				           						<tr>
				           							<td class="text-bold text-left">
				           								<p style="font-size:16px;">Cash Back</p>
				           								<ul class="text-small">
				           									<li>Hotels & Rental Cars</li>
				           									<li>Flights</li>
				           									<li>Restaurants</li>
				           									<li>Supermarkets & Gas Stations</li>
				           									<li>All Purchases</li>
				           								</ul>
				           							</td>
				           							<td>
				           								<p>&nbsp;</p>
				           								<ul class="text-small text-center">
				           									<li>10%</li>
				           									<li>5%</li>
				           									<li>3%</li>
				           									<li>3%</li>
				           									<li>3%</li>
				           								</ul>
				           							</td>
				           							<td>
				           								<p>&nbsp;</p>
				           								<ul class="text-small text-center">
				           									<li>10%</li>
				           									<li>5%</li>
				           									<li>3%</li>
				           									<li>3%</li>
				           									<li>2%</li>
				           								</ul>
				           							</td>
				           							<td>
				           								<p>&nbsp;</p>
				           								<ul class="text-small text-center">
				           									<li>10%</li>
				           									<li>3%</li>
				           									<li>3%</li>
				           									<li>3%</li>
				           									<li>1%</li>
				           								</ul>
				           							</td>
				           							<td>Up to 100% off all travel purchases.</td>
				           						</tr>
				           						<tr>
				           							<td class="text-bold text-left">Annual Credit</td>
				           							<td>$300</td>
				           							<td>$300</td>
				           							<td>$0</td>
				           							<td>$0</td>
				           						</tr>
				           						<tr>
				           							<td class="text-bold text-left">Points Transferability</td>
				           							<td>No</td>
				           							<td>No</td>
				           							<td>No</td>
				           							<td>Yes</td>
				           						</tr>
				           					</tbody>
				           				</table>
			           				</div>
				           		</div>
				           	</div>
				        </div>
			    	</div>
			    	<div class="col-md-10 mb-4">
		                <div class="card bg-white">
			            	<div class="card-body">
			            		<div class="row">
				           			<div class="col-md-12">
				           				<h5>Comparing Deal Sweetners</h5>
				           				<p>By this point, you've probably realized that the travel credit cards are...suprisingly similar. Why pick one over the other? There are other factors to consider, such as existing relationships with each bank and "deal sweetners" that can sometimes be unique to each card. We'll highlight the important ones:</p>

				           				<table class="table" style="width:100%">
				           					<thead>
				           						<th style="width:25%;"></th>
				           						<th class="text-center"><img src="/assets/img/chase-sapphire-reserve-card.png" style="width:100%; max-width:200px;"></th>
				           						<th class="text-center"><img src="/assets/img/capital-one-venture-x-card.png" style="width:100%; max-width:200px;"></th>
				           						<th class="text-center"><img src="/assets/img/citi-premier-card.png" style="width:100%; max-width: 200px;"></th>
				           						<th class="text-center"><img src="/assets/img/venti-trademark-logo-black-boarding.png" style="width:100%; max-width: 200px;"></th>
				           					</thead>
				           					<tbody>
				           						<tr>
				           							<td class="text-bold text-left">Travel Protection/Insurance</td>
				           							<td><i class="fa fa-check"></i></td>
				           							<td><i class="fa fa-check"></i></td>
				           							<td></td>
				           							<td><p style="font-size:14px;margin:0">Available for Purchase</p></td>
				           						</tr>
				           						<tr>
				           							<td class="text-bold text-left">TSA Pre-Check</td>
				           							<td><i class="fa fa-check"></i></td>
				           							<td><i class="fa fa-check"></i></td>
				           							<td></td>
				           							<td></td>
				           						</tr>
				           						<tr>
				           							<td class="text-bold text-left">Lounge Access</td>
				           							<td><i class="fa fa-check"></i></td>
				           							<td><i class="fa fa-check"></i></td>
				           							<td></td>
				           							<td></td>
				           						</tr>
				           						<tr>
				           							<td class="text-bold text-left">No Cost Currency Conversion</td>
				           							<td><i class="fa fa-check"></i></td>
				           							<td><i class="fa fa-check"></i></td>
				           							<td><i class="fa fa-check"></i></td>
				           							<td></td>
				           						</tr>
				           						<tr>
				           							<td class="text-bold text-left">Price Drop Protection</td>
				           							<td></td>
				           							<td><i class="fa fa-check"></i></td>
				           							<td></td>
				           							<td></td>
				           						</tr>
				           					</tbody>
				           				</table>

				           				<p>
				           					Key Takeaways:
				           					<ul>
				           						<li>Credit Cards add in deal sweetners that make the most sense if you are new to the program. You're not applying for TSA pre-check every year.</li>
				           						<li>Not all airports have lounges in a convenient location from your gate. It's common that each non-card holder has to pay a fee to enter the lounge with you. Some lounges are card specific.</li>
				           						<li>Travel protection/insurance policies are underwritten from companies not affiliated with the credit cards, which means you have to battle the customer service department of a company you've likely never heard of to file a claim.</li>
				           					</ul>
				           				</p>
			           				</div>
				           		</div>
				           	</div>
				        </div>
			    	</div>
			    	<div class="col-md-10 mb-4">
		                <div class="card bg-white">
			            	<div class="card-body">
			            		<div class="row">
				           			<div class="col-md-12">
				           				<h5>Try the Math</h5>
				           				<p>With all the facts considered, it's time to put all four programs through a calculator.</p>
				           				<div id="calculator">
				           					<div class="row">
				           						<div class="form-group col-md-4">
				           							<label class="form-label">Weekly Spend on Groceries</label>
				           							<div class="input-group">
				           								<button class="btn btn-black disabled" type="button">$</button>
				           								<input type="number" class="form-control required" value="0" min="0" step="1" inputmode="numeric" id="groceries">
				           							</div>
				           						</div>
				           						<div class="form-group col-md-4">
					           						<label class="form-label">Weekly Spend on Gas</label>
					           						<div class="input-group">
				           								<button class="btn btn-black disabled" type="button">$</button>
				           								<input type="number" class="form-control required" value="0" min="0" step="1" inputmode="numeric" id="gas">
				           							</div>
					           					</div>
					           					<div class="form-group col-md-4">
					           						<label class="form-label">Weekly Spend on Restaurants</label>
					           						<div class="input-group">
				           								<button class="btn btn-black disabled" type="button">$</button>
				           								<input type="number" class="form-control required" value="0" min="0" step="1" inputmode="numeric" id="restaurants">
				           							</div>
					           					</div>
				           					</div>
				           					<div class="row">
					           					<div class="form-group col-md-4">
					           						<label class="form-label">Annual Spend on Airfare</label>
					           						<div class="input-group">
				           								<button class="btn btn-black disabled" type="button">$</button>
				           								<input type="number" class="form-control required" value="0" min="0" step="1" inputmode="numeric" id="airfare">
				           							</div>
					           					</div>
					           					<div class="form-group col-md-4">
					           						<label class="form-label">Annual Spend on Hotels</label>
					           						<div class="input-group">
				           								<button class="btn btn-black disabled" type="button">$</button>
				           								<input type="number" class="form-control required" value="0" min="0" step="1" inputmode="numeric" id="hotels">
				           							</div>
					           					</div>
					           					<div class="form-group col-md-4">
					           						<label class="form-label">Annual Spend on Rental Cars</label>
					           						<div class="input-group">
				           								<button class="btn btn-black disabled" type="button">$</button>
				           								<input type="number" class="form-control required" value="0" min="0" step="1" inputmode="numeric" id="rentals">
				           							</div>
					           					</div>
				           					</div>
				           					<div class="row">
				           						<div class="form-group col-md-4">
					           						<label class="form-label">Each Month, I Usually</label>
					           						<select class="form-control required" id="payments">
					           							<option value="4">Carry a high balance</option>
					           							<option value="3">Carry a modest balance</option>
					           							<option value="2">Carry a low balance</option>
					           							<option value="1">Pay off my card in full</option>
					           						</select>
					           					</div>
					           					<div class="form-group col-md-4">
					           						<label class="form-label">With Credit Cards, I feel</label>
					           						<select class="form-control required" id="feeling">
					           							<option value="4">Overwhelmed with debt</option>
					           							<option value="3">Nervous about the future </option>
					           							<option value="2">Neutral</option>
					           							<option value="1">Empowered</option>
					           						</select>
					           					</div>
					           					<div class="form-group col-md-4">
				           						<label class="form-label">With Credit Cards, I Plan To</label>
				           						<select class="form-control required" id="future">
				           							<option value="4">Aggressively pay down/off my debt</option>
				           							<option value="3">Step up monthly payments</option>
				           							<option value="2">Not make any changes</option>
				           							<option value="1">Open another credit card</option>
				           						</select>
				           					</div>
				           					</div>
				           					
				           				</div>
				           				
				           				<div style="width: 800px;"><canvas id="myChart"></canvas></div>

				           				<p>
				           					Key Takeaways:
				           					<ul>
				           						<li>Credit Cards add in deal sweetners that make the most sense if you are new to the program. You're not applying for TSA pre-check every year.</li>
				           						<li>Not all airports have lounges in a convenient location from your gate. It's common that each non-card holder has to pay a fee to enter the lounge with you. Some lounges are card specific.</li>
				           						<li>Travel protection/insurance policies are underwritten from companies not affiliated with the credit cards, which means you have to battle the customer service department of a company you've likely never heard of to file a claim.</li>
				           					</ul>
				           				</p>
			           				</div>
				           		</div>
				           	</div>
				        </div>
			    	</div>
	            </div>
	            
		    </div>
		</div>
	</div>
@endsection

@section('js')

<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
<script>
	jQuery(document).ready(function ($) {
		function updateCalculator(){
			var proceed = true;

			$(".required").each(function(){
				if($(this).val() == "" || $(this).val() == undefined){
					$(this).addClass("invalid-feedback");
					proceed = false;
				}
			});

			var gas = parseInt($("#gas").val()) * 52;
			var hotels = parseInt($("#hotels").val());
			var restaurants = parseInt($("#restaurants").val()) * 52;
			var airfare = parseInt($("#airfare").val());
			var rentals = parseInt($("#rentals").val());
			var groceries = parseInt($("#groceries").val()) * 52;
			var payments = parseInt($("#payments").val());
			var feeling = parseInt($("#feeling").val());
			var future = parseInt($("#future").val());



		}

		const ctx = document.getElementById('myChart');

  new Chart(ctx, {
    type: 'line',
    data: {
      labels: ['6 Months', '12 Months', '18 Months', '24 Months', '30 Months', '36 Months'],
      datasets: [
      {
        label: 'Sapphire Reserve',
        data: [12, 19, 60, 80, 400, 350],
        borderWidth: 1
      },
      {
        label: 'Venture X',
        data: [22, 29, 50, 80, 150, 280],
        borderWidth: 1
      },
      {
        label: 'Citi',
        data: [32, 39, 50, 90, 120, 300],
        borderWidth: 1
      },
      {
        label: 'Venti',
        data: [40, 69, 80, 120, 200, 500],
        borderWidth: 1
      },
      ]
    },
    options: {
      scales: {
        y: {
          beginAtZero: true
        }
      }
    }
  });

		
	});
</script>

@endsection