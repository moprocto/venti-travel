<header class="qodef-page-header">
   <div class="qodef-menu-area">
      <div class="qodef-vertical-align-containers">
         <div class="qodef-position-left">
            <div class="qodef-position-left-inner">
               <div class="qodef-logo-wrapper">
                  <a itemprop="url" href="https://setsail.qodeinteractive.com/" style="height: 48px;">
                  <img itemprop="image" class="qodef-normal-logo" src="/assets/img/venti-logo-dark.png" width="301" height="96" alt="logo" />
                  <img itemprop="image" class="qodef-dark-logo" src="/assets/img/venti-logo-dark.png" width="301" height="96" alt="dark logo" /> <img itemprop="image" class="qodef-light-logo" src="/assets/img/venti-logo-dark.png" width="301" height="96" alt="light logo" /> </a>
               </div>
            </div>
         </div>
         <!--
         <div class="qodef-position-right">
            <div class="qodef-position-right-inner">
               <a href="javascript:void(0)" class="qodef-fullscreen-menu-opener qodef-fullscreen-menu-opener-svg-path">
                  <span class="qodef-fullscreen-menu-close-icon">
                     <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="18px" height="18px" viewBox="0 0 20 20" enable-background="new 0 0 20 20" xml:space="preserve">
                        <line fill="none" x1="5.4" y1="15.6" x2="5.4" y2="15.6" />
                        <line fill="none" stroke="#fff" stroke-width="1.5" stroke-miterlimit="10" x1="0.5" y1="0.6" x2="19.4" y2="19.4" />
                        <line fill="none" stroke="#fff" stroke-width="1.5" stroke-miterlimit="10" x1="19.4" y1="0.6" x2="0.6" y2="19.4" />
                     </svg>
                  </span>
                  <span class="qodef-fullscreen-menu-opener-icon">
                     <svg class="qodef-setsail-burger" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="10px" y="10px" viewBox="0 0 18 20" enable-background="new 0 0 18 20" width="18" xml:space="preserve">
                        <rect x="7" width="11" height="2" />
                        <rect x="5" y="9" width="13" height="2" />
                        <rect x="0" y="18" width="18" height="2" />
                     </svg>
                  </span>
               </a>
            </div>
         </div>
         -->
      </div>
   </div>
</header>
<header class="qodef-mobile-header">
   <div class="qodef-mobile-header-inner" style="background-color:transparent; !important;">
      <div class="qodef-mobile-header-holder">
         <div class="qodef-grid">
            <div class="qodef-vertical-align-containers">
               <div class="qodef-position-left">
                  <div class="qodef-position-left-inner" >
                  </div>
               </div>
               <div class="qodef-position-center">
                  <div class="qodef-position-center-inner">
                     <div class="qodef-mobile-logo-wrapper">
                        <a itemprop="url" href="https://setsail.qodeinteractive.com/" style="height: 36px">
                        <img itemprop="image" src="/assets/img/venti-logo-dark.png" width="230" height="73" alt="Mobile Logo" />
                        </a>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</header>