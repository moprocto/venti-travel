@extends("layouts.excursion-master")

@section("css")
	<link rel="stylesheet" type="text/css" href="/assets/css/earn.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/landing.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper@8/swiper-bundle.min.css"/>
    <link rel="stylesheet" type="text/css" href="/assets/css/store.css">
    
    <style type="text/css">
        .bg-white{
            background: white;
        }
        .mob-shadow{
                box-shadow: rgb(100 100 111 / 30%) 0px 7px 29px 0px;
                position: absolute;
                height: 100%;
                width: 386px;
                background: transparent;
                right: 100px;
                bottom: 0;
                border-top-left-radius: 63px;
                border-top-right-radius: 63px;
        }
        @media (max-width: 767px){
            .mob-pt{
                padding-top: 3em;
            }
            .top-header{
                padding-top: 5em;
            }
            .mob-shadow{
                width: 333px;
                background: transparent;
                right: 20px;
            }
            .dark-on-light.nav-links{
                color: white;
            }
            .btn-white{
                color: black !important;
            }
            .swiper-slide {
              width: 100%;
              max-width: 380px;
            }
            .swiper{
                max-width: 380px !important;
            }
        }

        .swiper-wrapper{
                min-height: 500px !important;
            }
        
        #changethewords {
            display: inline !important;
        }
        h4.title{
         display: flex;
         justify-content: space-between;
         align-items: center;
         align-content: center;
      }
      .category{
         text-transform: uppercase;
         background: black;
         color: white;
         font-size: 12px;
         padding: 5px 10px;
         border-radius: 10px;
         margin-top: -3px;
      }
      body{
        background: black;
        background-color: black !important;
      }
      .gradient-wrap {
        margin-bottom: -64px;
      }
    span.fa-bars{
        background:rgba(255,255,255,0.9); 
        padding:8px 10px;
        border-radius:20px
    }
    .swiper-wrapper .grid-item{
        width: 100% !important;
        max-width: 100% !important;
    }
    .pills-tab{
    	border:1px solid black; padding: 5px 10px; border-radius:20px;
    	font-weight: bold;
    }
    .slider.slider-horizontal{
    	width: 87% !important;
    }
    .faq{
        margin-bottom: 20px;
        background-color: #F2F2F6;
        padding:20px;
        border-radius: 20px;
        padding-bottom:0;
        height: fit-content;
    }



    .swiper-slide {
      text-align: center;
      font-size: 18px;
      background: #fff;
      display: flex;
      justify-content: center;
      align-items: center;
      flex-direction: column;
      background: none;
      padding-bottom: 30px;

    }

    .swiper-slide img {
      
    }

    .modal-body{
        border-top-left-radius: 6px;
        border-bottom-left-radius: 6px;
    }
    .text-small{
        font-size: 11px;
        margin-top: 5px;
    }
    </style>
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/11.0.2/css/bootstrap-slider.min.css">
@endsection

@section("content")
<div id="page-content">
    <div id="backdrop" style="margin-top: -130px; width: 100%; z-index: 1; display: flex; align-items: center; min-height: 800px; background-position: center top;  background-size: cover; margin-bottom: -64px; background-image:url('/assets/img/excursions-backgrop.jpg')">
        <div class="gradient-wrap" style="background: transparent; width:100%;">
            <!--  HERO -->
            <div class="section-wrapper black position-relative">
                <div class="row">
                    <div class="col-md-4 col-sm-12 col-12 top-header" style="display: flex; align-items:center;">
                        <div id="hero-text-wrapper">
                          <h1 class="mb-3 section-title" style="color: white; font-weight:600 !important; font-size: 3.5em;">
                            Networking
                          <br />
                          Meets Travel
                          </h1>
                        <h3 class="section-subtitle mb-4" style="color: white;">
                            Connect with other professionals in your industry through shared adventures
                        </h3>
                          <a href="#catalog" style="width:125px;" class="btn btn-white">View Trips</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="gradient-wrap bg-white">
        <!--  HERO -->
        <div class="section-wrapper black position-relative mb-4" style="padding: 6em 0;">     
            <div class="row" style="min-height: 400px;">
                <div class="col-md-6" style="align-self: center;">
                    <img src="/assets/img/navigator-e1.jpg" style="width:100%;">
                </div>
                <div class="col-md-6 text-left mob-pt" style="display: grid; align-content: center;">
                    <h1 class="mb-3 section-title" style="font-weight:600 !important; font-size: ;">About our Program</h1>
                    <h3 class="section-subtitle mb-4" >
                         <strong>High-quality</strong> international travel designed to bring together industry professionals that wish to grow their network and career while building lifelong connections through shared experiences. 
                    </h3>
                    <ul>
                    	<li>Pay one price, and your flight, food, accommodation, activities, tours, and transportation are all included.</li>
                        <li>We do all the planning, so you can just focus on having the best time.</li>
                    	<li>We vet interested travelers to ensure they are a good fit for each trip. Spots are limited and in some cases invite-only. Fluency in English is required to participate.</li>
                        <li>Expert navigators guide each trip to maximize your experience.</li>
                        <li>Network with everyone on your trip before departure via our mobile app.</li>
                    </ul>
                    <a href="#catalog" style="width:125px;" class="btn btn-black">View Trips</a>
                </div>
            </div>
        </div>
    </div>
    <div class="gradient-wrap">
        <div class="section-wrapper black position-relative mb-4" style="padding: 6em 0;">     
            <div class="row" style="min-height: 400px;">
                <div class="col-md-12" id="catalog">
                <h1 class="mb-3 section-title mt-4 text-center" id="apply" style="font-weight:600 !important; width: 100%; margin-bottom:20px">Our 2024 Catalog</h1>
                <h4 class="section-subtitle text-center" style="display: block; max-width: 800px; margin:0 auto; margin-bottom:50px;">Unforgettable experiences with vetted people within your industry. All trips are approximately 9 to 11 days and start at $5,999 <strike>$6,499</strike> per person. Invite a friend, and you each will save 15%.</h4>
                </div>
                <div class="swiper" style="max-width:900px;">
                    <!-- Additional required wrapper -->
                    <div class="swiper-wrapper">
                        @include('excursion.catalog')
                    </div>
                    <!-- If we need pagination -->
                    <div class="swiper-pagination"></div>

                    <!-- If we need scrollbar -->
                    <div class="swiper-scrollbar"></div>
                </div>
                <div class="col-md-12 text-center">
                    <br><br>
                    <h4>Don't See Something for You?</h4>
                    <p>Use our waitlist form below to share your preference. You'll still be eligible for your $500 discount.</p>
                </div>
            </div>
        </div>
    </div>
    <div class="gradient-wrap bg-white">
        <!--  HERO -->
        <div class="section-wrapper black position-relative mb-4" style="padding: 6em 0;">     
            <div class="row" style="min-height: 400px;">
                <div class="col-md-6 text-left mob-pt" style="display: grid; align-content: center;">
                    <h1 class="mb-3 section-title" style="font-weight:600 !important; font-size: ;">What makes our trips special?</h1>
                    <h3 class="section-subtitle mb-4" >
                        Each trip is carefully crafted by a team of travel veterans to include world-class, life-changing activites:
                    </h3>
                    <ul>
                        <li>Exclusive tours of universities, manufacturing facilities, research labs, and more</li>
                        <li>Dinners with high-profile industry leaders</li>
                        <li>Volunteering, conservation, and mentoring opportunities</li>
                        <li>Cooking classes with renowned chefs</li>
                    </ul>
                    <a href="#waitlist" style="width:125px;" class="btn btn-black">Join Waitlist</a>
                </div>
                <div class="col-md-6" style="align-self: center;">
                    <img src="/assets/img/navigator-e2.jpg" style="width:100%;">
                </div>
            </div>
        </div>
    </div>
    <div class="gradient-wrap">
        <div class="section-wrapper black position-relative mb-4" style="padding: 6em 0;">     
            <div class="row" style="min-height: 400px;">
            	<div class="col-md-12">
            	<h1 class="mb-3 section-title mt-4 text-center" id="waitlist"  style="font-weight:600 !important; width: 100%; margin-bottom:20px">Join Waitlist</h1>
            	<h4 class="section-subtitle text-center" style="display: block; max-width: 800px; margin:0 auto; margin-bottom:50px;">Early birds save $500 when they book their first trip in 2024. Payment is not required to join our waitlist, and we will only contact you when we have critical updates.</h4>
            	</div>
            	<div class="col-md-12">
            		@include('forms.excursion')
            	</div>
            </div>
        </div>
    </div>
    <div class="gradient-wrap bg-white">
        <div class="section-wrapper black position-relative mb-4" style="padding: 6em 0;">     
            <div class="row" style="min-height: 400px;">
                <div class="col-md-12">
                <h1 class="mb-3 section-title mt-4 text-center" style="font-weight:600 !important; width: 100%; margin-bottom:20px">FAQs</h1>
                <h4 class="section-subtitle text-center" style="display: block; max-width: 800px; margin:0 auto; margin-bottom:50px;"></h4>
                </div>
                <div class="col-md-12 faq">
                    <h5>Can I pay for my trip via an installment plan?</h5>
                    <p>Yes, we will offer installment plans via affirm.</p>
                </div>
                <div class="col-md-12 faq">
                    <h5>Can I bring my spouse or significant other?</h5>
                    <p>Yes, but they must also meet the criteria for the trip. You each will receive our 15% referral discount.</p>
                </div>
                <div class="col-md-12 faq">
                    <h5>Will my trips be insured? What if I need to cancel?</h5>
                    <p>We include travel insurance in the fee assessed for each trip. Insurance does not cover cancelations. Cancelations will result in a full refund if requested <strong>more than 60 days prior to departure</strong>. If you need to cancel with less than 60 days remaining until departure, you will receive credits that can be applied to another trip or transferrable to someone else. If you find someone else to take your spot, you can receive a full refund.</p>
                </div>
                <div class="col-md-12 faq" id="faq4">
                    <h5>Can I request new destinations?</h5>
                    <p>Yes, you can use our <a href="#waitlist">waitlist form</a> to indicate your profession and if you prefer a different country. Destinations are currently limited to the expertise of our Navigators, but we will find a way to accommodate if enough people express interest.</p>
                </div>
                <div class="col-md-12 faq">
                    <h5>Can I purchase a trip as a gift for someone?</h5>
                    <p>Yes, as long as that person still meets the criteria for our trip. Trips purchased by recruiters, hiring managers, or gifters also qualify for our 15% referral discount.</p>
                </div>
                <div class="col-md-12 faq">
                    <h5>Can I purchase my own flight?</h5>
                    <p>To mitigate the risk of logistical issues, our team will manage this for you. If you are eligible for rewards points via a travel card or other program, we will work with our supplier to ensure you earn your points.</p>
                </div>
            </div>
        </div>
    </div>
  </div>
  <!-- MODAL -->

  <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
         <div class="modal-dialog modal-dialog-centered" role="document" style="max-width: 1000px;">
            <div class="modal-content">
               <div class="row">
                  <div class="col-md-6 d-flex">
                     <div class="modal-body p-5 img d-flex img text-center d-flex align-items-center" id="modal-body" style="background-size: cover; background-position: center; background-image: url('/assets/img/southafrica-e3-founders.jpg');"></div>
                  </div>
                  <div class="col-md-6 d-flex">
                     <div class="modal-body p-4 p-md-5 align-items-center color-2">
                        <div class="tabulation tabulation2">
                           <div class="tab-content border-0">
                              <div class="tab-pane p-0 container active" id="signin">
                                 <div class="text w-100">
                                    <h2 class="mb-4" id="modal-title"></h2>
                                    <h4 class="subtitle" id="subtitle"></h4>

                                    <p id="text" style="font-size:16px;"></p>
                                    <div>
                                        <a href="https://venti.co/excursions#waitlist" class="btn btn-black dismiss"><i class="fa-regular fa-bell"></i> Join Waitlist</a>
                                        <p class="text-small">Get notified when pricing and itinerary details become available. You'll also lock in a $500 early bird discount.</p>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
@endsection

@section("js")
    @if(env('APP_ENV') != "local")
        <!-- Meta Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s)
            {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};
            if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
            n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t,s)}(window, document,'script',
            'https://connect.facebook.net/en_US/fbevents.js');
            fbq('init', '1169331916805166');
            fbq('track', 'PageView');
        </script>
        <noscript><img height="1" width="1" style="display:none"
        src="https://www.facebook.com/tr?id=1169331916805166&ev=PageView&noscript=1"
        /></noscript>
        <!-- End Meta Pixel Code -->
    @endif
    <script type="text/javascript" src="/assets/js/changethewords.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/swiper@8/swiper-bundle.min.js"></script>
    <script>
        $(function() {
          $("#changethewords").changeWords({
            time: 2000,
            animate: "flipInX",
            selector: "span",
            repeat:true
          });
        });
        

        $(document).ready(function() {
            $.ajaxSetup({
                 headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                 }
              });

            toggleLogo("black","white");

            function toggleLogo(color, replaced){
                var logoSRC = $(".logo").attr("src");
                logoSRC = logoSRC.replace(color, replaced);
                console.log(logoSRC)
                $(".logo").attr("src", logoSRC);
            }

            $(".nav-icon").each(function(){
                var src = $(this).attr("src");
                console.log(src)
                src = src.replace('.png', '-white.png');
                $(this).attr("src", src);
              });

            $("#hamburger-menu").click(function(){
                if($("#page_home__show").hasClass("mobile-nav-open")){
                    toggleLogo("white","black");
                } else {
                    toggleLogo("black","white");
                }
                
            });

            var width = $(window).width();
            var numSlides = 3;
            if(width < 600){
                numSlides = 1;
            }

            const swiper = new Swiper('.swiper', {
                  // Optional parameters
                  direction: 'horizontal',
                  loop: false,
                  spaceBetween: 30,
                  slidesPerView: numSlides,

                  // If we need pagination
                  pagination: {
                    el: '.swiper-pagination',
                  },

                  // Navigation arrows
                  navigation: {
                    nextEl: '.swiper-button-next',
                    prevEl: '.swiper-button-prev',
                  },

                  
            });
            var myModal = new bootstrap.Modal(document.getElementById('exampleModalCenter'), {
                  keyboard: false
                });

            $(".swiper-slide").click(function(){

                var modalTitle = $(this).attr("data-country");
                $("#modal-title").text(modalTitle);

                var modalBody = $(this).attr("data-image");
                $("#modal-body").css("background-image", "url(" + modalBody + ")");

                var modalSubtitle = $(this).attr("data-subtitle");
                $("#subtitle").text(modalSubtitle);

                var modalText = $(this).attr("data-text");
                $("#text").text(modalText);

                

                var modalToggle = document.getElementById('exampleModalCenter') // relatedTarget
                myModal.show(modalToggle);
            });

            $(".dismiss").click(function(){
                var modalToggle = document.getElementById('exampleModalCenter') // relatedTarget
                myModal.hide(modalToggle);
            })


            @error('success')
		  		swal({
				  icon: 'success',
				  title: 'All Set!',
				  text: 'You\'ve been added to our waitlist. We have sent you a confirmation email',
				})
		  	@enderror

            @error('incorrectAnswer')
                    swal({
                      icon: 'error',
                      title: 'Uh oh!',
                      text: 'Your answer to our match question was incorrect. Please scroll downto retry.',
                    })
            @enderror
        });
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/11.0.2/bootstrap-slider.min.js"></script>
@endsection