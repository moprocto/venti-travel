<div class="swiper-slide text-center" style="justify-content: flex-start;" data-country="Switzerland" data-image="/assets/img/switzerland-e1-finance.jpg" data-subtitle="For analysts, bankers, brokers, and managers in finance" data-text="Join us on a 9-day networking journey to Switzerland, specifically crafted for professionals in finance. Amid the serenity of the Swiss Alps, you'll cultivate meaningful connections with industry peers, partaking in engaging discussions and sharing insights, all while enjoying the leisure of skiing. This trip offers a unique blend of networking, learning, and adventure, providing substantial value to your professional growth in the financial industry.">
    <img src="/assets/img/switzerland-e1-finance.jpg" style="width:100%; max-width: 240px; border-radius: 10px;">
        <br>
        <h4 class="mt-4">Finance</h4>
        <p>For analysts, bankers, brokers, and managers in finance to explore <strong>Switzerland</strong></p>
</div>
<div class="swiper-slide text-center" style="justify-content: flex-start;" data-country="Egypt" data-image="/assets/img/egypt-e4-physician.jpg" data-subtitle="For physicians and surgeons" data-text="Join us on a 9-day networking journey to Switzerland, specifically crafted for professionals in finance. Amid the serenity of the Swiss Alps, you'll cultivate meaningful connections with industry peers, partaking in engaging discussions and sharing insights, all while enjoying the leisure of skiing. This trip offers a unique blend of networking, learning, and adventure, providing substantial value to your professional growth in the financial industry.">
    <img src="/assets/img/egypt-e4-physician.jpg" style="width:100%; max-width: 240px; border-radius: 10px;">
        <br>
        <h4 class="mt-4">Medicine</h4>
        <p>For physicians and surgeons (excluding students and residents) to explore <strong>Egypt</strong></p>

</div>
<div class="swiper-slide text-center" style="justify-content: flex-start;" data-country="South Africa" data-image="/assets/img/southafrica-e3-founders.jpg" data-subtitle="For black tech founders" data-text="Join us for a distinctive 9-day networking journey to South Africa. Amidst South Africa's vibrant tech hubs, you'll engage with fellow founders, share your experiences, and forge connections that can fuel your entrepreneurial journey. This trip offers a unique opportunity to build a strong network within your niche, while also exploring the diverse attractions of South Africa.">
    <img src="/assets/img/southafrica-e3-founders.jpg" style="width:100%; max-width: 240px; border-radius: 10px;">
        <br>
        <h4 class="mt-4">Black Tech Founders</h4>
        <p>For black founders building all things tech to explore <strong>South Africa</strong></p>
</div>
<div class="swiper-slide text-center" style="justify-content: flex-start;" data-country="France" data-image="/assets/img/france-e9-womeninlaw.jpg" data-subtitle="For women partners, senior associates, and general counsels in big law" data-text="Immerse yourself in a unique networking experience on a 9-day journey to France, specifically designed for women in the law field. Starting in the heart of Paris, you will have the chance to engage with a diverse group of legal professionals, exchange insights, and create meaningful relationships that could prove instrumental in your career. This trip blends the enchantment of French culture with an excellent networking platform, offering you the chance to explore, connect, and learn. Step into a world of professional growth, where the value of each connection is as enriching as the journey itself.">
    <img src="/assets/img/france-e9-womeninlaw.jpg" style="width:100%; max-width: 240px; border-radius: 10px;">
        <br>
        <h4 class="mt-4">Women in Law</h4>
        <p>For partners, senior associates, and general counsels in big law to explore <strong>France</strong></p>
</div>
<div class="swiper-slide text-center" style="justify-content: flex-start;" data-country="Japan" data-image="/assets/img/japan-e2-gaming.jpg" data-subtitle="For lead game developers and designers" data-text="Embark on a unique 14-day networking journey to Japan, specifically curated for game developers. Starting in the bustling city of Tokyo, the trip provides an ideal platform to engage with a community of like-minded professionals, foster meaningful relationships, and share industry insights that can elevate your game development journey. A key highlight includes a tour of Nintendo's headquarters in Kyoto, offering an insider's look at one of the world's leading game development companies. This experience weaves together the rich culture of Japan with a high-value networking platform, offering substantial professional growth in the gaming industry.">
    <img src="/assets/img/japan-e2-gaming.jpg" style="width:100%; max-width: 240px; border-radius: 10px;">
        <br>
        <h4 class="mt-4">Game Developers</h4>
        <p>For developers and designers in the gaming industry to explore <strong>Japan</strong>.</p>
</div>
<div class="swiper-slide text-center" style="justify-content: flex-start;" data-country="Germany" data-image="/assets/img/germany-e5-engineer.jpg" data-subtitle="For engineers in product design, aerospace, and automotive" data-text="Join us on a 9-day networking journey through Germany, crafted specifically for engineers. This tour provides a valuable platform for you to connect with peers in your field, exchange ideas, and form relationships that can advance your engineering career. The itinerary includes a tour of Porsche's headquarters, giving you an inside look at the workings of one of the world's leading automotive companies. This trip combines the cultural richness of Germany with networking opportunities, offering you a unique blend of professional growth and cultural exploration.">
    <img src="/assets/img/germany-e5-engineer.jpg" style="width:100%; max-width: 240px; border-radius: 10px;">
        <br>
        <h4 class="mt-4">Engineers</h4>
        <p>For engineers in product design, aerospace, and automotive to explore <strong>Germany</strong></p>
</div>
<div class="swiper-slide text-center" style="justify-content: flex-start;" data-country="Dubai" data-image="/assets/img/dubai-e6-architect.jpg" data-subtitle="For building architects, city planners, and interior designers" data-text="Participate in an 11-day networking journey to Dubai, tailored specifically for architects and urban planners. The trip serves as a platform to engage with a diverse group of professionals, cultivate relationships, and share knowledge that can elevate your career in architecture and urban planning. The itinerary includes a tour of architectural wonders such as the Burj Khalifa, providing a first-hand look at some of the world's most innovative structures. The fusion of Dubai's vibrant architectural scene with this high-value networking opportunity offers a unique setting for professional growth and knowledge exchange.">
    <img src="/assets/img/dubai-e6-architect.jpg" style="width:100%; max-width: 240px; border-radius: 10px;">
        <br>
        <h4 class="mt-4">Architects</h4>
        <p>For building architects, city planners, and interior designers to explore <strong>Dubai</strong></p>
</div>
<div class="swiper-slide text-center" style="justify-content: flex-start;" data-country="Iceland" data-image="/assets/img/iceland-e7-investor.jpg" data-subtitle="For accredited private and public market investors" data-text="Join us on a 9-day networking journey to Iceland, designed specifically for accredited private and public market investors. Amidst the stunning Icelandic landscapes, you'll have the opportunity to interact with a select group of fellow investors, share insights, and form strategic relationships that could influence your investment decisions. The trip includes an unforgettable tour of the northern lights, juxtaposing Iceland's natural splendor with this unique networking opportunity. This journey promises not just an exploration of a breathtaking destination, but also a platform for valuable professional growth and dialogue.">
    <img src="/assets/img/iceland-e7-investor.jpg" style="width:100%; max-width: 240px; border-radius: 10px;">
        <br>
        <h4 class="mt-4">Venture Capital</h4>
        <p>For accredited private and public market investors (all industries) to explore <strong>Iceland</strong></p>
</div>
<div class="swiper-slide text-center" style="justify-content: flex-start;" data-country="Brazil" data-image="/assets/img/brazil-e8-realestate.jpg" data-subtitle="For commercial and residential real estate investors and builders" data-text="Seize the opportunity to elevate your professional network with our bespoke 9-day group trip to Brazil, designed specifically for commercial and residential real estate investors and builders. This journey is not just about discovering a new landscape, but about the invaluable exchanges and connections made with an elite group of industry peers sharing the same passion and ambition. Engage in meaningful dialogue, build powerful alliances, exchange ideas, and let the insights gained from fellow participants catalyze your next major real estate breakthrough.">
    <img src="/assets/img/brazil-e8-realestate.jpg" style="width:100%; max-width: 240px; border-radius: 10px;" >
        <br>
        <h4 class="mt-4">Real Estate</h4>
        <p>For commercial, residential, and market investors and builders to explore  <strong>Brazil</strong></p>
</div>
<div class="swiper-slide text-center" style="justify-content: flex-start;" data-country="Canada" data-image="/assets/img/canada-e10-marketer.jpg">
    <img src="/assets/img/canada-e10-marketer.jpg" style="width:100%; max-width: 240px; border-radius: 10px;">
        <br>
        <h4 class="mt-4">Marketing</h4>
        <p>For marketers in big advertising and digial agencies to explore <strong>Canada (East)</strong></p>
</div>
<div class="swiper-slide text-center" style="justify-content: flex-start;" data-country="Peru" data-image="/assets/img/peru-e11-dentist.jpg" data-subtitle="For dental practioners and surgeons" data-text="Embark on a unique 9-day networking journey to Peru, tailored specifically for leading dentists. This trip presents a rare opportunity to connect with your peers, sharing insights and forging connections that can enhance your dental practice. The trip's highlight includes a mission trip in collaboration with local dental students, providing a chance to give back while learning from the local community. This journey combines professional growth and social responsibility, making it an enriching experience that broadens your horizons both personally and professionally.">
    <img src="/assets/img/peru-e11-dentist.jpg" style="width:100%; max-width: 240px; border-radius: 10px;">
        <br>
        <h4 class="mt-4">Dentists</h4>
        <p>For practioners and dental surgeons to explore <strong>Peru</strong></p>
</div>
<div class="swiper-slide text-center" style="justify-content: flex-start;" data-country="Ghana" data-image="/assets/img/ghana-e12-humanitarian.jpg" data-subtitle="For charity, non-profit, and philantropy leaders" data-text="Join us on an 11-day networking journey to Ghana, specifically designed for philanthropists and leaders of humanitarian non-profits. This unique trip offers the chance to engage with like-minded individuals, allowing for the sharing of insights and building of relationships that can enhance your philanthropic pursuits. The itinerary features collaboration with local NGOs on a mission trip, providing a firsthand look at the impact of community work on the ground. This journey is not just about experiencing the rich culture and history of Ghana, but about leveraging the power of networking for social good.">
    <img src="/assets/img/ghana-e12-humanitarian.jpg" style="width:100%; max-width: 240px; border-radius: 10px;">
        <br>
        <h4 class="mt-4">Humanitarians</h4>
        <p>For charity, non-profit, and philantropy leaders to explore <strong>Ghana</strong></p>
</div>
<div class="swiper-slide text-center" style="justify-content: flex-start;" data-country="Greece" data-image="/assets/img/greece-e13-developer.jpg" data-subtitle="For lead programmers and engineers" data-text="Immerse yourself in an 11-day networking journey to Greece, thoughtfully curated for leading software developers. This trip provides a unique setting to engage with an exclusive circle of peers, fostering relationships and exchanging insights that could significantly impact your software development career. The itinerary includes tours of historical landmarks, beautiful beaches, and immersive cultural experiences, highlighting Greece's rich heritage alongside this networking opportunity. Offering a blend of professional growth and cultural discovery, this journey serves as a unique opportunity to widen your professional network within the backdrop of Greece's inspiring landscape.">
    <img src="/assets/img/greece-e13-developer.jpg" style="width:100%; max-width: 240px; border-radius: 10px;">
        <br>
        <h4 class="mt-4">Software Devs</h4>
        <p>For programmers and engineers from all sub-industries to explore <strong>Greece</strong></p>
</div>
<div class="swiper-slide text-center" style="justify-content: flex-start;" data-country="Spain" data-image="/assets/img/spain-e14-scientist.jpg" data-subtitle="For leading researchers and inventors in the hard sciences" data-text="Join us on a 9-day networking journey to Spain, designed specifically for leading scientists. This trip offers an unparalleled opportunity to interact with a select group of fellow scientists, fostering meaningful connections and promoting the exchange of innovative ideas that can further your scientific endeavors. The itinerary includes tours of prominent scientific facilities, such as the University of Barcelona, providing a unique chance to explore Spain's contributions to the scientific community. This experience combines professional growth and cultural immersion, allowing you to expand your network and broaden your scientific knowledge in the inspiring landscape of Spain.">
    <img src="/assets/img/spain-e14-scientist.jpg" style="width:100%; max-width: 240px; border-radius: 10px;">
        <br>
        <h4 class="mt-4">Scientists</h4>
        <p>For researchers and inventors in the hard sciences to explore <strong>Spain</strong></p>
</div>
<div class="swiper-slide text-center" style="justify-content: flex-start;" data-country="Italy" data-image="/assets/img/italy-e15-healthcare.jpg" data-subtitle="For sommeliers" data-text="Embark on an 11-day networking journey through Italy, meticulously designed for sommeliers. This trip presents an invaluable opportunity to interact with industry peers, share insights, and form relationships that can enrich your understanding of the world of wine. The itinerary includes visits to renowned wine regions and wine-making classes, seamlessly integrating professional networking with an immersive exploration of Italy's celebrated wine culture. This journey combines the charm of Italian viticulture with substantial networking opportunities, presenting a unique blend of professional growth and cultural discovery.">
    <img src="/assets/img/italy-e15-healthcare.jpg" style="width:100%; max-width: 240px; border-radius: 10px;">
        <br>
        <h4 class="mt-4">Sommelier</h4>
        <p>For highly-experienced and certified wine stewards to explore <strong>Italy</strong></p>
</div>
<div class="swiper-slide text-center" style="justify-content: flex-start;" data-country="Portugal" data-image="/assets/img/portugal-e16-physicist.jpg" data-subtitle="For physicists and mathmeticians" data-text="Embark on a 9-day networking journey to Portugal, curated specifically for physicists and mathematicians. This unique opportunity allows you to connect with peers in your field, foster meaningful relationships, and exchange ideas that can influence your future research. The itinerary includes a tour of the University of Lisbon and other significant research sites, providing an insightful exploration of Portugal's contributions to these disciplines. This trip blends professional networking, intellectual enrichment, and cultural immersion, offering a substantial platform for growth and connection in the fields of physics and mathematics.">
    <img src="/assets/img/portugal-e16-physicist.jpg" style="width:100%; max-width: 240px; border-radius: 10px;">
        <br>
        <h4 class="mt-4">Physicists</h4>
        <p>For reseachers, inventors, and mathmeticians to explore <strong>Portugal</strong></p>
</div>
<div class="swiper-slide text-center" style="justify-content: flex-start;" data-country="Bali" data-image="/assets/img/Bali-e17-vetenarian.jpg" data-subtitle="For vetenarian researchers and practicioners" data-text="Join us for a unique 9-day networking journey to Bali, designed exclusively for veterinarians. This experience provides a platform to connect with fellow professionals, exchange insights, and build meaningful relationships that can enhance your veterinary practice. The itinerary features volunteering at a local animal sanctuary and visits to key research sites, merging professional networking with impactful hands-on experience. This journey successfully intertwines professional growth with the rich culture and natural beauty of Bali, offering an opportunity for career enhancement and personal fulfillment.">
    <img src="/assets/img/bali-e17-vetenarian.jpg" style="width:100%; max-width: 240px; border-radius: 10px;">
        <br>
        <h4 class="mt-4">Vetenarians</h4>
        <p>For practicioners and researchers to explore <strong>Bali</strong></p>
</div>