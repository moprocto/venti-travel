@extends("layouts.curator-master")

@section("css")
   <style type="text/css">
   .card{
      background: none;
      background-color: none;
   }
      .card-header{
         width: 100%;
         background-size: cover;
         background-position: bottom;
         border-top-left-radius: 20px !important;
         border-top-right-radius: 20px !important;
      }
      .card-body, .tile-footer{
         background: white;
         padding: 20px;
      }
      .payment-table{
         width: 100%;
      }
      .payment-table > * {
         font-size: 18px;
      }
      .pills-tab{
         display: inline-block;
         width: auto;
         margin: 3px 2px;
         padding-right: 10px;
         padding-left: 10px;
         border-radius: 10px;
         background-color: #ecebea;
         font-size: 13px;
         line-height: 20px;
         font-weight: 400;
         text-align: center;
         -o-object-fit: fill;
         object-fit: fill;
      }
      .table td{

      }
      a.btn-primary {
          background-color: #1d7e89;
          border-color: #1d7e89;
          color: #fff;
       }
      .image-gallery{
         display: block;
         width: 100%;
         
      }
      .image-gallery a {
         display: block;
      }
      .sleep .image-gallery a{
         display: inline-block;
      }
      .sleep .image-gallery{
         display: block;
         overflow-x: scroll;
         white-space:nowrap;
      }

      .image-header{
         width: 75px;
          height: 75px;
          display: table-caption;
          margin-left: 10px;
          margin-top: -86px;
          background-position: center;
          background-size: cover;
          border: 2px solid white;
          border-radius: 10px;
      }

      .image-header.order-0{
         width: 100% !important;
         height: 200px !important;
         background-size: cover;
         background-position: center;
         border: none;
         display: inherit;
         margin-left: 0;
         margin-top: 0;
         border-top-left-radius: 20px;
         border-bottom-left-radius: 0px;
         border-bottom-right-radius: 0px;
         border-top-right-radius: 20px;
         
      }
      
      .card-body{
         border-bottom-left-radius: 20px;
         border-bottom-right-radius: 20px;
      }
      .stop .card-header{
         border-top-left-radius: 0;
         border-top-right-radius: 0;
      }
      .stop .card-body .inner-body{
         padding: 10px 20px;
      }
      .order-0{
         border-top-left-radius: 20px;
         border-top-right-radius: 20px;
      }
      .image-tile{
         display: inline-block;
         width: 200px;
         height: 200px;
         background-size: cover;
         background-position: bottom;
         border-radius: 20px;
         margin-right: 10px;
      }
      .sleep p {
         color: white;
      }
      h2 .btn-block{
         margin: 0;
         padding: 0;
         text-decoration: none !important;
      }
      h2 .btn-block p {
         display: inline-block;
      }
      h2 .btn-block i{
         display: inline-block;
         float: right;
      }
      i.rotate-icon{
         transform: rotate(180deg);
      }
      .collapsed i.rotate-icon{
          transform: rotate(0deg);
      }
   </style>
@endsection

@section("content")
<div id="page-content">
   <div class="gradient-wrap">
      <!--  HERO -->
      <div class="black position-relative hero-section mb-4" style="margin: 20px 4em;">
         <div class="row" style="justify-content: center;">
            <div class="col-md-12 text-center">
               <img src="/assets/img/404.png" style="width: 100%; max-width: 500px; margin: 40px 0;">
               <h1>This page is limited to group members only</h1>
               <p>Contact <a href="mailto:admin@venti.co">admin@venti.co</a> if you believe this is an error.</p>
            </div>
            <div class="col-md-12 text-center">
               <a class="dark-on-light nav-links btn btn-primary" href="/groups"><i class="fas fa-location-arrow"></i> RETURN TO GROUPS</a>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection

@section("js")
<script src="/js/lightbox/lightbox.min.js"></script>
@endsection