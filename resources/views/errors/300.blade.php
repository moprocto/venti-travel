@extends("layouts.curator-master")

@section("css")
   <style type="text/css">
   .card{
      background: none;
      background-color: none;
   }
      
      .image-gallery{
         display: block;
         width: 100%;
         
      }
      .image-gallery a {
         display: block;
      }
      .sleep .image-gallery a{
         display: inline-block;
      }
      .sleep .image-gallery{
         display: block;
         overflow-x: scroll;
         white-space:nowrap;
      }

      .image-header{
         width: 75px;
          height: 75px;
          display: table-caption;
          margin-left: 10px;
          margin-top: -86px;
          background-position: center;
          background-size: cover;
          border: 2px solid white;
          border-radius: 10px;
      }

      .image-header.order-0{
         width: 100% !important;
         height: 200px !important;
         background-size: cover;
         background-position: center;
         border: none;
         display: inherit;
         margin-left: 0;
         margin-top: 0;
         border-top-left-radius: 20px;
         border-bottom-left-radius: 0px;
         border-bottom-right-radius: 0px;
         border-top-right-radius: 20px;
         
      }
      
      .card-body{
         border-bottom-left-radius: 20px;
         border-bottom-right-radius: 20px;
      }
      .stop .card-header{
         border-top-left-radius: 0;
         border-top-right-radius: 0;
      }
      .stop .card-body .inner-body{
         padding: 10px 20px;
      }
      .order-0{
         border-top-left-radius: 20px;
         border-top-right-radius: 20px;
      }
      .image-tile{
         display: inline-block;
         width: 200px;
         height: 200px;
         background-size: cover;
         background-position: bottom;
         border-radius: 20px;
         margin-right: 10px;
      }
      .sleep p {
         color: white;
      }
      h2 .btn-block{
         margin: 0;
         padding: 0;
         text-decoration: none !important;
      }
      h2 .btn-block p {
         display: inline-block;
      }
      h2 .btn-block i{
         display: inline-block;
         float: right;
      }
      i.rotate-icon{
         transform: rotate(180deg);
      }
      .collapsed i.rotate-icon{
          transform: rotate(0deg);
      }
   </style>
@endsection

@section("content")
<div id="page-content">
   <div class="gradient-wrap">
      <!--  HERO -->
      <div class="black position-relative hero-section mb-4" style="margin: 20px 4em;">
         <div class="row" style="justify-content: center;">
            <div class="col-md-12 text-center">
               <img src="/assets/img/300.png" style="width: 100%; max-width: 400px; margin-top: 40px;">
               <h1>We're Performing Maintenance</h1>
               <p>This is temporary, and we'll have systems flying high shortly.</p>
            </div>
            <div class="col-md-12 text-center">
               <a class="dark-on-light nav-links btn btn-primary" href="/"><i class="fas fa-location-arrow"></i> HEAD HOME</a>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection

@section("js")
@endsection