@extends('layouts.boardingpass.master')

@section('css')
	<style type="text/css">
		.card{
			background: transparent;
			border: none;
		}
		
		.nav-item .nav-link{
			width: 100%;
			text-align: left;
			color: black;
		}
		.apexcharts-toolbar{
			display: none !important;
		}
		table > *{
			font-size: 12px;
		}

	</style>
	
	<link href="https://cdn.jsdelivr.net/npm/quill@2.0.2/dist/quill.snow.css" rel="stylesheet" />

<style>
	tr.text-grey > *{
		color: grey;
	}
</style>
@endsection

@section('content')
	<div id="page-content">
	    <div class="gradient-wrap">
		    <div class="container hero-section mb-4 mt-4">
		        <div class="row justify-content-center">
		            <div class="col-md-8 justify-content-center">
		            	<div class="card bg-white">
		            		<div class="card-body">
				            	<form method="POST" action="{{ route('crm-send-mass') }}">
				            		@CSRF
				            		<input type="hidden" name="message" id="email_content">
									<div class="form-group">
										<label class="form-label">Recipients (Copied from CSV)</label>
				            			<textarea class="form-control" name="CSVcontent"></textarea>
									</div>
									<div class="form-group">
										<label class="form-label">Content</label>
										<textarea class="form-control" name="template"></textarea>
									</div>
				            		<div class="form-group">
				            			<input type="submit" class="btn btn-success" value="Send Email">
				            		</div>
				            	</form>
			            	</div>
		            	</div>
		            </div>
		        </div>
		    </div>
		</div>
	</div>
@endsection

@section('js')
	<script src="https://cdn.jsdelivr.net/npm/quill@2.0.2/dist/quill.js"></script>
	<script>
		

// Initialize Quill editor
const quill = new Quill('#editor', {

            theme: 'snow'
        });

	  $('form').on('submit', function() {
            // Get the Quill editor content in HTML format
            const emailContent = quill.root.innerHTML; 

            // Set the hidden input value before submitting
            $('#email_content').val(emailContent);
        });
	</script>
@endsection