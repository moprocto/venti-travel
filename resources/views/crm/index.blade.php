@extends('layouts.boardingpass.master')

@section('css')
	<style type="text/css">
		.card{
			background: transparent;
			border: none;
		}
		
		.nav-item .nav-link{
			width: 100%;
			text-align: left;
			color: black;
		}
		.apexcharts-toolbar{
			display: none !important;
		}
		table > *{
			font-size: 12px;
		}

	</style>
	
	<link href="https://cdn.jsdelivr.net/npm/quill@2.0.2/dist/quill.snow.css" rel="stylesheet" />

<style>
	tr.text-grey > *{
		color: grey;
	}
</style>
@endsection

@section('content')
	<div id="page-content">
	    <div class="gradient-wrap">
		    <div class="container hero-section mb-4 mt-4">
		        <div class="row justify-content-center">
		            <div class="col-md-8 justify-content-center">
		            	<div class="card bg-white">
		            		<div class="card-body">
				            	<form method="POST" action="{{ route('crm-send-email') }}">
				            		@CSRF
				            		<input type="hidden" name="message" id="email_content"> 
				            		<div class="form-group">
				            			<label class="form-label">Their Full Name</label>
				            			<input type="text" name="fname" class="form-control required" required="">
				            		</div>
				            		<div class="form-group">
				            			<label class="form-label">Email</label>
				            			<input type="text" name="to" class="form-control required" required="">
				            		</div>
				            		<div class="form-group">
				            			<label class="form-label">Subject</label>
				            			<input type="text" name="subject" id="subject" class="form-control required">
				            		</div>
				            		<div class="form-group">
				            			<label class="form-label">Note</label>
				            			<div id="editor">
										  <p>Enter text here..</p>
										  <br>
										  <p>Best,<br>Markus Proctor<br>CEO<br>Venti</p>
										</div>
				            		</div>
				            		<div class="form-group">
				            			<input type="checkbox" name="cc"> CC the COO
				            		</div>
				            		<div class="form-group">
				            			<input type="submit" class="btn btn-success" value="Send Email">
				            		</div>
				            	</form>
			            	</div>
		            	</div>
		            </div>
		        </div>
		    </div>
		</div>
	</div>
@endsection

@section('js')
	<script src="https://cdn.jsdelivr.net/npm/quill@2.0.2/dist/quill.js"></script>
	<script>
		

// Initialize Quill editor
const quill = new Quill('#editor', {

            theme: 'snow'
        });

	  $('form').on('submit', function() {
            // Get the Quill editor content in HTML format
            const emailContent = quill.root.innerHTML; 

            // Set the hidden input value before submitting
            $('#email_content').val(emailContent);
        });
	</script>
@endsection