@extends('layouts.curator-master')

@section('css')
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<style type="text/css">
.profile-pic {
    width: 100%;
    height: 100%;
    cursor: pointer;
    border-radius: 50%;
}

.file-upload {
    display: none;
}
.circle {
    width: 125px;
    height: 125px;
    
    margin: 0 auto;
    display: block;
}

.p-image {
  width: 100%;
  text-align: center;
  color: #666666;
  transition: all .3s cubic-bezier(.175, .885, .32, 1.275);
}
.p-image:hover {
  transition: all .3s cubic-bezier(.175, .885, .32, 1.275);
}
.upload-button {
  font-size: 1.2em;
}

.upload-button:hover {
  transition: all .3s cubic-bezier(.175, .885, .32, 1.275);
  color: #999;
}
@media screen and (max-width:  600px){
	.mobMin{
		min-height: 200px;
	}
}
</style>
<link rel="stylesheet" href="/assets/css/intlTelInput.min.css">
@endsection

@section("content")
   <div id="page-content">
      <div class="gradient-wrap">
         <!--  HERO -->
        <div class="black position-relative hero-section mb-4" style="margin: 2em 4em;">
            <div class="row">
               	<div class="col-md-2">
               	</div>
               	<div class="col-md-8">
               		<form method="POST" action="{{ route('edit-profile') }}"  enctype="multipart/form-data">
               			@CSRF
               			<input type="hidden" name="referer" value="{{ Session::get('referer') }}">
               			<input type="hidden" name="pic" value="{{ $user->photoUrl ?? '/assets/img/profile.jpg' }}">
               			@error('success')
               				<div class="alert alert-success">
			            		<span>Your profile has been updated!</span>
			        		</div>
               			@enderror
               			@error('bioerror')
               				<div class="alert alert-danger">
			            		<span>Your bio exceeded the max character length of 150.</span>
			        		</div>
               			@enderror
               			@if(Session::get('newuser') !== null)
               				<div class="alert alert-success">
               					<h3>👋 Welcome to Venti!</h3>
               					<p>We recommend completing a profile before using our platform. A profile picture will be required to be visible to other travelers.</p>
               				</div>
               			@endif
               			@if(Session::get('reset-success') !== null)
               				<div class="alert alert-success">
               					<h3>👍 All set!</h3>
               					<p>Your password has been reset.</p>
               				</div>
               			@endif
						<h2>About Me</h2>
						<p>Information in this section is viewable to others on the Venti platform. Fields marked with * are required.</p>
						<div class="card mb-4" style="border-radius:20px">
							<div class="card-body" style="border-radius:20px">
								<div class="row">
									<div class="col-md-7">
										<div class="form-group">
											<label class="form-label">Name*</label>
											<input type="text" class="form-control" value="{{ $user->displayName }}" name="displayName" id="displayName" required="">
										</div>
										<div class="form-group">
											<label class="form-label">Date of Birth*</label>
											<input type="date" class="form-control pref" name="dob" id="dob" required="" value="{{ $user->customClaims['dob'] ?? ''}}">
										</div>
									</div>
									<div class="col-md-5">
										<div class="form-group">
											<div class="circle">
										       <img class="profile-pic" src="{{ $user->photoUrl ?? '/assets/img/profile.jpg' }}">
										    </div>
										    <div class="p-image">
										    	<p style="margin-bottom: 0;"><i class="fa fa-camera upload-button"></i> 200px x 200px</p>
										    	<p>Must be a clear photo of you</p>
										        <input class="file-upload" type="file" name="avatar"/>
											</div>
										</div>
									</div>
								</div>
								
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
											@php
												$languages = [];
												if(isset($user->customClaims["languages"])){
													foreach($user->customClaims["languages"] as $language){
														array_push($languages, $language["value"] ?? $language);
													}
												}
											@endphp
											<label class="form-label">Languages I Speak*<br><span style="font-size:12px;">Please only select what you can speak fluently</span></label>
											<select class="js-example-basic-multiple form-control" name="languages[]" multiple="multiple" data-minimum-selection-length="1" data-maximum-selection-length="3" required="">
		                                        <option value="English" @if(in_array("English", $languages)) selected @endif>English</option>
		                                        <option value="Spanish" @if(in_array("Spanish", $languages)) selected @endif>Spanish</option>
		                                        <option value="Mandarin" @if(in_array("Mandarin", $languages)) selected @endif>Mandarin</option>
		                                        <option value="Hindi" @if(in_array("Hindi", $languages)) selected @endif>Hindi</option>
		                                        <option value="French" @if(in_array("French", $languages)) selected @endif>French</option>
		                                        <option value="Arabic" @if(in_array("Arabic", $languages)) selected @endif>Arabic</option>
		                                        <option value="Russian" @if(in_array("Russian", $languages)) selected @endif>Russian</option>
		                                        <option value="Portuguese" @if(in_array("Portuguese", $languages)) selected @endif>Portuguese</option>
		                                        <option value="Bengali" @if(in_array("Bengali", $languages)) selected @endif>Bengali</option>
		                                        <option value="Indonesian" @if(in_array("Indonesian", $languages)) selected @endif>Indonesian</option>
		                                        <option value="Urdu" @if(in_array("Urdu", $languages)) selected @endif>Urdu</option>
		                                        <option value="German" @if(in_array("German", $languages)) selected @endif>German</option>
		                                    </select>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label class="form-label">Allow invitations to travel groups?<br><span style="font-size:12px;">We recommend keeping this turned on</span></label>
											<select class="form-control" name="requests">
												<option value="all" @if($user->customClaims['requests'] == 'all') selected @endif>Yes</option>
												<option value="none" @if($user->customClaims['requests'] == 'none') selected @endif>No</option>
											</select>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="card mb-4" style="border-radius:20px">
							<div class="card-body" style="border-radius:20px">
								<div class="row">
									<div class="col-md-4">
										<div class="form-group">
											<label class="form-label">Gender:</label>
											<select class="form-control pref" name="gender" id="gender">
												<option value="Prefer Not to Say" @if($user->customClaims['gender'] == 'Prefer Not to Say') selected @endif>Prefer Not to Say</option>
												<option value="Female" @if($user->customClaims['gender'] == 'Female') selected @endif>Female</option>
												<option value="Male" @if($user->customClaims['gender'] == 'Male') selected @endif>Male</option>
											</select>
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<label class="form-label">Occupation:</label>
											<input type="text" class="form-control" name="occupation" placeholder="Software Engineer" value="{{ $user->customClaims['occupation'] ?? ''}}">
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<label class="form-label">Instagram Username:</label>
											<input type="text" class="form-control" name="instagram" placeholder="venti-travel" value="{{ str_replace('@','',$user->customClaims['instagram'] ?? '')}}">
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-4">
										<div class="form-group">
											<label class="form-label">Smoking<br><span style="font-size:12px;">Any substance, include marijuana and electronic tobacco.</span></label>
											<select class="form-control" name="smoking">
												<option value="I Don't Smoke" @if($user->customClaims['smoking'] == "I Don't Smoke") selected @endif>I Don't Smoke</option>
												<option value="I Smoke Socially" @if($user->customClaims['smoking'] == 'I Smoke Socially') selected @endif>I Smoke Socially</option>
												<option value="I Smoke Often" @if($user->customClaims['smoking'] == 'I Smoke Often') selected @endif>I Smoke Often</option>
											</select>
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<label class="form-label">Drinking:<br><span style="font-size:12px;">Any alcoholic beverage, including wine and hard seltzers.</span></label>
											<select class="form-control" name="drinking">
												<option value="I Don't Drink" @if($user->customClaims['drinking'] == "I Don't Drink") selected @endif>I Don't Consume Alcohol</option>
												<option value="I Drink Socially" @if($user->customClaims['drinking'] == 'I Drink Socially') selected @endif>I Drink Socially</option>
												<option value="I Drink Often" @if($user->customClaims['drinking'] == 'I Drink Often') selected @endif>I Drink Often</option>
											</select>
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<label class="form-label">Religion:<br><span style="font-size:12px;">For now, please list your primary affiliation or belief.</span></label>
											<select class="form-control" name="religion">
												<option value="No Affiliation" @if(isset($user->customClaims['religion']) && $user->customClaims['religion'] == 'No Affiliation') selected @endif>No Affiliation</option>
												<option value="Christian" @if(isset($user->customClaims['religion']) && $user->customClaims['religion'] == 'Christian') selected @endif>Christian</option>
												<option value="Catholic" @if(isset($user->customClaims['religion']) && $user->customClaims['religion'] == 'Catholic') selected @endif>Catholic</option>
												<option value="Sikh" @if(isset($user->customClaims['religion']) && $user->customClaims['religion'] == 'Sikh') selected @endif>Sikh</option>
												<option value="Jewish" @if(isset($user->customClaims['religion']) && $user->customClaims['religion'] == 'Jewish') selected @endif>Jewish</option>
												<option value="Muslim" @if(isset($user->customClaims['religion']) && $user->customClaims['religion'] == 'Muslim') selected @endif>Muslim</option>
												<option value="Hindu" @if(isset($user->customClaims['religion']) && $user->customClaims['religion'] == 'Hindu') selected @endif>Hindu</option>
												<option value="Buddhist" @if(isset($user->customClaims['religion']) && $user->customClaims['religion'] == 'Buddhist') selected @endif>Buddhist</option>
												<option value="Spiritual" @if(isset($user->customClaims['religion']) && $user->customClaims['religion'] == 'Spiritual') selected @endif>Spiritual</option>
												<option value="Atheist" @if(isset($user->customClaims['religion']) && $user->customClaims['religion'] == 'Atheist') selected @endif>Atheist</option>
												<option value="Agnostic" @if(isset($user->customClaims['religion']) && $user->customClaims['religion'] == 'Agnostic') selected @endif>Agnostic</option>
											</select>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<div class="form-group">
											<label class="form-label">Bio:
												<br><span style="font-size:12px;">What should others know about you before joining a group with you? Max: 150 Characters </span></label>
												<textarea class="form-control mobMin" name="bio" id="bio" rows="3">{{ $user->customClaims['bio'] ?? ''}}</textarea>
												<span id="characterCount" class="">{{ 150 - strlen($user->customClaims['bio'] ?? '') }} Characters Remain</span>
										</div>
									</div>
									<div class="col-md-12">
										<div class="form-group">
											<label class="form-label">Interests (4 Max):
											<br><span style="font-size:12px;">What types of activities do you prefer?</span>
											</label>
											<br>
											@include("users.components.profile-interests", ["interests" => $interests])
										</div>
									</div>
								</div>
							</div>
						</div>
						<h2>My Preferences</h2>
						<p>What does your ideal group travel group look like? This helps us recommend other travelers that you are comfortable with. Information you provide in this section is not viewable to anyone else.</p>
						<div class="card mb-4" style="border-radius:20px">
							<div class="card-body">
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
											<label class="form-label">Group Size:</label>
											<select class="form-control pref" name="sizePref" id="sizePref">
												<option value="No Preference" @if($user->customClaims['sizePref'] == 'No Preference') selected @endif>No Preference</option>
												<option value="Four Or Less" @if($user->customClaims['sizePref'] == 'Four Or Less') selected @endif>Four or Smaller</option>
												<option value="No More Than 10" @if($user->customClaims['sizePref'] == 'No More Than 10') selected @endif>No More Than Ten</option>
												<option value="No More Than 20" @if($user->customClaims['sizePref'] == 'No More Than 20') selected @endif>No More Than 20</option>
											</select>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label class="form-label">Age Range:<span id="age-range"></span></label>
											<select class="form-control pref" name="agePref" id="agePref">
												<option value="No Preference" @if($user->customClaims['agePref'] == 'No Preference') selected @endif>No Preference</option>
												<option value="Within 4 Years" @if($user->customClaims['agePref'] == 'Within 4 Years') selected @endif>Within 4 Years</option>
												<option value="Within 8 Years" @if($user->customClaims['agePref'] == 'Within 8 Years') selected @endif>Within 8 Years</option>
												<option value="Within 16 Years" @if($user->customClaims['agePref'] == 'Within 16 Years') selected @endif>Within 16 Years</option>
											</select>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
											<label class="form-label">Gender:</label>
											<select class="form-control pref" name="genderPref" id="genderPref">
												<option value="No Preference"  @if($user->customClaims['genderPref'] == 'No Preference') selected @endif>No Preference</option>
												<option value="Same Gender"  @if($user->customClaims['genderPref'] == 'Same Gender') selected @endif>Same Gender</option>
											</select>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label class="form-label">Spending Style:</label>
											<select class="form-control pref" name="spendingPref" id="spendingPref">
												<option value="No Preference" @if($user->customClaims['spendingPref'] == 'No Preference') selected @endif>No Preference</option>
												<option value="Budget Conscience" @if($user->customClaims['spendingPref'] == 'Budget Conscience') selected @endif>Budget Conscience</option>
												<option value="Enjoys Comfort and Experience" @if($user->customClaims['spendingPref'] == 'Enjoys Comfort and Experience') selected @endif>Enjoys Comfort and Experience</option>
												<option value="Prefers Luxury" @if($user->customClaims['spendingPref'] == 'Prefers Luxury') selected @endif>Prefers Luxury</option>
											</select>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
											<label class="form-label">Smoking</label>
											<select class="form-control pref" name="smokingPref" id="smokePref">
												<option value="No Preference" @if($user->customClaims['smokingPref'] == 'No Preference') selected @endif>No Preference</option>
												<option value="Non-Smokers Only" @if($user->customClaims['smokingPref'] == 'Non-Smokers Only') selected @endif>Non-Smokers Only</option>
												<option value="Smokers Okay" @if($user->customClaims['smokingPref'] == 'Smokers Okay') selected @endif>Smokers Okay</option>
											</select>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label class="form-label">Drinking:</label>
											<select class="form-control pref" name="drinkingPref" id="drinkPref">
												<option value="No Preference" @if($user->customClaims['drinkingPref'] == 'No Preference') selected @endif>No Preference</option>
												<option value="Non-Drinkers Only" @if($user->customClaims['drinkingPref'] == 'Non-Drinkers Only') selected @endif>Non-Drinkers Only</option>
												<option value="Drinkers Okay" @if($user->customClaims['drinkingPref'] == 'Drinkers Okay') selected @endif>Drinkers Okay</option>
											</select>
										</div>
									</div>
								</div>
							</div>
						</div>
						<h4>Summary</h4>
						<p><span id="summary"></span></p>
						<h2>Verification</h2>
						@if(!$verificationComplete)
							<p>Safety is a top priority. Certain Venti features are exclusive to those that verify their profiles. Don't worry, we do not share this information with others on Venti.</p>

							@if(!$user->emailVerified)
								<div class="form-group">
									@if(Session::get('newuser') !== null)
										<p>We've already sent a verification email to {{ $user->email }}. Double-check your spam folder. If you have not yet received one by now, you can request another email below.</p>
									@endif
									<button type="button" class="btn btn-primary" id="sendEmailVerification">RESEND EMAIL <i class="fa fa-envelope"></i></button>
								</div>
								@else
								<div class="form-group">
									<h5>Your email is verified <i class="fa fa-check text-success"></i></h5>
								</div>
							@endif

							@if(!$smsVerified)
								
								<div class="form-group">
									<p style="margin:0">Verify a phone number to pass verification.</p>
									<span style="font-size:12px;">Data rates may apply.</span>
									<div class="form-group mt-2" style="display: flex; align-items: center;">
										
								        <input id="phoneNumber" class="form-control" name="phone" type="tel" style="max-width:300px;" placeholder="Enter Your Cell Number">
								        <button type='button' id="sendSMSVerification" class="btn btn-primary" style="margin-left:5px;"><i class="fa fa-paper-plane"></i></button>
								    </div>
								    <span id="full_number" style="font-size:12px;"></span>
								</div>
								
							@endif
						@else
							<p>Your account is fully verified! You have access to all free features.</p>
						@endif
						<br>
						<input type="submit" id="submit" class="btn btn-success"  value="UPDATE PROFILE">
					</form>
               	</div>
            </div>
            <div class="row">
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script type="text/javascript" src="/assets/js/profile.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script src="/assets/js/intlTelInput.js"></script>
<script>
$(document).ready(function() {

	var input = document.querySelector("#phoneNumber");
    window.intlTelInput(input, {
      // allowDropdown: false,
      // autoHideDialCode: false,
      // autoPlaceholder: "off",
      // dropdownContainer: document.body,
      // excludeCountries: ["us"],
      formatOnDisplay: true,
      // geoIpLookup: function(callback) {
      //   $.get("http://ipinfo.io", function() {}, "jsonp").always(function(resp) {
      //     var countryCode = (resp && resp.country) ? resp.country : "";
      //     callback(countryCode);
      //   });
      // },
      //hiddenInput: "full_number",
      // initialCountry: "auto",
      // localizedCountries: { 'de': 'Deutschland' },
      // nationalMode: false,
      // onlyCountries: ['us', 'gb', 'ch', 'ca', 'do'],
      // placeholderNumberType: "MOBILE",
      // preferredCountries: ['cn', 'jp'],
      // separateDialCode: true,
      utilsScript: "/assets/js/utils.js",
    });

    iti = intlTelInput(input);

	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': '{{ csrf_token() }}'
		}
	});

	$('.js-example-basic-multiple').select2({
        placeholder: "Select tags",
        maximumSelectionLength: 4,
        closeOnSelect: false,
        allowClear: true
    });
    
    var readURL = function(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('.profile-pic').attr('src', e.target.result);
            }
    
            reader.readAsDataURL(input.files[0]);
        }
    }
    
    $(".file-upload").on('change', function(){
        readURL(this);
    });
    
    $(".upload-button, .profile-pic").on('click', function() {
       $(".file-upload").click();
    });

    $(".pref").on("change", function(){
    	calcSummary();
    });

    $("#submit").on("click", function(){
    	if($("#dob").val() == "" || $("#displayName").val() == ""){
    		swal({
					  icon: 'error',
					  title: 'Missing required fields',
					  text: 'Name, Date of Birth, and Language fields are required',
			});
    	}
    });

    
    calcSummary();

    $("#sendEmailVerification").click(function(){
    	var button = $(this);
    	$.ajax({
	        url: "{{ route('send-email-verification') }}",
	        type: 'post',
	        processData: true,
            dataType: 'json',
	        success: function (data) {
	        	if(data == 200){
	        		swal({
					  icon: 'success',
					  title: 'On the Way!',
					  text: 'Your verification email has been sent. Check your spam folder just in case.',
					});
					button.parent().append("<div class='alert alert-success'><p>Verification email sent!</p></div>");
	            	button.remove();
	        	} else {
	        		swal({
					  icon: 'error',
					  title: 'Oops...',
					  text: 'We hit a snag sending an email. Try again in a few minutes.',
					})
	        	}
	        },
	        error: function (xhr, ajaxOptions, thrownError){
	        	swal({
					  icon: 'error',
					  title: 'Oops...',
					  text: 'We hit a snag sending an email. Try again in a few minutes.',
				})
	        }
	    });
    });
    var phoneNumber;

    $("#sendSMSVerification").click(function(){
    	var button = $(this);
    	var phoneNumber = iti.getNumber(); //input.getNumber(); //$("#phoneNumber").val();
    	if(phoneNumber.length < 10 || phoneNumber == ""){
    		alert("Hmmm the phone number doesn't seem right");
    	} else {

    		if(!phoneNumber.includes("+")){
    			var countryData = iti.getSelectedCountryData();
    			phoneNumber = "+" + countryData.dialCode + phoneNumber;
    		}

    		$.ajax({
		        url: "{{ route('send-sms-verification') }}",
		        type: 'post',
		        processData: true,
	            dataType: 'json',
	            data:{
	            	phoneNumber: phoneNumber
	            },
		        success: function (data) {
		        	console.log(data);
		        	if(data != "error"){

		        		button.parent().html("<br><input type='number' class='form-control' placeholder='123456' id='smscode' minlength='6' style='width:200px'><button type='button' data-phoneNumber='" + data.phoneNumber +"' data-service-id='" + data.serviceID + "' style='margin-left:5px;' class='btn btn-primary' id='verifySMS'><i class='fa fa-check'></i></button>");
		        		button.parent().parent().append("<p style='Enter the code we texted you. It may take a few seconds to arrive.'></p>");
		        	}else{
		        		alert("There was an error sending a text message to: " + $("#phoneNumber").val() + ". Please make sure the phone number is correct and the appropriate country code is applied.");
		        	}
		            
		        },
			   error: function(jqXhr, textStatus, errorMessage){
			      console.log("Error: ", errorMessage);
			   }
	    	});

	    	$("#full_number").text("We sent a txt to: " + phoneNumber);
    	}
    	
    });

    $("body").on("click", "#verifySMS", function(){
    	var button = $(this);
    	var smsCode = $("#smscode").val();
    	var serviceID = $(this).attr("data-service-id");
    	var phoneNumber = $(this).attr("data-phoneNumber");

    	if(phoneNumber.length < 10 || serviceID == ""){
    		alert("Hmmm the phone number doesn't seem right");
    	}



    	else {
    		$.ajax({
		        url: "{{ route('verify-sms-code') }}",
		        type: 'post',
		        processData: true,
	            dataType: 'json',
	            data:{
	            	phoneNumber: phoneNumber,
	            	smsCode: smsCode,
	            	serviceID: serviceID
	            },
		        success: function (data) {
		        	if(data == "ok"){
		        		button.parent().html("<p>Phone number verified <i class='fa fa-check'></i></p>");
		        	}
		            else {
		            	alert("That code isn't correct.");
		            }
		        }
		    });
    	}
    });

    $('.js-example-basic-multiple-interests').select2({
             placeholder: "Select Interests",
             maximumSelectionLength: 4,
             closeOnSelect: false,
             allowClear: true
         });
    $('#bio').on('keydown, keyup', function(e) {
    	 var remain = 150 - $(this).val().length;
	     $('#characterCount').text(remain + " Characters Remain");
	     if(parseInt(remain) <= 0){
	     	$('#characterCount').addClass("text-danger");
	     } else {
	     	$('#characterCount').removeClass("text-danger");
	     }
	});
});
</script>
@if(env('APP_ENV') != "local")
	<script>
        !function(f,b,e,v,n,t,s)
        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};
        if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
        n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t,s)}(window, document,'script',
        'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '1169331916805166');
        fbq('track', 'CompleteRegistration');
    </script>
    <noscript><img height="1" width="1" style="display:none"
    src="https://www.facebook.com/tr?id=1169331916805166&ev=PageView&noscript=1"
    /></noscript>
@endif
@endsection