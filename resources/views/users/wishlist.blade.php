@extends('layouts.curator-master')

@section('css')
<script src='https://api.mapbox.com/mapbox-gl-js/v1.8.0/mapbox-gl.js'></script>
    <link href='https://api.mapbox.com/mapbox-gl-js/v1.8.0/mapbox-gl.css' rel='stylesheet' />
    <script src='https://tiles.locationiq.com/v2/js/liq-styles-ctrl-gl.js?v=0.1.6'></script>
    <link href='https://tiles.locationiq.com/v2/css/liq-styles-ctrl-gl.css?v=0.1.6' rel='stylesheet' />
    <script src="https://tiles.locationiq.com/v2/libs/gl-geocoder/4.5.1/locationiq-gl-geocoder.min.js?v=0.2.2"></script>
    <link rel="stylesheet" href="https://tiles.locationiq.com/v2/libs/gl-geocoder/4.5.1/locationiq-gl-geocoder.css?v=0.2.2" type="text/css" />
    <style type="text/css">
        .button-group input, .button-group span{
            display: inline-block !important;
        }
        .button-group{
            max-width: 300px;
            display: flex;
        }
        .button-group span{
            margin-left: -42px;
        }
        .select2-results {
            max-height: none;
        }
        .wizard>.content>.body ul>li.select2-selection__choice{
            display: inline-block !important;
            max-width: 200px;
        }
                .searchResult{
            background: transparent;
        }
        .searchResult .card-body{
            background-color: white;
            padding: 12px;
            border-bottom-left-radius: 12px;
            border-bottom-right-radius: 12px;
        }
        .searchResult .card-img-top{
            border-top-left-radius: 12px;
            border-top-right-radius: 12px;
        }
        .form-label{
            font-weight: bold;
        }
        .wizard .steps{
            display: none;
        }
        .select2{
            width: 100% !important;
        }
        .form-label{
            font-weight: bold;
        }
    </style>
<style type="text/css">
table tr td{
    vertical-align: middle !important;
}
@media screen and (max-width:  600px){

table td {
    display: flex;
    justify-content: space-between;
    align-items: center;
  }
  table thead{
    display: none;
  }
table td::before {
    content: attr(label);
    font-weight: bold;
    min-width: 55px;
    text-align: center;
    vertical-align: middle;

  }
}
</style>
@endsection

@section("content")
    <div id="page-content">
        <div class="gradient-wrap">
             <!--  HERO -->
            <div class="black position-relative hero-section mb-4" style="margin: 2em 4em;">
                <div class="row">
                   	<div class="col-md-8">
                        <h1 style="cursor: pointer;" id="toggleModal">Wishlist <img src="/assets/img/plus-circle.png" width="20" style="margin-top:-10px;"></h1>
                        <div class="card" style="border-radius:20px;">
                            <div class="card-body">
                                @if(sizeof($wishlists) > 0)
                                    <table class="table">
                                        <thead>
                                            <th class="text-left">Date</th>
                                            <th>Destination</th>
                                            <th>Visibility</th>
                                            <th class="text-center"></th>
                                        </thead>
                                        <tbody>
                                            @foreach($wishlists as $wishlist)
                                                <tr>
                                                    <td label="Date" class="text-left">{{ $wishlist["month"] }}, {{ $wishlist["year"] }}</td>
                                                    <td label="Destination">{{ $wishlist["destination"] }}, {{ $wishlist["country"] }}</td>
                                                    <td label="Visibility">{{ $wishlist["privacy"] }}</td>
                                                    <td class="text-center">
                                                        <button type="button" class="btn btn-white editWishlistItem" data-title="{{ $wishlist['destination'] }}" data-year="{{ $wishlist['year'] }}" data-month="{{ $wishlist['month'] }}" data-privacy="{{ $wishlist['privacy'] }}" data-wishlistID="{{ $wishlist['wishlistID'] }}"><i class="fa fa-pencil"></i></button>
                                                        <button type="button" class="btn btn-danger deleteWishlistItem" data-wishlistID="{{ $wishlist['wishlistID'] }}"><i class="fa fa-trash"></i></button></td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                @else
                                    <h5>Add destinations to your wishlist to help us find the perfect group for you.</h5>
                                @endif
                            </div>
                        </div>
                   	</div>
                </div>
                <div class="row">
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="wishlistModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-body text-left">
            <h5 class="modal-title">Add a Destination</h5>
            <input type="hidden" name="wishlistID" id="wishlistID" value="{{ Date('mmddYHiss') }}">
            <input type="hidden" name="archived" value="1" id="willArchive">
            <input type="hidden" name="country" id="country" id="setcountry">
            <input type="hidden" name="latitude" id="latitude">
            <input type="hidden" name="longitude" id="longitude">
            <input type="hidden" name="destination" id="destination">
            <input type="hidden" name="imageURL" id="imageURL">
            <input type="hidden" name="imageAuthorProfile" id="imageAuthorProfile">
            <input type="hidden" name="imageAuthor" id="imageAuthor">
            <br><br>
                <div class="form-group">
                    <label class="form-label">Destination</label>
                    <div class="container">
                        <div class="search-box" id="search-box" style="width: 100%" name="destination"></div>
                        <div id="result"></div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="form-label">Preferred Month</label>
                    <p>Just use an approximation. You can change this and the year at any time.</p>
                    <select class="form-control" id="month" required="">
                        <option value="">-- Select Month --</option>
                        <option value="January">January</option>
                        <option value="February">February</option>
                        <option value="March">March</option>
                        <option value="April">April</option>
                        <option value="May">May</option>
                        <option value="June">June</option>
                        <option value="July">July</option>
                        <option value="August">August</option>
                        <option value="September">September</option>
                        <option value="October">October</option>
                        <option value="November">November</option>
                        <option value="December">December</option>
                    </select>
                </div>
                <div class="form-group">
                    <label class="form-label">Preferred Year</label>
                    <select class="form-control" id="year" required="">
                        <option value="">-- Select Year --</option>
                        @for($i = Date('Y'); $i < Date('Y') + 5; $i++)
                            <option value="{{ $i }}">{{ $i }}</option>
                        @endfor
                    </select>
                </div>
                <div class="form-group">
                    <label class="form-label">Visibility</label>
                    <select class="form-control" id="privacy" required="">
                        <option value="">-- Please Select --</option>
                        <option value="Show on Profile">Show on Profile</option>
                        <option value="Hide from Profile">Hide from Profile</option>
                    </select>
                </div>
            <br>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary closeModal" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-success createWishlist">Create</button>
          </div>
        </div>
      </div>
    </div>
    <div class="modal fade" id="wishlistModalEdit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-body text-left">
            <h5 class="modal-title" id="tilesTitle"></h5>
            <br><br>
                
                <div class="form-group">
                    <label class="form-label">Preferred Month</label>
                    <p>Just use an approximation. You can change this and the year at any time.</p>
                    <select class="form-control" id="monthEdit" required="">
                        <option value="">-- Select Month --</option>
                        <option value="January">January</option>
                        <option value="February">February</option>
                        <option value="March">March</option>
                        <option value="April">April</option>
                        <option value="May">May</option>
                        <option value="June">June</option>
                        <option value="July">July</option>
                        <option value="August">August</option>
                        <option value="September">September</option>
                        <option value="October">October</option>
                        <option value="November">November</option>
                        <option value="December">December</option>
                    </select>
                </div>
                <div class="form-group">
                    <label class="form-label">Preferred Year</label>
                    <select class="form-control" id="yearEdit" required="">
                        <option value="">-- Select Year --</option>
                        @for($i = Date('Y'); $i < Date('Y') + 5; $i++)
                            <option value="{{ $i }}">{{ $i }}</option>
                        @endfor
                    </select>
                </div>
                <div class="form-group">
                    <label class="form-label">Visibility</label>
                    <select class="form-control" id="privacyEdit" required="">
                        <option value="">-- Please Select --</option>
                        <option value="Show on Profile">Show on Profile</option>
                        <option value="Hide from Profile">Hide from Profile</option>
                    </select>
                </div>
            <br>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary closeModal" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-success" id="wishlistSave" data-wishlistID="">Save Changes</button>
          </div>
        </div>
      </div>
    </div>
@endsection

@section('js')
<script>
$(document).ready(function() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': "{{ csrf_token() }}"
        }
    });

    $("#toggleModal").click(function(){
        $("#wishlistModal").modal("show");
    });

    $(".closeModal").click(function(){
        $("#wishlistModal").modal("hide");
        $("#wishlistModalEdit").modal("hide");
    });

    $(".editWishlistItem").click(function(){
        $("#tilesTitle").text($(this).attr("data-title"));
        $("#yearEdit").val($(this).attr("data-year"));
        $("#monthEdit").val($(this).attr("data-month"));
        $("#privacyEdit").val($(this).attr("data-privacy"));
        $("#wishlistSave").attr("data-wishlistid", $(this).attr("data-wishlistid"));
        $("#wishlistModalEdit").modal("show");
    })

    $(".deleteWishlistItem").click(function(){
        var itemID = $(this).attr('data-wishlistID');
        var row = $(this).parent().parent();
        // accept invite

        $.ajax({
            url: "{{ route('user-wishlist-remove') }}",
            processData: true,
            dataType: 'json',
            data:{
                itemID: itemID
            },
            type: 'post',
            success: function (data) {
                row.remove();
            }
        });
    });

    function cleanNumber(text){
            text = text.replace(".", "");
            text = text.replace("-", "");
            return text;
        }

    locationiq.key = "{{ env('LOCATIONIQ_KEY') }} ";

        //Add Geocoder control to the map
    var geocoder = new MapboxGeocoder({
        accessToken: locationiq.key,
        limit: 5,
        dedupe: 1,
        name: "destination",
        id:"destination",
        getItemValue: function (item) {
            var name = item.address.name;

            if(item.address.city !== undefined){
                if(item.address.country == "USA" || item.address.country == "United States" || item.address.country == "United States of America"){
                    name = item.address.name;
                } else {
                    name = item.address.name + " " + item.address.country;
                }
            }

            $("#tripJoin").text($("#tripID").val());
            $("#destination").val(name);
            $("#country").val(item.address.country);
            $("#latitude").val(item.lat);
            $("#latJoin").text(cleanNumber(item.lat));
            $("#longitude").val(item.lon);
            $("#longJoin").text(cleanNumber(item.lat));


            return item.place_name
        }
    });

    geocoder.addTo('#search-box');

    $("#wishlistSave").click(function(){
        var year = $("#yearEdit").val();
        var month = $("#monthEdit").val();
        var privacy = $("#privacyEdit").val();
        var wishlistID = $(this).attr("data-wishlistid");

        if(year == "" || month == "" || privacy == ""){
            return alert("All fields are required to add to your wishlist.");
        }

        $.ajax({
            url: "{{ route('user-wishlist-update') }}",
            processData: true,
            dataType: 'json',
            data:{
                year: year,
                month: month,
                privacy: privacy,
                wishlistID: wishlistID
            },
            type: 'post',
            success: function (data) {
                alert($("#tilesTitle").text() + " has been updated.");
                location.reload();
            }
        });

    });

    $(".createWishlist").click(function(){

        var year = $("#year").val();
        var month = $("#month").val();
        var privacy = $("#privacy").val();


        if(year == "" || month == "" || privacy == ""){
            return alert("All fields are required to add to your wishlist.");
        }

        $.ajax({
            url: "{{ route('user-wishlist-create') }}",
            processData: true,
            dataType: 'json',
            data:{
                year: year,
                month: month,
                privacy: privacy,
                latitude: $("#latitude").val(),
                longitude: $("#longitude").val(),
                country: $("#country").val(),
                destination: $("#destination").val(),
            },
            type: 'post',
            success: function (data) {
                alert($("#destination").val() + " has been added to your wishlist.");
                location.reload();
            }
        });
    })
});
</script>
@endsection