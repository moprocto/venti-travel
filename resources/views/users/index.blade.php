@extends("layouts.curator-master")

@section("css")
   <link href="/css/jRange/jrange.css" rel="stylesheet" />
   <link href="/css/lightbox/lightbox.min.css" rel="stylesheet" />
   <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
   <link href="/assets/css/store.css" rel="stylesheet" />
   <style type="text/css">
      @media screen and (max-width:  600px){
         .grid{
            margin-top: 25px;
         }
         .card-row{
         	margin: 0;
         }
         .grid .card{
         	min-width: 170px !important;
         	min-height: 170px !important;
         }
         .gutter-sizer { width: 4%; }
         .grid-sizer { width: 22%; }
         #accordionExample, .accordion-body{
            display: none;
         }
      }
       @media screen and (min-width:  600px){
         .gutter-sizer { width: 1%; }
         .grid-sizer { width: 10%; }
      }

      .good:before {
        content: " "; /* to ensure it displays */
        border-radius: 20px;
        z-index: 2;
        position: absolute;
        left: 0;
        right: 0;
        top: 0;
        bottom: 0;
        border: 3px solid rgba(76, 175, 80, 0.68);
        pointer-events: none; /* user can't click on it */
      }
      .user-interests{
         background: white;
          position: absolute;
          top: 10px;
          background: rgba(255,255,255,0.9);
          border-radius: 20px;
          display: flex;
          justify-content: space-between;
          padding: 0 9px;
          right: 10px;
      }
      .interest-emoji{
         padding: 0 3px;
      }
   </style>
@endsection

@section("content")
	<div id="page-content">
      	<div class="gradient-wrap">
         <!--  HERO -->
			<div class="black position-relative hero-section mb-4" style="margin: 2em 4em;">
				<h1>Connect</h1>
				
            
				<div class="row">
					<div class="col-md-3">
                  <p>Find people to invite to your group. We only show verified profiles.</p>
						@if(Session::get('user') !== null)
                     @if(!isProfileComplete(Session::get('user'))[0])
                        <span style="display:block;" class="mb-4">{!! isProfileComplete(Session::get('user'))[1] !!} to be visible to others.</span>
                     @endif
                  @endif
                  <div class="row card-row" style="padding:0;">
                     @include('users.components.filters')
                  </div>
					</div>
					<div class="col-md-9">
						<div class="grid">
						@foreach($users as $user)
							@if(!is_null($user))
                        @if($user->photoUrl != "/assets/img/profile.jpg" && $user->photoUrl != "/assets/img/profile-2.jpg" && array_key_exists("dob", $user->customClaims) && !is_null($user->customClaims["dob"]) && calculateAge($user->customClaims["dob"]) >= 18 && $user->emailVerified == true)
                           @include('users.components.browse-widget', ["user" => $user])
                        @endif
							@endif
						@endforeach
						</div>
					</div>
				</div>
        	</div>
    	</div>
	</div>
@endsection

@section('js')
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script src="/js/isotope/isotope.js"></script>
<script src="/assets/js/connect.js"></script>
@endsection
