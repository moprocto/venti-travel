@extends("layouts.curator-master")

@section('css')
<link href="/assets/css/store.css" rel="stylesheet" />
	<style>
		.groupList .grid-item{
			max-width: 350px;
			width: 50%;
			max-width: 300px;
    		padding: 0;
		}
		.space-between{
			align-items: center;
		    justify-content: space-between;
		    display: flex;
		    margin-bottom: 5px;
		}
		.groupList{
			display: flex;
			width: 100%;
		}
		.card-container{
			margin: 5px;
			border-radius: 20px;
        	box-shadow: 0 1px 4px rgba(0,0,0,0.15);
		}
		.tile-footer{
			margin-bottom: 0px;
		}
		@media screen and (max-width:  600px){
         .col-xs-12{
         	width: 100% !important;
         	max-width: 100% !important;
         	display: block !important;
         }
         .groupList{
         	margin-top: 25px;
         }
         .navtab-bg{
         	margin-top: 25px;
         }
         #groups,#postcards{
         	overflow-x: scroll;
         }
         .storyCard, .wishlistCard{
         	width: 100% !important;
         	max-width: 100% !important;
         	min-width: 290px;
         }
      }
      .storyCard{
      	margin: 5px;
      }
      .nav-pills .nav-link{
      	color: black;
      	font-weight: bold;
      }
      .nav-pills .nav-link.active{
      	background: black;
      	color: white !important;
      }
		.chart-third{fill:#458B00}
		.chart-second{fill:#e6e600}
		.chart-first{fill:#e92213}
		.needle,.needle-center{fill:#464A4F}
		#scale10{
			margin-top: 10px;
			display: none;
		}
.category{
         text-transform: uppercase;
         background: black;
         color: white;
         font-size: 12px;
         padding: 5px 10px;
         border-radius: 10px;
         margin-top: -3px;
      }
	</style>
	<script src="https://cdn.plot.ly/plotly-2.14.0.min.js"></script>
@endsection

@section("content")
	<div id="page-content">
      	<div class="gradient-wrap">
         <!--  HERO -->
			<div class="black position-relative hero-section mb-4" style="margin: 2em 4em;">
				<div class="row">
					<div class="col-md-3">
						<div class="card" style="border-radius:20px;">
							<img src="{{ $user->photoUrl }}" style="border-top-left-radius:20px; border-top-right-radius: 20px;">
							<div class="card-body" style="border-bottom-left-radius: 20px; border-bottom-right-radius: 20px;">
								<div style="justify-content: space-between; display:flex;">
									<h3>{{ "@" . $user->customClaims['username'] }}</h3>
									@if(isset($user->customClaims['instagram']) && $user->customClaims['instagram'] != "")<a href="https://instagram.com/{{ str_replace('@','',$user->customClaims['instagram']) }}" target="_blank" style="font-size:20px;"><i class="fab fa-instagram"></i></a>@endif
								</div>
								<h5>{{ getFirstName($user->displayName) }}@if(array_key_exists("dob", $user->customClaims)), {{ calculateAge($user->customClaims['dob']) }}@endif
									@if(($user->customClaims['nav'] ?? null) === true)
										<img src="/assets/img/navigator-verified.png" style="width:20px; margin-top:-5px;" title="Verified"/> 
									@endif
									<br>{!! calculateOccupation($user->customClaims["occupation"] ?? "") !!}</h5>

								<p><span class="font-weight-bold">ABOUT:</span><br>
								{{ $user->customClaims['bio'] ?? "" }}
								<ul>
									{!! calculateGender($user->customClaims['gender']) !!}
									{!! calculateSmoking($user->customClaims['smoking'] ?? "none") !!}
									{!! calculateDrinking($user->customClaims['drinking'] ?? "none") !!}
									@if(isset($user->customClaims['religion']) && $user->customClaims['religion'] != "none")
										<li>{{ ucfirst($user->customClaims['religion']) }}</li>
									@endif
								</ul>
								</p>
								@if(isset($user->customClaims['interests']) && !is_null($user->customClaims['interests']) && sizeof((array) $user->customClaims['interests']) > 0)
									<p><span class="font-weight-bold">INTERESTS:</span><br>
									@include("users.components.interest-badges", ["user" => $user])
								@endif
								<p>
									@if(isset($user->customClaims['languages']) && !is_null($user->customClaims['languages']) && sizeof((array) $user->customClaims['languages']) > 0)

										<span class="font-weight-bold">LANGUAGES</span><br>
										@foreach($user->customClaims['languages'] as $language)
						                  <div class="pills-tab" style="text-transform: uppercase; font-weight: bold; background-color:rgba({{ strlen($language['value'] ?? $language) * 20  }}, 168, {{ strlen($language['value'] ?? $language) * 10  }}, 0.45)">{{ $language['value'] ?? $language }}</div>
						                  @endforeach
									@endif
								</p>
								@if($user->uid != Session::get('user')->uid)
								<p class="text-center">
								@if($user->customClaims['requests'] == 'all')
									@if(isProfileComplete(Session::get('user'))[0])
										@if($compatibility[1] >= 50)
											<button type="button" class="btn btn-primary" id="createInvite">INVITE TO GROUP</button>
										@endif
									@else
										<h5>{!! isProfileComplete(Session::get('user'))[1] !!} to send invites</h5>
									@endif
								@endif
								</p>
								<p class="text-center">
									<a href="mailto:admin@venti.co?subject=Reporting User: {{ $user->customClaims['username'] }}" class="text-danger">Report User</a>
								</p>
								@endif
								@if($user->emailVerified === true)
									<h6><i class="fa fa-check"></i> Email Verified</h6>
								@endif
								@if(array_key_exists("smsVerified", $user->customClaims) && $user->customClaims["smsVerified"] === true)
									<h6><i class="fa fa-check"></i> Phone Number Verified</h6>
								@endif
							</div>
						</div>
						@if(Session::get('user')->uid != $user->uid)
							@if($complete && $compatibility[1] != 0 && $compatibility[1] >= 50)
								<div class="card mt-4" style="border-radius:20px;">
									<div class="card-body" style="border-radius:20px;">
										<div class="chart-gauge"></div>
										<div id="gauge-title" style="margin-top:-20px"></div>
										<p style="font-size:11px;">This estimate is purely based on information you've both provided Venti about yourself and what you're looking for in a travel group. We recommend everyone to exercise due diligence when deciding to join groups.</p>
									</div>
								</div>
							@endif
							@if(!isProfileComplete(Session::get('user'))[0])
								<div class="card mt-4" style="border-radius:20px;">
									<div class="card-body" style="border-radius:20px;">
										<img src="/assets/img/compatibility-disabled.jpg" style="opacity: 0.4; width:100%; max-width:300px; margin:0 auto;">
										<p>{!! isProfileComplete(Session::get('user'))[1] !!} to see how compatible you are with {{ getFirstName($user->displayName) }} as a travel buddy.</p>
									</div>
								</div>
							@endif
						@endif
					</div>
					<div class="col-md-9">
						<ul class="nav nav-pills navtab-bg nav-justified" style="margin-bottom:10px">
					        <li class="nav-item" style="max-width:100px;">
					            <a href="#groups" data-bs-toggle="tab" aria-expanded="false" class="nav-link active">
					                GROUPS
					            </a>
					        </li>
					        <li class="nav-item" style="max-width:130px;">
					            <a href="#postcards" data-bs-toggle="tab" aria-expanded="false" class="nav-link">
					                POSTCARDS
					            </a>
					        </li>
					        <li class="nav-item" style="max-width:110px;">
					            <a href="#wishlist" data-bs-toggle="tab" aria-expanded="false" class="nav-link">
					                WISHLIST
					            </a>
					        </li>
					    </ul>
					    <div class="tab-content">
                			<div class="tab-pane active grid" id="groups">
                				<div class="groupList">
										@foreach($groups as $group)
											@if(array_key_exists("navigated", $group) && $group["navigated"] == "1")
												@include("groups.components.widget-navigated", ["group" => $group])
											@else
												@if($group["isTravel"])
													@include("groups.components.widget-travel", ["group" => $group])
												@else
													@include("groups.components.widget-activity", ["group" => $group])
												@endif
											@endif
											
										@endforeach
										@if(sizeof($groups) == 0)
											<p>{{ getFirstName($user->displayName) }} is not part of any travel groups yet.</p>
										@endif
									</div>
                			</div>
                			<div class="tab-pane" id="postcards">
                				<div class="groupList">
                				@foreach($postcards as $postcard)
                					@if($postcard["type"] == "image")
			                           @include("stories.components.image-card", ["story" => $postcard ])
			                        @endif

			                        @if($postcard["type"] == "video")

			                           @include("stories.components.video-card",["story" => $postcard ])
			                     	@endif
                				@endforeach
                				@if(sizeof($postcards) == 0)
										<p>{{ getFirstName($user->displayName) }} has not shared any postcards yet.</p>
									@endif
                				</div>
                			</div>
                			<div class="tab-pane" id="wishlist">
                				<div class="groupList">
                					@foreach($wishlists as $wishlist)
                						@include("users.components.wishlist-widget", ["wishlist" => $wishlist])
                					@endforeach
                				</div>
                			</div>
					</div>
					<div class="col-md-2">
					</div>
				</div>
        	</div>
    	</div>
	</div>
@endsection

@section('js')
	<script type="text/javascript" src="https://d3js.org/d3.v3.min.js"></script>
	<script src="/js/isotope/isotope.js"></script>
	<script>
		$(document).ready(function() {
			$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': '{{ csrf_token() }}'
				}
			});

			$("#createInvite").click(function(){
				$.ajax({
	                url: "{{ route('user-profile-groups', ['uid' => $user->uid]) }}",
	                type: "GET",
	                data: {id: 5},
	                dataType: "json",
	                success: function (data) {
	                	console.log(data);
	                	var myhtml = document.createElement("div");
	                	var html = '<ul style="list-style:none;">';
	                	jQuery.each(data, function(i, value) {
	                		html += "<li class='space-between'><span>" + value.title + "</span><button type='button' class='btn btn-primary sendInvite' data-tripID='" + value.tripID +"'><i class='fa fa-paper-plane'></i></button></li>";
	                	});
	                    
	                    html += '</ul>';
	                    myhtml.innerHTML = html;

	                    swal({
						  title: "Send Invite To...", 
						  content: myhtml,
						  confirmButtonText: "Done", 
						});
	                }
	            });


			});

			var $grid = $('.grid').isotope({
                 itemSelector: '.grid-item'
         });
        

			$("body").on("click", ".sendInvite", function(){
            	var uid = "{{ $user->customClaims['username'] }}";
            	var tripID = $(this).attr("data-tripID");
            	var button = $(this);

            	$.ajax({
				        url: "{{ route('group-invite') }}",
				        type: 'post',
				        processData: true,
		                dataType: 'json',
		                data:{
		                	tripID: tripID,
		                	user: uid
		            },
				        success: function (data) {
				        	if(data != 500 && data != 501 && data != 502 && data != 503 && data != 504 && data != 505){
				        		button.removeClass("btn-primary");
				        		button.removeClass("sendInvite");
				        		button.addClass("btn-success");
				        		button.html('<i class="fa fa-thumbs-up"></i>');
				        	}
				        	if(data == 502){
				            	alert("This user cannot be added");
				            }
				            if(data == 503){
				            	alert("You cannot add someone that is already in your group :)");
				            }
				            if(data == 504){
				            	alert("We could not find that user.");
				            }
				            if(data == 505){
				            	alert("This user is not accepting invites at this time.");
				            }
				        }
		    	});
            });

            $(".like-box").click(function(){
        var storyID = $(this).attr("data-story");
        var liker = $(this);
        var likeCount = parseInt(liker.find(".count").text());
         @if(Session::get('user') !== null)
            $.ajax({
               url: "{{ route('like-story') }}",
               type: 'post',
               processData: true,
               dataType: 'json',
               data:{
                  storyID: storyID
               },
               success: function (data) {
                  if(data == "200"){
                     liker.find("img").attr("src", "/assets/img/smile-liked.png");
                     likeCount++;
                     liker.find(".count").text(likeCount);
                  } else{
                     liker.find("img").attr("src", "/assets/img/smile.png");
                     likeCount--;
                     liker.find(".count").text(likeCount);
                  }
               }
            });
         @else
            var myhtml = document.createElement("div");
            myhtml.innerHTML = '<p>You need to be signed in to like a post!</p><a href="/login" class="btn btn-primary">LOG IN</a>';
            swal({
                    icon: 'warning',
                    title: 'Oh, hello!',
                    content: myhtml
            })
         @endif
      });

        	@if($complete && $compatibility[1] != 0 && Session::get('user')->uid != $user->uid)
        		$("#gauge-title").html("<p class='text-center'>Compatibility</p>")
        		@include('users.components.compatibilityJS', ["compatibility" => $compatibility[1]] )
			@endif

		});
	</script>
@endsection
