@extends("layouts.curator-master")

@section('css')

@endsection

@section('content')

<div id="page-content">
    <div class="gradient-wrap">
         <!--  HERO -->
        <div class="black position-relative hero-section mb-4" style="margin: 2em 4em;">
            <div class="row">
               	<div class="col-md-2">
               	</div>
               	<div class="col-md-8">
               		<h1>Settings</h1>
               		<form class="form" method="POST" action="{{ route('user-settings-update') }}">
               			@CSRF

                    	@if(Session::get('success') !== null)
               				<div class="alert alert-success">
               					<p>Your settings have been updated</p>
               				</div>
               			@endif
	                    <div class="card mb-4" style="border-radius:20px;">
	                    	<div class="card-body">
	                    		<div class="form-group">
	                    			<h5 class="form-label">Email Notifications</h5>
	                    			<ul style="list-style:none; ">
	                    				<li><input type="checkbox" class="form-check-input" name="emailJoinRequest" @if(isset($settings['emailJoinRequest']) && $settings['emailJoinRequest'] == 0) unchecked @else checked @endif value="1"> When someone requests to join my group</li>
	                    				<li><input type="checkbox" class="form-check-input" name="emailInvited" @if(isset($settings['emailInvited']) && $settings['emailInvited'] == 0) unchecked @else checked @endif value="1"> <span class="form-check-label">When I'm invited to a trip</span></li>
	                    				<li><input type="checkbox" class="form-check-input" name="emailInviteAccepted" @if(isset($settings['emailInviteAccepted']) && $settings['emailInviteAccepted'] == 0) unchecked @else checked @endif value="1"> When someone accepts my invitation</li>
	                    				<li><input type="checkbox" class="form-check-input" name="emailTripRecommendations" @if(isset($settings['emailTripRecommendations']) && $settings['emailTripRecommendations'] == 0) unchecked @else checked @endif value="1"> Trip recommendations</li>
	                    				<li><input type="checkbox" class="form-check-input" name="emailGroupRecommendations" @if(isset($settings['emailGroupRecommendations']) && $settings['emailGroupRecommendations'] == 0) unchecked @else checked  @endif value="1"> Group recommendations</li>
	                    				<li><input type="checkbox" class="form-check-input" name="emailNewFeatures" @if(isset($settings['emailNewFeatures']) && $settings['emailNewFeatures'] == 0) unchecked @else checked @endif value="1"> New feature release</li>
	                    			</ul>
	                    		</div>
	                    	</div>
	                    </div>
	                    <div class="form-group mt-4">
	                		<input type="submit" value="SAVE CHANGES" class="btn btn-success">
	                	</div>
                    </form>
               	</div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('js')

@endsection