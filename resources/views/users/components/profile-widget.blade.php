@if(isset($user->customClaims["username"]))
	<div class="card user-card" onclick="window.location.href= '/u/{{ $user->customClaims["username"] }}'" @if(!$pdf) style=" background-image: linear-gradient(rgba(0, 0, 0, 0), rgba(0, 0, 0, 0), rgba(0, 0, 0, 0),
	              rgba(0, 0, 0, 1)), url('{{ $user->photoUrl }}');"@endif>
		<div class="text-center">
			@if($pdf)
				<img src="{{ $user->photoUrl }}" style="width:100px; height:100px; border-radius:10px; display:inline-block;">
			@endif
			<h4 @if(!$pdf) class="text-white" @endif>{{ getFirstName($user->displayName) }}@if(isset($user->customClaims['dob'])){{ ", " . calculateAge($user->customClaims['dob']) }}@endif</h4>
		</div>
		@if(($user->customClaims['nav'] ?? null) === true)
			<img src="/assets/img/navigator-verified.png" style="width: 20px; position: absolute; top: 10px; right: 10px;" title="Verified"/> 
		@endif
	</div>
@endif