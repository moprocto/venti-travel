@foreach($user->customClaims['interests'] as $interest)
	<span style="font-size: 14px; border: 1px solid rgba(0,0,0,0.3); display: inline-block; text-transform:  uppercase; font-weight: bold; padding: 5px 10px; border-radius: 20px; margin: 5px; background-color:whitesmoke;">{{ interestEmoji($interest["value"] ?? $interest) }} {{ $interest["value"] ?? $interest }}</span>
@endforeach