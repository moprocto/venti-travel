<div class="accordion" id="accordionExample" style="width:100%; padding:0;">
  <div class="accordion-item" style="border-radius:20px;">
    <h2 class="accordion-header" id="headingOne" style="border-radius: 20px;">
      <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne" style="border-top-left-radius:20px; border-top-right-radius:20px;">FILTERS</button>
    </h2>
        <div id="collapseOne" class="accordion-collapse collapse show" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
            <div class="accordion-body">
                @if(Session::get('user') !== null)
                        <div class="form-group">
                        <label class="form-label">Compatibility</label>
                        <select class="form-control filter" id="compatibility" @if(!isProfileComplete(Session::get('user'))[0]) disabled @endif>
                            <option value="">Show All</option>
                            <option value=".good">Good Fit</option>
                            <option value=".workable">Possible Fit</option>
                        </select>
                        @if(!isProfileComplete(Session::get('user'))[0])
                        <span>{!! isProfileComplete(Session::get('user'))[1] !!} to use this filter</span>
                        @endif
                    </div>
                @endif
                <div class="form-group">
                    <label class="form-label">Languages</label>
                    <select class="js-example-basic-multiple form-control filter"  multiple="multiple" data-maximum-selection-length="3"  id="languages" required="">
                     <option value=".English">English</option>
                     <option value=".Spanish">Spanish</option>
                     <option value=".Mandarin">Mandarin</option>
                     <option value=".Hindi">Hindi</option>
                     <option value=".French">French</option>
                     <option value=".Arabic">Arabic</option>
                     <option value=".Russian">Russian</option>
                     <option value=".Portuguese">Portuguese</option>
                     <option value=".Bengali">Bengali</option>
                     <option value=".Indonesian">Indonesian</option>
                     <option value=".Urdu">Urdu</option>
                     <option value=".German">German</option>
                 </select>
                </div>
                <div class="form-group">
                    <label class="form-label">Interests</label>
                    <select name="interests[]" class="select2 js-example-basic-multiple-interests form-control filter" id="interests" multiple="">
                        <option value=".Hiking">🥾 Hiking</option>
                        <option value=".Camping">🏕️ Camping</option>
                        <option value=".Surfing">🌊 Surfing</option>
                        <option value=".Scuba">🤿 Scuba</option>
                        <option value=".KayakingRafting">🛶 Kayaking/Rafting</option>
                        <option value=".Beach">🏖️ Beach</option>
                        <option value=".Dancing">🕺💃 Dancing</option>
                        <option value=".Museums">🏛️ Museums</option>
                        <option value=".Tours">🗽 Tours</option>
                        <option value=".Foodie">🍱 Foodie</option>
                        <option value=".Photography">📸 Photography</option>
                        <option value=".Music">🎸 Music</option>
                        <option value=".Backpacking">🎒 Backpacking</option>
                        <option value=".Gambling">🎲 Gambling</option>
                        <option value=".Workouts">💪 Workouts</option>
                        <option value=".Drinks">🥃 Drinks</option>
                        <option value=".Smoking">💨 Smoking</option>
                        <option value=".Golfing">⛳ Golfing</option>
                        <option value=".MeetingLocals">👋 Meeting Locals</option>
                        <option value=".Shopping">🛍️ Shopping</option>
                        <option value=".Relaxing">💆‍♀️ Relaxing</option>
                        <option value=".WineTasting">🍷 Wine Tasting</option>
                        <option value=".Wildlife">🐒 Wildlife</option>
                    </select>
                </div>
                <div class="form-group">
                    <label class="form-label">Smoking</label>
                    <select class="form-control filter" id="smoking">
                        <option value="">Show All</option>
                        <option value=".i_dont_smoke-s">Non-Smokers Only</option>
                        <option value=".i_smoke_socially-s">Social Only</option>
                        <option value=".i_smoke_often-s">Loves to Smoke!</option>
                    </select>
                </div>
                <div class="form-group">
                    <label class="form-label">Drinking</label>
                    <select class="form-control filter" id="drinking">
                        <option value="">Show All</option>
                        <option value=".i_dont_drink-d">Non-Drinkers Only</option>
                        <option value=".i-drink_socially-d">Social Drinkers Only</option>
                        <option value=".i_drink_often-d">Loves to Drink!</option>
                    </select>
                </div>
                <div class="form-group">
                    <label class="form-label">Gender</label>
                    <select class="form-control filter" id="gender">
                        <option value="">Show All</option>
                        <option value=".men-g">Male</option>
                        <option value=".female-g">Female</option>
                    </select>
                </div>

                <div class="form-group">
                    <label class="form-label">Religion</label>
                    <select class="form-control filter" name="religion" id="religion">
                        <option value="">Show All</option>
                        <option value=".christian">Christian</option>
                        <option value=".catholic">Catholic</option>
                        <option value=".sikh">Sikh</option>
                        <option value=".jewish">Jewish</option>
                        <option value=".muslim">Muslim</option>
                        <option value=".hindu">Hindu</option>
                        <option value=".buddhist">Buddhist</option>
                        <option value=".spiritual">Spiritual</option>
                        <option value=".atheist">Atheist</option>
                        <option value=".agnostic">Agnostic</option>
                    </select>
                </div>
                <div class="form-group">
                    <label class="form-label">Accepting Invites</label>
                    <select class="form-control filter" id="invites">
                        <option value="">Show All</option>
                        <option value=".all-i">Yes</option>
                        <option value=".none-i">No</option>
                    </select>
                </div>
            </div>
        </div>
    </div>
</div>