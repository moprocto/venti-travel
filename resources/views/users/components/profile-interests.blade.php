@php
	$interests = [];
	if(isset($user->customClaims["interests"])){
		foreach($user->customClaims["interests"] as $interest){
			array_push($interests, $interest["value"]);
		}
	}
@endphp
<select name="interests[]" class="select2 js-example-basic-multiple-interests form-control" multiple="">
	<option value="🥾 Hiking" @if(in_array('🥾 Hiking', $interests)) selected @endif>🥾 Hiking</option>
	<option value="🏕️ Camping" @if(in_array('🏕️ Camping', $interests)) selected @endif>🏕️ Camping</option>
	<option value="🌊 Surfing" @if(in_array('🌊 Surfing', $interests)) selected @endif>🌊 Surfing</option>
	<option value="🤿 Scuba" @if(in_array('🤿 Scuba', $interests)) selected @endif>🤿 Scuba</option> 
	<option value="🛶 Kayaking/Rafting" @if(in_array('🛶 Kayaking/Rafting', $interests)) selected @endif>🛶 Kayaking/Rafting</option>
	<option value="🏖️ Beach" @if(in_array('🏖️ Beach', $interests)) selected @endif> 🏖️ Beach</option>
	<option value="🕺💃 Dancing" @if(in_array('🕺💃 Dancing', $interests)) selected @endif>🕺💃 Dancing</option>
	<option value="🏛️ Museums" @if(in_array('🏛️ Museums', $interests)) selected @endif>🏛️ Museums</option>
	<option value="🗽 Tours" @if(in_array('🗽 Tours', $interests)) selected @endif>🗽 Tours</option>
	<option value="🍱 Foodie" @if(in_array('🍱 Foodie', $interests)) selected @endif>🍱 Foodie</option>
	<option value="📸 Photography" @if(in_array('📸 Photography', $interests)) selected @endif>📸 Photography</option>
	<option value="🎸 Music" @if(in_array('🎸 Music', $interests)) selected @endif>🎸 Music</option>
	<option value="🎒 Backpacking" @if(in_array('🎒 Backpacking', $interests)) @endif>🎒 Backpacking</option>
	<option value="🎲 Gambling" @if(in_array('🎲 Gambling', $interests)) selected @endif>🎲 Gambling</option>
	<option value="💪 Workouts" @if(in_array('💪 Workouts', $interests)) selected @endif>💪 Workouts</option>
	<option value="🥃 Drinks" @if(in_array('🥃 Drinks', $interests)) selected @endif>🥃 Drinks</option>
	<option value="💨 Smoking" @if(in_array('💨 Smoking', $interests)) selected @endif>💨 Smoking</option>
	<option value="⛳ Golfing" @if(in_array('⛳ Golfing', $interests)) selected @endif>⛳ Golfing</option>
	<option value="👋 Meeting Locals" @if(in_array('👋 Meeting Locals', $interests)) selected @endif>👋 Meeting Locals</option>
	<option value="🛍️ Shopping" @if(in_array('🛍️ Shopping', $interests)) selected @endif>🛍️ Shopping</option>
	<option value="💆‍♀️ Relaxing" @if(in_array('💆‍♀️ Relaxing', $interests)) selected @endif>💆‍♀️ Relaxing</option>
	<option value="🍷 Wine Tasting" @if(in_array('🍷 Wine Tasting', $interests)) selected @endif>🍷 Wine Tasting</option>
	<option value="🐒 Wildlife" @if(in_array('🐒 Wildlife', $interests)) @endif>🐒 Wildlife</option>
</select>