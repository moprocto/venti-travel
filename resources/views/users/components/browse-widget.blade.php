@if(isset($user->customClaims["username"]))
	<div class="grid-sizer"></div>
	<div class="gutter-sizer"></div>
	<div class="profile grid-item {{ strtolower($user->customClaims['gender']) . '-g' }} {{ cleanString($user->customClaims['smoking']) . '-s' }} {{ cleanString($user->customClaims['drinking']) . '-d' }} {{ $user->customClaims['requests'] . '-i' }} {{ $user->compatibility }} @if(isset($user->customClaims['languages']))
			@if(is_array($user->customClaims['languages']))
				@php
					$languageList = [];
					$language = 'untouched array';
					foreach($user->customClaims['languages'] as $interest){
						if(is_array($interest)){
							array_push($languageList,$interest['value']);
						}
					}

					$language = implode(',', $languageList);
					
					//$language = implode(',', $user->customClaims['languages']); 
					//$language = implode(',', $user->customClaims['languages']);
					if(is_array($language)){
						//$language = $language[0];
					}
				@endphp
			@else
				@php
					$language = json_decode($user->customClaims['languages']);
					$languageList = [];
					foreach($language as $interest){
						array_push($languageList, $interest['value']);
					}
					$language = implode(',', $languageList);
				@endphp
			@endif
			{{ str_replace(',',' ', $language) }}
		@endif 

		@if(isset($user->customClaims['interests']))
			@if(is_array($user->customClaims['interests']))
				@php
					$interestsList = [];
					$interests = 'untouched array';
					foreach($user->customClaims['interests'] as $interest){
						if(is_array($interest)){
							array_push($interestsList,$interest['value']);
						}
					}

					$interests = implode(',', $interestsList);
					
					//$interests = implode(',', $user->customClaims['interests']); 
					//$interests = implode(',', $user->customClaims['interests']);
					if(is_array($interests)){
						//$interests = $interests[0];
					}
				@endphp
			@else
				@php
					$interests = json_decode($user->customClaims['interests']);
					$interestsList = [];
					foreach($interests as $interest){
						array_push($interestsList, $interest['value']);
					}
					$interests = implode(',', $interestsList);
				@endphp
			@endif
			{{ $interests }}
		@endif 

		@if(isset($user->customClaims['religion']))
			{{ cleanString($user->customClaims['religion']) }}
		@endif
		" 
		data-username='{{ $user->customClaims["username"]}}' onclick="window.location.href= '/u/{{ $user->customClaims["username"] }}'">
		<div class="card" style="
		@if(!array_key_exists("bio", $user->customClaims) || trim($user->customClaims["bio"]) == "")
			box-shadow: 0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24);
		@endif
		background-image: linear-gradient(rgba(0, 0, 0, 0), rgba(0, 0, 0, 0), rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.5)), url('{{ $user->photoUrl }}');
		@if(array_key_exists("bio", $user->customClaims) && trim($user->customClaims["bio"]) != "")
			border-bottom-left-radius:0; border-bottom-right-radius:0; 
		@endif
		@if(array_key_exists("occupation", $user->customClaims) && trim($user->customClaims["occupation"]) != "")
			border-bottom-left-radius:0; border-bottom-right-radius:0; 
		@endif
		">
		@if(($user->customClaims['nav'] ?? null) === true)
			<img src="/assets/img/navigator-verified.png" style="width: 20px; position: absolute; bottom: 15px; right: 10px;" title="Verified"/> 
		@endif
			@if(isset($user->customClaims['interests']))
				<div class="user-interests">
					@php
						$interests = $user->customClaims['interests'];
					@endphp
					@foreach($interests as $interest)
						<span class="interest-emoji" title="{{ $interest['value'] ?? $interest }}">{{ interestEmoji($interest['value'] ?? $interest) }}</span>
					@endforeach
				</div>
			@endif 
			<div class="text-center">
				<h4 class="text-white">{{ getFirstName($user->displayName) }}@if(isset($user->customClaims['dob'])){{ ", " . calculateAge($user->customClaims['dob']) }}@endif</h4>
			</div>
		</div>
		@if(array_key_exists("bio", $user->customClaims) && trim($user->customClaims["bio"]) != "")
			<p class="profile-bio">{{ substr($user->customClaims["bio"] ?? "",0,100) }}...</p>
			@else
				@if(array_key_exists("occupation", $user->customClaims) && trim($user->customClaims["occupation"]) != "")
					<p class="profile-bio">{{ substr($user->customClaims["occupation"] ?? "",0,100) }}</p>
				@endif
		@endif
	</div>
@endif