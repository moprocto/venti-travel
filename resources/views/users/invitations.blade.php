@extends('layouts.curator-master')

@section('css')

<style type="text/css">
table tr td{
    vertical-align: middle !important;
}
@media screen and (max-width:  600px){

table td {
    display: flex;
    justify-content: space-between;
    align-items: center;
  }
  table thead{
    display: none;
  }
table td::before {
    content: attr(label);
    font-weight: bold;
    min-width: 55px;
    text-align: center;
    vertical-align: middle;

  }
}
</style>
@endsection

@section("content")
   <div id="page-content">
      <div class="gradient-wrap">
         <!--  HERO -->
        <div class="black position-relative hero-section mb-4" style="margin: 2em 4em;">
            <div class="row">
               	<div class="col-md-8">
                    <h1>Invites</h1>
                    <div class="card" style="border-radius:20px;">
                        <div class="card-body">
                            @if(sizeof($invites) > 0)
                                <table class="table">
                                    <thead>
                                        <th class="text-center">Author</th>
                                        <th>Details</th>
                                        <th>Date</th>
                                        <th></th>
                                        <th></th>
                                    </thead>
                                    <tbody>
                                        @foreach($invites as $invite)
                                            <tr>
                                                <td class="text-center" label="Author">
                                                    <div>
                                                    <a href="/u/{{ $invite['author']->customClaims['username'] }}"><img src="{{ $invite['author']->photoUrl }}" style="width:50px; height: 50px; border-radius:50px;"></a>
                                                    <p>{{ getFirstName($invite['author']->displayName) }}</p>
                                                    </div>
                                                </td>
                                                @if($invite["isTravel"])
                                                <td label="Trip">
                                                    <div>
                                                    <a href="/group/{{ $invite['tripID'] }}">{{ getTripTitle($invite["title"], $invite["days"], $invite["destination"])}}</a><br>
                                                    {{ getTripLocation($invite["destination"], $invite["country"]) }}
                                                    </div>
                                                </td>
                                                <td label="Date">{{ Date('M d, Y',strtotime($invite["departure"])) }}<br>{{ $invite["daysRemaining"] }} Day(s) Away</td>
                                                @else
                                                    <td>
                                                        <div>
                                                            <a href="/group/{{ $invite['tripID'] }}">{{ $invite["title"] }}</a><br>
                                                        </div>
                                                    </td>
                                                    <td></td>
                                                @endif
                                                <td>{{ ucfirst($invite["myStatus"]) }}</td>
                                                <td class="text-right">
                                                    @switch($invite["myStatus"])
                                                        @case("invited")
                                                            <button class="btn btn-success replyInvite" data-tripID="{{$invite['tripID']}}" data-decide="accept"><i class="fa fa-check"></i></button>
                                                            <button class="btn btn-danger replyInvite" data-tripID="{{$invite['tripID']}}" data-decide="decline"><i class="fa fa-times"></i></button>
                                                            @break
                                                        @case("requested")
                                                            <button class="btn btn-dark replyInvite" data-tripID="{{$invite['tripID']}}" data-decide="cancel"><i class="fa fa-times"></i></button>
                                                            @break
                                                        @default
                                                    @endswitch
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            @else
                                <h3>No invites as this time</h3>
                            @endif
                        </div>
                    </div>
               	</div>
            </div>
            <div class="row">
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script>
$(document).ready(function() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': "{{ csrf_token() }}"
        }
    });

    $(".replyInvite").click(function(){
        var tripID = $(this).attr('data-tripID');
        var decide = $(this).attr('data-decide');
        var row = $(this).parent().parent();
        // accept invite

        $.ajax({
            url: "{{ route('group-invite-reply') }}",
            processData: true,
            dataType: 'json',
            data:{
                tripID: tripID,
                decide: decide
            },
            type: 'post',
            success: function (data) {
                row.remove();
            }
        });
    });
});
</script>
@endsection