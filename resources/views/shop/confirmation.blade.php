@extends("layouts.curator-master")

@section("css")
   <link href="/css/lightbox/lightbox.min.css" rel="stylesheet">
   <link href="/assets/css/story.css" rel="stylesheet">
   <link href="/assets/css/itinerary.css" rel="stylesheet">
   <link href="/assets/css/flickity.min.css" rel="stylesheet">
   <style type="text/css">
      @if(isset($embed) && $embed == 'true')
         #navbar, #footer{
            display: none;
         }
         #sticky-header{
            padding: 40px 40px 10px 40px;
         }
         #page-content{
            padding-top: 20px;
         }
         .slide-nav{
            padding-top: 40px;
         }
         .itinerary-menu{
            top: 60px;
         }
      @else
         #sticky-header{
            padding: 20px 40px 10px 40px;
         }
      @endif  

   </style>
   <script src="/assets/js/stories.js"></script>
@endsection

@section("content")

<div id="page-content">
    <div class="gradient-wrap">
      <!--  HERO -->
      <div class="black position-relative hero-section mb-4" style="margin: 20px 4em;">
         <div class="row" style="justify-content: center;">
            <div class="col-md-8">
               <div class="row">
                  <div class="col-md-12">
                     <div class="card-container">
                        <div class="card-header product-image" style="min-height: 300px; background-image:  url('{{ $idea['imageURL'] }}');">
                        </div>
                        <div class="card-body" style="border-bottom-left-radius:20px; border-bottom-right-radius: 20px;">
                           <h1>Success! You're going to {{ $idea['destination'] }}</h1>
                           <table class="table" style="max-width:500px;">
                              <tbody>
                                 <tr>
                                    <td><i class="fa fa-location-dot"></i> {{ $idea['destination'] }}</td>
                                    <td style="padding-left:20px;"><i class="fa fa-calendar"></i> Suggested Travel Date: <span class="month">{{ $idea["departure"] }}</span></td>
                                 </tr>
                              </tbody>
                           </table>
                           <div class="tile-footer" style="padding:10px 0;">
                                 <div style="flex-wrap: flex;">
                                 @foreach($idea['tags'] as $tag)
                                 <div class="pills-tab" style="background-color:rgba({{ strlen($tag) * 20  }}, 168, {{ strlen($tag) * 10  }}, 0.45)">{{ $tag }}</div>
                                 @endforeach
                                 </div>
                           </div>
                           <p>{{ $idea['description'] }}</p>

                           @if(!$preview)

                           <a href="/shop/receipt/{{$tripID}}/{{$userID}}/{{$stampID}}" target="_blank" class="btn btn-black" style="border:2px solid black;">View Receipt</a>
                           <br>
                           <p style="font-size:12px; margin-top:20px;">We also sent a receipt to {{ $email }}. Check your spam folder just in case!</p>

                           @else

                           <h1>This is a preview</h1>

                           @endif
                        </div>
                     </div>
                  </div>
               </div>
               @include("itinerary.story", ["itinerary" => $itinerary, "idea" => $idea])
               
               @include("itinerary.timeline", ["itinerary" => $itinerary, "idea" => $idea])
            </div>
         </div>
      </div>
    </div>
    <div id="sticky-header">
      <h2></h2>
   </div>
</div>

@endsection

@section("js")
<script src="https://js.stripe.com/v3/" id="striping1"></script>
<script src="/js/lightbox/lightbox.min.js"></script>
<script src="/assets/js/flickity.min.js"></script>
<script src="/assets/js/lazysizes.min.js"></script>
<script>
   jQuery(document).ready(function ($) {

      

      
      $(document).on('click', '[data-toggle="lightbox"]', function(event) {
          event.preventDefault();
          $(this).ekkoLightbox({
            maxWidth: 600,
            maxHeight: 500,
            alwaysShowClose: true
          });
      });

      var windowWidth = $(window).width();
      mobileEnhance(windowWidth);

      $(window).resize(function() {
         windowWidth = $(window).width();
         //mobileEnhance(windowWidth);
      });

      function mobileEnhance(windowWidth){
         if(windowWidth < 768){
            $("button.btn-block").each(function(){
               var targetID = $(this).attr("data-target").replace("#","");
               $(this).addClass("collapsed");
               $("#" + targetID).removeClass("show");
            });
         } else {
            $("button.btn-block").each(function(){
               var targetID = $(this).attr("data-target").replace("#","");
               $(this).removeClass("collapsed");
               $("#" + targetID).addClass("show");
            });
         }
      }

      $(".adjustedDate").each(function(i, val){
         var startDate = new moment("{{ $start }}");
         var endDate = new moment(startDate).add(i ,"days").format("dddd, MMMM. D, Y");
         $(this).html(endDate);
      });
      $('.main-carousel').flickity({
        // options
        autoPlay: false,
        width:'100%',
        height:'100%',
        cellAlign: 'center',
        adaptiveHeight: true
      });



      $(document).ready(function() {
         //Monitor scrolling
         $(window).scroll(function (event) {
            
            //y = current top position of window
            var y_pos = $(this).scrollTop();
            var current_title = "{{ $idea['title'] }}";

            if(y_pos > 1100){
               if($("#page-content").hasClass("show-sticky") == false || $("#page-content").hasClass("show-story") == false){
                  $("#page-content").addClass("show-sticky");
               }
               
            } else {

               $("#page-content").removeClass("show-sticky");
            }

            $('.timeline-section').each(function() {
               // if y_pos, the current top position of window, is past an element change the Title to that element's text.
                if (y_pos > $(this).offset().top) {
                   current_title = $(this).attr("data-section-title");
               }
            });

          //console.log("FOUND Furthest Down Title = [" + current_title + "]");
          $("#sticky-header h2").html(current_title);

         });

         setTimeout(function() {$("#itinerary-stories").css("opacity", "1");}, 2000);

      @if(isset($story) && $story == "true")
         $("#itinerary-stories").css("top", "0px");
         $("body").css("overflow", "hidden");
         $("#page-content").removeClass("show-sticky");
      @endif

      $("#story-start").click(function(){
         $("#itinerary-stories").css("top", "0px");
         $("body").css("overflow", "hidden");
         openFullscreen();
         $("#page-content").addClass("show-story");
         $("#page-content").removeClass("show-sticky");
      });

      $(".story-exit").click(function(){
         $("#itinerary-stories").css("top","-2000px");
         $("body").css("overflow", "auto");
         $("#page-content").removeClass("show-story");
         closeFullscreen();
      });

      $(".append-gif").each(function(){

         var gif = $(this).attr("src");
         var gallery = $(this).attr("data-gallery");

         var container = $(this).parent().parent().parent().parent().parent().parent().parent().find(".image-gallery");

         $(this).remove();

         container.html('<a href="' + gif + '" data-toggle="lightbox" data-type="image" data-gallery="' + gallery + '" data-title=""><div class="image-header order-0" style="background-image: url(\'' + gif +'\')"></div></a>')
      });



         var elem = document.documentElement;

         /* View in fullscreen */
         function openFullscreen() {
           if (elem.requestFullscreen) {
             elem.requestFullscreen();
           } else if (elem.webkitRequestFullscreen) { /* Safari */
             elem.webkitRequestFullscreen();
           } else if (elem.msRequestFullscreen) { /* IE11 */
             elem.msRequestFullscreen();
           }
         }

         /* Close fullscreen */
         function closeFullscreen() {
           if (document.exitFullscreen) {
             document.exitFullscreen();
           } else if (document.webkitExitFullscreen) { /* Safari */
             document.webkitExitFullscreen();
           } else if (document.msExitFullscreen) { /* IE11 */
             document.msExitFullscreen();
           }
         }

         document.addEventListener('lazybeforeunveil', function(e){
             var bg = e.target.getAttribute('data-bg');
             if(bg){
                 e.target.style.backgroundImage = 'url(' + bg + ')';
             }
         });
      });
   });
</script>
<script async src="//www.instagram.com/embed.js"></script>
@endsection