@extends("layouts.curator-master")

@section('css')
	<style>
		.body-rounded img{
			border-top-left-radius:20px;
			border-top-right-radius:20px;
		}
		.body-rounded{
			border-bottom-left-radius:20px;
			border-bottom-right-radius:20px;
		}
		.body-rounded .card-body-text{
			padding: 20px;
		}
	</style>
	<style type="text/css">
      .grid-item{
         cursor: pointer;
      }
      span.month{
         background: rgba(0,0,0,0.7);
         color: white;
         font-size: 12px;
         padding: 5px 10px;
         border-radius: 20px;
         float: right;
      }
      #boat{
         -webkit-animation: moveboat 10s linear infinite;
         -moz-animation: moveboat 10s linear infinite;
         -o-animation: moveboat 10s linear infinite;
      }
      .card-header{
         border-top-left-radius: 20px !important;
         border-top-right-radius: 20px !important;
      }
   }

   *{ margin: 0; padding: 0;}

@media screen and (max-width:  600px){
   .mobile-text-center{
      text-align:center  !important;
   }
  .mobile-mt{
      margin-top: 20px;
   }
   .mobile-mgng{
      margin-top: 20px;
   }
   #clouds{
      width: 100%;
      padding: 10px 0;
      position: absolute;
      top: 0;
      z-index: 4;
      padding-bottom: 0;
   }

}
@media screen and (min-width:  600px){
  .mobile-mgng{
     margin-top: -185px;
   }
}

#boat-container{
   position: absolute;
}
.pointer-label.high, .pointer-label.low{
   display: none !important;
}

#boat-container{
   -webkit-animation: waves 4s linear infinite;
   -moz-animation: waves 4s linear infinite;
   -o-animation: waves 4s linear infinite;
}
.card-row{
   padding:20px;
   background-color: white;
   border-radius: 20px;
   margin-bottom: 20px;
}
h6.elementor-heading-title{
   text-transform: uppercase;
    line-height: 1.5em;
    letter-spacing: 2px;
    color: #0057FC !important;
}
label.form-label{
   text-transform: uppercase;
   font-size: 10px;
   color: black;
   font-weight: 900;
   margin-bottom: 20px;
}
.btn-wide{
   width: 100%;
}
.card-header{
   height: 200px;
   width: 100%;
   background-size: cover;
   background-position: bottom;
   border-top-left-radius: 20px;
   border-top-right-radius: 20px;
}
.tile-footer{
   border-bottom-left-radius: 20px;
   border-bottom-right-radius: 20px;
   margin-bottom: 10px;
}
.pills-tab{
   display: inline-block;
    width: auto;
    margin: 3px 2px;
    padding-right: 10px;
    padding-left: 10px;
    border-radius: 10px;
    background-color: #ecebea;
    font-size: 13px;
    line-height: 20px;
    font-weight: 400;
    text-align: center;
    -o-object-fit: fill;
    object-fit: fill;
}
.card-body, .tile-footer{
   background: white;
   padding: 20px;
}
.grid-item{
   margin-bottom: 10px;
}
.slider-round {
    height: 10px;
}

.slider-round .noUi-connect {
    background: #c0392b;
}

.slider-round .noUi-handle {
    height: 18px;
    width: 18px;
    top: -5px;
    right: -9px; /* half the width */
    border-radius: 9px;
}
span.price{
   position: absolute;
    right: 0;
    right: 20px;
    top: 180px;
    background: #14bb98;
    color: white;
    font-size: 12px;
    padding: 8px;
    border-radius: 50%;
    text-align: center;
    min-width: 35px;

}
      </style>
@endsection

@section('content')
	<div id="page-content">
		<div class="gradient-wrap">
	  		<!--  HERO -->
	  		<div class="black position-relative hero-section mb-4" style="margin: 20px 4em;">
	     		<div class="row">
     				<div class="row" style="margin:20px 0;">
	                  	<div class="col-md-9" style="margin-bottom:20px;">
	                  		@foreach($groups as $group)
		                  		@include("store.components.destination", ["group" => $group])
	                  		@endforeach
	                  	</div>
						<div class="col-md-3" >
							<div class="card-container">
                        <div class="card-body body-rounded bg-white" style="border-radius:20px; padding:0">
                         	<img src="{{ $client['avatar'] }}" style="width:100%;">
                         	<div class="card-body-text">
                         		<h3>About</h3>
                         		<p>{{ $client["bio"] }}</p>

                         		<h5>Expertise</h5>
                         		<p>{{ $client["expertise"] }}</p>

                         		<a href="{{ $client['website'] }}" target="_blank"><i class="fa fa-globe"></i> Website</a> &nbsp; | &nbsp; <a href="{{ $client['email'] }}" target="_blank"><i class="fa fa-envelope"></i> Email</a>
                        	</div>
                        </div>
						 	</div>
						</div>
		           </div>
	     		</div>
			</div>
		</div>
	</div>
@endsection

@section('js')
<script>
$("span.month").each(function(){
            var departure = $(this).attr("data-departure")
            departure = new moment(departure).format("MMMM");
            $(this).html(departure.toUpperCase())
         });
</script>
@endsection