@extends("layouts.curator-master")

@section("css")
   
@endsection

@section("content")

<div id="page-content">
   <div class="gradient-wrap">
      <!--  HERO -->
      <div class="black position-relative hero-section mb-4" style="margin: 20px 4em;">
         <div class="row">
            <div class="col-md-8">
               <div class="card">
                  <div class="card-body">
                     <div class="row">
                        <div class="col-md-10" class="main-image">
                        </div>
                     </div>
                     @for($i = 0; $i < sizeof($product["photos"]); $i++)
                        @if($i == 0)
                           <div class="col-md-10">
                              <img src="{{ $product['photos'][$i] }}">
                           </div>
                           <div class="col-md-2">
                        @else
                           <div>
                              <img src="{{ $product['photos'][$i] }}">
                           </div>
                        @endif
                           </div>
                     @endfor
                  </div>
               </div>
            </div>
            <div class="col-md-4">
               <div class="card mb-3">
                  <div class="card-body">
                        <h1>{{ $product["image"] }}</h1>
                        <p>
                           {{ $product["description"] }}
                        </p>
                        <div class=""></div>
                  </div>
               </div>
               <div class="card mb-3 price-box">
                  <div class="card-body">
                        <h5><s>${{ $product["msrp"] }}</s></h5>
                        <h2>${{ $product["discountedPrice"] }}</h2>
                  </div>
               </div>
               <div class="card">
                  <button id="purchase" class="btn btn-primary">Purchase Item</button>
                  <ul>
                     <li>$5 will be deduced from your Cash Balance</li>
                     <li>45 points will be deducted from your Points Balance</li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

@endsection

@section("js")

@endsection