@extends("layouts.curator-master")

@section('css')
	<style type="text/css">
		.product-tag{
			background: whitesmoke;
			border-radius: 20px;
			padding: 5px 10px;
			float: right;
			font-weight: bold;
			font-size: 10px;
		}
		.image-frame{
			height: 250px;
			background-size: contain;
			width: 100%;
		}
		.color-sample{
			width: 30px;
			height: 30px;
			margin-right: 10px;
			cursor: pointer;
			border-radius: 7px;
		}
		.color-box{
			display: flex;
			flex-direction: row;
		}
		.enticer{
			display: none;
			position: absolute;
		    width: 100%;
		    height: 60%;
		    margin-left: -1em;
		    margin-top: -1em;
		    background: rgba(0,0,0,0.05);
		    justify-content: center;
		    
		    align-items: center;
		}
		.image-frame:hover .enticer{
			display: flex;
			transition: 2s;
			cursor: pointer;
		}
		
	</style>
@endsection

@section('content')
	<div id="page-content">
	    <div class="gradient-wrap">
	        <div class="container hero-section mb-4 mt-4">
	           	<div class="row">
	           		<div class="col-md-10 mt-4">
	                    <h1>Shop</h1>
	                    <p>Quality merchandise for your next big trip</p>
	                </div>
	            </div>
	            <div class="row">
	            	@foreach($products as $product)
	            		<div class="col-md-3">
	            			<div class="card bg-white">
	            				<div class="card-body">
	            					<label class="product-tag">{{ $product["msrp"] - $product["discountedPrice"] }} Points</label>
	            					<div class="image-frame" style="background-image:url('{{ $product["photos"][0] }}')" data-product-id="{{ $product['productID'] }}">
	            						<div class="enticer">
	            							<div class="product-tag">
	            								<i class="fa fa-search"></i> View Details
	            							</div>
	            						</div>
	            					</div>
	            					<div class="price-box">
	            						<s>${{ number_format($product["msrp"],2) }}</s>
	            						<h2>${{ number_format($product["discountedPrice"],2) }}</h2>
	            						<h5>{{ $product["name"] }}</h5>

	            					</div>
	            					<div class="color-box">
	            						@foreach($product["colors"] as $color)
	            							<div class="color-sample" style="background: {{ $color['hex'] }}"
	            								data-thumbnail="{{ $color['thumbnail'] }}"
	            								title="{{ $color['description'] }}"
	            								data-color="{{ $color['description'] }}"
	            							>
	            							</div>
	            						@endforeach
	            					</div>
	            				</div>
	            			</div>
	            		</div>
	            	@endforeach
	            </div>
	        </div>
	    </div>
	</div>
@endsection

@section('js')
	<script>
	$(".color-sample").click(function(){
		var thumbnail = $(this).attr("data-thumbnail");
		var color = $(this).attr("data-color");

		$(this).parent().parent().find(".image-frame").css("background-image", 'url(' + thumbnail + ')');
		$(this).parent().parent().find(".image-frame").attr("data-filter",color);
	});

	$(".image-frame").click(function(){
		var link = "/shop/product/" + $(this).attr("data-product-id");
		var filter = $(this).attr("data-filter");
		if(filter != "" && filter != undefined){
			link += "/" + filter;
		}
		return navigate(link, true);
	});

	function navigate(href, newTab) {
	   var a = document.createElement('a');
	   a.href = href;
	   if (newTab) {
	      a.setAttribute('target', '_blank');
	   }
	   a.click();
	}

	</script>
@endsection