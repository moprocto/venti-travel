@extends("layouts.curator-master")

@section("css")
   <link href="/css/jRange/jrange.css" rel="stylesheet" />
   <link href="/css/lightbox/lightbox.min.css" rel="stylesheet" />
   <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
   <link href="/assets/css/store.css" rel="stylesheet" />
@endsection

@section("content")
   <div id="page-content">
      <div class="gradient-wrap">
         <!--  HERO -->
         <div class="black position-relative hero-section mb-4" style="margin: 2em 4em;">
            <div class="row">
               <div class="col-md-3">
               </div>
               <div class="col-md-6">
                  <h2><span id="results-count">{{ sizeof($groups) }}</span> Results</h2>
               </div>
            </div>
            <div class="row">
               <div class="col-md-3">
                  <div class="row card-row">
                     <div class="col-md-12">
                        <div class="form-group">
                           <label class="form-label">Sort</label>
                           <select class="form-control" style="padding:5px 20px; height:35px;" id="sort">
                              <option>SORT</option>
                              <option value="departureASC" data-sort="ASC">RECOMMENDED START ↑</option>
                              <option value="departureDESC" data-sort="DESC">RECOMMENDED START ↓</option>
                              <option value="lengthASC" data-sort="ASC">TRIP LENGTH ↑</option>
                              <option value="lengthDESC" data-sort="DESC">TRIP LENGTH ↓</option>
                              <option value="priceASC" data-sort="ASC">ITINERARY PRICE ↑</option>
                              <option value="priceDESC" data-sort="DESC">ITINERARY PRICE ↓</option>
                           </select>
                        </div>
                        <div class="form-group">
                           <label class="form-label">Recommended Start</label>
                           <select class="form-control" id="month-select" style="padding:5px 20px; height:35px;">
                              <option name="" value="">ANY TIME</option>
                              <option name="January" value="01month">January</option>
                              <option name="February" value="02month">February</option>
                              <option name="March" value="03month">March</option>
                              <option name="April" value="04month">April</option>
                              <option name="May" value="05month">May</option>
                              <option name="June" value="06month">June</option>
                              <option name="July" value="07month">July</option>
                              <option name="August" value="08month">August</option>
                              <option name="September" value="09month">September</option>
                              <option name="October" value="10month">October</option>
                              <option name="November" value="11month">November</option>
                              <option name="December" value="12month">December</option>
                           </select>
                        </div>
                        <div class="form-group">
                           <label class="form-label">Trip Length: <span id="trip-range"></span></label>
                           <br>
                           <div id="day-slider" class="slider-round"></div>
                        </div>
                        <!--
                        <div class="form-group">
                           <label class="form-label">Destinations</label><br>
                           <ul style="list-style: none; margin:0; padding-left:0;">
                              <li><input type="checkbox" value="Africa"> Africa</li>
                              <li><input type="checkbox" value="Asia"> Asia</li>
                              <li><input type="checkbox" value="Australia / Pacific"> Australia / Pacific</li>
                              <li><input type="checkbox" value="Central America"> Central America</li>
                              <li><input type="checkbox" value="Europe"> Europe</li>
                              <li><input type="checkbox" value="North Africa / Middle East"> North Africa / Middle East</li>
                              <li><input type="checkbox" value="North America"> North America</li>
                              <li><input type="checkbox" value="South America"> South America</li>
                           </ul>
                        </div>
                        -->
                        <div class="form-group">
                           <label class="form-label">Categories</label><br>
                           <ul style="list-style: none; margin:0; padding-left:0;">
                              <li><input type="checkbox" class="categories" value=".Plenty_of_Exercise"> Plenty of Exercise</li>
                              <li><input type="checkbox" class="categories" value=".Light_Activity"> Light Activity</li>
                              <li><input type="checkbox" class="categories" value=".Nature"> Nature Adventure</li>
                              <li><input type="checkbox" class="categories" value=".Major_Savings"> Major Savings</li>
                              <li><input type="checkbox" class="categories" value=".Ecotourism"> Ecotourism</li>
                              <li><input type="checkbox" class="categories" value=".Educational/Cultural"> Educational/Cultural</li>
                              <li><input type="checkbox" class="categories" value=".Animals/Ecological"> Animals/Ecological</li>
                              <li><input type="checkbox" class="categories" value=".Foodie"> Foodie</li>
                              <li><input type="checkbox" class="categories" value=".Relaxing"> Relaxing</li>
                              <li><input type="checkbox" class="categories" value=".Water_Fun"> Water Fun</li>
                              <li><input type="checkbox" class="categories" value=".Backpacker"> Backpacker</li>
                              <li><input type="checkbox" class="categories" value=".Artsy"> Artsy </li>
                              <li><input type="checkbox" class="categories" value=".For_Couples"> For Couples</li>
                              <li><input type="checkbox" class="categories" value=".For_Friends"> For Friends</li>
                              <li><input type="checkbox" class="categories" value=".Quick_Getaway"> Quick Getaway</li>
                              <li><input type="checkbox" class="categories" value=".Multiple_Cities"> Multiple Cities</li>
                              <li><input type="checkbox" class="categories" value=".Holiday"> Holiday</li>
                              <li><input type="checkbox" class="categories" value=".Girls_Trip"> Girls Trip</li>
                              <li><input type="checkbox" class="categories" value=".Guys_Trip"> Guys Trip</li>
                           </ul>
                        </div>
                        <!--
                        <div class="form-group">
                           <label class="form-label">Trip Budget: <span id="budget-range"></span></label>
                           <br>
                           
                           <div id="budget-slider" class="slider-round"></div>
                        </div>
                     -->
                     </div>
                  </div>
                  <div class="row text-center">
                     
                     <p style="padding:20px;">Don't see an itinerary that interests you? We're happy to connect you with one of our premier travel designers.</p>
                     <a href="mailto:markus@venti.co" class="btn btn-primary btn-wide" style="border-radius: 10px;">REQUEST TRIP</a>

                  </div>
               </div>
               <div class="col-md-9" style="padding: 0 20px;">
                  <div class="row grid">
                     @foreach($groups as $group)
                        @include("store.components.destination", ["group" => $group])
                     @endforeach
                  </div>
               </div>
           </div>
         </div>
       </div>
     </div>
     </div>
    </div>
@endsection

@section("js")
<script src="/js/jRange/jrange.min.js"></script>
<script src="/js/isotope/isotope.js"></script>
<script src="/js/lightbox/lightbox.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script src="/assets/js/store.js"></script>
@endsection