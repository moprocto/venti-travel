<div class="col-md-4 grid-item {{ substr($group['departure'], 0,2) }}month {{$group['days']}}days @if(array_key_exists('tags', $group)) @foreach($group['tags'] as $tag){{ str_replace(' ','_',$tag) }} @endforeach @endif" data-price='{{ $group["purchasePrice"] ?? 0 }}' data-length='{{ $group["days"] }}' data-departure='{{ str_replace("/","",$group["departure"])}}' onclick="window.open('/shop/trip/{{ $group['tripID'] }}')">
   <div class="card-container">
      <div class="card-header" style="background-image:  url('{{ $group['imageURL'] }}');">
         <span class="month" data-departure="{{ $group['departure'] }}"></span>
      </div>
      <div class="card-body">
         <h4 style="font-weight: 700;">{{ $group['title'] ?? $group['days'] . ' Days in ' . $group["destination"] }} <span class="price">${{$group["purchasePrice"] ?? 0}}</span></h4>
         <p style="min-height: 48px; margin:0;">{{ substr ($group['description'] ?? "", 0,92) }}...</p>
      </div>
      
      <div class="tile-footer">
            <div style="flex-wrap: flex;">
               @if(array_key_exists("tags", $group))
            @foreach($group['tags'] as $tag)
            <div class="pills-tab" style="background-color:rgba({{ strlen($tag) * 20  }}, 168, {{ strlen($tag) * 10  }}, 0.45)">{{ $tag }}</div>
            @endforeach
            @endif
            </div>
      </div>
      
   </div>
</div>