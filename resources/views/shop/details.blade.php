@extends("layouts.curator-master")

@section('css')

@endsection

@section('content')
	<div id="page-content">
	    <div class="gradient-wrap">
	        <div class="container hero-section mb-4 mt-4">
	           	<div class="row">
	           		<div class="col-md-10 mt-4">
	                    <h1>Shop</h1>
	                    <p>Quality merchandise for your next big trip</p>
	                </div>
	            </div>
	        </div>
	    </div>
	</div>
@endsection

@section('js')

@endsection