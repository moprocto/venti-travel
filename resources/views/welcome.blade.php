@extends('layouts.savings.master')

@section('css')
   <style type="text/css">
      #boat-animation{
            background-size: 200vh
         }
         #boat{
            -webkit-animation: moveboat 30s linear infinite;
            -moz-animation: moveboat 30s linear infinite;
            -o-animation: moveboat 30s linear infinite;
         }
      @media(min-width: 768px ){
         .mobile-flex{
            position:absolute;
            z-index: 5;
            margin: 0 auto;
            max-width: 480px !important;
         }
         .desktop-push-up{
            margin-top: -10px;
         }
         #boat-animation{
            background-size: contain;
         }

      }
      @media(min-width: 1200px ){
         .desktop-push-up{
            padding-bottom: 50px;
         }
         .desktop-push-up{
            margin-top: -50px;
         }
         h6.accordion-title{
         font-size: 20px !important;
      }

      }

      .first.elementor-column-gap-extended{
         padding-top: 4%;
      }
      @media(min-width: 768px ){
         .order-lg-1{
            order: 1 !important;
            width: 100% !important;
         }
         .order-lg-2{
            order: 2 !important;
            width: 100% !important;
         }
      }
      @media(max-width: 768px ){
         .order-md-1{
            order: 1 !important;
            width: 100% !important;
            flex: inherit;
         }
         .order-md-2{
            order: 2 !important;
            width: 100% !important;
            flex: inherit;
         }
      }
      @media(max-width:  500px){
         
         .first.elementor-column-gap-extended{
            padding-top: 25%;
         }
         .cloud{
            opacity: 0.4;
         }
         #boat{
            -webkit-animation: moveboat 10s linear infinite;
            -moz-animation: moveboat 10s linear infinite;
            -o-animation: moveboat 10s linear infinite;
         }
      }

      /*Lets start with the cloud formation rather*/

/*The container will also serve as the SKY*/

*{ margin: 0; padding: 0;}



#clouds{
   padding: 100px 0;
   position: absolute;
   top: 0;
   z-index: 4;
}

/*Time to finalise the cloud shape*/
.cloud {
   width: 200px; height: 60px;
   background: #fff;
   
   border-radius: 200px;
   -moz-border-radius: 200px;
   -webkit-border-radius: 200px;
   
   position: relative; 
}

.cloud:before, .cloud:after {
   content: '';
   position: absolute; 
   background: #fff;
   width: 100px; height: 80px;
   position: absolute; top: -15px; left: 10px;
   
   border-radius: 100px;
   -moz-border-radius: 100px;
   -webkit-border-radius: 100px;
   
   -webkit-transform: rotate(30deg);
   transform: rotate(30deg);
   -moz-transform: rotate(30deg);
}

.cloud:after {
   width: 120px; height: 120px;
   top: -55px; left: auto; right: 15px;
}

/*Time to animate*/
.x1 {
   -webkit-animation: moveclouds 15s linear infinite;
   -moz-animation: moveclouds 15s linear infinite;
   -o-animation: moveclouds 15s linear infinite;
}

/*variable speed, opacity, and position of clouds for realistic effect*/
.x2 {
   left: 200px;
   
   -webkit-transform: scale(0.6);
   -moz-transform: scale(0.6);
   transform: scale(0.6);
   opacity: 0.6; /*opacity proportional to the size*/
   
   /*Speed will also be proportional to the size and opacity*/
   /*More the speed. Less the time in 's' = seconds*/
   -webkit-animation: moveclouds 50s linear infinite;
   -moz-animation: moveclouds 50s linear infinite;
   -o-animation: moveclouds 50s linear infinite;
}

.x3 {
   left: -250px; top: -200px;
   
   -webkit-transform: scale(0.8);
   -moz-transform: scale(0.8);
   transform: scale(0.8);
   opacity: 0.8; /*opacity proportional to the size*/
   
   -webkit-animation: moveclouds 80s linear infinite;
   -moz-animation: moveclouds 80s linear infinite;
   -o-animation: moveclouds 80s linear infinite;
}

.x4 {
   left: 470px; top: -250px;
   
   -webkit-transform: scale(0.75);
   -moz-transform: scale(0.75);
   transform: scale(0.75);
   opacity: 0.75; /*opacity proportional to the size*/
   
   -webkit-animation: moveclouds 18s linear infinite;
   -moz-animation: moveclouds 18s linear infinite;
   -o-animation: moveclouds 18s linear infinite;
}

.x5 {
   left: -150px; top: -150px;
   
   -webkit-transform: scale(0.8);
   -moz-transform: scale(0.8);
   transform: scale(0.8);
   opacity: 0.8; /*opacity proportional to the size*/
   
   -webkit-animation: moveclouds 80s linear infinite;
   -moz-animation: moveclouds 80s linear infinite;
   -o-animation: moveclouds 80s linear infinite;
}

.x6 {
   width: 1000px;
   left: -100px; top: 250px;
   position: absolute;
   
   -webkit-animation: moveplane 30s linear infinite;
   -moz-animation: moveplane 30s linear infinite;
   -o-animation: moveplane 30s linear infinite;
}
.x7 {
   width: 1000px;
   position: absolute;
   left: 100vw; top: 5.5%;
   
   -webkit-animation: moveplane-r 20s linear infinite;
   -moz-animation: moveplane-r 20s linear infinite;
   -o-animation: moveplane-r 20s linear infinite;
}

#boat-animation{
   background-repeat-x: inherit;
   background-repeat-y: no-repeat;
   position: absolute;
   width: 100vw;
   height: 35px;
   margin-left: 0;
   left: 0;
   bottom: 0;
   background-image: url(/assets/savings/img/water.png);"
}
#boat-container{
   position: absolute;
}


#boat-container{
   -webkit-animation: waves 4s linear infinite;
   -moz-animation: waves 4s linear infinite;
   -o-animation: waves 4s linear infinite;
}


@-webkit-keyframes moveplane {
   0% {margin-left: 0;}
   100% {margin-left: 200vw;}
}
@-webkit-keyframes moveplane-r {
   0% {margin-left: 0;}
   100% {margin-left: -130vw;}
}
@-webkit-keyframes moveboat {
   0% {margin-left: 0;}
   100% {margin-left: 100vw;}
}

@-webkit-keyframes waves {
   0% {margin-top: -5px;}
   50% {margin-top: -13px;}
   100% {margin-top: -5px;}
}


@-webkit-keyframes moveclouds {
   0% {margin-left: 100%;}
   100% {margin-left: -1000px;}
}
@-moz-keyframes moveclouds {
   0% {margin-left: 100%;}
   100% {margin-left: -1000px;}
}
@-o-keyframes moveclouds {
   0% {margin-left: 100%;}
   100% {margin-left: -1000px;}
}

@-webkit-keyframes moveclouds-r {
   0% {margin-left: 0px;}
   100% {margin-left: 1000px;}
}
@-moz-keyframes moveclouds-r {
   0% {margin-left: 0px;}
   100% {margin-left: 1000px;}
}
@-o-keyframes moveclouds-r {
   0% {margin-left: 0px;}
   100% {margin-left: 1000px;}
}
h6.elementor-heading-title{
   text-transform: uppercase;
    line-height: 1.5em;
    letter-spacing: 2px;
    color: #0057FC !important;
}

</style>
@endsection

@section('content')
<div id="clouds">
   <div class="cloud x1"></div>
   <div class="cloud x2"></div>
   <div class="cloud x3"></div>
   <div class="cloud x4"></div>
   <div class="cloud x5"></div>
   <div class="plane-1 x6">
      <img src="/assets/savings/img/plane-1.png" style="width:75px;" loading="lazy">
   </div>
   <div class="plane-2 x7">
      <img src="/assets/savings/img/plane-2.png" style="width:75px;" loading="lazy">
   </div>
</div>
<div class="row">
   <div id="primary" class="content-area">
      <article id="post-10507" class="post-10507 page type-page status-publish hentry post-no-thumbnail">
         <div class="entry-content">
            <div data-elementor-type="wp-page" data-elementor-id="10507" class="elementor elementor-10507" data-elementor-settings="[]">
               <div class="elementor-section-wrap">
                  <div class=" elementor elementor-4404 post-4404 page type-page status-publish hentry post-no-thumbnail">
                     <section style="background-color:#f9f9f9;" class="elementor-section elementor-top-section elementor-element elementor-element-63e82cd elementor-section-stretched elementor-section-full_width elementor-section-height-full fit-screen background-2 elementor-section-gap-beside-yes elementor-section-height-default elementor-section-items-middle elementor-section-column-vertical-align-stretch" data-id="63e82cd" data-element_type="section" data-settings="{&quot;stretch_section&quot;:&quot;section-stretched&quot;,&quot;background_background&quot;:&quot;classic&quot;,&quot;shape_divider_bottom&quot;:&quot;curve&quot;}">
                        <div class="elementor-shape elementor-shape-bottom" data-negative="false">
                      </div>
                        <div class="elementor-container elementor-column-gap-extended first">
                           <div class="elementor-shape elementor-shape-bottom" data-negative="false" style="bottom:0; z-index:3;">
                              <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1000 100" preserveAspectRatio="none">
                                 <path class="elementor-shape-fill" d="M1000,4.3V0H0v4.3C0.9,23.1,126.7,99.2,500,100S1000,22.7,1000,4.3z"></path>
                              </svg>

                           </div>
                           <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-a17bfd4" data-id="a17bfd4" data-element_type="column">
                              <div class="elementor-widget-wrap elementor-element-populated">
                                 <div class="elementor-element elementor-element-9ea554b elementor-widget__width-initial elementor-animated-fast elementor-widget elementor-widget-image">
                                    <div class="elementor-element elementor-element-42cb091 animated-fast elementor-invisible elementor-widget elementor-widget-heading" data-id="42cb091" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;salaFadeInUp&quot;,&quot;_animation_delay&quot;:500}" data-widget_type="heading.default">
                                       <div class="elementor-widget-container">
                                          <h6 class="heading-secondary elementor-heading-title">THE Easiest Way To Budget</h6>
                                       </div>
                                       <div class="elementor-widget-container">
                                          <h1 class="elementor-heading-title elementor-size-default">For All Your <br>Travel Adventures</h1>
                                          <br>
                                          @include('forms.waitlist')
                                       </div>
                                    </div>
                                 <div class="elementor-element elementor-element-592f96f elementor-widget__width-auto animated-fast elementor-invisible elementor-widget elementor-widget-image" data-id="592f96f" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;salaFadeInUp&quot;,&quot;_animation_delay&quot;:900}" data-widget_type="image.default">

                              <div class="elementor-widget-container desktop-push-up">
                                 <img width="440" height="128" src="/assets/savings/img/venti-hero-1.png" class="attachment-full size-full" alt="" loading="lazy" style="max-width: 700px; width: 100%;" />
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </section>

            </div>
                  <section class="elementor-section elementor-top-section elementor-element elementor-element-842ac1d elementor-section-stretched elementor-section-boxed elementor-section-gap-beside-yes elementor-section-height-default elementor-section-height-default elementor-section-column-vertical-align-stretch" data-id="842ac1d" data-element_type="section" data-settings="{&quot;stretch_section&quot;:&quot;section-stretched&quot;}">
                     <div class="elementor-container elementor-column-gap-extended">
                        <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-8f07b5d" data-id="8f07b5d" data-element_type="column">
                           <div class="elementor-widget-wrap elementor-element-populated">
                              <div class="elementor-element elementor-element-d9d54f9 elementor-invisible elementor-widget elementor-widget-sala-heading" data-id="d9d54f9" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;salaFadeInUp&quot;}" data-widget_type="sala-heading.default">
                                 <div class="elementor-widget-container">
                                    <div class="sala-modern-heading">

                                       <div class="heading-primary-wrap">
                                          <h2 class="heading-primary elementor-heading-title">How it Works</h2>
                                       </div>
                                       <div class="heading-description-wrap">
                                          <div class="heading-description">
                                             <p>Travel budgeting made so simple, even a sloth could do it</p>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              @include('components.landing.step-one')
                              
                              @include('components.landing.step-two')

                              @include('components.landing.step-three')
                           </div>
                        </div>
                     </div>
                  </section>
                  <div class="elementor-4033" style="display: none;">
                     <section class="elementor-section elementor-top-section elementor-element elementor-element-46079ce elementor-section-stretched elementor-section-boxed elementor-section-gap-beside-yes elementor-section-height-default elementor-section-height-default elementor-section-column-vertical-align-stretch" data-id="46079ce" data-element_type="section" data-settings="{&quot;stretch_section&quot;:&quot;section-stretched&quot;,&quot;background_background&quot;:&quot;classic&quot;}" style="width: 1440px; left: -135px;">
                        <div class="elementor-container elementor-column-gap-extended">
                           <div class="elementor-column elementor-col-50 elementor-top-column elementor-element elementor-element-46da98b" data-id="46da98b" data-element_type="column" data-settings="{&quot;background_background&quot;:&quot;classic&quot;,&quot;animation&quot;:&quot;none&quot;}">
                              <div class="elementor-widget-wrap elementor-element-populated" style="padding:40px;">
                                 <div class="elementor-element elementor-element-a18e27d elementor-widget elementor-widget-sala-heading" data-id="a18e27d" data-element_type="widget" data-widget_type="sala-heading.default">
                                    <div class="elementor-widget-container">
                                       <div class="sala-modern-heading">
                                          <div class="heading-primary-wrap">
                                             <h2 class="heading-primary elementor-heading-title">It <mark>Gets Better</mark></h2>
                                          </div>
                                          <div class="heading-description-wrap">
                                             <div class="heading-description">
                                                <p>For each budgeting milestone you accomplish, you'll receive reward points that you can apply towards discounted travel packages, airfare, or cashback. Earn additional points when friends and family you invite to venti achieve their savings goals. It's a win-win!</p>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="elementor-column elementor-col-50 elementor-top-column elementor-element elementor-element-46da98b" data-id="46da98b" data-element_type="column"  >
                              <div class="elementor-widget-container">
                                 <img width="800" height="834" src="/assets/savings/img/friends.png" class="attachment-full size-full mobile-flex" alt="" loading="lazy"  >                                            
                              </div>
                           </div>
                        </div>
                     </section>
                  </div>
                  <div class="elementor-12493">
                     <section class="elementor-section elementor-top-section elementor-element elementor-element-d669db7 elementor-section-boxed elementor-section-gap-beside-yes elementor-section-height-default elementor-section-height-default elementor-section-column-vertical-align-stretch" data-id="d669db7" data-element_type="section">
                        <div class="elementor-container elementor-column-gap-extended">
                           <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-d4f981f" data-id="d4f981f" data-element_type="column">
                              <div class="elementor-widget-wrap elementor-element-populated">
                                 <div class="elementor-element elementor-element-efbdbc0 elementor-invisible elementor-widget elementor-widget-sala-heading" data-id="efbdbc0" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;salaFadeInUp&quot;,&quot;_animation_delay&quot;:500}" data-widget_type="sala-heading.default">
                                    <div class="elementor-widget-container">
                                       <div class="sala-modern-heading">
                                          <div class="heading-secondary-wrap" style="margin-top:40px;">
                                             <h6 class="heading-secondary elementor-heading-title">Not Just a Budgeting App</h6>
                                          </div>
                                          <div class="heading-primary-wrap">
                                             <h2 class="heading-primary elementor-heading-title">All the Necessities From Day One</h2>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <section class="elementor-section elementor-inner-section elementor-element elementor-element-b9d2426 elementor-section-boxed elementor-section-gap-beside-yes elementor-section-height-default elementor-section-height-default elementor-section-column-vertical-align-stretch" data-id="b9d2426" data-element_type="section">
                                    <div class="elementor-container elementor-column-gap-extended">
                                       <div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-eb575e0" data-id="eb575e0" data-element_type="column">
                                          <div class="elementor-widget-wrap elementor-element-populated">
                                             <div class="elementor-element elementor-element-7bc9c0e sala-view-stacked sala-shape-square elementor-position-left elementor-vertical-align-top elementor-widget elementor-widget-sala-icon-box" data-id="7bc9c0e" data-element_type="widget" data-widget_type="sala-icon-box.default">
                                                <div class="elementor-widget-container">
                                                   <div class="sala-icon-box sala-box">
                                                      <div class="icon-box-wrapper">
                                                         <div class="sala-icon-wrap">
                                                            <div class="sala-icon-view">
                                                               <div class="sala-icon icon sala-solid-icon">
                                                                  <i aria-hidden="true" class="fal fa-piggy-bank"></i>            
                                                               </div>
                                                            </div>
                                                         </div>
                                                         <div class="icon-box-content">
                                                            <div class="heading-wrap">
                                                               <h4 class="heading">Smart Budgeting</h4>
                                                            </div>
                                                            <div class="description-wrap">
                                                               <div class="description">
                                                                  Financial peace of mind for all your trips
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="elementor-element elementor-element-5cd3ce2 sala-view-stacked sala-shape-square elementor-position-left elementor-vertical-align-top elementor-widget elementor-widget-sala-icon-box" data-id="5cd3ce2" data-element_type="widget" data-widget_type="sala-icon-box.default">
                                                <div class="elementor-widget-container">
                                                   <div class="sala-icon-box sala-box">
                                                      <div class="icon-box-wrapper">
                                                         <div class="sala-icon-wrap">
                                                            <div class="sala-icon-view">
                                                               <div class="sala-icon icon sala-solid-icon">
                                                                  <i aria-hidden="true" class="fal fa-lightbulb-on"></i>          
                                                               </div>
                                                            </div>
                                                         </div>
                                                         <div class="icon-box-content">
                                                            <div class="heading-wrap">
                                                               <h4 class="heading">Vision Board</h4>
                                                            </div>
                                                            <div class="description-wrap">
                                                               <div class="description">
                                                                  Save, organize, and share your travel groups
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             
                                             <div class="elementor-element elementor-element-8971ed1 sala-view-stacked sala-shape-square elementor-position-left elementor-vertical-align-top elementor-widget elementor-widget-sala-icon-box" data-id="8971ed1" data-element_type="widget" data-widget_type="sala-icon-box.default">
                                                <div class="elementor-widget-container">
                                                   <div class="sala-icon-box sala-box">
                                                      <div class="icon-box-wrapper">
                                                         <div class="sala-icon-wrap">
                                                            <div class="sala-icon-view">
                                                               <div class="sala-icon icon sala-solid-icon">
                                                                  <i aria-hidden="true" class="fal fa-cloud"></i>            
                                                               </div>
                                                            </div>
                                                         </div>
                                                         <div class="icon-box-content">
                                                            <div class="heading-wrap">
                                                               <h4 class="heading">Weather Forecast</h4>
                                                            </div>
                                                            <div class="description-wrap">
                                                               <div class="description">
                                                                  So you never get blindsided by rain again.
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-95bf984" data-id="95bf984" data-element_type="column">
                                          <div class="elementor-widget-wrap elementor-element-populated">
                                             <div class="elementor-element elementor-element-7625105 elementor-widget elementor-widget-image" data-id="7625105" data-element_type="widget" data-widget_type="image.default">
                                                <div class="elementor-widget-container">
                                                   <img width="500" height="500" src="/assets/savings/img/oval-100.png" class="attachment-full size-full" alt="" loading="lazy"/>                         
                                                </div>
                                             </div>
                                             <div class="elementor-element elementor-element-eca2815 elementor-widget__width-initial elementor-absolute animated-fast elementor-widget-tablet__width-initial elementor-widget-mobile__width-initial elementor-invisible elementor-widget elementor-widget-image" data-id="eca2815" data-element_type="widget" data-settings="{&quot;_position&quot;:&quot;absolute&quot;,&quot;_animation&quot;:&quot;salaFadeInLeft&quot;,&quot;_animation_delay&quot;:300}" data-widget_type="image.default">
                                                <div class="elementor-widget-container">
                                                   <img width="450" height="1000" src="/assets/savings/img/vision-board.png" class="attachment-full size-full" alt="" loading="lazy" />                                           
                                                </div>
                                             </div>
                                             <div class="elementor-element elementor-element-3841fbd elementor-widget__width-initial elementor-absolute elementor-hidden-tablet elementor-hidden-phone elementor-widget elementor-widget-image" data-id="3841fbd" data-element_type="widget" data-settings="{&quot;_position&quot;:&quot;absolute&quot;}" data-widget_type="image.default">
                                                <div class="elementor-widget-container">
                                                   <img width="184" height="336" src="/assets/savings/img/marginalia-452@2x.png" class="attachment-full size-full" alt="" loading="lazy" />                                             
                                                </div>
                                             </div>
                                             <div class="elementor-element elementor-element-9f9632e elementor-widget__width-initial elementor-absolute animated-fast elementor-widget-tablet__width-initial elementor-widget-mobile__width-initial elementor-invisible elementor-widget elementor-widget-image" data-id="9f9632e" data-element_type="widget" data-settings="{&quot;_position&quot;:&quot;absolute&quot;,&quot;_animation&quot;:&quot;salaFadeInRight&quot;,&quot;_animation_delay&quot;:200}" data-widget_type="image.default">
                                                <div class="elementor-widget-container">
                                                   <img width="546" height="1100" src="/assets/savings/img/trip.png" class="attachment-full size-full" alt="" loading="lazy"/>                                            
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-81ba67e" data-id="81ba67e" data-element_type="column">
                                          <div class="elementor-widget-wrap elementor-element-populated">
                                             
                                             <div class="elementor-element elementor-element-c77421e sala-view-stacked sala-shape-square elementor-position-left elementor-vertical-align-top elementor-widget elementor-widget-sala-icon-box" data-id="c77421e" data-element_type="widget" data-widget_type="sala-icon-box.default">
                                                <div class="elementor-widget-container">
                                                   <div class="sala-icon-box sala-box">
                                                      <div class="icon-box-wrapper">
                                                         <div class="sala-icon-wrap">
                                                            <div class="sala-icon-view">
                                                               <div class="sala-icon icon sala-solid-icon">
                                                                  <i aria-hidden="true" class="fal fa-plane"></i>           
                                                               </div>
                                                            </div>
                                                         </div>
                                                         <div class="icon-box-content">
                                                            <div class="heading-wrap">
                                                               <h4 class="heading">Sky Scanner</h4>
                                                            </div>
                                                            <div class="description-wrap">
                                                               <div class="description">
                                                                  So you can keep track of fluctuating prices
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="elementor-element elementor-element-1755878 sala-view-stacked sala-shape-square elementor-position-left elementor-vertical-align-top elementor-widget elementor-widget-sala-icon-box" data-id="1755878" data-element_type="widget" data-widget_type="sala-icon-box.default">
                                                <div class="elementor-widget-container">
                                                   <div class="sala-icon-box sala-box">
                                                      <div class="icon-box-wrapper">
                                                         <div class="sala-icon-wrap">
                                                            <div class="sala-icon-view">
                                                               <div class="sala-icon icon sala-solid-icon">
                                                                  <i aria-hidden="true" class="fal fa-users"></i>          
                                                               </div>
                                                            </div>
                                                         </div>
                                                         <div class="icon-box-content">
                                                            <div class="heading-wrap">
                                                               <h4 class="heading">Share Trip</h4>
                                                            </div>
                                                            <div class="description-wrap">
                                                               <div class="description">
                                                                  Easily share your itinerary with friends and family 
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="elementor-element elementor-element-95c34a7 sala-view-stacked sala-shape-square elementor-position-left elementor-vertical-align-top elementor-widget elementor-widget-sala-icon-box" data-id="95c34a7" data-element_type="widget" data-widget_type="sala-icon-box.default">
                                                <div class="elementor-widget-container">
                                                   <div class="sala-icon-box sala-box">
                                                      <div class="icon-box-wrapper">
                                                         <div class="sala-icon-wrap">
                                                            <div class="sala-icon-view">
                                                               <div class="sala-icon icon sala-solid-icon">
                                                                  <i aria-hidden="true" class="fal fa-star"></i>           
                                                               </div>
                                                            </div>
                                                         </div>
                                                         <div class="icon-box-content">
                                                            <div class="heading-wrap">
                                                               <h4 class="heading">Only The Best</h4>
                                                            </div>
                                                            <div class="description-wrap">
                                                               <div class="description">We only recommend dining and activities with top ratings.</div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </section>
                              </div>
                           </div>
                        </div>
                     </section>
                  </div>
                  <section class="elementor-section elementor-top-section elementor-element elementor-element-e67753f elementor-section-stretched background-2 elementor-section-boxed elementor-section-gap-beside-yes elementor-section-height-default elementor-section-height-default elementor-section-column-vertical-align-stretch" data-id="e67753f" data-element_type="section" data-settings="{&quot;stretch_section&quot;:&quot;section-stretched&quot;,&quot;background_background&quot;:&quot;classic&quot;,&quot;shape_divider_top&quot;:&quot;waves&quot;,&quot;shape_divider_bottom&quot;:&quot;waves&quot;}" style="background-color: #F8F9FC;">
                     <div class="elementor-shape elementor-shape-top" data-negative="false">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1000 100" preserveAspectRatio="none">
                           <path class="elementor-shape-fill" d="M421.9,6.5c22.6-2.5,51.5,0.4,75.5,5.3c23.6,4.9,70.9,23.5,100.5,35.7c75.8,32.2,133.7,44.5,192.6,49.7
                              c23.6,2.1,48.7,3.5,103.4-2.5c54.7-6,106.2-25.6,106.2-25.6V0H0v30.3c0,0,72,32.6,158.4,30.5c39.2-0.7,92.8-6.7,134-22.4
                              c21.2-8.1,52.2-18.2,79.7-24.2C399.3,7.9,411.6,7.5,421.9,6.5z"/>
                        </svg>
                     </div>
                     <div class="elementor-shape elementor-shape-bottom" data-negative="false">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1000 100" preserveAspectRatio="none">
                           <path class="elementor-shape-fill" d="M421.9,6.5c22.6-2.5,51.5,0.4,75.5,5.3c23.6,4.9,70.9,23.5,100.5,35.7c75.8,32.2,133.7,44.5,192.6,49.7
                              c23.6,2.1,48.7,3.5,103.4-2.5c54.7-6,106.2-25.6,106.2-25.6V0H0v30.3c0,0,72,32.6,158.4,30.5c39.2-0.7,92.8-6.7,134-22.4
                              c21.2-8.1,52.2-18.2,79.7-24.2C399.3,7.9,411.6,7.5,421.9,6.5z"/>
                        </svg>
                     </div>
                     <div class="elementor-container elementor-column-gap-extended">
                        <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-8f07b5d" data-id="8f07b5d" data-element_type="column">
                           <div class="elementor-widget-wrap elementor-element-populated">
                              <div class="elementor-element elementor-element-d9d54f9 elementor-invisible elementor-widget elementor-widget-sala-heading" data-id="d9d54f9" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;salaFadeInUp&quot;}" data-widget_type="sala-heading.default">
                                 <div class="elementor-widget-container" style="margin: 100px 0 50px 0;">
                                    <div class="sala-modern-heading">
                                       <div class="heading-secondary-wrap" style="margin-top:40px;">
                                          <h6 class="heading-secondary elementor-heading-title" style="text-decoration: line-through;">Pricing</h6>
                                       </div>
                                       <div class="heading-primary-wrap">
                                          <h2 class="heading-primary elementor-heading-title" style="">Venti is Free</h2>
                                       </div>
                                       <div class="heading-description-wrap" style="padding-bottom:80px;">
                                          <div class="heading-description">
                                             <p>Because what matters most is saving you money</p>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="elementor-element elementor-element-415be3b sala-pricing-style-template sala-pricing-style-template elementor-invisible elementor-widget elementor-widget-sala-content-toggle" data-id="415be3b" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;salaFadeInUp&quot;}" data-widget_type="sala-content-toggle.default" style="display:none;">
                                 <div class="elementor-widget-container">
                                    <div class="sala-pricing-plan">
                                       <div class="inner">
                                          <div class="sala-pricing-plan-main">
                                             <div class="pricing-plan-item pricing-plan-primary active">
                                                <div class="primary-template">
                                                   <div data-elementor-type="page" data-elementor-id="4267" class="elementor elementor-4267" data-elementor-settings="[]">
                                                      <div class="elementor-section-wrap">
                                                         <section class="elementor-section elementor-top-section elementor-element elementor-element-260226f elementor-section-boxed elementor-section-gap-beside-yes elementor-section-height-default elementor-section-height-default elementor-section-column-vertical-align-stretch" data-id="260226f" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                                                            <div class="elementor-container elementor-column-gap-extended">
                                                               <div class="elementor-column elementor-col-33 elementor-top-column elementor-element elementor-element-c691093" data-id="c691093" data-element_type="column">
                                                                  <div class="elementor-widget-wrap elementor-element-populated">
                                                                     <div class="elementor-element elementor-element-51650b6 background-1 button-1 sala-pricing-style-01 elementor-widget elementor-widget-sala-pricing-table" data-id="51650b6" data-element_type="widget" data-widget_type="sala-pricing-table.default">
                                                                        <div class="elementor-widget-container">
                                                                           <div class="sala-pricing">
                                                                              <div class="inner">
                                                                                 <div class="sala-image image">
                                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="62" height="64" viewBox="0 0 62 64">
                                                                                       <g fill="none" fill-rule="evenodd">
                                                                                          <g>
                                                                                             <g>
                                                                                                <g>
                                                                                                   <g>
                                                                                                      <g transform="translate(-535.000000, -1008.000000) translate(0.000000, 900.000000) translate(135.000000, 0.000000) translate(400.000000, 108.000000) translate(0.000000, 0.000000)">
                                                                                                         <path fill="#FF1F25" fill-rule="nonzero" d="M62 10.706c0 5.913-4.73 10.706-10.567 10.706H18.028c-5.836 0-10.567-4.793-10.567-10.706C7.461 4.793 12.192 0 18.028 0h33.405C57.27 0 62 4.793 62 10.706z"/>
                                                                                                         <rect width="54.539" height="21.412" x="7.461" y="35.227" fill="#FF1F25" fill-rule="nonzero" rx="10.706" transform="translate(34.730623, 45.933484) rotate(-180.000000) translate(-34.730623, -45.933484)"/>
                                                                                                         <rect width="50.708" height="17.557" x="1" y="10.216" stroke="#111" stroke-width="2" rx="8.779" transform="translate(26.353898, 18.994105) rotate(-180.000000) translate(-26.353898, -18.994105)"/>
                                                                                                         <rect width="50.708" height="17.557" x="1" y="45.443" stroke="#111" stroke-width="2" rx="8.779" transform="translate(26.353898, 54.221490) rotate(-180.000000) translate(-26.353898, -54.221490)"/>
                                                                                                      </g>
                                                                                                   </g>
                                                                                                </g>
                                                                                             </g>
                                                                                          </g>
                                                                                       </g>
                                                                                    </svg>
                                                                                 </div>
                                                                                 <div class="sala-pricing-header">
                                                                                    <div class="heading-wrap">
                                                                                       <h3 class="title">Backpacker</h3>
                                                                                    </div>
                                                                                 </div>
                                                                                 <div class="price-wrap">
                                                                                    <div class="price-wrap-inner">
                                                                                       <div class="sala-pricing-price">FREE</div>
                                                                                    </div>
                                                                                    <div class="pricing-description">
                                                                                    </div>
                                                                                 </div>
                                                                                 <div class="sala-pricing-body">
                                                                                    <ul class="sala-pricing-features">
                                                                                       <li class="item elementor-repeater-item-fc4a498">
                                                                                          <div class="sala-icon icon">
                                                                                             <i aria-hidden="true" class="far fa-check"></i>                            
                                                                                          </div>
                                                                                          2 automated deposits / month               
                                                                                       </li>
                                                                                       
                                                                                    </ul>
                                                                                 </div>
                                                                              </div>
                                                                           </div>
                                                                        </div>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                               <div class="elementor-column elementor-col-33 elementor-top-column elementor-element elementor-element-acf8f27" data-id="acf8f27" data-element_type="column">
                                                                  <div class="elementor-widget-wrap elementor-element-populated">
                                                                     <div class="elementor-element elementor-element-acbe0aa ribbon-style-02 background-1 sala-pricing-style-01 elementor-widget elementor-widget-sala-pricing-table" data-id="acbe0aa" data-element_type="widget" data-widget_type="sala-pricing-table.default">
                                                                        <div class="elementor-widget-container">
                                                                           <div class="sala-pricing">
                                                                              <div class="inner">
                                                                                 <div class="sala-image image">
                                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="67" height="65" viewBox="0 0 67 65">
                                                                                       <g fill="none" fill-rule="evenodd">
                                                                                          <g>
                                                                                             <g>
                                                                                                <g>
                                                                                                   <g>
                                                                                                      <g>
                                                                                                         <path fill="#FFDD0F" d="M39.457 57.373l.386.002v-.009C54.35 57.014 66 45.088 66 30.428V14.306c0-.589-.475-1.067-1.062-1.067-11.964 0-22.023 8.243-24.838 19.4l.011-1.103C40.291 14.173 26.334 0 9.053 0c-.705 0-1.28.57-1.287 1.28l-.242 23.28c-.185 17.846 13.984 32.453 31.66 32.808v.006l.273-.001z" transform="translate(-934.000000, -1008.000000) translate(0.000000, 900.000000) translate(135.000000, 0.000000) translate(800.000000, 108.000000) translate(0.000000, 0.000000)"/>
                                                                                                         <path stroke="#111" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M30.742 39.092L30.484 64C13.44 63.51-.177 49.38.002 32.135l.241-23.28c.003-.2.165-.361.364-.361 16.767 0 30.31 13.75 30.135 30.598zM31.166 46.376l-.126 17.62c13.87-.491 24.963-11.945 24.963-26.001V21.873c0-.077-.062-.139-.138-.139-13.573 0-24.602 11.004-24.7 24.642z" transform="translate(-934.000000, -1008.000000) translate(0.000000, 900.000000) translate(135.000000, 0.000000) translate(800.000000, 108.000000) translate(0.000000, 0.000000)"/>
                                                                                                      </g>
                                                                                                   </g>
                                                                                                </g>
                                                                                             </g>
                                                                                          </g>
                                                                                       </g>
                                                                                    </svg>
                                                                                 </div>
                                                                                 <div class="sala-pricing-header">
                                                                                    <div class="heading-wrap">
                                                                                       <h3 class="title">Economy</h3>
                                                                                    </div>
                                                                                    <!--
                                                                                       <div class="sala-pricing-ribbon">
                                                                                          <span>Popular</span>
                                                                                       </div>
                                                                                       -->
                                                                                 </div>
                                                                                 <div class="price-wrap">
                                                                                    <div class="price-wrap-inner">
                                                                                       <div class="sala-pricing-currency">$</div>
                                                                                       <div class="sala-pricing-price">4</div>
                                                                                       <div class="sala-pricing-period">/ month</div>
                                                                                    </div>
                                                                                 </div>
                                                                                 <div class="sala-pricing-body">
                                                                                    <ul class="sala-pricing-features">
                                                                                       <li class="item elementor-repeater-item-fc4a498">
                                                                                          <div class="sala-icon icon">
                                                                                             <i aria-hidden="true" class="far fa-check"></i>                            
                                                                                          </div>
                                                                                          8 automated deposits / month                   
                                                                                       </li>
                                                                                    </ul>
                                                                                 </div>
                                                                              </div>
                                                                           </div>
                                                                        </div>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                               <div class="elementor-column elementor-col-33 elementor-top-column elementor-element elementor-element-0b3fb81" data-id="0b3fb81" data-element_type="column">
                                                                  <div class="elementor-widget-wrap elementor-element-populated">
                                                                     <div class="elementor-element elementor-element-80aa464 background-1 button-1 sala-pricing-style-01 elementor-widget elementor-widget-sala-pricing-table" data-id="80aa464" data-element_type="widget" data-widget_type="sala-pricing-table.default">
                                                                        <div class="elementor-widget-container">
                                                                           <div class="sala-pricing">
                                                                              <div class="inner">
                                                                                 <div class="sala-image image">
                                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="56" height="64" viewBox="0 0 56 64">
                                                                                       <g fill="none" fill-rule="evenodd">
                                                                                          <g>
                                                                                             <g>
                                                                                                <g>
                                                                                                   <g>
                                                                                                      <g>
                                                                                                         <path fill="#0057FC" fill-rule="nonzero" d="M7.625 1.855C7.625.831 8.46 0 9.487 0h44.651C55.167 0 56 .83 56 1.855v53.347c0 1.025-.833 1.855-1.862 1.855H46.72c-21.591 0-39.095-17.441-39.095-38.956V1.855z" transform="translate(-135.000000, -1008.000000) translate(0.000000, 900.000000) translate(135.000000, 0.000000) translate(0.000000, 108.000000) translate(0.000000, 0.000000)"/>
                                                                                                         <path stroke="#111" stroke-width="2" d="M45.513 9.798V63c-12.284-.019-23.403-4.988-31.456-13.012C5.99 41.949 1 30.844 1 18.578h0v-8.78h44.513z" transform="translate(-135.000000, -1008.000000) translate(0.000000, 900.000000) translate(135.000000, 0.000000) translate(0.000000, 108.000000) translate(0.000000, 0.000000)"/>
                                                                                                         <path stroke="#111" stroke-width="2" d="M25.686 38.067l-.507 19.667c-8.426-4.06-15.262-10.805-19.42-19.17h0l19.927-.497z" transform="translate(-135.000000, -1008.000000) translate(0.000000, 900.000000) translate(135.000000, 0.000000) translate(0.000000, 108.000000) translate(0.000000, 0.000000)"/>
                                                                                                      </g>
                                                                                                   </g>
                                                                                                </g>
                                                                                             </g>
                                                                                          </g>
                                                                                       </g>
                                                                                    </svg>
                                                                                 </div>
                                                                                 <div class="sala-pricing-header">
                                                                                    <div class="heading-wrap">
                                                                                       <h3 class="title">First Class</h3>
                                                                                    </div>
                                                                                 </div>
                                                                                 <div class="price-wrap">
                                                                                    <div class="price-wrap-inner">
                                                                                       <div class="sala-pricing-currency">$</div>
                                                                                       <div class="sala-pricing-price">9</div>
                                                                                       <div class="sala-pricing-period">/ month</div>
                                                                                    </div>
                                                                                 </div>
                                                                                 <div class="sala-pricing-body">
                                                                                    <ul class="sala-pricing-features">
                                                                                       <li class="item elementor-repeater-item-fc4a498">
                                                                                          <div class="sala-icon icon">
                                                                                             <i aria-hidden="true" class="far fa-check"></i>                            
                                                                                          </div>
                                                                                          18 automated deposits / month                      
                                                                                       </li>
                                                                                       <li class="item elementor-repeater-item-fc4a498">
                                                                                          <div class="sala-icon icon">
                                                                                             <i aria-hidden="true" class="far fa-check"></i>                            
                                                                                          </div>
                                                                                          Expense tracking                      
                                                                                       </li>
                                                                                       <li class="item elementor-repeater-item-fc4a498">
                                                                                          <div class="sala-icon icon">
                                                                                             <i aria-hidden="true" class="far fa-check"></i>                            
                                                                                          </div>
                                                                                          Pre-Paid debit card                      
                                                                                       </li>
                                                                                    </ul>
                                                                                 </div>
                                                                              </div>
                                                                           </div>
                                                                        </div>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </section>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="pricing-plan-item pricing-plan-sercondary">
                                                <div class="sercondary-template">
                                                   <div data-elementor-type="page" data-elementor-id="4285" class="elementor elementor-4285" data-elementor-settings="[]">
                                                      <div class="elementor-section-wrap">
                                                         <section class="elementor-section elementor-top-section elementor-element elementor-element-260226f elementor-section-boxed elementor-section-gap-beside-yes elementor-section-height-default elementor-section-height-default elementor-section-column-vertical-align-stretch" data-id="260226f" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                                                            <div class="elementor-container elementor-column-gap-extended">
                                                               <div class="elementor-column elementor-col-33 elementor-top-column elementor-element elementor-element-c691093" data-id="c691093" data-element_type="column">
                                                                  <div class="elementor-widget-wrap elementor-element-populated">
                                                                     <div class="elementor-element elementor-element-51650b6 background-1 button-1 sala-pricing-style-01 elementor-widget elementor-widget-sala-pricing-table" data-id="51650b6" data-element_type="widget" data-widget_type="sala-pricing-table.default">
                                                                        <div class="elementor-widget-container">
                                                                           <div class="sala-pricing">
                                                                              <div class="inner">
                                                                                 <div class="sala-image image">
                                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="62" height="64" viewBox="0 0 62 64">
                                                                                       <g fill="none" fill-rule="evenodd">
                                                                                          <g>
                                                                                             <g>
                                                                                                <g>
                                                                                                   <g>
                                                                                                      <g transform="translate(-535.000000, -1008.000000) translate(0.000000, 900.000000) translate(135.000000, 0.000000) translate(400.000000, 108.000000) translate(0.000000, 0.000000)">
                                                                                                         <path fill="#FF1F25" fill-rule="nonzero" d="M62 10.706c0 5.913-4.73 10.706-10.567 10.706H18.028c-5.836 0-10.567-4.793-10.567-10.706C7.461 4.793 12.192 0 18.028 0h33.405C57.27 0 62 4.793 62 10.706z"/>
                                                                                                         <rect width="54.539" height="21.412" x="7.461" y="35.227" fill="#FF1F25" fill-rule="nonzero" rx="10.706" transform="translate(34.730623, 45.933484) rotate(-180.000000) translate(-34.730623, -45.933484)"/>
                                                                                                         <rect width="50.708" height="17.557" x="1" y="10.216" stroke="#111" stroke-width="2" rx="8.779" transform="translate(26.353898, 18.994105) rotate(-180.000000) translate(-26.353898, -18.994105)"/>
                                                                                                         <rect width="50.708" height="17.557" x="1" y="45.443" stroke="#111" stroke-width="2" rx="8.779" transform="translate(26.353898, 54.221490) rotate(-180.000000) translate(-26.353898, -54.221490)"/>
                                                                                                      </g>
                                                                                                   </g>
                                                                                                </g>
                                                                                             </g>
                                                                                          </g>
                                                                                       </g>
                                                                                    </svg>
                                                                                 </div>
                                                                                 <div class="sala-pricing-header">
                                                                                    <div class="heading-wrap">
                                                                                       <h3 class="title">Backpacker</h3>
                                                                                    </div>
                                                                                 </div>
                                                                                 <div class="price-wrap">
                                                                                    <div class="price-wrap-inner">
                                                                                       <div class="sala-pricing-currency">$</div>
                                                                                       <div class="sala-pricing-price">0</div>
                                                                                       <div class="sala-pricing-period">/ month</div>
                                                                                    </div>
                                                                                 </div>
                                                                                 <div class="sala-pricing-body">
                                                                                    <ul class="sala-pricing-features">
                                                                                       <li class="item elementor-repeater-item-fc4a498">
                                                                                          <div class="sala-icon icon">
                                                                                             <i aria-hidden="true" class="far fa-check"></i>                            
                                                                                          </div>
                                                                                          2 users                       
                                                                                       </li>
                                                                                       <li class="item elementor-repeater-item-b5de591">
                                                                                          <div class="sala-icon icon">
                                                                                             <i aria-hidden="true" class="far fa-check"></i>                            
                                                                                          </div>
                                                                                          SaaS Metrics                        
                                                                                       </li>
                                                                                    </ul>
                                                                                 </div>
                                                                              </div>
                                                                           </div>
                                                                        </div>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                               <div class="elementor-column elementor-col-33 elementor-top-column elementor-element elementor-element-acf8f27" data-id="acf8f27" data-element_type="column">
                                                                  <div class="elementor-widget-wrap elementor-element-populated">
                                                                     <div class="elementor-element elementor-element-acbe0aa ribbon-style-02 background-1 sala-pricing-style-01 elementor-widget elementor-widget-sala-pricing-table" data-id="acbe0aa" data-element_type="widget" data-widget_type="sala-pricing-table.default">
                                                                        <div class="elementor-widget-container">
                                                                           <div class="sala-pricing">
                                                                              <div class="inner">
                                                                                 <div class="sala-image image">
                                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="67" height="65" viewBox="0 0 67 65">
                                                                                       <g fill="none" fill-rule="evenodd">
                                                                                          <g>
                                                                                             <g>
                                                                                                <g>
                                                                                                   <g>
                                                                                                      <g>
                                                                                                         <path fill="#FFDD0F" d="M39.457 57.373l.386.002v-.009C54.35 57.014 66 45.088 66 30.428V14.306c0-.589-.475-1.067-1.062-1.067-11.964 0-22.023 8.243-24.838 19.4l.011-1.103C40.291 14.173 26.334 0 9.053 0c-.705 0-1.28.57-1.287 1.28l-.242 23.28c-.185 17.846 13.984 32.453 31.66 32.808v.006l.273-.001z" transform="translate(-934.000000, -1008.000000) translate(0.000000, 900.000000) translate(135.000000, 0.000000) translate(800.000000, 108.000000) translate(0.000000, 0.000000)"/>
                                                                                                         <path stroke="#111" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M30.742 39.092L30.484 64C13.44 63.51-.177 49.38.002 32.135l.241-23.28c.003-.2.165-.361.364-.361 16.767 0 30.31 13.75 30.135 30.598zM31.166 46.376l-.126 17.62c13.87-.491 24.963-11.945 24.963-26.001V21.873c0-.077-.062-.139-.138-.139-13.573 0-24.602 11.004-24.7 24.642z" transform="translate(-934.000000, -1008.000000) translate(0.000000, 900.000000) translate(135.000000, 0.000000) translate(800.000000, 108.000000) translate(0.000000, 0.000000)"/>
                                                                                                      </g>
                                                                                                   </g>
                                                                                                </g>
                                                                                             </g>
                                                                                          </g>
                                                                                       </g>
                                                                                    </svg>
                                                                                 </div>
                                                                                 <div class="sala-pricing-header">
                                                                                    <div class="heading-wrap">
                                                                                       <h3 class="title">Basic</h3>
                                                                                    </div>
                                                                                    <!--
                                                                                       <div class="sala-pricing-ribbon">
                                                                                          <span>Popular</span>
                                                                                       </div>
                                                                                       -->
                                                                                 </div>
                                                                                 <div class="price-wrap">
                                                                                    <div class="price-wrap-inner">
                                                                                       <div class="sala-pricing-currency">$</div>
                                                                                       <div class="sala-pricing-price">59</div>
                                                                                       <div class="sala-pricing-period">/ year</div>
                                                                                    </div>
                                                                                 </div>
                                                                                 <div class="sala-pricing-body">
                                                                                    <ul class="sala-pricing-features">
                                                                                       <li class="item elementor-repeater-item-fc4a498">
                                                                                          <div class="sala-icon icon">
                                                                                             <i aria-hidden="true" class="far fa-check"></i>                            
                                                                                          </div>
                                                                                          2 users                       
                                                                                       </li>
                                                                                       <li class="item elementor-repeater-item-b5de591">
                                                                                          <div class="sala-icon icon">
                                                                                             <i aria-hidden="true" class="far fa-check"></i>                            
                                                                                          </div>
                                                                                          SaaS Metrics                        
                                                                                       </li>
                                                                                       <li class="item elementor-repeater-item-22a672d">
                                                                                          <div class="sala-icon icon">
                                                                                             <i aria-hidden="true" class="far fa-check"></i>                            
                                                                                          </div>
                                                                                          Team collaboration                        
                                                                                       </li>
                                                                                       <li class="item elementor-repeater-item-d4d887e">
                                                                                          <div class="sala-icon icon">
                                                                                             <i aria-hidden="true" class="fal fa-times"></i>                            
                                                                                          </div>
                                                                                          Export HTML code                       
                                                                                       </li>
                                                                                       <li class="item elementor-repeater-item-46497a1">
                                                                                          <div class="sala-icon icon">
                                                                                             <i aria-hidden="true" class="fal fa-times"></i>                            
                                                                                          </div>
                                                                                          Upload Your Logo                       
                                                                                       </li>
                                                                                    </ul>
                                                                                 </div>
                                                                              </div>
                                                                           </div>
                                                                        </div>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                               <div class="elementor-column elementor-col-33 elementor-top-column elementor-element elementor-element-0b3fb81" data-id="0b3fb81" data-element_type="column">
                                                                  <div class="elementor-widget-wrap elementor-element-populated">
                                                                     <div class="elementor-element elementor-element-80aa464 background-1 button-1 sala-pricing-style-01 elementor-widget elementor-widget-sala-pricing-table" data-id="80aa464" data-element_type="widget" data-widget_type="sala-pricing-table.default">
                                                                        <div class="elementor-widget-container">
                                                                           <div class="sala-pricing">
                                                                              <div class="inner">
                                                                                 <div class="sala-image image">
                                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="56" height="64" viewBox="0 0 56 64">
                                                                                       <g fill="none" fill-rule="evenodd">
                                                                                          <g>
                                                                                             <g>
                                                                                                <g>
                                                                                                   <g>
                                                                                                      <g>
                                                                                                         <path fill="#0057FC" fill-rule="nonzero" d="M7.625 1.855C7.625.831 8.46 0 9.487 0h44.651C55.167 0 56 .83 56 1.855v53.347c0 1.025-.833 1.855-1.862 1.855H46.72c-21.591 0-39.095-17.441-39.095-38.956V1.855z" transform="translate(-135.000000, -1008.000000) translate(0.000000, 900.000000) translate(135.000000, 0.000000) translate(0.000000, 108.000000) translate(0.000000, 0.000000)"/>
                                                                                                         <path stroke="#111" stroke-width="2" d="M45.513 9.798V63c-12.284-.019-23.403-4.988-31.456-13.012C5.99 41.949 1 30.844 1 18.578h0v-8.78h44.513z" transform="translate(-135.000000, -1008.000000) translate(0.000000, 900.000000) translate(135.000000, 0.000000) translate(0.000000, 108.000000) translate(0.000000, 0.000000)"/>
                                                                                                         <path stroke="#111" stroke-width="2" d="M25.686 38.067l-.507 19.667c-8.426-4.06-15.262-10.805-19.42-19.17h0l19.927-.497z" transform="translate(-135.000000, -1008.000000) translate(0.000000, 900.000000) translate(135.000000, 0.000000) translate(0.000000, 108.000000) translate(0.000000, 0.000000)"/>
                                                                                                      </g>
                                                                                                   </g>
                                                                                                </g>
                                                                                             </g>
                                                                                          </g>
                                                                                       </g>
                                                                                    </svg>
                                                                                 </div>
                                                                                 <div class="sala-pricing-header">
                                                                                    <div class="heading-wrap">
                                                                                       <h3 class="title">Pro</h3>
                                                                                    </div>
                                                                                 </div>
                                                                                 <div class="price-wrap">
                                                                                    <div class="price-wrap-inner">
                                                                                       <div class="sala-pricing-currency">$</div>
                                                                                       <div class="sala-pricing-price">159</div>
                                                                                       <div class="sala-pricing-period">/ year</div>
                                                                                    </div>
                                                                                    <div class="pricing-description">
                                                                                       Perfect for business          
                                                                                    </div>
                                                                                 </div>
                                                                                 <div class="sala-pricing-body">
                                                                                    <ul class="sala-pricing-features">
                                                                                       <li class="item elementor-repeater-item-fc4a498">
                                                                                          <div class="sala-icon icon">
                                                                                             <i aria-hidden="true" class="far fa-check"></i>                            
                                                                                          </div>
                                                                                          2 users                       
                                                                                       </li>
                                                                                       <li class="item elementor-repeater-item-b5de591">
                                                                                          <div class="sala-icon icon">
                                                                                             <i aria-hidden="true" class="far fa-check"></i>                            
                                                                                          </div>
                                                                                          SaaS Metrics                        
                                                                                       </li>
                                                                                       <li class="item elementor-repeater-item-22a672d">
                                                                                          <div class="sala-icon icon">
                                                                                             <i aria-hidden="true" class="far fa-check"></i>                            
                                                                                          </div>
                                                                                          Team collaboration                        
                                                                                       </li>
                                                                                       <li class="item elementor-repeater-item-d4d887e">
                                                                                          <div class="sala-icon icon">
                                                                                             <i aria-hidden="true" class="far fa-check"></i>                            
                                                                                          </div>
                                                                                          Export HTML code                       
                                                                                       </li>
                                                                                       <li class="item elementor-repeater-item-46497a1">
                                                                                          <div class="sala-icon icon">
                                                                                             <i aria-hidden="true" class="far fa-check"></i>                            
                                                                                          </div>
                                                                                          Upload Your Logo                       
                                                                                       </li>
                                                                                    </ul>
                                                                                 </div>
                                                                              </div>
                                                                           </div>
                                                                        </div>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </section>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </section>
                  <section class="elementor-section elementor-inner-section elementor-element elementor-element-a661378 elementor-section-boxed elementor-section-gap-beside-yes elementor-section-height-default elementor-section-height-default elementor-section-column-vertical-align-stretch" data-id="a661378" data-element_type="section">
                     <div class="elementor-container elementor-column-gap-extended">
                     <div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-9e9d2d2" data-id="9e9d2d2" data-element_type="column">
                        <div class="elementor-widget-wrap elementor-element-populated">
                           
                           
                           <div class="elementor-element elementor-element-0cad2b6 salasvg elementor-invisible elementor-widget elementor-widget-image" data-id="0cad2b6" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;salaFadeInLeft&quot;,&quot;_animation_delay&quot;:100}" data-widget_type="image.default">
                              <div class="elementor-widget-container">
                                 <img  src="/assets/savings/img/faqs.png" class="attachment-full size-full" alt="" loading="lazy" style="width:100%;"/>                                             
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-b37e588 animated-fast elementor-invisible" data-id="b37e588" data-element_type="column" data-settings="{&quot;animation&quot;:&quot;salaFadeInRight&quot;}">
                        <div class="elementor-widget-wrap elementor-element-populated">
                           <div class="elementor-element elementor-element-de46432 elementor-widget elementor-widget-heading" data-id="de46432" data-element_type="widget" data-widget_type="heading.default">
                              <div class="elementor-widget-container">
                                 <h2 class="elementor-heading-title elementor-size-default">FAQs</h2>
                              </div>
                           </div>
                           <div class="elementor-element elementor-element-522155b sala-accordion-style-03 border-01 elementor-widget elementor-widget-sala-accordion" data-id="522155b" data-element_type="widget" data-widget_type="sala-accordion.default">
                              <div class="elementor-widget-container">
                                 <div class="sala-accordion sala-accordion-control sala-accordion-icon-right" data-id="">
                                    <div class="accordion-section elementor-repeater-item-fc079f7 expanded active">
                                       <div class="accordion-header">
                                          <div class="accordion-icon-primary">
                                             <i class="link-icon "></i>                
                                          </div>
                                          <div class="accordion-title-wrapper">
                                             <h6 class="accordion-title">Are there any fees?</h6>
                                          </div>
                                          <div class="accordion-icons">
                                             <span
                                                class="accordion-icon opened-icon"><i class="fal fa-plus"></i></span>
                                             <span
                                                class="accordion-icon closed-icon"><i class="fal fa-minus"></i></span>
                                          </div>
                                       </div>
                                       <div class="accordion-content " style="display:block;">
                                          venti is free to download and use towards organizing your next trip.
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </section>
                  <section class="elementor-section elementor-top-section elementor-element elementor-element-842ac1d elementor-section-stretched elementor-section-boxed elementor-section-gap-beside-yes elementor-section-height-default elementor-section-height-default elementor-section-column-vertical-align-stretch" data-id="842ac1d" data-element_type="section" data-settings="{&quot;stretch_section&quot;:&quot;section-stretched&quot;}">
                     <div class="elementor-container elementor-column-gap-extended">
                        <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-8f07b5d" data-id="8f07b5d" data-element_type="column">
                           <div class="elementor-widget-wrap elementor-element-populated">
                              <div class="elementor-element elementor-element-d9d54f9 elementor-invisible elementor-widget elementor-widget-sala-heading" data-id="d9d54f9" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;salaFadeInUp&quot;}" data-widget_type="sala-heading.default">
                                 <div class="elementor-widget-container" style="margin-top:50px;">
                                    <div class="sala-modern-heading">
                                       <div class="heading-secondary-wrap">
                                          <h6 class="heading-secondary elementor-heading-title">GET AHEAD OF THE CURVE</h6>
                                       </div>
                                       <div class="heading-primary-wrap">
                                          <h2 class="heading-primary elementor-heading-title">Become an Early Bird</h2>
                                          <p>Hundreds have signed up already to take their travel to the next level. You in?</p>
                                          <br>
                                       </div>
                                       <div class="heading-description-wrap">
                                          @include('forms.waitlist')
                                             <span style="font-size: 10px;">We dislike spam just as much as you. We'll only email you when it's important.</span>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>

                     </div>
                     <div id="boat-animation">
                        <div id="boat-container">
    <img src="/assets/savings/img/boat.png" style="width:100px;margin-top:-47px" id="boat">
 </div>
                        </div>
                  </section>
               </div>
            </div>
         </div>
      </article>
   </div>
</div>
@endsection
@section('js')
@endsection