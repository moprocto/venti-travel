<section id="faqs">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 offset-lg-3 col-md-8 offset-md-2 section-head text-center">
                <div class="section-head-tag">FAQs</div>
                <h2 class="section-head-title">Frequently Asked Questions</h2>
                <p class="section-head-body">It's in our DNA to be as transparent as possible</p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-8 col-md-12 faqs-container offset-lg-2">
                <div class="faq">
                    <div class="faq-head">
                        <h5 class="mb-0" data-toggle="collapse" data-target="#faqa">
                            Is membership required to purchase a travel package?
                        </h5>
                    </div>                      
                    <div class="collapse show" id="faqa">
                        <div class="faq-body">
                            <p>No, but you will pay full price when you could get the same package at a discount of up to 60 percent off. Even if you only have one major trip planned for {{ Date('Y') }} or {{ Date("Y") + 1 }}, a venti membership provides you access to deals throughout the year for smaller adventures.</p>
                        </div>
                    </div>
                </div>
                <div class="faq">
                    <div class="faq-head">
                        <h5 class="mb-0" data-toggle="collapse" data-target="#faqb">
                            Do you offer financing?
                        </h5>
                    </div>                      
                    <div class="collapse" id="faqb">
                        <div class="faq-body">
                            <p>venti does not offer credit cards or direct financing. However, we have partnered with a network of lenders to find <strong class="text-light">competitive interest rates</strong> and the lowest monthly payments possible exclusivly to venti members. Enrolling into a finance plan is not required for membership or to purchase any of our packages.</p>
                        </div>
                    </div>
                </div>
                <div class="faq">
                    <div class="faq-head">
                        <h5 class="mb-0" data-toggle="collapse" data-target="#faqc">
                            How are you able to offer such discounts?
                        </h5>
                    </div>                      
                    <div class="collapse" id="faqc">
                        <div class="faq-body">
                            <p>Empty seats and unoccupied rooms cost airlines, cruises, and resorts a lot of lost revenue. We have special partnerships and technology that enable us to best predict when vacancy risk is highest. In exchange, we offer deals to our members at prices you may not get anywhere else.</p>
                        </div>
                    </div>
                </div>
                <div class="faq">
                    <div class="faq-head">
                        <h5 class="mb-0" data-toggle="collapse" data-target="#faqe">
                            What about my airline miles and existing points?
                        </h5>
                    </div>                      
                    <div class="collapse" id="faqe">
                        <div class="faq-body">
                            <p>Our concierge will aim to maximize any discounts, miles, and points you already have available. Our packages offer incredible savings regardless of your points.</p>
                        </div>
                    </div>
                </div>
                <div class="faq">
                    <div class="faq-head">
                        <h5 class="mb-0" data-toggle="collapse" data-target="#faqd">
                            Can family and friends use my membership?
                        </h5>
                    </div>                      
                    <div class="collapse" id="faqd">
                        <div class="faq-body">
                            <p>You can use your individual membership to include the cost of having additional travelers on your trip. We expect that! However, no one but you can book the trip due to federal and state regulations. Inviting friends and family to start their own venti membership will unlock new perks for everyone!</p>
                        </div>
                    </div>
                </div>
                <!--
                <div class="faq">
                    <div class="faq-head">
                        <h5 class="mb-0" data-toggle="collapse" data-target="#faq1">
                            Can I get the funds deposited into my bank account?
                        </h5>
                    </div>                      
                    <div class="collapse show" id="faq1">
                        <div class="faq-body">
                            <p>We do not deposit the $25,000 directly into your bank account. The funds are loaded onto a debit card. The maximum amount of physical cash you can withdraw directly from your card is $500. This is primarily to help you cover expenses with vendors that do not accept card payments.</p>
                        </div>
                    </div>
                </div>
                <div class="faq">
                    <div class="faq-head">
                        <h5 class="mb-0" data-toggle="collapse" data-target="#faq2">
                            Do you offer other products?
                        </h5>
                    </div>                      
                    <div class="collapse" id="faq2">
                        <div class="faq-body">
                            <img src="/assets/img/venti-gold-cards.png" style="width:300px; margin:0 auto; display:block;">
                            <br>
                            <p>venti's Black Card program is very selective. However, our Champagne program is flexible and offers many of the same benefits but is capped at $10,000. Complete our <a href="https://apptestingco.typeform.com/to/aUCBMqOv" target="_blank">pre-qualification form</a> to see which program we will recommend for you.</p>
                        </div>
                    </div>
                </div>
                
                <div class="faq">
                    <div class="faq-head">
                        <h5 class="mb-0" data-toggle="collapse" data-target="#faq3">
                            How is this better than affirm, PayPal Credit, or even my credit card?
                        </h5>
                    </div>                      
                    <div class="collapse" id="faq3">
                        <div class="faq-body">
                            <p>Unlike our competitors, venti allows you to enjoy low interest rates, earn rewards, and we do not charge late fees if you miss a payment by a few days. With venti, your trip is automatically protected with third-party <strong class="text-light">travel insurance</strong> and you receive a host of other program benefits. Please email us if <a href="mailto:markus@venti.co">our program</a>can do more for you.</p>
                        </div>
                    </div>
                </div>
                
                <div class="faq">
                    <div class="faq-head">
                        <h5 class="mb-0" data-toggle="collapse" data-target="#faq4">
                            Is this for "any" trip?
                        </h5>
                    </div>                      
                    <div class="collapse" id="faq4">
                        <div class="faq-body">
                            <p>We strongly support the following <strong class="text-light">dream destinations</strong>, but we review each application on an individual basis:
                            <table class="table text-center text-light">
                                <tbody>
                                    <tr>
                                        <td>Brazil</td>
                                        <td>Dubai</td>
                                        <td>Japan</td>
                                    </tr>
                                    <tr>
                                        <td>Greece</td>
                                        <td>France</td>
                                        <td>Spain</td>
                                    </tr>
                                    <tr>
                                        <td>Italy</td>
                                        <td>Iceland</td>
                                        <td>Africa</td>
                                    </tr>
                                    <tr>
                                        <td>Australia</td>
                                        <td>Singapore</td>
                                        <td>U.S. Islands</td>
                                    </tr>
                                    <tr>
                                        <td>Maldives</td>
                                        <td>Bora Bora</td>
                                        <td>Monaco</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>   
                <div class="faq">
                    <div class="faq-head">
                        <h5 class="mb-0" data-toggle="collapse" data-target="#faq5">
                            What are the fees for this program?
                        </h5>
                    </div>                      
                    <div class="collapse" id="faq5">
                        <div class="faq-body">
                            <p>Interest rates are planned for 13% but may fluctuate depending on which program you receive an invitation to.</p>
                            <p>Our annual program fee starts at $249 for Champagne and $349 for Black. The annual fee is only assessed while there is an active balance on your account. No balance? No fee!</p>
                            <p>If you must convert the funds to the local currency, our exchange rate is <strong class="text-light">3% of the transaction amount</strong>.</p>
                        </div>
                    </div>
                </div>
            
                <div class="faq">
                    <div class="faq-head">
                        <h5 class="mb-0" data-toggle="collapse" data-target="#faq6">
                            Is the Flex Cash restricted in any way?
                        </h5>
                    </div>                      
                    <div class="collapse" id="faq6">
                        <div class="faq-body">
                            <p>Your flights, hotels, and transportation are already included in the package price when you book. Depending on your trip, you may have up to $10,000 in Flex Cash. The funds are available up to one week before your trip and deactivate one week after your trip. Your balance will exclude any unutilized Flex Cash. Additionally, we do not permit the use of your Flex Cash for any of the following purposes:
                            </p>
                            <ul style="color:#9e9e9e;">
                                <li>Wire Transfers, Money Orders, Money Transfers, International Remittance</li>
                                <li>Manual over-the-counter cash disbursements, or to purchase travelers cheques, foreign currency, money orders, precious metals, or savings bonds</li>
                                <li>Quasi Cash Transactions - travelers cheques, foreign currency, money orders, car and other loan repayments, financial account funding, securities brokers, or cryptocurrency transactions</li>
                                <li>MoneySend Payment Transactions</li>
                                <li>Internet Gambling Transactions - government regulated internet gambling sites and online platforms accepting the placement of wagers</li>
                                <li>Gambling Transactions - casinos, race tracks, card parlors, lottery tickets, spread betting, in-flight commerce gaming</li>
                                <li>Weapons, illicit substances, and dating/escort services</li>
                            </ul>
                        </div>
                    </div>
                </div>
                -->
                
                <div class="col-md-12 text-center">
                    <br>
                    <p>Still have questions? Email our CEO, <a href="mailto:markus@venti.co">Markus Proctor</a>, directly!</p>
                </div>      
            </div>
        </div>
    </div>
</section>