<!DOCTYPE html>
    <html lang="en">    
        <head>
            <!-- Meta Tags -->
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
            <!-- Title -->
            <title>venti: Save for Travel. Earn Rewards.</title>
            <meta name="description" content="venti is the easiest way to save up to 60% on your next big vacation."/>
            <meta property="og:url" content="https://venti.co" />
            <meta property="og:type" content="website" />
            <meta property="og:title" content="venti: Save for Travel. Earn Rewards." />
            <meta property="og:description" content="venti is the easiest way to save up to 60% on your next big vacation." />
            <meta property="og:image" content="https://venti.co/assets/img/venti-card.jpg" />
            <!-- ===============================================-->
            <!--    Favicons-->
            <!-- ===============================================-->
            <link rel="apple-touch-icon" sizes="180x180" href="/assets/img/venti-financial-logo-favicon.png">
            <link rel="icon" type="image/png" sizes="32x32" href="/assets/img/venti-financial-logo-favicon.png">
            <link rel="icon" type="image/png" sizes="16x16" href="/assets/img/venti-financial-logo-favicon.png">
            <link rel="shortcut icon" type="image/x-icon" href="/assets/img/venti-financial-logo-favicon.png">
            <meta name="msapplication-TileImage" content="/assets/img/venti-financial-logo-favicon.png">
            <meta name="theme-color" content="#ffffff">
            <meta name="facebook-domain-verification" content="gqrallzymmnyef0mhu3puxdbdjqf0q" />
            <!-- Bootstrap CSS -->
            <link rel="stylesheet" href="/assets/loan/css/bootstrap.min.css">
            <!-- Google Fonts -->
            <link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700;800;900&display=swap" rel="stylesheet">
            <!-- Material Icons -->
            <link href="https://fonts.googleapis.com/icon?family=Material+Icons+Round" rel="stylesheet">
            <!-- Main CSS -->
            <link rel="stylesheet" type="text/css" href="/assets/loan/css/main.css">
            <!-- OWL carousel css -->
            <link rel="stylesheet" type="text/css" href="/assets/loan/css/owl.carousel.min.css">
        	<link rel="stylesheet" type="text/css" href="/assets/loan/css/owl.theme.default.min.css">
            <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>

            @if(env('APP_ENV') != "local")
                <!-- Meta Pixel Code -->
                <script>
                    !function(f,b,e,v,n,t,s)
                    {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                    n.callMethod.apply(n,arguments):n.queue.push(arguments)};
                    if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
                    n.queue=[];t=b.createElement(e);t.async=!0;
                    t.src=v;s=b.getElementsByTagName(e)[0];
                    s.parentNode.insertBefore(t,s)}(window, document,'script',
                    'https://connect.facebook.net/en_US/fbevents.js');
                    fbq('init', '1169331916805166');
                    fbq('track', 'PageView');
                </script>
                <noscript><img height="1" width="1" style="display:none"
                src="https://www.facebook.com/tr?id=1169331916805166&ev=PageView&noscript=1"
                /></noscript>
                <!-- End Meta Pixel Code -->
            @endif

            @yield('css')
        </head>
    <body data-spy="scroll" data-target=".navbar" data-offset="50">
        @include('layouts.nav')

        @yield('hero')

        @yield('content')

        <!-- Start Footer -->
        <footer id="footer">
            <div class="container">
                <div class="row footer-main" style="display:none;">
                    <div class="col-lg-6 offset-lg-3 col-md-8 offset-md-2 text-center">
                        <a class="footer-brand" href="#"><img src="https://codeonec.web.app/prodco/assets/images/logo.png" alt="logo" height="20"></a>
                        <p class="footer-text mt-4">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Colonel gravity get thought fat smiling add but.</p>
                        <div class="footer-social-links mt-4">
                            <a href="#">
                                <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 24 24">
                                    <path d="M9 8h-3v4h3v12h5v-12h3.642l.358-4h-4v-1.667c0-.955.192-1.333 1.115-1.333h2.885v-5h-3.808c-3.596 0-5.192 1.583-5.192 4.615v3.385z"/>
                                </svg>
                            </a>
                            <a href="#">
                                <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 24 24">
                                    <path d="M12 2.163c3.204 0 3.584.012 4.85.07 3.252.148 4.771 1.691 4.919 4.919.058 1.265.069 1.645.069 4.849 0 3.205-.012 3.584-.069 4.849-.149 3.225-1.664 4.771-4.919 4.919-1.266.058-1.644.07-4.85.07-3.204 0-3.584-.012-4.849-.07-3.26-.149-4.771-1.699-4.919-4.92-.058-1.265-.07-1.644-.07-4.849 0-3.204.013-3.583.07-4.849.149-3.227 1.664-4.771 4.919-4.919 1.266-.057 1.645-.069 4.849-.069zm0-2.163c-3.259 0-3.667.014-4.947.072-4.358.2-6.78 2.618-6.98 6.98-.059 1.281-.073 1.689-.073 4.948 0 3.259.014 3.668.072 4.948.2 4.358 2.618 6.78 6.98 6.98 1.281.058 1.689.072 4.948.072 3.259 0 3.668-.014 4.948-.072 4.354-.2 6.782-2.618 6.979-6.98.059-1.28.073-1.689.073-4.948 0-3.259-.014-3.667-.072-4.947-.196-4.354-2.617-6.78-6.979-6.98-1.281-.059-1.69-.073-4.949-.073zm0 5.838c-3.403 0-6.162 2.759-6.162 6.162s2.759 6.163 6.162 6.163 6.162-2.759 6.162-6.163c0-3.403-2.759-6.162-6.162-6.162zm0 10.162c-2.209 0-4-1.79-4-4 0-2.209 1.791-4 4-4s4 1.791 4 4c0 2.21-1.791 4-4 4zm6.406-11.845c-.796 0-1.441.645-1.441 1.44s.645 1.44 1.441 1.44c.795 0 1.439-.645 1.439-1.44s-.644-1.44-1.439-1.44z"/>
                                </svg>
                            </a>
                            <a href="#">
                                <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 24 24">
                                    <path d="M4.98 3.5c0 1.381-1.11 2.5-2.48 2.5s-2.48-1.119-2.48-2.5c0-1.38 1.11-2.5 2.48-2.5s2.48 1.12 2.48 2.5zm.02 4.5h-5v16h5v-16zm7.982 0h-4.968v16h4.969v-8.399c0-4.67 6.029-5.052 6.029 0v8.399h4.988v-10.131c0-7.88-8.922-7.593-11.018-3.714v-2.155z"/>
                                </svg>
                            </a>
                            <a href="#">
                                <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 24 24">
                                    <path d="M24 4.557c-.883.392-1.832.656-2.828.775 1.017-.609 1.798-1.574 2.165-2.724-.951.564-2.005.974-3.127 1.195-.897-.957-2.178-1.555-3.594-1.555-3.179 0-5.515 2.966-4.797 6.045-4.091-.205-7.719-2.165-10.148-5.144-1.29 2.213-.669 5.108 1.523 6.574-.806-.026-1.566-.247-2.229-.616-.054 2.281 1.581 4.415 3.949 4.89-.693.188-1.452.232-2.224.084.626 1.956 2.444 3.379 4.6 3.419-2.07 1.623-4.678 2.348-7.29 2.04 2.179 1.397 4.768 2.212 7.548 2.212 9.142 0 14.307-7.721 13.995-14.646.962-.695 1.797-1.562 2.457-2.549z"/>
                                </svg>
                            </a>
                        </div>
                        <div class="footer-links mt-4">
                            <a href="#">About</a> ᛫ 
                            <a href="#">Terms of Use</a> ᛫
                            <a href="#">Privacy Policy</a> ᛫
                            <a href="#">Company</a> ᛫
                            <a href="#">Case Studies</a>
                        </div>
                    </div>
                    <div class="col-md-6">
                        
                    </div>
                </div>
                <div class="row footer-rights">
                   <div class="col-12 text-center">
                        Copyright © {{ Date('Y') }} Venti. All rights reserved.
                        <br><br>
                        <a href="/privacy">Privacy Policy</a>
                        <br><br>
                   </div>
                   <div class="col-12 text-left">
                        <p>The Mastercard card may be used everywhere Debit Mastercard is accepted.</p>

                        <p>Apple and the Apple logo are trademarks of Apple Inc., registered in the U.S. and other countries. App Store is a service mark of Apple Inc., registered in the U.S. and other countries.</p>

                        <p>Google Play and the Google Play logo are trademarks of Google Inc.</p>
                        <!--
                            <p>
                            All trademarks and brand names belong to their respective owners. Use of these trademarks and brand names do not represent endorsement by or association with this program. All rights reserved.</p>
                            <p>* IMPORTANT INFORMATION ABOUT PROCEDURES FOR OPENING A NEW ACCOUNT: To help the government fight the funding of terrorism and money laundering activities, federal law requires all financial institutions to obtain, verify, and record information that identifies each person who opens a venti account. What this means for you: When you open a venti account, we will ask for your name, address, date of birth, and other information that will allow us to identify you. Some of this information will be asked to pre-qualify you for our program. If accepted into our program, we may also ask to see a copy of your driver’s license or other verification documents. If we are unable to verify your identity, you will not be admitted into our program.</p>
                        -->
                   </div>
                </div>
            </div>
        </footer>
        <!-- End Footer -->

        

        <!-- JavaScript Files -->
        <script src="/assets/loan/js/jquery-3.4.1.min.js"></script>
        <script src="/assets/loan/js/popper.min.js"></script>
        <script src="/assets/loan/js/bootstrap.min.js"></script>
        <script src="/assets/loan/js/owl.carousel.min.js"></script> 
        <!-- Main JS -->
        <script src="/assets/loan/js/main.js"></script>

        @if(env('APP_ENV') != "local")
            <!-- Global site tag (gtag.js) - Google Analytics -->
            <script async src="https://www.googletagmanager.com/gtag/js?id=UA-219713767-1">
            </script>
            <script>
              window.dataLayer = window.dataLayer || [];
              function gtag(){dataLayer.push(arguments);}
              gtag('js', new Date());

              gtag('config', 'UA-219713767-1');
            </script>
            <!-- Hotjar Tracking Code for https://venti.co -->
            <script>
              (function(h,o,t,j,a,r){
                  h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
                  h._hjSettings={hjid:2651983,hjsv:6};
                  a=o.getElementsByTagName('head')[0];
                  r=o.createElement('script');r.async=1;
                  r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
                  a.appendChild(r);
              })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
            </script>
            <script>
            $("#mc_embed_signup").submit(function() {
                gtag('event', 'conversion', {'send_to': 'AW-988166781/PyJ4CNGUs5ADEP30mNcD'});
            });
            $(".application").click(function(){
            gtag('event', 'conversion', {'send_to': 'AW-988166781/PyJ4CNGUs5ADEP30mNcD'});
            });
            </script> 

        @endif

        @yield('js')
    </body>
</html>