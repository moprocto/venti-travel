<!DOCTYPE html>
<html lang="en-US">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
        <!-- Title -->
        @include("layouts.header")
        <link rel="stylesheet" href="/assets/css/bootstrap.min.css">
        @if(!isset($fontless))
          <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" integrity="sha512-KfkfwYDsLkIlwQp6LFnl8zNdLGxu9YAA1QvwINks4PhcElQSvqcyVLLD9aMhXd13uQjoXtEKNosOWaZqXgel0g==" crossorigin="anonymous" referrerpolicy="no-referrer" />
        @endif
        
        <script async defer src="https://polyfill.io/v3/polyfill.min.js?features=smoothscroll%2Cfetch%2Cdefault%2CIntersectionObserver" type="bc29ea08ed5de13fee4ff9a3-text/javascript"></script>
        <link rel="stylesheet" type="text/css" href="/assets/css/style.css">
        @yield("css")
    </head>
    <body id="page_home__show">
      <main role="main">
        @if(isset($admin) && $admin == true)
          @include('layouts.nav-admin')
        @else
          <nav class="navbar-static venti-navbar" id="navbar" style="border-top:none; margin-top:30px;">
  <div class="section-wrapper my-0">
    <div class="row">
        <div class="left-nav">
            <a href="/" class="active venti-logo" style="">
              <img class="logo" src="/assets/img/venti-trademark-logo-black-excursion.png"
                alt="venti logo" />
            </a>
        </div>
        <a href="javascript:void(0)" class="hamburger-menu right-nav dark-on-light"  aria-label="hamburger menu" id="hamburger-menu" style="margin-top:-8px">
            <span class="fa fa-bars" ></span>
        </a>
        <div data-html-src="/partials/login-nav" id="navbar-links-partial" class="right-nav right-nav-mobile">
            <ul class="hamburger-menu-list" id="hamburger-menu-list">

                
                @if(Session::get('user') == null)
                
                @else
                <li>
                    <a class="dark-on-light nav-links" data-event-name="Nav: Invites" href="{{ route('user-invitations') }}"><img src="/assets/img/mailbox.png" height="18px" class="nav-icon"><span> INVITES</span></a>
                </li>
                <li>
                    <a class="dark-on-light nav-links" data-event-name="Nav: Invites" href="{{ route('user-wishlist') }}"><img src="/assets/img/wishlist.png" height="18px" class="nav-icon"><span> WISHLIST</span></a>
                </li>
                <li class="position-relative" id="products-nav-link">
                    <a href="/u/{{ Session::get('user')->customClaims['username'] }}" class="nav-links dark-on-light userprofile" data-event-name="Nav: Products"><img src="{{ Session::get('user')->photoUrl }}" style="width:50px; height: 50px; border-radius:50px;"></a>
                    <div id="nav-products-list">
                        <div class="product-option px-3 py-3">
                          <a data-event-name="Nav: Profile" href="{{ route('user-profile') }}" rel="noopener" >EDIT PROFILE
                          </a>
                        </div>
                        <div class="product-option px-3 py-3">
                          <a data-event-name="Nav: Settings" href="{{ route('user-settings') }}" rel="noopener" >SETTINGS
                          </a>
                        </div>
                        @if((Session::get('user')->customClaims["nav"] ?? null) == true)
                            <div class="product-option px-3 py-3">
                              <a data-event-name="Nav: Feedback" href="{{ route('navigator-home')  }}" rel="noopener" target="_blank">NAVIGATOR <img src="/assets/img/navigator-verified.png" style="width:15px; margin-top:-3px;"> 
                              </a>
                            </div>
                        @else
                            <div class="product-option px-3 py-3">
                              <a data-event-name="Nav: Feedback" href="{{ route('navigator-home')  }}" rel="noopener" target="_blank">BECOME A NAVIGATOR
                              </a>
                            </div>
                        @endif
                        <div class="product-option px-3 py-3">
                          <a data-event-name="Nav: About" href="/about" rel="noopener" target="_blank">ABOUT
                          </a>
                        </div>
                        <div class="product-option px-3 py-3">
                          <a data-event-name="Nav: Logout" href="{{ route('logout') }}" rel="noopener" >LOG OUT</a>
                        </div>
                    </div>
                </li>
                @endguest
                
            </ul>
        </div>
    </div>
  </div>
</nav>
        @endif
        
        @yield("content")
      </main>
        @include('layouts.footer')
        <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
        @if(env('APP_ENV') != "local")
            <!-- Global site tag (gtag.js) - Google Analytics -->
            <script async src="https://www.googletagmanager.com/gtag/js?id=UA-219713767-1">
            </script>
            <script>
              window.dataLayer = window.dataLayer || [];
              function gtag(){dataLayer.push(arguments);}
              gtag('js', new Date());

              gtag('config', 'UA-219713767-1');
            </script>
            <!-- Google tag (gtag.js) -->
            <script async src="https://www.googletagmanager.com/gtag/js?id=G-HG76RQTB33"></script>
            <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'G-HG76RQTB33');
            </script>
            <!-- Hotjar Tracking Code for https://venti.co -->
            <script>
              (function(h,o,t,j,a,r){
                  h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
                  h._hjSettings={hjid:2651983,hjsv:6};
                  a=o.getElementsByTagName('head')[0];
                  r=o.createElement('script');r.async=1;
                  r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
                  a.appendChild(r);
              })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
            </script>
            <script>
              jQuery(document).ready(function ($) {
                $("#mc_embed_signup").submit(function() {
                    gtag('event', 'conversion', {'send_to': 'AW-988166781/PyJ4CNGUs5ADEP30mNcD'});
                });
                $(".application").click(function(){
                  gtag('event', 'conversion', {'send_to': 'AW-988166781/PyJ4CNGUs5ADEP30mNcD'});
                });
              });
            </script> 
        
        @endif
        
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" crossorigin="anonymous"></script>
        <script src="/assets/js/bootstrap.min.js"></script>
        <script>
          jQuery(document).ready(function ($) {
            $("#hamburger-menu").click(function(){
              $("body").toggleClass("mobile-nav-open");
              $(".hamburger-menu-list").toggleClass("active");
            });
          });
        </script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.min.js"></script>
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.3/moment.min.js"></script>
        <script src="/assets/js/scripts.js"></script>
        @yield("js")
    </body>
</html>
