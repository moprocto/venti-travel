<!DOCTYPE html>
<html lang="en-US" dir="ltr">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <!-- ===============================================-->
      <!--    Document Title-->
      <!-- ===============================================-->
      <title>venti: The Smartest Travel Companion</title>
      <meta property="og:url" content="https://venti.co" />
      <meta property="og:type" content="website" />
      <meta property="og:title" content="venti: The Smartest Travel Companion" />
      <meta property="og:description" content="venti is the fastest and easiest way to receive detailed travel inspirations" />
      <meta property="og:image" content="https://venti.co/assets/img/venti-banner.jpeg" />
      <!-- ===============================================-->
      <!--    Favicons-->
      <!-- ===============================================-->
      <link rel="apple-touch-icon" sizes="180x180" href="/assets/img/venti-favicon.png">
      <link rel="icon" type="image/png" sizes="32x32" href="/assets/img/venti-favicon.png">
      <link rel="icon" type="image/png" sizes="16x16" href="/assets/img/venti-favicon.png">
      <link rel="shortcut icon" type="image/x-icon" href="/assets/img/venti-favicon.png">
      <meta name="msapplication-TileImage" content="/assets/img/venti-favicon.png">
      <meta name="theme-color" content="#ffffff">
      <!-- ===============================================-->
      <!--    Stylesheets-->
      <!-- ===============================================-->
      <link href="/assets/css/theme.min.css" rel="stylesheet" />

      @yield('css')
   </head>
   <body>
      <!-- ===============================================-->
      <!--    Main Content-->
      <!-- ===============================================-->
      @yield('content')
      <!-- ===============================================-->
      <!--    End of Main Content-->
      <!-- ===============================================-->
      @if(env('APP_ENV') != "local")
      <!-- Global site tag (gtag.js) - Google Analytics -->
      <script async src="https://www.googletagmanager.com/gtag/js?id=G-Z6MV4F6SMV"></script>
      <script>
         window.dataLayer = window.dataLayer || [];
         function gtag(){dataLayer.push(arguments);}
         gtag('js', new Date());
         
         gtag('config', 'G-Z6MV4F6SMV');
      </script>
      <!-- Hotjar Tracking Code for https://venti.co -->
      <script>
          (function(h,o,t,j,a,r){
              h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
              h._hjSettings={hjid:2651983,hjsv:6};
              a=o.getElementsByTagName('head')[0];
              r=o.createElement('script');r.async=1;
              r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
              a.appendChild(r);
          })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
      </script>
      @endif
      <!-- ===============================================-->
      <!--    JavaScripts-->
      <!-- ===============================================-->
      <script src="https://technext.github.io/voyage-2/v1.0.2/vendors/@popperjs/popper.min.js"></script>
      <script src="https://technext.github.io/voyage-2/v1.0.2/vendors/bootstrap/bootstrap.min.js"></script>
      <script src="https://technext.github.io/voyage-2/v1.0.2/vendors/is/is.min.js"></script>
      <script src="https://polyfill.io/v3/polyfill.min.js?features=window.scroll"></script>
      <script src="https://technext.github.io/voyage-2/v1.0.2/vendors/fontawesome/all.min.js"></script>
      <script src="https://technext.github.io/voyage-2/v1.0.2/assets/js/theme.js"></script>
      <link href="https://fonts.googleapis.com/css2?family=Titillium+Web:wght@300;400;600;700&amp;display=swap" rel="stylesheet">
   </body>
</html>