<footer id="footer">
  <div class="row section-wrapper" style="margin: 0 auto;">
    <div class="col-lg-3 col-md-12 col-sm-12" style="margin-bottom:2em;">
      <h4 class="bold"><img src="/assets/img/venti-trademark-logo-black.png" style="width:100%; min-width:100px; max-width: 100px;"></h4>
    </div>
    <div class="col-lg-3 col-md-12 col-sm-12" style="margin-bottom:2em;">
      <h4 class="bold text">Quick Links</h4>
       <ul class="footer-lists">
        <li><a href="/privacy">Privacy</a></li>
        <li><a href="/contact">Contact Us</a></li>
      </ul>
    </div>
    <div class="col-lg-3 col-md-12 col-sm-12">
      <h4 class="bold text">Our Address</h4>
      <ul class="footer-lists">
        <li style="">Venti Financial, Inc.<br>
                  800 N King Street <br>
                  Suite 304 1552 <br>
                  Wilmington, DE 19801</li>
      </ul>
    </div>
    <div class="col-lg-3 col-md-12 col-sm-12">
      <div class="mt-2 mb-2 d-flex align-items-center">
        <a class="social-media-icons" data-event-name="Footer Social: Email" href="mailto:admin@venti.co" target="_blank" rel="noopener" aria-label="View Venti Email">
          <i class="fa fa-envelope mr-1"></i>
        </a>
        <a class="social-media-icons" data-event-name="Footer Social: Instragram" href="https://instagram.com/venti.travel" target="_blank" rel="noopener" aria-label="View Venti instagram">
          <i class="fab fa-instagram mr-1"></i>
        </a>
        <a class="social-media-icons" data-event-name="Footer Social: Facebook" target="_blank" href="https://www.facebook.com/venti.travel" rel="noopener" aria-label="View Venti facebook">
          <i class="fab fa-facebook mr-1"></i>
        </a>
        <a class="social-media-icons" data-event-name="Footer Social: Twitter" target="_blank" href="https://twitter.com/ventifinancial" aria-label="View Venti twitter" rel="noopener">
          <i class="fab fa-twitter mr-1"></i>
        </a>
      </div>
    </div>
  </div>
  <div class="section-wrapper">
    <div class="sub-footer">
      © {{ Date('Y') }} Venti Financial, Inc. All rights reserved.
      <br><br>
      <p>Venti Financial, Inc. is a financial technology company, not a bank.<br>
      Banking services provided by partner credit unions, members NCUA.</p>
    </p>
    </div>
  </div>
</footer>