<div class="owl-carousel owl-theme">
    <div class="item product">
        <div class="product-img">
            <img src="assets/loan/img/thailand-bg.jpg" bg-image="assets/loan/img/thailand-bg.jpg" class="img-fluid" alt="product">
        </div>
        <div class="product-body text-center">
            <h5>Thailand</h5>
            <P>14 Days, 13 Nights</P>
            <h5>$350/mo</h5>
        </div>
    </div>
    <div class="item product">
        <div class="product-img">
            <img src="assets/loan/img/egypt-bg.jpg" bg-image="assets/loan/img/egypt-bg.jpg" class="img-fluid" alt="product">
        </div>
        <div class="product-body text-center">
            <h5>Egypt</h5>
            <P>10 Days, 9 Nights</P>
            <h5>$275/mo</h5>
        </div>
    </div>
    <div class="item product">
        <div class="product-img">
            <img src="assets/loan/img/costa-rica-bg.jpg" bg-image="assets/loan/img/costa-rica-bg.jpg" class="img-fluid" alt="product">
        </div>
        <div class="product-body text-center">
            <h5>Costa Rica</h5>
            <P>10 Days, 9 Nights</P>
            <h5>$150/mo</h5>
        </div>
    </div>
    <div class="item product">
        <div class="product-img">
            <img src="/assets/loan/img/greece-bg-square.jpg" bg-image="assets/loan/img/greece-bg-square.jpg" class="img-fluid" alt="product">
        </div>
        <div class="product-body text-center">
            <h5>Greece</h5>
            <P>11 Days, 10 Nights</P>
            <h5>$350/mo</h5>
        </div>
    </div>
    <div class="item product">
        <div class="product-img">
            <img src="assets/loan/img/ghana-bg.jpg" bg-image="assets/loan/img/ghana-bg.jpg" class="img-fluid" alt="product">
        </div>
        <div class="product-body text-center">
            <h5>Ghana</h5>
            <P>11 Days, 10 Nights</P>
            <h5>$400/mo</h5>
        </div>
    </div>
    <div class="item product">
        <div class="product-img">
            <img src="assets/loan/img/japan-bg.jpg" bg-image="assets/loan/img/japan-bg.jpg" class="img-fluid" alt="product">
        </div>
        <div class="product-body text-center">
            <h5>Japan</h5>
            <P>9 Days, 8 Nights</P>
            <h5>$600/mo</h5>
        </div>
    </div>
    <div class="item product">
        <div class="product-img">
            <img src="assets/loan/img/maldives-bg-square.jpg" bg-image="assets/loan/img/maldives-bg-square.jpg" class="img-fluid" alt="product">
        </div>
        <div class="product-body text-center">
            <h5>Maldives</h5>
            <P>8 Days, 7 Nights</P>
            <h5>$1,100/mo</h5>
        </div>
    </div>
    <div class="item product">
        <div class="product-img" bg-image>
            <img src="assets/loan/img/spain-bg.jpg" bg-image="assets/loan/img/spain-bg.jpg" class="img-fluid" alt="product">
        </div>
        <div class="product-body text-center">
            <h5>Spain</h5>
            <P>9 Days, 8 Nights</P>
            <h5>$250/mo</h5>
        </div>
    </div>
    <div class="item product">
        <div class="product-img">
            <img src="assets/loan/img/croatia-bg.jpg" bg-image="assets/loan/img/croatia-bg.jpg" class="img-fluid" alt="product">
        </div>
        <div class="product-body text-center">
            <h5>Croatia</h5>
            <P>12 Days, 11 Nights</P>
            <h5>$250/mo</h5>
        </div>
    </div>
    <div class="item product">
        <div class="product-img">
            <img src="assets/loan/img/italy-bg.jpg" bg-image="assets/loan/img/italy-bg.jpg" class="img-fluid" alt="product">
        </div>
        <div class="product-body text-center">
            <h5>Italy</h5>
            <P>10 Days, 9 Nights</P>
            <h5>$400/mo</h5>
        </div>
    </div>
    <div class="item product">
        <div class="product-img">
            <img src="assets/loan/img/france.jpg" bg-image="assets/loan/img/france.jpg" class="img-fluid" alt="product">
        </div>
        <div class="product-body text-center">
            <h5>France</h5>
            <P>7 Days, 6 Nights</P>
            <h5>$400/mo</h5>
        </div>
    </div>
</div>