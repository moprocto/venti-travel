<!DOCTYPE html>
<html lang="en-US">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
        <!-- Title -->
        @include("layouts.enterprise.header")
        <link rel="stylesheet" href="/assets/css/bootstrap.min.css">

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" integrity="sha512-KfkfwYDsLkIlwQp6LFnl8zNdLGxu9YAA1QvwINks4PhcElQSvqcyVLLD9aMhXd13uQjoXtEKNosOWaZqXgel0g==" crossorigin="anonymous" referrerpolicy="no-referrer" />

        
        <script async defer src="https://polyfill.io/v3/polyfill.min.js?features=smoothscroll%2Cfetch%2Cdefault%2CIntersectionObserver" type="bc29ea08ed5de13fee4ff9a3-text/javascript"></script>
        <link rel="stylesheet" type="text/css" href="/assets/css/style.css">
        @yield("css")
    </head>
    <body id="page_home__show">
      <main role="main">
        @include('layouts.enterprise.nav')
        
        @yield("content")
      </main>
        @include('layouts.enterprise.footer')
        <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
        @if(env('APP_ENV') != "local")
            <!-- Global site tag (gtag.js) - Google Analytics -->
            <script async src="https://www.googletagmanager.com/gtag/js?id=UA-219713767-1">
            </script>
            <script>
              window.dataLayer = window.dataLayer || [];
              function gtag(){dataLayer.push(arguments);}
              gtag('js', new Date());

              gtag('config', 'UA-219713767-1');
            </script>
            <!-- Google tag (gtag.js) -->
            <script async src="https://www.googletagmanager.com/gtag/js?id=G-HG76RQTB33"></script>
            <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'G-HG76RQTB33');
            </script>
            <!-- Hotjar Tracking Code for https://venti.co -->
            <script>
              (function(h,o,t,j,a,r){
                  h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
                  h._hjSettings={hjid:2651983,hjsv:6};
                  a=o.getElementsByTagName('head')[0];
                  r=o.createElement('script');r.async=1;
                  r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
                  a.appendChild(r);
              })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
            </script>
            <script>
              jQuery(document).ready(function ($) {
                $("#mc_embed_signup").submit(function() {
                    gtag('event', 'conversion', {'send_to': 'AW-988166781/PyJ4CNGUs5ADEP30mNcD'});
                });
                $(".application").click(function(){
                  gtag('event', 'conversion', {'send_to': 'AW-988166781/PyJ4CNGUs5ADEP30mNcD'});
                });
              });
            </script> 
        
        @endif
        
        <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" crossorigin="anonymous"></script>
        <script src="/assets/js/bootstrap.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.min.js"></script>
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.3/moment.min.js"></script>
        <script src="/assets/js/scripts.js"></script>
        @yield("js")
    </body>
</html>
