<nav class="navbar-static venti-navbar" id="navbar" style="border-top:none; margin-top:30px;">
  <div class="section-wrapper my-0">
    <div class="row">
        <div class="left-nav">
            <a href="/" class="active venti-logo" style="">
              <img class="logo" src="/assets/img/venti-trademark-logo-black.png"
                alt="venti logo" />
            </a>
        </div>
        <a href="javascript:void(0)" class="hamburger-menu right-nav dark-on-light" aria-label="hamburger menu" id="hamburger-menu">
            <span class="fa fa-bars"></span>
        </a>
        <div data-html-src="/partials/login-nav" id="navbar-links-partial" class="right-nav right-nav-mobile">
            <ul class="hamburger-menu-list" id="hamburger-menu-list">
                <li class="position-relative" id="products-nav-link">
                    <a href="{{ route('dashboard-home') }}" class="nav-links dark-on-light " data-event-name="Nav: Products">HOME</a>
                </li>
                <li class="position-relative" id="products-nav-link">
                    <a href="{{ route('dashboard-catalog') }}" class="nav-links dark-on-light " data-event-name="Nav: Products">CATALOG</a>
                </li>
                <li class="position-relative" id="products-nav-link">
                    <a href="{{ route('dashboard-clients') }}" class="nav-links dark-on-light " data-event-name="Nav: Products">CLIENTS</a>
                </li>
                <!--
                <li>
                    <a class="dark-on-light nav-links" data-event-name="Nav: Enter Link" href="/shop">BROWSE TRIPS</a>
                </li>
            -->
            </ul>
        </div>
    </div>
  </div>
</nav>