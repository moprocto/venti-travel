<nav class="navbar-static venti-navbar" id="navbar" style="border-top:none; margin-top:30px;">
  <div class="section-wrapper my-0">
    <div class="row">
        <div class="left-nav">
            <a href="/" class="active venti-logo" style="">
              <img class="logo" src="/assets/img/venti-trademark-logo-black-boarding.png"
                alt="venti logo" />
            </a>
        </div>
        <a href="javascript:void(0)" class="hamburger-menu right-nav dark-on-light"  aria-label="hamburger menu" id="hamburger-menu" style="margin-top:-8px">
            <span class="fa fa-bars" ></span>
        </a>
        <div data-html-src="/partials/login-nav" id="navbar-links-partial" class="right-nav right-nav-mobile">
            <ul class="hamburger-menu-list" id="hamburger-menu-list">
                @if(Session::get('user') == null)
                
                <li class="nav-link-button">
                    <a class="dark-on-light nav-links btn btn-black" data-event-name="Nav: Login" href="/login" style="margin-top:-5px;">LOG IN</a>
                </li>
                <li class="nav-link-button">
                    <a class="dark-on-light nav-links btn btn-white" data-event-name="Nav: Register" href="/register" style="margin-top:-5px; margin-left:0">REGISTER</a>
                </li>
                @else
                @endguest
                
            </ul>
        </div>
    </div>
  </div>
</nav>