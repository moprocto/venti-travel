<!DOCTYPE html>
<html lang="en-US">
   <head>
         <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
            <!-- Title -->
            <title>venti: Save for Travel.</title>
            <meta name="description" content="venti is the easiest way to save up for all your travel adventures."/>
            <meta property="og:url" content="https://venti.co" />
            <meta property="og:type" content="website" />
            <meta property="og:title" content="venti: Save for Travel." />
            <meta property="og:description" content="venti is the easiest way to save up for all your travel adventures." />
            <meta property="og:image" content="https://venti.co/assets/savings/img/venti-opengraph-banner-1.png?i=1" />
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
      <link rel='stylesheet' id='wp-block-library-css'  href='https://sala.uxper.co/wp-includes/css/dist/block-library/style.min.css' media='all' />
      <link rel='stylesheet' id='contact-form-7-css'  href='https://sala.uxper.co/wp-content/plugins/contact-form-7/includes/css/styles.css' media='all' />
      <link rel='stylesheet' id='walcf7-datepicker-css-css'  href='https://sala.uxper.co/wp-content/plugins/date-time-picker-for-contact-form-7/assets/css/jquery.datetimepicker.min.css' media='all' />
      <link rel='stylesheet' id='select2-css'  href='https://sala.uxper.co/wp-content/plugins/uxper-crypto/assets/libs/select2/css/select2.min.css' media='all' />
      <link rel='stylesheet' id='uxper_crypto_-template-css'  href='https://sala.uxper.co/wp-content/plugins/uxper-crypto/assets/css/_template.css' media='all' />
      <link rel='stylesheet' id='sala-style-css'  href='/assets/savings/css/style.css' media='all' />
      <style id='sala-style-inline-css'>
         .site-content{padding-top:0}.site-content{padding-bottom:0}.lg-backdrop{}
      </style>
      <link rel='stylesheet' id='sala-child-style-css'  href='https://sala.uxper.co/wp-content/themes/sala-child-demo/style.css' media='all' />
       <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
      <link rel='stylesheet' id='atropos-css'  href='https://sala.uxper.co/wp-content/themes/sala/assets/libs/atropos/atropos.css' media='all' />
      <link rel='stylesheet' id='sala-font-poppins-css'  href='/assets/savings/css/poppins.css' media='all' />
      <link rel='stylesheet' id='sala-header-style-css' href="https://sala.uxper.co/wp-content/uploads/sala/header/header-01.css" media='all' />
      <link rel='stylesheet' id='elementor-frontend-css'  href='https://sala.uxper.co/wp-content/plugins/elementor/assets/css/frontend.min.css' media='all' />
      <link rel='stylesheet' id='elementor-post-3655-css'  href='https://sala.uxper.co/wp-content/uploads/elementor/css/post-3655.css' media='all' />
      <link rel='stylesheet' id='elementor-global-css'  href='https://sala.uxper.co/wp-content/uploads/elementor/css/global.css' media='all' />
      
      <link rel='stylesheet' id='elementor-post-10507-css'  href='/assets/savings/css/post-4033.css' media='all' />
      <link rel='stylesheet' id='elementor-post-10507-css'  href='/assets/savings/css/post-12493.css' media='all' />
      <link rel='stylesheet' id='elementor-post-10507-css'  href='/assets/savings/css/post.css' media='all' />
      <link rel='stylesheet' id='google-fonts-1-css'  href='https://fonts.googleapis.com/css?family=Poppins%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CCormorant%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic&#038;display=auto' media='all' />
      <script src='https://sala.uxper.co/wp-includes/js/jquery/jquery.min.js' id='jquery-core-js'></script>
      <script src='https://sala.uxper.co/wp-includes/js/jquery/jquery-migrate.min.js' id='jquery-migrate-js'></script>
      <noscript>
         <style>.woocommerce-product-gallery{ opacity: 1 !important; }</style>
      </noscript>
      <link rel="icon" href="/assets/img/venti-financial-logo-50.png" sizes="32x32" />
      <link rel="icon" href="/assets/img/venti-financial-logo-192.png" sizes="192x192" />
      <link rel="apple-touch-icon" href="/assets/img/venti-financial-logo.png" />
      <meta name="msapplication-TileImage" content="/assets/img/venti-financial-logo.png" />
      <style id="kirki-inline-styles">
      </style>
      <link rel='stylesheet' id='elementor-post-10507-css'  href='/assets/savings/css/global.css' media='all' />
            <link rel='stylesheet' id='woocommerce-layout-css'  href='https://sala.uxper.co/wp-content/plugins/woocommerce/assets/css/woocommerce-layout.css' media='all' />
      <link rel='stylesheet' id='woocommerce-smallscreen-css'  href='https://sala.uxper.co/wp-content/plugins/woocommerce/assets/css/woocommerce-smallscreen.css' media='only screen and (max-width: 768px)' />
      <link rel='stylesheet' id='woocommerce-general-css'  href='https://sala.uxper.co/wp-content/plugins/woocommerce/assets/css/woocommerce.css' media='all' />
      <style id='woocommerce-inline-inline-css'>
         .woocommerce form .form-row .required { visibility: visible; }
      </style>

      @yield('css')
   </head>
   <body class="page-template-default page page-id-4404 theme-sala woocommerce-js fullwidth right-sidebar page-title-none elementor-default elementor-kit-3655 elementor-page elementor-page-4404 e--ua-blink e--ua-chrome e--ua-mac e--ua-webkit loaded">
      <div id="page-preloader" class="page-loading-effect">
         <div class="bg-overlay"></div>
         <div class="page-loading-inner">
            <span class="sala-ldef-ring sala-ldef-loading"><span></span><span></span><span></span><span></span></span>			
         </div>
      </div>
      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 0 0" width="0" height="0" focusable="false" role="none" style="visibility: hidden; position: absolute; left: -9999px; overflow: hidden;" >
         <defs>
            <filter id="wp-duotone-dark-grayscale">
               <feColorMatrix color-interpolation-filters="sRGB" type="matrix" values=" .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 " />
               <feComponentTransfer color-interpolation-filters="sRGB" >
                  <feFuncR type="table" tableValues="0 0.49803921568627" />
                  <feFuncG type="table" tableValues="0 0.49803921568627" />
                  <feFuncB type="table" tableValues="0 0.49803921568627" />
                  <feFuncA type="table" tableValues="1 1" />
               </feComponentTransfer>
               <feComposite in2="SourceGraphic" operator="in" />
            </filter>
         </defs>
      </svg>
      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 0 0" width="0" height="0" focusable="false" role="none" style="visibility: hidden; position: absolute; left: -9999px; overflow: hidden;" >
         <defs>
            <filter id="wp-duotone-grayscale">
               <feColorMatrix color-interpolation-filters="sRGB" type="matrix" values=" .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 " />
               <feComponentTransfer color-interpolation-filters="sRGB" >
                  <feFuncR type="table" tableValues="0 1" />
                  <feFuncG type="table" tableValues="0 1" />
                  <feFuncB type="table" tableValues="0 1" />
                  <feFuncA type="table" tableValues="1 1" />
               </feComponentTransfer>
               <feComposite in2="SourceGraphic" operator="in" />
            </filter>
         </defs>
      </svg>
      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 0 0" width="0" height="0" focusable="false" role="none" style="visibility: hidden; position: absolute; left: -9999px; overflow: hidden;" >
         <defs>
            <filter id="wp-duotone-purple-yellow">
               <feColorMatrix color-interpolation-filters="sRGB" type="matrix" values=" .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 " />
               <feComponentTransfer color-interpolation-filters="sRGB" >
                  <feFuncR type="table" tableValues="0.54901960784314 0.98823529411765" />
                  <feFuncG type="table" tableValues="0 1" />
                  <feFuncB type="table" tableValues="0.71764705882353 0.25490196078431" />
                  <feFuncA type="table" tableValues="1 1" />
               </feComponentTransfer>
               <feComposite in2="SourceGraphic" operator="in" />
            </filter>
         </defs>
      </svg>
      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 0 0" width="0" height="0" focusable="false" role="none" style="visibility: hidden; position: absolute; left: -9999px; overflow: hidden;" >
         <defs>
            <filter id="wp-duotone-blue-red">
               <feColorMatrix color-interpolation-filters="sRGB" type="matrix" values=" .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 " />
               <feComponentTransfer color-interpolation-filters="sRGB" >
                  <feFuncR type="table" tableValues="0 1" />
                  <feFuncG type="table" tableValues="0 0.27843137254902" />
                  <feFuncB type="table" tableValues="0.5921568627451 0.27843137254902" />
                  <feFuncA type="table" tableValues="1 1" />
               </feComponentTransfer>
               <feComposite in2="SourceGraphic" operator="in" />
            </filter>
         </defs>
      </svg>
      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 0 0" width="0" height="0" focusable="false" role="none" style="visibility: hidden; position: absolute; left: -9999px; overflow: hidden;" >
         <defs>
            <filter id="wp-duotone-midnight">
               <feColorMatrix color-interpolation-filters="sRGB" type="matrix" values=" .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 " />
               <feComponentTransfer color-interpolation-filters="sRGB" >
                  <feFuncR type="table" tableValues="0 0" />
                  <feFuncG type="table" tableValues="0 0.64705882352941" />
                  <feFuncB type="table" tableValues="0 1" />
                  <feFuncA type="table" tableValues="1 1" />
               </feComponentTransfer>
               <feComposite in2="SourceGraphic" operator="in" />
            </filter>
         </defs>
      </svg>
      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 0 0" width="0" height="0" focusable="false" role="none" style="visibility: hidden; position: absolute; left: -9999px; overflow: hidden;" >
         <defs>
            <filter id="wp-duotone-magenta-yellow">
               <feColorMatrix color-interpolation-filters="sRGB" type="matrix" values=" .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 " />
               <feComponentTransfer color-interpolation-filters="sRGB" >
                  <feFuncR type="table" tableValues="0.78039215686275 1" />
                  <feFuncG type="table" tableValues="0 0.94901960784314" />
                  <feFuncB type="table" tableValues="0.35294117647059 0.47058823529412" />
                  <feFuncA type="table" tableValues="1 1" />
               </feComponentTransfer>
               <feComposite in2="SourceGraphic" operator="in" />
            </filter>
         </defs>
      </svg>
      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 0 0" width="0" height="0" focusable="false" role="none" style="visibility: hidden; position: absolute; left: -9999px; overflow: hidden;" >
         <defs>
            <filter id="wp-duotone-purple-green">
               <feColorMatrix color-interpolation-filters="sRGB" type="matrix" values=" .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 " />
               <feComponentTransfer color-interpolation-filters="sRGB" >
                  <feFuncR type="table" tableValues="0.65098039215686 0.40392156862745" />
                  <feFuncG type="table" tableValues="0 1" />
                  <feFuncB type="table" tableValues="0.44705882352941 0.4" />
                  <feFuncA type="table" tableValues="1 1" />
               </feComponentTransfer>
               <feComposite in2="SourceGraphic" operator="in" />
            </filter>
         </defs>
      </svg>
      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 0 0" width="0" height="0" focusable="false" role="none" style="visibility: hidden; position: absolute; left: -9999px; overflow: hidden;" >
         <defs>
            <filter id="wp-duotone-blue-orange">
               <feColorMatrix color-interpolation-filters="sRGB" type="matrix" values=" .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 " />
               <feComponentTransfer color-interpolation-filters="sRGB" >
                  <feFuncR type="table" tableValues="0.098039215686275 1" />
                  <feFuncG type="table" tableValues="0 0.66274509803922" />
                  <feFuncB type="table" tableValues="0.84705882352941 0.41960784313725" />
                  <feFuncA type="table" tableValues="1 1" />
               </feComponentTransfer>
               <feComposite in2="SourceGraphic" operator="in" />
            </filter>
         </defs>
      </svg>
      <div id="wrapper">
         <div id="canvas-search" class="sala-popup popup-search">
            <div class="bg-overlay"></div>
            <a href="#" class="btn-canvas-menu btn-close"><i class="fal fa-times"></i></a>
            <div class="inner-popup">
               <div class="search-form ajax-search-form">
                  <form action="https://sala.uxper.co/" method="get" class="search-form">
                     <div class="area-search form-field">
                        <button type="submit" class="icon-search"><i class="far fa-search large"></i></button>
                        <div class="form-field input-field">
                           <input name="s" class="input-search" type="text" value="" placeholder="Search..." autocomplete="off" />
                           <input type="hidden" name="post_type" class="post-type" value="post"/>
                           <div class="search-result area-result"></div>
                           <div class="sala-loading-effect"><span class="sala-dual-ring"></span></div>
                        </div>
                     </div>
                  </form>
               </div>
            </div>
         </div>
         
         @include('layouts.savings.header')

         <div class="site-content">
            <div class="container">

         @yield('content')
            </div>
         </div>
         
         @include('layouts.savings.footer')

      </div>
      <!-- End #wrapper -->
      
      <link rel='stylesheet' id='e-animations-css'  href='/assets/savings/css/animations.min.css' media='all' />
      <script src='https://sala.uxper.co/wp-includes/js/dist/vendor/regenerator-runtime.min.js' id='regenerator-runtime-js'></script>
      <script src='https://sala.uxper.co/wp-includes/js/dist/vendor/wp-polyfill.min.js' id='wp-polyfill-js'></script>
      
      <script src='https://sala.uxper.co/wp-content/plugins/uxper-crypto/assets/js/socket.io.min.js' id='socket-io-js'></script>
      <script src='https://sala.uxper.co/wp-content/plugins/uxper-crypto/assets/js/template.js' id='uxper_crypto_template-js'></script>
      <script src='https://sala.uxper.co/wp-content/plugins/woocommerce/assets/js/jquery-blockui/jquery.blockUI.min.js' id='jquery-blockui-js'></script>
      <script id='wc-add-to-cart-js-extra'>
         /* <![CDATA[ */
         var wc_add_to_cart_params = {"ajax_url":"\/wp-admin\/admin-ajax.php","wc_ajax_url":"\/?wc-ajax=%%endpoint%%","i18n_view_cart":"View cart","cart_url":"https:\/\/sala.uxper.co\/cart\/","is_cart":"","cart_redirect_after_add":"no"};
         /* ]]> */
      </script>
      <script src='https://sala.uxper.co/wp-content/plugins/woocommerce/assets/js/frontend/add-to-cart.min.js' id='wc-add-to-cart-js'></script>
      <script src='https://sala.uxper.co/wp-content/plugins/woocommerce/assets/js/js-cookie/js.cookie.min.js' id='js-cookie-js'></script>
      <script id='woocommerce-js-extra'>
         /* <![CDATA[ */
         var woocommerce_params = {"ajax_url":"\/wp-admin\/admin-ajax.php","wc_ajax_url":"\/?wc-ajax=%%endpoint%%"};
         /* ]]> */
      </script>
      <script src='https://sala.uxper.co/wp-content/plugins/woocommerce/assets/js/frontend/woocommerce.min.js' id='woocommerce-js'></script>
      <script id='wc-cart-fragments-js-extra'>
         /* <![CDATA[ */
         var wc_cart_fragments_params = {"ajax_url":"\/wp-admin\/admin-ajax.php","wc_ajax_url":"\/?wc-ajax=%%endpoint%%","cart_hash_key":"wc_cart_hash_dedc2cdf3af8ee9a4a6b9ee2fffd3c03","fragment_name":"wc_fragments_dedc2cdf3af8ee9a4a6b9ee2fffd3c03","request_timeout":"5000"};
         /* ]]> */
      </script>
      <script src='/assets/savings/js/widget-accordion.js'></script>
      <script src='https://sala.uxper.co/wp-content/plugins/woocommerce/assets/js/frontend/cart-fragments.min.js' id='wc-cart-fragments-js'></script>
      <script src='https://sala.uxper.co/wp-content/themes/sala-child-demo/script.js' id='sala-child-script-js'></script>
      <script src='https://sala.uxper.co/wp-content/themes/sala/assets/libs/isotope/js/isotope.pkgd.min.js' id='isotope-js'></script>
      <script src='https://sala.uxper.co/wp-content/themes/sala/assets/libs/infinite-scroll/infinite-scroll.pkgd.min.js' id='infinite-scroll-js'></script>
      <script src='https://sala.uxper.co/wp-content/themes/sala/assets/libs/packery/packery-mode.pkgd.min.js' id='packery-js'></script>
      <script src='https://sala.uxper.co/wp-content/themes/sala/assets/libs/atropos/atropos.min.js' id='atropos-js'></script>
      <script src='https://sala.uxper.co/wp-content/plugins/elementor/assets/lib/swiper/swiper.min.js' id='swiper-js'></script>
      <script src='https://sala.uxper.co/wp-content/themes/sala/assets/libs/growl/js/jquery.growl.min.js' id='growl-js'></script>
      <script src='https://sala.uxper.co/wp-content/themes/sala/assets/libs/waypoints/jquery.waypoints.js' id='waypoints-js'></script>
      <script src='https://sala.uxper.co/wp-content/themes/sala/assets/libs/smartmenus/jquery.smartmenus.js' id='smartmenus-js'></script>
      <script src='https://sala.uxper.co/wp-content/themes/sala/assets/libs/smooth-scroll/SmoothScroll.min.js' id='smooth-scroll-js'></script>
      <script src='https://sala.uxper.co/wp-content/themes/sala/assets/libs/smooth-scroll/jquery.smooth-scroll.min.js' id='jquery-smooth-scroll-js'></script>
      <script src='https://sala.uxper.co/wp-content/themes/sala/assets/libs/validate/jquery.validate.min.js' id='validate-js'></script>
      <script src='https://sala.uxper.co/wp-content/themes/sala/assets/libs/elastic-stack/js/modernizr.custom.js' id='modernizr-js'></script>
      <script src='https://sala.uxper.co/wp-content/themes/sala/assets/libs/elastic-stack/js/draggabilly.pkgd.min.js' id='elastic-stack-pkgd-js'></script>
      <script src='https://sala.uxper.co/wp-content/themes/sala/assets/libs/elastic-stack/js/elastiStack.js' id='elastic-stack-js'></script>
      <script src='https://sala.uxper.co/wp-content/themes/sala/assets/libs/elastic-stack/js/jquery.windy.js' id='windy-js'></script>
      <script src='https://sala.uxper.co/wp-content/themes/sala/assets/libs/flickity/js/flickity.pkgd.min.js' id='flickity-marquee-js'></script>
      <script src='https://sala.uxper.co/wp-content/themes/sala/assets/libs/jquery.countdown/js/jquery.countdownTimer.js' id='countdown-js'></script>
      <script id='sala-swiper-js-extra'>
         /* <![CDATA[ */
         var $salaSwiper = {"prevText":"Prev","nextText":"Next"};
         /* ]]> */
      </script>
      <script src='https://sala.uxper.co/wp-content/themes/sala/assets/js/swiper.js' id='sala-swiper-js'></script>
      <script src='https://sala.uxper.co/wp-content/themes/sala/assets/libs/jquery-nice-select/js/jquery.nice-select.min.js' id='nice-select-js'></script>
      <script src='https://sala.uxper.co/wp-content/themes/sala/assets/js/grid-query.js' id='sala-grid-query-js'></script>
      <script src='https://sala.uxper.co/wp-includes/js/imagesloaded.min.js' id='imagesloaded-js'></script>
      <script src='https://sala.uxper.co/wp-content/themes/sala/assets/libs/isotope/js/isotope.pkgd.min.js' id='isotope-masonry-js'></script>
      <script src='https://sala.uxper.co/wp-content/themes/sala/assets/libs/packery/packery-mode.pkgd.min.js' id='packery-mode-js'></script>
      <script src='https://sala.uxper.co/wp-content/themes/sala/assets/js/grid-layout.js' id='sala-grid-layout-js'></script>
      <script id='sala-main-js-js-extra'>
         /* <![CDATA[ */
         var theme_vars = {"ajax_url":"https:\/\/sala.uxper.co\/wp-admin\/admin-ajax.php","header_sticky":"","content_protected_enable":"0","scroll_top_enable":"0","send_user_info":"Sending user info,please wait...","notice_cookie_enable":"","notice_cookie_confirm":"no","notice_cookie_messages":"<i class=\"fal fa-cookie-bite\"><\/i><p>We use cookies to ensure that we give you the best experience on our website. If you continue to use this site we will assume that you are happy with it.<\/p><a id=\"sala-button-cookie-notice-ok\" class=\"sala-button full-filled size-xs wide\">Accept all cookies<\/a>"};
         /* ]]> */
      </script>
      <script src='https://sala.uxper.co/wp-content/themes/sala/assets/js/main.js' id='sala-main-js-js'></script>
      <script src='https://sala.uxper.co/wp-content/themes/sala//inc/elementor/assets/js/widgets/widget-list.js' id='sala-widget-list-js'></script>
      <script src='https://sala.uxper.co/wp-content/themes/sala//inc/elementor/assets/libs/vivus/vivus.js' id='vivus-js'></script>
      <script src='https://sala.uxper.co/wp-content/themes/sala//inc/elementor/assets/js/widgets/widget-icon-box.js' id='sala-widget-icon-box-js'></script>
      <script src='https://sala.uxper.co/wp-content/themes/sala//inc/elementor/assets/js/widgets/group-widget-carousel.js' id='sala-group-widget-carousel-js'></script>
      <script defer src='https://sala.uxper.co/wp-content/plugins/mailchimp-for-wp/assets/js/forms.js' id='mc4wp-forms-api-js'></script>
      <script src='https://sala.uxper.co/wp-content/plugins/elementor/assets/js/webpack.runtime.min.js' id='elementor-webpack-runtime-js'></script>
      <script src='https://sala.uxper.co/wp-content/plugins/elementor/assets/js/frontend-modules.min.js' id='elementor-frontend-modules-js'></script>
      <script src='https://sala.uxper.co/wp-content/plugins/elementor/assets/lib/waypoints/waypoints.min.js' id='elementor-waypoints-js'></script>
      <script src='https://sala.uxper.co/wp-includes/js/jquery/ui/core.min.js' id='jquery-ui-core-js'></script>
      <script src='https://sala.uxper.co/wp-content/plugins/elementor/assets/lib/share-link/share-link.min.js' id='share-link-js'></script>
      <script src='https://sala.uxper.co/wp-content/plugins/elementor/assets/lib/dialog/dialog.min.js' id='elementor-dialog-js'></script>
      <script id='elementor-frontend-js-before'>
         var elementorFrontendConfig = {"environmentMode":{"edit":false,"wpPreview":false,"isScriptDebug":false},"i18n":{"shareOnFacebook":"Share on Facebook","shareOnTwitter":"Share on Twitter","pinIt":"Pin it","download":"Download","downloadImage":"Download image","fullscreen":"Fullscreen","zoom":"Zoom","share":"Share","playVideo":"Play Video","previous":"Previous","next":"Next","close":"Close"},"is_rtl":false,"breakpoints":{"xs":0,"sm":480,"md":768,"lg":1025,"xl":1440,"xxl":1600},"responsive":{"breakpoints":{"mobile":{"label":"Mobile","value":767,"default_value":767,"direction":"max","is_enabled":true},"mobile_extra":{"label":"Mobile Extra","value":880,"default_value":880,"direction":"max","is_enabled":false},"tablet":{"label":"Tablet","value":1024,"default_value":1024,"direction":"max","is_enabled":true},"tablet_extra":{"label":"Tablet Extra","value":1200,"default_value":1200,"direction":"max","is_enabled":false},"laptop":{"label":"Laptop","value":1366,"default_value":1366,"direction":"max","is_enabled":false},"widescreen":{"label":"Widescreen","value":2400,"default_value":2400,"direction":"min","is_enabled":false}}},"version":"3.4.8","is_static":false,"experimentalFeatures":{"e_dom_optimization":true,"a11y_improvements":true,"e_import_export":true,"landing-pages":true,"elements-color-picker":true,"admin-top-bar":true},"urls":{"assets":"https:\/\/sala.uxper.co\/wp-content\/plugins\/elementor\/assets\/"},"settings":{"page":[],"editorPreferences":[]},"kit":{"active_breakpoints":["viewport_mobile","viewport_tablet"],"global_image_lightbox":"yes","lightbox_enable_counter":"yes","lightbox_enable_fullscreen":"yes","lightbox_enable_zoom":"yes","lightbox_enable_share":"yes","lightbox_title_src":"title","lightbox_description_src":"description"},"post":{"id":10507,"title":"Home%20Dating%20%E2%80%93%20Sala","excerpt":"","featuredImage":false}};
      </script>
      <script src='https://sala.uxper.co/wp-content/plugins/uxper-crypto/assets/libs/select2/js/select2.min.js' id='select2-js'></script>
      <script src='https://sala.uxper.co/wp-content/plugins/elementor/assets/js/frontend.min.js' id='elementor-frontend-js'></script>
      <script src='https://sala.uxper.co/wp-content/plugins/elementor/assets/js/preloaded-modules.min.js' id='preloaded-modules-js'></script>

      @if(env('APP_ENV') != "local")
            <!-- Global site tag (gtag.js) - Google Analytics -->
            <script async src="https://www.googletagmanager.com/gtag/js?id=UA-219713767-1">
            </script>
            <script>
              window.dataLayer = window.dataLayer || [];
              function gtag(){dataLayer.push(arguments);}
              gtag('js', new Date());

              gtag('config', 'UA-219713767-1');
            </script>
            <!-- Hotjar Tracking Code for https://venti.co -->
            <script>
              (function(h,o,t,j,a,r){
                  h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
                  h._hjSettings={hjid:2651983,hjsv:6};
                  a=o.getElementsByTagName('head')[0];
                  r=o.createElement('script');r.async=1;
                  r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
                  a.appendChild(r);
              })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
            </script>
            <script>
            $("#mc_embed_signup").submit(function() {
                gtag('event', 'conversion', {'send_to': 'AW-988166781/PyJ4CNGUs5ADEP30mNcD'});
            });
            $(".application").click(function(){
            gtag('event', 'conversion', {'send_to': 'AW-988166781/PyJ4CNGUs5ADEP30mNcD'});
            });
            </script> 

        @endif
   </body>
</html>