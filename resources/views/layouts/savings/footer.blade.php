<footer id="footer" class="site-footer">
            <div data-elementor-type="wp-post" data-elementor-id="5690" class="elementor elementor-5690" data-elementor-settings="[]">
               <div class="elementor-section-wrap">
                  
                  <div class="elementor-section elementor-top-section elementor-element elementor-element-42ef917 border-01 elementor-section-stretched elementor-section-boxed elementor-section-gap-beside-yes elementor-section-height-default elementor-section-height-default elementor-section-column-vertical-align-stretch" style="border:none; ">
                     <div class="elementor-container elementor-column-gap-extended">
                        <div class="elementor-column elementor-col-50 elementor-top-column elementor-element elementor-element-6800281" data-id="6800281" data-element_type="column">
                           <div class="elementor-widget-wrap elementor-element-populated">
                              <div class="elementor-element elementor-element-6bf144d elementor-widget elementor-widget-text-editor" data-id="6bf144d" data-element_type="widget" data-widget_type="text-editor.default">
                                 <div class="elementor-widget-container">
                                    <p>© {{ Date('Y') }} Venti Financial, Inc.</p>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="elementor-column elementor-col-50 elementor-top-column elementor-element elementor-element-689e548" data-id="689e548" data-element_type="column">
                           <div class="elementor-widget-wrap elementor-element-populated">
                              <div class="elementor-element elementor-element-15c1462 sala-list-layout-inline sala-list-footer elementor-widget elementor-widget-sala-list" data-id="15c1462" data-element_type="widget" data-widget_type="sala-list.default">
                                 <div class="elementor-widget-container">
                                    <div class="sala-list" data-id="">
                                       <div class="item">

                                             <div class="list-header">
                                                <div class="text-wrap">
                                                   <div class="text">
                                                      Made in Baltimore ❤️                     
                                                   </div>
                                                </div>
                                             </div>

                                       </div>
                                       <div class="item">
                                          <a class="link" href="/privacy" style="color: #0057FC !important">
                                             <div class="list-header">
                                                <div class="text-wrap">
                                                   <div class="text" style="color: #0057FC !important">
                                                      Terms	& Privacy								
                                                   </div>
                                                </div>
                                             </div>
                                          </a>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </footer>