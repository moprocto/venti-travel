<footer id="footer">
  <div class="row section-wrapper" style="margin: 0 auto;">
    <div class="col-lg-3 col-md-12 col-sm-12" style="margin-bottom:2em;">
      <h4 class="bold"><img src="/assets/img/venti-trademark-logo-black.png" style="width:100%; min-width:100px; max-width: 100px;"></h4>
      <div class="sub-footer">
      
      <br><br>
      <p>Venti Financial, Inc. is a financial technology company, not a bank.
    </p>
    </div>
    </div>
    <div class="col-lg-3 col-md-12 col-sm-12" style="margin-bottom:2em;">
      
    </div>
    <div class="col-lg-3 col-md-12 col-sm-12">
      <h4 class="bold text">Our Address</h4>
      <ul class="footer-lists">
        <li style="">Venti Financial, Inc.<br>
                  800 N King Street <br>
                  Suite 304 1552 <br>
                  Wilmington, DE 19801</li>
      </ul>
    </div>
    <div class="col-lg-3 col-md-12 col-sm-12">
      <div class="mt-2 mb-2 d-flex align-items-center">
      </div>
    </div>
  </div>
  <div class="section-wrapper">
  <div class="sub-footer">© {{ Date('Y') }} Venti Financial, Inc. All rights reserved.</div>
    
  </div>

</footer>