<title>{{ $title ?? 'Reward Everything' }}</title>
<meta name="description" content="{{ $description ?? 'Venti helps you explore the world with people that have fun like you do' }}"/>
<meta property="og:url" content="{{ $url ?? 'https://venti.co' }}" />
<meta property="og:type" content="website" />
<meta content="en_US" property="og:locale">
<meta content="summary" property="og:card">
<meta property="og:title" content="{{ $title ?? 'Discover Your Next Adventure' }}" />
<meta property="og:description" content="{{ $description ?? 'Venti helps you explore the world with people that have fun like you do' }}" />
<meta property="og:image" content="{{ $image ?? 'https://venti.co/assets/img/venti-social-banner.jpeg' }}" />
<link href="/css/landing.css" rel="stylesheet">
<link rel="icon" href="/assets/img/venti-financial-logo-50.png" sizes="32x32" />
<link rel="icon" href="/assets/img/venti-financial-logo-192.png" sizes="192x192" />
<link rel="preconnect" href="https://fonts.gstatic.com/" crossorigin>
<link href="{{ $url ?? 'https://venti.co' }}" rel="canonical">
<meta charset="utf-8">
<meta content="IE=edge" http-equiv="X-UA-Compatible">
<meta content="{{ $title ?? 'Discover Your Next Adventure' }}" name="title">
<meta content="{{ $description ?? 'Venti helps you explore the world with people that have fun like you do' }}" name="description">
<meta content="initial-scale=1.0,minimum-scale=1.0,maximum-scale=5.0,width=device-width,height=device-height,user-scalable=yes" name="viewport">
<meta content="summary" name="twitter:card">
<meta content="@ventifinancial" name="twitter:site">
<meta content="{{ $url ?? 'https://venti.co' }}" name="twitter:url">
<meta content="{{ $title ?? 'Discover Your Next Adventure' }}" name="twitter:title">
<meta content="{{ $description ?? 'Venti helps you explore the world with people that have fun like you do' }}" name="twitter:description">
<meta content="Venti" property="og:site_name">
<meta content="https://venti.co" property="og:site">
<meta content="@venti.travel" property="og:creator">
@if(isset($hide))
	<meta name=”robots” content=”noindex,nofollow”>
@endif