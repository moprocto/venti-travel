<nav class="navbar-static venti-navbar" id="navbar" style="border-top:none; margin-top:30px;">
  <div class="section-wrapper my-0">
    <div class="row">
        <div class="left-nav">
            <a href="/" class="active venti-logo" style="">
              <img class="logo" src="/assets/img/venti-trademark-logo-black.png"
                alt="venti logo" />
            </a>
            <div data-html-src="/partials/login-nav" id="navbar-links-partial" class="left-nav left-nav-mobile">
            @if(Route::current() !== null)
            <ul class="hamburger-menu-list" id="hamburger-menu-list">
                <li>
                    <a style="font-weight: 700;" class="dark-on-light nav-links btn bg-none  @if(Route::current()->getName() == 'boarding-index')bg-active @endif" data-event-name="Nav: Home" href="/boardingpass">Savings</a>
                </li>
                <li>
                    <a style="font-weight: 700;" class="dark-on-light nav-links btn bg-none  @if(Route::current()->getName() == 'flights')bg-active @endif" data-event-name="Nav: Flights" href="{{ route('flights') }}">Flights</a>
                </li>

                <li>
                    <a style="font-weight: 700;" class="dark-on-light nav-links btn bg-none  @if(Route::current()->getName() == 'hotels')bg-active @endif" data-event-name="Nav: Shop" href="{{ route('hotels') }}">Hotels</a>
                </li>

                <li>
                    <a style="font-weight: 700;" class="dark-on-light nav-links btn bg-none  @if(Route::current()->getName() == 'insurance')bg-active @endif" data-event-name="Nav: Insurance" href="/insurance">Insurance</a>
                </li>
                @if(Session::get('user') !== null)
                    @if(env("APP_ENV") == "local")
                        <!--
                        <li>
                            <a style="font-weight: 700;" class="dark-on-light nav-links btn bg-none  @if(Route::current()->getName() == 'shop')bg-active @endif" data-event-name="Nav: Shop" href="{{ route('shop') }}">Shop</a>
                        </li>
                        -->
                    @endif
                    <li>
                        <a style="font-weight: 700;" class="dark-on-light nav-links btn bg-none  @if(Route::current()->getName() == 'trips')bg-active @endif" data-event-name="Nav: Trips" href="{{ route('trips') }}">Trips</a>
                    </li>
                @endif

                @if(Session::get('user') == null)

                        <li class="d-xs-block d-sm-block d-md-block d-xl-none">
                            <a class="dark-on-light nav-links btn btn-white bg-none  @if(Route::current()->getName() == 'boarding-index')bg-active @endif" data-event-name="Nav: Invites" href="/login">Login</a>
                        </li>
                        <li class="register-row d-xs-block d-sm-block d-md-block d-xl-none">
                            <a class="dark-on-light nav-links btn btn-white bg-none  @if(Route::current()->getName() == 'flights')bg-active @endif" data-event-name="Nav: Invites" href="/register">Register</a>
                        </li>

                    <div style="position: absolute; right:20px; top:5px" class="d-none d-md-none d-lg-none d-xl-block">
                    <li class="nav-link-button">
                        <a class="dark-on-light nav-links btn btn-black login-btn" data-event-name="Nav: Login" href="/login" style="margin-top:-5px;">LOG IN</a>
                    </li>
                    <li class="nav-link-button">
                        <a class="dark-on-light nav-links btn btn-white login-btn" data-event-name="Nav: Register" href="/register" style="margin-top:-5px; margin-left:20px">REGISTER</a>
                    </li>
                    </div>
                @else
                    <li class="position-relative" id="products-nav-link" style="min-width:137px">
                        <a href="/home" style="width:100%; justify-content: flex-start; font-weight: 700;" class="nav-links dark-on-light userprofile btn bg-none @if(Route::current()->getName() == 'home')bg-active @endif text-left" data-event-name="Nav: Products">Boarding Pass</a>
                    </li>
                @endguest
                    <li class="position-relative d-xs-block d-sm-block d-md-block d-xl-none" id="" style="min-width:137px">
                        <a href="/about" style="width:100%" class="nav-links dark-on-light userprofile btn btn-white bg-none @if(Route::current()->getName() == 'home')bg-active @endif text-left" data-event-name="Nav: Products">About</a>
                    </li>
                    <li class="position-relative d-xs-block d-sm-block d-md-block d-xl-none" id="" style="min-width:137px">
                        <a href="/contact" style="width:100%" class="nav-links dark-on-light userprofile btn btn-white bg-none @if(Route::current()->getName() == 'home')bg-active @endif text-left" data-event-name="Nav: Products">Contact Us</a>
                    </li>
                    <li class="position-relative d-xs-block d-sm-block d-md-block d-xl-none" id="" style="min-width:137px">
                        <a href="/terms" style="width:100%" class="nav-links dark-on-light userprofile btn btn-white bg-none @if(Route::current()->getName() == 'home')bg-active @endif text-left" data-event-name="Nav: Products">Terms & Privacy Policy</a>
                    </li>
            </ul>
            @endif
        </div>
        </div>
        <a href="javascript:void(0)" class="hamburger-menu right-nav dark-on-light"  aria-label="hamburger menu" id="hamburger-menu" style="margin-top:-8px">
            <span class="fa fa-bars" ></span>
        </a>
        
    </div>
  </div>
</nav>
@if(request()->is('home'))
<ribbon style="font-size: 14px; padding: 5px 0; bottom: 0; left: 0; position: fixed; width:100%; height: 63px; background:white;  flex-direction:row;z-index: 99; justify-content: space-around;" class="d-flex d-xs-flex d-sm-flex d-md-flex d-lg-none d-xl-none" role="tablist">
    <a class="text-center nav-link active" href="#" id="pills-home-tab" data-bs-toggle="pill" data-bs-target="#pills-home" type="button" role="tab" aria-controls="pills-home" aria-selected="true"><i class="fa fa-home"></i><span class="d-block">Home</span></a>
    <button class="text-center nav-link btn btn-white pt-0" style="height:fit-content; border:0;" href="#" id="pills-funding-tab-mobile" data-bs-toggle="pill" data-bs-target="#pills-funding" type="button" role="tab" aria-controls="pills-funding" aria-selected="false"><i class="fa fa-bank"></i><span class="d-block">Accounts</span></button>
    <a class="text-center nav-link" href="#" id="pills-orders-tab" data-bs-toggle="pill" data-bs-target="#pills-orders" type="button" role="tab" aria-controls="pills-orders" aria-selected="false"><i class="fa fa-money-check-dollar"></i><span class="d-block">Ledger</span></a>
    <a class="text-center nav-link" href="#" id="pills-settings-tab" data-bs-toggle="pill" data-bs-target="#pills-settings" type="button" role="tab" aria-controls="pills-settings" aria-selected="false"><i class="fa fa-user"></i><span class="d-block">Profile</span></a>
</ribbon>
@endif