@extends('layouts.loan-master')

@section('hero')
	@if($goal == "complete")
		<section id="cover">
		    <div class="container">
		        <div class="row">
		            <div class="col-md-7 cover-image align-self-center">
		                <img src="/assets/loan/img/venti-loan-hero-square.jpeg" class="img-fluid" alt="product">
		            </div>
		            <div class="col-md-5 align-self-center cover-body">
		                <h1>Success!</h1>
		                <p>Your Information has been received! The Black Card program is very selective, and we will stay in touch over email regarding the results of this pre-qualification form. We hope you're one of the lucky ones!</p>
		            </div>
		        </div>
		    </div>
		</section>
	@else
		<style type="text/css">
		#footer{
			display: none;
		}
		</style>
	@endif
@endsection

@section('js')
	<script type="text/javascript">
		@if($goal != "complete")
			@if($goal == "waitlisted")
				fbq('track', 'Subscribe');
				setTimeout(() => {  window.location.href = "/waitlisted"; }, 600);
			@endif
			@if($goal == "gold" || $goal == "black")
				fbq('track', 'SubmitApplication');
				setTimeout(() => {  window.location.href = "/success"; }, 600);
			@endif
		@endif
	</script>
@endsection