<script type="application/javascript">var qodefToursAjaxURL = "https://setsail.qodeinteractive.com/wp-admin/admin-ajax.php"</script>
      <meta name='robots' content='max-image-preview:large' />
      <script data-cfasync="false" data-pagespeed-no-defer>//<![CDATA[
         var gtm4wp_datalayer_name = "dataLayer";
         var dataLayer = dataLayer || [];
         //]]>
      </script>
      <link rel='dns-prefetch' href='//export.qodethemes.com' />
      <link rel='dns-prefetch' href='//apis.google.com' />
      <link rel='dns-prefetch' href='//maps.googleapis.com' />
      <link rel='dns-prefetch' href='//static.zdassets.com' />
      <link rel='dns-prefetch' href='//fonts.googleapis.com' />
      <link rel='dns-prefetch' href='//s.w.org' />
      <link rel="alternate" type="application/rss+xml" title="SetSail &raquo; Feed" href="https://setsail.qodeinteractive.com/feed/" />
      <link rel="alternate" type="application/rss+xml" title="SetSail &raquo; Comments Feed" href="https://setsail.qodeinteractive.com/comments/feed/" />
      <script type="text/javascript">
         window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.1\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/setsail.qodeinteractive.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.7"}};
         !function(e,a,t){var n,r,o,i=a.createElement("canvas"),p=i.getContext&&i.getContext("2d");function s(e,t){var a=String.fromCharCode;p.clearRect(0,0,i.width,i.height),p.fillText(a.apply(this,e),0,0);e=i.toDataURL();return p.clearRect(0,0,i.width,i.height),p.fillText(a.apply(this,t),0,0),e===i.toDataURL()}function c(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(o=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},r=0;r<o.length;r++)t.supports[o[r]]=function(e){if(!p||!p.fillText)return!1;switch(p.textBaseline="top",p.font="600 32px Arial",e){case"flag":return s([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])?!1:!s([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!s([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]);case"emoji":return!s([55357,56424,8205,55356,57212],[55357,56424,8203,55356,57212])}return!1}(o[r]),t.supports.everything=t.supports.everything&&t.supports[o[r]],"flag"!==o[r]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[o[r]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(n=t.source||{}).concatemoji?c(n.concatemoji):n.wpemoji&&n.twemoji&&(c(n.twemoji),c(n.wpemoji)))}(window,document,window._wpemojiSettings);
      </script>
      <style type="text/css">
         img.wp-smiley,
         img.emoji {
         display: inline !important;
         border: none !important;
         box-shadow: none !important;
         height: 1em !important;
         width: 1em !important;
         margin: 0 .07em !important;
         vertical-align: -0.1em !important;
         background: none !important;
         padding: 0 !important;
         }
      </style>
      <link rel='stylesheet' id='wp-block-library-css' href='https://setsail.qodeinteractive.com/wp-includes/css/dist/block-library/style.min.css?ver=5.7' type='text/css' media='all' />
      <link rel='stylesheet' id='wc-block-vendors-style-css' href='https://setsail.qodeinteractive.com/wp-content/plugins/woocommerce/packages/woocommerce-blocks/build/vendors-style.css?ver=4.4.3' type='text/css' media='all' />
      <link rel='stylesheet' id='wc-block-style-css' href='https://setsail.qodeinteractive.com/wp-content/plugins/woocommerce/packages/woocommerce-blocks/build/style.css?ver=4.4.3' type='text/css' media='all' />
      <link rel='stylesheet' id='titan-adminbar-styles-css' href='https://setsail.qodeinteractive.com/wp-content/plugins/anti-spam/assets/css/admin-bar.css?ver=7.2.7' type='text/css' media='all' />
      <link rel='stylesheet' id='contact-form-7-css' href='https://setsail.qodeinteractive.com/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=5.4' type='text/css' media='all' />
      <link rel='stylesheet' id='rabbit_css-css' href='https://export.qodethemes.com/_toolbar/assets/css/rbt-modules.css?ver=5.7' type='text/css' media='all' />
      <link rel='stylesheet' id='rs-plugin-settings-css' href='https://setsail.qodeinteractive.com/wp-content/plugins/revslider/public/assets/css/rs6.css?ver=6.4.6' type='text/css' media='all' />
      <style id='rs-plugin-settings-inline-css' type='text/css'>
         #rs-demo-id {}
      </style>
      <link rel='stylesheet' id='setsail-membership-style-css' href='https://setsail.qodeinteractive.com/wp-content/plugins/setsail-membership/assets/css/membership.min.css?ver=5.7' type='text/css' media='all' />
      <link rel='stylesheet' id='setsail-membership-responsive-style-css' href='https://setsail.qodeinteractive.com/wp-content/plugins/setsail-membership/assets/css/membership-responsive.min.css?ver=5.7' type='text/css' media='all' />
      <link rel='stylesheet' id='setsail-select-modules-css' href='https://setsail.qodeinteractive.com/wp-content/themes/setsail/assets/css/modules.min.css?ver=5.7' type='text/css' media='all' />
      <link rel='stylesheet' id='setsail-tours-style-css' href='https://setsail.qodeinteractive.com/wp-content/plugins/setsail-tours/assets/css/tours.min.css?ver=5.7' type='text/css' media='all' />
      <link rel='stylesheet' id='setsail-select-modules-responsive-css' href='https://setsail.qodeinteractive.com/wp-content/themes/setsail/assets/css/modules-responsive.min.css?ver=5.7' type='text/css' media='all' />
      <link rel='stylesheet' id='setsail-tours-responsive-style-css' href='https://setsail.qodeinteractive.com/wp-content/plugins/setsail-tours/assets/css/tours-responsive.min.css?ver=5.7' type='text/css' media='all' />
      <link rel='stylesheet' id='nouislider-css' href='https://setsail.qodeinteractive.com/wp-content/plugins/setsail-tours/assets/css/nouislider.min.css?ver=5.7' type='text/css' media='all' />
      <style id='woocommerce-inline-inline-css' type='text/css'>
         .woocommerce form .form-row .required { visibility: visible; }
      </style>
      <link rel='stylesheet' id='swiper-css' href='https://setsail.qodeinteractive.com/wp-content/plugins/qi-addons-for-elementor/assets/plugins/swiper/swiper.min.css?ver=5.7' type='text/css' media='all' />
      <link rel='stylesheet' id='qi-addons-for-elementor-grid-style-css' href='https://setsail.qodeinteractive.com/wp-content/plugins/qi-addons-for-elementor/assets/css/grid.min.css?ver=5.7' type='text/css' media='all' />
      <link rel='stylesheet' id='qi-addons-for-elementor-helper-parts-style-css' href='https://setsail.qodeinteractive.com/wp-content/plugins/qi-addons-for-elementor/assets/css/helper-parts.min.css?ver=5.7' type='text/css' media='all' />
      <link rel='stylesheet' id='qi-addons-for-elementor-style-css' href='https://setsail.qodeinteractive.com/wp-content/plugins/qi-addons-for-elementor/assets/css/main.min.css?ver=5.7' type='text/css' media='all' />
      <link rel='stylesheet' id='setsail-select-default-style-css' href='https://setsail.qodeinteractive.com/wp-content/themes/setsail/style.css?ver=5.7' type='text/css' media='all' />
      <link rel='stylesheet' id='qodef-dripicons-css' href='https://setsail.qodeinteractive.com/wp-content/themes/setsail/framework/lib/icons-pack/dripicons/dripicons.css?ver=5.7' type='text/css' media='all' />
      <link rel='stylesheet' id='qodef-font_elegant-css' href='https://setsail.qodeinteractive.com/wp-content/themes/setsail/framework/lib/icons-pack/elegant-icons/style.min.css?ver=5.7' type='text/css' media='all' />
      <link rel='stylesheet' id='qodef-font_awesome-css' href='https://setsail.qodeinteractive.com/wp-content/themes/setsail/framework/lib/icons-pack/font-awesome/css/fontawesome-all.min.css?ver=5.7' type='text/css' media='all' />
      <link rel='stylesheet' id='qodef-ion_icons-css' href='https://setsail.qodeinteractive.com/wp-content/themes/setsail/framework/lib/icons-pack/ion-icons/css/ionicons.min.css?ver=5.7' type='text/css' media='all' />
      <link rel='stylesheet' id='qodef-linea_icons-css' href='https://setsail.qodeinteractive.com/wp-content/themes/setsail/framework/lib/icons-pack/linea-icons/style.css?ver=5.7' type='text/css' media='all' />
      <link rel='stylesheet' id='qodef-linear_icons-css' href='https://setsail.qodeinteractive.com/wp-content/themes/setsail/framework/lib/icons-pack/linear-icons/style.css?ver=5.7' type='text/css' media='all' />
      <link rel='stylesheet' id='qodef-simple_line_icons-css' href='https://setsail.qodeinteractive.com/wp-content/themes/setsail/framework/lib/icons-pack/simple-line-icons/simple-line-icons.css?ver=5.7' type='text/css' media='all' />
      <link rel='stylesheet' id='mediaelement-css' href='https://setsail.qodeinteractive.com/wp-includes/js/mediaelement/mediaelementplayer-legacy.min.css?ver=4.2.16' type='text/css' media='all' />
      <link rel='stylesheet' id='wp-mediaelement-css' href='https://setsail.qodeinteractive.com/wp-includes/js/mediaelement/wp-mediaelement.min.css?ver=5.7' type='text/css' media='all' />
      <link rel='stylesheet' id='setsail-select-woo-css' href='https://setsail.qodeinteractive.com/wp-content/themes/setsail/assets/css/woocommerce.min.css?ver=5.7' type='text/css' media='all' />
      <style id='setsail-select-woo-inline-css' type='text/css'>
         .page-id-4515 .qodef-page-header .qodef-menu-area { background-color: rgba(255, 255, 255, 0);}
      </style>
      <link rel='stylesheet' id='setsail-select-woo-responsive-css' href='https://setsail.qodeinteractive.com/wp-content/themes/setsail/assets/css/woocommerce-responsive.min.css?ver=5.7' type='text/css' media='all' />
      <link rel='stylesheet' id='setsail-select-style-dynamic-css' href='https://setsail.qodeinteractive.com/wp-content/themes/setsail/assets/css/style_dynamic.css?ver=1583314560' type='text/css' media='all' />
      <link rel='stylesheet' id='setsail-select-style-dynamic-responsive-css' href='https://setsail.qodeinteractive.com/wp-content/themes/setsail/assets/css/style_dynamic_responsive.css?ver=1583314560' type='text/css' media='all' />
      <style id="setsail-select-google-fonts-css" media="all">/* tamil */
         @font-face {
         font-family: 'Catamaran';
         font-style: normal;
         font-weight: 300;
         src: url(/fonts.gstatic.com/s/catamaran/v8/o-0IIpQoyXQa2RxT7-5r8zRASf6M7VBj.woff2) format('woff2');
         unicode-range: U+0964-0965, U+0B82-0BFA, U+200C-200D, U+20B9, U+25CC;
         }
         /* latin-ext */
         @font-face {
         font-family: 'Catamaran';
         font-style: normal;
         font-weight: 300;
         src: url(/fonts.gstatic.com/s/catamaran/v8/o-0IIpQoyXQa2RxT7-5r6zRASf6M7VBj.woff2) format('woff2');
         unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
         }
         /* latin */
         @font-face {
         font-family: 'Catamaran';
         font-style: normal;
         font-weight: 300;
         src: url(/fonts.gstatic.com/s/catamaran/v8/o-0IIpQoyXQa2RxT7-5r5TRASf6M7Q.woff2) format('woff2');
         unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
         }
         /* tamil */
         @font-face {
         font-family: 'Catamaran';
         font-style: normal;
         font-weight: 400;
         src: url(/fonts.gstatic.com/s/catamaran/v8/o-0IIpQoyXQa2RxT7-5r8zRASf6M7VBj.woff2) format('woff2');
         unicode-range: U+0964-0965, U+0B82-0BFA, U+200C-200D, U+20B9, U+25CC;
         }
         /* latin-ext */
         @font-face {
         font-family: 'Catamaran';
         font-style: normal;
         font-weight: 400;
         src: url(/fonts.gstatic.com/s/catamaran/v8/o-0IIpQoyXQa2RxT7-5r6zRASf6M7VBj.woff2) format('woff2');
         unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
         }
         /* latin */
         @font-face {
         font-family: 'Catamaran';
         font-style: normal;
         font-weight: 400;
         src: url(/fonts.gstatic.com/s/catamaran/v8/o-0IIpQoyXQa2RxT7-5r5TRASf6M7Q.woff2) format('woff2');
         unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
         }
         /* tamil */
         @font-face {
         font-family: 'Catamaran';
         font-style: normal;
         font-weight: 500;
         src: url(/fonts.gstatic.com/s/catamaran/v8/o-0IIpQoyXQa2RxT7-5r8zRASf6M7VBj.woff2) format('woff2');
         unicode-range: U+0964-0965, U+0B82-0BFA, U+200C-200D, U+20B9, U+25CC;
         }
         /* latin-ext */
         @font-face {
         font-family: 'Catamaran';
         font-style: normal;
         font-weight: 500;
         src: url(/fonts.gstatic.com/s/catamaran/v8/o-0IIpQoyXQa2RxT7-5r6zRASf6M7VBj.woff2) format('woff2');
         unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
         }
         /* latin */
         @font-face {
         font-family: 'Catamaran';
         font-style: normal;
         font-weight: 500;
         src: url(/fonts.gstatic.com/s/catamaran/v8/o-0IIpQoyXQa2RxT7-5r5TRASf6M7Q.woff2) format('woff2');
         unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
         }
         /* tamil */
         @font-face {
         font-family: 'Catamaran';
         font-style: normal;
         font-weight: 600;
         src: url(/fonts.gstatic.com/s/catamaran/v8/o-0IIpQoyXQa2RxT7-5r8zRASf6M7VBj.woff2) format('woff2');
         unicode-range: U+0964-0965, U+0B82-0BFA, U+200C-200D, U+20B9, U+25CC;
         }
         /* latin-ext */
         @font-face {
         font-family: 'Catamaran';
         font-style: normal;
         font-weight: 600;
         src: url(/fonts.gstatic.com/s/catamaran/v8/o-0IIpQoyXQa2RxT7-5r6zRASf6M7VBj.woff2) format('woff2');
         unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
         }
         /* latin */
         @font-face {
         font-family: 'Catamaran';
         font-style: normal;
         font-weight: 600;
         src: url(/fonts.gstatic.com/s/catamaran/v8/o-0IIpQoyXQa2RxT7-5r5TRASf6M7Q.woff2) format('woff2');
         unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
         }
         /* tamil */
         @font-face {
         font-family: 'Catamaran';
         font-style: normal;
         font-weight: 700;
         src: url(/fonts.gstatic.com/s/catamaran/v8/o-0IIpQoyXQa2RxT7-5r8zRASf6M7VBj.woff2) format('woff2');
         unicode-range: U+0964-0965, U+0B82-0BFA, U+200C-200D, U+20B9, U+25CC;
         }
         /* latin-ext */
         @font-face {
         font-family: 'Catamaran';
         font-style: normal;
         font-weight: 700;
         src: url(/fonts.gstatic.com/s/catamaran/v8/o-0IIpQoyXQa2RxT7-5r6zRASf6M7VBj.woff2) format('woff2');
         unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
         }
         /* latin */
         @font-face {
         font-family: 'Catamaran';
         font-style: normal;
         font-weight: 700;
         src: url(/fonts.gstatic.com/s/catamaran/v8/o-0IIpQoyXQa2RxT7-5r5TRASf6M7Q.woff2) format('woff2');
         unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
         }
         /* tamil */
         @font-face {
         font-family: 'Catamaran';
         font-style: normal;
         font-weight: 800;
         src: url(/fonts.gstatic.com/s/catamaran/v8/o-0IIpQoyXQa2RxT7-5r8zRASf6M7VBj.woff2) format('woff2');
         unicode-range: U+0964-0965, U+0B82-0BFA, U+200C-200D, U+20B9, U+25CC;
         }
         /* latin-ext */
         @font-face {
         font-family: 'Catamaran';
         font-style: normal;
         font-weight: 800;
         src: url(/fonts.gstatic.com/s/catamaran/v8/o-0IIpQoyXQa2RxT7-5r6zRASf6M7VBj.woff2) format('woff2');
         unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
         }
         /* latin */
         @font-face {
         font-family: 'Catamaran';
         font-style: normal;
         font-weight: 800;
         src: url(/fonts.gstatic.com/s/catamaran/v8/o-0IIpQoyXQa2RxT7-5r5TRASf6M7Q.woff2) format('woff2');
         unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
         }
         /* devanagari */
         @font-face {
         font-family: 'Poppins';
         font-style: normal;
         font-weight: 300;
         src: url(/fonts.gstatic.com/s/poppins/v15/pxiByp8kv8JHgFVrLDz8Z11lFd2JQEl8qw.woff2) format('woff2');
         unicode-range: U+0900-097F, U+1CD0-1CF6, U+1CF8-1CF9, U+200C-200D, U+20A8, U+20B9, U+25CC, U+A830-A839, U+A8E0-A8FB;
         }
         /* latin-ext */
         @font-face {
         font-family: 'Poppins';
         font-style: normal;
         font-weight: 300;
         src: url(/fonts.gstatic.com/s/poppins/v15/pxiByp8kv8JHgFVrLDz8Z1JlFd2JQEl8qw.woff2) format('woff2');
         unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
         }
         /* latin */
         @font-face {
         font-family: 'Poppins';
         font-style: normal;
         font-weight: 300;
         src: url(/fonts.gstatic.com/s/poppins/v15/pxiByp8kv8JHgFVrLDz8Z1xlFd2JQEk.woff2) format('woff2');
         unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
         }
         /* devanagari */
         @font-face {
         font-family: 'Poppins';
         font-style: normal;
         font-weight: 400;
         src: url(/fonts.gstatic.com/s/poppins/v15/pxiEyp8kv8JHgFVrJJbecnFHGPezSQ.woff2) format('woff2');
         unicode-range: U+0900-097F, U+1CD0-1CF6, U+1CF8-1CF9, U+200C-200D, U+20A8, U+20B9, U+25CC, U+A830-A839, U+A8E0-A8FB;
         }
         /* latin-ext */
         @font-face {
         font-family: 'Poppins';
         font-style: normal;
         font-weight: 400;
         src: url(/fonts.gstatic.com/s/poppins/v15/pxiEyp8kv8JHgFVrJJnecnFHGPezSQ.woff2) format('woff2');
         unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
         }
         /* latin */
         @font-face {
         font-family: 'Poppins';
         font-style: normal;
         font-weight: 400;
         src: url(/fonts.gstatic.com/s/poppins/v15/pxiEyp8kv8JHgFVrJJfecnFHGPc.woff2) format('woff2');
         unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
         }
         /* devanagari */
         @font-face {
         font-family: 'Poppins';
         font-style: normal;
         font-weight: 500;
         src: url(/fonts.gstatic.com/s/poppins/v15/pxiByp8kv8JHgFVrLGT9Z11lFd2JQEl8qw.woff2) format('woff2');
         unicode-range: U+0900-097F, U+1CD0-1CF6, U+1CF8-1CF9, U+200C-200D, U+20A8, U+20B9, U+25CC, U+A830-A839, U+A8E0-A8FB;
         }
         /* latin-ext */
         @font-face {
         font-family: 'Poppins';
         font-style: normal;
         font-weight: 500;
         src: url(/fonts.gstatic.com/s/poppins/v15/pxiByp8kv8JHgFVrLGT9Z1JlFd2JQEl8qw.woff2) format('woff2');
         unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
         }
         /* latin */
         @font-face {
         font-family: 'Poppins';
         font-style: normal;
         font-weight: 500;
         src: url(/fonts.gstatic.com/s/poppins/v15/pxiByp8kv8JHgFVrLGT9Z1xlFd2JQEk.woff2) format('woff2');
         unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
         }
         /* devanagari */
         @font-face {
         font-family: 'Poppins';
         font-style: normal;
         font-weight: 600;
         src: url(/fonts.gstatic.com/s/poppins/v15/pxiByp8kv8JHgFVrLEj6Z11lFd2JQEl8qw.woff2) format('woff2');
         unicode-range: U+0900-097F, U+1CD0-1CF6, U+1CF8-1CF9, U+200C-200D, U+20A8, U+20B9, U+25CC, U+A830-A839, U+A8E0-A8FB;
         }
         /* latin-ext */
         @font-face {
         font-family: 'Poppins';
         font-style: normal;
         font-weight: 600;
         src: url(/fonts.gstatic.com/s/poppins/v15/pxiByp8kv8JHgFVrLEj6Z1JlFd2JQEl8qw.woff2) format('woff2');
         unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
         }
         /* latin */
         @font-face {
         font-family: 'Poppins';
         font-style: normal;
         font-weight: 600;
         src: url(/fonts.gstatic.com/s/poppins/v15/pxiByp8kv8JHgFVrLEj6Z1xlFd2JQEk.woff2) format('woff2');
         unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
         }
         /* devanagari */
         @font-face {
         font-family: 'Poppins';
         font-style: normal;
         font-weight: 700;
         src: url(/fonts.gstatic.com/s/poppins/v15/pxiByp8kv8JHgFVrLCz7Z11lFd2JQEl8qw.woff2) format('woff2');
         unicode-range: U+0900-097F, U+1CD0-1CF6, U+1CF8-1CF9, U+200C-200D, U+20A8, U+20B9, U+25CC, U+A830-A839, U+A8E0-A8FB;
         }
         /* latin-ext */
         @font-face {
         font-family: 'Poppins';
         font-style: normal;
         font-weight: 700;
         src: url(/fonts.gstatic.com/s/poppins/v15/pxiByp8kv8JHgFVrLCz7Z1JlFd2JQEl8qw.woff2) format('woff2');
         unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
         }
         /* latin */
         @font-face {
         font-family: 'Poppins';
         font-style: normal;
         font-weight: 700;
         src: url(/fonts.gstatic.com/s/poppins/v15/pxiByp8kv8JHgFVrLCz7Z1xlFd2JQEk.woff2) format('woff2');
         unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
         }
         /* devanagari */
         @font-face {
         font-family: 'Poppins';
         font-style: normal;
         font-weight: 800;
         src: url(/fonts.gstatic.com/s/poppins/v15/pxiByp8kv8JHgFVrLDD4Z11lFd2JQEl8qw.woff2) format('woff2');
         unicode-range: U+0900-097F, U+1CD0-1CF6, U+1CF8-1CF9, U+200C-200D, U+20A8, U+20B9, U+25CC, U+A830-A839, U+A8E0-A8FB;
         }
         /* latin-ext */
         @font-face {
         font-family: 'Poppins';
         font-style: normal;
         font-weight: 800;
         src: url(/fonts.gstatic.com/s/poppins/v15/pxiByp8kv8JHgFVrLDD4Z1JlFd2JQEl8qw.woff2) format('woff2');
         unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
         }
         /* latin */
         @font-face {
         font-family: 'Poppins';
         font-style: normal;
         font-weight: 800;
         src: url(/fonts.gstatic.com/s/poppins/v15/pxiByp8kv8JHgFVrLDD4Z1xlFd2JQEk.woff2) format('woff2');
         unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
         }
         /* latin */
         @font-face {
         font-family: 'Satisfy';
         font-style: normal;
         font-weight: 400;
         src: url(/fonts.gstatic.com/s/satisfy/v11/rP2Hp2yn6lkG50LoCZOIHTWEBlw.woff2) format('woff2');
         unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
         }
      </style>
      <link rel='stylesheet' id='setsail-core-dashboard-style-css' href='https://setsail.qodeinteractive.com/wp-content/plugins/setsail-core/core-dashboard/assets/css/core-dashboard.min.css?ver=5.7' type='text/css' media='all' />
      <link rel='stylesheet' id='js_composer_front-css' href='https://setsail.qodeinteractive.com/wp-content/plugins/js_composer/assets/css/js_composer.min.css?ver=6.6.0' type='text/css' media='all' />
      <link rel='stylesheet' id='qode-zendesk-chat-css' href='https://setsail.qodeinteractive.com/wp-content/plugins/qode-zendesk-chat//assets/main.css?ver=5.7' type='text/css' media='all' />
      <script type='text/javascript' src='https://setsail.qodeinteractive.com/wp-includes/js/jquery/jquery.min.js?ver=3.5.1' id='jquery-core-js'></script>
      <script type='text/javascript' src='https://setsail.qodeinteractive.com/wp-includes/js/jquery/jquery-migrate.min.js?ver=3.3.2' id='jquery-migrate-js'></script>
      <script type='text/javascript' src='https://setsail.qodeinteractive.com/wp-content/plugins/revslider/public/assets/js/rbtools.min.js?ver=6.4.4' id='tp-tools-js'></script>
      <script type='text/javascript' src='https://setsail.qodeinteractive.com/wp-content/plugins/revslider/public/assets/js/rs6.min.js?ver=6.4.6' id='revmin-js'></script>
      <script type='text/javascript' src='https://apis.google.com/js/platform.js' id='setsail-membership-google-plus-api-js'></script>
      <script type='text/javascript' src='https://setsail.qodeinteractive.com/wp-content/plugins/woocommerce/assets/js/jquery-blockui/jquery.blockUI.min.js?ver=2.70' id='jquery-blockui-js'></script>
      <script type='text/javascript' id='wc-add-to-cart-js-extra'>
         /* <![CDATA[ */
         var wc_add_to_cart_params = {"ajax_url":"\/wp-admin\/admin-ajax.php","wc_ajax_url":"\/?wc-ajax=%%endpoint%%","i18n_view_cart":"View cart","cart_url":"https:\/\/setsail.qodeinteractive.com\/cart\/","is_cart":"","cart_redirect_after_add":"no"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='https://setsail.qodeinteractive.com/wp-content/plugins/woocommerce/assets/js/frontend/add-to-cart.min.js?ver=5.1.0' id='wc-add-to-cart-js'></script>
      <script type='text/javascript' src='https://setsail.qodeinteractive.com/wp-content/plugins/duracelltomi-google-tag-manager/js/gtm4wp-form-move-tracker.js?ver=1.11.6' id='gtm4wp-form-move-tracker-js'></script>
      <script type='text/javascript' src='https://setsail.qodeinteractive.com/wp-content/plugins/js_composer/assets/js/vendors/woocommerce-add-to-cart.js?ver=6.6.0' id='vc_woocommerce-add-to-cart-js-js'></script>
      <link rel="https://api.w.org/" href="https://setsail.qodeinteractive.com/wp-json/" />
      <link rel="alternate" type="application/json" href="https://setsail.qodeinteractive.com/wp-json/wp/v2/pages/4515" />
      <link rel="EditURI" type="application/rsd+xml" title="RSD" href="https://setsail.qodeinteractive.com/xmlrpc.php?rsd" />
      <link rel="wlwmanifest" type="application/wlwmanifest+xml" href="https://setsail.qodeinteractive.com/wp-includes/wlwmanifest.xml" />
      <meta name="generator" content="WordPress 5.7" />
      <meta name="generator" content="WooCommerce 5.1.0" />
      <link rel="canonical" href="https://setsail.qodeinteractive.com/vacation-showcase/" />
      <link rel='shortlink' href='https://setsail.qodeinteractive.com/?p=4515' />
      <link rel="alternate" type="application/json+oembed" href="https://setsail.qodeinteractive.com/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fsetsail.qodeinteractive.com%2Fvacation-showcase%2F" />
      <link rel="alternate" type="text/xml+oembed" href="https://setsail.qodeinteractive.com/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fsetsail.qodeinteractive.com%2Fvacation-showcase%2F&#038;format=xml" />
      <script data-cfasync="false" data-pagespeed-no-defer>//<![CDATA[
         var dataLayer_content = {"pagePostType":"page","pagePostType2":"single-page","pagePostAuthor":"admin"};
         dataLayer.push( dataLayer_content );//]]>
      </script>
      <noscript>
         <style>.woocommerce-product-gallery{ opacity: 1 !important; }</style>
      </noscript>
      <meta name="generator" content="Powered by WPBakery Page Builder - drag and drop page builder for WordPress." />
      <meta name="generator" content="Powered by Slider Revolution 6.4.6 - responsive, Mobile-Friendly Slider Plugin for WordPress with comfortable drag and drop interface." />
      <link rel="icon" href="/assets/img/venti-favicon.png" sizes="32x32" />
      <link rel="icon" href="/assets/img/venti-favicon.png" sizes="192x192" />
      <link rel="apple-touch-icon" href="/assets/img/venti-favicon.png" />
      <meta name="msapplication-TileImage" content="/assets/img/venti-favicon.png" />
      <script type="text/javascript">function setREVStartSize(e){
         //window.requestAnimationFrame(function() {              
             window.RSIW = window.RSIW===undefined ? window.innerWidth : window.RSIW;    
             window.RSIH = window.RSIH===undefined ? window.innerHeight : window.RSIH;   
             try {                               
                 var pw = document.getElementById(e.c).parentNode.offsetWidth,
                     newh;
                 pw = pw===0 || isNaN(pw) ? window.RSIW : pw;
                 e.tabw = e.tabw===undefined ? 0 : parseInt(e.tabw);
                 e.thumbw = e.thumbw===undefined ? 0 : parseInt(e.thumbw);
                 e.tabh = e.tabh===undefined ? 0 : parseInt(e.tabh);
                 e.thumbh = e.thumbh===undefined ? 0 : parseInt(e.thumbh);
                 e.tabhide = e.tabhide===undefined ? 0 : parseInt(e.tabhide);
                 e.thumbhide = e.thumbhide===undefined ? 0 : parseInt(e.thumbhide);
                 e.mh = e.mh===undefined || e.mh=="" || e.mh==="auto" ? 0 : parseInt(e.mh,0);        
                 if(e.layout==="fullscreen" || e.l==="fullscreen")                       
                     newh = Math.max(e.mh,window.RSIH);                  
                 else{                   
                     e.gw = Array.isArray(e.gw) ? e.gw : [e.gw];
                     for (var i in e.rl) if (e.gw[i]===undefined || e.gw[i]===0) e.gw[i] = e.gw[i-1];                    
                     e.gh = e.el===undefined || e.el==="" || (Array.isArray(e.el) && e.el.length==0)? e.gh : e.el;
                     e.gh = Array.isArray(e.gh) ? e.gh : [e.gh];
                     for (var i in e.rl) if (e.gh[i]===undefined || e.gh[i]===0) e.gh[i] = e.gh[i-1];
                                         
                     var nl = new Array(e.rl.length),
                         ix = 0,                     
                         sl;                 
                     e.tabw = e.tabhide>=pw ? 0 : e.tabw;
                     e.thumbw = e.thumbhide>=pw ? 0 : e.thumbw;
                     e.tabh = e.tabhide>=pw ? 0 : e.tabh;
                     e.thumbh = e.thumbhide>=pw ? 0 : e.thumbh;                  
                     for (var i in e.rl) nl[i] = e.rl[i]<window.RSIW ? 0 : e.rl[i];
                     sl = nl[0];                                 
                     for (var i in nl) if (sl>nl[i] && nl[i]>0) { sl = nl[i]; ix=i;}                                                         
                     var m = pw>(e.gw[ix]+e.tabw+e.thumbw) ? 1 : (pw-(e.tabw+e.thumbw)) / (e.gw[ix]);                    
                     newh =  (e.gh[ix] * m) + (e.tabh + e.thumbh);
                 }               
                 if(window.rs_init_css===undefined) window.rs_init_css = document.head.appendChild(document.createElement("style"));                 
                 document.getElementById(e.c).height = newh+"px";
                 window.rs_init_css.innerHTML += "#"+e.c+"_wrapper { height: "+newh+"px }";              
             } catch(e){
                 console.log("Failure at Presize of Slider:" + e)
             }                      
         //});
         };
      </script>
      <style type="text/css" id="wp-custom-css">
         .qodef-woo-single-page .woocommerce-tabs #reviews .comment-respond .stars a.active:after, .qodef-woo-single-page .woocommerce-tabs #reviews .comment-respond .stars a:before{
         letter-spacing: 10px;
         }
         .qodef-woo-single-page .woocommerce-tabs #reviews .comment-respond .stars > span{
         font-size: 0;
         }
         .qodef-woo-single-page .woocommerce-tabs #reviews .comment-respond .stars a.active:after{
         bottom: 6px;
         }
         .qodef-parallax-row-holder {
         background-size: cover;
         }
         .qodef-row-grid-section-wrapper{
         background-size: cover;
         }
         .qodef-title-holder.qodef-bg-parallax {
         background-size: cover;
         }       
      </style>
      <style type="text/css" data-type="vc_custom-css">.qodef-fullscreen-menu-opened .qodef-logo-wrapper a img.qodef-normal-logo {
         opacity: 0;
         }
         .qodef-fullscreen-menu-opened .qodef-logo-wrapper a img.qodef-light-logo {
         opacity: 1;
         }
      </style>
      <noscript>
         <style> .wpb_animate_when_almost_visible { opacity: 1; }</style>
      </noscript>