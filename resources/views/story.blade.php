@extends("layouts.curator-master")

@section("css")
   <link href="/css/landing.css" rel="stylesheet">
   <style type="text/css">
      .card-header{
         height: 400px;
         width: 100%;
         background-size: cover;
         background-position: bottom;
         border-top-left-radius: 20px;
         border-top-right-radius: 20px;
      }
      .card-body, .tile-footer{
         background: white;
         padding: 20px;
      }
      .StripeElement {
         width: 100%;
         background-color: white;
         height: 40px;
         padding: 10px 12px;
         border-radius: 4px;
         border: 1px solid transparent;
         box-shadow: 0 1px 3px 0 #e6ebf1;
         -webkit-transition: box-shadow 150ms ease;
         transition: box-shadow 150ms ease;
      }

      .StripeElement--focus {
        box-shadow: 0 1px 3px 0 #cfd7df;
      }

      .StripeElement--invalid {
        border-color: #fa755a;
      }

      .StripeElement--webkit-autofill {
        background-color: #fefde5 !important;
      }
      .payment-table{
         width: 100%;
      }
      .payment-table > * {
         font-size: 18px;
      }
      .pills-tab{
         display: inline-block;
         width: auto;
         margin: 3px 2px;
         padding-right: 10px;
         padding-left: 10px;
         border-radius: 10px;
         background-color: #ecebea;
         font-size: 13px;
         line-height: 20px;
         font-weight: 400;
         text-align: center;
         -o-object-fit: fill;
         object-fit: fill;
      }
      .table td{

      }
      a.btn-primary {
          background-color: #1d7e89;
          border-color: #1d7e89;
          color: #fff;
       }
   </style>
@endsection

@section("content")

<div id="page-content">
    <div class="gradient-wrap">
      <!--  HERO -->
      <div class="black position-relative hero-section mb-4" style="margin: 20px 4em;">
         <div class="row">
            
         </div>
      </div>
    </div>
  </div>

@endsection

@section("js")
<script src="/js/lightbox/lightbox.min.js"></script>
@endsection