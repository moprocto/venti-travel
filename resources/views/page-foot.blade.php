
      <script type="text/html" id="wpb-modifications"></script> <script type="text/javascript">
         (function () {
             var c = document.body.className;
             c = c.replace(/woocommerce-no-js/, 'woocommerce-js');
             document.body.className = c;
         })();
      </script>
      <script type='text/javascript' src='https://setsail.qodeinteractive.com/wp-includes/js/dist/vendor/wp-polyfill.min.js?ver=7.4.4' id='wp-polyfill-js'></script>
      <script type='text/javascript' id='wp-polyfill-js-after'>
         ( 'fetch' in window ) || document.write( '<script src="https://setsail.qodeinteractive.com/wp-includes/js/dist/vendor/wp-polyfill-fetch.min.js?ver=3.0.0"></scr' + 'ipt>' );( document.contains ) || document.write( '<script src="https://setsail.qodeinteractive.com/wp-includes/js/dist/vendor/wp-polyfill-node-contains.min.js?ver=3.42.0"></scr' + 'ipt>' );( window.DOMRect ) || document.write( '<script src="https://setsail.qodeinteractive.com/wp-includes/js/dist/vendor/wp-polyfill-dom-rect.min.js?ver=3.42.0"></scr' + 'ipt>' );( window.URL && window.URL.prototype && window.URLSearchParams ) || document.write( '<script src="https://setsail.qodeinteractive.com/wp-includes/js/dist/vendor/wp-polyfill-url.min.js?ver=3.6.4"></scr' + 'ipt>' );( window.FormData && window.FormData.prototype.keys ) || document.write( '<script src="https://setsail.qodeinteractive.com/wp-includes/js/dist/vendor/wp-polyfill-formdata.min.js?ver=3.0.12"></scr' + 'ipt>' );( Element.prototype.matches && Element.prototype.closest ) || document.write( '<script src="https://setsail.qodeinteractive.com/wp-includes/js/dist/vendor/wp-polyfill-element-closest.min.js?ver=2.0.2"></scr' + 'ipt>' );( 'objectFit' in document.documentElement.style ) || document.write( '<script src="https://setsail.qodeinteractive.com/wp-includes/js/dist/vendor/wp-polyfill-object-fit.min.js?ver=2.3.4"></scr' + 'ipt>' );
      </script>
      <script type='text/javascript' src='https://setsail.qodeinteractive.com/wp-includes/js/dist/hooks.min.js?ver=50e23bed88bcb9e6e14023e9961698c1' id='wp-hooks-js'></script>
      <script type='text/javascript' src='https://setsail.qodeinteractive.com/wp-includes/js/dist/i18n.min.js?ver=db9a9a37da262883343e941c3731bc67' id='wp-i18n-js'></script>
      <script type='text/javascript' id='wp-i18n-js-after'>
         wp.i18n.setLocaleData( { 'text direction\u0004ltr': [ 'ltr' ] } );
      </script>
      <script type='text/javascript' src='https://setsail.qodeinteractive.com/wp-includes/js/dist/vendor/lodash.min.js?ver=4.17.19' id='lodash-js'></script>
      <script type='text/javascript' id='lodash-js-after'>
         window.lodash = _.noConflict();
      </script>
      <script type='text/javascript' src='https://setsail.qodeinteractive.com/wp-includes/js/dist/url.min.js?ver=0ac7e0472c46121366e7ce07244be1ac' id='wp-url-js'></script>
      <script type='text/javascript' id='wp-api-fetch-js-translations'>
         ( function( domain, translations ) {
             var localeData = translations.locale_data[ domain ] || translations.locale_data.messages;
             localeData[""].domain = domain;
             wp.i18n.setLocaleData( localeData, domain );
         } )( "default", { "locale_data": { "messages": { "": {} } } } );
      </script>
      <script type='text/javascript' src='https://setsail.qodeinteractive.com/wp-includes/js/dist/api-fetch.min.js?ver=a783d1f442d2abefc7d6dbd156a44561' id='wp-api-fetch-js'></script>
      <script type='text/javascript' id='wp-api-fetch-js-after'>
         wp.apiFetch.use( wp.apiFetch.createRootURLMiddleware( "https://setsail.qodeinteractive.com/wp-json/" ) );
         wp.apiFetch.nonceMiddleware = wp.apiFetch.createNonceMiddleware( "081fa7fb52" );
         wp.apiFetch.use( wp.apiFetch.nonceMiddleware );
         wp.apiFetch.use( wp.apiFetch.mediaUploadMiddleware );
         wp.apiFetch.nonceEndpoint = "https://setsail.qodeinteractive.com/wp-admin/admin-ajax.php?action=rest-nonce";
      </script>
      <script type='text/javascript' id='contact-form-7-js-extra'>
         /* <![CDATA[ */
         var wpcf7 = [];
         /* ]]> */
      </script>
      <script type='text/javascript' src='https://setsail.qodeinteractive.com/wp-content/plugins/contact-form-7/includes/js/index.js?ver=5.4' id='contact-form-7-js'></script>
      <script type='text/javascript' src='https://export.qodethemes.com/_toolbar/assets/js/rbt-modules.js?ver=5.7' id='rabbit_js-js'></script>
      <script type='text/javascript' src='https://setsail.qodeinteractive.com/wp-includes/js/underscore.min.js?ver=1.8.3' id='underscore-js'></script>
      <script type='text/javascript' src='https://setsail.qodeinteractive.com/wp-includes/js/jquery/ui/core.min.js?ver=1.12.1' id='jquery-ui-core-js'></script>
      <script type='text/javascript' src='https://setsail.qodeinteractive.com/wp-includes/js/jquery/ui/tabs.min.js?ver=1.12.1' id='jquery-ui-tabs-js'></script>
      <script type='text/javascript' id='setsail-select-modules-js-extra'>
         /* <![CDATA[ */
         var qodefGlobalVars = {"vars":{"qodefAddForAdminBar":0,"qodefElementAppearAmount":-100,"qodefAjaxUrl":"https:\/\/setsail.qodeinteractive.com\/wp-admin\/admin-ajax.php","sliderNavPrevArrow":"icon-arrows-left","sliderNavNextArrow":"icon-arrows-right","ppExpand":"Expand the image","ppNext":"Next","ppPrev":"Previous","ppClose":"Close","qodefStickyHeaderHeight":0,"qodefStickyHeaderTransparencyHeight":70,"qodefTopBarHeight":0,"qodefLogoAreaHeight":0,"qodefMenuAreaHeight":80,"qodefMobileHeaderHeight":70}};
         var qodefPerPageVars = {"vars":{"qodefMobileHeaderHeight":70,"qodefStickyScrollAmount":0,"qodefHeaderTransparencyHeight":0,"qodefHeaderVerticalWidth":0}};
         /* ]]> */
      </script>
      <script type='text/javascript' src='https://setsail.qodeinteractive.com/wp-content/themes/setsail/assets/js/modules.min.js?ver=5.7' id='setsail-select-modules-js'></script>
      <script type='text/javascript' id='setsail-membership-script-js-extra'>
         /* <![CDATA[ */
         var qodefSocialLoginVars = {"social":{"facebookAppId":"1897037670413390","googleClientId":""}};
         /* ]]> */
      </script>
      <script type='text/javascript' src='https://setsail.qodeinteractive.com/wp-content/plugins/setsail-membership/assets/js/membership.min.js?ver=5.7' id='setsail-membership-script-js'></script>
      <script type='text/javascript' src='https://setsail.qodeinteractive.com/wp-content/plugins/setsail-tours/assets/js/modules/plugins/nouislider.min.js?ver=5.7' id='nouislider-js'></script>
      <script type='text/javascript' src='https://setsail.qodeinteractive.com/wp-content/plugins/setsail-tours/assets/js/modules/plugins/typeahead.bundle.min.js?ver=5.7' id='typeahead-js'></script>
      <script type='text/javascript' src='https://setsail.qodeinteractive.com/wp-content/plugins/setsail-tours/assets/js/modules/plugins/bloodhound.min.js?ver=5.7' id='bloodhound-js'></script>
      <script type='text/javascript' src='https://setsail.qodeinteractive.com/wp-includes/js/jquery/ui/datepicker.min.js?ver=1.12.1' id='jquery-ui-datepicker-js'></script>
      <script type='text/javascript' id='jquery-ui-datepicker-js-after'>
         jQuery(document).ready(function(jQuery){jQuery.datepicker.setDefaults({"closeText":"Close","currentText":"Today","monthNames":["January","February","March","April","May","June","July","August","September","October","November","December"],"monthNamesShort":["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],"nextText":"Next","prevText":"Previous","dayNames":["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"],"dayNamesShort":["Sun","Mon","Tue","Wed","Thu","Fri","Sat"],"dayNamesMin":["S","M","T","W","T","F","S"],"dateFormat":"MM d, yy","firstDay":1,"isRTL":false});});
      </script>
      <script type='text/javascript' id='setsail-tours-script-js-extra'>
         /* <![CDATA[ */
         var qodefToursSearchData = {"tours":["Beautiful Holland","Temple Tour","Caving","Ubud","Tarragona","Seminyak","Madrid","Denpasar","Barcelona","Active Winter","Kids Ski School","Snow Surfing","Skiing In Alps","Magic Of  Italy","Winter Action","New Year In Toronto","New Year On The Lake","China Avio 17 Days","Best New Year Ever","Berlin Avio 7 Days","The Best New Year","Australia Avio 14 Days","London Avio 5 days","Italy Bus 8 Days","Krakow Bus 5 Days","Jungle Adventure","Island Paradise","Bali Lanterns","Local Wines","Villages","Champagne","Harvest","Camping","Wine Tasting","Wine Festival","Marbella","Toronto","Romantic Paris","Holland Canals","Monuments","Hudson Sailing","Shopping Tour","Summer Fun","Italy Tour","Varadero","Valencia","Seville","Milan","Rome","Vatican City","Corfu","Tainan","Taipei","Kaohsiung","Sydney Opera"],"destinations":["Hiking","Country Side","Wine Tour","Holiday","Far Places","Exotic Places","France","Switzerland","Slovenia","San Marino","China","Indonesia","India","Italy","Russia","Cuba","Egypt","New York","Japan","Bali","Australia","Spain","Netherlands","Greece","Taiwan"]};
         /* ]]> */
      </script>
      <script type='text/javascript' src='https://setsail.qodeinteractive.com/wp-content/plugins/setsail-tours/assets/js/tours.min.js?ver=5.7' id='setsail-tours-script-js'></script>
      <script type='text/javascript' src='https://setsail.qodeinteractive.com/wp-content/plugins/woocommerce/assets/js/js-cookie/js.cookie.min.js?ver=2.1.4' id='js-cookie-js'></script>
      <script type='text/javascript' id='woocommerce-js-extra'>
         /* <![CDATA[ */
         var woocommerce_params = {"ajax_url":"\/wp-admin\/admin-ajax.php","wc_ajax_url":"\/?wc-ajax=%%endpoint%%"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='https://setsail.qodeinteractive.com/wp-content/plugins/woocommerce/assets/js/frontend/woocommerce.min.js?ver=5.1.0' id='woocommerce-js'></script>
      <script type='text/javascript' id='wc-cart-fragments-js-extra'>
         /* <![CDATA[ */
         var wc_cart_fragments_params = {"ajax_url":"\/wp-admin\/admin-ajax.php","wc_ajax_url":"\/?wc-ajax=%%endpoint%%","cart_hash_key":"wc_cart_hash_3f86dcd62f9ffc319b72a234fcbc3060","fragment_name":"wc_fragments_3f86dcd62f9ffc319b72a234fcbc3060","request_timeout":"5000"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='https://setsail.qodeinteractive.com/wp-content/plugins/woocommerce/assets/js/frontend/cart-fragments.min.js?ver=5.1.0' id='wc-cart-fragments-js'></script>
      <script type='text/javascript' id='qi-addons-for-elementor-script-js-extra'>
         /* <![CDATA[ */
         var qodefQiAddonsGlobal = {"vars":{"adminBarHeight":0,"iconArrowLeft":"<svg  xmlns=\"http:\/\/www.w3.org\/2000\/svg\" xmlns:xlink=\"http:\/\/www.w3.org\/1999\/xlink\" x=\"0px\" y=\"0px\" viewBox=\"0 0 34.2 32.3\" xml:space=\"preserve\" style=\"stroke-width: 2;\"><line x1=\"0.5\" y1=\"16\" x2=\"33.5\" y2=\"16\"\/><line x1=\"0.3\" y1=\"16.5\" x2=\"16.2\" y2=\"0.7\"\/><line x1=\"0\" y1=\"15.4\" x2=\"16.2\" y2=\"31.6\"\/><\/svg>","iconArrowRight":"<svg  xmlns=\"http:\/\/www.w3.org\/2000\/svg\" xmlns:xlink=\"http:\/\/www.w3.org\/1999\/xlink\" x=\"0px\" y=\"0px\" viewBox=\"0 0 34.2 32.3\" xml:space=\"preserve\" style=\"stroke-width: 2;\"><line x1=\"0\" y1=\"16\" x2=\"33\" y2=\"16\"\/><line x1=\"17.3\" y1=\"0.7\" x2=\"33.2\" y2=\"16.5\"\/><line x1=\"17.3\" y1=\"31.6\" x2=\"33.5\" y2=\"15.4\"\/><\/svg>","iconClose":"<svg  xmlns=\"http:\/\/www.w3.org\/2000\/svg\" xmlns:xlink=\"http:\/\/www.w3.org\/1999\/xlink\" x=\"0px\" y=\"0px\" viewBox=\"0 0 9.1 9.1\" xml:space=\"preserve\"><g><path d=\"M8.5,0L9,0.6L5.1,4.5L9,8.5L8.5,9L4.5,5.1L0.6,9L0,8.5L4,4.5L0,0.6L0.6,0L4.5,4L8.5,0z\"\/><\/g><\/svg>"}};
         /* ]]> */
      </script>
      <script type='text/javascript' src='https://setsail.qodeinteractive.com/wp-content/plugins/qi-addons-for-elementor/assets/js/main.min.js?ver=5.7' id='qi-addons-for-elementor-script-js'></script>
      <script type='text/javascript' src='https://setsail.qodeinteractive.com/wp-includes/js/jquery/ui/accordion.min.js?ver=1.12.1' id='jquery-ui-accordion-js'></script>
      <script type='text/javascript' id='mediaelement-core-js-before'>
         var mejsL10n = {"language":"en","strings":{"mejs.download-file":"Download File","mejs.install-flash":"You are using a browser that does not have Flash player enabled or installed. Please turn on your Flash player plugin or download the latest version from https:\/\/get.adobe.com\/flashplayer\/","mejs.fullscreen":"Fullscreen","mejs.play":"Play","mejs.pause":"Pause","mejs.time-slider":"Time Slider","mejs.time-help-text":"Use Left\/Right Arrow keys to advance one second, Up\/Down arrows to advance ten seconds.","mejs.live-broadcast":"Live Broadcast","mejs.volume-help-text":"Use Up\/Down Arrow keys to increase or decrease volume.","mejs.unmute":"Unmute","mejs.mute":"Mute","mejs.volume-slider":"Volume Slider","mejs.video-player":"Video Player","mejs.audio-player":"Audio Player","mejs.captions-subtitles":"Captions\/Subtitles","mejs.captions-chapters":"Chapters","mejs.none":"None","mejs.afrikaans":"Afrikaans","mejs.albanian":"Albanian","mejs.arabic":"Arabic","mejs.belarusian":"Belarusian","mejs.bulgarian":"Bulgarian","mejs.catalan":"Catalan","mejs.chinese":"Chinese","mejs.chinese-simplified":"Chinese (Simplified)","mejs.chinese-traditional":"Chinese (Traditional)","mejs.croatian":"Croatian","mejs.czech":"Czech","mejs.danish":"Danish","mejs.dutch":"Dutch","mejs.english":"English","mejs.estonian":"Estonian","mejs.filipino":"Filipino","mejs.finnish":"Finnish","mejs.french":"French","mejs.galician":"Galician","mejs.german":"German","mejs.greek":"Greek","mejs.haitian-creole":"Haitian Creole","mejs.hebrew":"Hebrew","mejs.hindi":"Hindi","mejs.hungarian":"Hungarian","mejs.icelandic":"Icelandic","mejs.indonesian":"Indonesian","mejs.irish":"Irish","mejs.italian":"Italian","mejs.japanese":"Japanese","mejs.korean":"Korean","mejs.latvian":"Latvian","mejs.lithuanian":"Lithuanian","mejs.macedonian":"Macedonian","mejs.malay":"Malay","mejs.maltese":"Maltese","mejs.norwegian":"Norwegian","mejs.persian":"Persian","mejs.polish":"Polish","mejs.portuguese":"Portuguese","mejs.romanian":"Romanian","mejs.russian":"Russian","mejs.serbian":"Serbian","mejs.slovak":"Slovak","mejs.slovenian":"Slovenian","mejs.spanish":"Spanish","mejs.swahili":"Swahili","mejs.swedish":"Swedish","mejs.tagalog":"Tagalog","mejs.thai":"Thai","mejs.turkish":"Turkish","mejs.ukrainian":"Ukrainian","mejs.vietnamese":"Vietnamese","mejs.welsh":"Welsh","mejs.yiddish":"Yiddish"}};
      </script>
      <script type='text/javascript' src='https://setsail.qodeinteractive.com/wp-includes/js/mediaelement/mediaelement-and-player.min.js?ver=4.2.16' id='mediaelement-core-js'></script>
      <script type='text/javascript' src='https://setsail.qodeinteractive.com/wp-includes/js/mediaelement/mediaelement-migrate.min.js?ver=5.7' id='mediaelement-migrate-js'></script>
      <script type='text/javascript' id='mediaelement-js-extra'>
         /* <![CDATA[ */
         var _wpmejsSettings = {"pluginPath":"\/wp-includes\/js\/mediaelement\/","classPrefix":"mejs-","stretching":"responsive"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='https://setsail.qodeinteractive.com/wp-includes/js/mediaelement/wp-mediaelement.min.js?ver=5.7' id='wp-mediaelement-js'></script>
      <script type='text/javascript' src='https://setsail.qodeinteractive.com/wp-content/themes/setsail/assets/js/modules/plugins/jquery.appear.js?ver=5.7' id='appear-js'></script>
      <script type='text/javascript' src='https://setsail.qodeinteractive.com/wp-content/themes/setsail/assets/js/modules/plugins/modernizr.min.js?ver=5.7' id='modernizr-js'></script>
      <script type='text/javascript' src='https://setsail.qodeinteractive.com/wp-includes/js/hoverIntent.min.js?ver=1.8.1' id='hoverIntent-js'></script>
      <script type='text/javascript' src='https://setsail.qodeinteractive.com/wp-content/themes/setsail/assets/js/modules/plugins/jquery.plugin.js?ver=5.7' id='jquery-plugin-js'></script>
      <script type='text/javascript' src='https://setsail.qodeinteractive.com/wp-content/themes/setsail/assets/js/modules/plugins/owl.carousel.min.js?ver=5.7' id='owl-carousel-js'></script>
      <script type='text/javascript' src='https://setsail.qodeinteractive.com/wp-content/themes/setsail/assets/js/modules/plugins/jquery.waypoints.min.js?ver=5.7' id='waypoints-js'></script>
      <script type='text/javascript' src='https://setsail.qodeinteractive.com/wp-content/themes/setsail/assets/js/modules/plugins/fluidvids.min.js?ver=5.7' id='fluidvids-js'></script>
      <script type='text/javascript' src='https://setsail.qodeinteractive.com/wp-content/themes/setsail/assets/js/modules/plugins/perfect-scrollbar.jquery.min.js?ver=5.7' id='perfect-scrollbar-js'></script>
      <script type='text/javascript' src='https://setsail.qodeinteractive.com/wp-content/themes/setsail/assets/js/modules/plugins/ScrollToPlugin.min.js?ver=5.7' id='ScrollToPlugin-js'></script>
      <script type='text/javascript' src='https://setsail.qodeinteractive.com/wp-content/themes/setsail/assets/js/modules/plugins/parallax.min.js?ver=5.7' id='parallax-js'></script>
      <script type='text/javascript' src='https://setsail.qodeinteractive.com/wp-content/themes/setsail/assets/js/modules/plugins/jquery.waitforimages.js?ver=5.7' id='waitforimages-js'></script>
      <script type='text/javascript' src='https://setsail.qodeinteractive.com/wp-content/themes/setsail/assets/js/modules/plugins/jquery.prettyPhoto.js?ver=5.7' id='prettyphoto-js'></script>
      <script type='text/javascript' src='https://setsail.qodeinteractive.com/wp-content/themes/setsail/assets/js/modules/plugins/jquery.easing.1.3.js?ver=5.7' id='jquery-easing-1.3-js'></script>
      <script type='text/javascript' src='https://setsail.qodeinteractive.com/wp-content/plugins/js_composer/assets/lib/bower/isotope/dist/isotope.pkgd.min.js?ver=6.6.0' id='isotope-js'></script>
      <script type='text/javascript' src='https://setsail.qodeinteractive.com/wp-content/plugins/qi-addons-for-elementor/inc/masonry/assets/js/plugins/packery-mode.pkgd.min.js?ver=5.7' id='packery-js'></script>
      <script type='text/javascript' src='https://setsail.qodeinteractive.com/wp-content/themes/setsail/assets/js/modules/plugins/jquery.mousewheel.min.js?ver=5.7' id='jquery-mousewheel-js'></script>
      <script type='text/javascript' src='https://setsail.qodeinteractive.com/wp-content/plugins/woocommerce/assets/js/select2/select2.full.min.js?ver=4.0.3' id='select2-js'></script>
      <script type='text/javascript' src='https://setsail.qodeinteractive.com/wp-content/plugins/setsail-core/shortcodes/countdown/assets/js/plugins/jquery.countdown.min.js?ver=5.7' id='countdown-js'></script>
      <script type='text/javascript' src='https://setsail.qodeinteractive.com/wp-content/plugins/setsail-core/shortcodes/counter/assets/js/plugins/counter.js?ver=5.7' id='counter-js'></script>
      <script type='text/javascript' src='https://setsail.qodeinteractive.com/wp-content/plugins/setsail-core/shortcodes/counter/assets/js/plugins/absoluteCounter.min.js?ver=5.7' id='absoluteCounter-js'></script>
      <script type='text/javascript' src='https://setsail.qodeinteractive.com/wp-content/plugins/qi-addons-for-elementor/inc/shortcodes/typeout-text/assets/js/plugins/typed.js?ver=1' id='typed-js'></script>
      <script type='text/javascript' src='https://setsail.qodeinteractive.com/wp-content/plugins/setsail-core/shortcodes/full-screen-sections/assets/js/plugins/jquery.fullPage.min.js?ver=5.7' id='fullPage-js'></script>
      <script type='text/javascript' src='https://setsail.qodeinteractive.com/wp-content/plugins/setsail-core/shortcodes/vertical-split-slider/assets/js/plugins/jquery.multiscroll.min.js?ver=5.7' id='multiscroll-js'></script>
      <script type='text/javascript' src='//maps.googleapis.com/maps/api/js?key=AIzaSyDm0zEJ3h0OeMp_qsvHIzcx_5w5HJVH1L4&#038;ver=5.7' id='setsail-select-google-map-api-js'></script><script type='text/javascript' src='https://setsail.qodeinteractive.com/wp-includes/js/wp-embed.min.js?ver=5.7' id='wp-embed-js'></script>
      <script type='text/javascript' src='https://setsail.qodeinteractive.com/wp-content/plugins/js_composer/assets/js/dist/js_composer_front.min.js?ver=6.6.0' id='wpb_composer_front_js-js'></script>