@foreach($flights as $order)	
	@php

		$duffelOrder = $order["order"];
		$owner = $duffelOrder["owner"];
		$slices = $duffelOrder["slices"];

		$originData = getTripOriginData($slices);

		$destination = $originData["destination"];


		$lastSlice = sizeof($slices) - 1;
		$firstSlice = $slices[0];
		$lastSlice = $slices[$lastSlice];

		$origin = $firstSlice["origin"];
		$firstSegment = $firstSlice["segments"][0] ?? $firstSlice;
		$departingAt = \Carbon\Carbon::parse($firstSegment["departing_at"])->format("F d, Y g:i A");
	@endphp
	<div class="col-md-12 mb-3">
		<div class="card">
			<div class="card-body">
				<div class="row">
					<div class="col-md-12" id="{{ $order['refID'] }}">
						<div class="d-flex" style="flex-direction: row; justify-content:space-between; align-items:center;">
							<div style="max-width:120px;" class="text-center">
								<img src="{{ $owner['logo_symbol_url'] }}" style="width:60px;"><br>
								<div style="position: absolute; top:7px; left:10px;">
									@if($order["airlock"])
										<img src="/assets/img/icons/airlock-active.png" style="width:20px;">
									@endif
								</div>
								<span class="mt-2 text-small flight-order-status btn btn-white" style="min-width: 100px;" data-order="{{ $order['refID'] }}"><i class="fa fa-sync fa-spin text-dark"></i> Getting Status</span>
							</div>
							<div class="text-right col-xs-12 col-xl-3 col-lg-3">
								<h5>Order: {{ $order["booking_reference"] }}</h5>
								<a href="/trips/{{ $order['refID'] }}" class="btn btn-success" target="_blank">View Trip</a>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="text-left col-lg-10 col-xl-10" style="flex:1; padding-left:25px;">
						Flight from {{ $origin["city_name"] }} to {{ $destination["city_name"] }} at {{ $departingAt }}<br>
						Departing from {{ $origin["name"] }} 
					</div>
				</div>
			</div>
		</div>
	</div>
@endforeach
@if(sizeof($flights ) == 0)
	<div class="col-md-10">
		<div class="card">
			<div class="card-body text-center">
				<h3>Nothing Here, Yet!</h3> 
				<h5>Seems like you need to book a flight</h5>
				<a href="/flights" class="btn btn-success"><i class="fa fa-plane"></i> Search Flights</a>
			</div>
		</div>
	</div>
@endif