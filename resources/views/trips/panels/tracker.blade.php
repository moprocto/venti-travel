@extends("layouts.curator-master")

@section('css')
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/switchery/0.8.2/switchery.min.css" integrity="sha512-uyGg6dZr3cE1PxtKOCGqKGTiZybe5iSq3LsqOolABqAWlIRLo/HKyrMMD8drX+gls3twJdpYX0gDKEdtf2dpmw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
	<style type="text/css">
		.card-title{
			font-size: 14px;
			text-transform: uppercase;
			font-weight: 700;
		}
		ul {
			font-size: 13px
		}
	</style>
@endsection

@section('content')
	<div id="page-content">
	    <div class="gradient-wrap">
	        <div class="container hero-section mb-4 mt-4">
	           	<div class="row">
	           		<div class="col-md-10 mt-4">
	                    <h1>Track | Flight from {{ $tracker["iata_origin"] }} to {{ $tracker["iata_destination"] }}</h1>
	                    <p id="tracking-text">Price tracking data for this flight will appear here</p>
	                </div>
	            </div>
				<div class="row">
					<div class="col-md-3">
						<div class="card">
							<div class="card-body">
								<h5 class="card-title">Status:</h5>
								<div class="form-group" style="display: flex; flex-direction: row; justify-content: space-between; align-items: center;">
									<input type="checkbox" class="js-switch" id="status" @if($tracker['status'] == 'active') checked="checked" value="1" @endif>
									<span id="statusState">{{ ucfirst($tracker["status"]) }}</span>
								</div>
								
								<h5 class="card-title">Alert Level:</h5>
								<p style="font-size:13px;">Receive notification when the price of this flight drops to <strong>${{ number_format($tracker["strike"],0) }}</strong></p>

								<h5 class="card-title">Passengers:</h5>
								<ul style="list-style:none; padding:0; margin:0">
									<li><strong>Adults:</strong> {{ $tracker["adults"] }}</li>
									<li><strong>Children:</strong> {{ $tracker["children"] ?? 0 }}</li>
								</ul>
								<br>
								<h5 class="card-title">Itinerary:</h5>
								<ul style="list-style:none; padding:0; margin:0">
									<li><strong>Origin:</strong> {{ $tracker["origin"] }}</li>
									<li><strong>Destination:</strong> {{ $tracker["destination"] }}</li>
									<li><strong>Departure:</strong> {{ \Carbon\Carbon::parse($tracker["start"])->format("F d, Y") }}</li>
									@if($tracker["type"] == "round-trip")
										<li><strong>Return:</strong> {{ \Carbon\Carbon::parse($tracker["end"])->format("F d, Y") }}</li>
									@endif
								</ul>
							</div>
						</div>
					</div>
					<div class="col-md-9">
						<div class="card">
							<div class="card-body">
								<h5 class="card-title">Price History</h5>
								<table class="table">
									<thead>
										<th>Airline</th>
										<th class="text-center">MSRP</th>
										<th class="text-center">Venti Price</th>
										<th class="text-center">Points Needed</th>
										<th>Date Checked</th>
									</thead>
									<tbody>
										@foreach($prices as $price)
										@php

											$flight = $price["data"];
											$discountAlgo = getDiscountPercent("airfare", $flight['total_amount'],0);
											$discountedPrice = $flight["total_amount"] * $discountAlgo;
											$pointsNeeded = $flight["total_amount"] - $discountedPrice;
										@endphp
											<tr>
												<td>{{ $flight["airline"]["name"] ?? "N/A" }}</td>
												<td class="text-center">${{ number_format($flight["total_amount"],2) }}</td>
												<td class="text-center">${{ number_format($discountedPrice,2) }}</td>
												<td class="text-center">{{ number_format($pointsNeeded) }}</td>
												<td>{{ \Carbon\Carbon::parse($price["createdAt"])->format("F d, Y") }}</td>
											</tr>
										@endforeach
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('js')
	<script src="https://cdnjs.cloudflare.com/ajax/libs/switchery/0.8.2/switchery.min.js" integrity="sha512-lC8vSUSlXWqh7A/F+EUS3l77bdlj+rGMN4NB5XFAHnTR3jQtg4ibZccWpuSSIdPoPUlUxtnGktLyrWcDhG8RvA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
	<script>
		$(document).ready( function () {
	    	$.ajaxSetup({
		      headers: {
		          'X-CSRF-TOKEN': "{{ csrf_token() }}"
		      }
			});

			var elem = document.querySelector('#status');

		    var switchery = new Switchery(elem, { size: 'small' });

			$(".switchery").click(function(){
				var status = elem.checked;
				
				$.ajax({
		            url: "{{ route('trip-update-status') }}",
					type: 'POST',
					async: true,
					processData: true,
			        dataType: 'json',
		            data:{
		            	tracker: "{{ $tracker['trackerID'] }}",
		            	status: status
		            },
		            success: function (data) {
		            	console.log(status);
		            	if(status == true){
		            		$("#statusState").text("Active")
		            	}
		            	else{
		            		$("#statusState").text("Inactive")
		            	}
		            }
			    });
			});
		});
	</script>
@endsection