<div class="col-md-10">
	<h4>Travel Insurance</h4>
	<div class="card bg-white">
		<div class="card-body">
			<p>Venti is officially an affiliate of <a href="https://www.visitorscoverage.com/?affid=36a20d3544aad" target="_blank">Visitors Coverage</a>. When you purchase travel insurance for this trip using our referral portal below, you will be credited 10 Points! To claim your credit, send an email to admin@venti.co with your policy number and your confirmation ID: {{ $booking_reference }}</p>
			<br>
				<div id="visitorscoverage_quote_1"><a href="https://www.visitorscoverage.com/?affid=36a20d3544aad" target="_blank"><img src="/assets/img/visitors-coverage-logo.png" style="width:100%; border-radius:10px"></a></div>
			<br>
			<br>
			@if($type == "flight")
				Note: Venti's Airlock Flight Protection is not travel insurance. 
			@endif
		</div>
	</div>
</div>