@foreach($hotels as $order)
	@php
		$photo = false;
		$photos = $order["order"]["accommodation"]["photos"] ?? [];
		if(sizeof($photos)>0){
			$photo = $photos[0]["url"];
		}
	@endphp
	<div class="col-md-12 mb-3">
		<div class="card">
			<div class="card-body">
				<div class="row">
					<div class="col-md-12" id="{{ $order['refID'] }}">
						<div class="d-flex" style="flex-direction: row; justify-content:space-between; align-items:center;">
							<div style="width:240px; max-width: 100%;" class="text-center">
								@if($photo)
									@foreach($photos as $photo)
										<div class="hotel-thumbnail" style="background-image: url('{{ $photo['url'] }}'); background-size: cover; background-position:center;">
										</div>
										@php break; @endphp
									@endforeach
								@endif
							</div>
							<div style="padding:10px;">
								<h3>{{ $order["order"]["accommodation"]["name"] }}</h3>
								<strong>Check In:</strong> {{ $order["order"]["check_in_date"] }}<br>
								<strong>Check Out:</strong> {{ $order["order"]["check_out_date"] }}
							</div>
							<div class="text-right col-xs-12 col-xl-3 col-lg-3">
								<h5>Order: {{ $order["booking_reference"] }}</h5>
								<a href="/trips/{{ $order['refID'] }}" class="btn btn-success" target="_blank">View Reservation</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endforeach

@if(sizeof($hotels) == 0)
	<div class="col-md-10">
		<div class="card">
			<div class="card-body text-center">
				<h3>Nothing Here, Yet!</h3> 
				<h5>Seems like you need to book a hotel</h5>
				<a href="/hotels" class="btn btn-success"><i class="fa fa-hotel"></i> Search Hotels</a>
			</div>
		</div>
	</div>
@endif