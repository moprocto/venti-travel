@php
	$date = \Carbon\Carbon::parse($order["timestamp"])->timezone($user->customClaims["timezone"])->format("F d, Y g:i A");
	
	$departure = \Carbon\Carbon::parse($destination["originData"]["departing_at"])->format("F d, Y");
@endphp
<html>
	<head>
		<title>Receipt for Booking {{ $order["booking_reference"] }}</title>
		<link rel="stylesheet" href="/assets/css/bootstrap.min.css">
		<link rel="icon" href="/assets/img/venti-financial-logo-50.png" sizes="32x32" />
		<link rel="icon" href="/assets/img/venti-financial-logo-192.png" sizes="192x192" />
		<style type="text/css">
			.text-right{
				text-align: right;
			}
			td{
				font-size: 11px;
			}
			@media print {
			    .pagebreak { page-break-after: always; } /* page-break-after works, as well */
			}
		</style>
	</head>
	<body>
		<div class="container">
			<div class="row" style="padding-top:50px">
				<div class="col-md-8">
					<img src="https://venti.co/assets/img/venti-trademark-logo-black.png" style="width:100px;">
						<ul style="list-style:none; font-size: 12px; padding:0;padding-top:10px">
							<li>Venti Financial, Inc.</li>
							<li>800 N King Street</li>
							<li>Suite 304 1552</li>
							<li>Wilmington, DE 19801</li>
						</ul>
				</div>
				<div class="col-md-4">
					<h5 style="padding-bottom: 0; margin-bottom:0;padding-top: 44px">Payment Receipt</h5>
					<p style="font-size: 12px;">For: {{ cleanName(Session::get('user')->displayName) }} <br></p>
					<br>
				</div>
			</div>
			<div class="row" style="padding-top:10px; padding-bottom:10px">
				<div class="col-md-12 text-left">
					<div class="d-flex" style="flex-direction:row; justify-content:space-between; align-content:center; align-items: center;">
						<strong><p>Confirmation: {{ $order["booking_reference"] }}</p></strong>
						<h3 style="margin:0;">Paid</h3>
					</div>
				</div>
			</div>
			
			<table class="table">
				<thead>
					<th style="width:14%">Date</th>
					<th>Description</th>
					<th>From</th>
					<th>To</th>
					<th style="width:5%">Total</th>
				</thead>
				<tbody>
				<tr>
					<td>{{ $date }}</td>
					<td class="text-left" colspan="3">Base Fare</td>
					<td class="text-right">${{ number_format($duffelData["base_amount"],2) }}</td>
				</tr>
				<tr>
					<td>{{ $date }}</td>
					<td class="text-left" colspan="3">Taxes</td>
					<td class="text-right">${{ number_format($duffelData["tax_amount"],2) }}</td>
				</tr>

				@foreach($order["seatUpgrades"] as $seatUpgrade)
					<tr>
						<td>{{ $date }}</td>
						<td class="text-left" colspan="3">Seat Selection: {{ $seatUpgrade["metadata"]["designator"] }}</td>
						<td class="text-right is-add-on" data-total="{{ $seatUpgrade['newPrice'] }}">${{ $seatUpgrade["newPrice"] }}</td>
					</tr>
				@endforeach
				@foreach($order["bagUpgrades"] as $bagUpgrade)
					<tr>
						<td>{{ $date }}</td>
						<td class="text-left" colspan="3">Additional Checked Bag</td>
						<td class="text-right is-add-on" data-total="{{ $bagUpgrade['newPrice'] }}">${{ $bagUpgrade["newPrice"] }}</td>
					</tr>
				@endforeach
				@if($order["airlock"])
					<tr>
						<td>{{ $date }}</td>
						<td class="text-left" colspan="3">Airlock Flight Protection</td>
						<td class="text-right is-add-on">${{ $order["airlockPrice"] }}</td>
					</tr>
				@endif
					<tr>
						<td>{{ $date }}</td>
						<td>{{ $departure }} | {{ $duffelData["owner"]["name"] }} Flight to {{ $destination["destinationArrival"]["city_name"] }}</td>
						<td>My {{ $order["from"] ?? "Boarding Pass" }}</td>
						<td>Venti</td>
						<td style="text-align:right">${{ number_format( $order["amount"],2) }}</td>
					</tr>
					<tr>
						<td>{{ $date }}</td>
						<td>{{ $departure }} | {{ $duffelData["owner"]["name"] }} Flight to {{ $destination["destinationArrival"]["city_name"] }}</td>
						<td>My Points Balance</td>
						<td>Venti</td>
						<td style="text-align:right">{{ number_format($order["points"],2) }}</td>
					</tr>
					<tr>
						<td colspan="4" style="text-align: right; font-weight:bold;">Amount Paid<br><span class="d-block" style="font-size:11px;">In cash and cash-equivalent Points value</span></td>
						<td><strong>${{ number_format((float) $order["amount"] + (float) $order["points"],2) }}</strong></td>
					</tr>
				</tbody>
			</table>
			<h5>Think You Found a Mistake?</h5>
			<br>
			<p>You can send a letter to Venti Financial, Inc. 800 N King Street Suite 304 1552, Wilmington, DE 19801. You may also contact us at <a href="https://venti.co">venti.co</a> or send an email to admin@venti.co</p>
			<div class="col-md-8" style="padding-bottom:50px;">
				<img src="https://venti.co/assets/img/venti-trademark-logo-black.png" style="width:100px;">
			</div>
		</div>
	</body>
</html>
