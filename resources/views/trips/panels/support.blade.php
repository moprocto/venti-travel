<div class="col-md-10">
	<form method="POST" id="supportForm"a>
		<input type="hidden" name="_token" value="{{ csrf_token() }}" id="csrf-token">
		<h4>How Can We Help?</h4>
		<div class="card bg-white mb-3">
			<div class="card-body">
				<p>Please fill out the below form to submit a support request to our team. We will provide a response within two business days. Depending on the airline and restrictions imposed on your booking, we may not be able to fulfill your request. Some airline supported changes may come with additional fees. We will request your written approval to initiate a change to your booking if it requires a fee.</p>
				<label class="form-label">Request Support Categories <span class="d-block text-small">Please select up to three categories.</span></label>
				<div class="form-group">
					<select class="form-control required" name="supportRequest" id="supportRequest" required="" multiple="">
						<option value="insurance">Add Travel Insurance</option>
						<option value="luggage">Adding Luggage</option>
						<option value="seats">Seat Selection</option>
						@if($changeable)
						<option value="upgrading">Upgrading/Downgrading Fare Class</option>
						@endif
						<option value="passenger">Update Passenger Info</option>
						<option value="accomodations">Request Accomodations</option>
						<option value="documentation">Request Additional Documentation</option>
						@if($changeable)
							<option value="departure">Change Departure Date</option>
						@endif
						@if($cancelable)	
							<option value="cancelation">Request Cancelation</option>
						@endif
					</select>
				</div>
				<div class="form-group mb-3">
					<label class="form-label">Personalize Your Message to Support
					<span class="d-block text-small" style="font-weight:400">This is optional. You are requesting accomodations for yourself or other passengers, please indicate them below. The more detail you provide here, the faster our team can fulfill requests.</span></label>
					<textarea class="form-control required" name="supportMessage" id="supportMessage"></textarea>
				</div>
				<div class="form-group">
					<button type="button" class="btn btn-success" id="submitSupport"><i class="fa fa-sync fa-spin"></i> Submit Support Request</button>
				</div>

				@if($canceledOn)
					<div class="alert alert-danger">
						<p>This trip was canceled on {{ $canceledOn }}. Our support team prioritizes active bookings. Please expect a response of up to seven business days.</p>
					</div>
				@endif
			</div>
		</div>
	</form>
</div>