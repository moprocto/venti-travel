<ul class="nav nav-pills mb-3 full-width" id="pills-tab" role="tablist">
  <li class="nav-item" role="presentation">
    <button class="nav-link active" id="pills-home-tab" data-bs-toggle="pill" data-bs-target="#pills-home" type="button" role="tab" aria-controls="pills-home" aria-selected="true">Itinerary</button>
  </li>

  <li class="nav-item" role="presentation">
    <button class="nav-link" id="pills-funding-tab" data-bs-toggle="pill" data-bs-target="#pills-funding" type="button" role="tab" aria-controls="pills-funding" aria-selected="false">@if($type == "flight")Passengers @else Guests @endif</button>
  </li>
  <li class="nav-item" role="presentation">
    <button class="nav-link" id="pills-orders-tab" data-bs-toggle="pill" data-bs-target="#pills-orders" type="button" role="tab" aria-controls="pills-orders" aria-selected="false">Order Summary</button>
  </li>
  <li class="nav-item" role="presentation">
    <button class="nav-link" id="pills-insurance-tab" data-bs-toggle="pill" data-bs-target="#pills-insurance" type="button" role="tab" aria-controls="pills-settings" aria-selected="false">Travel Insurance</button>
  </li>
  <li class="nav-item" role="presentation">
    <button class="nav-link" id="pills-settings-tab" data-bs-toggle="pill" data-bs-target="#pills-settings" type="button" role="tab" aria-controls="pills-settings" aria-selected="false">Support</button>
  </li>
  @if($type == "hotel")
    <li class="nav-item" role="presentation">
      <button class="nav-link" id="pills-terms-tab" data-bs-toggle="pill" data-bs-target="#pills-terms" type="button" role="tab" aria-controls="pills-terms" aria-selected="false">Terms</button>
    </li>
  @endif
</ul>