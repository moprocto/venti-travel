<div class="col-md-10 mb-4">
  <h4>Order Summary</h4>
  <div class="card bg-white mb-3">
    <div class="card-body">
      <ul style="list-style:none">
        <li>
          <div class="d-flex" style="flex-direction: row;justify-content:space-between;">
            <p>Order Date:</p>
            <p>{{ \Carbon\Carbon::parse($order["timestamp"])->timezone($user->customClaims["timezone"])->format("F d, Y @ g:i A") }}</p>
          </div>
        </li>
        <li>
          <div class="d-flex" style="flex-direction: row;justify-content:space-between;">
            <p>Base Fare:</p>
            <p>$ {{ number_format($data["base_amount"],2) }}</p>
          </div>
        </li>
        <li>
          <div class="d-flex" style="flex-direction: row;justify-content:space-between;">
            <p>Taxes:</p>
            <p>$ {{ number_format($data["tax_amount"],2) }}</p>
          </div>
        </li>
        @if($order["bagUpgrades"])
          @foreach($order["bagUpgrades"] as $bagUpgrade)
            <li>
              <div class="d-flex" style="flex-direction: row;justify-content:space-between;">
                <p>Additional Checked Bag:</p>
                <p>$ {{ number_format($bagUpgrade["newPrice"],2) }}</p>
              </div>
            </li>
          @endforeach
        @endif
        @if($order["seatUpgrades"])
          @foreach($order["seatUpgrades"] as $seatUpgrade)
            <li>
              <div class="d-flex" style="flex-direction: row;justify-content:space-between;">
                <p>Seat Selection: {{ $seatUpgrade["metadata"]["designator"] }}</p>
                <p>$ {{ number_format($seatUpgrade["newPrice"],2) }}</p>
              </div>
            </li>
          @endforeach
        @endif
        @if($order["airlock"])
          <li>
            <div class="d-flex" style="flex-direction: row;justify-content:space-between;">
              <p>Airlock Flight Protection:</p>
              <p>$ {{ number_format($order["airlockPrice"],2) }}</p>
            </div>
          </li>
        @endif
        <li >
          <div class="d-flex" style="border-top: 1px solid black; padding-top: 5px; flex-direction: row;justify-content:space-between;">
            <p>Paid from {{ $order["from"] ?? "Boarding Pass" }}:</p>
            <p>$ {{ number_format($order["amount"],2) }}</p>
          </div>
        </li>
        <li>
          <div class="d-flex" style="flex-direction: row;justify-content:space-between;">
            <p>Paid with Points:</p>
            <p>{{ number_format($order["points"],2) }}</p>
          </div>
        </li>
        <li>
          <div class="d-flex" style="flex-direction: row;justify-content:flex-end;">
            <p class="text-right" style="padding-right: 20px; font-weight:bold; fon">Total:<span class="d-block" style="font-size:11px;">In cash and cash-equivalent Points value</span></p>
            <p style="font-weight: bold;">$ {{ number_format((float) $order["amount"] + (float) $order["points"],2) }}</p>
          </div>
        </li>
      </ul>
      <div class="d-flex" style="flex-direction:row; justify-content:space-between;">
      <button class="btn btn-black" id="resendReceipt"><i class="fa fa-spin fa-sync"></i> Resend Receipt</button>

      <a href="/trips/{{ $order['refID'] }}/invoice" target="_blank" class="btn btn-white text-primary"><i class="fa fa-file"></i> Downloadable Invoice</a>
      </div>
    </div>
  </div>
  @include("trips.panels.airlock")
</div>