<div class="row" id="newTrackerForm">
	<input type="hidden" id="iata_destination">
	<input type="hidden" id="iata_origin">
	<input type="hidden" id="city_name">
	<div class="col-md-12">
		<h5>Passengers</h5>
		<div class="row">
			<div class="form-group col-md-6">
				<label class="form-label">Number of Adults</label>
				<input type="number" min="0" name="" class="form-control required" value="1" id="adults">
			</div>
			<div class="form-group col-md-6">
				<label class="form-label">Number of Children</label>
				<input type="number" min="0" name="" class="form-control required" value="0" id="children">
			</div>
		</div>
	</div>
	<div class="col-md-12">
		<h5>Flight Plan</h5>
		<div class="row">
			<div class="form-group col-md-6">
				<label class="form-label d-block">Origin</label>
				<select class="form-control"  id="flight-from">
					<option value="">Select Origin</option>
				</select>
			</div>
			<div class="form-group col-md-6">
				<label class="form-label d-block">Destination</label>
				<select class="form-control" required="" id="flight-to">
					<option value="">Select Destination</option>
				</select>
			</div>
		</div>
		<div class="row">
			<div class="form-group col-md-6">
				<label class="form-label col-md-6">Flight Type</label>
				<select class="form-control required" required="" id="flight_type">
					<option value="round-trip">Round Trip</option>
					<option value="one-way">One Way</option>
				</select>
			</div>
			<div class="form-group col-md-6">
				<label class="form-label col-md-6">Seat Class</label>
				<select class="form-control required" id="flight_class">
					<option value="economy">Economy</option>
					<option value="economy_plus">Economy Plus</option>
					<option value="business">Business</option>
				</select>
			</div>
		</div>
		<div class="row">
			<div class="form-group col-md-6">
				<label class="form-label">Departure</label>
				<input type="date" class="form-control required" id="departure">
			</div>
			<div class="form-group col-md-6">
				<label class="form-label">Return</label>
				<input type="date" class="form-control required" id="return">
			</div>
		</div>
		
	</div>
	<div class="col-md-12">
		<h5>Tracking Details</h5>
		<p>We will track the cost of this flight each night at 2 a.m. EST and will stop tracking one day before departure or by the date defined below, whichever comes first.</p>
		<div class="row">
			<div class="form-group col-md-12">
				<label class="form-label">Stop Tracking On (Optional)</label>
				<input type="date" class="form-control" id="end">
			</div>
		</div>
		<div class="row">
			<div class="form-group col-md-12">
				<label class="form-label">Alert me via email when the price (without discount) of this flight plan drops below a specified amount, or when a strong recommendation to book is available. The lowest possible price with Points will also be tracked. Please round to the nearest dollar.</label>
				<input type="number" class="form-control required" min="0" step="1" id="strike" placeholder="50">
			</div>
		</div>
	</div>
	<div class="col-md-12">
		<h5>Confirmation</h5>
		<p>Please confirm the above details before submitting. There is a fee of 1 Point to create this tracker. Your current Points balance is: <strong><span id="pointsBalance">{{ number_format($points,2) }}</span></strong></p>
		<button type="button" id="createTracker" class="btn btn-success"><i class="fa fa-sync fa-spin"></i> Create Flight Tracker</button>
	</div>
</div>