<div class="col-md-10 mb-4">
	<h4>Passengers</h4>
	@php
		$documents = $duffelData["documents"] ?? false;
	@endphp
	@foreach($passengers as $passenger)
		@php
			$dob = \Carbon\Carbon::parse($passenger["born_on"])->format("F d, Y");
			$age = \Carbon\Carbon::parse($passenger["born_on"])->age;

		@endphp
		<div class="row mb-3">
			<div class="col-md-12">
				<div class="card bg-white">
					<div class="card-body">
						<h4>{{ $passenger["given_name"] }} {{ $passenger["family_name"] }} | {{ ucfirst($passenger["type"]) }}</h4>
						<ul>
							<li><strong>Date of Birth:</strong> {{ $dob }} | {{ $age }}</li>
							<li><strong>Email:</strong> {{ $passenger["email"] }}</li>
							<li><strong>Phone:</strong> {{ $passenger["phone_number"] }}</li>
							@if($documents)
								<li><strong>Ticket #:</strong> {{ getPassengerTicketNumber($documents, $passenger["id"]) }}</li>
							@endif
						</ul>
					</div>
				</div>
			</div>
		</div>
	@endforeach
</div>