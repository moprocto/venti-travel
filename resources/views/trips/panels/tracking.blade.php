<div class="col-md-12">
	<div class="accordion mb-4" id="accordionExample">
		<div class="accordion-item">
			<h2 class="accordion-header" id="headingOne">
			  <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
			    Track New Flight
			  </button>
			</h2>
			<div id="collapseOne" class="accordion-collapse collapse" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
			  <div class="accordion-body">
			    @include("trips.panels.flight-tracker-form")
			  </div>
			</div>
		</div>
	</div>
	<h5>Tracked Flights</h5>
	<div class="card">
		<div class="card-body text-center">
			<table class="table" style="font-size:12px;" id="trackers">
				<thead>
					<th class="text-left">Origin</th>
					<th class="text-left">Destination</th>
					<th class="text-left">Details</th>
					<th>Watch Price</th>
					<th class="text-center">Status</th>
					<th class="text-center"></th>
				</thead>
				<tbody>
					@foreach($trackedFlights as $trackedFlight)
						@php
							$flightType = ucfirst( str_replace("-"," ",$trackedFlight["type"]));
							$departure = Carbon\Carbon::parse($trackedFlight["start"])->format("F d, Y");
							$return = $trackedFlight["end"] ?? false;
							if($return != false){
								$return = Carbon\Carbon::parse($trackedFlight["end"])->format("F d, Y");
							}
						@endphp
						<tr>
							<td class="text-left">{{ $trackedFlight["origin"] }}</td>
							<td class="text-left">{{ $trackedFlight["destination"] }}</td>
							<td class="text-left">
								<ul style="list-style:none; margin:0;padding:0; text-align: left;">
									<li><strong>Type:</strong> {{ $flightType }}</li>
									<li><strong>Departure:</strong> {{ $departure }}</li>
									@if($trackedFlight["type"] == "round-trip")
										<li><strong>Return:</strong> {{ $return }}</li>
									@endif
								</ul>
							</td>
							<td class="text-center">${{ $trackedFlight["strike"] }}</td>
							<td>{{ ucfirst($trackedFlight["status"]) }}</td>
							<td><a href="/trips/tracker/view/{{ $trackedFlight['trackerID'] }}" target="_blank" class="btn btn-white"><i class="fa fa-chevron-right text-primary"></i></td>
						</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>