@if($order["airlock"] == true)
	@php
		$airlockExpiration = \Carbon\Carbon::parse($originData["details"]["departing_at"])->format("F d, Y-g:i A");
		$airlockExpiry = explode('-', $airlockExpiration);

		$hasExpiryPassed = false;

		$now = \Carbon\Carbon::now()->timezone($originData["origin"]["time_zone"]);

		$remainingTime = (int) $now->diffInMinutes($airlockExpiration, false);

		if($remainingTime < 0){
			$hasExpiryPassed = true;
		}

		$airlockPrice = $order["airlockPrice"] ?? 0;

		 // this is how much we charged in warranty for the flight

		$originalTotal = $order["amount"];
		$pointsUsed = $order["points"];
		$billedTotal = $originalTotal + $pointsUsed;

		$maxRefundableAmountPoints = $pointsUsed * 0.7;
		$airlockRatio = 1 - $airlockPrice/$billedTotal;
		$cancelQuoteCash = round($originalTotal * $airlockRatio,2);
		$cancelQuotePoints = round($pointsUsed * $airlockRatio,2);
        

	@endphp
		<div class="d-flex mt-3 mb-3" style="align-items: center;flex-direction: row; justify-content:space-between">
			<h4 style="padding:0; margin:0">Airlock Flight Protection</h4>
			<img src="/assets/img/icons/airlock-active.png" style="width:50px;">
		</div>
		<div class="card bg-white">
			<div class="card-body">
				@if(!$canceledOn)
					<p>This trip has been secured with Airlock and can be canceled for a full refund until <strong>{{ $airlockExpiry[1] }} on {{ $airlockExpiry[0] }}</strong></p>
					<p>Airlock can only be used to cancel the entire order. Cancelation is non-reversible.</p>
					<div class="form-group mb-3">
						<label class="form-label">Please Select Cancelation Reason</label>
	    				<select class="form-control" id="cancelationReason" required="">
	    					<option value="">--- Please Select ---</option>
	    					<option value="death">A passenger has died</option>
	    					<option value="sick">A passenger is too sick</option>
	    					<option value="changed">My trip plans changed</option>
	    					<option value="weather">Severe weather</option>
	    					<option value="safety">My trip is no longer safe</option>
	    					<option value="interest">No longer interested in this trip</option>
	    					<option value="affordability">I can no longer afford this trip</option>
	    				</select>
					</div>
					<div class="form-group mb-3">
						<label class="form-label">Refund Quote <span class="d-block text-small" style="font-weight: 400;">You originally paid ${{ number_format($airlockPrice,2) }} for Airlock</span></label>
						<ul style="list-style:none">
							<li>
								<div class="d-flex" style="flex-direction: row; justify-content: space-between;">
									<span>Cash Balance Refund</span>
									<span>${{ number_format($cancelQuoteCash,2) }}</span>
								</div>
							</li>
							<li>
								<div class="d-flex" style="flex-direction: row; justify-content: space-between;">
									<span>Points Balance Refund</span>
									<span style="text-align:right">{{ number_format($cancelQuotePoints,2) }}</span>
								</div>
							</li>
							<li>
								<div class="d-flex" style="flex-direction: row; justify-content: flex-end;">
									<span style="text-align:right; padding-right:15px;">Total<span style="font-size:11px; display: block;">In Cash and Cash-Equivalent Value</span></span>
									<span style="text-align:right">${{ number_format($cancelQuoteCash + $cancelQuotePoints,2) }}</span>
								</div>
							</li>
						</ul>
					</div>
					<div class="form-group">
						<div class="d-flex" style="flex-direction: row; justify-content: space-between;">
							<input type="checkbox" id="cancelAccept" required="" value="1">
							<p style="font-size:11px; padding: 0 20px; margin:0">To proceed with cancelation, I understand and accept that my entire booking will be canceled, which impacts all passengers, and that I will be refunded the above amounts within three business days. I understand that the refund amounts may differ if this order was partially refunded or approved for price matching and that this cancelation is non-reversible. I agree to the terms and conditions outlined in Venti's Airlock Agreement.</p>
						</div>
					</div>
					@if(!$hasExpiryPassed)
						<button class="d-block btn btn-primary btn-lg" id="cancelTrip"><i class="fa fa-sync fa-spin"></i> Cancel My Trip</button>
					@else
						<p style="width:100%; text-align:center">The cancelation window for this trip has passed.</p>
					@endif
				@else
					@php
						
					@endphp
					<p>This trip was canceled on {{ $canceledOn }}</p>
				@endif
			</div>
		</div>
@endif