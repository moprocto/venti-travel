<div class="col-md-10 mb-4">
  <h4>Order Summary</h4>
  <div class="card bg-white mb-3">
    <div class="card-body">
      <ul style="list-style:none">
        <li>
          <div class="d-flex" style="flex-direction: row;justify-content:space-between;">
            <p>Order Date:</p>
            <p>{{ \Carbon\Carbon::parse($duffelData["timestamp"])->timezone($user->customClaims["timezone"])->format("F d, Y @ g:i A") }}</p>
          </div>
        </li>
        <li>
          <div class="d-flex" style="flex-direction: row;justify-content:space-between;">
            <p>Base Fare:</p>
            <p>$ {{ number_format($order["accommodation"]["rooms"][0]["rates"][0]["base_amount"],2) }}</p>
          </div>
        </li>
        <li>
          <div class="d-flex" style="flex-direction: row;justify-content:space-between;">
            <p>Taxes:</p>
            <p>$ {{ number_format($order["accommodation"]["rooms"][0]["rates"][0]["tax_amount"],2) }}</p>
          </div>
        </li>
        @if($order["accommodation"]["rooms"][0]["rates"][0]["fee_amount"] > 0)
        <tr>
          <td class="text-left">Additional Resort Fees</td>
          <td class="text-right"><h5>${{ number_format($order["accommodation"]["rooms"][0]["rates"][0]["fee_amount"],2) }}</h5></td>
        </tr>
        @endif
        <li >
          <div class="d-flex" style="border-top: 1px solid black; padding-top: 5px; flex-direction: row;justify-content:space-between;">
            <p>Paid from {{ $duffelData["from"] ?? "My Boarding Pass" }}:</p>
            <p>$ {{ number_format($duffelData["amount"],2) }}</p>
          </div>
        </li>
        <li>
          <div class="d-flex" style="flex-direction: row;justify-content:space-between;">
            <p>Paid with Points:</p>
            <p>{{ number_format($duffelData["points"],2) }}</p>
          </div>
        </li>
        <li>
          <div class="d-flex" style="flex-direction: row;justify-content:flex-end;">
            <p class="text-right" style="padding-right: 20px; font-weight:bold; fon">Total:<span class="d-block" style="font-size:11px;">In cash and cash-equivalent Points value</span></p>
            <p style="font-weight: bold;">$ {{ number_format((float) $duffelData["amount"] + (float) $duffelData["points"],2) }}</p>
          </div>
        </li>
      </ul>
      <div class="d-flex" style="flex-direction:row; justify-content:space-between;">
      <button class="btn btn-black" id="resendReceipt"><i class="fa fa-spin fa-sync"></i> Resend Receipt</button>

      <a href="/trips/{{ $duffelData['refID'] }}/invoice" target="_blank" class="btn btn-white text-primary"><i class="fa fa-file"></i> Downloadable Invoice</a>
      </div>
    </div>
  </div>
</div>