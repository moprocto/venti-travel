<div class="col-md-12">
	<form method="POST" id="supportForm"a>
		<input type="hidden" name="_token" value="{{ csrf_token() }}" id="csrf-token">
		<h4>Terms</h4>
		<div class="card bg-white mb-3">
			<div class="card-body">
				@php 
					$conditions = $order["accommodation"]["rooms"][0]["rates"][0]["conditions"];
					$conditionsString = "";
					$cancelationTimelines = [];
					foreach($order["accommodation"]["rooms"][0]["rates"] as $rate){

						$cancelationTimelines = array_merge($cancelationTimelines, $rate["cancellation_timeline"]);
						foreach($rate["conditions"] as $condition){
							$conditionsString .= "<h6>" . $condition["title"] . "</h6><p>" . $condition["description"] . "</p>";
						}
					}
				@endphp
				<ul style="padding-left:12px !important; list-style:none !important; padding-bottom:10px;">
					@foreach($cancelationTimelines as $cancelationTimeline)
						<li>${{ number_format($cancelationTimeline["refund_amount"],2) }} refunded if canceled before {{ Carbon\Carbon::parse($cancelationTimeline["before"])->format("F j, Y @ h:i A") }}</li>
					@endforeach
				</ul>
				{!! $conditionsString !!}
			</div>
		</div>
	</form>
</div>