<div class="col-md-10 mb-4">
	<h4>Passengers</h4>
	@foreach($passengers as $passenger)
		<div class="row mb-3">
			<div class="col-md-12">
				<div class="card bg-white">
					<div class="card-body">
						<h4>{{ $passenger["given_name"] }} {{ $passenger["family_name"] }}
					</div>
				</div>
			</div>
		</div>
	@endforeach
</div>