<script>
	$(document).ready( function () {
    	$.ajaxSetup({
	      headers: {
	          'X-CSRF-TOKEN': "{{ csrf_token() }}"
	      }
		});

		var ajaxCalls = [];

		$(".flight-order-status").each(function(){
			var btn = $(this);
			var orderID = $(this).attr("data-order");
			var ajaxCall = $.ajax({
	            url: "{{ route('flight-order-status') }}",
	            processData: true,
	            dataType: 'json',
	            data: {
	            	orderID: orderID
	            },
	            type: 'post',
	            success: function (data) {
	            	if(data == 200){
	            		$("#" +  orderID).find(".flight-order-status").addClass();
	            		$("#" +  orderID).find(".flight-order-status").addClass("text-success");
	            		$("#" +  orderID).find(".flight-order-status").html("<i class='fa fa-check'></i> Confirmed");
	            	}
	            	if(data == 202){
	            		$("#" +  orderID).find(".flight-order-status").addClass("text-danger");
	            		$("#" +  orderID).find(".flight-order-status").html("Canceled");
	            	}
	            }
		    });

		    ajaxCalls.push(ajaxCall);
		});

		$.when.apply($, ajaxCalls).done(function() {
		    // Arguments are the responses of the Ajax calls
		    // Since we don't know the number of calls, we handle it as an array
		    var responses = arguments;

		    // Loop through the responses (each argument is a response array with [data, statusText, jqXHR])
		    $.each(responses, function(index, response) {
		        // Process each response from the Ajax calls
		        var responseData = response[0]; // The data from the response
		        
		        // You can now utilize the responseData for further processing
		    });
		}).fail(function() {
		    // This function is called if any of the requests fail
		    console.log("An error occurred with one of the requests.");
		});
	});

	$(".trips-menu .nav-link").click(function(){
		var title = $(this).data("title");
		$("#page-subtitle").text(title);
		if(title != "Price Tracking"){
			$("#booking-text").show();
			$("#tracking-text").hide();
		} else {
			$("#booking-text").hide();
			$("#tracking-text").show();
		}
	});

	$("#newTrackerModal").click(function(){
		$("#trackerModal").modal("show");
	});

	$("body").on("click","#createTracker",function(){

		var btn = $(this);
		btn.addClass("disable-while-loading");

		var passable = checkRequiredFields("newTrackerForm");

		if(!passable){
			// alert 
			swal({
                  icon: 'warning',
                  title: 'Missing required fields',
                  text: 'Please check your entries',
            });

            btn.removeClass("disable-while-loading");

			return false;
		}

		var flightFrom = $('#flight-from').select2('data')[0];
		var flightTo = $('#flight-to').select2('data')[0];


		if(flightFrom.text == "" || flightFrom.text == "Select Origin"){
			swal({
                  icon: 'warning',
                  title: 'Missing required fields',
                  text: 'Please add your origin',
            });

            btn.removeClass("disable-while-loading");

            return false;
		}

		if(flightTo.text == "" || flightTo.text == "Select Destination"){
			swal({
                  icon: 'warning',
                  title: 'Missing required fields',
                  text: 'Please add your destination',
            });

            btn.removeClass("disable-while-loading");

            return false;
		}

		$.ajax({
            url: "{{ route('trip-create-tracker') }}",
            processData: true,
            dataType: 'json',
            data: {
            	origin: flightFrom.text,
            	destination: flightTo.text,
            	flight_class: $("#flight_class").val(),
            	flight_type: $("#flight_type").val(),
            	adults: $("#adults").val(),
            	children: $("#children").val(),
            	end: $("#end").val(),
            	strike: $("#strike").val(),
            	departure: $("#departure").val(),
            	return: $("#return").val(),
            	type: "flight"
            },
            type: 'post',
            success: function (data) {
            	btn.removeClass("disable-while-loading");

            	if(data == 200){
            		swal({
		                  icon: 'success',
		                  title: 'We are tracking your flight to ' + flightTo.text,
		                  text: 'You will begin receiving alerts via email when the price drops below your target or when we have detected an unbeatable deal.',
		            });
            	}
            	if(data == 400){
            		swal({
		                  icon: 'error',
		                  title: 'Insufficient Points',
		                  text: 'You need at least one Boarding Pass Point to create this trafker',
		            });
            	}
            },
            error: function(data){
            	btn.removeClass("disable-while-loading");
            	
            	swal({
					icon: 'error',
					title: 'Something went wrong',
					text: 'Please try again, or contact support via admin@venti.co',
		        });
            }
	    });
	});

	function checkRequiredFields(target){
		var passable = true;


		$("#" + target + " .required").each(function(){
			$(this).removeClass("invalid-feedback");
			var entry = $(this).val();

			if(entry == "" || entry == undefined || entry == null){
				passable = false;
				

				$(this).addClass("invalid-feedback");
			}
		});

		return passable;
	}

	function resetForm(){

	}

	function fetchTrackers(){

	}

	$("body").on("change","#flight_type",function(){
		var entry = $(this).val();
		if(entry == "round-trip"){
			$("#return").addClass("required");

		} else {
			$("#return").removeClass("invalid-feedback");
			$("#return").removeClass("required");
		}
	});


	$("#flight-from").select2({
	    placeholder: "Select Origin",
	    minimumInputLength: 3,
	    language: {
		    'inputTooShort': function () {
		        return '';
		    }
		},
	    delay:250,
	    data: function (term) {
            return {
                term: term
            };
        },
		ajax: {
		    url: '/flights/data/airports',
		    dataType: 'json',
	        processResults: function (data) {

	            return {

	                results: $.map(data, function (item) {

	                	var item = JSON.parse(item);

	                	return item.map((airport, i) => {
	                		var id = parseInt(i + getRandomInt(30));
	                		return {
		                        text: airport.text,
		                        id: id
		                    }
	                	});
	                })
	            };
	        }
		}
	});	

	$("#flight-to").select2({
		    placeholder: "Select Destination",
		    delay:250,
		    minimumInputLength: 3,
		    data: function (term) {
	            return {
	                term: term
	            };
	        },
			ajax: {
			    url: '/flights/data/airports',
			    dataType: 'json',
		        processResults: function (data) {
		            return {
		                results: $.map(data, function (item) {
		                	var item = JSON.parse(item);

		                	return item.map((airport, i) => {
								var id = parseInt(i + getRandomInt(30));
		                		return {
			                        text: airport.text,
			                        id: id
			                    }
		                	});
		                })
		            };
		        }
			}
		});

	function getRandomInt(max) {
	  return Math.floor(Math.random() * max);
	}
</script>