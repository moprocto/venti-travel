<ul class="nav nav-pills mb-3 full-width trips-menu" id="pills-tab" role="tablist">
  <li class="nav-item" role="presentation">
    <button class="nav-link active" id="pills-home-tab" data-bs-toggle="pill" data-bs-target="#pills-home" type="button" role="tab" aria-controls="pills-home" aria-selected="true" data-title="Flights">Flights</button>
  </li>
  <li class="nav-item" role="presentation">
    <button class="nav-link" id="pills-funding-tab" data-bs-toggle="pill" data-bs-target="#pills-funding" type="button" role="tab" aria-controls="pills-funding" aria-selected="false" data-title="Hotels">Hotels</button>
  </li>
  <li class="nav-item" role="presentation" style="display:none;">
    <button class="nav-link" id="pills-orders-tab" data-bs-toggle="pill" data-bs-target="#pills-orders" type="button" role="tab" aria-controls="pills-orders" aria-selected="false" data-title="Price Tracking">Price Tracking</button>
  </li>
</ul>