@extends("layouts.curator-master")

@section('css')
	<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
	<style type="text/css">
		.product-tag{
			background: whitesmoke;
			border-radius: 20px;
			padding: 5px 10px;
			float: right;
			font-weight: bold;
			font-size: 10px;
		}
		.image-frame{
			height: 250px;
			background-size: contain;
			width: 100%;
		}
		.color-sample{
			width: 30px;
			height: 30px;
			margin-right: 10px;
			cursor: pointer;
			border-radius: 7px;
		}
		.color-box{
			display: flex;
			flex-direction: row;
		}
		.nav-pills{
			display: block;
		}
		.full-width .nav-link{
			width: 100%;
			text-align: left;
		}
		.image-frame:hover .enticer{
			display: flex;
			transition: 2s;
			cursor: pointer;
		}
		.btn .fa-sync{
			display: inherit;
		}
		.btn-success .fa-sync{
			display: none;
		}
		.text-small{
			font-size: 8px;
		}
		span.text-success{
			pointer-events: none;
			font-size: 10px !important;
			min-width: 97.5px;
			font-weight: bold;
		}
		#tracking-text{
			display: none;
		}
		.hotel-thumbnail{
			width: 240px;
			height: 200px;
		}
		.select2-container .select2-selection--single,.select2-container--default .select2-selection--single .select2-selection__clear,.select2-container--default .select2-selection--single .select2-selection__arrow{
			height: 43px;
			font-size: 12px;
		}
		.select2-selection__rendered{
			padding-top: 5px;
		}
		.select2-container--default .select2-selection--single{
			border: 1px solid #ced4da;
		}
		.select2-container--default{
			width: 100% !important;
		}
		.select2-selection__placeholder{
			color: #212529;
			font-size:  0.95rem;
		}
		.invalid-feedback{
			border: 1px solid #dc3545;
			display: block;
			margin-top: 0;
		}
		#trackers .td{
			vertical-align: middle;
		}
	</style>
@endsection

@section('content')
	<div id="page-content">
	    <div class="gradient-wrap">
	        <div class="container hero-section mb-4 mt-4">
	           	<div class="row">
	           		<div class="col-md-10 mt-4">
	                    <h1>Trips | <span id="page-subtitle">Flights</span></h1>
	                    <p id="booking-text">Your bookings and reservations will appear here</p>
	                    <p id="tracking-text">Prices you're tracking will appear here</p>
	                </div>
	            </div>
	            <div class="row">
	            	<div class="col-xl-9 col-md-8 order-lg-first order-md-last">
	            		<div class="tab-content" id="pills-tabContent">
							<div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
								@include('trips.panels.flights')
							</div>
							<div class="tab-pane fade" id="pills-funding" role="tabpanel" aria-labelledby="pills-profile-tab">
			                    @include('trips.panels.hotels')
			                </div>
	                    	<div class="tab-pane fade" id="pills-orders" role="tabpanel" aria-labelledby="pills-contact-tab">
	                    		@include('trips.panels.tracking')
	                    	</div>
						</div>
	            	</div>
	            	<div class="col-xl-3 col-md-4 order-lg-last order-md-first order-first mb-4">
	            		<div class="card bg-white">
	            			<div class="card-body">
	            				@include('trips.menu')	
	            			</div>
	            		</div>
	            	</div>
	            </div>
	        </div>
	    </div>
	</div>
@endsection

@section('js')
	<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
	@include('trips.trips-js')
@endsection