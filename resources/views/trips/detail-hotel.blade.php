@extends("layouts.curator-master")

@php
	$passengers = $order["guests"];
	$booking_reference = $duffelData["booking_reference"];
	$baggages = 0;
	$data = $duffelData;

	$i = 0;

	$canceledOn = $order["canceledOn"] ?? false;

	if($canceledOn){
		$canceledOn = Carbon\Carbon::parse($canceledOn)->timezone($user->customClaims["timezone"])->format("F d, Y @ g:i A");
	}

	$data["accommodation"] = $order["accommodation"];
	$data["check_in_date"] = $order["check_in_date"];
	$data["check_out_date"] = $order["check_out_date"];
	$data["id"] = $order["id"];

	$data["imgBlock"] = false;
	$data["showPricing"] = false;
	$data["adults"] = sizeof($order["guests"]);
	$data["rooms"] = $order["rooms"];

	$address = $order["accommodation"]["location"]["address"];

    $addressString = "";

    foreach($address as $key => $value){
        $addressString .= $value . "<br>";
    }

    $data["addressString"] = $addressString;


@endphp

@section('css')
	<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
	<style type="text/css">
		.btn-wide{
			width: 100%;
			margin: 0 auto;
			justify-content: center;
		}
		.image-frame:hover .enticer{
			display: flex;
			transition: 2s;
			cursor: pointer;
		}
		#flight-plan-pricing{
			display: none;
		}
		.nav-pills{
			display: block;
		}
		.full-width .nav-link{
			width: 100%;
			text-align: left;
		}
		.text-small{
			font-weight: inherit;
			font-size: 14px;
		}
		.select2{
			width: 100% !important;
		}
		.select2-container .select2-selection--single{
			height: 46px;
			border: 1px solid #ced4da;
			border-radius: .375rem;
		}
		.select2-container--default .select2-selection--single .select2-selection__arrow{
			height: 46px;
		}
		.select2-container--default .select2-selection--single .select2-selection__rendered{
			line-height: 46px;
		}
		.form-label{
			font-weight: bold;
		}
		@media(max-width: 768px){
			.hotel-thumbnail{
				border-top-left-radius: 0.375rem; border-top-right-radius: 0.375rem; 
				height: 220px; 
				width: 100%;
				background-position: center;
			}
			.hotel-card .col-md-5{
				padding: 20px 30px !important;
			}
			.hotel-card .col-md-4{
				padding-bottom: 20px !important;
			}
		}
		@media(min-width: 768px){
			.hotel-thumbnail{
				border-top-left-radius: 0.375rem; border-bottom-left-radius: 0.375rem;
				height: 220px;
				width:200px;
			}
		}
		.hotel-card-content img{
			max-width: 230px;
		}
		.hotel-card-content .col-md-5{
			width: 70%;
		}
	</style>
@endsection

@section('content')
	<div id="page-content">
	    <div class="gradient-wrap">
	        <div class="container hero-section mb-4 mt-4">
	           	<div class="row">
	           		<div class="col-md-10 mt-4">
	                    <h1>Reservation at {{ $order["accommodation"]["name"] }} | {{ $booking_reference }}</h1>
	                </div>
	            </div>
	            <div class="row">
	            	<div class="col-xl-9 col-md-8 order-lg-first order-md-last">
	            		<div class="row">
	            			<div class="col-md-12">
	            				<div class="tab-content" id="pills-tabContent">
									<div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
										@if($canceledOn)
											<div class="alert alert-danger">
												<p>This trip was canceled on {{ $canceledOn }}</p>
											</div>
										@endif
										@include("hotels.components.hotel-result",["embed" => true])
			                    	</div>
			                    	<div class="tab-pane fade" id="pills-funding" role="tabpanel" aria-labelledby="pills-profile-tab">
			                    		@include('trips.panels.hotel.passengers')
			                    	</div>
			                    	<div class="tab-pane fade" id="pills-orders" role="tabpanel" aria-labelledby="pills-contact-tab">
			                    		@include('trips.panels.hotel.order')
			                    	</div>
			                    	<div class="tab-pane fade" id="pills-insurance" role="tabpanel" aria-labelledby="pills-contact-tab">
			                    		@include('trips.panels.insurance')
			                    	</div>
			                    	<div class="tab-pane fade" id="pills-settings" role="tabpanel" aria-labelledby="pills-contact-tab">
			                    		@include('trips.panels.hotel.support')
			                    	</div>
			                    	<div class="tab-pane fade" id="pills-terms" role="tabpanel" aria-labelledby="pills-terms-tab">
			                    		@include('trips.panels.hotel.terms')
			                    	</div>
			                    </div>
	            			</div>
	            		</div>
	            	</div>
	            	<div class="col-xl-3 col-md-4 order-lg-last order-md-first order-first">
	            		<div class="row">
	            			<div class="col-md-12 mb-4">
	            				<h4>&nbsp;</h4>
	            				<div class="card bg-white">
	            					<div class="card-body">
            							@include("trips.panels.menu")
            						</div>	
			            		</div>
	            			</div>
	            		</div>
	            	</div>
	            </div>
	        </div>
	    </div>
	</div>
@endsection

@section('js')
	<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
	<script id="visitorscoverage_quote_widget" type="text/javascript" src="https://www.visitorscoverage.com/widgets/quotes/js/main.js"></script>
	<script>
		$(document).ready( function () {
	    	$.ajaxSetup({
		      headers: {
		          'X-CSRF-TOKEN': $("#csrf-token").val()
		      }
			});

			//getAvailableServices();

			var windowWidth = $(window).width();
			var themeSize = "large-theme3";

			if(windowWidth < 768){
				themeSize = "small-theme3";
			}

			$("#supportRequest").select2({
			    maximumSelectionLength: 3
			});

			$("#resendReceipt").click(function(){
				var btn = $(this);
				btn.addClass("disable-while-loading");
				$.ajax({
		            url: "{{ route('trip-resend-receipt') }}",
		            processData: true,
		            dataType: 'json',
		            data: {
		            	order: "{{ $duffelData['refID'] }}",
		            	type: "hotel"
		            },
		            type: 'post',
		            success: function (data) {
		            	// success message
		            	if(data == 200){
		            		swal({
					            icon: 'success',
					            title: 'Your email was sent',
					            text: "It will arrive from no-reply@venti.co and may be in your junk folder.",
					        });
		            	}
		            	btn.removeClass("disable-while-loading");
		            },
		            error: function (e){
		            	// error
		            	btn.removeClass("disable-while-loading");
		            }
			    });
			});

			$("#cancelTrip").click(function(){
				var btn = $(this);
				var cancelationReason = $("#cancelationReason").val();
				var cancelAccept = $("#cancelAccept:checked").val();
				btn.addClass("disable-while-loading");

				if(cancelationReason == "" || cancelationReason == undefined){
					btn.removeClass("disable-while-loading");
					swal({
			            icon: 'warning',
			            title: 'Please select cancelation reason',
			            text: "Your reason will not impact your ability to get refunded for your trip.",
			        });

			        return false;
				}

				if(cancelAccept != 1){
					btn.removeClass("disable-while-loading");
					swal({
			            icon: 'warning',
			            title: 'Agreement required',
			            text: "Your must check the box noting your understanding and acceptance of the quote",
			        });
			        return false;
				}

				$.ajax({
		            url: "{{ route('cancel-trip') }}",
		            processData: true,
		            dataType: 'json',
		            data: {
		            	order: "{{ $duffelData['refID'] }}",
		            	cancelationReason: cancelationReason
		            },
		            type: 'post',
		            success: function (data) {
		            	btn.removeClass("disable-while-loading");

		            	swal({
				            icon: 'success',
				            title: 'Trip canceled',
				            text: "You've successfully activated your Airlock to receive a refund for this trip. We've sent you a confirmation email. Expect your funds to arrive within three business days.",
				        });

				        btn.parent().remove();
				        
				        $("#pills-home").prepend('<div class="alert alert-danger"><p>This trip was canceled today.</p></div>');

				        return true;
		            },
		            error:function(){
		            	btn.removeClass("disable-while-loading");
		            }
			    });
			});

			$("#submitSupport").click(function(){
				var btn = $(this);
				var supportReason = $("#supportRequest").val();
				var supportMessage = $("#supportMessage").val();
				btn.addClass("disable-while-loading");

				if(supportReason == null || supportReason == "" || supportReason == undefined){
					// fields required
					swal({
			            icon: 'error',
			            title: 'Please check all required fields',
			            text: "",
			        });
			        
			        btn.removeClass("disable-while-loading");
					return false;
				}

				$.ajax({
		            url: "{{ route('trip-support') }}",
		            processData: true,
		            dataType: 'json',
		            data: {
		            	supportReason: supportReason,
		            	supportMessage: supportMessage,
		            	refID: "{{ $duffelData['refID'] }}"
		            },
		            type: 'post',
		            success: function (data) {
		            	// success message
		            	if(data == 200){
		            		$("#supportMessage").val("");
		            		$("#supportRequest").val("");

		            		swal({
					            icon: 'success',
					            title: 'Your support request was sent',
					            text: "We also sent you a confirmation email. It will arrive from no-reply@venti.co and may be in your junk folder. Our team will follow up with you there.",
					        });
		            	}
		            	btn.removeClass("disable-while-loading");
		            },
		            error: function (e){
		            	// error
		            	btn.removeClass("disable-while-loading");
		            }
			    });
			});

			

			function navigate(href, newTab) {
			   var a = document.createElement('a');
			   a.href = href;
			   if (newTab) {
			      a.setAttribute('target', '_blank');
			   }
			   a.click();
			}
		});
	</script>
@endsection