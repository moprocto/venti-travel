@extends('layouts.curator-master')

@php


$class = $data["cabin_class"];
$offers = $data["offers"] ?? [];
$owner = $data["offers"][0]["owner"] ?? [];

$carriers = [];

@endphp

@section('css')
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/switchery/0.8.2/switchery.min.css" integrity="sha512-uyGg6dZr3cE1PxtKOCGqKGTiZybe5iSq3LsqOolABqAWlIRLo/HKyrMMD8drX+gls3twJdpYX0gDKEdtf2dpmw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
	<style type="text/css">
		.form-small{
			padding: 0;
			font-size: 12px;
		}
		.no-bg{
			background-color: transparent;
			border: none;
		}
		.select2-container .select2-selection--single,.select2-container--default .select2-selection--single .select2-selection__clear,.select2-container--default .select2-selection--single .select2-selection__arrow{
			height: 38px;
			font-size: 12px;
		}
		.select2-selection__rendered{
			padding-top: 5px;
		}
		.select2-container--default .select2-selection--single{
			border: 1px solid #ced4da;
		}
		.datepicker{
			max-width: 170px;
			font-size:12px;
			height:38px;
			
		}
		.btn-primary{
			border-radius: 4px;
		}
		.value-button{
			display: inline-block;
		    border: 1px solid #ddd;
		    margin: 0px;
		    width: 40px;
		    height: 40px;
		    text-align: center;
		    vertical-align: middle;
		    padding: 7px 0;
		    background: #eee;
		    -webkit-touch-callout: none;
		    -webkit-user-select: none;
		    -khtml-user-select: none;
		    -moz-user-select: none;
		    -ms-user-select: none;
		    user-select: none;
		}
		.decrease{
			margin-right: -4px;
    		border-radius: 8px 0 0 8px;
		}
		.increase{
			margin-left: 0;
    		border-radius: 0 8px 8px 0;
		}
		.steppable{
			text-align: center;
		    border: none;
		    border-top: 1px solid #ddd;
		    border-bottom: 1px solid #ddd;
		    margin: 0px;
		    width: 40px;
		    height: 40px;
		}
		td.vertical-align{
			vertical-align: middle;
		}
		tr.borderless td{
			border-bottom: none !important;
		}
		.table,.card-body{
			padding-bottom: 0;
			margin: 0;
		}
		ul.nolist{
			list-style: none;
		}
		.text-small{
			font-size: 12px;
		}
		.results-table .text-left{
			font-size: 13px;
			font-weight: 700;
		}
		.results-table .text-right{
			font-size: 13px;
		}
		table h5, table h2, table h3{
			padding: 0;
			margin: 0 !important;
		}
		.table tr td{
			vertical-align: middle;
		}
		.card{
			background: transparent;
		}
		.bg-white{
			background-color: transparent;
			background-image: linear-gradient(112.1deg, rgba(248, 248, 248,0) 11.4%, rgba(255, 255, 255, 0.7) 70.2%);
		}
		.row_total td{
			border-bottom: none;
		}
		.bg-success > *, .bg-success .card-title{
			color: white;
		}
		.bg-white{
			cursor: pointer;
		}
		li strong{
			font-size: 13px;
		}
		.card-title .fa-check, .hide{
			display: none;
		}
		h5.card-title.selected .fa-check{
			display: inline-block;
			float: right;
		}
	</style>
@endsection

@section('content')
<div class="gradient-wrap" style="background: transparent; width:100%;">
    <!--  HERO -->
    <div class="section-wrapper black position-relative">
    	@if($offers && sizeof($offers) != 0)
    	<div class="row mb-3">
    		<div class="col-md-12">
    			<h4>Offers by {{ $owner["name"] }} </h4>
    			<p>Select one offer and review policies and baggage allotments before proceeding to checkout. Depending on the number of offers, this may take a few seconds as we have to verify each one with the airline(s).</p>
    		</div>
    		<div class="col-md-6">
	    		<div class="card" style="background:white">
	    			<div class="card-body">
	    			<h4>Flight Summary: {{ ucfirst(str_replace("_"," ",$class)) }}</h4>
	    			<ul>
						@foreach($data["slices"] as $slice)
							<li>Departing from {{ $slice["origin"]["city_name"] ?? $slice["origin"]["name"] }} on {{ \Carbon\Carbon::parse($slice["departure_date"])->format("m-d-Y") }}</li>
						@endforeach
					</ul>
					</div>
	    		</div>
    		</div>
    	</div>
    	@endif
		<div class="row mb-4">
			@php
				$s = 0;
			@endphp
			@foreach($offers as $offer)
				<div class="col-md-3 offer-card" data-offer="{{ $offer['id'] }}" id="{{ $offer['id'] }}">
					
					<div class="card bg-white mb-3">
						<div class="card-header">
							<i class="fa fa-sync fa-spin"></i>
						</div>
						
	    				<div class="card-body intro hide">
							@foreach($offer["slices"] as $slice)
								@php
									$cabinClass = str_replace(' ','_',strtolower($slice['fare_brand_name']));
									$conditions = $slice["conditions"];
									$carryOns = $checkedBags = 0;
									$discount = getDiscountPercent("airfare",$offer["total_amount"], "USD");

									$segments = $slice["segments"] ?? [];

									if($segments){
										$baggages = $slice["segments"][0]["passengers"][0]["baggages"];
										foreach($baggages as $baggage){
											if($baggage["type"] == "carry_on"){
												$carryOns = $baggage["quantity"];
											}
											if($baggage["type"] == "checked"){
												$checkedBags = $baggage["quantity"];
											}
										}
										$seatSelectable = seatableAirline($slice["segments"][0]['operating_carrier']['name']);
										$bagSelectable = baggableAirline($slice["segments"][0]['operating_carrier']['name']);

									}

									$isFlexible = false;

									if(
										str_contains($slice["fare_brand_name"], "Flex") || str_contains($slice["fare_brand_name"], "Flexible")
									){
										$isFlexible = true;
									}

									$baggableCarriers = [];
									foreach($slice["segments"] as $segment){
										if(baggableAirline($segment["operating_carrier"]["name"])){
											$baggableCarriers[$segment["operating_carrier"]["name"]] = [
												"name" => $segment["operating_carrier"]["name"],
												"url" =>$segment["operating_carrier"]["conditions_of_carriage_url"]
											];
										}
									}
									
									foreach($slice["segments"] as $segment){
										$carriers[$segment["operating_carrier"]["name"]] = [
											"name" => $segment["operating_carrier"]["name"],
											"url" =>$segment["operating_carrier"]["conditions_of_carriage_url"]
										];
									}
								@endphp
		    					<h5 class="card-title fare-offer @if($s == 0) selected @endif" id="{{ $cabinClass }}">
		    						<span>{{ $slice["fare_brand_name"] }}</span>
		    						<i class="fa fa-check"></i>
		    					</h5>
		    					<div>
		    						@if(sizeof($conditions) != 0)
		    							<ul style="list-style:none; padding:0">

		    								@foreach($offer["conditions"] as $key => $value)
		    									@if($key == "refund_before_departure")
		    										<li><strong>Refunds:</strong>
		    											<p class="text-small">
		    											@if(!is_null($value) && $value["allowed"] == true)
		    												@if($value["penalty_amount"] == "null" || $value["penalty_amount"] == "0.00")
	    														@if(!$isFlexible)
	    															Free within 48 hours of booking.
	    															<ul class="text-small">
		    														<li>Then {{ number_format($offer["total_amount"] * 0.10,2) }} {{ $value["penalty_currency"] }} penalty.</li>
		    														</ul>
		    													@else
		    														Free if canceling at least 72 hours before departure. Add flight protection to fully guard your purchase.
		    													@endif
		    													
		    												@else
		    													The airline imposes a {{ $value["penalty_amount"] }} {{ $value["penalty_currency"] }} penalty for cancelations.
		    												@endif
		    											@else
		    												This ticket is non-refundable. Without flight protection, your purchase is at risk if your travel plans change.
		    											@endif
		    											</p>
		    										</li>
		    									@endif
		    									@if($key == "change_before_departure")
		    										<li><strong>Changes:</strong>
		    											<p class="text-small">
		    											@if(!is_null($value) && $value["allowed"] == true)
		    												@if($value["penalty_amount"] == "null" || $value["penalty_amount"] == "0.00")
		    													@if(!$isFlexible)
		    														Changes to this fare are limited and may include additional fees for fare differences and airline imposed fees. You will have to contact Venti support after booking.
		    													@else
		    														Changes to this fare are unlimited prior to departure. You will have to contact Venti support after booking.
		    													@endif
		    												@else
		    													The airline imposes a fee of {{ $value["penalty_amount"] }} {{ $value["penalty_currency"] }} for any change to your flight, which may include additional fees if the price changes. You will have to contact Venti support after booking.
		    												@endif
		    											@else
		    												Changes to this ticket are not allowed by the carrier.
		    											@endif
		    											</p>
		    										</li>
		    									@endif
		    								@endforeach
		    								
		    							</ul>
		    							<div class="row">
		    								<div class="col-md-6">
		    									<strong>Carry Ons:</strong>
		    									<p class="text-small">{{ $carryOns }} bag(s) included per passenger</p>
		    								</div>
		    								<div class="col-md-6">
		    									<strong>Checked Bags:</strong>
		    									<p class="text-small">{{ $checkedBags }} bag(s) included per passenger</p>
		    								</div>
		    							</div>
		    							
		    						@endif
		    					</div>
		    					<h3 class="card-title">
		    						<input type="radio" name="offer" class="offer" value="{{ $offer['id'] }}" @if($class == $cabinClass) selected @endif> ${{ number_format($offer["total_amount"] * $discount,2) }}
		    					</h3>
		    					@php break; @endphp
							@endforeach
						</div>
					</div>
				</div>
				@php $s++; @endphp
			@endforeach
			
			@if(!$offers)
				<div class="col-md-4 mt-4">
					<div class="card bg-white">
						<div class="card-body">
							<h4>This deal has expired!</h4>
							<p>Inventory goes quickly. You'll need to restart your search to get the latest prices and availability.</p>
							<p>
							<a href="/flights" class="btn btn-black">Search Flights</a>
							</p>
						</div>
					</div>
				</div>
			@endif
		</div>	
		@if($offers)
			<div class="row">
				<div class="col-md-12">
					<div class="row">
						<div class="col-md-9 mb-3">
							<h5>Terms and Conditions</h5>
							<div class="card bg-white">
								<div class="card-body">
									<p>Prices displayed above are the lowest possible prices that can be achieved for this flight when using Boarding Pass Points. Each fare is subject to terms and conditions based on operating carrier and fare type, which includes carrier-specific refund and cancellation policies.  Venti's internal cancellation, refund, and flight protection terms are carrier agnostic. Visit the link(s) below to review the terms of each carrier that will be operating your trip:</p>
									<ul>
										@foreach($carriers as $carrier)
											<li><a href="{{ $carrier['url'] }}" target="_blank">{{ $carrier["name"] }}</a></li>
										@endforeach
									</ul>
								</div>
							</div>
						</div> 
					</div>
				</div>
				<div class="col-md-3">
					<button id="select" type="button" class="btn btn-primary btn-lg @if(env('APP_ENV') == 'production')isInterested @endif" style="width:100%;"><img src="/assets/img/icons/passenger.png" style="width:22px; margin-top:-5px;" />Continue</button>
					<p class="text-small">Next, you'll add passengers, baggage, and flight protection to finalize your order.</p>
				</div>
			</div>
		@endif
	</div>
</div>

@include('flights.components.cfar-explainer')

@endsection

@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/switchery/0.8.2/switchery.min.js" integrity="sha512-lC8vSUSlXWqh7A/F+EUS3l77bdlj+rGMN4NB5XFAHnTR3jQtg4ibZccWpuSSIdPoPUlUxtnGktLyrWcDhG8RvA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
	<script type="text/javascript">
			$.ajaxSetup({
				headers: {
			    	'X-CSRF-TOKEN': '{{ csrf_token() }}'
				}
		    });

		    markActiveFare();

		    function markActiveFare(){
		    	$(".fare-offer").each(function(){
		    		$(this).parent().parent().removeClass("bg-success");
		    		$(this).parent().parent().addClass("bg-white");
			    	if($(this).hasClass("selected")){

			    		$(this).parent().parent().removeClass("bg-white");
			    		$(this).parent().parent().addClass("bg-success");
			    		$(this).parent().parent().find(".offer").prop("checked",true);
			    	}
			    });
		    }

		    $("body").on("click", ".bg-white",function(){
		    	var card = $(this);
		    	$(".fare-offer").each(function(){
		    		$(this).removeClass("selected");
		    	});

		    	card.find(".fare-offer").addClass("selected");

		    	markActiveFare();
		    	console.log("looping");
		    });

		    $("#select").click(function(){
		    	var offer = $(".offer:checked").val();
		    	var url = "/flights/search/offer/" + offer + "/{{ $iata_destination }}/checkout";

		    	window.location.href = url;
		    });

		    var ajaxCalls = [];


		    $(".offer-card").each(function(){
		    	// we need to remove dead offers
		    	var offerID = $(this).attr("data-offer");

		    	var ajaxCall = $.ajax({
		            url: "{{ route('flight-offer-status') }}",
					type: 'POST',
			        dataType: 'json',
		            data:{
		                offer : offerID
		            },
	                success: function (data) {
	                	if(data == 500){
	                		$("#" + offerID).remove();
	                	} else{
	                		$("#" + offerID).find(".card-header").hide();
	                		$("#" + offerID).find(".card-body").show();
	                	}
	                },
	                error: function(data){

	                }
	            });

	            ajaxCalls.push(ajaxCall);


		    });

		    $.when.apply($, ajaxCalls).done(function() {
			    // Arguments are the responses of the Ajax calls
			    // Since we don't know the number of calls, we handle it as an array
			    var responses = arguments;

			    // Loop through the responses (each argument is a response array with [data, statusText, jqXHR])
			    $.each(responses, function(index, response) {
			        // Process each response from the Ajax calls
			        var responseData = response[0]; // The data from the response
			        console.log("Response from call " + (index + 1) + ":", responseData);
			        
			        // You can now utilize the responseData for further processing
			    });
			}).fail(function() {
			    // This function is called if any of the requests fail
			    console.log("An error occurred with one of the requests.");
			});
	</script>
@endsection

