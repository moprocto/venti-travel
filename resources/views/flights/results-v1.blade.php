@extends('layouts.curator-master')

@section('css')
	<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
	<style type="text/css">
		.form-small{
			padding: 0;
			font-size: 12px;
		}
		.no-bg{
			background-color: transparent;
			border: none;
		}
		.select2-container .select2-selection--single,.select2-container--default .select2-selection--single .select2-selection__clear,.select2-container--default .select2-selection--single .select2-selection__arrow{
			height: 38px;
			font-size: 12px;
		}
		.select2-selection__rendered{
			padding-top: 5px;
		}
		.select2-container--default .select2-selection--single{
			border: 1px solid #ced4da;
		}
		.datepicker{
			max-width: 170px;
			font-size:12px;
			height:38px;
			
		}
		.btn-primary{
			border-radius: 4px;
		}
		.value-button{
			display: inline-block;
		    border: 1px solid #ddd;
		    margin: 0px;
		    width: 40px;
		    height: 40px;
		    text-align: center;
		    vertical-align: middle;
		    padding: 7px 0;
		    background: #eee;
		    -webkit-touch-callout: none;
		    -webkit-user-select: none;
		    -khtml-user-select: none;
		    -moz-user-select: none;
		    -ms-user-select: none;
		    user-select: none;
		}
		.decrease{
			margin-right: -4px;
    		border-radius: 8px 0 0 8px;
		}
		.increase{
			margin-left: 0;
    		border-radius: 0 8px 8px 0;
		}
		.steppable{
			text-align: center;
		    border: none;
		    border-top: 1px solid #ddd;
		    border-bottom: 1px solid #ddd;
		    margin: 0px;
		    width: 40px;
		    height: 40px;
		}
		td.vertical-align{
			vertical-align: middle;
		}
		tr.borderless td{
			border-bottom: none !important;
		}
		.table,.card-body{
			padding-bottom: 0;
			margin: 0;
		}
		ul.nolist{
			list-style: none;
		}
		.text-small{
			font-size: 12px;
		}
		.results-table .text-left{
			font-size: 13px;
			font-weight: 700;
		}
		.results-table .text-right{
			font-size: 13px;
		}
		.card{
			background: transparent;
			background-image: linear-gradient(112.1deg, rgba(242, 242, 246,0) 11.4%, rgba(255, 255, 255, 0.7) 70.2%);
			border: none;
			box-shadow:
		      0 1px 1px hsl(0deg 0% 0% / 0.015),
		      0 2px 2px hsl(0deg 0% 0% / 0.015),
		      0 4px 4px hsl(0deg 0% 0% / 0.015),
		      0 8px 8px hsl(0deg 0% 0% / 0.015),
		      0 16px 16px hsl(0deg 0% 0% / 0.015)
		    ;
		}
		
		@media(min-width:786px){
			.flight-card-content{
				display: flex;
				align-items: center;
			}
			.boarding-pass-footer{
				padding: 0 10px;
				font-size: 11px;
				margin: 0;
				max-width: 170px;
			}
			.checked-bags{
				line-height: 0.9;
				display:block;
				max-width: 155px;
				margin:0 auto;
			}
			#sorter-row{
				margin-top: -22px;
			}
		}
		#sorter-row,#filters-menu{
			display: none;
		}
		@media(max-width:785px){
			#filters-menu{
				margin-top: 0 !important;
			}
			.flight-card-content{
				display: block;
				align-items: center;
			}
			.flight-card .table{
				width: 100% !important;
			}
			.flight-card .table h5{
				font-size: 1rem;
			}
			.flight-card .table img{
				max-width: 40px !important;
			}
			.carrier-data{
				max-width: 80px !important;
			}
			.table .text-small{
				font-size: 9px;
			}
			.duration-info{
				width: 100px;
			}
			.boarding-pass-footer{
				padding: 0 10px;
				font-size: 11px;
				margin: 0;
				max-width: inherit;
			}
			.checked-bags{
				line-height: 0.9;
				display:block;
				max-width: 155px;
				margin:0 auto;
				margin-top: 20px;
			}
			.section-wrapper.black{
				margin-top: 50px;
			}
			#sorter-row{
				margin-top: 22px;
				padding-top: 5px !important;
			}

		}
		.remove{
			display: none !important;
		}
		

		button.accordion-button{
			line-height: 2;
    		font-weight: 400 !important;
    		font-family: 'sofia';
    		border-top-left-radius: 0.375rem;
    		border-top-right-radius: 0.375rem;
    		padding: 10px 10px 5px 10px;
		}
		.accordion-item{
			border-radius: 0.375rem;
    		border: none;
		}

		.accordion-item,.accordion-button:not(.collapsed){
			background-color: white;
		}
		.accordion-item{
			border: none;
		}
	</style>
@endsection

@section('content')
<div class="gradient-wrap" style="background: transparent; width:100%;">
    <!--  HERO -->
    <div class="section-wrapper black position-relative">
    	<div class="row">
    		<div class="col-md-4">
    			<div class="row">
    				<div class="col-md-12">
    					<div class="accordion mb-3" id="accordionExample" style="width:100%; padding:0;">
						  <div class="accordion-item bg-shadow" style="border-radius: 0.375rem">
						    <h2 class="accordion-header" id="headingOne" style="border-radius: 10px;">
						      <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne" style="border-top-left-radius:0.375rem; border-top-right-radius:0.375rem;">My Search</button>
						    </h2>
						        <div id="collapseOne" class="accordion-collapse collapse show" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
						            <div class="accordion-body">
						            	<table class="table results-table">
		    						<tbody>
		    							<tr>
		    								<td class="text-left">From:</td>
		    								<td class="text-right">{{ $request["from"] }}</td>
		    							</tr>
		    							<tr>
		    								<td class="text-left">To:</td>
		    								<td class="text-right">{{ $request["to"] }}</td>
		    							</tr>
		    							<tr>
		    								<td class="text-left">Departure:</td>
		    								<td class="text-right">{{ Carbon\Carbon::parse($request["start"])->format("l, M. d, Y") }}</td>
		    							</tr>
		    							@if($request["type"] == "round-trip")
			    							<tr>
			    								<td class="text-left">Return:</td>
			    								<td class="text-right">{{ Carbon\Carbon::parse($request["end"])->format("l, M. d, Y") }}</td>
			    							</tr>
		    							@endif
		    							<tr>
		    								<td class="text-left">Class:</td>
		    								<td class="text-right">{{ str_replace("Premium_economy","Premium Economy",ucfirst(str_replace("-"," ",$request["class"]))) }}</td>
		    							</tr>
		    							<tr>
		    								<td class="text-left">Adults:</td>
		    								<td class="text-right">{{ $request["adults"] }}</td>
		    							</tr>
		    							@if(isset($request["children"]) && $request["children"] > 0)
			    							<tr>
			    								<td class="text-left">Children:</td>
			    								<td class="text-right">{{ $request["children"] }}</td>
			    							</tr>
		    							@endif
		    							@if(isset($request["infants"]) && $request["infants"] > 0)
			    							<tr>
			    								<td class="text-left">Infants:</td>
			    								<td class="text-right">{{ $request["infants"] }}</td>
			    							</tr>
		    							@endif
		    						</tbody>
		    					</table>
		    					<br>
		    					
						            </div>
						        </div>
						   </div>
						</div>
    					<a href="/flights" class="btn btn-black mb-4">New Search</a>
    				</div>
    			</div>
    			<div class="row mt-4" id="filters-menu">
    				<div class="col-md-12">
    					<div class="accordion mb-3" id="accordionExample2" style="width:100%; padding:0;">
						  <div class="accordion-item bg-shadow" style="border-radius: 0.375rem">
						    <h2 class="accordion-header" id="headingTwo" style="border-radius: 10px;">
						      <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo" style="border-top-left-radius:0.375rem; border-top-right-radius:0.375rem;">Filter Results</button>
						    </h2>
						        <div id="collapseTwo" class="accordion-collapse collapse show" aria-labelledby="headingOne" data-bs-parent="#accordionExample2">
						            <div class="accordion-body">
						            	<div id="flight-filters"></div>
						            </div>
						        </div>
						   </div>
						</div>
						<p class="text-small">Currently in Beta. Better filters will be added in due time.</p>
    				</div>
    			</div>
    		</div>
    		<div class="col-md-8">
				<div class="mb-2" id="sorter-row" style="padding-top:23px;">
					<div style="flex-direction: row; display: flex; align-items: center; justify-content: space-between;">
						<div style="width: 100%; max-width: 200px">
							<select class="form-control bg-shadow" id="sorter" style="border:none;">
								<option value="lowest">Sort by Lowest Price</option>
								<option value="fastest">Sort by Fastest Travel</option>
							</select>
						</div>
						<div>
							<span style="color: grey; font-size: 11px; padding: 5px 20px; text-align: right;">All Times Local</span>
						</div>
					</div>
				</div>
				<div id="flights" style="max-height:900px; overflow-y:scroll"><div id="flights-loader" style="width: 300px; margin: 0 auto; text-align:center;"><img src="/assets/img/airplane.gif" style="width:100%; min-width:200px;"><h5>Searching for flights...</h5></div></div>
    		</div>
    	</div>
    </div>
</div>

@endsection

@section('js')
	<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
	<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
	<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
	<script src="https://unpkg.com/@popperjs/core@2"></script>
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
	<script type="text/javascript">
			$.ajaxSetup({
				headers: {
			    	'X-CSRF-TOKEN': '{{ csrf_token() }}'
				}
		    });

		    showFlightsLoad();


			var windowWidth = $(window).width();
			   if(windowWidth < 768){
			      
			      $(".collapse").each(function(){
			         $(this).removeClass('show');
			      });
			    }

			$("#flipLocations").click(function(){
				var flightFrom = $('#flight-from').select2('data')[0];
				var flightTo = $('#flight-to').select2('data')[0];

				$("#flight-from").select2('destroy');
				clearOptions("flight-from");
				$('#flight-from').append('<option value="1">' + flightTo.text + '</option>')
				$("#flight-from").select2();

				initFlightFrom();

				$("#flight-to").select2('destroy');
				clearOptions("flight-to");
				$('#flight-to').append('<option value="1">' + flightFrom.text + '</option>')
				$("#flight-to").select2();

				initFlightTo();
			});

			$('#search').click(function(){
				var flightFrom = $('#flight-from').select2('data')[0];
				var flightTo = $('#flight-to').select2('data')[0];
				var flightType = $('.flight-type[type=radio]:checked').val();
				var flightClass = $('#flight-class').val();
				var flightStartDate = $('.datepicker').val().split(" - ")[0];
				var flightReturnDate = $('.datepicker').val().split(" - ")[1];


				var adults = $("#adultNumber").val();
				var children = $("#childrenNumber").val();
				var infants = $("#infantsNumber").val();

			  	var fieldCheck = checkMandatoryFields(flightFrom, flightTo, flightType, flightClass, flightStartDate, flightReturnDate, adults, children, infants);

			  	if(fieldCheck === true){
			  		window.location.href = "/flights/search" +
			  		"?from=" + flightFrom.text + 
			  		"&to=" + flightTo.text + 
			  		"&type=" + flightType + 
			  		"&class=" + flightClass + 
			  		"&start=" + flightStartDate +  
			  		"&end=" + flightReturnDate + 
			  		"&adults=" + adults + 
			  		"&children=" + children + 
			  		"&infants=" + infants
			  	}
			});

			

			function checkMandatoryFields(flightFrom, flightTo, flightType, flightClass, flightStartDate, flightReturnDate, adults, children, infants){
				console.log(flightFrom);

				if(isBlank(flightFrom.text) || flightFrom.text == "Select Origin"){
					swal({
	                      icon: 'error',
	                      title: 'Missing required fields',
	                      text: 'Please add where you are flying from',
		            });

		            return false;
				}

				if(isBlank(flightTo.text) || flightTo.text == "Select Destination"){
					swal({
	                      icon: 'error',
	                      title: 'Missing required fields',
	                      text: 'Please add where you are flying to',
		            });

		            return false;
				}

				if(isBlank(flightType)){
					swal({
	                      icon: 'error',
	                      title: 'Missing required fields',
	                      text: 'You need to select your flight type',
		            });

		            return false;
				}

				if(isBlank(flightClass)){
					swal({
	                      icon: 'error',
	                      title: 'Missing required fields',
	                      text: 'You need to select your seat class',
		            });

		            return false;
				}

				if(adults == 0 && children == 0 && infants == 0){
					swal({
	                      icon: 'error',
	                      title: 'Missing required fields',
	                      text: 'You need to input at least one traveler',
		            });
		            return false;
				}
				if(infants >= 1 && adults == 0){
					swal({
	                      icon: 'error',
	                      title: 'Missing required fields',
	                      text: 'You need to input at least one adult',
		            });
		            return false;
				}

				return true;
			}

			function isBlank(value){
				if(value == "" || value == undefined || value == null){
					return true;
				}
				return false;
			}

			function clearOptions(elementId) {
			   var courseSelect = document.querySelector("#" + elementId);

			   while (courseSelect.childNodes[0] != null) {
			       courseSelect.childNodes[0].remove();
			   }
			}

			var dayDif = 0;

			$(".flight-type[type=radio]").click(function(){
				var flightStartDate = $('.datepicker').val().split(" - ")[0];
				var flightReturnDate = $('.datepicker').val().split(" - ")[1];
				if(!isBlank(flightReturnDate)){
					dayDif = moment(flightReturnDate).diff(moment(flightStartDate), 'days', false);

				}
				
				if($(this).val() == "one-way"){
					$('.datepicker').daterangepicker({
					    locale: {
					          format: 'MM/DD/YYYY',
					    },
					    singleDatePicker: true,
					    showDropdowns: true,
					    startDate: flightStartDate,
					    drops: "up",
						opens:"left",
						minDate: moment().format("MM/DD/YYYY"),
						maxDate: moment().add(10, 'M').format("MM/DD/YYYY")
					});
				} else {

					$('.datepicker').daterangepicker({
					    locale: {
					          format: 'MM/DD/YYYY',
					    },
					    showDropdowns: true,
					    startDate: flightStartDate,
					    endDate: (dayDif > 0) ? moment(flightStartDate).add(dayDif, 'days').format("MM/DD/YYYY") : flightReturnDate,
					    drops: "up",
						opens:"left",
						minDate: moment().format("MM/DD/YYYY"),
						maxDate: moment().add(10, 'M').format("MM/DD/YYYY")
					});
				}
			});

		$('input[name="dates"]').daterangepicker({
			drops: "up",
			opens:"left",
			minDate: moment().format("MM/DD/YYYY"),
			maxDate: moment().add(10, 'M').format("MM/DD/YYYY")
		});

		function initFlightFrom(){
			return $("#flight-from").select2({
			    placeholder: "Select Origin",
			    minimumInputLength: 3,
			    language: {
				    'inputTooShort': function () {
				        return '';
				    }
				},
			    delay:250,
			    data: function (term) {
		            return {
		                term: term
		            };
		        },
				ajax: {
				    url: '/flights/data/airports',
				    dataType: 'json',
			        processResults: function (data) {

			            return {

			                results: $.map(data, function (item) {

			                	var item = JSON.parse(item);


			                	return item.map((airport, i) => {

			                		return {
				                        text: airport.text,
				                        id: airport.id,
				                    }
			                	});
			                })
			            };
			        }
				}
			});
		}

		function initFlightTo(){
			return $("#flight-to").select2({
		    placeholder: "Select Destination",
		    delay:250,
		    minimumInputLength: 3,
		    data: function (term) {
	            return {
	                term: term
	            };
	        },
			ajax: {
			    url: '/flights/data/airports',
			    dataType: 'json',
		        processResults: function (data) {
		            return {
		                results: $.map(data, function (item) {
		                	var item = JSON.parse(item);

		                	return item.map((airport, i) => {

		                		return {
			                        text: airport.text,
			                        id: airport.id
			                    }
		                	});
		                })
		            };
		        }
			}
		});
		}


		// get flights data

		getFlights();

		$('body #flight-filters').on("click",'.filter-carriers',function() {
			var carriers = [];
			$(".filter-carriers").each(function(){
				if($(this).is(":checked")){
					carriers.push($(this).val());
				}
			});

			console.log(carriers);

			$(".flight-slice").each(function(){
				var flightCard  = $(this).closest(".flight-card");
				if(carriers.length == 0){
					$(this).parent().parent().parent().parent().parent().removeClass("remove");
				} else {

					// loop through each row of enclosed table

					var tableCarriers = [];
					var flightSlices = $(this).parent().parent().parent().parent().find(".flight-slice");

					$(flightSlices).each(function(){
						tableCarriers.push($(this).attr("data-carrier"));
					});

					let isFound = tableCarriers.some( ai => carriers.includes(ai) );

					if(!isFound){
						$(this).parent().parent().parent().parent().parent().addClass("remove");
					}
					else{
						$(this).parent().parent().parent().parent().parent().removeClass("remove");
					}
				}
			});

			filterTripsEval();
        });

        function filterTripsEval(){
        	var trips = [];

        	$(".filter-trips").each(function(){
				if($(this).is(":checked")){
					trips.push($(this).val());
				}
			});

			$(".flight-card").each(function(){
				var trip = $(this).attr("data-total-flights");
				
				if(trips.length == 0){
					$(this).show();
				} else {
					if(!trips.includes(trip)){
						$(this).hide();
					}
				}
			});
        }

        $('body #flight-filters').on("click",'.filter-trips',function() {
			filterTripsEval();
        });

		function getFlights(){
			var url = "{{ str_replace('&amp;','&',$path) }}";
			url = url.replace(/&amp;/g, '&');

			$.ajax({
                url: "/flights/search/results" + url,
                type: 'post',
                processData: true,
              	dataType: 'json',
                success: function (data) {
                	console.log(data.length)
                	if(data.length == 0){
                		return swal.fire({
									title: 'Nothing found',
									text: 'We could not find any flights. This usually happens because the seat class is either unavailable or there are no flights from the airport of origin. Please try changing your trip plan by starting a new search.',
						imageUrl: '/assets/img/404.png',
						imageWidth: 210,
						imageHeight: 130,

						showCancelButton: true,
						imageAlt: 'Custom image',
						

		  				confirmButtonText: 'New Search',
					}).then((result) => {
					  if (result.isConfirmed) {
					  	window.location.href = "/flights"
					  }
					});
			        }


                	dismissFlightsLoad();

                	$.when(
	                    $.each(data, function(i, item) {
	                        $("#flights").append(item);
	                    })
		            ).then(function () {
			               getCarriers();
			        });  

			        
            	},
            	error: function(data){
            		$("#loaderText").html("<a class='btn btn-black' style='width:100px; margin:0 auto;' href='" + window.location.href + "'>Try Again</a>");
            		return swal.fire({
								icon: 'warning',
								title: 'Something went wrong',
								text: 'There was an issue pulling flights from the sky. Please refresh the page or start a new search.',
							confirmButtonText: 'Refresh',
							showCancelButton: true,
						}).then((result) => {
						  if (result.isConfirmed) {
						  	window.location.href = window.location.href
						  }
						});
            	}
            });
		}

		function showFlightsLoad(){
			$("#flights").html('<div id="flights-loader" style="width: 300px; margin: 0 auto; text-align:center;"><img src="/assets/img/airplane.gif" style="width:100%; min-width:200px;"><h5 id="loaderText">Searching for flights...</h5></div>');
		}

		function dismissFlightsLoad(){
			$("#flights-loader").remove();
		}

		function getCarriers(){
			$("#sorter-row").show();
			$("#filters-menu").show();

			var carriers = [];

			$(".flight-slice").each(function(){
				var carrier = $(this).attr("data-carrier");
				carriers.push(carrier);
			});

			carriers = carriers.filter(function (value, index, array) { 
			  return array.indexOf(value) === index;
			});

			$("#flight-filters").append("<h5>By Operating Carrier</h5><ul style='list-style:none !important;' id='flight-filters-carriers'>");

			for(i = 0; i < carriers.length; i++){
				
				$("#flight-filters-carriers").append("<li><input type='checkbox' class='filter-carriers' value='" + carriers[i] +"'/> " + carriers[i] +" </li>");
				
			}

			$("#flight-filters").append("</ul>");

			getPlanes();
		}

		$("body").on("click",".isInterested",function(){
			Swal.fire({
				title: 'Get Your Boarding Pass',
				text: 'We appreciate your interest in this flight. At this time, booking will be limited to verified Boarding Pass members. Members can save up to 20% on their flights when they use their travel points earned through our savings program. Click the Learn More button to see how you can become a VIP.',
				imageUrl: '/assets/img/icons/boarding-pass.png',
				imageWidth: 100,
				imageHeight: 100,
				imageAlt: 'Custom image',
				confirmButtonText: 'Learn More',
  				denyButtonText: 'Cancel'
			}).then((result) => {
			  if (result.isConfirmed) {
			  	window.location.href = "/"
			  }
			})
		});

		$("#sorter").change(function(){
			var sorter = $(this).val();
			sortResults(sorter);
		});

		function sortResults(sortType){
			if(sortType == "fastest"){
				var result = $('.flight-card').sort(function (a, b) {
			      var contentA =parseInt( $(a).data('total-duration'));
			      var contentB =parseInt( $(b).data('total-duration'));
			      return (contentA < contentB) ? -1 : (contentA > contentB) ? 1 : 0;
			   });
			} else {
				var result = $('.flight-card').sort(function (a, b) {
			      var contentA =parseInt( $(a).data('default-order'));
			      var contentB =parseInt( $(b).data('default-order'));
			      return (contentA < contentB) ? -1 : (contentA > contentB) ? 1 : 0;
			   });
			}
			$('#flights').html(result);
			$("#flights").animate({scrollTop: $("#flights").offset().top});
		}

		function getPlanes(){
			// to allow filtering but number of number of stops
			$("#flight-filters").append("<h5>By Total Trips</h5><ul style='list-style:none !important;' id='flight-filters-trips'>");

			var stops = [];

			$(".flight-card").each(function(){
				var stop = $(this).attr("data-total-flights");
				stops.push(stop);
			});

			stops = stops.filter(function (value, index, array) { 
			  return array.indexOf(value) === index;
			});

			stops = stops.sort();

			for(i = 0; i < stops.length; i++){
				$("#flight-filters-trips").append("<li><input type='checkbox' class='filter-trips' value='" + stops[i] +"'/> " + stops[i] +" Trips</li>");
			}

			$("#flight-filters").append("</ul>");
		}

		setTimeout(function(){
			setExpiry();
		}, 600000);

		function setExpiry(){
			// after 10 minutes, all deals are marked as expired.

			swal.fire({
					icon: 'warning',
					title: 'Offers expired',
					text: 'Airline prices move fast! To ensure you see the latest prices and availability, please refresh the page',
				confirmButtonText: 'Refresh',
				showCancelButton: false,
			}).then((result) => {
			  if (result.isConfirmed) {
			  	window.location.href = window.location.href
			  }
			});

			$(".detailsButton").each(function(){
				$(this).removeClass("btn-primary");
				$(this).addClass("btn-warning");
				$(this).text("Deal Expired");
				$(this).attr("href","#");
				$(this).attr("target","");
				$(this).attr("disabled","disabled");
			});
		}

	</script>
@endsection