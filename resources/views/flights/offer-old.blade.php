@extends('layouts.curator-master')

@php

$baggages = $total_amount = $base_amount = $tax_amount = 0;

// we need to add up the cost of all fares

foreach($data["slices"] as $slice){
	$total_amount += $slice["total_amount"];
	$base_amount += $slice["base_amount"];
	$tax_amount += $slice["tax_amount"];
}

$markup = 0;
$i = 0;
$destination = ["name" => ""];
$numSlices = sizeof($data["slices"]); // more than one slice indicates round trip
$flightType = "one-way";
if($numSlices > 1){
	$flightType = "round trip";
}
$duration = "";

$supportedDocs = [];

foreach($data["supported_passenger_identity_document_types"] as $doc){
	if(!is_null($doc)){
		array_push($supportedDocs, ucfirst(str_replace("_"," ",$doc)));
	}
}

$refundAllowed = $data["conditions"]["refund_before_departure"]["allowed"] ?? false;
$refundPenalty = $data["conditions"]["refund_before_departure"]["penalty_amount"] ?? false;
$changesAllowed = $data["conditions"]["change_before_departure"]["allowed"] ?? false;
$changePenalty = $data["conditions"]["change_before_departure"]["penalty_amount"] ?? false;

$requires_instant_payment = $data["payment_requirements"]["requires_instant_payment"];

$cabin_class = "economy";

if($priorSearch){
	$cabin_class = $priorSearch["class"];
}
$destination = ["name" => ""];
$numSlices = sizeof($data["slices"]); // more than one slice indicates round trip
if($numSlices == 1){
	$destination = $data["slices"][0]["segments"][0]["destination"];
}else{
	$destination = $data["slices"][1]["segments"][0]["origin"];
}

$available_services = $data["available_services"];

foreach($available_services as $available_service){

}

@endphp

@section('css')
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/switchery/0.8.2/switchery.min.css" integrity="sha512-uyGg6dZr3cE1PxtKOCGqKGTiZybe5iSq3LsqOolABqAWlIRLo/HKyrMMD8drX+gls3twJdpYX0gDKEdtf2dpmw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
	<style type="text/css">
		.form-small{
			padding: 0;
			font-size: 12px;
		}
		.no-bg{
			background-color: transparent;
			border: none;
		}
		.select2-container .select2-selection--single,.select2-container--default .select2-selection--single .select2-selection__clear,.select2-container--default .select2-selection--single .select2-selection__arrow{
			height: 38px;
			font-size: 12px;
		}
		.select2-selection__rendered{
			padding-top: 5px;
		}
		.select2-container--default .select2-selection--single{
			border: 1px solid #ced4da;
		}
		.datepicker{
			max-width: 170px;
			font-size:12px;
			height:38px;
			
		}
		.btn-primary{
			border-radius: 4px;
		}
		.value-button{
			display: inline-block;
		    border: 1px solid #ddd;
		    margin: 0px;
		    width: 40px;
		    height: 40px;
		    text-align: center;
		    vertical-align: middle;
		    padding: 7px 0;
		    background: #eee;
		    -webkit-touch-callout: none;
		    -webkit-user-select: none;
		    -khtml-user-select: none;
		    -moz-user-select: none;
		    -ms-user-select: none;
		    user-select: none;
		}
		.decrease{
			margin-right: -4px;
    		border-radius: 8px 0 0 8px;
		}
		.increase{
			margin-left: 0;
    		border-radius: 0 8px 8px 0;
		}
		.steppable{
			text-align: center;
		    border: none;
		    border-top: 1px solid #ddd;
		    border-bottom: 1px solid #ddd;
		    margin: 0px;
		    width: 40px;
		    height: 40px;
		}
		td.vertical-align{
			vertical-align: middle;
		}
		tr.borderless td{
			border-bottom: none !important;
		}
		.table,.card-body{
			padding-bottom: 0;
			margin: 0;
		}
		ul.nolist{
			list-style: none;
		}
		.text-small{
			font-size: 12px;
		}
		.results-table .text-left{
			font-size: 13px;
			font-weight: 700;
		}
		.results-table .text-right{
			font-size: 13px;
		}
		table h5, table h2, table h3{
			padding: 0;
			margin: 0 !important;
		}
		.table tr td{
			vertical-align: middle;
		}
		.card{
			background: transparent;
		}
		.bg-white{
			background-color: transparent;
			background-image: linear-gradient(112.1deg, rgba(248, 248, 248,0) 11.4%, rgba(255, 255, 255, 0.7) 70.2%);
		}
		.row_total td{
			border-bottom: none;
		}
	</style>
@endsection

@section('content')
<div class="gradient-wrap" style="background: transparent; width:100%;">
    <!--  HERO -->
    <div class="section-wrapper black position-relative">
    	<div class="row">
    		<div class="col-md-8 justify-content-center">
    			<h4>Flight Offer <span class="text-small">(Expires in  
    						{{ Carbon\Carbon::parse($data["expires_at"])->diffInMinutes(\Carbon\Carbon::now()) }} minutes)</span></h4>
    			<div class="card bg-white mb-3">
					<div class="card-body intro">
						<p>Your {{ $flightType }} flight offer to <span class="destination"></span> is presented below. All times are local and are approximations based on data provided.</p>
					</div>
    			</div>
    			@include('flights.components.flight-plan')
    		</div>
    		<div class="col-md-4">
    			@if($cabin_class != "economy")<h4>Without Boarding Pass</h4>@else <h4>Price Breakdown</h4>@endif
    			@include('flights.components.offer-price-table')

    			<a id="select" @if(env('APP_ENV') == 'local') href="./{{ $destination['iata_code'] }}/checkout/1" @else href="#" @endif class="btn btn-primary btn-lg @if(env('APP_ENV') == 'production')isInterested @endif" style="width:100%;"><img src="/assets/img/icons/passenger.png" style="width:22px; margin-top:-5px;" /> Add Passengers</a>
				<div class="card bg-white mt-3">
					<div class="card-body">
						<p><strong>From {{ $data["owner"]["name"] }}:</strong><br>
						@if(!$refundAllowed)Refunds for this flight are not allowed without the purchase of trip insurance.
						@else
							Refunds for this flight are allowed when you cancel within 48 hours.
							@if((int) $refundPenalty > 0)
								The cancellation fee is ${{ (float) $refundPenalty }}.
							@endif
						@endif
						@if(!$changesAllowed) Changes to this flight are not allowed without the purchase of trip insurance.
						@else
							Changes to this flight are allowed, but there may be fees depending on when the change is requested.
						@endif
						</p>
					</div>
				</div>
    		</div>
    	</div>
    </div>
</div>

@include('flights.components.cfar-explainer')
@include('flights.components.amenities');

@endsection

@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/switchery/0.8.2/switchery.min.js" integrity="sha512-lC8vSUSlXWqh7A/F+EUS3l77bdlj+rGMN4NB5XFAHnTR3jQtg4ibZccWpuSSIdPoPUlUxtnGktLyrWcDhG8RvA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
	<script type="text/javascript">
			$.ajaxSetup({
				headers: {
			    	'X-CSRF-TOKEN': '{{ csrf_token() }}'
				}
		    });

		    $(".destination").each(function(){
		    	$(this).text("{{ $destination['city_name'] }}");
		    });


			var elem = document.querySelector('.js-switch');

			var switchery = new Switchery(elem, { size: 'small' });

			$(".switchery").click(function(){
				var switcher = $(this);
				
				if($("#protection").hasClass("disabledEconomy")){
					return Swal.fire({
				title: 'Cannot be removed',
				icon: "warning",
				text: 'We appreciate your interest in this flight. At this time, We do not allow the purchase of economy fligths without flight protection.',
				confirmButtonText: 'Okay',
  				denyButtonText: 'Cancel'
			}).then((result) => {
			  
			});
				}
				
				var cfar = elem.checked;
				calculateTotals(cfar);
				var href = $("#select").attr("href");

				if(!cfar){
					href =  href.replace("/1","/0");
				} else {
					href =  href.replace("/0","/1");
				}

				$("#select").attr("href",href);
			});

			function calculateTotals(cfar){
				var markUp = parseFloat({{ $total_amount * .045 }});
				console.log("Mark Up: " + markUp);
				var total_amount = parseFloat({{ $total_amount }});
				console.log("Base: " + total_amount);
				var protectionFee = total_amount * 0.2; // 
				console.log("Protection Fee: " + protectionFee);
				var subtotal = total_amount + protectionFee;

				var platformFeeBase = total_amount * 0.03;

				console.log("Platform Fee Base: " + platformFeeBase);

				var creditCardFeeRate = parseFloat(0.029);
				var creditCardFeeFlat = 2;

				var creditCardFee = ((subtotal + platformFeeBase) * creditCardFeeRate) + creditCardFeeFlat;

				console.log("Credit Card Fee: " + creditCardFee);

				subtotal = subtotal + platformFeeBase;

				console.log("Subtotal: " + subtotal);
				
				var platformFee = Math.round(creditCardFee + platformFeeBase,2);

				console.log("Platform Fee" + platformFee);
	
				var platformFee = $("#platformFee").val();



				if(!cfar){
					// they've opted out of CFAR, so we have to re-calculate the platform fee.
					$("#protectionOn").html("0");
					$("#protectionDiscountOn").html("0");

					creditCardFee = Math.round(( ((total_amount + platformFeeBase) * creditCardFeeRate) + creditCardFeeFlat + markUp ) * 100)/100; 

					var newTotal = Math.round((total_amount + creditCardFee) * 100)/100;

					$(".platformFee").each(function(){
						$(this).text(creditCardFee);
					});

					$("#onDiscount").text((Math.round((newTotal - creditCardFee) * 100)/100).toLocaleString(undefined, {minimumFractionDigits: 2}));


					$("#newTotal").text(newTotal.toLocaleString(undefined, {minimumFractionDigits: 2}));

					$(".pointsFinal").each(function(){
						$(this).text(creditCardFee);
					});

				} else {
					

					$(".platformFee").each(function(){
						$(this).text(platformFee);
					});

					$("#total-cost").text(total_amount);
					$("#newTotal").text($("#total-cost").val());

					$("#onDiscount").text($("#discounted-price").val());

					$("#protectionOn").html($("#protectionFee").val());
					$("#protectionDiscountOn").html($("#protectionDiscount").val());


					$(".pointsFinal").each(function(){
						$(this).text($("#total-discount").val());
					});
				}

				
			}

	</script>
@endsection

