<h4>Who is Flying?</h4>
<p>Before placing your order, {{ $data["owner"]["name"] }} requires us to collect information from each passenger. For each passenger, confirm the following:</p>
<ul class="mb-3">
	<li>• All information matches identifying documents. {{ $data["owner"]["name"] }} reserves the right to ask for such documentation at any time before departure. <strong>If the name for you is incorrect, please contact admin@venti.co prior to booking.</strong></li>
	<li>• Each passenger has the appropriate carry on and checked-bags for their journey. @if($canAddBags) {{ $data["owner"]["name"] }} is allowing each passenger to add additional checked bags at this step. If passengers need additional baggage, Venti will add bags for a fee of {{ $checkedBagFee . " " . $checkedBagCurrency}}.@endif</li>
</ul>
@php $p = 1; @endphp
<div class="accordion" id="accordionExample">
	@foreach($passengers as $passenger)
		@php
			$pax_fname = $pax_lname = "";
			$user = Session::get('user');

			if($p == 1){
				// the first passenger must be the logged in user
				$pax_fname = getFirstName($user->displayName);
				$pax_lname = getLastName($user->displayName);
			}
			$passengerBags = getBaggagesByPassengerId($data["slices"], $passenger['id']);
		@endphp
		<div class="passenger-card mb-3" id="form-{{$passenger['id']}}">
			<div class="">
				<div class="accordion-item">
				    <h2 class="accordion-header" id="heading{{ $p }}">
				      <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapse{{ $p }}" aria-expanded="true" aria-controls="collapse{{ $p }}">
				        Passenger {{ $p }}: &nbsp;<span class="accordion-PaxName">@if($p == 1) {{ $pax_fname }} {{ $pax_lname }} @endif</span> &nbsp;({{ ucfirst($passenger["type"]) }})
				        <span class="ready"><i class="fa fa-check"></i> <span>Ready for Check Out</span></span>
				      </button>
				    </h2>
				    <div id="collapse{{ $p }}" class="accordion-collapse collapse @if($p == 1) show @endif" aria-labelledby="heading{{ $p }}" data-bs-parent="#accordionExample">
				      <div class="accordion-body">
				        <h5></h5>
							<input type="hidden" class="pax_id{{$p}}" value="{{$passenger['id']}}">
							@if($p == 1 && $subscription != "first")
								<div class="row">
									<div class="col-md-12">
										<div class="alert alert-info">
											<span>Your subscription requires your name to be on each booking. To book on behalf of others, please upgrade to a paid subscription by contacting admin@venti.co. Your name must match your legal documents for flying. If there is an error, please contact us via email before booking. You are responsible for making sure your name is accurate.</span>
										</div>
									</div>
								</div>
							@endif
							@if(env('APP_ENV') == "production")
								<div class="row">
									<div class="col-md-2">
										<div class="form-group">
											<label class="form-label">Title:</label>
											<select class="form-control pax_title{{$p}} required">
												<option value="">Select</option>
												<option value="mr">Mr.</option>
												<option value="ms">Ms.</option>
												<option value="mrs">Mrs.</option>
												<option value="miss">Miss</option>
												<option value="dr">Dr.</option>
											</select>
										</div>
									</div>
									<div class="col-md-5">
										<div class="form-group">
											<label class="form-label">First Name:</label>
											<input type="text" class="form-control required @if($p == 1 && $subscription != 'first') disabled @endif pax_fname{{$p}}" required=""  @if($p == 1) value="{{ $pax_fname }}" @endif>
										</div>
									</div>
									<div class="col-md-5">
										<div class="form-group">
											<label class="form-label">Last Name:</label>
											<input type="text" class="form-control required @if($p == 1 && $subscription != 'first') disabled @endif pax_lname{{$p}}" required="" @if($p == 1) value="{{ $pax_lname }}"  @endif>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
											<label class="form-label d-block">Phone Number</label>
											<input id="phone" type="text" class="form-control required @if($p == 1 && $subscription != 'first') disabled @endif pax_phone{{$p}} pax_cell" required=""  @if($p == 1)value="{{ $phoneNumber }}" @endif>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label class="form-label">Email</label>
											<input type="text" id="email" class="form-control required @if($p == 1) disabled @endif pax_email{{$p}}" required=""  @if($p == 1)value="{{ Session::get('user')->email }}"  @endif>
										</div>
									</div>
								</div>
								<div class="row mb-3">
									<div class="col-md-6">
										<div class="form-group">
											<label class="form-label">Gender:</label>
											<select id="gender" class="form-control required pax_gender{{$p}}">
												<option value="">--- Please Select ---</option>
												<option value="m">Male</option>
												<option value="f">Female</option>
											</select>
										</div>
									</div>
									<div class="col-md-6">
										<label class="form-label">Date of Birth:</label>
										<input type="date" class="form-control required pax_dob{{$p}} pax-age"  required="" data-pax-type="{{ $passenger['type'] }}">
									</div>
									<div class="col-md-12">
										<label class="form-label d-block">Primary Citizenship</label>
										<select type="text" class="required form-control pax_citizenship{{$p}} select2">
											@include('flights.components.countries')
										</select>
									</div>
								</div>
								<div class="row mb-3">
									<div class="col-md-6">
										<label class="form-label">Known Traveler Number (Optional)</label>
										<input type="text" class="form-control pax_ktn{{$p}}">
									</div>
									<div class="col-md-6">
										
										<label class="form-label">{{ $data["owner"]["name"] }} Rewards Programme/Number (Optional)</label>
										<input type="text" class="form-control pax_rewards{{$p}}">
										
									</div>
								</div>
							@else
								<div class="row">
									<div class="col-md-2">
										<div class="form-group">
											<label class="form-label">Title:</label>
											<select class="form-control pax_title{{$p}} required">
												<option value="">Select</option>
												<option value="mr" selected="">Mr.</option>
												<option value="ms">Ms.</option>
												<option value="mrs">Mrs.</option>
												<option value="miss">Miss</option>
												<option value="dr">Dr.</option>
											</select>
										</div>
									</div>
									<div class="col-md-5">
										<div class="form-group">
											<label class="form-label">First Name:</label>
											<input type="text" class="form-control required @if($p == 1 && $subscription != 'first') disabled @endif pax_fname{{$p}}" required=""  @if($p == 1) value="{{ $pax_fname }}" @endif>
										</div>
									</div>
									<div class="col-md-5">
										<div class="form-group">
											<label class="form-label">Last Name:</label>
											<input type="text" class="form-control required @if($p == 1 && $subscription != 'first') disabled @endif pax_lname{{$p}}" required="" @if($p == 1) value="{{ $pax_lname }}" @endif>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
											<label class="form-label d-block">Phone Number</label>
											<input id="phone" type="text" class="form-control required @if($p == 1) disabled @endif pax_phone{{$p}} pax_cell" required=""  value="+12404412060">
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label class="form-label">Email</label>
											<input type="text" id="email" class="form-control required @if($p == 1) disabled @endif pax_email{{$p}}" required=""  value="admin@venti.co">
										</div>
									</div>
								</div>
								<div class="row mb-3">
									<div class="col-md-6">
										<div class="form-group">
											<label class="form-label">Gender:</label>
											<select id="gender" class="form-control required pax_gender{{$p}}">
												<option value="">--- Please Select ---</option>
												<option value="m" selected="">Male</option>
												<option value="f">Female</option>
											</select>
										</div>
									</div>
									<div class="col-md-6">
										<label class="form-label">Date of Birth:</label>
										<input type="date" value="1995-11-15" class="form-control required pax_dob{{$p}} pax-age"  required="" data-pax-type="{{ $passenger['type'] }}">
									</div>
									<div class="col-md-12">
										<label class="form-label d-block">Primary Citizenship</label>
										<select type="text" class="required form-control pax_citizenship{{$p}} select2">
											@include('flights.components.countries')
										</select>
									</div>
								</div>
								<div class="row mb-3">
									<div class="col-md-6">
										<label class="form-label">Known Traveler Number (Optional)</label>
										<input type="number" class="form-control pax_ktn{{$p}}" inputmode="numeric">
									</div>
									<div class="col-md-6">
										@if($loyalty_programme_accepted)
										<label class="form-label">{{ $data["owner"]["name"] }} Rewards Programme/Number (Optional)</label>
										<input type="text" class="form-control pax_rewards{{$p}}">
										@else
											<label class="form-label">Looking to add loyalty info? At this time, {{ $data["owner"]["name"] }} does not allow this information to be collected from Venti.</label>
										@endif
									</div>
								</div>
							@endif
							@if($docsRequired)
								<div class="row mb-3">
									<div class="col-md-12">
										<hr>
										<label class="form-label">Documentation Type:</span></label>
										<select class="form-control required pax_doc{{$p}} pax_document">
											<option value="">-- Please Select--</option>
											<option value="passport" selected="">Passport</option>
										</select>
									</div>
								</div>
								<div class="row mb-3">
									<div class="col-md-6">
										<label class="form-label"><span class="docTypeLabel">Passport</span> Number:<span class="d-block text-small"><i class="fa fa-lock"></i> This data is not saved on Venti.</span></label>
										<input type="text" class="form-control required pax_passport_number{{$p}}" required="" inputmode="numeric" maxlength="12" >
									</div>
									<div class="col-md-6">
										<label class="form-label"><span class="docTypeLabel">Passport</span> Expiration:<span class="d-block text-small"><i class="fa fa-lock"></i> This data is not saved on Venti.</span></label>
										<input type="date" class="form-control required passport-expiry pax_passport_exp{{$p}}">
									</div>
								</div>
								<div class="passportInput passportInput-{{$p}}">
									<div class="row mb-3">
										
										<div class="col-md-12">
											<label class="form-label"><span class="docTypeLabel">Passport</span> Issued By (Country):<span class="d-block text-small"><i class="fa fa-lock"></i> This data is not saved on Venti.</span></label>
											<select class="form-control required pax_passport_country{{$p}} select2">
												@include('flights.components.countries')
											</select>
										</div>
									</div>
									<div class="row mb-3">
										<p>It is your responsibility to ensure you have presented all necessary travel documents, including Visas, to the airline prior to departure.</p>
									</div>
								</div>
							@else
								<div class="row">
									<div class="col-md-12">
										<hr>
										<label class="form-label">Documents Not Required to Book:</label>
										<div class="alert alert-success">For faster checkout, {{ $data["owner"]["name"] }} is not requiring this passenger to provide travel documents at this step. It is your responsibility to ensure you have presented all necessary travel documents, including Visas, to the airline prior to departure.</div>
									</div>
								</div>
							@endif
							@if($canAddBags)
								<div class="row mb-3">
									<div class="col-md-12">
										<hr>
										<label class="form-label">Bag Check:</label>
										<p>Your order already includes the following baggage for this passenger</p>
										<ul style="padding-left:20px">
											@foreach($passengerBags as $passengerBag)
												<li>{{ ucfirst(str_replace('_',' ',$passengerBag['type'])) }}: {{ $passengerBag['quantity'] }}</li>
											@endforeach
											
										</ul>
										<br>
										<ul class="list-group" style="list-style:none !important;">
											@foreach($data["baggable"] as $bagAssign)
												@if(in_array($passenger['id'], $bagAssign["passenger_ids"]))
													<li style="padding:5px">
													<input type="checkbox"
													class="addBaggage"
													data-msrp="{{ $bagMSRP }}"
													id="bag-{{$passenger['id']}}"
													value="{{ $bagAssign['id'] }}"> Add one checked bag to their <strong>flight to {{ $bagAssign["segment"]["destinationCity"] }}</strong> for {{ $checkedBagFee . " " . $checkedBagCurrency}}</li>
												@endif
											@endforeach
										</ul>
											
									</div>
								</div>
							@endif
							<div class="row">
								<div class="col-md-12">
									<button type="button" class="btn btn-primary savePax" data-pax-id="{{ $passenger['id'] }}" data-pax-i="{{ $p }}" data-rewards-iata="{{ $data['owner']['iata_code'] }}"><i class="fa fa-sync fa-spin"></i> Save Passenger Info</button>
								</div>
							</div>
				      	</div>
				    </div>
	  			</div>
			</div>
		</div>
		@php $p++; @endphp
	@endforeach
	@if(env('APP_ENV') == "local")
		<pre>
			@php
				print_r($available_services);
			@endphp
		</pre>
	@endif
</div>


	