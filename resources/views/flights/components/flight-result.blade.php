@php

$baggages = 0;
$discountEstimateMultiplier = 4.2;
$class = ucfirst(strtolower($data["class"]));
if($class == "Business"){
	$discountEstimateMultiplier = 6;
}
if($class == "First"){
	$discountEstimateMultiplier = 10;
}
$estimatedPlatformFee = calcVentiPlatformFee($data['total_amount']);
$estimatedDiscount = $estimatedPlatformFee - calVentiDiscount($data['total_amount']);

$i = 0;

$numSlices = sizeof($data['slices']);
$carrierName = false;


@endphp

<div id="{{ $data['offer'] }}"
class="card bg-white mb-3 flight-card card-{{ $data['offer'] }}"
data-stops='{{ sizeof($data["slices"]) }}'
data-total-duration='{{ $data["total_duration"] }}'
data-total-flights='{{ $data["total_flights"] }}'
data-default-order='{{ $data["default_order"] }}'
data-total="{{ $data['total_amount'] }}"
>
	<div class="card-body" style="padding-top:0">
		<div class="flight-card-content">
			<div class="d-flex flex-sm-column flex-lg-row" style="flex-grow:1; justify-content:space-between; align-items:center;">
				<div style="width: 100%;">
				@foreach($data['slices'] as $slice)
					@php 

					$baggages = 0;
					$carryBaggages = 0;
					$power = false;
					$wifi = false;
					$wifiPaid = false;


					$ccFeeEstimate = round(($data['total_amount'] * .029) + 0.30,2);
					$ventiPriceEstimate = number_format(round($data['total_amount'] - $ccFeeEstimate,2));

					@endphp
					@if(array_key_exists("segments", $slice))
						
						@foreach($slice["segments"] as $slice)
							@php

								$duration = str_replace("PT","", $slice["duration"]);
								$duration = str_replace("H","H ", $duration);
								$timezone1 = $slice["destination"]["time_zone"];
								$timezone2 = $slice["destination"]["time_zone"];
								$departingTime = Carbon\Carbon::parse($slice["departing_at"])->format('g:i A');
								$departingDay = Carbon\Carbon::parse($slice["departing_at"])->formatLocalized('%b. %d');
								$arrivingTime =  Carbon\Carbon::parse($slice["arriving_at"])->format('g:i A');
								$arrivingDay = Carbon\Carbon::parse($slice["arriving_at"])->formatLocalized('%b. %d');
								
								$class = strtoupper($slice["class"]);

								if(!$class){
									$class = strtoupper($data["class"]);
								}

								$passengers = $slice["passengers"];

								$seatSelectable = seatableAirline($slice['carrier']['name']);
								$bagSelectable = baggableAirline($slice['carrier']['name']);
								$carrierName = $slice['carrier']['name'] ?? false;

								if(array_key_exists("fare_brand_name", $slice)){
									$class = $slice["fare_brand_name"] . "-b";
								}
								
								foreach($passengers as $passenger){


									foreach($passenger["baggages"] as $baggage){
										if($baggage["type"] == "checked" && $baggage["quantity"] > 0){
											$baggages += $baggage["quantity"];
										}

										if($baggage["type"] == "carry_on" && $baggage["quantity"] > 0){
											$carryBaggages += $baggage["quantity"];
										}

										

										$wifiAvailable = $passenger["cabin"]["amenities"]["wifi"]["available"] ?? false;
										if($wifiAvailable){
											$wifi = true;
											$wifiPaid = false;
											if($passenger["cabin"]["amenities"]["wifi"]["cost"] == "paid"){
												$wifiPaid = true;
											}
										}
										$powerAvailable = $passenger["cabin"]["amenities"]["power"]["available"] ?? false;
										if($powerAvailable){
											$power = true;
										}
									}
								}
							@endphp

							<div class="@if($numSlices == 1) borderless @endif flight-slice d-flex flex-sm-column" data-carrier="{{ $slice['carrier']['name'] }}" style="width:100%;">
								<div class="d-flex flex-lg-column flex-sm-row mw-lg-100">
									<div style="width: 100%; padding-right: 10px;" class="text-center">
										<img src="{{ $slice['carrier']['logo_symbol_url'] }}" style="width:100%; max-width: 50px;">
									</div>
									<p class="text-left text-small mb-0">
										<strong>{{ $slice['carrier']['name'] }}</strong> <br>
										Flight: {{ $slice['flight_number'] ?? " # Pending" }}<br>
										Class: {{ $class }}<br>
										@if($wifi) @if($wifiPaid) <img src="/assets/img/icons/wifi-paid.png" style="width:12px;" class="flight-amenity"> @else <img src="/assets/img/icons/wifi.png" style="width:12px;" class="flight-amenity">@endif @endif
										@if($power) <img src="/assets/img/icons/power.png" style="width:12px;" class="flight-amenity">@endif
										@if($bagSelectable)
											<img src="/assets/img/icons/luggage.png" style="width:12px;" class="flight-amenity">
										@endif
										@if($seatSelectable)
											<img src="/assets/img/icons/seat.png" style="width:12px;" class="flight-amenity">
										@endif
										
									</p>
								</div>
								<div class="d-flex" style="width:100%">
									<div class="text-center d-flex flex-column justify-content-center" style="width:100%;">
										<div>
											<h5 class="mb-0">{{ $departingTime }}</h5>
											<span class="text-small">{{ $slice["origin"]["city_name"] . ", " . $slice["origin"]["iata_country_code"] }} <span class="d-block">{{ $departingDay }}</span></span>
										</div>
									</div>
									<div class="text-center d-flex flex-column justify-content-center duration-info" style="width:100%">
										<span class="text-small">({{ $duration }})</span>
										<hr style="width: 100%; display:block; margin-top:0">
									</div>
									<div class="text-center d-flex flex-column justify-content-center" style="width:100%">
										<div>
											<h5 class="mb-0">{{ $arrivingTime }}</h5>
											<span class="text-small">{{ $slice["destination"]["city_name"] . ", " . $slice["destination"]["iata_country_code"] }} <span class="d-block">{{$arrivingDay}}</span></span>
										</div>
									</div>
								</div>
							</div>
						@endforeach
					@else
						@php
							// this flight includes multiple planes

							$duration = str_replace("PT","", $slice["duration"]);
							$duration = str_replace("H","H ", $duration);
							$timezone1 = $slice["destination"]["time_zone"];
							$timezone2 = $slice["destination"]["time_zone"];
							$departingTime = Carbon\Carbon::parse($slice["departing_at"])->format('g:i A');
							$departingDay = Carbon\Carbon::parse($slice["departing_at"])->formatLocalized('%b. %d');
							$arrivingTime =  Carbon\Carbon::parse($slice["arriving_at"])->format('g:i A');
							$arrivingDay = Carbon\Carbon::parse($slice["arriving_at"])->formatLocalized('%b. %d');
							$class = strtoupper($slice["class"]);

							if(!$class){
								$class = strtoupper($data["class"]);
							}

							$isReturnLeg = false;

							if($slice["origin"]["iata_code"] == $data["iata_destination"]){
								$isReturnLeg = true;
							}

							$passengers = $slice["passengers"] ?? [];

							if(!array_key_exists("carrier", $slice)){
								// direct flights may not have this entry
							}

							foreach($passengers as $passenger){

								foreach($passenger["baggages"] as $baggage){
									if($baggage["type"] == "checked" && $baggage["quantity"] > 0){
										$baggages += $baggage["quantity"];
									}

									if($baggage["type"] == "carry_on" && $baggage["quantity"] > 0){
										$carryBaggages += $baggage["quantity"];
									}

									$wifiAvailable = $passenger["cabin"]["amenities"]["wifi"]["available"] ?? false;
									if($wifiAvailable){
										$wifi = true;
										$wifiPaid = false;
										if($passenger["cabin"]["amenities"]["wifi"]["cost"] == "paid"){
											$wifiPaid = true;
										}
									}
									$powerAvailable = $passenger["cabin"]["amenities"]["power"]["available"] ?? false;
									if($powerAvailable){
										$power = true;
									}
								}
							}
							$seatSelectable = seatableAirline($slice['carrier']['name']);
							$bagSelectable = baggableAirline($slice['carrier']['name']);
							$carrierName = $slice['carrier']['name'] ?? false;
						@endphp
							<div class="@if($i > 0 || sizeof($data['slices']) == 1) borderless @endif flight-slice d-flex flex-sm-column flex-lg-row" data-carrier="{{ $slice['carrier']['name'] }}" style="width:100%;">
								<div class=" d-flex flex-lg-column flex-sm-row text-center vertical-align carrier-data justify-content-sm-between mw-lg-100" style="width:100% !important; align-items: center; align-items:flex-start;">
									<div style="align-items:center;">
										<div style="width: 100%; padding-right: 10px;" class="text-center">
											<img src="{{ $slice['carrier']['logo_symbol_url'] }}" style="width:100%; max-width:50px;">
										</div>
									</div>
									<p class="text-left text-small mb-0">
										<strong>{{ $slice['carrier']['name'] }}</strong> <br>
										Flight: {{ $slice['flight_number'] ?? " # Pending" }}<br>
										Class: {{ $class }}<br>
										@if($wifi) @if($wifiPaid) <img src="/assets/img/icons/wifi-paid.png" style="width:12px;" class="flight-amenity"> @else <img src="/assets/img/icons/wifi.png" style="width:12px;" class="flight-amenity">@endif @endif
										@if($power) <img src="/assets/img/icons/power.png" style="width:12px;" class="flight-amenity">@endif
										@if($bagSelectable)
											<img src="/assets/img/icons/luggage.png" style="width:12px;" class="flight-amenity">
										@endif
										@if($seatSelectable)
											<img src="/assets/img/icons/seat.png" style="width:12px;" class="flight-amenity">
										@endif
										
									</p>
									
								</div>
								<div class="d-flex" style="width:100%">
									<div class="text-center d-flex flex-column justify-content-center" style="width:100%">
										<div>
											<h5 class="mb-0">{{ $departingTime }}</h5>
											<span class="text-small">{{ $slice["origin"]["city_name"] . ", " . $slice["origin"]["iata_country_code"] }}  <span class="d-block">{{ $departingDay }}</span></span>
										</div>
									</div>
									<div class="text-center d-flex flex-column justify-content-center duration-info" style="width:100%;">
											<span class="text-small">({{ $duration }})</span>
											<hr style="width: 100%; display:block; margin-top:0">
									</div>
									<div class="text-center d-flex flex-column justify-content-center" style="width:100%">
										<div>
											<h5 class="mb-0">{{ $arrivingTime }}</h5>
											<span class="text-small">{{ $slice["destination"]["city_name"] . ", " . $slice["destination"]["iata_country_code"] }} <span class="d-block">{{$arrivingDay}}</span></span>
										</div>
									</div>
								</div>
							</div>
					@endif
					
					@php
						$i++;
					@endphp
					@endforeach
					@php
					$discountAlgo = getDiscountPercent("airfare", $data['total_amount'],"USD");
					$finalPrice = $data['total_amount'] * $discountAlgo;
					
				@endphp
			</div>
				<div class="price-box text-center" style="@if($numSlices < 2) padding:15px 0; @endif">
					<span class="text-small checked-bags" data-card="card-{{ $data['offer'] }}" data-bag-count="{{ $baggages }}" style="">Checked Bags: {{ $baggages }}</span>
					<span class="text-small mb-3 carry-on-bags" data-card="card-{{ $data['offer'] }}" data-bag-count="{{ $carryBaggages }}" style="">Carry Ons: {{ $carryBaggages }}</span>
					<br>
					<s class="p-0 m-0 text-bold mt-3">${{ number_format($data['total_amount'],2) }}</s>
					<h2 style="margins:0">${{ number_format($finalPrice,2) }}</h2>
					<!--
					<h2 style="margin-bottom:0">${{ number_format($data['total_amount'],2) }}</h2>
						<a href="/flights/search/offer/{{ $data['offer'] }}/{{ $data['iata_destination'] }}" target="_blank" class="btn btn-primary detailsButton" style="width:140px; font-size:13px;">
							VIEW DETAILS
						</a>
						<div class="text-center mt-3 mb-3">
							<h5 style="margin:0">Save up to</h5>
							<h2 style="margin:0; font-size:1.8rem;">${{ number_format($estimatedDiscount,2) }}</h2>
							<div class="mt-1" style="display:flex; justify-content:space-between; align-items:center;">
								<p class="mt-2 boarding-pass-footer">When you book this flight with a Venti Boarding Pass</p>
							</div>
						</div>
					!-->

					<button type="button" data-seat-class="{{ $data['class'] }}" data-partial="{{ $data['partial'] ?? 'p' }}" data-search-id="{{ $data['search_id'] }}" data-offer="{{ $data['offer'] }}" class="btn btn-primary detailsButton @if(!$data['isReturn'])originFlight @else returnFlight @endif" data-amount="{{ $data['total_amount'] }}" data-msrpd="{{ $discountAlgo }}" style="width:140px; font-size:13px;">Select</button>
					@if($carrierName != "Hawaiian Airlines" || $carrierName != "TAP Air Portugal")
						<a href="javascript:void(0)" class="priceMatchPop d-block text-decoration text-center text-small mt-1">We Price Match</a>
					@endif
				</div>
			</div>
		</div>
	</div>
</div>