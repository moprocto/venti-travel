@php
	$mapl = $data["map"]["cabins"];
	$pax = $data["passenger_id"];
	$segmentLabel = $data["origin_iata"] . " to " . $data["destination_iata"];
	$cabinIterator = $data["cabinIterator"];
@endphp
@foreach($mapl as $map)
	@php
		if($mapl == false){
			continue;
		}
	@endphp
	<h4>{{ $data['map']['passenger_name'] }}'s Seat from {{ $segmentLabel }}</h4>
	<seatsection data-cabin="{{ $data['map']['id'] }}-{{ $cabinIterator }}" class="{{ $data['map']['id'] }} {{ $data['map']['id'] }}-{{ $cabinIterator }}">
		<h5 class="pax_id {{ $pax }} text-center">Front of Plane</h5>
		@foreach($mapl as $cabin)
			@php
				$aisles = $cabin["aisles"];
				$columns = 12/$aisles;
				$availableSeatCount = 0;
			@endphp
			@for($r = 0; $r < sizeof($cabin["rows"]); $r++)
				<div style="max-width:900px; margin:0 auto;">
					<div class="row" style="justify-content:center;">
						<div class="col-md-1" style="background:rgba(0,0,0,0.05); max-width: 10px; color:white"></div>
						@for($a = 0; $a <= $aisles; $a++)
						<div class="col-md-3 cabin-row" style=" min-height:75px">
							@php
								$sections = $cabin["rows"][$r]["sections"][$a];
							@endphp	
							<div>
								@php
									$seats = $sections["elements"];
								@endphp
								<div class="row text-center" style="justify-content:center;">
									@foreach($seats as $seat)
										@php
											$available = $seat["available_services"] ?? false;
											$designator = $seat["designator"] ?? false;
											$assignable = true;
											$price = false;
											$seatID = [];
											$msrp = 0;
											if(!$available){
												$assignable = false;
												$designator = "<i class='fa fa-times'></i>";
												
											} else {
												$msrp = $available[0]["total_amount"];
												$price = round($msrp * getMarkupMultiplier("seat", $msrp),0);
												$price = number_format(baseMarkUpNonZero($price),0);

												foreach($available as $availableSeat){
													array_push($seatID,[
														"passenger" => $availableSeat["passenger_id"],
														"serviceID" => $availableSeat["id"]
													]);
												}

												// each passenger will get an available service
												$availableSeatCount++;
											}
										@endphp
										@if($seat["type"] == "seat")
											<div class="col-md-4 cabin-seat @if($assignable) assignable @endif cabin-{{ $data['map']['id'] }} pax-{{ $pax }} seat-segment-{{ $data['map']['segment_id'] }}  seat-segment-{{ $data['map']['segment_id'] }}-{!! $designator !!} " style="text-align: center; height:50px;" 
											data-msrp="{{ $msrp }}"
											data-seat-id='{{ json_encode($seatID) }}'
											data-pax-name="{{ $data['map']['passenger_name'] }}" data-segment-label="{{ $segmentLabel }}" 
											data-segment-id="{{ $data['map']['segment_id'] }}" data-upgrade-total="{{ $price ?? 0 }}" 
											data-cabin="{{ $data['map']['id'] }}-{{ $cabinIterator }}"
											data-passenger="{{ $pax }}" data-seat-number="{{ $seat['designator'] }}"><p style=" font-size: 16px; font-weight: 400; ">{!! $designator !!}@if($price)<br><span class="d-block p-name">{{ $data['map']['passenger_name'] }}</span><p class="d-block text-center m-0" style="font-size:10px; font-weight: 500;">${{ $price }}</p>@endif</p>
										</div>
										@endif
										@if($seat["type"] == "lavatory")
											<div class="col-md-12  text-center" style="text-align: center;  height:50px; background:rgb(200,200,200); " data-upgrade-total="" data-passenger="{{ $pax }}">Bathroom</div>
										@endif
										@if($seat["type"] == "galley")
											<div class="col-md-12  text-center" style="text-align: center;  height:50px; background:rgb(200,200,200); " data-upgrade-total="" data-passenger="{{ $pax }}">Food</div>
										@endif
										@if($seat["type"] == "exit_row")
											<div class="col-md-12 text-center" style="text-align: center;  height:50px; background:rgb(200,200,200); " data-upgrade-total="" data-passenger="{{ $pax }}">Exit</div>
										@endif
									@endforeach
									@if(!$seats)
										<div class="col-md-12 text-center" style="text-align: center;  height:50px; background:rgb(200,200,200); " data-upgrade-total="" data-passenger="{{ $pax }}">Exit</div>
									@endif
								</div>
							</div>
						</div>
						@if($a != $aisles)
						<div class="col-md-1" style="background:rgba(0,0,0,0.2); color:white">
						</div>
						@endif

						@endfor
						<div class="col-md-1" style="background:rgba(0,0,0,0.05); max-width: 10px; color:white"></div>
					</div>
				<div>
			@endfor
		@endforeach

		<h5 class="text-center">Rear of Plane</h5>
		@if($availableSeatCount == 0)
			<br>
			<h4 class="text-center">All Selectable Seats Taken</h4><p class="text-center">Please press the Next Seat button to advance to the next selection screen.</p>
		@endif
	</seatsection>
@endforeach
<br><hr><hr><br>
