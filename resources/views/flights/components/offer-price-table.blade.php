
<div class="card bg-white mb-3">
	<div class="card-body ">
		<div class="text-right">
			<table class="table">
				<input type="hidden" class="destination">
				@php
					$total_amount = $data["total_amount"];
					$base_amount = $data["base_amount"];
					$tax_amount = $data["tax_amount"];

					$cabin_class = "economy";

					if($data["priorSearch"]){
						$cabin_class = $data["priorSearch"]["class"];
					}

					$extras = 0;

				@endphp
				<tr>
					<td class="text-left">Base Fare</td>
					<td class="text-right"><h5>${{ number_format($base_amount,2) }}</h5></td>
				</tr>
				<tr>
					<td class="text-left">Taxes</td>
					<td class="text-right"><h5>${{ number_format($tax_amount,2) }}</h5></td>
				</tr>
				@foreach($data["seatUpgrades"] as $seatUpgrade)
					<tr>
						<td class="text-left">Seat Selection: {{ $seatUpgrade["metadata"]["designator"] }}</td>
						<td class="text-right is-add-on" data-total="{{ $seatUpgrade['newPrice'] }}"><h5>${{ $seatUpgrade["newPrice"] }}</h5></td>
					</tr>
					@php $extras += $seatUpgrade["newPrice"]; @endphp
				@endforeach
				@foreach($data["bagUpgrades"] as $bagUpgrade)
					<tr>
						<td class="text-left">Additional Checked Bag</td>
						<td class="text-right is-add-on" data-total="{{ $bagUpgrade['newPrice'] }}"><h5>${{ $bagUpgrade["newPrice"] }}</h5></td>
					</tr>
					@php $extras += $bagUpgrade["newPrice"]; @endphp
				@endforeach
				@php


				$total_amount += $extras;

				// now the "total" represents the cost of everything plus upgrades

				$protection = round($total_amount * 0.3,2);

				// protection is 30% of the total price
				// this is standard

				// the subtotal with no points applied 

				$subtotal = $total_amount + $protection;

				// 

				$maxDiscount = getDiscountPercent("airfare", $subtotal, "USD");

				// returns the price after discount

				$lowestPossible = $subtotal * $maxDiscount;

				$maxPoints = round($subtotal - $lowestPossible,2);

				@endphp
				<tr>
					<input type="hidden" id="protectionFee" value="{{ $protection }}">
					<td class="text-left">Airlock Flight Protection <input type="checkbox" id="protection" class="js-switch" checked /><span style="display:block; font-size:11px;"><a href="#" data-bs-toggle="modal" data-bs-target="#CFARexplainerModal">Hassle-Free Cancelation.<sup><i class="fa fa-arrow-up-right-from-square"></i></sup></a></span></td>
					<td class="text-right"><h5>$<span id="protectionOn">{{ number_format($protection,2) }}</span></h5></td>
				</tr>
				<tr>
					<input type="hidden" id="total-cost" value="{{ number_format($subtotal,2) }}">
					<td class="text-left"><strong>Subtotal</strong></td>
					<td class="text-right"><h5>$<span id="newTotal">{{ number_format($subtotal,2) }}</span></h5></td>
				</tr>
				<tr class="row_total lowest_price">
					<input type="hidden" id="lowest_price" value="{{ number_format($lowestPossible,2) }} ">
					<input type="hidden" id="maxPointsAllowedOG" value="{{ $maxPoints }}">
					<td class="text-left"><h3>Lowest Price <span  style="font-size:14px; display:block;">You can use up to <span class="maxPoints">{{ number_format($maxPoints,2) }}</span> Points to achieve this price.</span></h3></td>
					<td class="text-right"><h3>$<span id="lowestPossiblePrice">{{ number_format($lowestPossible,2) }}</span></h3></td>
				</tr>
			</table>
		</div>
	</div>
</div>
    			
    			
