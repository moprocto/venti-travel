<select class="form-small no-bg" id="flight-class">
	@php
		$priorClass = $priorSearch['class'] ?? false;
	@endphp
	<option value="economy"

	@if(!$priorSearch)
		selected="" 
	@else
		@if($priorClass == 'economy')
			selected="" 
		@endif
	@endif
		><i class="fa fa-plane"></i> Economy</option>


	<option value="premium_economy"
	@if($priorSearch && $priorClass == 'premium_economy')
			selected="" 
		@endif
	><i class="fa fa-plane"></i> Premium Economy</option>

	
	<option value="business"
	@if(isset($priorClass) && $priorClass == 'business') selected=""
	@endif><i class="fa fa-plane"></i> Business</option>
	<!--
	<option value="first"
	@if(isset($priorClass) && $priorClass == 'first') selected=""
	@endif><i class="fa fa-plane"></i> First-Class</option>	
	-->
</select>