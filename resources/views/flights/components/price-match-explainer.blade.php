<div class="modal fade" id="MatchexplainerModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true" style="background-image: linear-gradient(112.1deg, rgba(125, 110, 112,0.5) 11.4%, rgba(152, 135, 125,0.8) 70.2%); ">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header" style="position: absolute;
    right: 0;
    top: 10px; border-bottom: transparent; z-index: 99;">
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
      	<h4>Price Match<span class="d-block" style="font-size:14px;">Let us be the best deal!</span></h4>
        @if($pm == "flights")
      	  <p>Venti does not mark up the cost of fares. However, flights you see on Venti may differ slightly from other popular search tools. Our goal is to ensure we're offering the best prices to Venti Boarding Pass holders. For this flight to qualify for price matching, the following conditions must be met:</p>
          <ul>
            <li>Origin and destination are the same, including departure dates and times, and carriers.</li>
            <li>Fare name (Economy, Business, First, etc.) are the same.</li>
            <li>Amenities are equal, such as seat selection, number of carry-on and checked bags, and flexibility.</li>
            <li>The competing fare is not part of an exclusive membership program, rebate, temporarily discounted/promotional offer, or marked down due to vouchers, points, miles, and other credits.</li>
          </ul>
          <p><strong>We do not price match Hawaiian Airlines or TAP Air Portugal</strong></p>
          <p>Please review our <a href="/about/price-match" target="_blank">full policy</a> before requesting a price match. Please contact Venti Support via admin@venti.co BEFORE booking your flight.
          <br>To expedite review, please provide screenshots of the competing offer and this offer. Price matching is not price protection in the instance that your fare changes in price.</p>

        @else
          <p>Venti does not mark up the cost of hotels. However, accomodations you see on Venti may differ slightly from other popular search tools. Our goal is to ensure we're offering the best prices to Venti Boarding Pass holders. For this accomodation to qualify for price matching, the following conditions must be met:</p>
          <ul>
            <li>Check in, check out, guest count (and types) are the same</li>
            <li>Room size, bed count, and room configuration are the same</li>
            <li>Amenities and refund policies are equal.</li>
            <li>The competing accomodation is not part of an exclusive membership program, rebate, temporarily discounted/promotional offer, or marked down due to vouchers, points, room nights, and other credits.</li>
          </ul>

        <p>Please review our <a href="/about/price-match" target="_blank">full policy</a> before requesting a price match. Please contact Venti Support via admin@venti.co BEFORE booking your accomodation.
        <br>To expedite review, please provide screenshots of the competing offer and this offer. Price matching is not price protection in the instance that your quote changes in price.</p>
        @endif
      </div>
    </div>
  </div>
</div>