<div class="modal fade" id="amenityExplainerModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true" style="background-image: linear-gradient(112.1deg, rgba(125, 110, 112,0.5) 11.4%, rgba(152, 135, 125,0.8) 70.2%); ">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header" style="position: absolute;
    right: 0;
    top: 10px; border-bottom: transparent; z-index: 99;">
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <h5>In-Flight Amenities</h5>
        <p>See what's available with your flight.</p>
        <table class="table">
          <tbody>
            <tr>
              <td style="width: 20px; text-align:center;"><img src="/assets/img/icons/power.png" style="width:15px;"/></td>
              <td>Device charging is available with your seat. Power capacity and outlet quantity may vary.</td>
            </tr>
              <td style="width: 20px; text-align:center;"><img src="/assets/img/icons/wifi-paid.png" style="width:15px;"/></td>
              <td>Wireless connectivity is included in the fare price. In-flight entertainment options are not known at this time.</td>
            </tr>
            <tr>
              <td style="width: 20px; text-align:center;"><img src="/assets/img/icons/wifi.png" style="width:15px;"/></td>
              <td>Wireless connectivity is available for purchase during flight. In-flight entertainment options are not known at this time.</td>
            </tr>
            <tr>
              <td style="width: 20px; text-align:center;"><img src="/assets/img/icons/luggage.png" style="width:15px;"/></td>
              <td>This carrier allows you to purchase additional checked bags. May not be available for economy fares. Fees may vary.</td>
            </tr>
            <tr>
              <td style="width: 20px; text-align:center;"><img src="/assets/img/icons/seat.png" style="width:15px;"/></td>
              <td>This carrier allows you to select your preferred seats. May not be available for economy fares. Additional fees may apply. Quantity is limited.</td>
            </tr>
          </tbody> 
        </table>
      </div>
    </div>
  </div>
</div>