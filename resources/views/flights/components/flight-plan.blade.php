<table class="table" style="width: 100%" data-bags="{{ $baggages }}">
	@foreach($data['slices'] as $slice)
		@if(array_key_exists("segments", $slice))
			@php
				$destination = $data["slices"][$numSlices - 1]["segments"][0]["origin"];
				$origin = $data["slices"][0]["segments"][0]["origin"];
			@endphp

			@foreach($slice["segments"] as $segment)
				@php

				$passengers = $segment["passengers"];
				$baggages = 0;
				$carry_on = 0;
				$power = $powerAvailable = false;
				$wifi = $wifiAvailable = false;
				$wifiPaid = false;
				$class = $slice["fare_brand_name"] ?? false;

				$duration = str_replace("PT","", $segment["duration"]);
				$duration = str_replace("H","H ", $duration);

				$seatSelectable = seatableAirline($segment['operating_carrier']['name']);
				$bagSelectable = baggableAirline($segment['operating_carrier']['name']);

				foreach($passengers as $passenger){

					if(!$class || is_null($class)){
						$class = $passenger["cabin_class_marketing_name"] ?? ucfirst($passenger["cabin_class"] ?? "n/a"); 
					}
					
					foreach($passenger["baggages"] as $baggage){

						if($baggage["type"] == "checked" && $baggage["quantity"] > 0){
							$baggages += $baggage["quantity"];
						}
						if($baggage["type"] == "carry_on" && $baggage["quantity"] > 0){
							$carry_on += $baggage["quantity"];
						}
						$wifiAvailable = $passenger["cabin"]["amenities"]["wifi"]["available"] ?? false;
						if($wifiAvailable){
							$wifi = true;
							$wifiPaid = false;
							if($passenger["cabin"]["amenities"]["wifi"]["cost"] == "paid"){
								$wifiPaid = true;
							}
						}
						$powerAvailable = $passenger["cabin"]["amenities"]["power"]["available"] ?? false;
						if($powerAvailable){
							$power = true;
						}
					}
				}


				@endphp
				@if($numSlices > 1)
				@if($i == 0)
					<h4>Journey to <span class="destination">{{ $destination["city_name"] ?? "" }}</span></h4>
					@if($data["total_amount"] > 0)
						<p id="flight-plan-pricing">Total: ${{ number_format($data["total_amount"],2) }}</p>
					@endif
				@endif
				@if($segment["origin"]["iata_code"] == $data["iata_destination"])
					<h4>Returning from <span class="destination">{{ $destination["city_name"] ?? "" }}</span></h4>
					
				@endif
				@endif
				<div class="card bg-white mb-3">
					<div class="card-body">
						@php
							$flightNumber = $segment["operating_carrier_flight_number"] ?? $segment["marketing_carrier_flight_number"];
							$carrier = $segment["operating_carrier"] ?? $segment["marketing_carrier"];
							$departingTime = Carbon\Carbon::parse($segment["departing_at"])->format('g:i A');
							$departingDay = Carbon\Carbon::parse($segment["departing_at"])->formatLocalized("%a %b. %d, %Y");
							$arrivingTime =  Carbon\Carbon::parse($segment["arriving_at"])->format('g:i A');
							$arrivingDay = Carbon\Carbon::parse($segment["arriving_at"])->formatLocalized('%a %b. %d, %Y');
						@endphp
						<div style="display:flex; flex-direction: row; justify-content: space-around;">
							<div class="text-left" style="width: 15%;">
								<img src="{{ $carrier['logo_symbol_url'] }}" style="width:100%; max-width:75px;">
								<ul class="mt-1" style="list-style:none; padding:0">
									<li style="font-size: 12px;" class="text-small"><strong>Flight:</strong> {{ $flightNumber }}</li>
									<li style="font-size: 12px;" class="text-small"><strong>Class:</strong> {{ $class }}</li>
								@if(array_key_exists("aircraft", $segment) && $segment["aircraft"] != null)
									<li style="font-size: 12px;" class="text-small"><strong>Aircraft:</strong> {{ $segment["aircraft"]["name"] }}</li>
								@endif
								</ul>
							</div>
							<div style="width:60%">
								<div class="row">
									<div class="col-md-12 mb-2">
										<h4 class="mb-0">{{ $segment["origin"]["city_name"] }} &nbsp; <span style="font-size: 12px;" class="text-small d-block"> {{ $segment["origin"]["name"] }} ({{ $segment["origin"]["iata_code"] }})</span></h4>
									</div>
									<div class="col-md-12">
										<strong style="font-size: 12px;" class="text-small">What's Included:</strong>
										<ul style="list-style:none;padding:0" style="font-size: 12px;" class="text-small">
											@if($carry_on > 0)
												<li>{{ $carry_on }} Carry on Bag(s)</li>
											@else
												<li>1 Personal Item</li>
											@endif
											
											<li>{{ $baggages }} Checked Bag(s)</li>
											@if($wifiPaid)
												<li>Courtesy in-flight Wi-Fi</li>
											@endif
											@if($powerAvailable)
												<li>Device Charging</li>
											@endif
										</ul>
									</div>
								</div>
							</div>
							<div>
								<h5 class="text-center">
								{{ $departingDay }}<p>{{ $departingTime }}</p>
								</h5>
							</div>
						</div>
						<div class="text-center">
							<p style="padding:0; margin:0">Duration: {{ $duration }}</p>
						</div>
						<hr style="margin:0 0 15px 0;"></hr>
						<div style="display:flex; flex-direction: row; justify-content: space-around;">
							<div class="text-left" style="width: 15%">
								<img src="{{ $carrier['logo_symbol_url'] }}" style="width:100%; max-width:75px;">
								<ul class="mt-1" style="list-style:none; padding:0">
									<li style="font-size: 12px;" class="text-small"><strong>Flight:</strong> {{ $flightNumber }}</li>
									<li style="font-size: 12px;" class="text-small"><strong>Class:</strong> {{ $class }}</li>
								@if(array_key_exists("aircraft", $segment) && $segment["aircraft"] != null)
									<li style="font-size: 12px;" class="text-small"><strong>Aircraft:</strong> {{ $segment["aircraft"]["name"] }}</li>
								@endif
								</ul>
								
							</div>
							<div style="width:60%">
								<div class="row">
								<div class="col-md-12 mb-2">
									<h4 class="mb-0">{{ $segment["destination"]["city_name"] }} &nbsp; <span style="font-size: 12px;" class="text-small d-block">{{ $segment["destination"]["name"] }} ({{ $segment["destination"]["iata_code"] }})</span></h4>
								</div>
								<div class="col-md-5">
									<strong style="font-size: 12px;" class="text-small">What's Included:</strong>
									<ul style="list-style:none;padding:0" style="font-size: 12px;" class="text-small">
										@if($carry_on > 0)
											<li>{{ $carry_on }} Carry on Bag(s)</li>
										@else
											<li>1 Personal Item</li>
										@endif
										
										<li>{{ $baggages }} Checked Bag(s)</li>
										@if($wifiPaid)
											<li>Courtesy in-flight Wi-Fi</li>
										@endif
										@if($powerAvailable)
											<li>Device Charging</li>
										@endif
									</ul>
								</div>
								</div>
							</div>
							<div>
								<h5 class="text-center">
								{{ $arrivingDay }}<p>{{ $arrivingTime }}</p>
								</h5>
							</div>
						</div>
					</div>
				</div>
				@php $i++; @endphp
			@endforeach
		@else

		@endif
	@endforeach
</table>