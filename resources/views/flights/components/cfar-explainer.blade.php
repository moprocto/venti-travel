<div class="modal fade" id="CFARexplainerModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true" style="background-image: linear-gradient(112.1deg, rgba(125, 110, 112,0.5) 11.4%, rgba(152, 135, 125,0.8) 70.2%); ">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header" style="position: absolute;
    right: 0;
    top: 10px; border-bottom: transparent; z-index: 99;">
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
      	
        <img src="/assets/img/icons/airlock.png" class="d-block mt-4" style="width:100px; margin:0px auto;">
        <h4 class="mt-3 d-block text-center" style="width:100%;">Airlock<span class="d-block" style="font-size:14px;">Hassle-Free. Contact-less Cancelation Coverage</span></h4>
        <p>With our premium flight protection, you can cancel your trip for any reason whatsover. Have peace of mind knowing that your travel investment is safe. When activating protection, there's no need to call, email, or fight through a chatbot system. Simply click cancel from your <i>upcoming trips</i> page, and we'll begin processing your refund right away. The cost of flight protection varies per trip and is non-refundable. Airlock Flight Protection is not travel insurance. If you require insurance for your trip, please contact us directly via admin@venti.co</p>
        <p style="display: none;">
          Some "flexible" tickets have generous cancellation policies and allow you to cancel up until 24 hours from departure. You must check the airline's website directly to confirm any limits on cancelation. Depending on the airline, you may not get a full refund, and you may required to open a support ticket directly with the airline or insurance company, which may take weeks. For full protection, we recommend purchasing Venti Airlock even if you've purchased a flexible ticket.
        </p>
        <p>
          You cannot activate Airlock coverage after the departure date and time of the first flight has passed. View our full explanation of <a href="/terms/airlock">terms and conditions</a>.
        </p>
      </div>
    </div>
  </div>
</div>