<script type="text/javascript">

	var windowWidth = $( window ).width();


	if(windowWidth < 800){
		@if(Route::current()->getName() != 'flights')
			var accordionElement = document.getElementById('collapseOne');
		    var bsCollapse = new bootstrap.Collapse(accordionElement, {
		        toggle: true
		    });

			var accordionElement = document.getElementById('collapseTwo');
		    var bsCollapse = new bootstrap.Collapse(accordionElement, {
		        toggle: true
		    });
	    @endif
	}


	@if(!$priorSearch)
		$("#roundTripDate").show();
		$("#oneWayDate").hide();
	@else

		@if(($priorSearch["end"] ?? false) == false || $priorSearch["end"] == "undefined")
			$("#roundTripDate").hide();
		@else
			$("#oneWayDate").hide();
		@endif
	@endif
	$(".value-button").click(function(){
		// calc number of travelers
		var adults = parseInt($("#adultNumber").val());
		var children = parseInt($("#childrenNumber").val());
		var infants = parseInt($("#infantsNumber").val());

		var passengers = parseInt(adults + children + infants);
		if(children != 0 || infants != 0){
			$("#passengers").text(passengers + " Travelers");
		} else {
			$("#passengers").text(passengers + " Adults");
		}
		
    });

    $('#flightSearch').submit(function(e){
	  //e.preventDefault();
	});

	initFlightTo();
	initFlightFrom();

	$("#flipLocations").click(function(){
		var flightFrom = $('#flight-from').select2('data')[0];
		var flightTo = $('#flight-to').select2('data')[0];

		$("#flight-from").select2('destroy');
		clearOptions("flight-from");
		$('#flight-from').append('<option value="1">' + flightTo.text + '</option>')
		$("#flight-from").select2();

		initFlightFrom();

		$("#flight-to").select2('destroy');
		clearOptions("flight-to");
		$('#flight-to').append('<option value="1">' + flightFrom.text + '</option>')
		$("#flight-to").select2();

		initFlightTo();
	});

	$('#search').click(function(){
		var flightFrom = $('#flight-from').select2('data')[0];
		var flightTo = $('#flight-to').select2('data')[0];
		var flightType = $('.flight-type[type=radio]:checked').val();
		var flightClass = $('#flight-class').val();
		var flightStartDate = $('.datepicker').val().split(" - ")[0];
		var flightReturnDate = $('.datepicker').val().split(" - ")[1];


		var adults = $("#adultNumber").val();
		var children = $("#childrenNumber").val();
		var infants = $("#infantsNumber").val();

	  	var fieldCheck = checkMandatoryFields(flightFrom, flightTo, flightType, flightClass, flightStartDate, flightReturnDate, adults, children, infants);

	  	if(fieldCheck === true){
	  		window.location.href = "/flights/search" +
	  		"?from=" + flightFrom.text + 
	  		"&to=" + flightTo.text + 
	  		"&type=" + flightType + 
	  		"&class=" + flightClass + 
	  		"&start=" + flightStartDate +  
	  		"&end=" + flightReturnDate + 
	  		"&adults=" + adults + 
	  		"&children=" + children + 
	  		"&infants=" + infants
	  	}
	});

	
	

	function checkMandatoryFields(flightFrom, flightTo, flightType, flightClass, flightStartDate, flightReturnDate, adults, children, infants){
		console.log(flightFrom);

		if(isBlank(flightFrom.text) || flightFrom.text == "Select Origin"){
			swal({
                  icon: 'error',
                  title: 'Missing required fields',
                  text: 'Please add where you are flying from',
            });

            return false;
		}

		if(isBlank(flightTo.text) || flightTo.text == "Select Destination"){
			swal({
                  icon: 'error',
                  title: 'Missing required fields',
                  text: 'Please add where you are flying to',
            });

            return false;
		}

		if(isBlank(flightType)){
			swal({
                  icon: 'error',
                  title: 'Missing required fields',
                  text: 'You need to select your flight type',
            });

            return false;
		}

		if(isBlank(flightClass)){
			swal({
                  icon: 'error',
                  title: 'Missing required fields',
                  text: 'You need to select your seat class',
            });

            return false;
		}



		if(adults == 0 && children == 0 && infants == 0){
			swal({
                  icon: 'error',
                  title: 'Missing required fields',
                  text: 'You need to input at least one traveler',
            });
            return false;
		}
		if(infants >= 1 && adults == 0){
			swal({
                  icon: 'error',
                  title: 'Missing required fields',
                  text: 'You need to input at least one adult',
            });
            return false;
		}

		return true;
	}

	function isBlank(value){
		if(value == "" || value == undefined || value == null){
			return true;
		}
		return false;
	}

	function clearOptions(elementId) {
	   var courseSelect = document.querySelector("#" + elementId);

	   while (courseSelect.childNodes[0] != null) {
	       courseSelect.childNodes[0].remove();
	   }
	}

	var dayDif = 0;

	@if(!$priorSearch)
			$(".datepicker").daterangepicker({
				locale: {
			          format: 'MM/DD/YYYY',
			    },

			    singleDatePicker: false,
				showDropdowns: true,
				drops: "auto",
				opens:"auto",
				startDate: moment().add(30, 'days').format("MM/DD/YYYY"),
				endDate: moment().add(37, 'days').format("MM/DD/YYYY"),
				minDate: moment().format("MM/DD/YYYY"),
				maxDate: moment().add(10, 'M').format("MM/DD/YYYY")
			});
	@else


			@if(array_key_exists("end", $priorSearch) == false || ($priorSearch["end"] ?? null) == null || ($priorSearch["end"] ?? null) == "undefined")
				$('.datepicker').daterangepicker({
				    locale: {
				          format: 'MM/DD/YYYY',
				    },
				    singleDatePicker: true,
				    showDropdowns: true,
				    startDate: "{{ $priorSearch['start'] ?? Carbon\Carbon::now()->format('m/d/Y') }}",
				    drops: "auto",
					opens:"auto"
				});
			@else
				$('input[name="dates"]').daterangepicker({
					drops: "auto",
					opens:"auto",
					startDate: "{{ $priorSearch['start'] }}",
					endDate: "{{ $priorSearch['end'] }}",
					minDate: moment().format("MM/DD/YYYY"),
					maxDate: moment().add(10, 'M').format("MM/DD/YYYY")
				});
			@endif

	@endif

	$("body").on("click",".applyBtn", function(){
		updateDates();
	});

	$("body").on("click",".cancelBtn", function(){
		updateDates();
	});


	function updateDates(){
		$("#datepickModal").modal('hide');
		var flightStartDate = $('.datepicker').val().split(" - ")[0];
		var flightReturnDate = $('.datepicker').val().split(" - ")[1];
		var flighType = $(".flight-type[type=radio]:checked").val();

		if(flighType == "one-way"){
			$(".datePick").text(flightStartDate);
		} else {
			$(".datePick").text(flightStartDate + " - " + flightReturnDate);
		}
	}

	$(".datePick").click(function(){
		var roundTripDate = $("#roundTripDate");
		var oneWayDate = $("#oneWayDate");

		setTimeout(function(){
			$(".modal-dialog .datepicker").trigger("click");
		}, 500);

		var flighType = $(".flight-type[type=radio]:checked").val();
	});

	$(".flight-type[type=radio]").click(function(){
		var flightStartDate = $('.datepicker').val().split(" - ")[0];
		var flightReturnDate = $('.datepicker').val().split(" - ")[1];
		if(!isBlank(flightReturnDate)){
			dayDif = moment(flightReturnDate).diff(moment(flightStartDate), 'days', false);

		}

		if($(this).val() == "one-way"){

			$("#roundTripDate").hide();
			$("#oneWayDate").show();
			$(".datePick").text(flightStartDate);
			$('.datepicker').daterangepicker({
			    locale: {
			          format: 'MM/DD/YYYY',
			    },
			    singleDatePicker: true,
			    showDropdowns: true,
			    startDate: flightStartDate,
			    drops: "auto",
				opens:"auto",
				minDate: moment().format("MM/DD/YYYY"),
				maxDate: moment().add(10, 'M').format("MM/DD/YYYY")
			});
		} else {
			if(flightReturnDate == undefined){
				flightReturnDate = moment(flightStartDate).add(7, 'days').format("MM/DD/YYYY");
			}

			$(".datePick").text(flightStartDate + " - " + flightReturnDate);
			$("#oneWayDate").hide();
			$("#roundTripDate").show();
			$('.datepicker').daterangepicker({
			    locale: {
			          format: 'MM/DD/YYYY',
			    },
			    showDropdowns: true,
			    startDate: flightStartDate,
			    endDate: (dayDif > 0) ? moment(flightStartDate).add(dayDif, 'days').format("MM/DD/YYYY") : moment(flightStartDate).add(7, 'days').format("MM/DD/YYYY"),
			    drops: "auto",
				opens:"auto",
				minDate: moment().format("MM/DD/YYYY"),
				maxDate: moment(flightStartDate).add(10, 'M').format("MM/DD/YYYY")
			});
		}
	});

	function getRandomInt(max) {
	  return Math.floor(Math.random() * max);
	}

	function initFlightFrom(){
		@if(isset($priorSearch['from']))
		$("#flight-from").append("<option value='{{ $priorSearch['from'] }}' selected>{{ $priorSearch['from'] }}</option>");
		@endif

		

		$("#flight-from").select2({
		    placeholder: "Select Origin",
		    minimumInputLength: 3,
		    language: {
			    'inputTooShort': function () {
			        return '';
			    }
			},
		    delay:250,
		    data: function (term) {
	            return {
	                term: term
	            };
	        },
			ajax: {
			    url: '/flights/data/airports',
			    dataType: 'json',
		        processResults: function (data) {

		            return {

		                results: $.map(data, function (item) {

		                	var item = JSON.parse(item);

		                	return item.map((airport, i) => {
		                		var id = parseInt(i + getRandomInt(30));
		                		return {
			                        text: airport.text,
			                        id: id
			                    }
		                	});
		                })
		            };
		        }
			}
		});

		


		return true;
	}

	function initFlightTo(){
		@if(isset($priorSearch['to']))
		$("#flight-to").append("<option value='{{ $priorSearch['to'] }}' selected>{{ $priorSearch['to'] }}</option>");
		@endif

		return $("#flight-to").select2({
		    placeholder: "Select Destination",
		    delay:250,
		    minimumInputLength: 3,
		    data: function (term) {
	            return {
	                term: term
	            };
	        },
			ajax: {
			    url: '/flights/data/airports',
			    dataType: 'json',
		        processResults: function (data) {
		            return {
		                results: $.map(data, function (item) {
		                	var item = JSON.parse(item);

		                	return item.map((airport, i) => {
								var id = parseInt(i + getRandomInt(30));
		                		return {
			                        text: airport.text,
			                        id: id
			                    }
		                	});
		                })
		            };
		        }
			}
		});
	}

	function increaseAdultCount() {
	  var value = parseInt(document.getElementById('adultNumber').value, 10);
	  value = isNaN(value) ? 0 : value;
	  value++;
	  document.getElementById('adultNumber').value = value;
	}

	function decreaseAdultCount() {
	  var value = parseInt(document.getElementById('adultNumber').value, 10);
	  value = isNaN(value) ? 0 : value;
	  value < 1 ? value = 1 : '';
	  value--;
	  document.getElementById('adultNumber').value = value;
	}

	function increaseChildrenCount() {
	  var value = parseInt(document.getElementById('childrenNumber').value, 10);
	  value = isNaN(value) ? 0 : value;
	  value++;
	  document.getElementById('childrenNumber').value = value;
	}

	function decreaseChildrenCount() {
	  var value = parseInt(document.getElementById('childrenNumber').value, 10);
	  value = isNaN(value) ? 0 : value;
	  value < 1 ? value = 1 : '';
	  value--;
	  document.getElementById('childrenNumber').value = value;
	}

	function increaseInfantsCount() {
	  var value = parseInt(document.getElementById('infantsNumber').value, 10);
	  value = isNaN(value) ? 0 : value;
	  value++;
	  document.getElementById('infantsNumber').value = value;
	}

	function decreaseInfantsCount() {
	  var value = parseInt(document.getElementById('infantsNumber').value, 10);
	  value = isNaN(value) ? 0 : value;
	  value < 1 ? value = 1 : '';
	  value--;
	  document.getElementById('infantsNumber').value = value;
	}

	$(document).on('select2:open', function(e) {
	  document.querySelector(`[aria-controls="select2-${e.target.id}-results"]`).focus();
	});

	function getDiscountPercent(productClass, msrp, $estimatedProfitMargin = null){
	    if (productClass === 'airfare') {
	    	var newPrice = msrp - 250;
	        if (msrp > 5000) {
	            // 5 percent off business and first class flights
	            return (newPrice / msrp);
	        } else if (msrp < 5000 && msrp >= 2000) {
	            // 10 percent off premium economy
	            return (newPrice / msrp);
	        } else if (msrp < 2000 && msrp >= 500) {
	            // 15 percent off economy
	            return (newPrice / msrp);
	        } else if (msrp < 500 && msrp >= 250) {
	            // 30 percent off domestic economy
	            return (newPrice / msrp);
	        } else {
	            // Handle any other msrp ranges if needed
	            return 0; // No discount applied
	        }
	    } else {
	        // Handle other product classes if needed
	        return 0; // Default return value when not 'airfare'
	    }
	}
</script>