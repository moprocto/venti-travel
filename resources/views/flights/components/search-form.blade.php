
		<form method="GET" action="/flights/search" id="flightSearch">
			<input type="hidden" name="adults" value="{{ $priorSearch['adults'] ?? 1 }}">
			<input type="hidden" name="children" value="{{ $priorSearch['children'] ?? 0 }}">
			<input type="hidden" name="infants" value="{{ $priorSearch['infants'] ?? 0 }}">
			<div class="row flight-search-details-row" style="align-items:center; margin-bottom: 10px;">
				<div class="col-lg-1 col-xs-6 col-sm-6 col-md-6" style="min-width:106px; margin-right: 0px; font-size: 12px; display: flex;align-items:center;">
					<input type="radio" class="flight-type" name="flight-type"
					@if(isset($priorSearch['type']))
						@if($priorSearch['type'] == 'round-trip')
							checked
						@endif
					@else

					checked

					@endif

					value="round-trip"> <span style="margin-left:5px">Round-Trip</span>
				</div>
				<div class="col-lg-1 col-xs-6 col-sm-6 col-md-6" style="min-width:95px; margin-right: 0px; font-size: 12px; display: flex;align-items:center;">
					<input type="radio" class="flight-type" name="flight-type" 

						@if(isset($priorSearch['type']) && $priorSearch['type'] == 'one-way')
							checked
						@endif

					value="one-way"> <span style="margin-left:5px">One-Way</span>
				</div>
				<div class="col-lg-1 col-xs-6 col-sm-6 col-md-6" style="min-width:110px; margin-right: 10px; display:flex;">
					<a href="#" data-bs-toggle="modal" data-bs-target="#passengerModal" style="font-size:12px; color:black;">
					  <span id="passengers">{{ $priorSearch['adults'] ?? 1 }}  
					@if(!isset($priorSearch))
						Adult(s)
					@else
						Traveler(s)
					@endif
			</span> <i class="fa fa-chevron-down"></i>
					</a>
					@include('flights.components.passengers')
				</div>
				<div class="col-lg-1 col-xs-6 col-sm-6 col-md-6" style="min-width:80px; display:flex">
					@include('flights.components.flight-class')
				</div>
			</div>
			<div class="row" style="align-items:baseline;">
				<div class="col-md-4" style="margin-right:0">
					<select class="form-control flightInput required" id="flight-from" style="font-size:12px">
						<option value="">Select Origin</option>
					</select>
					<!--
						<span class="d-flex form-small mt-1" style="align-items:center"><input type="checkbox" name="nearbyFrom" id="nearbyFrom" class="mr-2"> Include Nearby Airports</span>
					-->
				</div>
				<div class="col-md-4">
					<select class="form-control flightInput required" id="flight-to">
						<option value="">Select Destination</option>
					</select>
					<!--
					<span class="d-flex form-small mt-1" style="align-items:center"><input type="checkbox" name="nearbyTo" id="nearbyTo" class="mr-2"> Include Nearby Airports</span>
					-->
				</div>
				<div class="col-md-2 flight-search-dates">
					<button class="btn btn-white text-left datePick" type="button" data-bs-toggle="modal" data-bs-target="#datepickModal" id="roundTripDate"style="font-size:11px; width:100%; border:1px solid #ced4da; padding:10px;">
						@if($priorSearch)
							@if(isset($priorSearch['end']) && $priorSearch['end'] != 'undefined' && $priorSearch['end'] != '' && isset($priorSearch['start'])) 
								{{ $priorSearch['start'] }} - {{ $priorSearch['end'] }}
							@endif
						@else
							{{ Carbon\Carbon::now()->add("30","days")->format("m/d/Y") }} - {{ Carbon\Carbon::now()->add("37","days")->format("m/d/Y") }}
						@endif
					</button>
					<button class="btn btn-white text-left datePick" type="button" data-bs-toggle="modal" data-bs-target="#datepickModal" id="oneWayDate"style="font-size:11px; width:100%; border:1px solid #ced4da; padding:10px;">
						@if($priorSearch)
							{{ $priorSearch['start'] ?? Carbon\Carbon::now()->add("30","days")->format("m/d/Y") }}
						@else
							{{ Carbon\Carbon::now()->add("30","days")->format("m/d/Y") }}
						@endif
					</button>
				</div>
				<div class="col-md-1 flight-search-submit">
					<button class="btn btn-primary" type="button" id="search"><i class="fa fa-search"></i> <span class="btn-text-sm">Search</span></button>
				</div>
			</div>
		</form>
	