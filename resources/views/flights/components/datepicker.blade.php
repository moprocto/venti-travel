<div class="modal fade" id="datepickModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true" style="background-image: linear-gradient(112.1deg, rgba(125, 110, 112,0.5) 11.4%, rgba(152, 135, 125,0.8) 70.2%); ">
	  <div class="modal-dialog">
	    <div class="modal-content" style=" padding-top:20px">
			<div class="modal-header" style="padding:5px 10px;">
				<h5>Select Dates</h5>
			</div>
		    <div class="modal-body">
		    	<input type="text" name="dates" class="form-control datepicker" style="font-size:11px">
			</div>
	    </div>
	</div>
</div>