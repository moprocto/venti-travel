@extends('layouts.curator-master')

@php

$baggages = 0;
$total_amount = $data["total_amount"];
$base_amount = $data["base_amount"];
$tax_amount = $data["tax_amount"];

// we need to add up the cost of all fares

$markup = 0;
$i = 0;
$destination = ["name" => ""];
$numSlices = sizeof($data["slices"]); // more than one slice indicates round trip
if($numSlices == 1){
	$origin = $data["slices"][0]["segments"][0];
	$destination = $data["slices"][0]["segments"][sizeof($data["slices"][0]["segments"]) - 1]['destination']['city_name'];
}else{
	$origin = $data["slices"][0]["segments"][0];
	$destination = $data["slices"][sizeof($data["slices"]) - 1]["segments"][0]['origin']['city_name'];
}

$flightType = "one-way";
if($numSlices > 1){
	$flightType = "round trip";
}
$duration = "";

$supportedDocs = [];

foreach($data["supported_passenger_identity_document_types"] as $doc){
	array_push($supportedDocs, ucfirst(str_replace("_"," ",$doc)));
}

$requires_instant_payment = $data["payment_requirements"]["requires_instant_payment"];

$passengers = $data["passengers"];

$cabin_class = "economy";

if($priorSearch){
	$cabin_class = $priorSearch["class"];
}

$available_services = $data["available_services"] ?? [];

$canPickSeats = $canAddBags = false;

$checkedBagFee = 0;
$checkedBagCurrency = $seatSelectionCurrency = "USD";

foreach($available_services as $service){
	$serviceType = $service["type"] ?? false;

	if($serviceType){
		if($serviceType == "seat"){
			$canPickSeats = true;
		}
		if($serviceType == "baggage"){
			$bagMSRP = $service["total_amount"];
			$canAddBags = true;
			$multiplier = getMarkupMultiplier("baggage", $service["total_amount"]);
			$checkedBagCurrency = $service["total_currency"];
			if($multiplier < 2){
				$checkedBagFee = round($service["total_amount"] * $multiplier,0);
			}
			else {
				$checkedBagFee = 50;
			}
		}
	}
}

$user = Session::get('user');
$customClaims = $user->customClaims ?? [];
$phoneNumber = $customClaims["phoneNumber"] ?? "";

$pax_docs_required = $data["passenger_identity_documents_required"];


// we need to communicate the terms of refunds, changes, and penalties here

$changeable = getTripCancelationDetails($data["conditions"]);
$cancelable = getTripChangeDetails($data["conditions"]);
$refundAllowed = $data["conditions"]["refund_before_departure"]["allowed"] ?? false;
$refundPenalty = $data["conditions"]["refund_before_departure"]["penalty_amount"] ?? false;
$changesAllowed = $data["conditions"]["change_before_departure"]["allowed"] ?? false;
$changePenalty = $data["conditions"]["change_before_departure"]["penalty_amount"] ?? false;

@endphp

@section('css')
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/switchery/0.8.2/switchery.min.css" integrity="sha512-uyGg6dZr3cE1PxtKOCGqKGTiZybe5iSq3LsqOolABqAWlIRLo/HKyrMMD8drX+gls3twJdpYX0gDKEdtf2dpmw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
	<link rel="stylesheet" type="text/css" href="/assets/css/steps.css">
	<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
	<link rel="stylesheet" href="/assets/css/intlTelInput.min.css">
	<style type="text/css">
		.form-label{
			font-weight: 500;
		}
		.wizard>.content>.body{
            height: 100%;
            position: relative;
            padding: 0;
        }
        .wizard>.content{
            min-height: 120px;
            background: none;
        }
        
		.form-small{
			padding: 0;
			font-size: 12px;
		}
		.no-bg{
			background-color: transparent;
			border: none;
		}
		.select2-container .select2-selection--single,.select2-container--default .select2-selection--single .select2-selection__clear,.select2-container--default .select2-selection--single .select2-selection__arrow{
			height: calc(2.75rem + 2px);
			font-size: 12px;
			border-radius: .375rem;
		}
		.select2-selection__rendered{
			padding-top: 5px;
			font-size: 1rem;
			line-height: 35px !important;
		}
		.select2-container--default .select2-selection--single{
			border: 1px solid #ced4da;
		}
		.datepicker{
			max-width: 170px;
			font-size:12px;
			height:38px;
			
		}
		.btn-primary{
			border-radius: 4px;
		}
		.value-button{
			display: inline-block;
		    border: 1px solid #ddd;
		    margin: 0px;
		    width: 40px;
		    height: 40px;
		    text-align: center;
		    vertical-align: middle;
		    padding: 7px 0;
		    background: #eee;
		    -webkit-touch-callout: none;
		    -webkit-user-select: none;
		    -khtml-user-select: none;
		    -moz-user-select: none;
		    -ms-user-select: none;
		    user-select: none;
		}
		.decrease{
			margin-right: -4px;
    		border-radius: 8px 0 0 8px;
		}
		.increase{
			margin-left: 0;
    		border-radius: 0 8px 8px 0;
		}
		.steppable{
			text-align: center;
		    border: none;
		    border-top: 1px solid #ddd;
		    border-bottom: 1px solid #ddd;
		    margin: 0px;
		    width: 40px;
		    height: 40px;
		}
		td.vertical-align{
			vertical-align: middle;
		}
		tr.borderless td{
			border-bottom: none !important;
		}
		.table,.card-body{
			padding-bottom: 0;
			margin: 0;
		}
		ul.nolist{
			list-style: none;
		}
		.text-small{
			font-size: 12px;
		}
		.results-table .text-left{
			font-size: 13px;
			font-weight: 700;
		}
		.results-table .text-right{
			font-size: 13px;
		}
		table h5, table h2, table h3{
			padding: 0;
			margin: 0 !important;
		}
		.table tr td{
			vertical-align: middle;
		}
		.card{
			background: transparent;
		}
		.bg-white{
			background-color: transparent;
			background-image: linear-gradient(112.1deg, rgba(248, 248, 248,0) 11.4%, rgba(255, 255, 255, 0.7) 70.2%);
		}
		.row_total td{
			border-bottom: none;
		}
		.driversInput, #points-input-module, .hide{
			display: none;
		}
		.modal-body .card{
			box-shadow: none;
		}
		 .cabin-row, .cabin-seat{
			display: inline-flex;
			vertical-align: middle;
			align-items: center;
		}
		.cabin-row{
			justify-content: center;
		}
		.cabin-seat{
			width: 49px;
			height: 52px;
			margin: 2px;
			justify-content: center;
			background:rgb(200,200,200);
			border-radius: 7px;
			flex-direction: column;
		}
		.cabin-seat.assignable{
			background:rgb(250,250,245);
			cursor: pointer;
		}
		.cabin-seat.assignable:hover{
			background:rgb(255,255,255);
			box-shadow: 0 1px 1px hsl(0deg 0% 0% / 0.015), 0 2px 2px hsl(0deg 0% 0% / 0.015), 0 4px 4px hsl(0deg 0% 0% / 0.015), 0 8px 8px hsl(0deg 0% 0% / 0.015), 0 16px 16px hsl(0deg 0% 0% / 0.015)
		}
		.cabin-seat p{
			display: contents;
			align-self:  center;
			font-size: 14px;
		}

		#spending-power-balance h1, #cash-balance h1, #points-balance h1{
			font-size: 200% !important;
		}
		.required.error,.input-group.error .btn-white, .card.error{
	      border-color: #dc3545 !important;
	   }
	   label .fa-lock{
	   	opacity: 0.3;
	   }
	   .accordion-success{
	   	background-color: #d1e7dd !important;
	   	color: #0f5132 !important;
	   }
	   .accordion-success::after{
	   		color: #0f5132 !important;
	   }

	   #seat-map-picker .content{
	   	max-height: 450px;
	   	overflow-y: scroll;
	   }
	   .cabin-seat.selected{
	   	background: #198754;
	   	color: white;
	   }
	   .cabin-seat.selected:hover{
	   	color: black;
	   }
	   .p-name{
	   	display: none !important;
	   }
	   .cabin-seat.selected .p-name, .cabin-seat.taken .p-name{
	   	display:block !important;
	   	font-size:7px;
	   }
	   .cabin-seat.taken{
	   	display:block !important;
	   	font-size:7px;
	   	background: #9dc8e2 !important;
	   	pointer-events: none;
	   }
	   .ready, #ccFeeDisclosure{
	   	display: none;
	   }
	   .accordion-success .ready{
	   	display: inline-block;
	   	background: #0f5132 !important;
	    color: white;
	    font-size: 10px;
	    padding: 5px 10px;
	    margin-left: 10px;
	    border-radius: 14px;
	    margin-top: -2px;
	   }
	   @media(max-width: 500px){
	   	.accordion-success .ready span{
	   		display: none;
	   	}
	   }

	   #seat-map-picker{
	   	background: white;
	   	padding: 20px;
	   	border-radius: 10px;
	   	margin-bottom: 100px;
	   }
	   body #seat-map-picker ul{
	   	list-style: none !important;
	   }
	   #seat-map-picker.wizard>.steps .current a{
	    color: black;
	    border-bottom: 2px solid black;
	   }

	   #seat-map-picker.wizard >.steps a{
	   	padding: 10px 20px !important;
	    background: white;
	    font-size: 0.75em;
	    border-radius: 0;
	    color: #aaa;
	    border-bottom: 2px solid #aaa;
	   }

	  #seat-map-picker.wizard >.steps .number,
	  #seat-map-picker .actions{
	  	display: none;
	  }
	  #seat-map-picker.wizard>.actions a{
	  	background: black;
	  }
	</style>
@endsection

@section('content')
<div class="gradient-wrap" style="background: transparent; width:100%;">
    <!--  HERO -->
    <div class="section-wrapper black position-relative">
    	<div class="row">
    		<div class="col-md-12">
    			<div class="d-flex" style="justify-content:space-between; flex-direction:row">
    				<h4>Flight Offer <span class="text-small" id="offer-expiry"></span></h4>
    				<button type="button" class="btn btn-black mb-3 mr-3" data-bs-toggle="modal" data-bs-target="#flightPlanModal">Review Flight Plan</button>
    			</div>
    		</div>
    		<div class="col-md-12">
    			<div id="checkout-wizard">
    				<h3>Passengers</h3>
    				@if(env('DUFFEL_BOOKABLE_LIMIT') >= $total_amount)
    					<section>
	    					@php
	    						$ableToBook = false;
	    						if(Session::get('user') !== null){
	    							$subscription = Session::get('user')->customClaims["subscription"] ?? "buddy";
	    							
	    							if($subscription != "buddy"){
	    								$ableToBook = true;
	    							}
	    						}
	    					@endphp
	    					@if($ableToBook)
								@include('flights.components.flight-passengers-input',[
									"docsRequired" => $pax_docs_required
								])
							@else
							<div class="card bg-white">
								<div class="card-body">
									<h4>Thank you for your interest in booking a Venti Flight.</h4>
									<p>At this time, you need a paid Venti <a href="/boardingpass" target="_blank">Boarding Pass</a> account to make travel purchases.</p>
									<a href="/login" target="_blank" class="btn btn-success">Log In</a>
									<br>
									<br>
								</div>
							</div>
						@endif
    					</section>
    				@else
    					<section>
    						<h4>Thank you for your interest in booking a Venti flight.</h4>
    						<p>Please contact support via admin@venti.co to proceed with booking.</p>
    					</section>
    				@endif
    				@if(seatableAirline($data['owner']['name']))
	    				<h3>Seat Selection</h3>
	    				<section id="seatSelectionStep">
	    					
	    					<div style="display: flex; justify-content: space-between; flex-direction: column; align-items:center;">
						        <div id="seat-map-picker" style="width:100%; min-height:350px;">
						        	<div style="text-align:center;align-items: center;display: flex;flex-direction: column;"><img src="/assets/img/flight-loader-circle.gif" class="d-block"><h3>Searching for Seats...</h3></div>
						        </div>
						      </div>
	    				</section>
    				@endif
    				<h3>Finalize Order</h3>
    				<section>
    					<div class="col-md-12">
			    			@if(Session::get('user') == null)
			    				<h4>Venti Boarding Pass Required <a href="/boardingpass" target="_blank"><img src="/assets/img/icons/boarding-pass.png" style="width:40px;margin-top: -10px;"></a></h4>
			    				<div class="card bg-white mt-3">
									<div class="card-body">
										<p>Flight bookings are limited to Boarding Pass members only.</p>
										<a href="/login" target="_blank" class="btn btn-primary mb-3">Log In</a>
										<p>Then refresh this page after logging in</p>
									</div>
								</div>
			    			@else
			    				@if(env('APP_ENV') == "local")
			    					<button class="retry btn btn-dark">retry</button>
			    				@endif
			    				@include("flights.components.checkout-step")
				    		@endif
				    	</div>
    				</section>
       			</div>
    		</div>
    	</div>
    </div>

    <div class="modal fade" id="flightPlanModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true" style="background-image: linear-gradient(112.1deg, rgba(125, 110, 112,0.5) 11.4%, rgba(152, 135, 125,0.8) 70.2%); ">
	  <div class="modal-dialog modal-dialog-centered modal-lg">
	    <div class="modal-content">
	      <div class="modal-header" style="position: absolute;
	    right: 0;
	    top: 10px; border-bottom: transparent; z-index: 99;">
	        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
	      </div>
	      <div class="modal-body">
    		@include('flights.components.flight-plan')
	      </div>
	    </div>
	  </div>
	</div>
</div>
@include("boardingpass.panels.points-explainer")
@include("boardingpass.panels.balance-explainer")
@include("boardingpass.panels.spending-power")
@include('flights.components.cfar-explainer')
@include('flights.components.price-match-explainer', ["pm" => "flights"]))
@include('flights.components.amenities')
@include('flights.components.loading')

@endsection

@section('js')
<script type="text/javascript" src="/assets/js/jquery.steps.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/switchery/0.8.2/switchery.min.js" integrity="sha512-lC8vSUSlXWqh7A/F+EUS3l77bdlj+rGMN4NB5XFAHnTR3jQtg4ibZccWpuSSIdPoPUlUxtnGktLyrWcDhG8RvA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script type="text/javascript" src="/assets/js/flights.js"></script>
<script src="https://cdn.jsdelivr.net/npm/intl-tel-input@18.2.1/build/js/intlTelInput.min.js"></script>
	<script type="text/javascript">
			$.ajaxSetup({
				headers: {
			    	'X-CSRF-TOKEN': '{{ csrf_token() }}'
				}
		    });

			// init select2 dropdowns

			$(".select2").select2({
			    placeholder: "Select Country",
			    minimumInputLength: 3
			});

			var wizardCheckout = $("#checkout-wizard").steps({
				headerTag: "h3",
			    bodyTag: "section",
			    transitionEffect: "slideLeft",
			    onStepChanging: function(event, currentIndex, priorIndex){
			    	$("#checkout-wizard .actions ul li:eq(2) a").hide();
			    	if(currentIndex > 0){
			    		@if(seatableAirline($data['owner']['name']))
				    		$("#seat-map-picker-p-0").hide();
				    		setTimeout(function(){
				    			$("#seat-map-picker-t-0").click();
				    			$("#seat-map-picker-p-0").addClass("current");
								$("#seat-map-picker-p-0").css("display", "block");
								$("#seat-map-picker-p-0").show();
				    		}, 1000)
			    		@endif
	    				
			    		return true;
			    	}
			    	if(currentIndex == 0){
			    		// seat selection
			    		// check if seat selection is allowed
			    		
			    		var completion = checkPaxComplete();

			    		if(completion){
			    			// check if seatMap already rendered
			    			@if(seatableAirline($data['owner']['name']))
				    			if($("#seat-map-picker seatsection").length == 0){
				    				getSeatMaps();
				    				
				    			}
			    			@endif
			    			
			    			return true;
			    		}
			    		else{
		    				@if(Session::get('user') !== null)
			    			swal.fire({
								icon: 'warning',
								title: 'You must complete passenger data before continuing.',
								text: 'Be sure to press the blue "Save Passenger Info" button to save changes.',
								confirmButtonText: 'Ok'
							});
			    			@endif
			    			
			    		}
			    		
			    		return false;
			    	}
			    },
			    onStepChanged: function(event, currentIndex, priorIndex){
			    	@if(seatableAirline($data['owner']['name']))
				    	if(currentIndex == 2){
				    		$("#checkout-wizard-p-2").addClass("current");
							$("#checkout-wizard-p-2").css("display", "block");
							$("#checkout-wizard-p-2").show();
							// this should only fire once?
							getCheckout();
				    	}
				    	else{
				    		$("#checkout-wizard-p-2").hide();
				    	}
			    	@else
				    	if(currentIndex == 1){
				    		$("#checkout-wizard-p-1").addClass("current");
							$("#checkout-wizard-p-1").css("display", "block");
							$("#checkout-wizard-p-1").show();

							getCheckout();
				    	}
				    	else{
				    		$("#checkout-wizard-p-1").hide();
				    	}
			    	@endif
			    }
			});

			// init all the telephone inputs

			var smsInputs = document.querySelectorAll(".pax_cell");
			var iti = [];

			for(sms = 1; sms <= smsInputs.length; sms++){


				window.intlTelInput(smsInputs[sms - 1], {
			      formatOnDisplay: true,
			      utilsScript: "https://cdn.jsdelivr.net/npm/intl-tel-input@18.2.1/build/js/utils.js"
			    });

			    iti[sms] = intlTelInput(smsInputs[sms-1]);

			}
    		
    		initCodeInput();

		    $(".destination").each(function(){
		    	$(this).text("{{ $destination }}");
		    });




		    $("#finalizePurchase").click(function(){
		    	var btn = $(this);
		    	btn.addClass("disable-while-loading");

		    	var passable = true;

		    	passable = checkPaxData("");

		    	if(passable !== true){
		    		btn.removeClass("disable-while-loading");
					return false;
		    	}

		    	// need to build super object for the order

		    	var numPassengers = {{ sizeof($passengers) }};
		    	var paxData = [];

		    	for(i = 1; i <= numPassengers; i++){

		    		var pax_cell = iti[i].getNumber();

		    		paxData.push({
		    			id: $(".pax_id" + i).val(),
		    			title: $(".pax_title" + i).val(),
		    			first_name: $(".pax_fname" + i).val(),
		    			last_name: $(".pax_lname" + i).val(),
		    			email: $(".pax_email" + i).val(),
		    			dob: $(".pax_dob" + i).val(),
		    			phone: pax_cell,
		    			ktn: $(".pax_ktn" + i).val(),
		    			gender: $(".pax_gender" + i).val(),
		    			citizenship: $(".pax_citizenship" + i).val(),
		    			document_type: $(".pax_doc" + i).val(),
		    			passport_expiration: $(".pax_passport_exp" + i).val(),
		    			passport_number: $(".pax_passport_number" + i).val(),
		    			passport_country: $(".pax_passport_country" + i).val()
		    		});
		    	}


		    	// add the upgrades to the order

		    	var addedBags = [];

				$(".addBaggage:checked").each(function(){
					addedBags.push({
						id: $(this).val()
					});
				});

				var addedSeats = [];

				$("#selected-seats .mr-3").each(function(){
					addedSeats.push({
						passenger: $(this).attr("data-passenger"),
						segment: $(this).attr("data-segment"),
						id: $(this).attr("data-seat-id"),
					});
				});

		    	var pointsApplied = $("#maxPointsInput").val();
		    	var elem = document.querySelector('.js-switch');
		    	var cfar = elem.checked;
		    	var destination = "{{ $destination }}";
		    	var paymentPref = $("#paymentPrefSelected").val();

		    	

		    	if(paymentPref != "pass"){
		    		// if CC, calculate amount due via payment intent
		    		if(paymentPref.length < 9){
		    			btn.removeClass("disable-while-loading");
		    			swal.fire({
							icon: 'warning',
							title: 'You missed a step',
							text: "Please select a valid payment method",
							confirmButtonText: 'Ok'
						});
						return false;
		    		}
		    	}

		    	$("#loadingModal").modal("show");

		    	$.ajax({
		            url: "{{ route('flight-order') }}",
					type: 'POST',
					async: true,
					processData: true,
			        dataType: 'json',
		            data:{
		                offer : "{{ $data['offer'] }}",
		                pax: paxData,
		                cfar: cfar,
		                points: pointsApplied,
		                destination: destination,
		                addedBags: addedBags,
		                addedSeats: addedSeats,
		                paymentPref: paymentPref
		            },
		            success: function (data) {
		            	$("#loadingModal").modal("hide");

		            	if(data.error != undefined){
		            		// we found an error in the order
		            		btn.removeClass("disable-while-loading");

		            		swal.fire({
								icon: 'error',
								title: 'Your order could not be processed',
								text: data.message,
								confirmButtonText: 'Ok'
							});

		        			return false;
		            	}
		            	if(data.success != undefined){
		            		swal.fire({
								icon: 'success',
								title: 'Your order was processed successfully',
								text: data.message,
								confirmButtonText: 'Ok'
							});

		            		setTimeout(function(){
		            			window.location.href = "/trips/" + data.order_id;
		            		}, 10000);
		        			// redirect user to order confirmation page
		            	}
		            },
		            error: function(e){
		            	$("#loadingModal").modal("hide");
		            	btn.removeClass("disable-while-loading");

		            	swal.fire({
							icon: 'error',
							title: 'Your order could not be processed',
							text: e.message,
							confirmButtonText: 'Ok'
						});
						return false;
		            }
		       });
		    });

		    $(".required").on("keyup", function(){
		    	var field = $(this).val();
		    	if(field != "" && field != undefined){
		    		$(this).removeClass("error");
		    	}
		    });

		    $(".required").on("change", function(){
		    	var field = $(this).val();
		    	if(field != "" && field != undefined){
		    		$(this).removeClass("error");
		    	}
		    });

		    var elem = document.querySelector('.js-switch');

			updateBalanceDetails();

			$(".maxPointsButton").click(function(){
				var maxPoints = parseFloat($(".maxPoints:eq(0)").text());
				var myPointsBalance = $("#points-balance").find("h1").text().replace(",","");
				var myPointsBalance = parseFloat(myPointsBalance);

				if(maxPoints > myPointsBalance){
					maxPoints = myPointsBalance.toLocaleString();
				}

				maxPoints = (Math.round(maxPoints * 100)/100).toFixed(2);

				$("#maxPointsInput").val(maxPoints);
				$("#maxPointsInput").attr("max", maxPoints);
				calculateSuperTotal();
			});

			$("#maxPointsInput").on("change", function(){
				calculateSuperTotal();
			});

		

			$(".pax_document").on("change", function(){
				// changes form label to match preference of that passenger
				var documentType = $(this).val();

				$(this).parent().parent().parent().find(".docTypeLabel").each(function(){
					$(this).text((documentType == "passport") ? "Passort" : "License");
				})
			});

			getOfferExpiry();

			var intervalId = window.setInterval(function(){
				getOfferExpiry();
			  
			  // check status of order every 30 seconds
			}, 30000);

			$("body").on("click", ".savePax", function(){
				var btn = $(this);
				btn.addClass("disable-while-loading");
				
				var i = btn.attr("data-pax-i");
				var pax_id = btn.attr("data-pax-id");
				var rewards_iata = btn.attr("data-rewards-iata");

				var passable = checkPaxData("#form-" + pax_id);

				if(!passable){
					btn.removeClass("disable-while-loading");
					return false;
				}

				var pax_cell = iti[i].getNumber();

				btn.parent().parent().parent().parent().parent().find(".accordion-PaxName").text($(".pax_fname" + i).val() + " " + $(".pax_lname" + i).val());


				$.ajax({
		            url: "{{ route('flight-update-offer-pax') }}",
					type: 'POST',
					async: true,
					processData: true,
			        dataType: 'json',
		            data:{
		                pax_id: pax_id,
		                offer : "{{ $data['offer'] }}",
		                id: $(".pax_id" + i).val(),
		    			title: $(".pax_title" + i).val(),
		    			first_name: $(".pax_fname" + i).val(),
		    			last_name: $(".pax_lname" + i).val(),
		    			email: $(".pax_email" + i).val(),
		    			dob: $(".pax_dob" + i).val(),
		    			ktn: $(".pax_ktn" + i).val(),
		    			rewards: $(".pax_rewards" + i).val(),
		    			rewards_iata: rewards_iata,
		    			phone: pax_cell,
		    			gender: $(".pax_gender" + i).val(),
		    			document_type: $(".pax_doc" + i).val(),
		    			passport_expiration: $(".pax_passport_exp" + i).val(),
		    			passport_number: $(".pax_passport_number" + i).val(),
		    			passport_country: $(".pax_passport_country" + i).val()
		            },
		            success: function (data) {
		            	btn.removeClass("disable-while-loading");
		            	if(data == 200){
		            		btn.parent().parent().parent().parent().parent().find(".accordion-button").addClass("accordion-success");
		            		btn.addClass("btn-success");
		            		btn.html("Saved Successfully");
		            	}
		            	else {
		            		btn.addClass("btn-warning");
		            		btn.html(data.message)
		            	}
		            },
		            error: function(e){
		            	btn.removeClass("disable-while-loading");
		            	btn.html("Error Saving. Try Again or Contact admin@venti.co")
		            }
		       });
			});

			$("body").on("click",".cabin-seat", function(e){
				// made a seat selection
				var pax = $(this).attr("data-passenger");
				var assignableIDs = JSON.parse($(this).attr("data-seat-id"));

				var seatID = false;
				for(i = 0; i < assignableIDs.length; i++){

					if(assignableIDs[i].passenger == pax){
						seatID = assignableIDs[i].serviceID;
					}
				}
				

				var segment = $(this).attr("data-segment-id");
				var price = $(this).attr("data-upgrade-total");
				
				var seat = $(this).attr("data-seat-number");
				var label = $(this).attr("data-segment-label");
				var paxName = $(this).attr("data-pax-name");
				var cabin = $(this).attr("data-cabin");
				var seatPaxClass = segment + "-" + pax;
				var cabinInView = $(this).closest("." + cabin);
				var cabinInViewId = cabinInView.attr("data-cabin");
				var clickedChair = $(this);




				$(".seat-segment-" + segment + ".seat-segment-" + segment + "-" + seat + ".pax-" + pax).each(function(){
					// we are looping through each chair that belongs to the same segment
					var currentChair = $(this);

					// we need to remove "taken" from the other cabins
					$(".seat-segment-" + segment + ".taken-" + pax).each(function(){
						var takenChair = $(this);
						var paxOGName = takenChair.attr("paxName");
						takenChair.removeClass("taken");
						takenChair.removeClass("taken-" + pax);
						takenChair.find(".p-name").text(paxOGName);
					});


					if(currentChair.hasClass("selected")){
						if(cabinInViewId == cabin){
							// the person being seated matches the chair slot
							// we are unassigning this seat
							currentChair.removeClass("selected");
						}
					} else {
						// we are making a new assignment

						if(cabinInViewId == cabin){

							$("." + cabinInViewId + " .seat-segment-" + segment + ".selected").each(function(){
								// removing all prior selections within this cabin
								$(this).removeClass("selected");
							});
							// we are removing this from the selector menu at the bottom


							$("." + seatPaxClass).remove();


							// we are adding the selected seat to the menu
							$("#selected-seats").append('<div class="' + seatPaxClass + ' mb-3 mr-3 selectedSeat" data-passenger="'+pax+'" data-segment="'+segment+'" data-seat-id="'+seatID+'"><div class="btn btn-primary"><h4 class="seat-number text-light">' + seat + '</h4><span class="d-block">' + paxName + '</span><span class="text-light d-block">' + label + '</span><div class="seat-price btn btn-white btn-sm">$' + price +'</div></div></div>');

							// updating the UI to denote the seat is taken

							currentChair.addClass("selected");
						}
						else {
							
							$(".seat-segment-" + segment + ".seat-segment-" + segment + "-" + seat)
						}

						

						// do not add the taken class if 

						

							// loop through every chair



						// we need to update the other seatmaps to indicate where previous selections have been made if part of the same segment
					}
				});

				

				$("#seat-map-picker seatsection").not("." + cabin).find(".seat-segment-" + segment + ".seat-segment-" + segment + "-" + seat + ":not(.pax-" + pax + ")").each(function(){

						$(this).addClass("taken");
						$(this).addClass("taken-" + pax);
						$(this).find(".p-name").text(paxName)
					
				});


				

				var upgradeTotal = 0;

				$(".cabin-seat.selected").each(function(){
					upgradeTotal += parseFloat( $(this).attr("data-upgrade-total"));
				});

				$("#seatUpgradeTotal").text(upgradeTotal);
			});

		    function initCodeInput(){
		        const inputElements = [...document.querySelectorAll('input.code-input')]

		        inputElements.forEach((ele,index)=>{
		          ele.addEventListener('keydown',(e)=>{
		            // if the keycode is backspace & the current field is empty
		            // focus the input before the current. Then the event happens
		            // which will clear the "before" input box.
		            if(e.keyCode === 8 && e.target.value==='') inputElements[Math.max(0,index-1)].focus()
		          })
		          ele.addEventListener('input',(e)=>{
		            // take the first character of the input
		            const [first,...rest] = e.target.value
		            e.target.value = first ?? '' // first will be undefined when backspace was entered, so set the input to ""
		            const lastInputBox = index===inputElements.length-1
		            const didInsertContent = first!==undefined
		            if(didInsertContent && !lastInputBox) {
		              // continue to input the rest of the string
		              inputElements[index+1].focus()
		              inputElements[index+1].value = rest.join('')
		              inputElements[index+1].dispatchEvent(new Event('input'))
		            }
		          })
		        });
		    }

		    function checkPaxData(target){
		    	var passable = true;

		    	$(target).each(function(){

		    		var card = $(this);
		    		card.find(".required").each(function(){
		    			// look through the required fields of each passenger card
		    			var field = $(this);

		    			if(field.val() == "" || field.val() == undefined){
		    				field.addClass("error");
		    				passable = false;
		    				card.addClass("error");
		    				Swal.fire({
					    		icon: "warning",
								title: 'All passenger fields are required',
							});
		    			}

		    			

						// check the age of passenger
						if(field.hasClass("pax-age")){
							var paxType = field.attr("data-pax-type");
							var dob = field.val();
							var age = moment().diff(moment(dob), 'years');



							if(paxType == "adult" && age < 12){
								// the passenger is an adult but is not of age;
								passable = false;
								field.addClass("error");
							}
							if(paxType == "child" && age > 12){
								// the passenger is too old to be a child
								passable = false;
								field.addClass("error");
							}
							if(!passable){
								return swal.fire({
									icon: 'warning',
									title: 'Invalid passenger information',
									text: 'Please check to make sure the date of birth for each passenger is correct. Anyone above the age of 12 is considered an Adult.',
									confirmButtonText: 'Ok'
								});
							}
						}

						@if($pax_docs_required)

						if(field.hasClass("passport-expiry")){
							// use moment 
							var expiry = field.val();
							var departureDate = "{{ $origin['departing_at'] }}"
							var dateDif =  moment(expiry).diff(moment(departureDate), 'days');

							if(dateDif < 0){
								// the passport will expire before departure
								field.addClass("error");
								passable = false;
								return swal.fire({
									icon: 'warning',
									title: 'Invalid passenger documentation',
									text: 'Please check to make sure passenger documentation will not expire before departure.',
									confirmButtonText: 'Ok'
								});
							}
						}
						@endif
		    		});
		    	});

		    	return passable;
		    }

		    function checkPaxComplete(){
		    	var paxNum = {{ sizeof($passengers) }};
		    	var successCount = 0;

		    	$(".accordion-success").each(function(){
		    		successCount++;
		    	});



		    	if(successCount == paxNum){
		    		return true;
		    	}

		    	return false;
		    }

		    function getAddOnTotals(){
		    	var total = 0;
		    	$(".is-add-on").each(function(){
		    		var price = parseFloat($(this).attr('data-total'));
		    		total += price;
		    	});

		    	return total;
		    }

			function calculateTotals(cfar){
				var markUp = 0;
				var total_amount = parseFloat({{ $total_amount }});
				var protectionFee = total_amount * 0.3; // 
				var subtotal = total_amount + protectionFee;
				var platformFeeBase = total_amount * 0;
				var creditCardFeeRate = 0;
				var creditCardFeeFlat = 0;
				var creditCardFee = ((subtotal + platformFeeBase) * creditCardFeeRate) + creditCardFeeFlat;
				var platformFee = Math.round(creditCardFee + platformFeeBase,2);
				var platformFee = $("#platformFee").val();
				var extendedDiscount = (Math.round((total_amount * 0.015)* 100)/100).toLocaleString(undefined, {minimumFractionDigits: 2});

				var addOns = getAddOnTotals();

				// we add addOns to the total price

				

				// now return the rest of the calcs;

				if(!cfar){
					// they've opted out of CFAR, so we have to re-calculate the platform fee.
					$("#protectionOn").html("0");
					$("#protectionDiscountOn").html("0");
					total_amount += addOns;

					creditCardFee = 0;

					var newTotal = total_amount;

					var newDiscount = getDiscountPercent("airfare", newTotal);

					console.log(newTotal);
					console.log(newDiscount);
					
					var lowestPossiblePrice = newDiscount * newTotal;

					var maxPoints = newTotal - lowestPossiblePrice;
					
					$(".platformFee").each(function(){
						$(this).text(extendedDiscount);
					});

					$("#onDiscount").text((Math.round((newTotal - extendedDiscount) * 100)/100).toLocaleString(undefined, {minimumFractionDigits: 2}));

					$("#newTotal").text(newTotal.toLocaleString(undefined, {minimumFractionDigits: 2}));

					$("#lowestPossiblePrice").text(lowestPossiblePrice.toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2}));

					var total_points = Math.round((extendedDiscount) * 100,0)/100;

					$(".maxPoints").each(function(){
						$(this).text(maxPoints.toLocaleString(undefined, {
							maximumFractionDigits: 2,
							minimumFractionDigits: 2
						}));
					});
					

				} else {
					var newTotal = $("#total-cost").val();
					
					var newDiscount = getDiscountPercent("airfare", newTotal);
					var maxPoints = $("#maxPointsAllowedOG").val();
					var lowestPossiblePrice = (newDiscount * newTotal) + addOns;


					$("#newTotal").text(newTotal.toLocaleString(undefined, {minimumFractionDigits: 2}));

					$("#onDiscount").text($("#discounted-price").val());

					$("#protectionOn").html($("#protectionFee").val());
					$("#protectionDiscountOn").html($("#protectionDiscount").val());


					$(".pointsFinal").each(function(){
						$(this).text($("#total-discount").val());
					});

					var newLowest = parseFloat($("#lowest_price").val());

					$("#lowestPossiblePrice").text(newLowest.toLocaleString(undefined, {minimumFractionDigits: 2}));

					$(".maxPoints").each(function(){
						$(this).text(maxPoints.toLocaleString(undefined, {
							maximumFractionDigits: 2,
							minimumFractionDigits: 2
						}));
					})

					
				}
			}

			function getSeatMaps(){
				$.ajax({
		            url: "{{ route('flight-seatmap') }}",
					type: 'POST',
					async: true,
					processData: true,
			        dataType: 'json',
		            data:{
		                offer : "{{ $data['offer'] }}"
		            },
		            success: function (data) {
		            	$("#seat-map-picker").html("");

		            	if(data != 500){

		            		$("#seatSelectionButton").show();

		            		$.each(data, function(index, item) {

					            $("#seat-map-picker").append(item.map);
					        });

		            		var wizard = $("#seat-map-picker").steps({
							    headerTag: "h4",
							    bodyTag: "seatsection",
							    transitionEffect: "slideLeft",
							    autoFocus: false,
							    onFinished: function (event, currentIndex)
						        {
						            $("#checkout-wizard").steps("next");

						        }
							});


							$("#seat-map-picker").append('<div class="d-flex" style="justify-content:space-around; flex-direction: row; width: 100%; padding: 0 5em; align-items: center;"> <div id="selected-seats" class="d-flex"></div></div>');

		            		$("#seat-map-picker .actions ul li:eq(0) a").html("<img src='/assets/img/icons/seat-light.png' style='width:15px; margin-top:-2px'> Previous Seat");
							$("#seat-map-picker .actions ul li:eq(1) a").html("<img src='/assets/img/icons/seat-light.png' style='width:15px; margin-top:-2px;'> Next Seat");
							$("#seat-map-picker .actions ul li:eq(2) a").html("Save Seat Selection");

							$("#seat-map-picker .actions").show();


							$("#seat-map-picker .body:first").addClass("current");
							$("#seat-map-picker .body:first").css("display", "block");

							if(data.length == 0){
								$("#seat-map-picker").html("<h5 style='width:380px; margin: 0 auto; margin-top:100px; text-align:center'>At this time, {{ $data['owner']['name'] }} is not allowing seat selection for this flight<br><br><span style='font-size:15px;'>You can skip seat selection by pressing the Next button below, or rebuild this search with a different seat class.</span></h5>");
							}

		            		return true;
		            	} else {
		            		$("#seat-map-picker").html("<h3>{{ $data['owner']['name'] }} is not allowing seat selection for this flight at this time.</h3>");
		            	}
		            	return false;
		            },
		            error: function(e){
		            	$("#seat-map-picker").html("<h3>An error occured when searching seats for this {{ $data['owner']['name'] }} flight. Please refresh the page or contact admin@venti.co.</h3>");
		            }
		       });
		       
			}

			function updateBalanceDetails(){
				@if(Session::get('user') !== null)
					$.ajax({
			            url: "{{ route('home-customer-details') }}",
									type: 'POST',
									async: true,
									processData: true,
				        dataType: 'json',
			            data:{
			                wallet : "{{ $wallet }}",
			                user: "{{ $user->uid }}"
			            },
			            success: function (data) {
			            	$("#spending-power-balance").html(data.spendingPowerHTML);
		            		$("#cash-balance").html(data.cashHTML);
		            		$("#points-balance").html(data.pointsHTML);
			            }
			       });
				@endif
			}

			function checkMaxPoints(){
				currentInput = parseFloat($("#maxPointsInput").val());
				var maxPoints = parseFloat($(".pointsFinal:eq(1)").text());

				if($("#total-discount").val() == currentInput){
					$("#maxPointsInput").val(maxPoints);
				}
				
				$("#maxPointsInput").attr("max", maxPoints);
				calculateSuperTotal();
			}

			function calculateSuperTotal(){
				var baseTotal = parseFloat($("#newTotal").text().replace(',', ''));
				
				var pointsApplied = parseFloat($("#maxPointsInput").val());

				if(isNaN(pointsApplied)){
					pointsApplied = 0;
				}

				var superTotal = baseTotal - pointsApplied;

				$("#pointsApplied").text(pointsApplied.toLocaleString(undefined, {minimumFractionDigits: 2,maximumFractionDigits:2}));

				var deductedAmount = superTotal;

				$(".superTotal").each(function(){
					$(this).text(superTotal.toLocaleString(undefined, {
						minimumFractionDigits: 2,
						maximumFractionDigits: 2
					}))
				});

				if($("#paymentPrefSelected").val() != "pass"){
					// cc selected
					$("#ccFeeDisclosure").show();
					var ccFee = deductedAmount * 0.03;
					$("#ccFee").text(ccFee.toLocaleString(undefined, {minimumFractionDigits: 2,maximumFractionDigits:2}));
					deductedAmount += ccFee;
					$("#finalOffer").text(deductedAmount.toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits:2}));
				} else {
					$("#ccFeeDisclosure").hide();
				}

				$("#deductedAmount").text(deductedAmount.toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits:2}));
			}

			function getOfferExpiry(){
				$.ajax({
		            url: "{{ route('flight-offer-status') }}",
					type: 'POST',
					async: true,
					processData: true,
			        dataType: 'json',
		            data:{
		                offer : "{{ $data['offer'] }}"
		            },
		            success: function (data) {
		            	if(data == 500){
		            		// the offer has expired
		            		swal.fire({
								icon: 'error',
								title: 'Offer no longer available',
								text: "This offer expired or was rescinded by the airline. Either way, it cannot be purchased. Please restart your search.",
								confirmButtonText: 'Ok'
							});

							$("#finalizePurchase").remove();
		            	}
		            	else{

		            		var expiry = moment(data).diff(moment(), 'minutes');
		            		if(expiry <= 0){
		            			swal.fire({
									icon: 'error',
									title: 'Offer no longer available',
									text: "This offer expired or was rescinded by the airline. Either way, it cannot be purchased. Please restart your search.",
									confirmButtonText: 'Ok'
								});

								$("#finalizePurchase").remove();
		            		}

		            		$("#offer-expiry").text("(Expires in " + expiry + " minutes)");
		            	}
		            },
		            error: function(e){
		            	
		            }
		       });
			}

			function checkRequiredFields(target){
				var passable = true;
				$(target + " .required").each(function(){
					$(this).removeClass("invalid-feedback");

					if(!checkIfEmpty($(this).val())){
						$(this).addClass("invalid-feedback");
						passable = false;
					}

					// check if passport expiry is before the departure/return date of the trip

				});

				return false; //passable;
			}

			$(".retry").click(function(){
				getCheckout()
			})

			function getCheckout(){
				// check to see if baggage was added to the order

				var addedBags = [];

				$(".addBaggage:checked").each(function(){
					addedBags.push({
						id: $(this).val()
					});
				});

				var addedSeats = [];

				$("#selected-seats .mr-3").each(function(){
					addedSeats.push({
						passenger: $(this).attr("data-passenger"),
						segment: $(this).attr("data-segment"),
						id: $(this).attr("data-seat-id"),
					});
				});

				$.ajax({
		            url: "{{ route('flight-order-preview') }}",
					type: 'POST',
					async: true,
					processData: true,
			        dataType: 'json',
		            data:{
		            	addedBags: addedBags,
		            	addedSeats: addedSeats,
		                offer : "{{ $data['offer'] }}"
		            },
		            success: function (data) {
		            	if(data != 500){
		            		$("#checkout-box").html(data);

		            		var elem = document.querySelector('.js-switch');

						    var switchery = new Switchery(elem, { size: 'small' });

							$(".switchery").click(function(){
								var cfar = elem.checked;
								checkMaxPoints();
								calculateTotals(cfar);
								calculateSuperTotal();
							});

							setTimeout(function(){
		            			var maxPoints = parseFloat($("#maxPointsAllowedOG").val());

			            		$("#maxPointsInput").attr("max", maxPoints);
			            		$("#points-input-module").addClass("d-flex")
			            		
			            		$(".maxPoints").each(function(){
			            			$(this).text(maxPoints);
			            		});

			            		calculateSuperTotal();
		            		}, 1000);
		            	}
		            }
		        });
			}

			$("body").on("click", ".paymentPref", function(){
				var paymentPref = $(this).data("pref");
				updateCheckoutOptions(paymentPref);

			});

			function updateCheckoutOptions(pref){
				var withdrawSource = "will be withdrawn from your Cash Balance";
				var selectedCard = $('input.paymentPref:checked');
				var last4 = selectedCard.data("last4") ?? false;
				var paymentId = selectedCard.data("id");

				if(pref != "pass"){
					withdrawSource = "will be charged to your card";
					if(last4){
						withdrawSource = "will be charged to your card ending in " + last4;
					}	
					$("#paymentPrefSelected").val(paymentId);
				} else {
					$("#paymentPrefSelected").val("pass");
				}

				$("#withdrawSource").text(withdrawSource);
				calculateSuperTotal();
			}
			
	</script>
@endsection