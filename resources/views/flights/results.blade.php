@extends("layouts.curator-master")

@section('css')
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<link rel="stylesheet" type="text/css" href="/assets/css/steps.css">
<style type="text/css">
	input.disabled{
        pointer-events: none;
    }
    .wizard>.content>.body{
        height: 100%;
        position: relative;
        padding: 0;
    }
    .wizard>.content{
        min-height: 120px;
        background: none;
    }
    .wizard .steps{

    }
    .accordion-button:not(.collapsed){
    	background: transparent;
    }
    .accordion-item{
		border: none;
	}
	td.vertical-align{
		vertical-align: middle;
	}
	tr.borderless td{
		border-bottom: none !important;
	}
	.table,.card-body{
		padding-bottom: 0;
		margin: 0;
	}
	ul.nolist{
		list-style: none;
	}
	.text-small{
		font-size: 12px;
	}
	.results-table .text-left{
		font-size: 13px;
		font-weight: 700;
	}
	.results-table .text-right{
		font-size: 13px;
	}
	.card{
		background: transparent;
		background-image: linear-gradient(112.1deg, rgba(242, 242, 246,0) 11.4%, rgba(255, 255, 255, 0.7) 70.2%);
		border: none;
		box-shadow:
	      0 1px 1px hsl(0deg 0% 0% / 0.015),
	      0 2px 2px hsl(0deg 0% 0% / 0.015),
	      0 4px 4px hsl(0deg 0% 0% / 0.015),
	      0 8px 8px hsl(0deg 0% 0% / 0.015),
	      0 16px 16px hsl(0deg 0% 0% / 0.015)
	    ;
	}
	td.borderless{
		border-bottom: none;
	}
	#sorter-row,#filters-menu,.flight-search-dates .form-label, .btn-text-sm, .hide{
		display: none;
	}
	@media(min-width:786px){
		.flight-card-content{
			display: flex;
			align-items: center;
			padding: 8px 0;
		}
		.boarding-pass-footer{
			padding: 0 10px;
			font-size: 11px;
			margin: 0;
			max-width: 170px;
		}
		.checked-bags{
			line-height: 0.9;
			display:block;
			max-width: 155px;
			margin:0 auto;
		}
		#sorter-row{
			margin-top: -22px;
		}
		.flex-lg-row{
			flex-direction: row !important;
		}
		.flex-lg-column{
			flex-direction: column; !important;
		}
		.mw-lg-100{
			max-width: 100px;
		}
	}


	@media(max-width:785px){

		.flex-sm-row{
			flex-direction: row !important;
		}
		.flex-sm-column{
			flex-direction: column !important;
		}
		
		.justify-content-sm-between{
			justify-content: space-between;
			padding: 10px;
		}

		#filters-menu{
			margin-top: 0 !important;
		}
		.flight-card-content{
			display: block;
			align-items: center;
		}
		.flight-card .table{
			width: 100% !important;
		}
		.flight-card .table h5{
			font-size: 1rem;
		}
		.flight-card .table img{
			max-width: 40px !important;
		}

		.table .text-small{
			font-size: 9px;
		}
		.table,.card-body{
			padding: 0;
			margin: 0;
			padding-bottom: 10px;
		}

		.duration-info{
			width: 100px;
		}
		.boarding-pass-footer{
			padding: 0 10px;
			font-size: 11px;
			margin: 0;
			max-width: inherit;
		}
		.checked-bags{
			line-height: 0.9;
			display:block;
			max-width: 155px;
			margin:0 auto;
			margin-top: 20px;
		}
		.section-wrapper.black{
			margin-top: 50px;
		}
		#sorter-row{
			margin-top: 22px;
			padding-top: 5px !important;
		}
	}

	.remove{
		display: none !important;
	}

	li.completed a,li.completed a:hover{
		background-color: rgba(25, 135, 84, 0.3) !important;
	}
	.wizard>.content>.body{
		width: 100%;
	}

	button.accordion-button{
		line-height: 2;
		font-weight: 400 !important;
		font-family: 'sofia';
		border-top-left-radius: 0.375rem;
		border-top-right-radius: 0.375rem;
		padding: 10px 10px 5px 10px;
	}
	.accordion-item{
		border-radius: 0.375rem;
		border: none;
	}

	.accordion-item,.accordion-button:not(.collapsed){
		background-color: white;
	}
	.accordion-item{
		border: none;
	}

	.wizard>.steps a,.wizard>.steps a:hover{
		padding: 8px 7px 7px 20px;
	}
	.stepThreeContent .btn-success{
		pointer-events: none;
		opacity: 0.6;
	}
	td.vertical-align .text-small{
		line-height: 1;
	}
	.select2-container .select2-selection--single,.select2-container--default .select2-selection--single .select2-selection__clear,.select2-container--default .select2-selection--single .select2-selection__arrow{
			height: 38px;
			font-size: 12px;
		}
		.select2-selection__rendered{
			padding-top: 5px;
		}
		.select2-container--default .select2-selection--single{
			border: 1px solid #ced4da;
		}
		.select2-container--open{
			z-index: 9999;
		}
		.datepicker{
			max-width: 170px;
			font-size:12px;
			height:38px;
		}
		.btn-primary{
			border-radius: 4px;
		}
		.value-button{
			display: inline-block;
		    border: 1px solid #ddd;
		    margin: 0px;
		    width: 40px;
		    height: 40px;
		    text-align: center;
		    vertical-align: middle;
		    padding: 7px 0;
		    background: #eee;
		    -webkit-touch-callout: none;
		    -webkit-user-select: none;
		    -khtml-user-select: none;
		    -moz-user-select: none;
		    -ms-user-select: none;
		    user-select: none;
		}
		.decrease{
			margin-right: -4px;
    		border-radius: 8px 0 0 8px;
		}
		.increase{
			margin-left: 0;
    		border-radius: 0 8px 8px 0;
		}
		.steppable{
			text-align: center;
		    border: none;
		    border-top: 1px solid #ddd;
		    border-bottom: 1px solid #ddd;
		    margin: 0px;
		    width: 40px;
		    height: 40px;
		}
		.applyBtn{
			width: 100%;
			padding: 10px 5px !important;
		}

		@media(max-width:785px){
			.section-wrapper.black{
				margin-top: 0 !important;
			}
			.col-xs-6{
				width: 48% !important;
				margin: 2px !important;
				padding: 10px;
				border: 1px solid #ced4da;
				border-radius: 7px;
			}
			.flight-search-details-row{
				padding: 8px;
				margin-right: 0;
			}
			.swap-destinations{
				margin: 10px auto;
				width: 100% !important;
				max-width: 200px !important;
			}
			.swap-destinations .btn{
				width: 100%;
				max-width: inherit;
			}

			.flight-search-dates .form-label{
				display: block;
				font-size: 12px;
			}

			.flight-search-dates{
				width: 100%;
				margin: 10px 0;
			}
			.datepicker{
				max-width: 700px !important;
			}
			.flight-search-submit{
				margin: 0 auto;
				width: 100% !important;
				max-width: 200px !important;
			}
			.flight-search-submit .btn{
				width: 100%;
				max-width: 175px;
			}
			.btn-text-sm{
				display: inline-block;
			}
				.modal-content{
					min-height:700px;
				}
				.daterangepicker{
					border: none !important;
					left: 50%;
	    			width: 83% !important;
	    			max-width: 350px !important;
				}
				.drp-calendar{
					width: 100% important;
					max-width: 350px !important;
				}

			
			.daterangepicker:before{
				display: none !important;
			}
		}

		@media(min-width:785px){
			.daterangepicker:before{
				display: none !important;
			}
			.modal-content{
				min-height:500px;
			}
			.modal-dialog{
				max-width: 530px !important;
			}
			.daterangepicker{
				border: none !important;

				max-width: 500px !important;
				
			}
			.daterangepicker.single{
				min-width:  500px !important;
			}
			.drp-calendar{
				width: 100% important;
				max-width: 500px !important;
				
			}
			.single .drp-calendar{
				min-width:  500px !important;
			}
		}
		.no-bg{
			background-color: transparent;
			border: none;
		}
		.form-small {
		    padding: 0;
		    font-size: 12px;
		}

		#example-basic .actions ul li:nth-child(2){
			display: none !important;
		}
</style>

@endsection

@section('content')

<div class="gradient-wrap" style="background: transparent; width:100%;">
    <!--  HERO -->
    <div class="section-wrapper black position-relative">
    	<div class="row mb-3">
    		<div class="col-md-12">
    			<div class="accordion " id="accordionExample" style="width:100%; padding:0;">
					  <div class="accordion-item bg-shadow" style="border-radius: 0.375rem">
					    <h2 class="accordion-header" id="headingOne" style="border-radius: 10px;">
					      <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne" style="border-top-left-radius:0.375rem; border-top-right-radius:0.375rem;">My Search</button>
					    </h2>
				        <div id="collapseOne" class="accordion-collapse collapse show" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
				            <div class="accordion-body">
				    			@include('flights.components.search-form')
				            </div>
				        </div>
				   </div>
				</div>
			</div>
		</div>
    	<div class="row">
    		<div class="col-md-4">
    			<div class="mb-2" id="sorter-row" style="padding-top:23px;">
					<div style="flex-direction: row; display: flex; align-items: center; justify-content: space-between;">
						<div style="width: 100%; max-width: 200px">
							<select class="form-control bg-shadow" id="sorter" style="border:none;">
								<option value="lowest">Sort by Lowest Price</option>
								<option value="fastest">Sort by Fastest Travel</option>
							</select>
						</div>
						<div>
							<span style="color: grey; font-size: 11px; padding: 5px 20px; text-align: right;">All Times Local</span>
						</div>
					</div>
				</div>
    			<div class="row">
    				<div class="col-md-12">
    					
    				</div>
    			</div>
    			<div class="row mt-4" id="filters-menu">
    				<div class="col-md-12">
    					<div class="accordion mb-3" id="accordionExample2" style="width:100%; padding:0;">
						  <div class="accordion-item bg-shadow" style="border-radius: 0.375rem">
						    <h2 class="accordion-header" id="headingTwo" style="border-radius: 10px;">
						      <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo" style="border-top-left-radius:0.375rem; border-top-right-radius:0.375rem;">Filter Results</button>
						    </h2>
						        <div id="collapseTwo" class="accordion-collapse collapse show" aria-labelledby="headingOne" data-bs-parent="#accordionExample2">
						            <div class="accordion-body">
						            	<div id="flight-filters"></div>
						            </div>
						        </div>
						   </div>
						</div>
						<p class="text-small">Currently in Beta. Better filters will be added in due time.</p>
    				</div>
    			</div>
    		</div>
    		<div class="col-md-8">
    			<div id="example-basic">
				    <h3 class="stepOne">{{ $iata_home }} <i class="fa fa-arrow-right"></i> {{ $iata_destination }}</h3>
				    <section class="stepOneContent">
				    	<div id="flightsTo" style="max-height:900px; overflow-y:scroll"><div id="flights-loader" style="width: 300px; margin: 0 auto; text-align:center;"><img src="/assets/img/airplane.gif" style="width:100%; min-width:200px;"><h5>Searching for flights...</h5></div></div>
				    </section>
				    @if($trip_type == "round-trip")
					    <h3 class="stepTwo">{{ $iata_destination }} <i class="fa fa-arrow-right"></i> {{ $iata_home }}</h3>
					    <section class="stepTwoContent">
					    	<div id="flightsReturn" style="max-height:900px; overflow-y:scroll"><div id="flights-loader" style="width: 300px; margin: 0 auto; text-align:center;"><img src="/assets/img/airplane.gif" style="width:100%; min-width:200px;"><h5>Searching for flights...</h5></div></div>
					    </section>
				    @endif
				    <h3 class="stepThree">Review</h3>
				    <section class="stepThreeContent" id="reviewOffers">

				    </section>
				</div>
    		</div>
    	</div>
    </div>
</div>

@include('flights.components.amenities')
@include('flights.components.price-match-explainer', ["pm" => "flights"])
@include('flights.components.datepicker') 
@endsection

@section('js')
	<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
	<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
	@include('flights.components.flights-js')
	<script type="text/javascript" src="/assets/js/jquery.steps.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
	<script type="text/javascript">
		$.ajaxSetup({
	        headers: {
	            'X-CSRF-TOKEN': '{{ csrf_token() }}'
	        }
	    });


		$("#example-basic").steps({
	        headerTag: "h3",
	        bodyTag: "section",
	        autoFocus: true,
	        onInit:function(){
	        	var url = "{{ str_replace('&amp;','&',$path) }}";
				url = url.replace(/&amp;/g, '&');
	        	getFlights("To", url);
	        	$('a[href$="finish"]').text('Continue');
	        },
	        onStepChanging: function(e, currentIndex, newIndex) {
	            if(newIndex < currentIndex){
	                return true;
	            }
	            
	            switch (currentIndex){
	                
	            }

	            return true;
	        },
	        onStepChanged: function(event, currentIndex, priorIndex){

	        	if(currentIndex == 1){
	        		// don't refresh on every page turn

	        		var returnFlights = $(".returnFlight").length;

	        		if(returnFlights == 0){
	        			getReturnFlights();
	        			
	        		}

	        	}
	        	if(currentIndex == 2){

	        	}
	        },
	        onFinished: function(){
	        	var originOffer = $(".originFlight.btn-success").attr("data-offer");
				var returnOffer = $(".returnFlight.btn-success").attr("data-offer");
				var search_id = $(".returnFlight.btn-success").attr("data-search-id");
				var seat_class = $(".originFlight.btn-success").attr("data-seat-class");
				var amount = $(".returnFlight.btn-success").attr("data-amount");

				if(returnOffer != undefined){
					return navigate("/flights/search/offer/" + search_id + "/" + originOffer + "/" + returnOffer + "/{{ $iata_destination }}/" + seat_class + "/" + amount, true);
				} else {
					return navigate("/flights/search/offer/" + search_id + "/" + originOffer + "/direct/{{ $iata_destination }}/" + seat_class + "/0", true);
				}
	        }
	    });

	    $("body").on('click', '.flight-amenity', function(){
	    	$("#amenityExplainerModal").modal("toggle");
	    });

	    $("body").on('click', '.priceMatchPop', function(){
	    	$("#MatchexplainerModal").modal("toggle");
	    });

	    

	    $('body #flight-filters').on("click",'.filter-carriers',function() {
			var carriers = [];
			$(".filter-carriers").each(function(){
				if($(this).is(":checked")){
					carriers.push($(this).val());
				}
			});

			$(".flight-slice").each(function(){
				var flightCard  = $(this).closest(".flight-card");
				if(carriers.length == 0){
					$(this).parent().parent().parent().parent().parent().removeClass("remove");
				} else {

					// loop through each row of enclosed table

					var tableCarriers = [];
					var flightSlices = $(this).parent().parent().parent().parent().find(".flight-slice");

					$(flightSlices).each(function(){
						tableCarriers.push($(this).attr("data-carrier"));
					});

					let isFound = tableCarriers.some( ai => carriers.includes(ai) );

					if(!isFound){
						$(this).parent().parent().parent().parent().parent().addClass("remove");
					}
					else{
						$(this).parent().parent().parent().parent().parent().removeClass("remove");
					}
				}
			});

			filterTripsEval();
        });

        function filterTripsEval(){
        	var trips = [];

        	$(".filter-trips").each(function(){
				if($(this).is(":checked")){
					trips.push($(this).val());
				}
			});

			$(".flight-card").each(function(){
				var trip = $(this).attr("data-total-flights");
				
				if(trips.length == 0){
					$(this).show();
				} else {
					if(!trips.includes(trip)){
						$(this).hide();
					}
				}
			});
        }

        $('body #flight-filters').on("click",'.filter-trips',function() {
			filterTripsEval();
        });

        $('body #flight-filters').on("click",'.filter-carry',function() {
        	var carryBagAllowances = [];

        	$(".filter-carry").each(function(){
				if($(this).is(":checked")){
					carryBagAllowances.push($(this).val());
				}
			});

        	$(".carry-on-bags").each(function(){
        		var bagCount = $(this).attr("data-bag-count");

        		var targetedCard = $(this).attr("data-card");
        		targetedCard = $("." + targetedCard);
				
				if(carryBagAllowances.length == 0){
					targetedCard.show();
				} else {
					if(!carryBagAllowances.includes(bagCount)){
						targetedCard.hide();
					}
				}
        	});
        });

        $('body #flight-filters').on("click",'.filter-bags',function() {

        	var checkedBagAllowances = [];

        	$(".filter-bags").each(function(){
				if($(this).is(":checked")){
					checkedBagAllowances.push($(this).val());
				}
			});

        	$(".checked-bags").each(function(){
        		var bagCount = $(this).attr("data-bag-count");

        		var targetedCard = $(this).attr("data-card");
        		targetedCard = $("." + targetedCard);
				
				if(checkedBagAllowances.length == 0){
					targetedCard.show();
				} else {
					if(!checkedBagAllowances.includes(bagCount)){
						targetedCard.hide();
					}
				}
        	});
        });



	    async function getFlights(slice, url){
			$.ajax({
                url: "/flights/search/results" + url,
                type: 'post',
                processData: true,
                async: true,
              	dataType: 'json',
                success: function (data) {

                	if(data.length == 0){
                		return swal.fire({
							title: 'Nothing found',
							text: 'We could not find any flights. This usually happens because the seat class is either unavailable or there are no flights from the airport of origin. Please try changing your trip plan by starting a new search.',
							imageUrl: '/assets/img/404.png',
							imageWidth: 210,
							imageHeight: 130,

							showCancelButton: true,
							imageAlt: 'Custom image',
		  					confirmButtonText: 'New Search',
						}).then((result) => {
						  if (result.isConfirmed) {
						  	window.location.href = "/flights"
						  }
						});
			        }


                	dismissFlightsLoad();

                	$.when(
	                    $.each(data, function(i, item) {
	                        $("#flights" + slice).append(item);
	                    })
		            ).then(function () {
			               getCarriers();
			        });  

			        
            	},
            	error: function(data){
            		$("#loaderText").html("<a class='btn btn-black' style='width:100px; margin:0 auto;' href='" + window.location.href + "'>Try Again</a>");
            		return swal.fire({
								icon: 'warning',
								title: 'Something went wrong',
								text: 'There was an issue pulling flights from the sky. Please refresh the page or start a new search.',
							confirmButtonText: 'Refresh',
							showCancelButton: true,
						}).then((result) => {
						  if (result.isConfirmed) {
						  	window.location.href = window.location.href
						  }
						});
            	}
            });
		}

		function dismissFlightsLoad(){
			$("#flights-loader").remove();
		}

		function getCarriers(){
			$("#sorter-row").show();
			$("#filters-menu").show();

			var carriers = [];

			$(".flight-slice").each(function(){
				var carrier = $(this).attr("data-carrier");
				carriers.push(carrier);
			});

			carriers = carriers.filter(function (value, index, array) { 
			  return array.indexOf(value) === index;
			});

			$("#flight-filters").html("");

			$("#flight-filters").append("<h5>By Operating Carrier</h5><ul style='list-style:none !important;' id='flight-filters-carriers'>");

			for(i = 0; i < carriers.length; i++){
				
				$("#flight-filters-carriers").append("<li><input type='checkbox' class='filter-carriers' value='" + carriers[i] +"'/> " + carriers[i] +" </li>");
				
			}

			$("#flight-filters").append("</ul>");

			getPlanes();
		}



		$("#sorter").change(function(){
			var sorter = $(this).val();
			sortResults(sorter);
		});

		function sortResults(sortType){
			if(sortType == "fastest"){
				var result = $('#flightsTo .flight-card').sort(function (a, b) {
			      var contentA =parseInt( $(a).data('total-duration'));
			      var contentB =parseInt( $(b).data('total-duration'));
			      return (contentA < contentB) ? -1 : (contentA > contentB) ? 1 : 0;
			   });
			} else {
				var result = $('#flightsTo .flight-card').sort(function (a, b) {
			      var contentA =parseInt( $(a).data('default-order'));
			      var contentB =parseInt( $(b).data('default-order'));
			      return (contentA < contentB) ? -1 : (contentA > contentB) ? 1 : 0;
			   });
			}
			$('#flightsTo').html(result);
			$("#flightsTo").animate({scrollTop: $("#flightsTo").offset().top});


			if(sortType == "fastest"){
				var result = $('#flightsFrom .flight-card').sort(function (a, b) {
			      var contentA =parseInt( $(a).data('total-duration'));
			      var contentB =parseInt( $(b).data('total-duration'));
			      return (contentA < contentB) ? -1 : (contentA > contentB) ? 1 : 0;
			   });
			} else {
				var result = $('#flightsFrom .flight-card').sort(function (a, b) {
			      var contentA =parseInt( $(a).data('default-order'));
			      var contentB =parseInt( $(b).data('default-order'));
			      return (contentA < contentB) ? -1 : (contentA > contentB) ? 1 : 0;
			   });
			}
			$('#flightsFrom').html(result);
			$("#flightsFrom").animate({scrollTop: $("#flightsFrom").offset().top});
		}

		function getPlanes(){
			// to allow filtering but number of number of stops
			$("#flight-filters").append("<h5>By Connections</h5><ul style='list-style:none !important;' id='flight-filters-trips'>");

			var stops = [];

			$(".flight-card").each(function(){
				var stop = $(this).attr("data-total-flights");
				stops.push(stop);
			});

			stops = stops.filter(function (value, index, array) { 
			  return array.indexOf(value) === index;
			});

			stops = stops.sort();

			for(i = 0; i < stops.length; i++){
				if(i == 0){
					$("#flight-filters-trips").append("<li><input type='checkbox' class='filter-trips' value='" + stops[i] +"'/> Direct</li>");
				} else {
					$("#flight-filters-trips").append("<li><input type='checkbox' class='filter-trips' value='" + stops[i] +"'/> " + (parseInt(stops[i]) - 1) + " </li>");
				}
				
			}

			$("#flight-filters").append("</ul>");

			getCarryOnFilter();
		}

		function getCarryOnFilter(){
			$("#flight-filters").append("<h5>Carry Ons</h5><ul style='list-style:none !important;' id='flight-filters-carry'>");

			for(i = 0; i <= 2; i++){
				$("#flight-filters-carry").append("<li><input type='checkbox' name='' class='filter-carry' value='" + i +"'/> " + i + " Bags</li>");
			}

			$("#flight-filters").append("</ul>");

			getBaggageFilter();

		}

		function getBaggageFilter(){
			$("#flight-filters").append("<h5>Checked Luggage</h5><ul style='list-style:none !important;' id='flight-filters-bags'>");

			for(i = 0; i <= 2; i++){
				$("#flight-filters-bags").append("<li><input type='checkbox' class='filter-bags' value='" + i +"'/> " + i + " Bags</li>");
			}

			$("#flight-filters").append("</ul>");
		}

		function reviewOrder(){
			var originOffer = $(".originFlight.btn-success").attr("data-offer");
			var returnOffer = $(".returnFlight.btn-success").attr("data-offer");
			var discount = getDiscountPercent("airfare",parseFloat($( "#" + originOffer ).attr("data-total")),null);

			var total = parseFloat($( "#" + originOffer ).attr("data-total")) * discount;

			total = (Math.round((total)* 100)/100);

			var pointsNeeded = parseFloat( $( "#" + originOffer ).attr("data-total") ) - parseFloat($( "#" + originOffer ).attr("data-total") * discount);

			

			var p = @if($trip_type == "round-trip") 2 @else 1 @endif; 

			$("#example-basic-p-" + p).html("<h5>Departing Flight</h5>");

			$("#example-basic-p-" + p).append($( "#" + originOffer ).clone());
			if(returnOffer != undefined){
				discount = getDiscountPercent("airfare",parseFloat($( "#" + returnOffer ).attr("data-total")));

				total = parseFloat($( "#" + returnOffer ).attr("data-total")) * discount;
				total = (Math.round((total)* 100)/100);

				pointsNeeded = parseFloat($( "#" + returnOffer ).attr("data-total") - parseFloat($( "#" + returnOffer ).attr("data-total")) * discount);

				$("#example-basic-p-" + p).append("<h5>Return Flight</h5>");
			}
			$("#example-basic-p-" + p).append($( "#" + returnOffer ).clone());

			pointsNeeded = (Math.round((pointsNeeded)* 100)/100);



			$("#example-basic-p-" + p).append("<h3 class='d-block text-right mb-0' style='width:100%'>Subtotal: $" + total.toLocaleString(undefined, {minimumFractionDigits: 2}) +"<span class='d-block' style='font-size:14px;'>" + pointsNeeded + " Points needed to achieve this price</span></h3><p class='d-block text-small text-right' style='width:100%;'>.</p>");
		}

		function navigate(href, newTab) {
		   var a = document.createElement('a');
		   a.href = href;
		   if (newTab) {
		      a.setAttribute('target', '_blank');
		   }
		   a.click();
		}

		$("body").on("click", ".originFlight",function(){
			$(".originFlight").each(function(){
				$(this).removeClass("btn-success");
				$(this).text("Select");
			});
			
			$(this).addClass("btn-success");
			$(this).text("Selected");
			$("#example-basic").steps("next");

			var selectedAmount = $(this).attr("data-amount");
			var selectedOffer = $(this).attr("data-offer");
			var msrpd = $(this).attr("data-msrpd");

			$("#example-basic-t-0").parent().addClass("completed");

			reviewOrder();

			updateSearchSelection("departure", selectedAmount, selectedOffer, msrpd);

			var returnFlights = $(".returnFlight").length;
			var trip_type = "{{ $trip_type }}";
			if(returnFlights > 0 && trip_type == "round-trip"){
				// when the departing flight is changed, we have to fetch new return flights
				$("#flightsReturn").html('<div id="flights-loader" style="width: 300px; margin: 0 auto; text-align:center;"><img src="/assets/img/airplane.gif" style="width:100%; min-width:200px;"><h5>Searching for flights...</h5></div>');
				getReturnFlights();
			}
		});

		function updateSearchSelection(direction, selectedAmount, selectedOffer, msrpd){
			$.ajax({
                url: "/flights/search/update/selection",
                type: 'post',
                processData: true,
              	dataType: 'json',
              	data:{
              		direction: direction,
              		selectedAmount: selectedAmount,
              		selectedOffer: selectedOffer,
              		msrpd: msrpd
              	},
                success: function (data) {

                }
            });
		}

		function getReturnFlights(){
			var search_id = $(".originFlight.btn-success").attr("data-search-id");
				var selected_id = $(".originFlight.btn-success").attr("data-offer");

				var url = "{{ str_replace('&amp;','&',$returnPath) }}";
				url = url.replace(/&amp;/g, '&');

				url += "&partial_id=" + search_id + "&selected_partial_id=" + selected_id;
				var trip_type = "{{ $trip_type }}";
				if(trip_type == "round-trip"){
					getFlights("Return", url);
			}
		}
		

		$("body").on("click", ".returnFlight",function(){
			$(".returnFlight").each(function(){
				$(this).removeClass("btn-success");
				$(this).text("Select");
			});
			$("#example-basic").steps("next");
			$(this).addClass("btn-success");
			$(this).text("Selected");

			var selectedAmount = $(this).attr("data-amount");
			var selectedOffer = $(this).attr("data-offer");
			var msrpd = $(this).attr("data-msrpd");

			updateSearchSelection("return", selectedAmount, selectedOffer, msrpd);


			$("#example-basic-t-1").parent().addClass("completed");

			reviewOrder();
		});

		setTimeout(function(){
			setExpiry();
		}, 600000);

		function setExpiry(){
			// after 10 minutes, all deals are marked as expired.

			swal.fire({
					icon: 'warning',
					title: 'Offers expired',
					text: 'Airline prices move fast! To ensure you see the latest prices and availability, please refresh the page',
				confirmButtonText: 'Refresh',
				showCancelButton: false,
			}).then((result) => {
			  if (result.isConfirmed) {
			  	window.location.href = window.location.href
			  }
			});

			$(".detailsButton").each(function(){
				$(this).removeClass("btn-primary");
				$(this).addClass("btn-warning");
				$(this).text("Deal Expired");
				$(this).attr("href","#");
				$(this).attr("target","");
				$(this).attr("disabled","disabled");
			});
		}
	</script>
@endsection