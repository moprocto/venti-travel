<h6>To-Do List</h6>
<p class="text-small">Complete the required items to receive 120 Points towards your next flight or hotel booking.</p>
<ul class="mb-3 full-width" style="list-style:none; margin:0; padding: 0;">
  <li class="mb-3" style="border-bottom: 1px solid whitesmoke;">
    <div class="d-flex justify-content-between align-items-center">
      <span class="circle @if(!$completedProSteps['complete']) circle-empty @endif circle-lg"></span>
      <span>Link Accounts</span>
    </div>
    <ul class="mt-3 mb-3 full-width" style="list-style:none;">
      <li><p class="text-small text-bold">Required</p></li>
      <li class="mb-1">
        <div class="d-flex justify-content-between align-items-center">
          <span class="circle @if(!$completedProSteps['funding']) circle-empty @endif circle-md"></span>
          <span class="text-small">Savings Account</span>
        </div>
      </li>
      <li class="mb-3">
        <div class="d-flex justify-content-between align-items-center">
          <span class="circle @if(!$completedProSteps['transactions']) circle-empty @endif circle-md"></span>
          <span class="text-small">Allow Transaction Data</span>
        </div>
      </li>
      <li><p class="text-small text-bold">Optional</p></li>
      <li class="mb-1">
        <div class="d-flex justify-content-between align-items-center">
          <span class="circle @if(!$completedProSteps['checking']) circle-empty @endif circle-md"></span>
          <span class="text-small">Link Checking Account</span>
        </div>
      </li>
      <li class="mb-1">
        <div class="d-flex justify-content-between align-items-center">
          <span class="circle circle-empty circle-md"></span>
          <span class="text-small">Rewards Card</span>
        </div>
      </li>
      <li class="mb-1">
        <div class="d-flex justify-content-between align-items-center">
          <span class="circle circle-empty circle-md"></span>
          <span class="text-small">Retirement/Investment Account</span>
        </div>
      </li>
      <li class="mb-1">
        <div class="d-flex justify-content-between align-items-center">
          <span class="circle circle-empty circle-md"></span>
          <span class="text-small">Loan Account</span>
        </div>
      </li>
    </ul>
  </li>
  <li><p class="text-small">Optional</p></li>
  <li class="mb-3">
    <div class="d-flex justify-content-between align-items-center">
      <span class="circle @if(!$completedProSteps['card']) circle-empty @endif circle-lg"></span>
      <span>Add a Card to Wallet</span>
    </div>
  </li>
</ul>