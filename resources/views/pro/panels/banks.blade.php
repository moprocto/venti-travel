@if(sizeof($fundingSources) == 0)
<div class="card bg-white small-shadow mb-3">
	<div class="card-body">
		<p>To begin earning rewards, you'll need to link a bank account belonging to one of the financial institutions on our <a href="/about/pro" target="_blank">favorites list</a>. </p>
	</div>
</div>
<div class="row">
	<div class="col-lg-12 col-md-12">
		<div style="display:flex; flex-direction: column;">
			<div class="col-md-12 col-lg-12 col-xl-6 card bg-white small-shadow mb-3">
				<div class="card-body text-center" style="display: flex; flex-direction:row">
					<div style="display: flex; flex-direction: row; justify-content:center; align-items: center;">
						<img src="/assets/img/icons/bank.png" style="height: fit-content; max-height: 55px;  width: 100%; max-width:55px;">
						<span style="padding: 0 5px; text-align: left; font-size: 13px;">Link a savings account with a qualified credit union to earn Points.</span>
					</div>
					<a class="btn btn-success btn-small btn-sm btn-square" href="/pro/link/bank" style="height:45px; min-width:45px; align-content:center;"><i class="fa fa-plus" style="font-size:2em"></i></a>
				</div>
			</div>
		</div>
	</div>
</div>
@endif
<div class="row mt-4 sm-mt-0"> 
	<div id="linkedBanks" style="display:contents;"></div>
</div>