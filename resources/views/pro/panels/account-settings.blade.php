<h1>{{ $bank["institution"] }}</h1>
<div class="card mb-4">
	<div class="card-body">
		<div class="row">
			<h5>Connected Subaccounts</h5>
			@foreach($accounts as $account)
				<div class="col-md-12 mb-4">
					<div class="d-flex" style="flex-direction: row;  justify-content: space-between; align-items: center;">
						<div>{{ $account->official_name }}</div>
						<button type="button" class="btn btn-white border-danger text-danger deleteAccount" data-vid="{{ $account->account_id  }}">Remove</button>
					</div>
				</div>
			@endforeach
		</div>
	</div>
</div>
<div class="card mb-4">
	<div class="card-body">
		<h5>Delete Financial Institution</h5>
		<p class="text-small">This will completely unlink all subaccounts and transaction data from Venti. Doing this will pause rewards on your account.</p>
		<button type="button" class="btn btn-black btn-wide deleteBank" data-vid="{{ $bank['vid'] }}" style="max-width:300px;">Delete Bank Connection</button>
	</div>
</div>