<div class="container">
	<div class="row mb-3">
		
	</div>
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12">
			<div class="row">
				<div class="col-xl-4 col-lg- col-md-6 col-sm-6 col-xs-6 mb-4">
					<div class="card bg-white p-3 text-center" style="background-image: linear-gradient(112.1deg, rgb(253, 118, 78) 11.4%, rgb(254, 174, 27) 70.2%);">
						<button href=";javascript" type="button" data-bs-toggle="modal" data-bs-target="#pointsExplainerModal" style="background: none; border: none;">
							<span id='points-balance'><i style="font-size: 200%;" class="fa fa-sync fa-spin text-large text-light"></i></span>
						</button>
					</div>
				</div>
				@if(sizeof($fundingSources) == 0)
					<div class="col-md-4 col-sm-12 mb-4">
						<style>
							.ring-container {
							    position: absolute;
							    top: -12px;
							    left: 0;
							    z-index: 4;
							}
							.ringring {
							    border: 3px solid #62bd19;
							    -webkit-border-radius: 30px;
							    height: 20px;
							    width: 20px;
							    position: absolute;
							    left: 18px;
							    top: 18px;
							    -webkit-animation: pulsate 2s ease-out;
							    -webkit-animation-iteration-count: infinite; 
							    opacity: 0.0
							}
							@-webkit-keyframes pulsate {
							    0% {-webkit-transform: scale(0.1, 0.1); opacity: 0.0;}
							    50% {opacity: 0.7;}
							    100% {-webkit-transform: scale(1.2, 1.2); opacity: 0.0;}
							}
						</style>

						<div class="ring-container">
						    <div class="ringring"></div>
						    <div class="circle"></div>
						</div>
						
						<div class="card bg-white p-3 text-center" style="cursor:pointer;">

							<a href="{{ route('link-new-bank') }}"><div class="d-block"><img src="/assets/img/icons/bank.png" style="width: 100%; max-width:47px;"></div><span style="font-weight: 500;">Link Bank Account</span></a>
						</div>

					</div>
				@endif
				<div class="col-xl-3 col-lg-12 col-md-4 col-sm-12 mb-3 mysteryButton disabled">
					<div class="card bg-white p-3 text-center">
						<a href="{{ route('mystery-box-closed') }}"><div class="d-block"><img src="/assets/img/icons/mystery-box-closed.png" style="width: 100%; max-width:47px;"></div><span style="font-weight: 500;">Mystery Box</span>
						</a>
					</div>
				</div>
				<div class="col-xl-3 col-lg-12 col-md-4 col-sm-12 mb-3 depositButton">
					<div class="card bg-white p-3 text-center">
						<a href="#" data-bs-toggle="modal" data-bs-target="#referralModal"><div class="d-block"><img src="/assets/img/icons/refer.png" style="width: 100%; max-width:47px;"></div><span style="font-weight: 500;">Share</span>
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
