<div id="balances-{{ $account_id }}">
	<div class="row mb-4">
		<div class="col-md-8">
			<h1>{{ $bank["institution"] }}</h1>
			<h5 class="mt-2">{{ $official_name }}</h5>
		</div>
		<div class="col-md-2">
			<h1 class="text-center" style="background-color:rgba(255,255,255,0.8); border-radius:10px; padding:10px">${{ $account["balances"]->current }}<span class="d-block" style="font-size:12px;">Current Balance</span></h1>
		</div>
		<div class="col-md-2">
			<h1 class="text-center" style="background-color:rgba(255,255,255,0.8); border-radius:10px; padding:10px">${{ $account["balances"]->available }}<span class="d-block" style="font-size:12px;">Available Balance</span></h1>
		</div>
	</div>
	<div class="card">
		<div class="card-body">
			<table class="table" id="transactions-{{ $account_id }}">
				<thead>
					<th style="max-width:50px" width="50"></th>
					<th class="text-left">Transaction</th>
					<th class="text-right">Amount</th>
					<th class="text-right">Date</th>
				</thead>
				<tbody>
					@foreach($account["transactions"] as $transaction)
						<tr>
							<td><img src="{{ $transaction->logo_url ?? '/assets/img/icons/bank.png' }}" style="width: 40px;"></td>
							<td class="text-left align-middle">{{ $transaction->name }}</td>
							<td class="text-right align-middle">{{ $transaction->amount }}</td>
							<td class="text-right align-middle">{{ $transaction->date }}</td>
						</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>
