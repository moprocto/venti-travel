@extends('layouts.curator-master')

@section('css')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.13.6/css/dataTables.bootstrap5.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/2.4.1/css/buttons.bootstrap5.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.5.0/css/responsive.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/rowreorder/1.4.1/css/rowReorder.dataTables.min.css"/>
    <style>
        .btn.btn-wide{
            width:100%;
        }
    </style>
@endsection

@php $i = 0; @endphp

@section("content")
    <div id="page-content">
        <div class="gradient-wrap">
            <div class="container hero-section mb-4 mt-4">
                <div class="row">
                    
                </div>
                <div class="row">
                    <div class="col-md-9">
                        <div class="tab-content">
                            @php $i = 0; @endphp
                            @foreach($accounts as $account)
                            @php $account = (array) $account @endphp
                                <div id="pills-{{ $account['account_id'] }}" data-id="{{ $account['account_id'] }}" class="tab-pane fade in @if($i == 0) active @endif show account-transactions-tab" role="tabpanel">
                                    @include('pro.panels.account-balances', $account)
                                </div>
                                @php $i++; @endphp
                            @endforeach
                            <div id="pills-account-settings" class="tab-pane fade in" role="tabpanel">
                                @include('pro.panels.account-settings', $accounts)
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="row">
                            <div class="col-md-12 mb-4">
                                <div class="card">
                                    <div class="card-body">
                                        <ul class="nav nav-pills mb-3 full-width d-block text-left" role="tablist">
                                            @php $i = 0; @endphp
                                            @foreach($accounts as $account)
                                                <li class="nav-item text-left" role="presentation">
                                                    <a class="nav-link text-left @if($i == 0) active @endif" aria-current="page" href="#pills-{{ $account->account_id }}" data-bs-toggle="pill" data-bs-target="#pills-{{ $account->account_id }}" type="button" role="tab" aria-controls="pills-{{ $account->account_id }}" aria-selected="true">{{ $account->name }} ({{ $account->mask }})</a>
                                                </li>
                                                @php $i++; @endphp
                                            @endforeach
                                            <li class="nav-item text-left" role="presentation">
                                                <a class="nav-link text-left" aria-current="page" href="#pills-account-settings" data-bs-toggle="pill" data-bs-target="#pills-account-settings" type="button" role="tab" aria-controls="pills-account-settings" aria-selected="true">Settings</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script type="text/javascript" src="https://cdn.datatables.net/v/bs5/dt-1.13.6/datatables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/2.2.3/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/2.2.3/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/2.2.3/js/buttons.print.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/responsive/2.5.0/js/dataTables.responsive.min.js"></script>
    <script type="text/javascript">
        $(document).ready( function () {
            $.ajaxSetup({
              headers: {
                  'X-CSRF-TOKEN': "{{ csrf_token() }}"
              }
            });
            $("body").on("click", ".deleteBank", function(){

                var vid = $(this).data("vid");
                var button = $(this);
                swal({
                    icon: 'warning',
                    title: 'Remove this bank?',
                    text: 'Are you sure you want to remove this bank from your Venti profile? This cannot be reversed. All connected subaccounts will also be removed.',
                    denyButtonText: 'Cancel',
                    showDenyButton: true,
                    buttons: {
                        cancel: "Cancel",
                        delete: true,
                    },
                }).then((result) => {

                  if (result == "delete") {
                        $.ajax({
                            url: "{{ route('delete-bank') }}",
                            processData: true,
                            dataType: 'json',
                            data:{
                                vid : vid
                            },
                            type: 'post',
                            success: function (data) {
                                if(data == 200){
                                    window.location.href = "/pro";
                                } else {
                                    swal({
                                        icon: 'warning',
                                        title: 'Something went wrong',
                                        text: 'We encountered an error while removing this card. Please contact support via admin@venti.co if you continue to have issues.',
                                    });
                                }
                            }
                        });
                    }
                });
            });

            $("body").on("click", ".deleteAccount", function(){
                var vid = $(this).data("vid");
                var button = $(this);
                swal({
                    icon: 'warning',
                    title: 'Remove this account',
                    text: 'Are you sure you want to remove this account from your Venti profile? This cannot be reversed.',
                    denyButtonText: 'Cancel',
                    showDenyButton: true,
                    buttons: {
                        cancel: "Cancel",
                        delete: true,
                    },
                }).then((result) => {
                  if (result == "delete") {
                        $.ajax({
                            url: "{{ route('delete-subaccount') }}",
                            processData: true,
                            dataType: 'json',
                            data:{
                                vid : vid
                            },
                            type: 'post',
                            success: function (data) {
                                if(data == 200){
                                    swal({
                                            icon: 'success',
                                            title: 'Done!',
                                            text: "We removed this card from your account",
                                            showCancelButton: true,
                                                    closeOnConfirm: true,
                                                    closeOnCancel: true
                                        }).then((result) => {
                                            
                                        });
                                } else {
                                    swal({
                                        icon: 'warning',
                                        title: 'Something went wrong',
                                        text: 'We encountered an error while removing this card. Please contact support via admin@venti.co if you continue to have issues.',
                                    });
                                }
                            }
                        });
                    }
                });
            });

            @foreach($accounts as $account)
                var dataTable = new DataTable('#transactions-{{ $account->account_id }}', {
                    dom: 'Bfrtip',
                    buttons: [
                        'copy', 'csv', 'excel', 'pdf', 'print'
                    ],
                    responsive: true,
                    autoWidth: false,
                    rowReorder: {
                        selector: 'td:nth-child(2)'
                    }
                });
            @endforeach

            $(".dt-button").each(function(){
                $(this).addClass("btn");
                $(this).addClass("btn-primary");
            });
        });
    </script>
@endsection