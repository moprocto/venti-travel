@extends('layouts.curator-master')

@section('css')
<link rel="stylesheet" type="text/css" href="/assets/css/stripe.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/switchery/0.8.2/switchery.min.css" integrity="sha512-uyGg6dZr3cE1PxtKOCGqKGTiZybe5iSq3LsqOolABqAWlIRLo/HKyrMMD8drX+gls3twJdpYX0gDKEdtf2dpmw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.13.6/css/dataTables.bootstrap5.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/2.4.1/css/buttons.bootstrap5.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.5.0/css/responsive.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/rowreorder/1.4.1/css/rowReorder.dataTables.min.css"/>
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    
    <style type="text/css">
        .card{
            background: transparent;
            border: none;
        }
        .nav-pills{
            display: block;
        }
        .nav-item .nav-link{
            width: 100%;
            text-align: left;
            color: black;
            max-width: 200px;
        }
        .apexcharts-toolbar{
            display: none !important;
        }
        .btn-sm{
            padding: 3px;
            font-size: 10px;
            font-weight: bold;
        }
        .card {
            box-shadow: 0 1px 1px rgba(0,0,0,0.01), 
            0 2px 2px rgba(0,0,0,0.01), 
            0 4px 4px rgba(0,0,0,0.01), 
            0 8px 8px rgba(0,0,0,0.01),
            0 16px 16px rgba(0,0,0,0.01);

        }
        .disabled{
            opacity: 0.4;
            pointer-events: none;
        }
        p, span{
            font-weight: 300;
        }
        .form-label{
            font-weight: 600;
        }
        .invalid-feedback{
            margin-top: 0;
            display: block;
            color: inherit;
            border-color: #dc3545;
        }
        .full-width .nav-link{
            max-width: 100%;
        }
        
        .nav-pills .nav-link.active, .nav-pills .show>.nav-link{
            background: none;
            background-image: linear-gradient(112.1deg, rgba(32, 32, 46,0.9) 21.4%, rgba(10, 10, 10, 1) 70.2%);
        }
        .mb-xs-3{
            margin-bottom: 1rem!important;
        }
        div.dt-buttons .dt-button{
            padding: 3px 10px;
        font-size: 12px;
        }
        .morris-chart text{font-family:"Cerebri Sans,sans-serif"!important; fill: #2e2e2e}
        .morris-hover{position:absolute;z-index:10}
        .morris-hover.morris-default-style{font-size:12px;text-align:center;border-radius:5px;padding:10px 12px;background:white;font-family:var(--ct-font-sans-serif)}.morris-hover.morris-default-style .morris-hover-row-label{font-weight:700;margin:.25em 0;font-family:"Cerebri Sans,sans-serif"}.morris-hover.morris-default-style .morris-hover-point{white-space:nowrap;margin:.1em 0;color:#fff}
        .tab-content tbody td{
            font-size: 12px;
        }
        #card-success, .hide, .logoutRow, #recurringDepositEnabled{
            display: none;
        }

        button.nav-link{
            box-shadow: none !important;
        }

        .select2-results {
            max-height: none;
        }

        .wizard>.content>.body ul>li.select2-selection__choice{
            display: inline-block !important;
            max-width: 200px;
            display: none;
        }
        .select2{
            width: 100% !important;
        }
        .select2-container .select2-selection--single{
            height: 46px;
            border: 1px solid #ced4da;
            border-radius: .375rem;
        }
        .select2-container--default .select2-selection--single .select2-selection__arrow{
            height: 46px;
        }
        .select2-container--default .select2-selection--single .select2-selection__rendered{
            line-height: 46px;
        }
        form{
            width: 100% !important;
        }
        .text-large{
            font-size: 275%;
            line-height: 3;
        }
        @media(max-width: 768px){
            .sm-hide{
                display: none;
            }
            .sm-mt-0{
                margin-top: 0 !important;
            }
            .welcome{
                padding-top: 0 !important;
            }
            .hero-section{
                margin-top: 0 !important;
            }
            .logoutRow{
                display: block;
            }
            body{
                padding-bottom: 150px;
            }
            #products-nav-link{
                display: none;
            }
            #footer{
                display: none;
            }
            .dtr-details{
                list-style: none;
            padding: 0;
            padding-left: 10px;
            width: 100%;
            }
            .dtr-details li.text-center{
                text-align: left !important;
            }
        }
        @media(min-width: 1200px){
            .nav-justified .nav-item, .nav-justified>.nav-link{
                flex-grow: 0.25;
            }
            .lg-topside{
                right: 0;
            position: absolute;
            top: -56px;
            }
        }
        @media(max-width: 1200px){
            .nav-justified .nav-item, .nav-justified>.nav-link{
                flex-grow: 1;
            }
        }
        status{
            width: 10px;
            height: 10px;
            border-radius: 50%;
            background: grey;
            margin-top: 10px;
        margin-left: 10px;
        position: absolute;
        }
        status.success{
            background: #19b69b;
        }
        status.warning{
            background: #e69d11;
        }

        #warpDriveFunds .modal-dialog{
            z-index: 999;
        }
        .features ul{
            list-style: none;
            padding: 0;
        }
        .features ul li{
        padding: 10px;
        border-bottom: 1px solid whitesmoke;
        font-size: 13px;
        }
        .features .btn{
            width: 100%;
        }

            #holder canvas,#holder{
                background-color: rgba(0, 0, 0, 0);
            }

            ribbon .nav-link.active > *{
                color: #0d6efd;
            }

            .text-small{
                font-size: 12px;
            }

            .no-shadow{
                box-shadow: none !important;
            }
            .btn-square{
                width: 55px;
                height: 55px;
            }
            .StripeElement {
              /* Adjust the height property as needed */
              height: 40px; /* Example height adjustment */
            }
            #withdraw-balance-button .fa-sync.text-large{
                font-size: 180% !important;
            }

            #linkedBanks .account-list{
                list-style:none;
                text-align:left;
                padding-left: 0;
            }

        #linkedBanks .account-list li{
            border-bottom: 1px solid rgba(0, 0, 0, 0.1);
          padding: 4px 0;
          font-weight: 400;
          font-size: 14px;
        }
        .btn.btn-wide{
            width: 100%;
        }
        .circle {
            width: 10px;
            height: 10px;
            background-color: #62bd19;
            opacity: 0.7;
            border-radius: 50%;
            position: absolute;
            top: 23px;
            left: 23px;
        }
        .circle.circle-lg{
            position: relative;
            width: 25px;
            height: 25px;
            display: block;
            top: 0;
            left: 0;
        }
        .circle.circle-md{
            position: relative;
            width: 15px;
            height: 15px;
            display: block;
            top: 0;
            left: 0;
        }
        .circle.circle-empty{
            border: 1px dashed black;
            background: white;
        }
    </style>

@endsection

@section('content')
    <div id="page-content">
        <div class="gradient-wrap">
            <div class="container hero-section mb-4 mt-4">
                <div class="col-md-12">
                <div class="welcome pt-4">
                        <h2>Welcome, {{ getFirstName($user->displayName) }}

                            <br>
                            <p style="font-size:12px; font-weight:400">Member ID: {{ getMemberID($user->uid, "A") }}
                                <!--
                                <a href="#" id="sweepstakesModal"><span style="color: black; background-color: rgba(255,255,255,0.6); padding: 5px 9px; border-radius: 10px;"><img src='/assets/img/icons/ticket.png' style='width:20px; margin-top: -3px; margin-right: 2px;'> <span class="sweepstakeEntries"><i class="fa fa-sync fa-spin"></i></span></span></a>
                                    <a href="#" id="leaderboardModal"><span style="color: black; background-color: rgba(255,255,255,0.6); padding: 5px 9px; border-radius: 10px;"><img src='/assets/img/icons/trophy.png' style='width:12px; margin-top: -3px; margin-right: 2px;'> 0</span></span></a>
                                -->
                            </p>

                    </h2>
                    </div>
                </div>
    
                <div class="row justify-content-center">
                    <div class="col-xl-3 col-md-4 order-md-2 d-none d-xs-none d-sm-none d-md-none d-lg-block d-xl-block">
                        <div class="col-md-12 pt-3">
                            <div class="card bg-white">
                                <div class="card-body">
                                    @include('pro.panels.menu')
                                </div>
                            </div>
                        </div>
                        @if(!$completedProSteps["complete"])
                            <div class="col-md-12 pt-3">
                                <div class="card bg-white">
                                    <div class="card-body">
                                        @include('pro.panels.onboarding')
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>

                    <div class="col-xl-3 col-md-4 order-md-2 d-lg-none d-xl-none">
                        <div class="col-md-12 pt-3">
                            @if(!$completedProSteps["complete"])
                                <div class="col-md-12 pt-3">
                                    <div class="card bg-white">
                                        <div class="card-body">
                                            @include('pro.panels.onboarding')
                                        </div>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>

                    <div class="col-xl-9 col-md-8 order-md-1">
                        <div class="tab-content" id="pills-tabContent">
                            <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                                @include('pro.panels.welcome')
                            </div>
                            <div class="tab-pane fade" id="pills-funding" role="tabpanel" aria-labelledby="pills-profile-tab">
                                @include('pro.panels.funding', [])
                            </div>
                            <div class="tab-pane fade" id="pills-orders" role="tabpanel" aria-labelledby="pills-contact-tab">
                                @include('boardingpass.panels.transactions')
                            </div>
                            <div class="tab-pane fade" id="pills-settings" role="tabpanel" aria-labelledby="pills-contact-tab">
                                @include('pro.panels.account')
                            </div>
                        </div>
                    </div>
                </div>
          </div>
        </div>
    </div>
    @include("boardingpass.panels.funding-source")      
    @include("boardingpass.panels.points-explainer")
    @include("boardingpass.panels.transactions.receipt")
    @include("boardingpass.panels.cancel-transaction")
    @include("boardingpass.panels.share")
    @include("boardingpass.panels.sweepstakes")
    @include("boardingpass.panels.card") 
@endsection

@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/switchery/0.8.2/switchery.min.js" integrity="sha512-lC8vSUSlXWqh7A/F+EUS3l77bdlj+rGMN4NB5XFAHnTR3jQtg4ibZccWpuSSIdPoPUlUxtnGktLyrWcDhG8RvA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="https://cdn.jsdelivr.net/npm/apexcharts"></script>
    <script type="text/javascript" src="https://coderthemes.com/ubold/layouts/assets/libs/morris.js06/morris.min.js"></script>
    <script async defer src="https://polyfill.io/v3/polyfill.min.js?features=smoothscroll%2Cfetch%2Cdefault%2CIntersectionObserver" type="bc29ea08ed5de13fee4ff9a3-text/javascript"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/v/bs5/dt-1.13.6/datatables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/2.2.3/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/2.2.3/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/2.2.3/js/buttons.print.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/responsive/2.5.0/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
    @include("pro.dashboard-js")

    <script type="text/javascript">
         $("#requestNewPassword").click(function(){
            $.ajax({
                url: "{{ route('boarding-reset-password') }}",
                dataType: 'json',
                type: 'post',
                success: function (data) {
                    if(data == 200){
                        swal({
                                icon: 'success',
                                title: 'Password email sent!',
                                text: "",
                            });
                    }
                    if(data == 500){
                        swal({
                                icon: 'error',
                                title: 'Something went wrong',
                                text: "We we unable to send your password reset email. ",
                            });
                    }
                }
            });
         });
    </script>
@endsection