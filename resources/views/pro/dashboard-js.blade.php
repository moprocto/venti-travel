	<script src="https://js.stripe.com/v3/"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/ismobilejs/0.4.1/isMobile.min.js"></script>
    <script type="text/javascript">
        $(document).ready( function () {
        	$.ajaxSetup({
		      headers: {
		          'X-CSRF-TOKEN': "{{ csrf_token() }}"
		      }
			});

			fetchAutosaves();
			getCustomerCards();
			fetchConnectedBanks();

			const stripe = Stripe("{{ env('STRIPE_KEY_' . strtoupper(env('APP_ENV'))) }}");
			var elements = stripe.elements();

			const newCardElement = elements.create('card', {
			  style: {
			    base: {
			      fontSize: '16px',
			      color: '#32325d',
			      fontFamily: '-apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Helvetica, Arial, sans-serif',
			      fontVariant: 'normal',
			      fontWeight: '400',
			      lineHeight: '1.5',
			      display: 'block',
			      width: '100%',
			      height: '40px',
			      padding: '10px 12px',
			      borderRadius: '0.25rem',
			      backgroundColor: '#f8f9fa',
			      transition: 'all 0.15s ease-in-out',
			    },
			    invalid: {
			      color: '#fa755a',
			      iconColor: '#fa755a',
			    },
			  },
			});

			newCardElement.mount('#new-card-element');



			const form = document.getElementById('addCardForm');

			let cardComplete = false;

			newCardElement.on('change', function(event) {
			  cardComplete = event.complete
			});

			form.addEventListener('submit', async (event) => {
				var btn = $("#addCard");
				btn.addClass("disable-while-loading");

				var passable = checkRequiredFields("#addCardForm");

				if(!passable){
					btn.removeClass("disable-while-loading");
					return swal({
			            icon: 'warning',
			            title: 'Missing required fields',
			            text: 'Please go back and check that all required steps have been completed',
			        });
				}


				event.preventDefault();

				if (!cardComplete) {
					btn.removeClass("disable-while-loading");
					swal({
					  icon: 'error',
					  title: 'Try again',
					  text: 'Please fill out all form fields',
					});
					return;
				}

				// Create a payment method object from the card element
				const { paymentMethod, error } = await stripe.createPaymentMethod('card',newCardElement);

				if (error) {
					btn.removeClass("disable-while-loading");
					console.error('Error creating payment method:', error);
					return;
				}

			  	// Send the payment method ID to your Laravel backend for saving
			 	const paymentMethodId = paymentMethod.id;
			 	const cardBrand = paymentMethod.card.brand;
			 	const cardLast4 = paymentMethod.card.last4;
			 	const cardExpMonth = paymentMethod.card.exp_month;
			 	const cardExpYear = paymentMethod.card.exp_year
			 	const cardType = paymentMethod.card.funding;

				// Use AJAX or fetch to send data to your Laravel route

				if(paymentMethodId != undefined && paymentMethodId != ""){
					$.ajax({
						url: "{{ route('boarding-save-card') }}",
						processData: true,
						contentType: "application/json",
						dataType: 'json',
						type: 'post',
						data: JSON.stringify({
							payment_method_id: paymentMethodId,
							cardName: $("#cardName").val(),
							cardNickname: $("#cardNickname").val(),
							cardBrand: cardBrand,
							cardLast4: cardLast4,
							cardExpMonth: cardExpMonth,
                			cardExpYear: cardExpYear,
                			cardType: cardType
						}),
						success: function (data) {
							if(data.status == 200){
								getCustomerCards();
								clearFields("#addCardForm");
								$("#addCardModal").modal("hide");
								btn.removeClass("disable-while-loading");
								return swal({
								  icon: 'success',
								  title: 'Success!',
								  text: 'Your card was securely saved for future payments.',
								});
							}
							if(data.status == 500){
								btn.removeClass("disable-while-loading");
								return swal({
								  icon: 'error',
								  title: 'Something went wrong',
								  text: data.message,
								});
							}
						},
						error: function (data){
							btn.removeClass("disable-while-loading");
							return swal({
							  icon: 'error',
							  title: 'Try again',
							  text: 'There was a problem saving this card to your account. Please try again or contact support via admin@venti.co',
							});
						}
					});
				}

				
			});


        	var maxAmount = parseInt(0);
    		var max = parseInt(0);

    		$(".nav-link").click(function(){
    			$('#transactionsTable').dataTable().fnAdjustColumnSizing();
    			$('#snapshotsTable').dataTable().fnAdjustColumnSizing();
    		});

        	var transactionsTable = new DataTable('#transactionsTable', {
    			dom: 'Bfrtip',
	            buttons: [
	            	'copy', 'csv', 'excel', 'pdf', 'print'
	            ],
	            responsive: true,
	            autoWidth: false,
	            rowReorder: {
			        selector: 'td:nth-child(2)'
			    },
	            ajax:{
					"url":"{{ route('transactions-table') }}",
					"type": "POST",
					"data": function (d){
						d.transactable = true,
						d.user = "{{ $user->uid }}"
					}
				},
				order: [[5, 'desc']],
				columns: [
					{ "data": "description"},
					{ "data": "category", "class" : "text-left"},
					{ "data": "from", "class" : "text-left"},
					{ "data": "to"},
					{ "data": "total", "class" : "text-right"},
					{ "data": "timestamp", "class" : "text-left"},
					{ "data": "status", "class" : "text-center"},
					{ "data": "action", "class" : "text-cetner"}
				],
				initComplete: function () {
				        this.api()
			            .columns()
			            .every(function (i) {
			                let column = this;
			 				if(i > 0 && i != 4 && i != 5 && i != 7){
				                let select = document.createElement('select');
				                select.add(new Option(''));
				                column.footer().replaceChildren(select);
				 
				                // Apply listener for user change in value
				                select.addEventListener('change', function () {
				                    var val = DataTable.util.escapeRegex(select.value);
				                    //
				                    column.search(val ? '^' + val + '$' : '', true, false)
		                        	.draw();
				                    
				                });
				 
				                // Add list of options
				                column
				                    .data()
				                    .unique()
				                    .sort()
				                    .each(function (d, j) {
				                        select.add(new Option(d));
				                 });
			 				}
			            });
				    }
			});

			transactionsTable.buttons().container().appendTo('#transactionsTable_wrapper .col-md-6:eq(0)');

			var snapshotsTable = new DataTable('#snapshotsTable', {
    			dom: 'Bfrtip',
	            buttons: [
	            	'copy', 'csv', 'excel', 'pdf', 'print'
	            ],
	            responsive: true,
	            autoWidth: false,
	            rowReorder: {
			        selector: 'td:nth-child(2)'
			    },
	            ajax:{
						"url":"{{ route('snapshots-table') }}",
						"type": "POST",

						"data": function (d){
							d.transactable = true,
							d.user = "{{ $user->uid }}" 
						}
				},
				order: [[3, 'desc']],
				columns: [
					{ "data": "description"},
					{ "data": "balance", "class" : "text-right"},
					{ "data": "points", "class" : "text-right"},
					{ "data": "timestamp", "class" : "text-left"},
					{ "data": "action", "class" : "text-center"},
				],
				initComplete: function () {
				        
				}
			});

			snapshotsTable.buttons().container().appendTo('#snapshotsTable_wrapper .col-md-6:eq(0)');

			$(".dt-button").each(function(){
    			$(this).addClass("btn");
    			$(this).addClass("btn-primary");
			});

			function getCustomerCards(){
				$.ajax({
		            url: "{{ route('boarding-get-cards') }}",
		            processData: true,
		            dataType: 'json',
		            data:{
		            	uid: "{{ $user->uid }}"
		            },
		            type: 'post',
		            success: function (data) {
		            	$("#cardList").empty();
		            	$.each(data, function(index, element) {
						     var cardElement = "<div class='col-md-12 col-lg-6 col-xl-4 mb-4'><div class='card bg-white small-shadow'><div class='card-body'><div style='display:flex; flex-direction:row; justify-content:space-between; align-items:center;'><span style='font-size:13px;'>" + element.cardNickname + "</span><button type='button' class='btn btn-white deleteCard' data-id='" + element.paymentId + "'><i class='fa fa-times'></i></button></div><p class='pt-4' style='align-items:center; font-size:17px; font-weight:700; letter-spacing:0.3em; display:flex;'>" + element.cardNumber + "<p><div style='display:flex; flex-direction:row; justify-content:space-between; align-items:center;'><span class='pt-4 pb-1'>" + element.cardExpMonth + "/" + element.cardExpYear + "</span><span class='pt-4 pb-1'>" + element.cardBrand + "</span></div></div></div></div>"
						     $("#cardList").append(cardElement);
						});
		            }
			    });
			}

			function fetchConnectedBanks(){

				$.ajax({
					url: "{{ route('list-plaid-banks') }}",
					processData: true,
					contentType: "application/json",
					dataType: 'json',
					type: 'post',
					data: JSON.stringify({
						user: "{{ $user->uid }}"
					}),
					success: function (data) {
						$.each(data, function(index, element) {
							var html = '<div class="col-md-12 col-lg-12 col-xl-4 mb-xs-3 mt-4 sm-mt-0"><div class="card bg-white small-shadow"><div class="card-body text-center">';

							html += '<div style="display: flex; justify-content: space-between; width:100%;"><span style="font-size:11px;">' + element.institution + '</span><span style="margin-top:-4px"></span></div>';

							html += '<h1 style="padding-top:20px; padding-bottom:20px;"><img src="/assets/img/icons/bank.png" style="width: 100%; min-width:55p; max-width:55px; min-height:55px;"></h1>';

							html += '<p class="text-small text-left">Linked Accounts</p>';

							html += '<ul class="account-list">';

								$.each(element.accounts, function(inde, account) {
									html += '<li><div class="d-flex" style="flex-direction:row; justify-content:space-between"><span>' + account.name + '</span><span>(' + account.mask + ')</span></div></li>';
								});

							html += '</ul>';

							html += '<a href="./pro/accounts/' + element.vid + '" class="btn btn-black btn-wide" target="_blank">Manage</a>'

							html += '</div></div></div>';

							$("#linkedBanks").append(html);
						});
					},
					error: function (data){
						
						
					}
				});

				
			}

			getPoints();


			function getPoints(){
				$.ajax({
					url: "{{ route('wallet-get-points-balance') }}",
					type: 'POST',
					async: true,
					processData: true,
					dataType: 'json',
					success: function (data) {
						$("#points-balance").html('<span id="points-balance"><h1 style=" color:white; font-size: 235%; margin: 0">' + data.toLocaleString(undefined, {minimumFractionDigits: 2}) + '</h1><span style="font-weight: 500; color: white;" class="text-small">Points</span></span>');
					}
				});
			}

			$("body").on("click", ".deleteCard", function(){
				var paymentMethodId = $(this).data("id");
				var button = $(this);
				swal({
		      		icon: 'warning',
		      		title: 'Delete card from profile',
		      		text: 'Are you sure you want to delete this card? This cannot be reversed. Any pending transactions will continue to be processed.',
				    denyButtonText: 'Cancel',
				    showDenyButton: true,
				    buttons: {
					    cancel: "Cancel",
					    delete: true,
					},
		  		}).then((result) => {
				  if (result == "delete") {
				    	$.ajax({
				            url: "{{ route('boarding-delete-card') }}",
				            processData: true,
				            dataType: 'json',
				            data:{
				                paymentMethodId : paymentMethodId
				            },
				            type: 'post',
				            success: function (data) {
				            	if(data == 200){
				            		swal({
								            icon: 'success',
								            title: 'Done!',
								            text: "We removed this card from your account",
								            showCancelButton: true,
												    closeOnConfirm: true,
												    closeOnCancel: true
								        }).then((result) => {
								        	getCustomerCards();
								        });
				            	} else {
				            		swal({
							            icon: 'warning',
							            title: 'Something went wrong',
							            text: 'We encountered an error while removing this card. Please contact support via admin@venti.co if you continue to have issues.',
							        });
				            	}
				   			}
						});
				  	}
				});
			});



			function generateShareLinkData(){
				$.ajax({
		            url: "{{ route('generate-share-data') }}",
		            processData: true,
		            dataType: 'json',
		            type: 'post',
		            success: function (data) {
		            	$("#sharing-loader").hide();
		            	$(".sharing-link").removeClass("hide");
		            	
		            	$("#link").html(data);
		            }
			    });
			}

			generateShareLinkData();

			$("#copyLink").click(function(){
				copyLink();
			});

			async function copyLink(){
	          if (navigator.clipboard) {

				  console.log('Clipboard API available');

				  await navigator.clipboard.writeText($('#link').text());
				}
			        
	            return swal({
					icon: 'success',
					title: 'All Set!',
					text: 'Your referral link has been copied',
				});
	        }

		  	var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));

		  	elems.forEach(function(html) {
			  var switchery = new Switchery(html);
			});

			getMarketingSubscriptions();

			function getMarketingSubscriptions(){
				$.ajax({
			        url: "{{ route('boarding-status-customer-marketing') }}",
			        processData: true,
			        dataType: 'json',
			        data:{
			            user : "{{ $user->uid }}"
			        },
			        type: 'post',
			        success: function (data) {
			        	for(i = 0; i < data.length; i++){
			        		$("#" + data[i]).trigger("click"); // turn it on
			        	}
			        }
			    });
			}

			$("body").on("click", ".switchery", function(){
				console.log("clicked")
				var tag = $(this).parent().find(".js-switch").attr("id");
				var enabled = true;
				console.log(tag);
				var elem = document.querySelector("#" + tag);
				var enabled = elem.checked;
				updateMarketingSubscription(tag, enabled);
			})

			function updateMarketingSubscription(tag, enabled){
				$.ajax({
			        url: "{{ route('boarding-status-customer-marketing-update') }}",
			        processData: true,
			        dataType: 'json',
			        data:{
			            user : "{{ $user->uid }}",
			            tag: tag,
			            enabled: enabled
			        },
			        type: 'post',
			        success: function (data) {
			        	
			        }
			    });
			}

			$("#goToPayments").click(function(){
				var screenWidth = $( window ).width();
				if(screenWidth > 768){
					$("button#pills-funding-tab").trigger("click");
				}
				else {
					$("#pills-funding-tab-mobile").trigger("click");
				}
			});

			@if(sizeof($fundingSources) >= 1 || $subscription == "pro")
				$("#addBankAccount").click(function(){
					var btn = $(this);
					btn.addClass("disable-while-loading");
					var accountType = $("#accountType").val();
					var accountName = $("#accountName").val();
					var accountNickname = $("#accountNickname").val();
					var accountNumber = $("#accountNumber").val();
					var routingNumber = $("#routingNumber").val();
					var accountNumberConfirm = $("#accountNumberConfirm").val();

					var passable = checkRequiredFields("#addFundingForm");

					if(!passable){
						btn.removeClass("disable-while-loading");
						return swal({
				            icon: 'warning',
				            title: 'Missing required fields',
				            text: 'Please go back and check that all required steps have been completed',
				        });
					}

					if(accountNumber != accountNumberConfirm){
						btn.removeClass("disable-while-loading");
						return swal({
				            icon: 'warning',
				            title: 'Check entries again',
				            text: 'Your account number confirmation does not match',
				        });
					}

					if(routingNumber.length != 9 || routingNumber.length > 9){
						btn.removeClass("disable-while-loading");
						return swal({
				            icon: 'warning',
				            title: 'Check entries again',
				            text: 'Your routing number must be nine digits long',
				        });
					}

					$.ajax({
			          url: "{{ route('boarding-create-funding-source') }}",
			          processData: true,
			          dataType: 'json',
			          data:{
			              routingNumber : routingNumber,
			        			accountNumber : accountNumber,
			        			bankAccountType : accountType,
			        			name : accountNickname,
			        			accountName: accountName
			          },
			          type: 'post',
			          success: function (data) {
			          	if(data == 200){
			          		btn.removeClass("disable-while-loading");
			          		swal({
						            icon: 'success',
						            title: 'Verification Pending',
						            text: "We've added this payment source to your Venti account, but it requires verification from our ACH payments service provider. Two deposits between $0.01 and $0.20 have been made to your bank account and will appear within the next 2 to 3 business days. To verify this funding source, you will need to enter the deposit amounts before you can use it for future deposits and withdrawals.",
						            showCancelButton: true,
								    confirmButtonColor: '#DD6B55',
								    confirmButtonText: 'Ok',
								    closeOnConfirm: true,
								    closeOnCancel: true
						        }).then((result)=>{
						        	window.location.href = "/home";
						        });

			          		clearFields("#addFundingForm");
			          	} else {
			          		btn.removeClass("disable-while-loading");
			          		swal({
						            icon: 'warning',
						            title: 'Something went wrong',
						            text: data.message,
						        });
			          	}
	 				}
					});
				});
			@endif

			$("#cardExpiry").on("keydown keyup", function(event) {
				var input = $(this).val();

				// Restrict input to MM/YY format
		        if (input.length > 5) {
		            input = input.substring(0, 5);
		        }

		        // Remove any non-digit characters except /
		        input = input.replace(/[^0-9\/]/g, '');

		        // Add / if it's not already there and the length is appropriate
		        if (input.length === 2 && !input.includes('/') && event.keyCode != 8) {
		            input = input + '/';
		        }

		        

		        // Validate month
		        var parts = input.split('/');
		        if (parts.length > 0 && parts[0].length === 2) {
		            var month = parseInt(parts[0], 10);
		            if (month < 1 || month > 12) {
		                input = input.substring(0, 1); // Remove invalid month digit
		            }
		        }

		        $(this).val(input);
			});

			$("#cardNumber").on("input keypress", function() {
			    var input = $(this).val();
	        
		        // Remove all non-digit characters
		        input = input.replace(/\D/g, '');

		        // Check for American Express pattern (starts with 34 or 37)
		        var isAmex = /^3[47]\d{0,13}$/.test(input);
		        
		        if (isAmex) {
		            // Format for American Express: 4-6-5
		            input = input.replace(/(\d{4})(\d{0,6})(\d{0,5})/, function(_, g1, g2, g3) {
		                return g1 + (g2 ? ' ' + g2 : '') + (g3 ? ' ' + g3 : '');
		            });
		        } else {
		            // General format: 4-4-4-4
		            input = input.replace(/(\d{4})(\d{0,4})(\d{0,4})(\d{0,4})/, function(_, g1, g2, g3, g4) {
		                return g1 + (g2 ? ' ' + g2 : '') + (g3 ? ' ' + g3 : '') + (g4 ? ' ' + g4 : '');
		            });
		        }

		        // Limit the length of the input to 19 characters for non-Amex and 17 for Amex
		        if (isAmex) {
		            input = input.substring(0, 17);
		        } else {
		            input = input.substring(0, 19);
		        }

		        // Set the formatted value back to the input field
		        $(this).val(input);
			 });

			function checkRequiredFields(target){
				var passable = true;

				$(target + " .required").each(function(){
					$(this).removeClass("invalid-feedback");
					
					if(!checkIfEmpty($(this).val())){

						$(this).addClass("invalid-feedback");
						passable = false;
					}
				});

				return passable;
			}

			function clearFields(target){
				$(target + " .required").each(function(){
					$(this).val("");
				});
			}

			$(".deleteSource").click(function(){
				// are you sure?
				var button = $(this);
				var fundingSourceID = $(this).attr("data-funding-id");
				swal({
		      		icon: 'warning',
		      		title: 'Delete funding source',
		      		text: 'Are you sure you want to delete this funding source? This cannot be reversed. Any pending transactions will continue to be processed.',
				    denyButtonText: 'Cancel',
				    showDenyButton: true,
				    buttons: {
					    cancel: "Cancel",
					    delete: true,
					},
		  		}).then((result) => {
					  if (result == "delete") {
					    	$.ajax({
					            url: "{{ route('boarding-remove-funding-source') }}",
					            processData: true,
					            dataType: 'json',
					            data:{
					                fundingSourceID : fundingSourceID
					            },
					            type: 'post',
					            success: function (data) {
					            	if(data == 200){
					            		swal({
									            icon: 'success',
									            title: 'Done!',
									            text: "We removed this funding source from your account",
									            showCancelButton: true,
													    closeOnConfirm: true,
													    closeOnCancel: true
									        }).then((result) => {
									        	button.parent().parent().parent().parent().parent().remove();
									        });
					            	} else {
					            		swal({
									            icon: 'warning',
									            title: 'Something went wrong',
									            text: 'We encountered an error while removing this bank account. Please contact support via admin@venti.co if you continue to have issues.',
									        });
					            	}
					   			}
							});
					  	}
					});
			});

		    function recalculateDepositAmount(){
		    	var depositAmount = $("#depositAmount").val();
		    	var depositSpeed = $("#depositSpeed").val();
		    	var depositSource = $("#depositSource").val();
		    	
		    	var fundText = "Funds";

		    	if(depositAmount > 0){
		    		if(depositSpeed == "standard"){
			    			fundText = "$" + depositAmount.toLocaleString(2);
			    			$("#fundEstimate").text("in 3 to 5 business days");
			    	}
			    	if(depositSpeed == "next-available"){
			    			if(depositAmount < 0){
			    				//error
			    			}
			    			fundText = "$" + depositAmount.toLocaleString(2);
			    			$("#fundEstimate").text("in 1 to 3 business days");
			    	}
			    	if(depositSpeed == "today"){
			    			fundText = "$" + depositAmount.toLocaleString(2);
			    			$("#fundEstimate").text("later today or tomorrow");
			    	}
		    	}
		    	return fundText;
		    }

		    function recalculateWithdrawAmount(){
		    	var withdrawAmount = $("#withdrawAmount").val();
		    	var withdrawSpeed = $("#withdrawSpeed").val();
		    	var withdrawSource = $("#withdrawSource").val();
		    	
		    	var fundText = "Funds";

		    	if(withdrawAmount > 0){
		    		if(withdrawSpeed == "standard"){
			    			fundText = "$" + withdrawAmount.toLocaleString(2);
			    			$("#withdrawFundEstimate").text("in 3 to 5 business days");
			    	}
			    	if(withdrawSpeed == "next-available"){
			    			withdrawAmount -= 2;
			    			if(withdrawAmount < 0){
			    				//error
			    			}
			    			fundText = "$" + withdrawAmount.toLocaleString(2);
			    			$("#withdrawFundEstimate").text("in 1 to 3 business days");
			    	}
			    	if(withdrawSpeed == "today"){
			    			withdrawAmount -= 2;
			    			fundText = "$" + withdrawAmount.toLocaleString(2);
			    			$("#withdrawFundEstimate").text("later today or tomorrow");
			    	}
		    	}
		    	return fundText;
		    }

		    // set the default date for recurring deposits
		    var recurringMin = moment().add(1, "days").format("YYYY-MM-DD");
		    var recurringMax = moment().add(1, "months").format("YYYY-MM-DD");
		    $("#depositStartDate")
		    	.attr("min", recurringMin)
		    	.attr("max", recurringMax);

		    $("#recurringDepositForm .required").on("change", function(){
		    	// update the confirmation text
		    	var depositAmount = $("#recurringDepositAmount").val();
				var depositSpeed = $("#recurringDepositSpeed").val();
				var depositSource = $("#recurringDepositSource").val();
				var depositFrequency = $("#recurringDepositSource").val();
				var depositStartDate = moment($("#depositStartDate").val() ?? "today").format("MMMM D, YYYY");
				var bankName = $("#recurringDepositSource").find(":selected").attr("data-name");
				var helperText = $("#depositFrequency").find(":selected").attr("data-helper-text");
				var summaryText = "Starting " + depositStartDate + ", we will deduct $" + depositAmount.toLocaleString(undefined, {maximumFractionDigits: 0}) + " from " + bankName + " " + helperText + " until you stop this deposit, a deposit fails to processs, or your maximum Cash Balance will be exceeded. We estimate your funds to arrive in 3 to 5 business days from the date the deposit is initiated</span>.<sup>*</sup>";

				var depositReward = (depositAmount <= 100) ? 0.5 : Math.round(depositAmount * .0013, 0);

				var passable = checkRequiredFields("#recurringDepositForm");

				if(depositAmount < 25 || depositAmount > 7500){
					passable = false;
				}

				if(passable){
					$("#depositSummary").html(summaryText);
					$("#depositReward").html("<strong>You'll earn " + depositReward + " Points each time this deposit clears</strong>");
					$("#fundAmountRecurring").text("$" + depositAmount.toLocaleString(undefined, {maximumFractionDigits: 0}));
				}
				else {
					$("#depositSummary").text("Please complete all requred fields.");
					$("#depositReward").html("");
				}
		    });

		    $("#finalizeRecurringDeposit").click(function(){
		    	var btn = $(this);
				btn.addClass("disable-while-loading");
				var depositAmount = $("#recurringDepositAmount").val();
				var depositSpeed = $("#recurringDepositSpeed").val();
				var depositSource = $("#recurringDepositSource").val();
				var depositFrequency = $("#depositFrequency").val();
				var depositStartDate = $("#depositStartDate").val();
				var bankName = $("#recurringDepositSource").find(":selected").attr("data-name");
				var helperText = $("#depositFrequency").find(":selected").attr("data-helper-text");

				var passable = checkRequiredFields("#recurringDepositForm");

				if(!passable){
					btn.removeClass("disable-while-loading");
					return swal({
			            icon: 'warning',
			            title: 'Missing required fields',
			            text: 'Please go back and check that all required steps have been completed',
			        });
				}

				if(depositAmount < 25 || depositAmount > 7500){
					btn.removeClass("disable-while-loading");
					return swal({
			            icon: 'warning',
			            title: 'Something went wrong',
			            text: 'At this time, your deposit amount must be greater than $25 and less than $1,000',
			        });
				}
				
				// process deposit

				$.ajax({
		            url: "{{ route('recurring-deposit-to-boarding-pass') }}",
		            processData: true,
		            dataType: 'json',
		            data:{
		            	depositAmount : depositAmount,
		      			depositSpeed : depositSpeed,
		      			depositSource : depositSource,
		      			depositFrequency: depositFrequency,
		      			depositStartDate: depositStartDate,
		      			bankName: bankName,
		      			helperText: helperText
		            },
		            type: 'post',
		            success: function (data) {
		            	if(data == 200){
		            		$("#depositFunds").modal("hide");

		            		btn.removeClass("disable-while-loading");
		            		clearFields("#recurringDepositForm");

		            		swal({
					            icon: 'success',
					            title: 'Your recurring deposit has been scheduled',
					            text: 'We have also sent you a confirmation email',
					            buttons: {
									refresh: "Finish"
								}
					        }).then((value) => {
					        	if(value == "Finish"){
					        		window.location.href = "/home"
					        	}
					        });

							return true;
		            	} 

		            	else{
		            		btn.removeClass("disable-while-loading");

		            		return swal({
					            icon: 'error',
					            title: 'Something went wrong',
					            text: data.message
					        });
		            	}
		        	},
		        	error: function(){
		        		btn.removeClass("disable-while-loading");
		        		return swal({
				            icon: 'error',
				            title: 'Your request could not be processed',
				            text: 'We were unable to process this request, which could be due to a variety of reasons. We recommend trying again or contacting admin@venti.co if issues persist.',
					    });
		        	}
		      });
		    });

			$("#finalizeDeposit").click(function(){
				var btn = $(this);
				btn.addClass("disable-while-loading");
				var depositAmount = $("#depositAmount").val();
				var depositSpeed = $("#depositSpeed").val();
				var depositSource = $("#depositSource").val();

				var passable = checkRequiredFields("#depositForm");


				if(!passable){
					btn.removeClass("disable-while-loading");
					return swal({
			            icon: 'warning',
			            title: 'Missing required fields',
			            text: 'Please go back and check that all required steps have been completed',
			        });
				}

				if(depositAmount > 7500){
					btn.removeClass("disable-while-loading");
					return swal({
			            icon: 'warning',
			            title: 'Something went wrong',
			            text: 'At this time, your deposit amount must be less than $5,000',
			        });
				}

				$.ajax({
		            url: "{{ route('deposit-to-boarding-pass') }}",
		            processData: true,
		            dataType: 'json',
		            data:{
		                depositAmount : depositAmount,
		          			depositSpeed : depositSpeed,
		          			depositSource : depositSource
		            },
		            type: 'post',
		            success: function (data) {
		            	if(data == 200){
		            		$("#depositFunds").modal("hide");

		            		btn.removeClass("disable-while-loading");
		            		clearFields("#depositForm");

		            		updateBalanceDetails();

		            		swal({
					            icon: 'success',
					            title: 'Your deposit has been initiated',
					            text: 'We have also sent you a confirmation email',
					            buttons: {
									refresh: "Finish"
								}
					        }).then((value) => {
					        	if(value == "Finish"){
					        		window.location.href = "/home"
					        	}
					        });

						setTimeout(function() {
							$("#transactionsTable").DataTable().ajax.reload();
						},7000);

							return true;
		            	} 

		            	else{
		            		btn.removeClass("disable-while-loading");

		            		return swal({
					            icon: 'error',
					            title: 'Something went wrong',
					            text: data.message
					        });
		            	}
		        	},
		        	error: function(){
		        		btn.removeClass("disable-while-loading");
		        		return swal({
					            icon: 'error',
					            title: 'Your request could not be processed',
					            text: 'We were unable to process this request, which could be due to a variety of reasons. We recommend trying again or contacting admin@venti.co if issues persist.',
					        });
		        	}
		      });
			});

			// fetch any autosaves

			function fetchAutosaves(){
				$.ajax({
		            url: "{{ route('recurring-deposit-status') }}",
		            processData: true,
		            dataType: 'json',
		            type: 'post',
		            success: function (data) {
		            	if(data.code != 400){
		            		$("#recurringDepositEnabled").show();
		            		$("#recurringSummary").html(data.summary);
		            		$("#recurringDepositForm").html("");
		            		$("#deleteRecurring").attr("data-autosave-id", data.id);
		            	}
		            }
		        });
			}

			$("body").on("click", "#deleteRecurring", function(){
				$.ajax({
		            url: "{{ route('recurring-deposit-delete') }}",
		            processData: true,
		            dataType: 'json',
		            type: 'post',
		            data:{
		            	id: $(this).attr("data-autosave-id")
		            },
		            success: function (data) {
		            	return swal({
				            icon: 'success',
				            title: 'Your recurring deposit was deleted',
				            text: 'Any transactions that are pending and past the cancellation window will still be processed. Please refresh the page.',
				            buttons: {
							    refresh: "Refresh"
							}
				        }).then((value) => {
							window.location.href = "/home"
				        });
		            }
		        });
			});

		    $(".toggleFundingVerify").click(function(){
		    	var fundingSourceID = $(this).attr("data-source-id");
		    	var bankName = $(this).attr("data-bank-name");
		    	console.log(bankName)
		    	$("#verifyingBank").val(fundingSourceID);
		    	$("#bankName").text(bankName);
		    })

		    $("#depositVerification").click(function(){
		    	var btn = $(this);
		    	btn.addClass("disable-while-loading");
		    	var fundingSourceID = $("#verifyingBank").val();
		    	var firstDeposit = parseInt($("#firstDeposit").val());
		    	var secondDeposit = parseInt($("#secondDeposit").val());

		    	var passable = checkRequiredFields("#depositVerifyForm");

		    	if(!passable){
		    		btn.removeClass("disable-while-loading");
		    		return swal({
			            icon: 'warning',
			            title: 'Missing required fields',
			            text: 'Please go back and check that all required steps have been completed',
			        });
		    	}

		    	if(firstDeposit > 20 || firstDeposit < 1 || secondDeposit > 20 || secondDeposit < 1 || isNaN(firstDeposit) || isNaN(secondDeposit)){
		    		btn.removeClass("disable-while-loading");
		    		return swal({
			            icon: 'warning',
			            title: 'Invalid deposit amounts',
			            text: 'Please go back and check your deposit entries are between 1 and 10',
			      });
		    	}

		    	$.ajax({
			        url: "{{ route('boarding-verify-bank') }}",
			        processData: true,
			        dataType: 'json',
			        data:{
			            fundingSourceID : fundingSourceID,
			      			firstDeposit : firstDeposit,
			      			secondDeposit : secondDeposit
			        },
			        type: 'post',
			        success: function (data) {
			        	if(data == 200){
			        		btn.removeClass("disable-while-loading");
			        		$("#verifyFunds").modal("hide");
			        		$("#verifyFunds").hide();

			        		return swal({
					            icon: 'success',
					            title: 'Your funding source is verified',
					            text: 'You can now begin using this bank account for deposits and withdrawals.',
					            buttons: {
										    refresh: "Refresh"
										  }
					        }).then((value) => {
										window.location.href = "/home"
					        });
			        	}
			        	if(data == 201){
			        		btn.removeClass("disable-while-loading");
			        		return swal({
					            icon: 'error',
					            title: 'Invalid entry',
					            text: 'The deposit amounts do not match what was sent to this bank. Remember, you are limited to three verification attempts. After the third failed attempt, this funding source will be permanently blocked. If you believe you are receiving this in error, please contact support via admin@venti.co',
					        });
			        	}
			        	if(data == 500){
			        		btn.removeClass("disable-while-loading");
			        		return swal({
					            icon: 'error',
					            title: 'Something went wrong',
					            text: 'We tried to verify this bank, but hit a snag. Try again. If you continue having issues, please contact support via admin@venti.co',
					        });
			        	}
			        }
			    });
		    	
		    });	

			var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'))

			var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
			  return new bootstrap.Tooltip(tooltipTriggerEl)
			});

			$("#timezone").select2();

			$("#updateAccountButton").click(function(){
				var btn = $(this);
				btn.addClass("disable-while-loading");

				var timezone = $("#timezone").find(':selected').val();

				if(timezone != ""){
					$.ajax({
			            url: "{{ route('update-timezone') }}",
			            processData: true,
			            dataType: 'json',
			            data:{
			                timezone : timezone
			            },
			            type: 'post',
			            success: function (data) {
			            	if(data == 500){
			            		btn.removeClass("disable-while-loading");

			            		return swal({
					            icon: 'warning',
					            title: 'Your request could not be processed',
					            text: 'We were unable to process this request, which could be due to a variety of reasons. We recommend trying again or contacting admin@venti.co if issues persist.',
					        });
			            	}
			            	if(data == 200){
			            		btn.removeClass("disable-while-loading");

			            		return swal({
					            icon: 'success',
					            title: 'Your profile has been updated'
					            });
			            	}
			            }
			    });
				}
			});

			$("body").on("click",".btn-generate-receipt-preview",function(){

				var transactionID = $(this).attr("data-transaction-id");
				var note = $(this).attr("data-note");
				var from = $(this).attr("data-from");
				var to = $(this).attr("data-to");
				var type = $(this).attr("data-type");
				var amount = parseFloat($(this).attr("data-amount")).toLocaleString(undefined, {minimumFractionDigits: 2});
				var total = parseFloat($(this).attr("data-total")).toLocaleString(undefined, {minimumFractionDigits: 2});
				var fee = parseFloat($(this).attr("data-fee")).toLocaleString(undefined, {minimumFractionDigits: 2});
				var timestamp = $(this).attr("data-timestamp");
				var ampm = $(this).attr("data-ampm");
				var status = $(this).attr("data-status");
				var category = $(this).attr("data-category");

				if(fee >0){
					note = note + " | Expedited";
					$("#fee-disclaimer").text("Transaction fees are withdrawn from your Boarding Pass balance. Deposit speeds vary by bank.");
				}

				$("#receipt-transaction-from").text(from);
				$("#receipt-transaction-to").text(to);

				$("#receipt-transaction-id").text(transactionID);
				$("#receipt-transaction-note").text(note + ":");
				$("#receipt-transaction-date").text(timestamp + " " + ampm);
				$("#receipt-transaction-status").text(status);

				if(category != "Points"){
					amount = "$" + amount;
					total = "$" + total;
					fee = "$" + fee;
				}

				$("#receipt-transaction-amount").text(amount);
				$("#receipt-transaction-total").text(total);
				$("#receipt-transaction-fee").text(fee);

				$("#receipt-transaction-type").text(type);

			});

			$("#sendPoints").click(function(){
				var btn = $(this);
				btn.addClass("disable-while-loading");
				var pointsToSend = parseFloat($("#pointsToSend").val());
				var pointsRecipient = $("#pointsRecipient").val();
				pointsToSend = Math.round(pointsToSend * 100)/100;

				if(pointsToSend <= 0 || pointsToSend == undefined || pointsToSend == "" || isNaN( pointsToSend) ){
					btn.removeClass("disable-while-loading");
					$("#pointsToSend").addClass("invalid-feedback");

					swal({
			            icon: 'warning',
			            title: 'Invalid Points Amount',
			            text: 'Please enter a whole number',
			        });

					return false;
				}

				if(pointsRecipient == "" || pointsRecipient == undefined){
					btn.removeClass("disable-while-loading");
					$("#pointsRecipient").addClass("invalid-feedback");

					swal({
			            icon: 'warning',
			            title: 'Invalid Recipient',
			            text: 'Recipient cannot be blank',
			        });

					return false;
				}

				$.ajax({
		            url: "{{ route('share-points') }}",
		            processData: true,
		            dataType: 'json',
		            data:{
		                pointsToSend : pointsToSend,
		                pointsRecipient : pointsRecipient
		            },
		            type: 'post',
		            success: function (data) {
		            	btn.removeClass("disable-while-loading");
		            	if(data == 500){
		            		return swal({
					            icon: 'warning',
					            title: 'Your request could not be processed',
					            text: 'We could not find a Venti account with that email address',
					        });
		            	}
		            	if(data == 501){
		            		return swal({
					            icon: 'warning',
					            title: 'Your request could not be processed',
					            text: 'This person has not completed the setup of their account',
					        });
		            	}
		            	if(data == 502){
		            		return swal({
					            icon: 'warning',
					            title: 'Your request could not be processed',
					            text: 'You need to wait a full week before you can purchase Boarding Pass Points.',
					        });
		            	}
		            	if(data == 503){
		            		return swal({
					            icon: 'warning',
					            title: 'Your request could not be processed',
					            text: 'This account cannot receive this many points at this time. Enter a lower amount or try again later.',
					        });
		            	}
		            	if(data == 504){
		            		return swal({
					            icon: 'warning',
					            title: 'Your request could not be processed',
					            text: 'The amount of Points exceeds what is available on your Boarding Pass.',
					        });
		            	}
		            	if(data == 200){

		            		updateBalanceDetails();
		            		setTimeout(function() {
				            		$("#transactionsTable").DataTable().ajax.reload();
				            	},7000);


		            		$("#pointsToSend").val("");
							$("#pointsRecipient").val("");

		            		swal({
				            	icon: 'success',
				            	title: 'You sent ' + pointsToSend + ' Points! Your recipient was notified via email'
				            });

				            $("#referralModal").modal("hide");

				            return true;
		            	}
		            },
		            error: function(e){
		            	btn.removeClass("disable-while-loading");
		            }
			    });
			});
		});

		var mobile = false;

		//---

		if ( isMobile.phone || isMobile.tablet ) {
		  
		mobile = true;
		  
		}
		$("#chatBubble").click(function(){
			$(this).addClass("opened");
			$("#chatBubble").hide();
			$("#chatWindow").addClass("opened");
			$("#chatWindow").animate({
			    width: '350px',
			    height: '550px'
			  }, 0); // 1000 milliseconds = 1 second
		});
		$("#closeChat").click(function(){
			$("#chatBubble").show();
			$("#chatBubble").removeClass("opened");
			$("#chatWindow").removeClass("opened");
			$("#chatWindow").animate({
			    width: '0px',
			    height: '0px'
			  }, 100); // 1000 milliseconds = 1 second
		})
</script>