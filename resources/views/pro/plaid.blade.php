@extends('layouts.curator-master')

@section('css')
    <style type="text/css">
        .text-small{
            font-size: 12px;
        }
    </style>
@endsection

@section("content")
    <div id="page-content">
        <div class="gradient-wrap">
            <div class="container hero-section mb-4 mt-4">
                <div class="row" style="justify-content:center;">
                    <div class="col-md-6 col-md-offset-3 justify-content-center">
                        <div class="card mb-4">
                            <div class="card-body">
                                <p>Link a <strong>savings account</strong> belonging to one of the below credit unions, you'll earn Points every week based on your average daily balance. <strong>When connecting your account, you must enable permissions for Venti to read your account balances and transaction history.</strong></p>
                                <div class="row">
                                    <div class="col-md-4">
                                        <a href="https://www.wingsfinancial.com/" target="_blank">
                                            <img src="/assets/img/wings-logo.png" class="d-block" style="height:50px; margin:20px auto;">
                                        </a>
                                    </div>
                                    <div class="col-md-4">
                                        <a href="https://www.meriwest.com/" target="_blank">
                                            <img src="/assets/img/meriwest-logo.png" class="d-block" style="height:50px; margin:20px auto;">
                                        </a>
                                    </div>
                                    <div class="col-md-4">
                                        <a href="https://www.msufcu.org/" target="_blank">
                                            <img src="/assets/img/msufcu-logo.png" class="d-block" style="height:50px; margin:20px auto;">
                                        </a>
                                    </div>
                                </div>

                                <button id="link-button" class="btn btn-black" style="width:100%;">Link Savings Account</button>
                                <br>
                                <p class="text-small mt-4">Venti is not an affiliate of these credit unions. All logos are trademarks of their respective owners. Linking a bank account does not give Venti the ability to move funds on your behalf.</p>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-body">
                                <h4>Get 120 Points</h4>
                                <p>Reward offer is only credited once in the form of 120 Points to your Boarding Pass. You can use these Points to take $120 off your next flight or hotel booking on Venti. <strong>Must link a NEW savings account</strong> by one of the following credit unions: Wings Financial Credit Union, Meriwest Credit Union, or Michigan State University Federal Credit Union. See <a href="/about/pro" target="_blank">full criteria</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script src="https://cdn.plaid.com/link/v2/stable/link-initialize.js"></script>
    <script>
        document.getElementById('link-button').addEventListener('click', function () {
        fetch('/pro/plaid/link')
            .then(response => response.json())
            .then(data => {
                if (!data.link_token) {
                    console.error("No link_token found in response");
                    return;
                }

                const linkHandler = Plaid.create({
                    token: data.link_token,
                    onSuccess: function (public_token, metadata) {
                        fetch('/pro/plaid/exchange-token', {
                            method: 'POST',
                            headers: {
                                'Content-Type': 'application/json',
                                'X-CSRF-TOKEN': '{{ csrf_token() }}'
                            },
                            body: JSON.stringify({
                                public_token: public_token
                            })
                        })
                        .then(response => response.json())
                        .then(data => {
                            console.log(data);
                            swal({
                              icon: 'success',
                              title: 'We connected your bank!',
                              text: 'You will be redirected soon',
                            });

                            setTimeout(function(){
                                window.location.href = "/pro/accounts/" + data.vid;
                            }, 3000);
                        });
                          
                    },
                    onExit: function (err, metadata) {
                        if (err != null) {
                            console.error(err);
                        } else {
                            console.log(metadata);
                        }
                    }
                });

                // Open the Plaid Link flow
                linkHandler.open();
            })
            .catch(error => console.error('Error fetching link token:', error));
    });
    </script>
@endsection