@extends("layouts.curator-master")

@section("css")
    <link rel="stylesheet" type="text/css" href="/assets/css/landing.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/changethewords.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper@8/swiper-bundle.min.css"/>
    <link rel="stylesheet" type="text/css" href="/assets/css/store.css">
    
    <style type="text/css">
        .bg-white{
            background: white;
        }
        .mob-shadow{
                box-shadow: rgb(100 100 111 / 30%) 0px 7px 29px 0px;
                position: absolute;
                height: 100%;
                width: 386px;
                background: transparent;
                right: 100px;
                bottom: 0;
                border-top-left-radius: 63px;
                border-top-right-radius: 63px;
        }
        @media (max-width: 767px){
            .mob-pt{
                padding-top: 3em;
            }
            .top-header{
                padding-top: 5em;
            }
            .mob-shadow{
                width: 333px;
                background: transparent;
                right: 20px;
            }
            .dark-on-light.nav-links{
                color: white;
            }
            .btn-white{
                color: black !important;
            }
        }
        
        #changethewords {
            display: inline !important;
        }
        h4.title{
         display: flex;
         justify-content: space-between;
         align-items: center;
         align-content: center;
      }
      .category{
         text-transform: uppercase;
         background: black;
         color: white;
         font-size: 12px;
         padding: 5px 10px;
         border-radius: 10px;
         margin-top: -3px;
      }
      body{
        background: black;
        background-color: black !important;
      }
      .gradient-wrap {
        margin-bottom: -64px;
      }
    span.fa-bars{
        background:rgba(255,255,255,0.9); 
        padding:8px 10px;
        border-radius:20px
    }
    .swiper-wrapper .grid-item{
        width: 100% !important;
        max-width: 100% !important;
    }
    .searchbox{

    }
    .searchfield{
        color: white;
        font-size: 24px;
        font-weight: bold;
        border: none !important;
        border-bottom: 2px solid white;
        background-color: rgba(255, 255, 255, .08);
        backdrop-filter: blur(5px);
        display: block;
        width: 100%;
        height: 100%;
        padding: 20px;
        border-radius: 20px;
    }
    .searchfield::placeholder { /* Chrome, Firefox, Opera, Safari 10.1+ */
  color: white;
  opacity: 1; /* Firefox */
}

:-ms-input-placeholder { /* Internet Explorer 10-11 */
  color: red;
}

::-ms-input-placeholder { /* Microsoft Edge */
  color: red;
}

/* scaffolding */
/* ----------- */



.tt-menu,
.gist {
  text-align: left;
}

/* base styles */
/* ----------- */



.table-of-contents li {
  display: inline-block;
  *display: inline;
  zoom: 1;
}

.table-of-contents li a {
  font-size: 16px;
  color: #999;
}



/* site theme */
/* ---------- */

.title {
  margin: 20px 0 0 0;
  font-size: 64px;
}

.example {
  padding: 30px 0;
}

.example-name {
  margin: 20px 0;
  font-size: 32px;
}

.demo {
  position: relative;
  *z-index: 1;
  margin: 50px 0;
}

.typeahead,
.tt-query,
.tt-hint {

  font-size: 24px;
  line-height: 30px;
  border: 2px solid #ccc;
  -webkit-border-radius: 8px;
     -moz-border-radius: 8px;
          border-radius: 8px;
  outline: none;
}

.typeahead {

}

.typeahead:focus {
  border: 2px solid #0097cf;
}

.tt-query {
  -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
     -moz-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
          box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
}

.tt-hint {
  color: #999
}

.tt-menu {
  width: 422px;
  margin: 12px 0;
  padding: 8px 0;

  border: 1px solid #ccc;
  border: 1px solid rgba(0, 0, 0, 0.2);
  -webkit-border-radius: 8px;
     -moz-border-radius: 8px;
          border-radius: 8px;
  -webkit-box-shadow: 0 5px 10px rgba(0,0,0,.2);
     -moz-box-shadow: 0 5px 10px rgba(0,0,0,.2);
          box-shadow: 0 5px 10px rgba(0,0,0,.2);
}

.tt-suggestion {
  padding: 3px 20px;
  font-size: 18px;
  line-height: 24px;
}

.tt-suggestion:hover {
  cursor: pointer;
  color: #0d6efd !important;

}

.tt-suggestion.tt-cursor {
  color: #fff;


}

.tt-suggestion p {
  margin: 0;
}

.gist {
  font-size: 14px;
}

/* example specific styles */
/* ----------------------- */

#custom-templates .empty-message {
  padding: 5px 10px;
 text-align: center;
}

#multiple-datasets .league-name {
  margin: 0 20px 5px 20px;
  padding: 3px 0;
  border-bottom: 1px solid #ccc;
}

#scrollable-dropdown-menu .tt-menu {
  max-height: 150px;
  overflow-y: auto;
}

#rtl-support .tt-menu {
  text-align: right;
}
.tt-dropdown-menu{
background: white;
    
}
</style>
@endsection

@section("content")
<div id="page-content">
    <div id="backdrop" style="margin-top: -130px; width: 100%; z-index: 1; display: flex; align-items: center; min-height: 800px; background-position: center;  background-size: cover; margin-bottom: -64px; background-image:url('/assets/img/venti-backdrop.jpeg')">
        <div class="gradient-wrap" style="background: transparent; width:100%;">
            <!--  HERO -->
            <div class="section-wrapper black position-relative">
                <div class="row" style="align-items:center;">
                    <div class="col-md-4 col-sm-12 col-12 top-header" style="display: flex; align-items:center;">
                        <div id="hero-text-wrapper">
                          <h1 class="mb-3 section-title" style="color: white; font-weight:600 !important; font-size: 3.5em;">
                            Explore
                          <br />
                          The World
                          </h1>
                        <h3 class="section-subtitle mb-4" style="color: white;">
                            Make Friends. Create Memories. 
                        </h3>
                        @if(Session::get('user') === null)
                            <a class="btn btn-primary" style="display: block; font-size: 13px; border-color: #3b5998; border-radius: 7px; background-color:#3b5998 !important; max-width:200px;" href="/login/facebook/redirect"><i class="fab fa-facebook"></i> LOG IN WITH FACEBOOK</a><br>
                        @endif
                          <a href="https://apps.apple.com/us/app/venti-friends-and-adventure/id1620282387" style="width:200px;" class="btn btn-white"><i class="fab fa-apple mr-1"></i> DOWNLOAD VENTI</a>
                        </div>
                    </div>
                    <!--
                        <div class="col-md-8 mob-pt">
                            <div class="searchbox" id="prefetch">
                                <input class="searchfield typeahead" data-provider="typeahead" type="text" style="width:100%;" placeholder="Where to next?">
                            </div>
                        </div>
                    -->
                </div>
            </div>
        </div>
    </div>
    <div class="gradient-wrap">
        <!--  HERO -->
        <div class="section-wrapper black position-relative mb-4" style="padding: 6em 0;">     
            <div class="row" style="min-height: 400px;">
                <div class="col-md-6" style="align-self: center;">
                    <img src="/assets/img/connect-demo-1.png" style="width:100%;">
                </div>
                <div class="col-md-6 text-left mob-pt" style="display: grid; align-content: center;">
                    <h1 class="mb-3 section-title" style="font-weight:600 !important; font-size: ;">What is Venti?</h1>
                    <h3>A social discovery platform with real people that want to <div id="changethewords">
                        <span data-id="1">travel</span>
                        <span data-id="2">hike</span>
                        <span data-id="6">backpack</span>
                        <span data-id="5">kayak</span>
                        <span data-id="7">surf</span>
                        <span data-id="3">scuba dive</span>
                        <span data-id="4">museum hop</span>
                      </div></h3>
                    <h3 class="section-subtitle mb-4" >
                         Venti helps you find people to go on adventures with. All trips on our platform have no fee. Just cover your own expenses.
                    </h3>
                    <a href="/connect" class="btn btn-primary" style="max-width:180px;">👀 WHO'S ON VENTI</a>
                    <br>
                    <a href="/register">Get Started</a>
                </div>
            </div>
        </div>
    </div>
    <div class="gradient-wrap">
        <div class="row mt-4" style="min-height: 100px;">
            <div class="col-md-12 text-center" style="padding-top: 4em;">
                <h1 class="mb-3 section-title mt-4" style="font-weight:600 !important; font-size: ;">How does it work?</h1>
                <h3 class="section-subtitle">
                    It's Simple. It's Free. Here's the 1-2-3. All trips on Venti have no fee.
                </h3>
            </div>
            
        </div>
        <div class="row mb-3" style="align-items: center; justify-content: center; padding:20px">
                <div class="col-md-3" style="padding:2%;">
                    <div class="card">
                        <div class="card-body text-center">
                            <img src="/assets/img/create-profile.png" style="width: 100% ;max-width:300px;" class="mb-3">
                            <p>Create a profile and tell Venti what you're looking for in a travel group or browse existing group trips.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-3" style="padding:2%;">
                    <div class="card">
                        <div class="card-body text-center">
                            <img src="/assets/img/preferences.png" style="width: 100% ;max-width:300px;" class="mb-3">
                            <p>Venti will deliver recommendations for destinations and travel buddies. Accept our ideas or create your own group.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-3" style="padding:2%;">
                    <div class="card">
                        <div class="card-body text-center">
                            <img src="/assets/img/travel.png" style="width: 100% ;max-width:300px;" class="mb-3">
                            <p>Go forth and conquer! Make friends, share digital postcards, and inspire others.</p>
                        </div>
                    </div>
                </div>
            </div>
    </div>
    <div class="gradient-wrap" style="padding:4em 0;">
        <div class="section-wrapper black position-relative mb-4">
            <h2 class="mb-3 section-title text-center">Upcoming Trips</h2>
            <h3 class="section-subtitle mb-4 text-center">
                See what trips are already being planned.
            </h3>
            <div class="swiper" style="max-width:900px;">
              <!-- Additional required wrapper -->
              <div class="swiper-wrapper"></div>
              <!-- If we need pagination -->
              <div class="swiper-pagination"></div>

              <!-- If we need scrollbar -->
              <div class="swiper-scrollbar"></div>
            </div>
        </div>
        <br>
        <h3 class="section-subtitle mb-4 text-center">
                And there are so many more!
        </h3>
        <a href="/groups" class="btn btn-primary text-center" style="width:150px; margin:0 auto; display:block;margin-bottom: 2em;">VIEW ALL TRIPS</a>
    </div>
    <div class="gradient-wrap bg-white" style="padding: 4em 0; background: whitesmoke !important;">
        <!--  HERO -->
        <div class="section-wrapper black position-relative  mb-4">
            <div class="row mt-4">
                <div class="col-md-6 text-right" style="display: grid; align-content: center;">
                    <h1 class="mb-3 section-title" style="font-weight:600 !important; font-size: ;">Get Paid to Travel</h1>
                    <h3 class="section-subtitle mb-4">
                        Organize adventures as a Venti Navigator, and you can earn up to $500+ per trip. Earn travel points that you can redeem for free trips, flights, and more.
                    </h3>
                    <a href="https://venti.co/navigator" class="btn btn-primary" style="width:150px; margin-left:auto;">LEARN MORE</a>
                    <br><br>
                </div>
                <div class="col-md-6 text-center">
                    <img src="/assets/img/navigator-backdrop-sm.jpg" style="width:100%; max-width:400px;">
                </div>
            </div>
        </div>
    </div>
    
    <div class="gradient-wrap" style="padding: 4em 0; display: none;">
        <!--  HERO -->
        <div class="section-wrapper black position-relative hero-section mb-4">
            <div class="row mt-4">
                <div class="col-md-6 text-right" style="display: grid; align-content: center;">
                    <h1 class="mb-3 section-title" style="font-weight:600 !important; font-size: ;">Share Your Story</h1>
                    <h3 class="section-subtitle mb-4">
                        to inspire and inform other travelers through reviews, tips/suggestions, postcards, and more:
                    </h3>
                    <a class="dark-on-light nav-links btn btn-primary" href="/stories" style="max-width:200px; margin-left:auto;"><i class="fa fa-camera-retro"></i> VIEW STORIES</a>
                </div>
                <div class="col-md-4 text-center mob-pt">
                    <div class="card" style="border-radius:20px">
                          <video autoplay="true" muted="true" loop="true" defaultMuted playsinline  oncontextmenu="return false;"  preload="auto" src="https://firebasestorage.googleapis.com/v0/b/venti-344622.appspot.com/o/sloth-cropped.mp4?alt=media&token=981a838a-706b-4627-99d8-8f86a853b14b" style="width:100%; border-top-left-radius: 20px; border-top-right-radius: 20px;"></video>
                          <div class="card-body">
                             <div class="mb-4" style="display: flex; justify-content:space-between; align-items:center">
                                <a href="/stories" style="display: flex; align-items:center; color: black; width:50%; text-align: left;">
                                   <img src="https://storage.googleapis.com/download/storage/v1/b/venti-344622.appspot.com/o/1468285-Screen-Shot-2022-10-14-at-2.54.08-PM.png?generation=1666015399971376&alt=media" style="width:50px; border-radius:50%;">
                                   <div style="padding-left:10px;">
                                      <span style="display:block; font-weight: bold;">Markus</span>
                                      <span style="display:block; font-size: 12px;">Oct. 11, 2022</span>
                                   </div>
                                </a>
                                <div style="display:flex;" class="like-box" data-story="">
                                   <img src="/assets/img/smile-liked.png " style="width:25px; border-radius:0;">
                                   <span class="count" style="padding-left: 5px;">2</span>
                                </div>
                             </div>
                             <h4 class="title text-left">
                                Finally Saw a Sloth 🙌🏽
                                <span class="category">HIGHLIGHT</span>
                             </h4>
                             <p class="text-left">On our last full day in Costa Rica, our group visited the Refugio Animal de Costa Rica in San José. It did not disappoint. They also provide tours in English.</p>
                             <p style="width:100%; text-align:center"><i class="fa fa-location-dot"></i> San José, Costa Rica</p>
                          </div>
                       </div>
                </div>
            </div>
        </div>
    </div>
    <div class="gradient-wrap bg-white" style="padding: 4em 0;">
        <!--  HERO -->
        <div class="section-wrapper black position-relative mb-4">
            <div class="row mt-4">
                <div class="col-md-6 text-center">
                    <img src="/assets/img/user-safety.png" style="width:100%; max-width:500px;">
                </div>
                <div class="col-md-6 text-left" style="display: grid; align-content: center;">
                    <h1 class="mb-3 section-title mob-pt" style="font-weight:600 !important; font-size: ;">Peace of Mind</h1>
                    <h3 class="section-subtitle mb-4">
                        Our team is committed to ensure everyone has a safe and enjoyable experience on our platform.
                    </h3>
                    <ul>
                        <li>Create fully private groups <i class="fa fa-check"></i></li>
                        <li>Limit who can invite you <i class="fa fa-check"></i></li>
                        <li>User and trip reporting <i class="fa fa-check"></i></li>
                        <li>Email & SMS verification <i class="fa fa-check"></i></li>
                    </ul>
                    <p style="font-size:12px;"><img src="/assets/img/tip.png" style="width:25px;"> Requesting payment from others on our platform is not allowed.</p>
                </div>
            </div>
        </div>
    </div>

    <div class="gradient-wrap bg-white" style="padding: 4em 0;">
        <!--  HERO -->
        <div class="section-wrapper black position-relative hero-section mb-4">
            <div class="row mt-4">
                <div class="col-md-6 text-left" style="display: grid; align-content: center;">
                    <h1 class="mb-3 section-title mob-pt" style="font-weight:600 !important; font-size: ;">Ready When You Are</h1>
                    <h3 class="section-subtitle mb-4">
                        Our app has launched and is ready to help you discover your next adventure.
                    </h3>
                    <a href="https://apps.apple.com/us/app/venti-friends-and-adventure/id1620282387" class="btn btn-primary" style="max-width: 300px"><i class="fab fa-apple mr-1" ></i> Download Venti</a>
                    <br>
                    <p>Coming soon to Android.</p>
                </div>
                <div class="col-md-6 text-center">
                    <img src="/assets/img/mobile-app-2.png" style="width:100%; max-width:400px;">
                </div>
            </div>
        </div>
    </div>
    
    <div class="gradient-wrap">
        <div class="row mt-4" style="min-height: 400px;">
            <div class="col-md-12 text-center" style="padding-top: 4em;">
                <h1 class="mb-3 section-title mt-4" style="font-weight:600 !important; font-size: ;">Free for Everyone</h1>
                <h3 class="section-subtitle">
                    We believe adventuring solo should only be by choice, so we made it free to use Venti
                </h3>
            </div>
        </div>
    </div>
  </div>

@endsection

@section("js")
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/typeahead.js/0.10.4/typeahead.bundle.min.js"></script>
    @if(env('APP_ENV') != "local")
        <!-- Meta Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s)
            {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};
            if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
            n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t,s)}(window, document,'script',
            'https://connect.facebook.net/en_US/fbevents.js');
            fbq('init', '1169331916805166');
            fbq('track', 'PageView');
        </script>
        <noscript><img height="1" width="1" style="display:none"
        src="https://www.facebook.com/tr?id=1169331916805166&ev=PageView&noscript=1"
        /></noscript>
        <!-- End Meta Pixel Code -->
    @endif
    <script type="text/javascript" src="/assets/js/changethewords.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/swiper@8/swiper-bundle.min.js"></script>
    <script>
        $(function() {
          $("#changethewords").changeWords({
            time: 2000,
            animate: "flipInX",
            selector: "span",
            repeat:true
          });
        });
        

        $(document).ready(function() {
            $.ajaxSetup({
                 headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                 }
              });

            toggleLogo("black","white");

            function toggleLogo(color, replaced){
                var logoSRC = $(".logo").attr("src");
                logoSRC = logoSRC.replace(color, replaced);
                console.log(logoSRC)
                $(".logo").attr("src", logoSRC);
            }

            $(".nav-icon").each(function(){
                var src = $(this).attr("src");
                console.log(src)
                src = src.replace('.png', '-white.png');
                $(this).attr("src", src);
              });

            $("#hamburger-menu").click(function(){
                if($("#page_home__show").hasClass("mobile-nav-open")){
                    toggleLogo("white","black");
                } else {
                    toggleLogo("black","white");
                }
                
            });


            getTripsRotatorContent();

            function getTripsRotatorContent(){
                $.ajax({
                    url: "{{ route('groups-browse-ajax', ['limit' => 20, 'cache' => 'N']) }}",
                    type: 'post',
                    processData: true,
                      dataType: 'json',
                        success: function (data) {
                        $.each(data, function(i, item) {
                            console.log(i);
                            var html = '<div class="swiper-slide">' + item + '</div>';
                           $(".swiper-wrapper").append(html);

                        });  
                        $(".groupAvatars").addClass("opened");           
                    }
                });
            }

            var width = $(window).width();
            var numSlides = 3;
            
            if(width < 600){
                numSlides = 1;
            }

            const swiper = new Swiper('.swiper', {
                  // Optional parameters
                  direction: 'horizontal',
                  loop: false,
                  slidesPerView: numSlides,

                  // If we need pagination
                  pagination: {
                    el: '.swiper-pagination',
                  },

                  // Navigation arrows
                  navigation: {
                    nextEl: '.swiper-button-next',
                    prevEl: '.swiper-button-prev',
                  }
            });

            var dataSource = new Bloodhound({
                datumTokenizer: Bloodhound.tokenizers.obj.whitespace('country'),
                queryTokenizer: Bloodhound.tokenizers.whitespace,
                prefetch: {
                    url: "https://apiv1.venti.co/cache/search"
                }
            });


            dataSource.initialize();

            $('.typeahead').typeahead({
                highlight: true
            }, {
                displayKey: 'title',
                source: dataSource.ttAdapter()
            });

            $('.typeahead').bind('typeahead:select', function(ev, suggestion) {
                console.log(ev);
                console.log('Selection: ' + suggestion);
            });

        });
    </script>

@endsection