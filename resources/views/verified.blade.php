@extends('layouts.curator-master')

@section('content')

<div id="page-content">
    <div class="gradient-wrap">
        <div class="container hero-section mb-4 mt-4">
            <div class="row justify-content-center">
                <div class="col-md-12 text-center mt-4">
                    <h1>Success</h1>
                </div>
                <div class="col-md-8">
                     <div class="card">
                        <div class="card-body text-center">
                           <img src="/assets/img/high-five.png" style="width:340px;">
                           <br><br>
                           <h4>Your email has been verified!</h4>
                            @if(Session::get('user') === null)
                                <a class="dark-on-light nav-links btn btn-primary" href="/login">LOG IN</a>
                              @else
                                <a class="dark-on-light nav-links btn btn-primary" href="/groups"><i class="fa-solid fa-person"></i><i class="fa-solid fa-person"></i> BROWSE GROUPS</a>
                            @endif
                        </div>
                     </div>
               </div>
            </div>
        </div>
    </div>
</div>

@endsection