<table class="table" style="font-size: 13px;">
  <thead>
    <tr><th>Prize</th>
    <th>Draw Date</th>
    <th class="text-center">Minimum Cash Balance</th>
  </tr></thead>
  <tbody>
  <tr>
    <td><s>Flight to San Francisco</s></td>
    <td><s>Jan. 19</s></td>
    <td class="text-center"><s>$250</s></td>
  </tr>
  <tr>
    <td><s>Las Vegas Vacation</s></td>
    <td><s>Feb. 2</s></td>
    <td class="text-center"><s>$350</s></td>
  </tr>
  <tr>
    <td><s>Flight to Miami</s></td>
    <td><s>March 8</s></td>
    <td class="text-center"><s>$400</s></td>
  </tr>
  <tr>
    <td><s>Flight to Los Angeles</s></td>
    <td><s>April 5</s></td>
    <td class="text-center"><s>$450</s></td>
  </tr>
  <tr>
    <td><s>Flight to Cancún</s></td>
    <td><s>May 10</s></td>
    <td class="text-center"><s>$500</s></td>
  </tr>
  <tr>
    <td><s>Flight to Vancouver</s></td>
    <td><s>June 7</s></td>
    <td class="text-center"><s>$550</s></td>
  </tr>
  <tr>
    <td><s>Flight to Puerto Rico</s></td>
    <td><s>July 19</s></td>
    <td class="text-center"><s>$600</s></td>
  </tr>
  <tr>
    <td>Orlando Vacation</td>
    <td>September</td>
    <td class="text-center">$700</td>
  </tr>
  </tbody>
</table>