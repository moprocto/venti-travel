@extends("layouts.curator-master")

@section('css')
  <style type="text/css">
    .text-small{
      font-size: 12px;
    }
    
    .enticer{
      display: none;
      position: absolute;
        width: 100%;
        height: 60%;
        margin-left: -1em;
        margin-top: -1em;
        background: rgba(0,0,0,0.05);
        justify-content: center;
        
        align-items: center;
    }
    .image-frame:hover .enticer{
      display: flex;
      transition: 2s;
      cursor: pointer;
    }
    
  </style>
   <script id="visitorscoverage_quote_widget" type="text/javascript" src="https://www.visitorscoverage.com/widgets/quotes/js/main.js"></script>
@endsection

@section('content')
  <div id="page-content">
      <div class="gradient-wrap">
          <div class="container hero-section mb-4 mt-4">
              <div class="row">
                <div class="col-md-10 mt-4">
                      <h1>Sweepstakes</h1>
                      
                      <h5 class="mt-4">Free Flight Fridays</h5>
                      <div class="card bg-white small-shadow">
                        <div class="card-body">
                          <p>Boarding Pass (not Buddy Pass) holders will automatically be entered into random monthly drawings for a free, one-way Economy flight to one of the following destinations:</p>
                          <ul>
                            <li>January: San Francisco</li>
                            <li>March: Miami</li>
                            <li>April: Los Angeles</li>
                            <li>May: Cancún</li>
                            <li>June: Vancouver</li>
                            <li>July: Puerto Rico</li>
                          </ul>
                          <p>On Friday, January 19 at 12:00 a.m. EST, we will begin taking a balance snapshot of all active Priority and First Class Boarding Pass holders.</p>
                          <p>We will calculate the number of entries into the sweepstakes based on the following:</p>
                          <p>Every dollar in your Cash Balance is one entry into the sweepstakes. Must have at least $250 in your Cash Balance at the time of the snapshot to qualify.<br>Every Point in your balance is equal to:</p>
                            <ul>
                              <li>2 entries for Priority</li>
                              <li>4 entries for Business Class</li>
                              <li>8 entries for First Class</li>
                            </ul>
                          <p>The winner will receive an email to accept their flight. Cash equivalent value for each flight may not exceed $300 for one-way and $500 for roundtrip. Winners have 12 months to use their free flight. If a winner declines their prize, they can donate their ticket to charity, pass it to another Boarding Pass member, or accept a $50 Amazon Gift Card. Winner is responsible for the costs of any add-ons or upgrades.</p>
                        </div>
                      </div>
                      <h5 class="mt-4">Sweepstakes Terms</h5>
                      <div class="card bg-white small-shadow">
                        <div class="card-body">
                          <p>All sweepstakes winners and prize recipients must have an active <a href="/boardingpass">Boarding Pass</a> and be a U.S. resident of at least 18 years of age. The prize must be activated within the 2024 calendar year, or within 12 months of receipt. Sweepstakes winners will be posted on our website, including their:</p>
                          <ul>
                            <li>First name</li>
                            <li>Member ID</li>
                            <li>City</li>
                          </ul>
                          <p>The same person cannot win a sweepstakes prize more than once during a calendar year.</p>
                          <p>
                           A completed 1099-MISC is required to receive any sweepstakes prize except gift cards. Venti reserves the right to cancel any sweepstakes and sweepstakes prize delivery. Prizes are subjected to Terms and Conditions of the vendor, and Venti is not responsible for products and services rendered by prize vendors. You will be required to sign a waiver and agree to the terms of the vendors associated with your prize.</p>
                          <p>You can track the number of entries you have in ongoing sweepstakes from your dashboard. Simply look for the <img src="/assets/img/icons/ticket.png" style="width:20px;"> icon next to your member ID.</p>
                          <p>Gift Card recipients will receive their card by December, 2024.</p>
                        </div>
                      </div>
                      <h5 class="mt-4">Our 2024 Schedule</h5>
                      <div class="card bg-white small-shadow">
                        <div class="card-body">
                          @include('sweepstakes.schedule')
                        </div>
                      </div>
                      <h5 class="mt-4">Past Winners</h5>
                      <div class="card bg-white small-shadow">
                        <div class="card-body">
                          <table class="table">
                            <thead>
                              <th>Member</th>
                              <th>Member ID</th>
                              <th>Date Won</th>
                              <th>Prize</th>
                            </thead>
                            <tbody>
                              <tr>
                                <td>
                                  Mark
                                  <span class="d-block text-small">North Carolina</span>
                                </td>
                                <td>NZE035C64</td>
                                <td>January 19, 2024</td>
                                <td>One-Way Flight to San Francisco</td>
                              </tr>
                              <tr>
                                <td>
                                  Chi Wai
                                  <span class="d-block text-small">New York</span>
                                </td>
                                <td>X0ZZ1F54C</td>
                                <td>February 2, 2024</td>
                                <td>Las Vegas Vacation</td>
                              </tr>
                              <tr>
                                <td>
                                  Ja Ryung
                                  <span class="d-block text-small">Washington</span>
                                </td>
                                <td>PF7D361F8</td>
                                <td>March 8, 2024</td>
                                <td>One-Way Flight to Miami</td>
                              </tr>
                              <tr>
                                <td>
                                  Justin
                                  <span class="d-block text-small">Hawaii</span>
                                </td>
                                <td>TRCL2FE74</td>
                                <td>April 5, 2024</td>
                                <td>One-Way Flight to Los Angeles</td>
                              </tr>
                              <tr>
                                <td>
                                  Kevin
                                  <span class="d-block text-small">California</span>
                                </td>
                                <td>NVJ42FFB5</td>
                                <td>May 10, 2024</td>
                                <td>One-Way Flight to Cancún</td>
                              </tr>
                              <tr>
                                <td>
                                  Bin
                                  <span class="d-block text-small">Pennsylvania</span>
                                </td>
                                <td>TWHN23A75</td>
                                <td>June 7, 2024</td>
                                <td>One-Way Flight to Vancouver</td>
                              </tr>
                              <tr>
                                <td>
                                  Chris
                                  <span class="d-block text-small">Minnesota</span>
                                </td>
                                <td>9N5K24A47</td>
                                <td>July 20, 2024</td>
                                <td>Roundtrip Flight to Puerto Rico</td>
                              </tr>
                            </tbody>
                          </table>
                        </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </div>
@endsection

@section('js')
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" integrity="sha512-KfkfwYDsLkIlwQp6LFnl8zNdLGxu9YAA1QvwINks4PhcElQSvqcyVLLD9aMhXd13uQjoXtEKNosOWaZqXgel0g==" crossorigin="anonymous" referrerpolicy="no-referrer" />

 
  <script>
    $(document).ready( function () {
        $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $("#csrf-token").val()
          }
      });
    });
  </script>
@endsection