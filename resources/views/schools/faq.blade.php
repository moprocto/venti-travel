<section id="faqs" class="md-spacing">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 offset-lg-2 col-md-12 section-head text-center">
                <h2 class="text-lg">FAQs</h2>
                <h5 class="section-head-body mb-4">It's in our DNA to be as transparent as possible</h5>
            </div>
        </div>
        <div class="row mt-4">
            <div class="col-md-12 col-lg-8 offset-lg-2">
                <div class="accordion" id="customAccordion">
                    <div class="accordion-item first">
                        <h2 class="accordion-header">
                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                              Is my information shared with anyone? Like my grades? 
                            </button>
                        </h2>
                        <div id="collapseTwo" class="accordion-collapse collapse" data-bs-parent="#customAccordion">
                            <div class="accordion-body">
                                <p>We do not share your information, especially grades, with third parties without your explicit permission. All data is anonymized and encrypted to keep your date safe and private.</p>
                            </div>
                        </div>
                    </div>

                    <div class="accordion-item ">
                        <h2 class="accordion-header">
                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                Do I have to get an "A" to earn points?</button>
                        </h2>
                        <div id="collapseFour" class="accordion-collapse collapse" data-bs-parent="#customAccordion">
                            <div class="accordion-body">
                                <p>No, but you must earn at least 70% ("C" equivalent) or higher on an assignment to receive travel points. You'll only be able to receive travel points for grades received during the Fall 2024 semester and onward. Attendance does not count. See the below table for our points breakdown.</p>
                                <table class="table mb-4">
                                    <thead>
                                        <th></th>
                                        <th colspan="3" class="text-center">Grade</th>
                                    </thead>
                                    <thead class="text-center">
                                        <th class="text-left">Assignment</th>
                                        <th>A</th>
                                        <th>B</th>
                                        <th>C</th>
                                    </thead>
                                    <tbody>
                                        <tr class="text-center">
                                            <td class="text-left">Homework</td>
                                            <td>$3</td>
                                            <td>$2</td>
                                            <td>$1</td>
                                        </tr>
                                        <tr class="text-center">
                                            <td class="text-left">Quiz</td>
                                            <td>$6</td>
                                            <td>$4</td>
                                            <td>$2</td>
                                        </tr>
                                        <tr class="text-center">
                                            <td class="text-left">Exam/Project</td>
                                            <td>$9</td>
                                            <td>$6</td>
                                            <td>$3</td>
                                        </tr>
                                        <tr class="text-center" >
                                            <td class="text-left">Final</td>
                                            <td>$12</td>
                                            <td>$8</td>
                                            <td>$4</td>
                                        </tr>
                                    </tbody>
                                </table>

                                <p>We do not accept grade revisions. Only submit once you have achieved the final result on your assignment. Payouts can change at any time. Calculator is for illustrative purposes only.</p>
                            </div>
                        </div>
                    </div>
                    <div class="accordion-item ">
                        <h2 class="accordion-header">
                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseFiv" aria-expanded="false" aria-controls="collapseFiv">
                                Can I exchange points for cash?</button>
                        </h2>
                        <div id="collapseFiv" class="accordion-collapse collapse" data-bs-parent="#customAccordion">
                            <div class="accordion-body">
                                
                                <p>Not at this time. However, this is something we are strongly considering.</p>
                            </div>
                        </div>
                    </div>
                    <div class="accordion-item ">
                        <h2 class="accordion-header">
                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                                Are any subjects excluded from earning points?</button>
                        </h2>
                        <div id="collapseSix" class="accordion-collapse collapse" data-bs-parent="#customAccordion">
                            <div class="accordion-body">
                                
                                <p>Yes, we do not offer travel points for half or quarter-credit courses. Examples include filler classes, physical education, and extra curricular activities.</p>
                            </div>
                        </div>
                    </div>
                    <div class="accordion-item last">
                        <h2 class="accordion-header">
                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                              How does Venti make money?
                            </button>
                        </h2>
                        <div id="collapseThree" class="accordion-collapse collapse" data-bs-parent="#customAccordion">
                            <div class="accordion-body">
                                
                                <p>Employers and universities are charged a fee to post job, internship, and research positions on our platform. Our system will filter out the best opportunities for you based on your goals and academic performance.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-8 col-md-12 faqs-container offset-lg-2">
                <div class="col-md-12 text-center">
                    <br>
                    <p>Still have questions? Email our CEO, <a href="mailto:markus@venti.co">Markus</a>. <br> Fan mail welcome. Hate mail is not 👎🏽</p>
                </div>      
            </div>
        </div>
    </div>
</section>