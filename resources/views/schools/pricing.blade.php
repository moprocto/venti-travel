<section class="full-width" style="margin-top: 50px; background: linear-gradient(to bottom, rgba(0,0,0,0) 0%, rgba(85, 245, 163, 0.4) 25%, rgba(87, 245, 243, 0.3) 70%, rgba(0,0,0,0) 100%);padding-top:100px; padding-bottom:100px;">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-lg-8 offset-lg-2 text-center">
                <div class="row mb-4">
                    <div class="col-md-12">
                        <h2 class="text-lg">Pricing</h2>
                        <h5>20X value no matter how you slice it</h5>
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col-md-12 col-lg-10 offset-lg-1">
                        <div class="row">
                            <div class="col-md-6 mb-4">
                                <h4 class="mb-4">👍 FREE</h4>
                                <div class="card" style="border-radius:20px;">
                                    <div class="card-body">
                                        <h2>$0<span class="d-block text-small">Per Month</span></h2>
                                        <ul class="pricing">
                                            <li>Up to 6 grade submissions per semester</li>
                                            <li>Limited to resume upload</li>
                                            <li>No ranking analysis</li>
                                            <li>No improvment analysis</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 mb-4">
                                <h4 class="mb-4">💪 OVERACHIEVER</h4>
                                <div class="card" style="border-radius:20px;">
                                    <div class="card-body">
                                        
                                        <h2>$20<span class="d-block text-small">Per Month</span></h2>
                                        <ul class="pricing">
                                            <li>Unlimited grade submissions per semester</li>
                                            <li>Upload detailed resume</li>
                                            <li>Internships, research, and jobs board</li>
                                            <li>See how you rank</li>
                                            <li>Improvement analysis with smart recommendations</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <h4>We only work with employers that are ready to hire international students and are willing sponsor</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>