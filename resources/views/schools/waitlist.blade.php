 <section class="lg-spacing" style="margin-top:50px;">
    <div class="container">
        <div class="row">
            <div class="col-md-10 offset-md-1">
                <div class="row">
                    <div class="col-md-6 mb-4">
                        <h2 class="text-lg" >How Many Points?</h2>
                        <h5 class="mb-4">Full-time students taking at least 12 credits can expect to earn a free flight per semester.</h5>
                        <div class="row">
                            <div class="col-md-6 mb-4">
                                <div class="card card-info curvy-card">
                                    <div class="card-body">
                                        <div class="form-group">
                                            <label class="form-label">Number of Credits</label>
                                            <input inputmode="numeric" type="number" class="form-control required mb-2" id="NumCredits" min="1" step="0.5" max="5" placeholder="12">
                                            <p class="text-small">Per semester</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 mb-4">
                                <div class="card card-success curvy-card">
                                    <div class="card-body">
                                        <p>Estimate</p>
                                        <div class="form-group">
                                            <h3 class="text-light" id="CreditEstimate">$0</h3>
                                        </div>
                                        <p class="text-small">in travel credits per semester<sup>*</sup></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <p class="text-small"><sup>*</sup> The above estimate assumes straight A's. See <a href="#faqs">FAQ section</a> for more info.</p>
                            </div>
                        </div>
                        <div class="orb orb6"></div>
                    </div>
                    <div class="col-md-1" id="waitlist"></div>
                    <div class="col-md-5 text-right mb-4">
                        <div class="row mb-4">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-12">
                                        <h2 class="text-lg">Join Waitlist</h2>
                                        <h5>Prepare for Launch 🚀</h5>
                                    </div>
                                </div>
                                <div class="row mt-4">
                                    <div class="col-md-12 text-left">
                                        <div class="card mt-4" style="border-radius: 20px;">
                                            <div class="card-body">
                                                <form method="POST" action="{{ route('schools-waitlist') }}">
                                                    {{ csrf_field() }}
                                                    <div class="form-group">
                                                        <label class="form-label">Your Email<br><span class="text-small d-block">You must be currently enrolled at a college or university</span></label>
                                                        <input type="email" name="email" class="form-control required" required="" placeholder="someone.awesome@email.com">
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="form-label">Name of Your School</label>
                                                        <input type="text" name="school" class="form-control" placeholder="SciTech University">
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="form-label">Your Country</label>
                                                        <input type="text" name="country" class="form-control" placeholder="Brazil">
                                                    </div>
                                                    <div class="form-group">
                                                        <button type="submit" class="btn btn-success" style="width:100%">Join Waitlist</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>