@extends('layouts.enterprise')

@section('css')
    <link rel="stylesheet" href="https://unpkg.com/tippy.js@5/dist/backdrop.css" />
    <link rel="stylesheet"  href="https://unpkg.com/tippy.js@4/themes/light.css" />
    <link rel="stylesheet" href="https://unpkg.com/tippy.js@4/themes/light-border.css"/>
    <link rel="stylesheet" href="https://unpkg.com/tippy.js@4/themes/google.css"/>
    <link rel="stylesheet" href="https://unpkg.com/tippy.js@4/themes/translucent.css"/>
    <link rel="stylesheet" href="https://unpkg.com/tippy.js@4/animations/scale.css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/switchery/0.8.2/switchery.min.css" integrity="sha512-uyGg6dZr3cE1PxtKOCGqKGTiZybe5iSq3LsqOolABqAWlIRLo/HKyrMMD8drX+gls3twJdpYX0gDKEdtf2dpmw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" type="text/css" href="/assets/css/schools.css">
    <style type="text/css">
    .card-title{
        text-transform: uppercase;
        font-weight: 700 !important;
        font-size: 12px;
    }
    .frost{
        background-color: rgba(255, 255, 255, 0.4);
    }
    ul.pricing{
        list-style: none;
        margin: 0;
        padding: 0;
    }
    ul.pricing li{
        padding: 5px 10px;
        border-bottom: 1px solid rgba(0, 0, 0, 0.1);
        font-size: 13px;
    }
    body section .card.curvy-card{
        border: none !important;
        border-color: transparent !important;
        box-shadow: none !important;
    }
    </style>
@endsection

@section('content')
    <div class="orb orb1"></div>
    <div class="orb orb2"></div>
	<div class="gradient-wrap" style="background: transparent; width:100%; min-height: 600px;">
        <!--  HERO -->
        <div class="section-wrapper black position-relative">
            <div class="row" style="align-items:center;">
                <div class="col-md-8 text-left">
                    <h1 class="text-xl text-left">
                        Earn an <img src="/assets/img/icons/letterA.png" style="width:100px;"> in 
                        <span class="word-carousel text-left">
                            <span class="word-carousel-inner">
                                <span>Math</span>
                                <span>Biology</span>
                                <span>Physics</span>
                                <span>Economics</span>
                                <span>English</span>
                                <span>Comp Sci</span>
                            </span>
                        </span>
                    </h1>
                    <h1 class="hero-text">
                        Get free travel to
                        <span class="word-carousel text-left">
                            <span class="word-carousel-inner2">
                                <span>Japan</span>
                                <span>Brazil</span>
                                <span>the USA</span>
                                <span>Canada</span>
                                <span>Iceland</span>
                                <span>Anywhere</span>
                            </span>
                        </span>
                    </h1>
                    <div class="col-md-12 mt-4">
                        <h5 style="max-width:500px;">We help students with good grades travel for free and get jobs, internships, and research opportunities in the United States.</h5>
                        <br>
                        <a href="#waitlist" class="btn btn-primary btn-lg">Join Waitlist</a>
                        <br>
                    </div>
                </div>
                <div class="col-md-4 fancy-cards">
                    <img class="box-shadow first" src="/assets/img/friends-brazil.jpg">
                    <img class="box-shadow second" src="/assets/img/friends-surf.jpg">
                    <img class="box-shadow third" src="/assets/img/friends-boat.jpg">
                </div>
            </div>
        </div>
    </div>
    <section class="lg-spacing" style="margin-top:0; margin-bottom:95px;">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h2 class="text-lg">How it Works</h2>
                    <h5>Easy enough, right?</h5>
                    <div class="orb orb4"></div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-8 col-md-12 offset-lg-2">
                    <div class="row mt-4">
                        <div class="col-md-4 mb-4">
                            <div class="card curvy-card">
                                <img src="/assets/img/person-laptop.png" style="width:75px; position:absolute; top:-80px; left:0">
                                <div class="card-body text-center">
                                    <img src="/assets/img/icons/student.png" style="width:55px;" class="mb-4 mt-4">
                                    <p>Complete a profile to include your school, degree plan, and career aspirations</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 mb-4">
                            <div class="card curvy-card">
                                <div class="card-body text-center">
                                    <img src="/assets/img/icons/grades.png" style="width:50px;" class="mb-4 mt-4">
                                    <p>Submit your grades online in seconds and earn travel points per assignment, quiz, exam, etc.<span class="d-block text-small"><sup>*</sup>Just the score, not the actual assignments.</span></p>
                                </div>
                                <img src="/assets/img/data-uploading-person.png" class="hide-md" style="width:200px; margin: 0 auto; position:absolute; bottom:-130px; left:30px">
                            </div>
                        </div>
                        <div class="col-md-4 mb-4">
                            <div class="card curvy-card">
                                <img src="/assets/img/woman-airport-2.png" class="hide-md" style="width:95px; position:absolute; top:-150px; left:50%; margin-left:-50px;">
                                <div class="card-body text-center">
                                    <div class="card-body text-center">
                                        <img src="/assets/img/airplane.gif" style="height:75px;">
                                        <p style="margin-bottom:0">1 Point = $1 towards a flight or hotel booked directly on Venti. Points never expire.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="lg-spacing">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-lg-8 offset-lg-2 text-center">
                    <div class="row">
                        <div class="col-md-12">
                            <h2 class="text-lg">Why Grades?</h2>
                            <h5 class="mt-4 mb-4">We believe your brilliance and potential are not adequately reflected in transcripts and GPAs. Your grades are never shared with anyone and will always remain private.</h5>
                        </div>
                    </div>
                    <div class="row mt-4">
                        <div class="orb orb2" style="right:-50px;" ></div>
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-6 mb-4">
                                    <div class="card frost">
                                        <div class="card-body">
                                            <h3><i class="fa fa-briefcase"></i></h3>
                                            <h5>Internships and Jobs</h5>
                                            <p>We'll help you find opportunities where you have the best shot without compromising on your academic and career growth.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 mb-4">
                                    <div class="card frost">
                                        <div class="card-body">
                                            <h3><i class="fa fa-trophy"></i></h3>
                                            <h5>Check Your Rank</h5>
                                            <p>See how you stack up against other students in your school, state, and around the world. You can turn this off if you don't want it.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 mb-4">
                                    <div class="card frost" style="min-height: 100px;">
                                        <div class="card-body">
                                            <h3><i class="fa fa-graduation-cap"></i></h3>
                                            <h5>Scholarships and Grants</h5>
                                            <p>We help connect you to attainable financial aid opportunities that help cover the cost of school, conference attendance, etc. </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 mb-4">
                                    <div class="card frost" style="min-height: 100px;">
                                        <div class="card-body">
                                            <h3><i class="fa-solid fa-arrow-up-right-dots"></i></h3>
                                            <h5>Personal Growth</h5>
                                            <p>We'll share insights on what you should focus on (extracurriculars, skills, etc.) to stand out among your peers.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    @include('schools.pricing')

    @include('schools.waitlist')
    @include('schools.leaderboard')
    <section class="lg-spacing" style="margin-top:50px;">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-lg-6 offset-lg-3">
                    <div class="row mb-4">
                        <div class="col-md-12 text-center">
                            <h2 class="text-lg">Our Team</h2>
                            <h5>We're just building something we wish existed when we were students</h5>
                        </div>
                    </div>
                    <div class="row mt-4">
                        <div class="col-md-6 text-center mb-4">
                            <div class="card curvy-card">
                                <div class="card-body">
                                    <img src="/assets/img/mp-headshot.jpeg" class="mb-4" style="width:100px; border-radius: 50%;">
                                    <h5>Markus, CEO</h5>
                                    <p>UMBC, Georgetown</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 text-center mb-4">
                            <div class="card curvy-card">
                                <div class="card-body">
                                    <img src="/assets/img/ez-headshot.jpg" class="mb-4" style="width:100px; border-radius: 50%;">
                                    <h5>Elliott, CTO</h5>
                                    <p>University of Maryland</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="md-spacing" style="display:none;">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h2 class="text-lg">How Do You Stack Up?</h2>
                    <h5 class="section-head-body mb-4">It's in our DNA to be as transparent as possible</h5>
                </div>
            </div>
        </div>
    </section>
    
    @include('schools.faq')

    <div id="countdown">
        <div id="timer">
          <div id="days"></div>
          <div id="hours"></div>
          <div id="minutes"></div>
          <div id="seconds"></div>
        </div>
        <div class="description">
            LEFT TO <a href="#waitlist">JOIN WAITLIST</a>
        </div>
    </div>
@endsection('content')

@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/switchery/0.8.2/switchery.min.js" integrity="sha512-lC8vSUSlXWqh7A/F+EUS3l77bdlj+rGMN4NB5XFAHnTR3jQtg4ibZccWpuSSIdPoPUlUxtnGktLyrWcDhG8RvA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="https://unpkg.com/popper.js@1"></script>
    <script src="https://unpkg.com/tippy.js@5"></script>
    <script src="https://ricostacruz.com/jquery.transit/jquery.transit.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
    <script>
        $(document).ready(function() {
            $.ajaxSetup({
                 headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                 }
              });

            $("#NumCredits").on("change", function(){
                var credits = $(this).val();
                var points = credits * 10;

                $("#CreditEstimate").text("$" + points);
            });

        });

        $(document).ready(function() {
            var $wordCarousel = $(".word-carousel-inner");
            var wordCount = $wordCarousel.children().length;
            var currentWord = 0;

            function rotateWord() {
                currentWord = (currentWord + 1) % wordCount;
                $wordCarousel.css("transform", `translateY(-${currentWord * 100}%)`);
            }

            setInterval(rotateWord, 2500); // Change word every 2 seconds

            var $wordCarousel2 = $(".word-carousel-inner2");
            var wordCount = $wordCarousel2.children().length;
            var currentWord = 0;

            function rotateWord2() {
                currentWord = (currentWord + 1) % wordCount;
                $wordCarousel2.css("transform", `translateY(-${currentWord * 100}%)`);
            }

            setInterval(rotateWord2, 3500); // Change word every 2 seconds

            // Sample data (replace this with your actual data)
            var schools = [
                {
                    "name": "University of Maryland",
                    "score": "45.3"
                },
                {
                    "name": "Georgetown University",
                    "score": "15.3"
                },
                {
                    "name": "George Washington University",
                    "score": "20.1"
                },
                {
                    "name": "American University",
                    "score": "12.7"
                },
                {
                    "name": "Howard University",
                    "score": "6.6"
                }
            ];

            // Modern, user-friendly color palette
            var colors = [
                '#FF6B6B', // Soft Red
                '#4ECDC4', // Teal
                '#45B7D1', // Sky Blue
                '#FFA07A', // Light Salmon
                '#98D8C8'  // Mint
            ];

            // Prepare data for Chart.js
            var labels = schools.map(school => school.name);
            var data = schools.map(school => parseFloat(school.score));

            // Create the chart
            var ctx = document.getElementById('schoolWaitlistChart').getContext('2d');
            var chart = new Chart(ctx, {
                type: 'pie',
                data: {
                    labels: labels,
                    datasets: [{
                        data: data,
                        backgroundColor: colors,
                        borderColor: 'white',
                        borderWidth: 2
                    }]
                },
                options: {
                    responsive: true,
                    plugins: {
                        legend: {
                            display: false,
                        },
                        title: {
                            display: false,
                            text: 'Schools on Our Waitlist',
                            font: {
                                size: 18
                            }
                        },
                        tooltip: {
                            callbacks: {
                                label: function(context) {
                                    var label = context.label || '';
                                    var value = context.parsed || 0;
                                    return label + ': ' + value + '%';
                                }
                            }
                        }
                    }
                }
            });

            makeTimer();

            function makeTimer() {
                var endTime = new Date("31 October 2024 3:59:49 GMT-04:00");          
                endTime = (Date.parse(endTime) / 1000);

                var now = new Date();
                now = (Date.parse(now) / 1000);

                var timeLeft = endTime - now;

                var days = Math.floor(timeLeft / 86400); 
                var hours = Math.floor((timeLeft - (days * 86400)) / 3600);
                var minutes = Math.floor((timeLeft - (days * 86400) - (hours * 3600 )) / 60);
                var seconds = Math.floor((timeLeft - (days * 86400) - (hours * 3600) - (minutes * 60)));
      
                if (hours < "10") { hours = "0" + hours; }
                if (minutes < "10") { minutes = "0" + minutes; }
                if (seconds < "10") { seconds = "0" + seconds; }

                $("#days").html(days + "<span> DAYS</span>");
                $("#hours").html(hours + "<span> HOURS</span>");
                $("#minutes").html(minutes + "<span> MINUTES</span>");
                $("#seconds").html(seconds + "<span style='padding-right:0'> SECONDS</span>");       
            }

            function createSocialShareButtons() {
              const url = encodeURIComponent('https://venti.co/schools');
              const text = encodeURIComponent("I'll be traveling for free with Venti. Turn your good grades into free travel and job opportunities in the U.S. Join the waitlist via: ");

              return `
                <h5 class="mb-3 text-center">Share on Social Media</h5>
                <div class="social-share-container" style="margin-top: 20px;">
                  <button type="button" class="btn facebook btn-secondary share-btn text-light" style="margin: 5px;"><i class="fab facebook fa-facebook share-btn"></i> </button>
                  <button type="button" class="btn twitter btn-black share-btn " style="margin: 5px;"><img src="/assets/img/icons/twitter-x.png" style="width:10px" class="share-btn twitter"></button>
                  <button type="button" class="btn linkedin btn-primary share-btn " style="margin: 5px;"><i class="fab linkedin fa-linkedin share-btn"></i></button>
                  <button type="button" class="btn reddit btn-warning share-btn " style="margin: 5px;"><i class="fab reddit fa-reddit share-btn"></i></button>
                  <button type="button" class="btn email btn-info share-btn" style="margin: 5px;"><i class="fas email fa-envelope share-btn"></i></button>
                </div>
              `;
            }

            // Function to handle social share button clicks
            function handleSocialShare(platform) {
              const url = encodeURIComponent('https://venti.co/schools');
              const text = encodeURIComponent("I'll be traveling for free with Venti. Turn your good grades into free travel and job opportunities in the U.S.");

              let shareUrl;
              switch(platform) {
                case 'facebook':
                  shareUrl = `https://www.facebook.com/sharer/sharer.php?u=${url}`;
                  break;
                case 'twitter':
                  shareUrl = `https://twitter.com/intent/tweet?url=${url}&text=${text}`;
                  break;
                case 'linkedin':
                  shareUrl = `https://www.linkedin.com/shareArticle?mini=true&url=${url}&title=${text}`;
                  break;
                case 'reddit':
                  shareUrl = `https://www.reddit.com/submit?url=${url}&title=${text}`;
                  break;
                case 'email':
                  window.location.href = `mailto:?subject=Check out Venti&body=${text}%0A%0A${url}`;
                  return;
              }
              window.open(shareUrl, `${platform}-share-dialog`, 'width=626,height=436');
            }

            setInterval(function() { makeTimer(); }, 1000);

            @if($errors->any())
                @php
                    $errorString = "";
                @endphp
                @foreach ($errors->all() as $error)
                    @php $errorString .= " $error"; @endphp
                @endforeach

                swal({
                    icon: 'error',
                    title: 'Oops!',
                    text: "{{ $errorString ?? 'Something went horribly wrong. Contact admin@venti.co.'}}"
                })
            @endif

            @if(session('success') !== null)
                swal({
                  icon: 'success',
                  title: 'Nice work!',
                  text: "You're on the list. Now, invite your classmates so everyone at your school can get Venti free!",
                  content: {
                    element: "div",
                    attributes: {
                      innerHTML: createSocialShareButtons()
                    },
                  },
                    buttons: {
                        confirm: {
                          text: "Close",
                          value: true,
                          visible: true,
                          className: "",
                          closeModal: true
                        }
                    }
                }).then(() => {
                    
                });
            @endif 

            document.addEventListener('click', function(event) {
              if (event.target.classList.contains('share-btn')) {
                const platform = event.target.classList[1]; // Get the second class name
                console.log(platform)
                handleSocialShare(platform);
              }
            });
        });
    </script>
@endsection