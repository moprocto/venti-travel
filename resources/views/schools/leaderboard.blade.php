<section class="lg-spacing" style="margin-top:50px;">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-lg-6 offset-lg-3">
                <div class="row mb-4">
                    <div class="col-md-12 text-center">
                        <div class="orb orb5"></div>
                        <h2 class="text-lg text-center">Leaderboard</h2>
                        <h5 class="mb-4">We have a special surprise for the school with the most waitlist signups by November 1</h5>
                    </div>
                    <div class="col-md-12 mt-4 mb-4">
                        <div class="row">
                            <div class="col-md-6">
                                <canvas id="schoolWaitlistChart"></canvas>
                            </div>
                            <div class="col-md-6">
                                <ul class="leaderboard">
                                    <li><span class="badge badge-1">1</span> University of the Philippines 🇵🇭</li>
                                    <li><span class="badge badge-2">2</span> University of Maryland 🇺🇸</li>
                                    <li><span class="badge badge-3">3</span> Federal Polytechnic Nekede 🇳🇬</li>
                                    <li><span class="badge badge-4">4</span> Tshwane Univ. of Technology 🇿🇦</li>
                                    <li><span class="badge badge-5">5</span> UNIFATECIE: Centro Univ. 🇧🇷</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8 offset-md-2 mt-4 text-center">
                        <a href="#waitlist" class="btn btn-primary btn-lg">Join Waitlist</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>