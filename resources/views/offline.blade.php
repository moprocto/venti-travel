<!doctype html>
<html lang="en">
<head>
	<title>Offline</title>
</head>
	<body>
		<div style="width: 100%;">
			<div style="display: block; width: 330px; margin:auto;">
				<h2>Closing Operations</h2>
				<h4>We regret to inform you that Venti will be closing operations on Feb. 25, 2025.</h4>
				<br>
				<p>Despite aggressive efforts and partnership negotiations that fell through, we have made the difficult decision to shut down our service. Those with a Points balance will be offered a buyout option in the coming months. We apologize for the inconvenience and thank you for giving us a shot.</p>
				<p>Our support line will remain active during the remainder of the period via admin@venti.co</p>
			</div>
		</div>
	</body>
</html>