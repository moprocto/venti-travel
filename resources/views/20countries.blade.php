@extends('layouts.curator-master')

@php
    $selectable = false;
@endphp

@section('css')
    <style type="text/css">
        html{
            overflow-x: hidden;
        }
        @media(max-width: 1368px ){
            .tb-full-width{
                width: 100%;
            }
        }
        @media(max-width: 768px ){

        }
        .hamburger-menu-list li a{
            color: white;
        }
        .mobile-nav-open .hamburger-menu-list li a{
            color: black;
        }
        .btn-white{
            color: black !important;
        }
        .mobile-nav-open #backdrop{
            margin-top: 0 !important;
        }
        .about-body{
            justify-content: center;
        }
        @media(min-width: 1367px){
            {
                margin-left: 22px;
            }
            .row.about-body{
                margin-bottom: 22px;
            }
            .desktop-hide{
                display: none;
            }
        }
        @media(max-width: 800px){
            .mb-pd-20{
                padding-bottom: 20px;
            }
            .gradient-wrap{
                margin-top: 100px;
            }
            .sm-text-center{
                text-align: center !important;
            }
        }
        @media(max-width: 1368px){
            .mobile-block{
                display: block !important;
            }
            .mobile-hide{
                display: none;
            }
            #about .about-body .about-body-item .about-body-icon-cnt{
                justify-content: left !important;
                margin-bottom: 10px;
            }
            .pills-item{
                background-position-y: inherit !important;
            }
            .sign-up-box{
                padding-top: 80px;
                padding-bottom:  0;
            }
        }
        .about-body-icon-cnt{
            border: 1px solid black;
            border-radius: 20px;
            padding: 5px 10px;
        }
        

        .font-light{
            font-weight: 300 !important;
        }
        .full-width{
            max-width: 9999px !important;
            padding-left:  0;
            padding-right: 0;
        }
        #about{
            padding-top: 0;
        }
        .pd-20{
            padding: 80px 40px;
        }
        i.text-small{
            font-size: 16px;
        }
        .about-body-icon-cnt, .about-body-text-head{
            padding-left: 14px;
        }
        .about-body-text-head{
            padding-right: 15px;
            font-weight: bold;
        }
        @media(min-width: 1367px){
            {
                margin-left: 22px;
            }
            .row.about-body{
                margin-bottom: 22px;
            }
            .desktop-hide{
                display: none;
            }
        }
        #bg-video{
            width: 100%;
        }
        @media(max-width: 768px){
            .mobile-hide{
                display: none;
            }
            #bg-video{
                height: 100%;
                width: fit-content;
            }
        }
        @media(max-width: 468px){
            #minutes, #seconds{
                display: none;
            }
            .card-preview{
                margin-top: 50px;
                text-align: center !important;
            }
            .card-preview img{
                max-width: 350px;
            }
        }   
        @media(max-width: 1368px){
            .mh-500-sm{
                max-height: 500px;
            }
            .mobile-block{
                display: block !important;
            }
            .mobile-hide{
                display: none;
            }
            #about .about-body .about-body-item .about-body-icon-cnt{
                justify-content: left !important;
                margin-bottom: 10px;
            }
            .pills-item{
                background-position-y: inherit !important;
            }
            .sign-up-box{
                padding-top: 80px;
                padding-bottom:  0;
            }
            .preview-mh{
                width: 143% !important;
                margin-left: -73px;
            }
        }

        .text-gold{
            color: #9D8A7C !important;
        }

        .text-large{
            font-size: 20px;
        }

        ol.pills-list{
            display: block;
            padding: 0 0 0 26px;
            list-style: none;
            overflow: hidden;
        }

        .pills-list li{
            display: flex;
            justify-content: space-between;
            align-items: center;
            margin-bottom: 10px;
            padding: 10px 20px 5px 20px;
        }
        .sand-badge{
            background-color: rgba(157, 138, 124, 0.7);
            opacity: 0.8;
            color: white;
            font-size: 22px;
            border-radius: 50%;
            width: 42px;
            height: 40px;
            letter-spacing: 1;
        }
        .pills-item{
            padding: 7px 20px;
            border-radius: 10px;
            margin-bottom: 20px;
            background-color: whitesmoke;
        }
        .pills-item div{
            padding-top: 10px;
        }
        .pills-item p{
            padding-top: 3px;
        }
        .pills-item-1{
            background-position-y: 0;
        }

        .pills-item-2{
            background-position-y: -90px;
        }
        .pills-item-3{
            background-position-y: -180px;
        }
        .pills-item-4{
            background-position-y: -220px;
        }
        li.pills-item-3{
            background: #d7d0ca;
        }
        #mc_embed_signup .text-small{
            font-size: 12px !important;
        }

        #subscribe .text-small{
            margin-top: 15px;
            
        }
        .about-body-item{
            margin-bottom: 10px;
        }.slider-horizontal{
            width: 100% !important;
        }
        #faqs
        {
        padding-bottom: 100px;
        }
        #faqs .faqs-container .faq
        {
            border-top: 1px solid var(--color-border);
            padding: 15px;
        }
        #faqs .faqs-container .faq .faq-head
        {
            padding: 15px 0px;
            position: relative;
        }
        #faqs .faqs-container .faq .faq-head h5
        {
            width: 90%;
            cursor: pointer;
        }
        #faqs .faqs-container .faq .faq-head h5::before
        {
            content: 'keyboard_arrow_down';
            text-align: center;
            font-size: 20px;
            line-height: 27px;
            font-family: 'Material Icons Round';
            height: 25px;
            width: 25px;
            color: #fefefe;
            background: var(--color-sec);
            position: absolute;
            top: 50%;
            right: 0;
            transform: translateY(-50%);
            border-radius: 50%;
            transition: 0.5s;
        }
        #faqs .faqs-container .faq .faq-head h5[aria-expanded=true]::before
        {
            content: 'keyboard_arrow_up';
        }
        .form-container .emailoctopus-form-wrapper .emailoctopus-success-message{
            color:  orange !important;
            font-size: 18px;
            padding-bottom: 20px;
        }
        .accordion-item{
            margin-bottom: 10px;
        }
        #timer{
            margin-top: -2px;
            font-weight: bold;
            display: flex;
            justify-content: space-between;
        }
        #timer{
            align-items: center;
        }
        #timer *{
            padding-right: 4px;
        }

        .about-body-icon-cnt{
            border: 1px solid rgba(0,0,0,0.1);
        }
        .footnote{
            width: 30px;
            display: inline-block;
        }
        html body{
            background: #F2F2F6 !important;
        }
        .features ul {
            list-style: none;
            padding: 0;
        }
        .features ul li{
            padding: 10px;
            border-bottom: 1px solid whitesmoke;
            font-size: 13px;
        }
        .text-bold{
            font-weight: bold;
        }
        .pills-item-1{
            background-color: rgba(190, 223, 230, 1) !important;
        }
        .pills-item-2{
            background-color: rgba(190, 223, 230, 0.6) !important;
        }
        .pills-item-3{
            background-color: rgba(190, 223, 230, 0.4) !important;
        }
        .pills-item-4{
            background-color: rgba(190, 223, 230, 0.2) !important;
        }
        .slide.logos{
            display: flex;
            flex-direction: row;
            justify-content: center;
            align-items: center;
        }
        .slide.logos img{
            width: 100px;
        }

        @keyframes slide {
          from {
            transform: translateX(0);
          }
          to {
            transform: translateX(-100%);
          }
        }

        .logos {
          overflow: hidden;
          padding: 60px 0;
          background: white;
          white-space: nowrap;
          position: relative;
        }

        .logos:before, .logos:after {
          position: absolute;
          top: 0;
          width: 250px;
          height: 100%;
          content: "";
          z-index: 2;
        }

        .logos:before {
          left: 0;
          background: linear-gradient(to left, rgba(255, 255, 255, 0), white);
        }

        .logos:after {
          right: 0;
          background: linear-gradient(to right, rgba(255, 255, 255, 0), white);
        }

        .logos:hover .logos-slide {
          animation-play-state: paused;
        }

        .logos-slide {
          display: inline-block;
          animation: 38s slide infinite linear;
        }

        .logos-slide img {
          width: 90px;
          height: 90px;
          margin: 0 40px;
        }
    </style>
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/11.0.2/css/bootstrap-slider.min.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons+Round" rel="stylesheet">
    <!-- Main CSS -->
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>

@endsection

@php
    $vips = env('VIPS');
    $vip = number_format($vips, 0);
    $counter = $vips * 20;
@endphp

@section('content')
<div id="backdrop" style="margin-top: -130px; width: 100%; z-index: 1; display: flex; align-items: center; min-height: 800px; background-position: center;  background-size: cover; margin-bottom: -64px; overflow:hidden;">
    <div style="position: absolute; z-index: 2; top: 0; background-color: rgba(0,0,0,0.3); width:100%; height: 100%;"></div>
    <div style="position: absolute; z-index: 1; top: 0; width: 100%; height:100%; overflow:hidden;">
        <video  autoplay muted loop id="bg-video" style="position:absolute;  overflow-y: hidden;" >
            <source src="/assets/video/montage.webm" type="video/webm">
        </video>
    </div>
        <div class="gradient-wrap" style="background: transparent; width:100%;">
            <!--  HERO -->
            <div class="section-wrapper black position-relative" style="z-index: 3;">
                <div class="row" style="align-items:center;">
                    <div class="col-lg-5 col-md-12 col-sm-12 col-12 top-header" style="display: flex; align-items:center;">
                        <div id="hero-text-wrapper" style="max-width:900px;">
                            <h1 class="mb-3 section-title" style="line-height: 3.5rem; color: white; font-weight:600 !important; font-size: 1.8rem; margin-top:100px">
                            It's Your Turn to Visit
                            <span style="font-size: 2.45em; display:block;">20 Countries</span>
                            </h1>
                            <h2 class="section-subtitle mb-4" style="color: white; max-width:900px;">
                            Starting at $200 per month. Flights and accommodation included.</h2>
                         <p class="section-head-body" style="color:white;"><strong></strong></p>
                         <a href="#waitlist" class="btn btn-primary">Join Waitlist</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<!-- Cover End -->
 <!-- About Start -->

    <section id="" style="padding-top: 50px; padding-bottom: 100px; background-color:white">
        <div class="container">
            <div class="row" style="max-width:950px; justify-content: center; margin:0 auto;">
                <div class="col-md-12  text-center pd-20 text-center" style="padding-bottom: 0; margin-bottom: -3px; padding:40px 20px;" >
                    <h2 class="section-head-title text-dark mb-3 section-title" style="margin:40px; margin-top:20px">A Travel Subscription</h2>
                    <h5>How it Works</h5>
                </div>
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-body text-center">
                            <img src="/assets/img/icons/countries.png" class="mb-4" style="width:100%; max-width:50px;">
                            <h5>Pick Your Countries</h5>
                            <p>You pick 20 countries, and we'll organize your list to maximize savings</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-body text-center">
                            <img src="/assets/img/icons/subscription.png" class="mb-4" style="width:100%; max-width:50px;">
                            <h5>Confirm Payment Plan</h5>
                            <p>Accept our recommendation and start making monthly payments.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-body text-center">
                            <img src="/assets/img/icons/travel-luggage.png" class="mb-4" style="width:100%; max-width:50px;">
                            <h5>Pack Your Bags</h5>
                            <p>We'll give you travel dates and handle all bookings for you. We work with your schedule.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 text-center">
                    <br><br>
                    <h5>Our plan guarantees 20 countries regardless of future price increases</h5>
                </div>
                
            </div>
            
        </div>
    </section>
    <!-- About End -->

    <!-- End Preview -->

    <section id="about" style="padding-bottom: 0;">
        <div class="container full-width">
            <div class="row" style="background-color: #f7f9ff;">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 text-center pd-20" style="padding-bottom: 0; margin-bottom: -3px; padding:40px 80px;" >
                    <h2 class="section-head-title text-dark mb-4 section-title" style="margin:60px; margin-top:20px">Here for You</h2>
                    <div class="row text-dark text-center " style="justify-content:center;">
                        <div class="col-md-6 ">
                            <div class="pills-item pills-item-1">
                                <h5 class="mt-3">Cutting-Edge Tech</h5>
                                <p>Our technology aggregates data from over 40 different sources to maximize savings</p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="pills-item pills-item-2">
                                <h5 class="mt-3">Stress Free</h5>
                                <p>You only need to worry about what goes into your suitcase and when to fly.</p>
                            </div>
                        </div>
                    </div>
                    <div class="row text-dark text-center " style="justify-content:center;">
                        <div class="col-md-6 ">
                            <div class="pills-item pills-item-3">
                                <h5 class="mt-3">Flexible</h5>
                                <p>Swap out countries whenever you like. You're not restricted to what's on your list.</p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="pills-item pills-item-4">
                                <h5 class="mt-3">Bring a Friend</h5>
                                <p>Add a plus one to your trip, and we'll take up to $250 off their flights when they book through Venti and travel with you.</p>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <p class="mt-4">Your payment plan is individualized. Minimum payments are made over 48-72 months. Cancel anytime and receive a full refund before booking. For your safety, certain destinations are not available through our program.</p>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 full-width">
                    <img src="/assets/img/bring-a-friend.jpg" style="width: 100%;" class="">
                </div>
            </div>
        </div>
    </section>

    <!-- Start FAQs -->

    <section id="" style="padding-bottom:50; background-color:white;">
        <div class="container full-width">
            <div class="row" id="waitlist">
                <div class="col-md-12 text-center" style="padding:90px 20px; padding-bottom:0;">
                    <h3>We Use Math to Save You</h3>
                    <h2 class="section-head-title text-dark mb-3 section-title" style="margin:40px; margin-top:20px">Save Up to 50%</h2>
                    <p style="max-width:800px; margin:0 auto; font-size:1.2em">Compared to visiting countries individually or in random order. You'll be glad you did this.<br><br>We'll email you when it's your turn to select your destinations.</p>
                    <br>
                    <div>
                        <input type="email" class="form-control d-block" placeholder="someone@somewhere.com" id="waitlistEmail" style="width: 300px; margin:0 auto;">
                        <br>
                        <button id="joinWaitlist" class="btn btn-primary"><i class="fa fa-sync fa-spin"></i> Join Waitlist</button>
                    </div>
                    <br>
                </div>
            </div>
        </div>
    </section>
    <!-- End FAQs -->
    

    <section><div class="container full-width" style="background-image:url('/assets/img/boarding-pass-bg5.jpg'); background-position: center; background-size:cover; padding-bottom:100px; padding-top:100px"><div class="row"><div class="col-md-12 text-center pd-20 full-width" style="margin-bottom: -3px; padding:40px; justify-content: center; padding-bottom: 0;"><h4 class="text-light mb-3">Travelers are earning 9% APY right now<br><span class="text-light pt-4 d-block" style="font-size: 1.5em;">With our high-yield savings account</span></h4><h2 class="section-head-title text-dark mb-3 section-title" style="font-size: 4rem; display: none;margin-top:20px; color:white !important">$<span class="" id="currency">10</span></h2><h5 class="section-head-body" style="color:white; max-width: 800px; justify-content:center; margin: 0 auto;">Join our fast-growing community of over travel savers</h5></div></div><div class="row" style="justify-content: center;"><a href="/boardingpass" class="btn btn-primary mt-4 mb-4">Learn More</a></div><br></div></section>
    
    <section>
        <div class="container pt-4">
            <div class="row">
                
            </div>
        </div>
    </section>
@endsection


    
@section("js")
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/typeahead.js/0.10.4/typeahead.bundle.min.js"></script>
    @if(env('APP_ENV') != "local")
        <!-- Meta Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s)
            {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};
            if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
            n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t,s)}(window, document,'script',
            'https://connect.facebook.net/en_US/fbevents.js');
            fbq('init', '1169331916805166');
            fbq('track', 'PageView');
        </script>
        <noscript><img height="1" width="1" style="display:none"
        src="https://www.facebook.com/tr?id=1169331916805166&ev=PageView&noscript=1"
        /></noscript>
        <!-- End Meta Pixel Code -->
    @endif
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/11.0.2/bootstrap-slider.min.js"></script>
    <script>
        $(document).ready(function() {
            $.ajaxSetup({
                 headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                 }
              });

            var interactable = false; 

             
            /**
             * Format the number, so it will be seperated by comma
             */
            function commaSeparateNumber(val){
                while (/(\d+)(\d{3})/.test(val.toString())){
                  val = val.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
                }
                return val;
            }

            toggleLogo("black","white");

            function toggleLogo(color, replaced){
                var logoSRC = $(".logo").attr("src");
                logoSRC = logoSRC.replace(color, replaced);
                console.log(logoSRC)
                $(".logo").attr("src", logoSRC);
            }

            $.fn.isInViewport = function() {
              var elementTop = $(this).offset().top;
              var elementBottom = elementTop + $(this).outerHeight();

              var viewportTop = $(window).scrollTop();
              var viewportBottom = viewportTop + $(window).height();

              return elementBottom > viewportTop && elementTop < viewportBottom;
            };

            


            function isElementVisible($elementToBeChecked)
            {
                var TopView = $(window).scrollTop();
                var BotView = TopView + $(window).height();
                var TopElement = $elementToBeChecked.offset().top;
                var BotElement = TopElement + $elementToBeChecked.height();
                return ((BotElement <= BotView) && (TopElement >= TopView));
            }

            

            $("#joinWaitlist").click(function(){
                var btn = $(this);
                btn.addClass("disable-while-loading");
                var email = $("#waitlistEmail").val();
                

                if(email == "" || email == undefined){
                    $("#waitlistEmail").addClass("invalid-feedback");
                    btn.removeClass("disable-while-loading");
                    return swal({
                        icon: 'error',
                        title: 'Email is required'
                    });
                }

                

                $.ajax({
                    url: "{{ route('jetbridge-join-waitlist') }}",
                    processData: true,
                    dataType: 'json',
                    type: 'post',
                    data: {
                        email: email,
                        product: "twentycountries",
                        income: parseInt($("#income").val()),
                        rent: parseInt($("#rent").val()),
                        spending: parseInt($("#spending").val())
                    },
                    success: function (data) {
                        if(data == 200){
                            btn.removeClass("disable-while-loading");
                            $("#waitlistEmail").val("");
                            swal({
                                icon: 'success',
                                title: "You're now VIP",
                                text: "We sent a confirmation email to " + email + " and outlined some next steps."
                            });
                        }
                        else {
                            btn.removeClass("disable-while-loading");
                            swal({
                                icon: 'warning',
                                title: "There was a problem",
                                text: "The email is either invalid or already on our VIP list. If you believe this was an error, please try again. Otherwise, please contact us at admin@venti.co."
                            });
                        }
                    },
                    error: function(data){
                        swal({
                            icon: 'warning',
                            title: "There was a problem",
                            text: "We could not establish a connection with our registration system. Please try disabling ad blockers or checking your internet subscriptions. If issues persist, please contact us at admin@venti.co."
                        });
                        btn.removeClass("disable-while-loading");
                    }
                });
            });

            $("#calcFreeTravel").click(function(){
                var btn = $(this);
                btn.addClass("disable-while-loading");
                var income = parseInt($("#income").val()) * 12 * .0011;
                var rent = parseInt($("#rent").val()) * 12 * .0022;
                var spending = parseInt($("#spending").val()) * 12 * .0033;

                var points = income + rent + spending;
                
                var numFlights = Math.round(points / 50);
                var numNights = Math.round(points / 30);
                
                

                $("#results").html("<h4 class='mt-4 mb-4'>Each Year You'll Earn</h4><div class='row mt-4' style='justify-content:center;'><div class='col-md-4'><div class='card'><div class='card-body'><h3><i class='fa fa-plane'></i></h3>Up to " + numFlights + " flight(s)</div></div></div><div class='col-md-4'><div class='card'><div class='card-body'><h3><i class='fa fa-hotel'></i></h3>Up to " + numNights + " hotel nights</div></div></div><div class='col-md-12 mt-4'><br>Accelerate your travel goals when you combine our rewards debit card <br> with our 9% APY <a href='/boardingpass'>travel savings account</a>.</div></div></p>");

                btn.removeClass("disable-while-loading");

            });


            $(".nav-icon").each(function(){
                var src = $(this).attr("src");
                console.log(src)
                src = src.replace('.png', '-white-boarding.png');
                $(this).attr("src", src);
              });

            $("#hamburger-menu").click(function(){
                if($("#page_home__show").hasClass("mobile-nav-open")){
                    toggleLogo("white","black");
                } else {
                    toggleLogo("black","white");
                }
                
            });
    


          

            @if(Session::get('subscribeError') !== null)
                swal({
                    icon: 'error',
                    title: 'Oops!',
                    text: 'We ran into a problem and could not add you to our list. Try again or send an email to markus@venti.co'
                })
            @endif

            makeTimer();

            function makeTimer() {

            var endTime = new Date("31 December 2024 23:59:49 GMT-04:00");          
            endTime = (Date.parse(endTime) / 1000);

            var now = new Date();
            now = (Date.parse(now) / 1000);

            var timeLeft = endTime - now;

            var days = Math.floor(timeLeft / 86400); 
            var hours = Math.floor((timeLeft - (days * 86400)) / 3600);
            var minutes = Math.floor((timeLeft - (days * 86400) - (hours * 3600 )) / 60);
            var seconds = Math.floor((timeLeft - (days * 86400) - (hours * 3600) - (minutes * 60)));
  
            if (hours < "10") { hours = "0" + hours; }
            if (minutes < "10") { minutes = "0" + minutes; }
            if (seconds < "10") { seconds = "0" + seconds; }

            $("#days").html(days + "<span> DAYS</span>");
            $("#hours").html(hours + "<span> HOURS</span>");
            $("#minutes").html(minutes + "<span> MINUTES</span>");
            $("#seconds").html(seconds + "<span style='padding-right:0'> SECONDS</span>");       

    }

    setInterval(function() { makeTimer(); }, 1000);

        });


    document.getElementById('bg-video').play();

    </script>

@endsection