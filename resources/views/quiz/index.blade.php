@extends('layouts.curator-master')

@section('css')
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper@9/swiper-bundle.min.css" />
    <link rel="stylesheet" type="text/css" href="/assets/css/steps.css">
    <style>
    	.wizard>.content>.body{
            height: 100%;
            position: relative;
        }
        .wizard>.content{
            min-height: 120px;
        }
        .wizard .steps{
            display: none;
        }
        .wizard>.content>.body ul.response{
        	list-style: none !important;
        	display: flex;
        	justify-content: center;
        }
        .wizard>.content>.body ul.response li{
        	min-width: 75px;
		    text-align: center;
		    justify-content: center;
		    display: flex;
		    padding: 10px 5px;
        }
        .wizard>.content>.body ul.response p{
        	font-size: 2em;
        }
        .wizard>.content>.body ul.response li:hover{
        	background: whitesmoke;
        	border-radius: 10px;
        	cursor: pointer;
        }
        .wizard>.content>.body ul.response li.active{
        	background: #90ee90;
        	border-radius: 10px;
        	cursor: pointer;
        	transition: 0.5s;
        }

        .wizard>.content>.body ul.response li input{
        	margin: 0 auto;
        }
        section img.mb-4{
        	border-radius: 20px;
        }
        section h4{
        	font-size: 1.2em;
        }
        .swiper {
      width: 240px;
      height: 320px;
    }

    .swiper-slide {
      display: flex;
      align-items: center;
      justify-content: center;
      border-radius: 18px;
      font-size: 22px;
      font-weight: bold;
      color: #fff;
    }

    .swiper-slide:nth-child(1n) {
      background-color: rgb(206, 17, 17);
    }

    .swiper-slide:nth-child(2n) {
      background-color: rgb(0, 140, 255);
    }

    .swiper-slide:nth-child(3n) {
      background-color: rgb(10, 184, 111);
    }

    .swiper-slide:nth-child(4n) {
      background-color: rgb(211, 122, 7);
    }

    .swiper-slide:nth-child(5n) {
      background-color: rgb(118, 163, 12);
    }

    .swiper-slide:nth-child(6n) {
      background-color: rgb(180, 10, 47);
    }

    .swiper-slide:nth-child(7n) {
      background-color: rgb(35, 99, 19);
    }

    .swiper-slide:nth-child(8n) {
      background-color: rgb(0, 68, 255);
    }

    .swiper-slide:nth-child(9n) {
      background-color: rgb(218, 12, 218);
    }

    .swiper-slide:nth-child(10n) {
      background-color: rgb(54, 94, 77);
    }
    #instructions{
    	font-size:14px;
    }
	</style>
@endsection

@section('content')
	<div id="page-content">
	    <div class="gradient-wrap">
	        <div class="container hero-section mb-4 mt-4">
	            <div class="row justify-content-center">
	                <div class="col-md-12 text-center mt-4">
	                    <h2>What's Your Travel Personality?<br><span id="instructions">Take our free quiz to find out</span></h2>
	                </div>
	                <div class="col-md-6">
		                <form method="POST">
		                	@CSRF
		                	<div class="card" style="border-radius:20px">
		                		<div class="card-body text-center">
		                			<div id="example-basic">
			                			<h3></h3>
			                			<section>
				                			<img class="mb-4" src="/assets/img/friends-hiking.jpg" style="width:100%;">
				                			<p><strong>Vote 👎, 😐, or 👍</strong><br> if you agree or disagree with each statement</p>
				                			<p>Your results will be revealed at the end</p>
			                			</section>
			                			<h3></h3>
			                			<section>
			                				<img class="mb-4" src="/assets/img/snake.jpg" style="width:100%;">
			                				<h4>I would be down to pose with a snake!</h4>
			                				<ul class="response">
			                					<li class="choice">
			                						<div>
			                							<p>👎</p>
			                							<input type="radio" name="pic1" value="-1" data-cat="adventure" class="selection">
			                						</div>
			                					</li>
			                					<li class="choice">
			                						<div>
			                							<p>😐</p>
			                							<input type="radio" name="pic1" value="0" data-cat="adventure" class="selection">
			                						</div>
			                					</li>
			                					<li class="choice">
			                						<div>
			                							<p>👍</p>
			                							<input type="radio" name="pic1" value="1" data-cat="adventure" class="selection">
			                						</div>
			                					</li>
			                				</ul>
			                			</section>
			                			<h3></h3>
			                			<section>
			                				<img class="mb-4" src="/assets/img/hotel.jpg" style="width:100%;">
			                				<h4>I prefer to stay in a luxury accomodation.</h4>
			                				<ul class="response">
			                					<li class="choice">
			                						<div>
			                							<p>👎</p>
			                							<input type="radio" name="pic2" value="-1" data-cat="spending" class="selection">
			                						</div>
			                					</li>
			                					<li class="choice">
			                						<div>
			                							<p>😐</p>
			                							<input type="radio" name="pic2" value="0" data-cat="spending" class="selection">
			                						</div>
			                					</li>
			                					<li class="choice">
			                						<div>
			                							<p>👍</p>
			                							<input type="radio" name="pic2" value="1" data-cat="spending" class="selection">
			                						</div>
			                					</li>
			                				</ul>
			                			</section>
			                			<h3></h3>
			                			<section>
			                				<img class="mb-4" src="/assets/img/touristy.jpg" style="width:100%;">
			                				<h4>I prefer to stick to popular tourist spots.</h4>
			                				<ul class="response">
			                					<li class="choice">
			                						<div>
			                							<p>👎</p>
			                							<input type="radio" name="pic3" value="-1" data-cat="classic" class="selection">
			                						</div>
			                					</li>
			                					<li class="choice">
			                						<div>
			                							<p>😐</p>
			                							<input type="radio" name="pic3" value="0" data-cat="classic" class="selection">
			                						</div>
			                					</li>
			                					<li class="choice">
			                						<div>
			                							<p>👍</p>
			                							<input type="radio" name="pic3" value="1" data-cat="classic" class="selection">
			                						</div>
			                					</li>
			                				</ul>
			                			</section>
			                			<h3></h3>
			                			<section>
			                				<img class="mb-4" src="/assets/img/restaurant.jpg" style="width:100%;">
			                				<h4>I prefer eating at quality restaurants during my trip</h4>
			                				<ul class="response">
			                					<li class="choice">
			                						<div>
			                							<p>👎</p>
			                							<input type="radio" name="pic4" value="-1" data-cat="spending" class="selection">
			                						</div>
			                					</li>
			                					<li class="choice">
			                						<div>
			                							<p>😐</p>
			                							<input type="radio" name="pic4" value="0" data-cat="spending" class="selection">
			                						</div>
			                					</li>
			                					<li class="choice">
			                						<div>
			                							<p>👍</p>
			                							<input type="radio" name="pic4" value="1" data-cat="spending" class="selection">
			                						</div>
			                					</li>
			                				</ul>
			                			</section>
			                			<h3></h3>
			                			<section>
			                				<img class="mb-4" src="/assets/img/dancing.jpg" style="width:100%;">
			                				<h4>I have to go dancing during my trip</h4>
			                				<ul class="response">
			                					<li class="choice">
			                						<div>
			                							<p>👎</p>
			                							<input type="radio" name="pic5" value="-1" data-cat="adventure" class="selection">
			                						</div>
			                					</li>
			                					<li class="choice">
			                						<div>
			                							<p>😐</p>
			                							<input type="radio" name="pic5" value="0" data-cat="adventure" class="selection">
			                						</div>
			                					</li>
			                					<li class="choice">
			                						<div>
			                							<p>👍</p>
			                							<input type="radio" name="pic5" value="1" data-cat="adventure" class="selection">
			                						</div>
			                					</li>
			                				</ul>
			                			</section>
			                			<h3></h3>
			                			<section>
			                				<img class="mb-4" src="/assets/img/nature.jpg" style="width:100%;">
			                				<h4>I need time to decompress and rebalance.</h4>
			                				<ul class="response">
			                					<li class="choice">
			                						<div>
			                							<p>👎</p>
			                							<input type="radio" name="pic6" value="-1" data-cat="peaceful" class="selection">
			                						</div>
			                					</li>
			                					<li class="choice">
			                						<div>
			                							<p>😐</p>
			                							<input type="radio" name="pic6" value="0" data-cat="peaceful" class="selection">
			                						</div>
			                					</li>
			                					<li class="choice">
			                						<div>
			                							<p>👍</p>
			                							<input type="radio" name="pic6" value="1" data-cat="peaceful" class="selection">
			                						</div>
			                					</li>
			                				</ul>
			                			</section>
			                			<h3></h3>
			                			<section>
			                				<img class="mb-4" src="/assets/img/surfing.jpg" style="width:100%;">
			                				<h4>I'm down to surf (or at least learn)</h4>
			                				<ul class="response">
			                					<li class="choice">
			                						<div>
			                							<p>👎</p>
			                							<input type="radio" name="pic7" value="-1" data-cat="adventure" class="selection">
			                						</div>
			                					</li>
			                					<li class="choice">
			                						<div>
			                							<p>😐</p>
			                							<input type="radio" name="pic7" value="0" data-cat="adventure" class="selection">
			                						</div>
			                					</li>
			                					<li class="choice">
			                						<div>
			                							<p>👍</p>
			                							<input type="radio" name="pic7" value="1" data-cat="adventure" class="selection">
			                						</div>
			                					</li>
			                				</ul>
			                			</section>
			                			<h3></h3>
			                			<section>
			                				<img class="mb-4" src="/assets/img/shots.jpg" style="width:100%;">
			                				<h4>I'm here for a good drinking time!</h4>
			                				<ul class="response">
			                					<li class="choice">
			                						<div>
			                							<p>👎</p>
			                							<input type="radio" name="pic8" value="-1" data-cat="taboo" class="selection">
			                						</div>
			                					</li>
			                					<li class="choice">
			                						<div>
			                							<p>😐</p>
			                							<input type="radio" name="pic8" value="0" data-cat="taboo" class="selection">
			                						</div>
			                					</li>
			                					<li class="choice">
			                						<div>
			                							<p>👍</p>
			                							<input type="radio" name="pic8" value="1" data-cat="taboo" class="selection">
			                						</div>
			                					</li>
			                				</ul>
			                			</section>
			                			<h3></h3>
			                			<section>
			                				<img class="mb-4" src="/assets/img/locals.jpg" style="width:100%;">
			                				<h4>I want to hang out with locals and learn their culture.</h4>
			                				<ul class="response">
			                					<li class="choice">
			                						<div>
			                							<p>👎</p>
			                							<input type="radio" name="pic9" value="-1" data-cat="adventure" class="selection">
			                						</div>
			                					</li>
			                					<li class="choice">
			                						<div>
			                							<p>😐</p>
			                							<input type="radio" name="pic9" value="0" data-cat="adventure" class="selection">
			                						</div>
			                					</li>
			                					<li class="choice">
			                						<div>
			                							<p>👍</p>
			                							<input type="radio" name="pic9" value="1" data-cat="adventure" class="selection">
			                						</div>
			                					</li>
			                				</ul>
			                			</section>
			                			<h3></h3>
			                			<section>
			                				<img class="mb-4" src="/assets/img/smoking.jpg" style="width:100%;">
			                				<h4>I need a smoke break or two.</h4>
			                				<ul class="response">
			                					<li class="choice">
			                						<div>
			                							<p>👎</p>
			                							<input type="radio" name="pic10" value="-1" data-cat="taboo" class="selection">
			                						</div>
			                					</li>
			                					<li class="choice">
			                						<div>
			                							<p>😐</p>
			                							<input type="radio" name="pic10" value="0" data-cat="taboo" class="selection">
			                						</div>
			                					</li>
			                					<li class="choice">
			                						<div>
			                							<p>👍</p>
			                							<input type="radio" name="pic11" value="1" data-cat="taboo" class="selection">
			                						</div>
			                					</li>
			                				</ul>
			                			</section>
			                			<h3></h3>
			                			<section>
			                				<img class="mb-4" src="/assets/img/groups.jpg" style="width:100%;">
			                				<h4>I prefer to travel in groups instead of solo.</h4>
			                				<ul class="response">
			                					<li class="choice">
			                						<div>
			                							<p>👎</p>
			                							<input type="radio" name="pic12" value="-1" data-cat="classic" class="selection">
			                						</div>
			                					</li>
			                					<li class="choice">
			                						<div>
			                							<p>😐</p>
			                							<input type="radio" name="pic12" value="0" data-cat="classic"class="selection">
			                						</div>
			                					</li>
			                					<li class="choice">
			                						<div>
			                							<p>👍</p>
			                							<input type="radio" name="pic12" value="1" data-cat="classic" class="selection">
			                						</div>
			                					</li>
			                				</ul>
			                			</section>
			                			<h3></h3>
			                			<section>
			                				<img class="mb-4" src="/assets/img/ecotourism.jpg" style="width:100%;">
			                				<h4>I want positively impact the environment during my trip.</h4>
			                				<ul class="response">
			                					<li class="choice">
			                						<div>
			                							<p>👎</p>
			                							<input type="radio" name="pic13" value="-1" data-cat="peaceful" class="selection">
			                						</div>
			                					</li>
			                					<li class="choice">
			                						<div>
			                							<p>😐</p>
			                							<input type="radio" name="pic13" value="0" data-cat="peaceful" class="selection">
			                						</div>
			                					</li>
			                					<li class="choice">
			                						<div>
			                							<p>👍</p>
			                							<input type="radio" name="pic13" value="1" data-cat="peaceful" class="selection">
			                						</div>
			                					</li>
			                				</ul>
			                			</section>
			                			<h3></h3>
			                			<section>
			                				<img class="mb-4" src="/assets/img/planning.jpg" style="width:100%;">
			                				<h4>My trip has to be organized and well-planned.</h4>
			                				<ul class="response">
			                					<li class="choice">
			                						<div>
			                							<p>👎</p>
			                							<input type="radio" name="pic14" value="-1" data-cat="classic" class="selection final">
			                						</div>
			                					</li>
			                					<li class="choice">
			                						<div>
			                							<p>😐</p>
			                							<input type="radio" name="pic14" value="0" data-cat="classic" class="selection final">
			                						</div>
			                					</li>
			                					<li class="choice">
			                						<div>
			                							<p>👍</p>
			                							<input type="radio" name="pic14" value="1" data-cat="classic" class="selection final">
			                						</div>
			                					</li>
			                				</ul>
			                				<br>
			                			</section>

			                			<h3></h3>
			                			<section>
			                				<img class="mb-4" src="/assets/img/tatoo.jpg" style="width:100%;">
			                				<h4>I wouldn't mind getting a tatoo during my trip.</h4>
			                				<ul class="response">
			                					<li class="choice">
			                						<div>
			                							<p>👎</p>
			                							<input type="radio" name="pic15" value="-1" data-cat="taboo" class="selection">
			                						</div>
			                					</li>
			                					<li class="choice">
			                						<div>
			                							<p>😐</p>
			                							<input type="radio" name="pic15" value="0" data-cat="taboo" class="selection">
			                						</div>
			                					</li>
			                					<li class="choice">
			                						<div>
			                							<p>👍</p>
			                							<input type="radio" name="pic15" value="1" data-cat="taboo" class="selection">
			                						</div>
			                					</li>
			                				</ul>
			                			</section>
			                			<h3></h3>
			                			<section>
			                				<img class="mb-4" src="/assets/img/airtravel.jpg" style="width:100%;">
			                				<h4>I'd pay more money to get to my destination faster.</h4>
			                				<ul class="response">
			                					<li class="choice">
			                						<div>
			                							<p>👎</p>
			                							<input type="radio" name="pic16" value="-1" data-cat="spending" class="selection final">
			                						</div>
			                					</li>
			                					<li class="choice">
			                						<div>
			                							<p>😐</p>
			                							<input type="radio" name="pic16" value="0" data-cat="spending" class="selection final">
			                						</div>
			                					</li>
			                					<li class="choice">
			                						<div>
			                							<p>👍</p>
			                							<input type="radio" name="pic16" value="1" data-cat="spending" class="selection final">
			                						</div>
			                					</li>
			                				</ul>
			                				<br>
			                				<div class="ending">
			                					<p>You've reached the end! 🎉 Press the Finish button below to see your travel personality.</p>
			                				</div>
			                			</section>
		                			</div>
		                		</div>
		                	</div>
		            	</form>
	            	</div>
	            </div>
	        </div>
	    </div>
	</div>
@endsection

@section('js')
	<script type="text/javascript" src="/assets/js/jquery.steps.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/swiper@9/swiper-bundle.min.js"></script>
	<script>
    var swiper = new Swiper(".mySwiper", {
      effect: "cards",
      grabCursor: true,

    });
  </script>
	<script>
		$(document).ready(function() {
			$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': '{{ csrf_token() }}'
				}
			});
		});
	</script>
	<script type="text/javascript" src="/assets/js/quiz.js"></script>
@endsection