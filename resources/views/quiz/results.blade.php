@extends('layouts.curator-master')

@section('css')
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper@9/swiper-bundle.min.css" />
    <style>
        section img.mb-4{
        	border-radius: 20px;
        }
        section h4{
        	font-size: 1.2em;
        }
        
    

    #instructions{
    	font-size:14px;
    }
    .shadow-5{
      box-shadow: 0 1px 1px rgba(0,0,0,0.12), 
              0 2px 2px rgba(0,0,0,0.12), 
              0 4px 4px rgba(0,0,0,0.12), 
              0 8px 8px rgba(0,0,0,0.12),
              0 16px 16px rgba(0,0,0,0.12);
    }
    .swiper-horizontal{
      padding-right: 20px;
      padding-left: 20px;
    }
	</style>
@endsection

@section('content')
<div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v16.0&appId=1298822020928144&autoLogAppEvents=1" nonce="afhxnWw9"></script>
	<div id="page-content">
	    <div class="gradient-wrap">
	        <div class="container hero-section mb-4 mt-4">
	            <div class="row justify-content-center">
	                <div class="col-md-8 mt-4">
		                	<div class="card">
		                			<div class="card-body">
		                				<div class="text-center mt-4">
					                		<h2>Your Travel Personality is</h2>
					                    <h1>{{ $header }}</h1>
                              <div style="width:300px; margin: 0 auto;">
                              <div class="fb-like" data-href="https://venti.co/quiz/results" data-width="" data-layout="" data-action="" data-size="" data-share="true" \></div>
                            </div>
					                   </div>
                             
		                				<p style="font-size:18px; padding-left:20px; padding-right: 20px;">{{ $description }}</p>
                              <h4 class="text-center">You're not alone!</h4>
                              <p style="font-size:18px; padding-left:20px; padding-right: 20px;">
                              There are hundreds of people just like you already on Venti, a new social platform to find travel buddies. We've already found eight that could be a great fit for you.<br><a href="/register" target="_blank">Create an account</a> or </p>
		                				<p class="text-center"><a href="https://apps.apple.com/us/app/venti-friends-and-adventure/id1620282387" style="width:200px;" class="btn btn-black"><i class="fab fa-apple mr-1"></i> DOWNLOAD VENTI</a></p>
		                				<br>

		                				<div class="row justify-content-center">
                              <div class="swiper">
                                <!-- Additional required wrapper -->
                                <div class="swiper-wrapper">
                                  @for($i = 0; $i < 8; $i++)
                                  <div class="swiper-slide text-center mb-4" style=" border-radius:20px;">
                                    <div>
                                    <div style="background-image: url('{{ $users[$i]['photoUrl'] }}'); width:100%; height: 320px; background-size:cover; border-top-left-radius: 20px; border-top-right-radius:20px;">
                                    </div>
                                    <h5 class="mt-4">{{ $users[$i]["displayName"] }}, {{ $users[$i]["age"] }}</h5>
                                    <p style="font-size: 12px; min-height:70;">{{ $users[$i]["bio"] }}</p>
                                    <h5>Interests</h5>
                                    @foreach($users[$i]["interests"] as $interest)
                                       <span style="font-size: 11px; border: 1px solid rgba(0,0,0,0.3); display: inline-block; text-transform:  uppercase; font-weight: bold; padding: 5px 10px; border-radius: 20px; margin: 5px; background-color:whitesmoke;">{{ $interest["value"] ?? interestEmoji($interest) . " " . $interest }}</span>
                                    @endforeach
                                  </div>
                                  </div>
                                @endfor
                                </div>
                                <!-- If we need pagination -->
                                <div class="swiper-pagination" style="margin-top:40px;"></div>

                                <!-- If we need scrollbar -->
                                <div class="swiper-scrollbar"></div>
                              </div>
		                						
		                				</div>
		                				<br>
		                				<div class="text-center">
		                					<a href="/connect" class="btn btn-primary" style="max-width:180px;" target="_blank">👀 WHO'S ON VENTI</a>
		                				</div>
                            <br>
                            <h4>What is Venti?</h4>
                            <p>We're a free social platform that helps you find people to go on adventures with that share your travel style and preferences.<br><a href="/" target="_blank">Learn More</a></p>
		                			</div>
		                	</div>
	            		</div>
	            </div>
	        </div>
	    </div>
	</div>
@endsection

@section('js')
	<script type="text/javascript" src="/assets/js/jquery.steps.min.js"></script>
	<script type="text/javascript" src="/assets/js/quiz.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/swiper@9/swiper-bundle.min.js"></script>
	<script>
var width = $(window).width();

            var numSlides = 3;
            if(width < 600){
numSlides = 1;
}
            
const swiper = new Swiper('.swiper', {
  // Optional parameters
  direction: 'horizontal',
  loop: false,
  slidesPerView: numSlides,

  // If we need pagination
  pagination: {
    el: '.swiper-pagination',
  },
  spaceBetween: 30,
  // Navigation arrows
  navigation: {
    nextEl: '.swiper-button-next',
    prevEl: '.swiper-button-prev',
  }
});
		$().click(function(){

		})
	</script>
@endsection