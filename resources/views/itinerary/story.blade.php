@php 
   $counter = 1; 
   function cleanParagrapText($string){
         $string = str_replace("<p>","", $string);
         $string = str_replace("</p>"," <br>", $string);
         $string = parseShortCodeStory($string);
         return $string;
   }
@endphp
<div id="itinerary-stories">
	<div id="stories-container" class="main-carousel carousel--full-width">
		<div data-slide="slide-0" class="slide slide-0 carousel-cell">
			<div class="itinerary-menu">
				<a href="javascript:void(0);" class="story-exit"><i class="fa fa-times"></i></a>
			</div>
			<nav class="slide-nav">
		      <div class="slide-thumb"></div>
		      <button class="slide-prev">Anterior</button>
		      <button class="slide-next">Próximo</button>
		    </nav>
		   <div class="slide-items">
		    		<!-- author slide -->
		    		<div class="slide-item-content" style="background-image:url('{{ $idea['imageURL'] }}');">
            		<div class="slide-item-text">
            			<img src="{{ $client['avatar'] }}" class="story-author">
            			<h2 class="text-left">{{ $client["name"] }} Presents</h2>
            			<h3 class="text-left">{{ $idea['title'] }}</h3>
            			<div style="width: 100%; min-width: 300px;">
            				<br>
            				<p style="color:white;">Swipe to Continue <i class="fa-solid fa-arrow-right-long"></i></p>
            			</div>
            		</div>
            	</div>
            </div>
         </div>
         
		@foreach($itinerary->days as $day)
			@php
		      $header = preg_split('#\r?\n#', $day->description, 0)[0];
		      $toggleID = mt_rand();
		   	@endphp

			<div data-slide="slide-{{ $counter }}" class="slide slide-{{ $counter }} carousel-cell">
				<div class="itinerary-menu">
					<a href="javascript:void(0);" class="story-exit"><i class="fa fa-times"></i></a>
				</div>
				<nav class="slide-nav">
			      <div class="slide-thumb"></div>
			      <button class="slide-prev">Anterior</button>
			      <button class="slide-next">Próximo</button>
			    </nav>
			    <div class="slide-items">
			    	@php $s = 0; @endphp
		            @foreach($day->stops as $stop)
			            @php $subheadercounter = mt_rand(); @endphp
			            @foreach($stop->photos as $photo)
			            	<div class="slide-item-content m-{{ $subheadercounter }} order-{{ $photo->order }} lazyload">
			            		<div class="slide-item-text">
			            			<h2 class="text-left">Day {{ $counter }}</h2>
			            			<h3 class="text-left">{{ $stop->name }}</h3>
			            			<div style="width: 100%; min-width: 300px;">
			            			{!! str_replace($header,"",$stop->description) !!}
			            			</div>
			            		</div>
			            		<style type="text/css">.slide-{{ $counter }} .m-{{ $subheadercounter }}.order-{{ $photo->order }}.lazyloaded{ background-image:url('{{ $photo->url }}') }</style>
			            	</div>
			            	
			            @endforeach
			        	@endforeach
			        	@if(isset($day->sleepStop->name))
			        	 	@foreach($day->sleepStop->photos as $sleepPhoto)
				        		<div class="slide-item-content" style="background-image: url('{{ $sleepPhoto->url }}');">
				            		<div class="slide-item-text">
				            			<h2 class="text-left"><i class="fa fa-moon" style="color:white;"></i> Lodging</h2>
				            			<h3 class="text-left">{{ $day->sleepStop->name }}</h3>
				            			<div style="width: 100%; min-width: 300px;">
				            				{!! $day->sleepStop->description !!}
				            			</div>
				            		</div>
				            </div>
				            @php break; @endphp
				         @endforeach
						@endif
			    </div>
			    <script type="text/javascript">new SlideStories('slide-{{$counter}}');</script>
			</div>

			@php $counter++ @endphp

		@endforeach

		<div data-slide="slide-{{ sizeof($itinerary->days) + 1 }}" class="slide slide-{{ sizeof($itinerary->days) + 1 }} carousel-cell">
			<div class="itinerary-menu">
				<a href="javascript:void(0);" class="story-exit"><i class="fa fa-times"></i></a>
			</div>
			<nav class="slide-nav">
		      <div class="slide-thumb"></div>
		      <button class="slide-prev">Anterior</button>
		      <button class="slide-next">Próximo</button>
		    </nav>
		   <div class="slide-items">
		    		<!-- author slide -->
		    		<div class="slide-item-content" style="background-image:url('{{ $idea['imageURL'] }}');">
            		<div class="slide-item-text">
            			<img src="{{ $client['avatar'] }}" class="story-author">
            			<h2 class="text-left">Done <i class="fa fa-check"></i></h2>
            			<h3 class="text-left">I hope you've enjoyed your trip to {{ $idea['destination'] }}!</h3>
            			<div style="width: 100%; min-width: 300px;">
            				<br>
            				<p style="color:white;"><i class="fa-solid fa-arrow-left-long"></i> Swipe to Backtrack</p>
            			</div>
            		</div>
            	</div>
            </div>
         </div>
	</div>
</div>
<script type="text/javascript">new SlideStories('slide-0');</script>
<script type="text/javascript">new SlideStories('slide-{{ sizeof($itinerary->days) + 1 }}');</script>
<style type="text/css">
	#itinerary-stories{
		position: fixed;
		height: 100%;
		width: 100%;
		background: rgba(0,0,0,0.95);
		top: -2000px;
		left: 0;
		z-index: 10;
		text-align: center;
		align-content: center;
		align-items: center;
		display: flex;
		opacity: 0;
	}
	#stories-container{
		position: relative;
		height: 100%;
		width: 100%;
	}
	#navigation-controls{
		position: absolute;
	    top: 50%;
	    left: 30%;
	    width: 40%;
	    justify-content: space-between;
	    display: flex;
	}
	#navigation-controls a{
		font-size: 30px;
		color: rgba(255,255,255,0.3);
	}
	.slide-items{
		max-width: 400px;
		margin: 0 auto;
		height: 100%;
	}
	.slide-items img{
		display: block;
		height: 100%;
	}
	.slide-content{
		position: absolute;
		bottom: 0;
		width: 40%;
		height: 40%;
		z-index: 10;
	}
	.slide-item-content{
		display: flex;
		width: 100%;
		height: 100%;
		background-size: cover;
		background-position: center; 

	}
	.slide-item-content h2, .slide-item-content h3, .slide-item-content p{
		color: white !important;
	}
	.slide-item-content h2{
		font-size: 2.5rem;
	}
	.slide-item-content h3{
		font-size: 1.75rem;
	}
	.slide-item-text{
		padding:20px;
		padding-top: 30px;
		text-align:left;
		vertical-align: top;
		margin-top: 60vh;
    	min-height: 40%;
    	border-bottom: 1px solid black;
    	width: 100%;
    	background: linear-gradient(to bottom, rgba(0, 0, 0, 0.01) 0%, rgba(0,0,0,0.6) 12% ,rgba(0, 0, 0, 1) 100%);
	}
	.carousel-cell {
        width: 100%; /* full width */
        height: 100%; /* height of carousel */
        margin-right: 50%;
        display: grid;
   }
   .itinerary-menu{
		position: absolute;
		z-index: 30;
		top: 40px;
		right: 20px;
	}
   .itinerary-menu a i{
   	color: white;

    font-size: 30px;
   }
   @media (max-width: 767px){
   	.flickity-button{
   		display: none;
   	}
   	.slide-thumb {
   		width: 98%;
   	}
   	#page-content {
         padding-bottom: 200px;
      }
      table tbody td{
      	display: table-row;
      	border:  none !important;
      	line-height: 2;
      }
   }
   .slide-nav{
   	background-image: linear-gradient(to bottom, rgba(0, 0, 0, 0.0) 100%, rgba(0,0,0,0.6) 88% ,rgba(0, 0, 0, 0.01) 0%);
   }
   .story-author{
   	width: 100px !important;
   	height: 100px !important;
   	margin-top: -250px;
   	position: absolute;
   	left: 50%;
   	margin-left: -50px;
   }

   .slide-item-text div p:nth-child(2), .slide-item-text div p:nth-child(3) {
   	font-size: 1.4vh;
   	overflow-wrap: anywhere;
   } {
   	font-size: 1.5vh;
   }
</style>