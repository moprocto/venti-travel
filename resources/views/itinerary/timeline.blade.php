<div class="row"  style="flex-wrap:wrap; width: 100%; margin:20px 0;">
   <div class="col-md-12 text-center" style="padding: 45px; margin-bottom: 30px; border-radius: 20px; background:black;" id="itinerary">
      <img src="{{ $client['avatar'] }}" class="product-author">
      <h4 style="color: #98877D;">{{ $client["name"] }} Presents</h4>
      <h1 style="color:white">{{ $idea['title'] }}</h1>

      <button type="button" class="btn btn-primary btn-rounded" id="story-start"><i class="fa fa-play"></i> Story Mode</button>

      @if($personalize)
         <p style="color:white;">Send an email to <a href='mailto:{{ $client["email"] }}' style="color:#98877D;">{{ $client["email"] }}</a> to have this trip customized.</p>
      @endif
   </div>
   @php 
   $counter = 1; 
   function cleanParagraphText($string){
         $string = str_replace("<p>","", $string);
         $string = str_replace("</p>"," <br>", $string);
         $string = parseShortCode($string);
         return $string;
   }

   @endphp
   @foreach($itinerary->days as $day)
   @php
      $header = preg_split('#\r?\n#', $day->description, 0)[0];
      $toggleID = mt_rand();
      
   @endphp
      <div class="col-md-12" style="margin-bottom:20px;" data-section-title="Day {{ $counter }}: {!! $header !!}">
         <h2>Day {{ $counter }}: <span style="font-size:18px;" class="adjustedDate"></span></h2>
         <div class="card">
            <div class="card-header">
            </div>
            <div class="card-body">
               @if(!empty($header))
                  <div class="accordion" id="accordionExample{{ $toggleID  }}">
                    <div class="card">
                      <div class="card-header" id="heading{{ $toggleID  }}">
                        <h2 class="mb-0 panel-">
                          <button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#c{{ $toggleID }}" aria-expanded="true" aria-controls="collapse{{ $toggleID  }}">
                              {!! $header !!}
                              <i class="fas fa-angle-down rotate-icon"></i>
                          </button>
                        </h2>
                      </div>
                      <div id="c{{ $toggleID }}" class="collapse show" aria-labelledby="heading{{ $toggleID }}" data-parent="#accordionExample{{ $toggleID  }}">
                        <div>
                          {!! cleanParagraphText(str_replace('\\','',str_replace($header,"",$day->description))) !!}
                        </div>
                      </div>
                    </div>
                  </div>
               @endif
            </div>
         </div>
      </div>
      @php $s = 0; @endphp
      @foreach($day->stops as $stop)
      @php
         $subheadercounter = mt_rand();
      @endphp
      <div class="col-md-10 offset-2 timeline-section" style="padding-left:15%;" data-section-title="Day {{ $counter }}: {{ $stop->name }}">
         <div class="col-md-12 stop" style="margin-bottom:20px;">
            <div class="card">
               <div class="image-gallery">
                     @foreach($stop->photos as $photo)
                     <a href="{{ $photo->url }}" data-toggle="lightbox" data-type='image' data-gallery="gallery-{{ str_replace(' ','-', $stop->name) }}" data-title="{{ $photo->description }}">
                        <div class="m-{{ $subheadercounter}} image-header order-{{ $photo->order }} lazyload"></div>
                        <style type="text/css">.image-gallery .m-{{ $subheadercounter}}.image-header.order-{{ $photo->order }}.lazyloaded{background-image: url('{{ $photo->url }}')}</style>
                     </a>
                     @endforeach
                  </div>
               <div class="card-body">
                  <h3>{{ $stop->name }}</h3>
                  <div class="accordion" id="accordionExample{{ $subheadercounter }}">
                    <div class="card">
                      <div class="card-header" id="heading{{ $subheadercounter }}">
                        <h2 class="mb-0 panel-">
                          <button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#d{{ $subheadercounter }}" aria-expanded="true" aria-controls="{{ $subheadercounter }}">
                              @php
                                 $header = preg_split('#\r?\n#', $stop->description, 0)[0];
                                 echo $header;
                              @endphp
                              <i class="fas fa-angle-down rotate-icon"></i>
                          </button>
                        </h2>
                      </div>
                      <div id="d{{ $subheadercounter }}" class="collapse show" aria-labelledby="heading{{ $subheadercounter }}" data-parent="#accordionExample{{ $subheadercounter }}">
                        <div>
                          {!! cleanParagraphText(str_replace($header,"",$stop->description)) !!}
                        </div>
                      </div>
                    </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      @php $s++; @endphp
      @endforeach
         @if(isset($day->sleepStop->name))
            <div class="col-md-12 sleep" style="margin-bottom:20px;">
               <div class="card">
                  <div class="card-body" style="background-color: #0e1e37; border-radius:20px;">
                     <h3 style="color:white"><i class="fa fa-moon" style="color:white;"></i> Lodging: {{ $day->sleepStop->name }}</h3>
                     {!! $day->sleepStop->description !!}
                     
                     @php $i = 0; @endphp
                     <div class="image-gallery">
                     @foreach($day->sleepStop->photos as $sleepPhoto)
                        <a href="{{ $sleepPhoto->url }}" data-toggle="lightbox" data-type='image' data-gallery="example-gallery">
                           <div class="image-tile order-{{ $i }} lazyload" data-bg="{{ $sleepPhoto->url }}"></div>
                        </a>
                     
                     @php $i++; @endphp
                     @endforeach
                     </div>
                  </div>
               </div>
            </div>
         @endif
      @php $counter++; @endphp
   @endforeach
</div>