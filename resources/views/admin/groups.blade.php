@extends('layouts.curator-master')

@section('css')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.12.1/css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/2.2.3/css/buttons.dataTables.min.css">
@endsection

@section('content')
<div id="page-content">
    <div class="gradient-wrap">
        <div class="container hero-section mb-4 mt-4">
	        <div class="row justify-content-center mb-4">
	        	<div class="col-md-4">
    				<div class="card">
    					<div class="card-body text-center">
    						<h2>{{ sizeof($groups) }}<br>Groups</h2>
    					</div>
    				</div>
    			</div>
    			<div class="col-md-4">
    				<div class="card">
    					<div class="card-body text-center">
    						<h2>{{ $archived }}<br>Archived</h2>
    					</div>
    				</div>
    			</div>
	    	</div>
	    	<div class="row justify-content-center mt-4">
	            	<div class="col-md-12 text-center">
	            		<div class="card">
	            			<div class="card-body">
	            				<table class="table" id="users">
			            			<thead>
			            				<th class="text-center">Trip</th>
			            				<th class="text-left">Author</th>
			            				<th class="text-left">Destination</th>
			            				<th>Status</th>
			            				<th>Start</th>
			            				<th>Days</th>
			            				<th>Privacy</th>
			            			</thead>
			            			<tbody>
			            				@foreach($groups as $group)
			            					<tr>
			            						<td class="text-center"><a href="/group/{{ $group['tripID'] }}" target="_blank"><img src="{{ $group['imageURL'] ?? '' }}" style="width:100px;"><br>{{ $group["title"] ?? "N/A" }}</a></td>
			            						<td class="text-left">{{ $group["author"]->displayName ?? "" }}</td>
			            						<td class="text-left">{{ getTripLocation($group["destination"], $group["country"]) }}</td>
			            						<td>{{ $group["archived"] }}</td>
			            						<td>{{ $group["departure"] }}</td>
			            						<td>{{ $group["days"] }}</td>
			            						<td>{{ $group["privacy"] ?? "N/A"}}</td>
			            					</tr>
			            				@endforeach
			            			</tbody>
			            		</table>
	            			</div>
	            		</div>
	            		
	            	</div>
	        </div>
	    </div>
	</div>
</div>
@endsection

@section("js")
    <script type="text/javascript" src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/2.2.3/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/2.2.3/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/2.2.3/js/buttons.print.min.js"></script>
    <script type="text/javascript">
        $(document).ready( function () {
            $('#users').DataTable({
            	"pageLength": 50,
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]
            });
        });
    </script>
@endsection