@extends("layouts.curator-master")


@section('css')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.12.1/css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/2.2.3/css/buttons.dataTables.min.css">
<style>
    tr.text-grey > *{
        color: grey;
    }
</style>
@endsection

@section('content')
<div id="page-content">
    <div class="gradient-wrap">
        <div class="container hero-section mb-4 mt-4">
            @include('admin.nav')
                <div class="row justify-content-center mb-4">
                    <div class="col-md-3">
                        <div class="card">
                            <div class="card-body text-center">
                                <h2>{{ sizeof($orders) }}<br> Orders</h2>
                                <ul>
                                </ul>
                                <p></p>
                                <p></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card">
                            <div class="card-body text-center">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-center mt-4">
                <div class="col-md-12 text-center">
                    <div class="card">
                        <div class="card-body">
                            <table class="table" id="users">
                                <thead>
                                    <th class="text-left">Order #</th>
                                    <th>Cost</th>
                                    <th>Revenue</th>
                                    <th>Points</th>
                                    <th>Profit</th>
                                    <th>Date</th>
                                </thead>
                                <tbody>
                                    @foreach($orders as $order)
                                        @php
                                            $date = \Carbon\Carbon::parse($order["timestamp"])->timezone("America/New_York")->format("F d, Y g:i A");
                                            
                                            $airlock = $order["airlock"] ?? "N/A";
                                            $type = $order["type"];
                                            
                                            $duffelData = json_decode($order["order"], true);

                                            if(array_key_exists("data", $duffelData)){
                                                $duffelData = $duffelData["data"];
                                            }

                                            if($type == "hotel"){
                                                $rate = $duffelData["accommodation"]["rooms"][0]["rates"][0];
                                                $cost = $rate["total_amount"];
                                            } else {
                                                $cost = $duffelData["total_amount"];
                                            }

                                            $profit = $order["amount"];

                                        @endphp
                                        <tr class="">
                                            <td class="text-left"><a href="/trips/{{ $order['refID'] }}" target="_blank">{{ $order["booking_reference"] }}</a><br>{{ $duffelData["status"] ?? "N/A" }}<br>{{ ucfirst($type) }}<br><a href="/admin/user/{{ $order['uid'] }}" target="_blank">Customer</a></td>
                                            <td>${{ number_format($cost,2) }}</td>
                                            <td>${{ number_format($order["amount"],2) }}</td>
                                            <td>{{ number_format($order["points"],2) }}</td>
                                            <td>${{ number_format($profit,2) }}</td>
                                            <td class="text-left">{{ $date }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section("js")
    <script type="text/javascript" src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/2.2.3/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/2.2.3/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/2.2.3/js/buttons.print.min.js"></script>
    <script type="text/javascript">
        $(document).ready( function () {
            $('#users').DataTable({
                orderCellsTop: true,
                fixedHeader: true,
                "pageLength": 50,
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]
            });
        });
    </script>
@endsection