@extends("layouts.curator-master")


@section('css')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.12.1/css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/2.2.3/css/buttons.dataTables.min.css">
<style>
	tr.text-grey > *{
		color: grey;
	}
</style>
@endsection

@section('content')
<div id="page-content">
    <div class="gradient-wrap">
        <div class="container hero-section mb-4 mt-4">
	    	<div class="row justify-content-center mb-4">
	        	<div class="col-md-3">
    				<div class="card">
    					<div class="card-body text-center">
    						<h2>{{ sizeof($navigators) }}
    						<p></p>
    						<p></p>
    					</div>
    				</div>
    			</div>
	    	</div>
		    <div class="row justify-content-center mt-4">
            	<div class="col-md-12 text-center">
            		<div class="card">
            			<div class="card-body">
            				<table class="table" id="users">
		            			<thead>
		            				<th></th>
		            				<th class="text-left">Name</th>
		            				<th class="text-left">Email</th>
		            				<th class="text-left">UID</th>
		            				<th></th>
		            			</thead>
		            			<tbody>
		            				@foreach($navigators as $navigator)
		            					<tr>
		            						<td><img src="{{ $navigator->photoUrl }}" style="width:75px;"></td>
		            						<td class="text-left"> {{ $navigator->displayName }}</td>
		            						<td class="text-left"> {{ $navigator->email }}</td>
		            						<td class="text-left"> {{ $navigator->uid }}</td>
		            						<td> {!! $navigator->customClaims["nav"] ?? "<a target='_blank' href='/admin/navigators/make/" . $navigator->uid . "'>Mint</a>" !!}</td>
		            					</tr>
		            				@endforeach
		            			</tbody>
		            		</table>
            			</div>
            		</div>
            	</div>
	        </div>
	    </div>
	</div>
</div>
@endsection

@section("js")
    <script type="text/javascript" src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/2.2.3/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/2.2.3/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/2.2.3/js/buttons.print.min.js"></script>
    <script type="text/javascript">
        $(document).ready( function () {
            $('#users').DataTable({
            	orderCellsTop: true,
        		fixedHeader: true,
            	"pageLength": 50,
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]
            });
        });
    </script>
@endsection