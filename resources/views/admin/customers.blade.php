@extends('layouts.curator-master')

@section('css')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.12.1/css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/2.2.3/css/buttons.dataTables.min.css">
@endsection

@section('content')
<div id="page-content">
    <div class="gradient-wrap">
        <div class="container hero-section mb-4 mt-4">
        	@include('admin.nav')
	    	<div class="row justify-content-center mb-4">
	        	<div class="col-md-3">
    				<div class="card">
    					<div class="card-body text-center">
    						<h2>{{ sizeof($customers) }}<br>Users</h2>
    						<ul>

    						</ul>
    						<p></p>
    						<p></p>
    					</div>
    				</div>
    			</div>
    			<div class="col-md-3">
    				<div class="card">
    					<div class="card-body text-center">
    						<h2><span id="balance"><i class="fa fa-sync fa-spin"></i></span><br>Balance</h2>
    						<ul>
    						</ul>
    					</div>
    				</div>
    			</div>
    			<div class="col-md-3">
    				<div class="card">
    					<div class="card-body text-center">
    						<h2><span id="points"><i class="fa fa-sync fa-spin"></i></span><br>Points</h2>
    					</div>
    				</div>
    			</div>
	    	</div>
	    	<div class="row justify-content-center mt-4">
            	<div class="col-md-12 text-center">
            		<div class="card">
            			<div class="card-body">
            				<table class="table" id="users">
		            			<thead>
		            				<th class="text-left">Profile</th>
		            				<th class="text-left">Dwolla</th>
		            				<th>Status</th>
		            			</thead>
		            			<tbody>
		            				@foreach($customers as $customer)
		            					@php
		            						$customerID = explode("/customers", $customer["customerID"])[1];
		            					@endphp
		            					<tr>
		            						<td class="profile-card text-left" data-uid="{{ $customer['uid'] }}"><i class="fa fa-spinner fa-spin"></i></td>
		            						<td class="dwolla-card text-left" data-customer="{{ $customerID }}"><i class="fa fa-spinner fa-spin"></i></td>
		            						<td>{{ $customer["status"] }}</td>
		            					</tr>
		            				@endforeach
		            			</tbody>
		            		</table>
            			</div>
            		</div>
            	</div>
	        </div>
	    </div>
	</div>
</div>
@endsection

@section("js")
    <script type="text/javascript" src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/2.2.3/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/2.2.3/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/2.2.3/js/buttons.print.min.js"></script>
    <script type="text/javascript">
        $(document).ready( function () {

        	$.ajaxSetup({
		        headers: {
		            'X-CSRF-TOKEN': '{{ csrf_token() }}'
		        }
		    });

            $('#users').DataTable({
            	"pageLength": 200,
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                "drawCallback": function( settings ) {
                	grabProfiles();
			    }
            });

            

            $("body").on("click",".paginate_button", function(){
            	queryMailchimp();
            });

            function grabProfiles(){

            	$(".profile-card").each(function(){
            		var card = $(this);
            		var uid = $(this).attr("data-uid");

            		$.ajax({ 
	                    type: 'post', 
	                    url: "/admin/profile",
	                    dataType: 'json',
	                    data:{
	                    	uid: uid
	                    },
	                    success: function (data) { 
	                    	card.html(data);
	                    }
	                });
            	});

            	grabDwolla();

            }

            function grabDwolla(){
            	$(".dwolla-card").each(function(){
            		var card = $(this);
            		var customer = $(this).attr("data-customer");

            		$.ajax({ 
	                    type: 'post', 
	                    url: "/admin/profile/dwolla",
	                    dataType: 'json',
	                    data:{
	                    	customer: customer
	                    },
	                    success: function (data) { 
	                    	card.html(data);
	                    	calculateTopBalances();
	                    }
	                });
            	});
            }

            function calculateUserProgress(){
            	$(".dwolla-card").each(function(){
            		var card = $(this);
            	});


            }

            function calculateTopBalances(){
            	var points = 0;
            	var balance = 0;

            	$(".walletPoints").each(function(){
            		points += Math.round(parseFloat($(this).text()));
            	});

            	$(".walletBalance").each(function(){
            		console.log($(this).attr("data-balance"));
            		console.log(parseFloat($(this).attr("data-balance")));
            		balance += parseFloat($(this).attr("data-balance"));
            	});

            	$("#points").text(points.toLocaleString(undefined, {minimumFractionDigits: 2}));
            	$("#balance").text("$" + balance.toLocaleString(undefined, {minimumFractionDigits: 2}))
            }

            function queryMailchimp(){
            	/*
	            	$(".getEmailStatus").each(function(){
		            	var span = $(this);
		            	var email = span.attr("data-email");

		            	$.ajax({ 
		                    type: 'GET', 
		                    url: "/admin/mailchimpStatus/" + email,
		                    dataType: 'json',
		                    success: function (data) { 
		                    	if(data == true){
		                    		span.text("Y");
		                    	}
		                    }
		                });
		            });
		            */
            }
        });
    </script>
@endsection