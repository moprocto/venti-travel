@extends('layouts.curator-master')

@php
$auth = app('firebase.auth');

        $firestore = app('firebase.firestore');
        $db = $firestore->database();
@endphp

@section('css')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.12.1/css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/2.2.3/css/buttons.dataTables.min.css">
@endsection

@section('content')
<div id="page-content">
    <div class="gradient-wrap">
        <div class="container hero-section mb-4 mt-4">
        	<div class="row justify-content-center mb-4">
        		<div class="col-md-12">
        			<ul class="nav nav-pills navtab-bg nav-justified" style="margin-bottom:10px">
				        <li class="nav-item" style="max-width:100px;">
				            <a href="/admin/" class="nav-link">
				                CUSTOMERS
				            </a>
				        </li>
				        <li class="nav-item" style="max-width:100px;">
				            <a href="/admin/users" class="nav-link">
				                USERS
				            </a>
				        </li>
				        <li class="nav-item" style="max-width:100px;">
				            <a href="/admin/transactions" class="nav-link">
				                TRANSACTIONS
				            </a>
				        </li>
				        <li class="nav-item" style="max-width:100px;">
				            <a href="/admin/searches" class="nav-link">
				                SEARCHES
				            </a>
				        </li>
				    </ul>
        		</div>
        	</div>
	        

		    	<div class="row justify-content-center mb-4">
		        	<div class="col-md-3">
	    				<div class="card">
	    					<div class="card-body text-center">
	    						<h2>{{ sizeof($users) }}<br>Users</h2>
	    					</div>
	    				</div>
	    			</div>
	    			<div class="col-md-3">
	    				<div class="card">
	    					<div class="card-body text-center">
	    						<h2><span id="balance"><i class="fa fa-sync fa-spin"></i></span><br>Balance</h2>
	    						<ul>
	    						</ul>
	    					</div>
	    				</div>
	    			</div>
	    			<div class="col-md-3">
	    				<div class="card">
	    					<div class="card-body text-center">
	    						<h2><span id="points"><i class="fa fa-sync fa-spin"></i></span><br>Points</h2>
	    					</div>
	    				</div>
	    			</div>
		    	</div>
		    	<div class="row justify-content-center mt-4">
	            	<div class="col-md-12 text-center">
	            		<div class="card">
	            			<div class="card-body">
	            				<table class="table" id="users">
			            			<thead>
			            				<th class="text-left">User</th>
			            				<th class="text-center">Dwolla</th>
			            				<th class="text-center">Created At</th>
			            			</thead>
			            			<tbody>
			            				@foreach($users as $user)
			            					@php
			            						$customClaims = $user->customClaims ?? false;
			            						$referralKey = "N/A";
			            						

			            						if(array_key_exists("referralKey", $customClaims)){
			            							$referralKey = $customClaims["referralKey"];
			            						}

			            						$customer = $dwolla->getCustomer($user, $auth, $db);

			            					@endphp
			            					<tr>
			            						<td class="text-left">
			            							{{ $user->displayName }}<br>
			            							{{ $user->uid }}<br>
			            							{{ $user->email }}
			            						</td>
			            						<td class="text-center">
			            							@if($customer)
			            								<i class="fa fa-check"></i>
			            							@endif
			            						</td>
			            						<td class="text-center">{{ $user->metadata->createdAt->format("m-d-Y") }}</td>
			            					</tr>
			            				@endforeach
			            			</tbody>
			            		</table>
	            			</div>
	            		</div>
	            	</div>
		        </div>
	    </div>
	</div>
</div>
@endsection

@section("js")
    <script type="text/javascript" src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/2.2.3/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/2.2.3/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/2.2.3/js/buttons.print.min.js"></script>
    <script type="text/javascript">
        $(document).ready( function () {

        	$.ajaxSetup({
		        headers: {
		            'X-CSRF-TOKEN': '{{ csrf_token() }}'
		        }
		    });

            $('#users').DataTable({
            	"pageLength": 50,
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                "drawCallback": function( settings ) {
                	grabProfiles();
			    }
            });

            

            $("body").on("click",".paginate_button", function(){
            	queryMailchimp();
            });

            function grabProfiles(){

            }

            function grabDwolla(){
            	
            }

            function calculateUserProgress(){
            	


            }

            function calculateTopBalances(){
            	
            }

            function queryMailchimp(){
            	/*
	            	$(".getEmailStatus").each(function(){
		            	var span = $(this);
		            	var email = span.attr("data-email");

		            	$.ajax({ 
		                    type: 'GET', 
		                    url: "/admin/mailchimpStatus/" + email,
		                    dataType: 'json',
		                    success: function (data) { 
		                    	if(data == true){
		                    		span.text("Y");
		                    	}
		                    }
		                });
		            });
		            */
            }
        });
    </script>
@endsection