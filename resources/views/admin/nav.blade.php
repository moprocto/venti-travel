<div class="row justify-content-center mb-4">
	<div class="col-md-12">
		<div class="card bg-white">
			<div class="card-body">
				<ul class="nav nav-pills navtab-bg nav-justified">
			        <li class="nav-item" style="">
			            <a href="/admin/" class="nav-link text-bold">
			                CUSTOMERS
			            </a>
			        </li>
			        <li class="nav-item" style="">
			            <a href="/admin/users" class="nav-link text-bold">
			                USERS
			            </a>
			        </li>
			        <li class="nav-item" style="">
			            <a href="/admin/transactions" class="nav-link text-bold">
			                TRANSACTIONS
			            </a>
			        </li>
			        <li class="nav-item" style="">
			            <a href="/admin/sources" class="nav-link text-bold">
			                SOURCES
			            </a>
			        </li>
			        <li class="nav-item" style="">
			            <a href="/admin/transactions/orders" class="nav-link text-bold">
			                ORDERS
			            </a>
			        </li>
			        <li class="nav-item" style="">
			            <a href="/admin/transactions/autosaves" class="nav-link text-bold">
			                AUTOSAVES
			            </a>
			        </li>
			        <li class="nav-item" style="">
			            <a href="/admin/searches" class="nav-link text-bold">
			                SEARCHES
			            </a>
			        </li>
			    </ul>
			</div>
		</div>
	</div>
</div>