@extends("layouts.curator-master")


@section('css')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.12.1/css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/2.2.3/css/buttons.dataTables.min.css">
<style>
    tr.text-grey > *{
        color: grey;
    }
</style>
@endsection

@section('content')
<div id="page-content">
    <div class="gradient-wrap">
        <div class="container hero-section mb-4 mt-4">
            <h1> Funding Sources</h1>
            <div class="row justify-content-center mb-4">
                
            </div>
            @include('admin.nav')
            <div class="row justify-content-center mt-4">
                <div class="col-md-12 text-center">
                    <div class="card">
                        <div class="card-body">
                            <table class="table" id="users">
                                <thead>
                                    <th>User</th>
                                    <th>Source Name</th>
                                    <th>Source ID</th>
                                    <th>Account Owner</th>
                                    <th>Account #</th>
                                    <th>Bank</th>
                                </thead>
                                <tbody>
                                    @foreach($sources as $source)
                                    <tr>
                                        <td class="text-left" style="max-width:100px"><a href="/admin/user/{{ $source['uid'] }}" target="_blank" >{{ $source["uid"] }}</a></td>
                                        <td class="text-left" style="max-width:70px">{{ $source["name"] }}</td>
                                        <td class="text-left" style="max-width:100px; overflow-x: hidden;">{{ $source["id"] }}</td>
                                        <td class="text-left">{{ $source["owner"] ?? "N/A" }}</td>
                                        <td class="text-left">{{ $source["number"] ?? "N/A" }}</td>
                                        <td style="min-width:100px; font-size: 11px;" class="text-left"><span class="bank" data-bank="{{ $source['id'] }}"><i class="fa fa-sync fa-spin"></span></td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section("js")
    <script type="text/javascript" src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/2.2.3/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/2.2.3/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/2.2.3/js/buttons.print.min.js"></script>
    <script type="text/javascript">
        $(document).ready( function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                }
            });

            $('#users').DataTable({
                orderCellsTop: true,
                fixedHeader: true,
                "pageLength": 50,
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                "drawCallback": function( settings ) {
                    grabBanks();
                }
            });

            function grabBanks(){
                $(".bank").each(function(){
                    var source = $(this).data("bank");
                    var item = $(this);

                    $.ajax({ 
                        type: 'post', 
                        url: "/boardingpass/customer/fundingSource/bank/detail",
                        dataType: 'json',
                        data:{
                            source: source
                        },
                        success: function (data) { 
                            var html = "<ul style='margin:0; padding:0; list-style:none;'><li>" + data.bank_name + "</li><li>" + data.bank_account_type + "</li><li>" + data.status + "</li></ul>";
                            item.html(html);
                        }
                    });
                });
            }
            
        });
    </script>
@endsection