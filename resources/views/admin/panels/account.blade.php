<div class="tab-content pt-3" id="pills-tabContent">
    <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
        <div class="">
            <div class="col-md-12">
                <div class="card bg-white small-shadow">
                    <div class="card-body">
                    </div>
                </div>
            </div>
        </div>
        <div class="">
            <div class="col-md-12">
            
            <h5 class="mt-4">My Information</h5>
            <div class="card bg-white small-shadow">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="form-label">First Name:</label>
                                <input type="email" name="fname" class="form-control disabled" value="{{ getFirstName($user->displayName) }}">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="form-label">Last Name:</label>
                                <input type="email" name="lname" class="form-control disabled" value="{{ getLastName($user->displayName) }}">
                            </div>
                        </div>
                        <div class="col-md-6">
                            @php
                                $regions = array(
                                'Africa' => DateTimeZone::AFRICA,
                                'America' => DateTimeZone::AMERICA,
                                'Antarctica' => DateTimeZone::ANTARCTICA,
                                'Aisa' => DateTimeZone::ASIA,
                                'Atlantic' => DateTimeZone::ATLANTIC,
                                'Europe' => DateTimeZone::EUROPE,
                                'Indian' => DateTimeZone::INDIAN,
                                'Pacific' => DateTimeZone::PACIFIC
                            );

                            $timezones = array();
                            foreach ($regions as $name => $mask)
                            {

                                $zones = DateTimeZone::listIdentifiers($mask);
                                foreach($zones as $timezone)
                                {
                                    // Lets sample the time there right now
                                    $time = new DateTime(NULL, new DateTimeZone($timezone));

                                    // Us dumb Americans can't handle millitary time
                                    $ampm = $time->format('H') > 12 ? ' '. $time->format('g:i a'). '' : $time->format('g:i a');

                                    // Remove region name and add a sample time

                                    $timezones[$name][$timezone] = substr($timezone, strlen($name) + 1) . ' - ' . $ampm;
                                }
                            }
                            @endphp

                            <label class="form-label">My Timezone</label>
                            <p>Venti transactions are recorded in U.S. New York EST/EDT. Changing your preferred timezone will only change how transaction data is displayed relative to you.</p>
                            <select id="timezone" class="form-control select2">
                                @foreach($timezones as $region => $list)
                                    <optgroup label="{{ $region }}">
                                        @foreach($list as $timezone => $name)
                                            <option value="{{ $timezone }}" @if($user->customClaims['timezone'] == $timezone) selected @endif>{{ str_replace("_"," ",$name) }}</option>
                                        @endforeach
                                    </optgroup>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <br>
            <button class="btn btn-success" type="button" id="updateAccountButton"><i class="fa fa-sync fa-spin"></i> Update Account</button>
            <br>
        </div>
    </div>
    <div class="col-md-12">
        <h5 class="mb-0 mt-4">Account Security</h5>
        <div class="card bg-white small-shadow mt-3">
            <div class="card-body">
                <div class="form-group">
                    <label class="form-label">Email:</label>
                    <input type="email" name="email" class="form-control disabled" value="{{ $user->email }}">
                </div>
                <div class="form-group">
                    <label class="form-label">My Cell Phone:</label>
                    <input type="tel" name="email" class="form-control disabled" value="{{ $user->customClaims['phoneNumber'] ?? '' }}">
                </div>
                <div class="form-group">
                    <label class="form-label">Update Password:<br><span class="d-block text-small">Use the button below to request a password reset.</span></label>
                    <br>
                    <button class="btn btn-black" id="requestNewPassword">Password Change</button>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <h5 class="mb-0 mt-4">Close Account</h5>
        <div class="card bg-white small-shadow mt-3">
            <div class="card-body">
                <div class="form-group">
                    <p>To proceed with account deletion, please use the button below. Our team will evaluate your account to determine if there are any outstanding orders/balances.</p>
                    <a class="btn btn-warning" type="button" href="mailto:admin@venti.co?subject=Delete My Account">Request Deletion</a>
                </div>
            </div>
        </div>
    </div>
    </div>
</div> 