<div class="tab-content pt-3" id="pills-tabContent">
	<div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
		<ul class="nav nav-pills nav-fill flex d-flex mb-3 nav-justified">
		  <li class="nav-item">
		    <a class="nav-link active  text-center" aria-current="page" href="#pills-all-transactions" data-bs-toggle="pill" data-bs-target="#pills-all-transactions" type="button" role="tab" aria-controls="pills-all-transactions">All Transactions</a>
		  </li>
		  <li class="nav-item">
		    <a class="nav-link text-center" aria-current="page" href="#pills-all-snapshots" data-bs-toggle="pill" data-bs-target="#pills-all-snapshots" type="button" role="tab" aria-controls="pills-all-snapshots">Snapshots</a>
		  </li>
		</ul>
		<div class="tab-content">
		  <div id="pills-all-transactions" class="tab-pane fade in active show">
		    @include('boardingpass.panels.transactions.index')
		  </div>
		  <div id="pills-all-snapshots" class="tab-pane fade">
		    @include('boardingpass.panels.transactions.snapshots')
		  </div>
		</div>
	</div>
</div>