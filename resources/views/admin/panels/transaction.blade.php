<div class="modal fade" id="transactModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true" style="background-image: linear-gradient(112.1deg, rgba(253, 118, 78,0.4) 11.4%, rgba(254, 174, 27,0.7) 70.2%);">
	<div class="modal-dialog modal-dialog-centered">
	    <div class="modal-content" >
			<div class="modal-header" style="border-bottom:0; padding-bottom:0">
				<h5 style="padding:0; margin:0">Boarding Pass Points</h5>
				<button type="button" class="btn-close text-white" data-bs-dismiss="modal" aria-label="Close" style=""></button>
			</div>
		    <div class="modal-body">
		        <form id="transactForm">
		          <div class="row">
		            <div class="col-md-12 mb-3 text-center" style="flex-direction: row;
		              display: flex;
		              padding-top: 20px;
		              padding-bottom: 20px;
		              justify-content: space-between;
		              align-items: center;
		              width: 300px;
		              margin: 0 auto;">
		              <div>
		                <img src="/assets/img/icons/bank.png" style="width: 100%; max-width:85px;">
		              </div>
		              <div>
		                <i class="fa fa-arrow-right" style="font-size:36px;"></i>
		              </div>
		              <div>
		                <img src="/assets/img/icons/boarding-pass.png" style="width: 100%; max-width:85px;">
		              </div>
		            </div>
		          </div>
		          <div class="row mb-3">
		          	<div class="col-md-12">
		          		<label class="form-label">Transaction Type</label>
		          		<select class="form-control required">
		          			<option value="deposit">Deposit</option>
		          			<option value="withdraw">Withdraw</option>
		          		</select>
		          	</div>
		          </div>
		          <div class="row mb-3">
		            <div class="col-md-12">
		              <label class="form-label">Transaction Amount</label>
		              <div class="input-group">
		                <span class="btn btn-white" style="border:1px solid #ced4da"><i class="fa fa-dollar" style="padding-top:8px"></i></span>
		                <input type="number" id="depositAmount" name="depositAmount" class="form-control required" placeholder="10" max="5000" maxlength="4" inputmode="numeric" oninput="this.value=this.value.slice(0,this.maxLength)">
		              </div>
		            </div>
		          </div>
		          <div class="row mb-3">
		            <div class="col-md-12">
		              <label class="form-label">Transction Description</label>
		              <textarea class="form-control required" id="description"></textarea>
		            </div>
		          </div>
		          <div class="row">
		            <div class="col-md-12">
		              <span style="display:block; width:100%; margin-bottom: 10px;">
		                We estimate your funds to arrive <span id="fundEstimate" style="">in 3 to 5 business days</span>.<sup>*</sup>
		              </span>
		              <button class="btn btn-success" type="button" id="finalizeDeposit"><i class="fa fa-sync fa-spin"></i> Submit</span> From My Bank</button>
		            </div>
		          </div>
		        </form>
      		</div>
	    </div>
	</div>
</div>