@extends('layouts.curator-master')

@section('css')
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<style type="text/css">
.profile-pic {
    width: 100%;
    height: 100%;
    cursor: pointer;
    border-radius: 50%;
}

.file-upload {
    display: none;
}
.circle {
    width: 125px;
    height: 125px;
    margin: 0 auto;
    display: block;
}

.p-image {
  width: 100%;
  text-align: center;
  color: #666666;
  transition: all .3s cubic-bezier(.175, .885, .32, 1.275);
}
.p-image:hover {
  transition: all .3s cubic-bezier(.175, .885, .32, 1.275);
}
.upload-button {
  font-size: 1.2em;
}

.upload-button:hover {
  transition: all .3s cubic-bezier(.175, .885, .32, 1.275);
  color: #999;
}
@media screen and (max-width:  600px){
    .mobMin{
        min-height: 200px;
    }
}
</style>
<link rel="stylesheet" href="/assets/css/intlTelInput.min.css">
@endsection

@section("content")
   <div id="page-content">
      <div class="gradient-wrap">
         <!--  HERO -->
        <div class="black position-relative hero-section mb-4" style="margin: 2em 4em;">
            <div class="row">
                <div class="col-md-2">
                    <ul class="nav nav-pills mb-3 full-width" id="pills-tab" role="tablist">
                      <li class="nav-item" role="presentation">
                        <button class="nav-link active" id="pills-settings-tab" data-bs-toggle="pill" data-bs-target="#pills-settings" type="button" role="tab" aria-controls="pills-settings" aria-selected="false">Account</button>
                      </li>
                      <li class="nav-item" role="presentation">
                        <button class="nav-link" id="pills-home-tab" data-bs-toggle="pill" data-bs-target="#pills-home" type="button" role="tab" aria-controls="pills-home" aria-selected="true">Boarding Pass</button>
                      </li>
                      <li class="nav-item" role="presentation">
                        <button class="nav-link" id="pills-funding-tab" data-bs-toggle="pill" data-bs-target="#pills-funding" type="button" role="tab" aria-controls="pills-funding" aria-selected="false">Funding Sources</button>
                      </li>
                      <li class="nav-item" role="presentation">
                        <button class="nav-link" id="pills-orders-tab" data-bs-toggle="pill" data-bs-target="#pills-orders" type="button" role="tab" aria-controls="pills-orders" aria-selected="false">Ledger</button>
                      </li>
                      
                    </ul>
                </div>
                <div class="col-md-8">
                    @include(admin.panels.account) 
                    @include(boardingpass.panels.funding) 
                    @include(boardingpass.panels.transactions)
                    @include(admin.panels.boarding-pass)
                </div>
            </div>
        </div>
    </div>
</div>
@include(admin.panels.transaction) 
@endsection

@section('js')

<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
@include("boardingpass.dashboard-js")
@if(env('APP_ENV') != "local")
    <script>
        !function(f,b,e,v,n,t,s)
        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};
        if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
        n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t,s)}(window, document,'script',
        'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '1169331916805166');
        fbq('track', 'CompleteRegistration');
    </script>
    <noscript><img height="1" width="1" style="display:none"
    src="https://www.facebook.com/tr?id=1169331916805166&ev=PageView&noscript=1"
    /></noscript>
@endif
@endsection