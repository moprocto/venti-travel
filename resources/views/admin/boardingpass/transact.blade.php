@extends('layouts.boardingpass.master')

@section('css')
	<style type="text/css">
		.card{
			background: transparent;
			border: none;
		}
		
		.nav-item .nav-link{
			width: 100%;
			text-align: left;
			color: black;
		}
		.apexcharts-toolbar{
			display: none !important;
		}
		table > *{
			font-size: 12px;
		}

	</style>
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/2.2.3/css/buttons.dataTables.min.css">
<style>
	tr.text-grey > *{
		color: grey;
	}
</style>
@endsection

@php


@endphp

@section('content')
	<div id="page-content">
	    <div class="gradient-wrap">
		    <div class="container hero-section mb-4 mt-4">
		    	@include('admin.nav')
		        <div class="row justify-content-center">
		            <div class="col-md-8 justify-content-center">
		            	<div class="card bg-white">
		            		<div class="card-body">
				            	<form method="POST" action="{{ route('admin-transact') }}">
				            		@CSRF
				            		<div class="form-group">
				            			<label class="form-label">Select Customer ID</label>
				            			<input type="text" name="customer" class="form-control required" required="">
				            		</div>
				            		<div class="form-group">
				            			<label class="form-label">Select Transaction Type</label>
				            			<select name="type" class="form-control required">
				            				<option value="points-deposit">Points Deposit</option>
				            				<option value="points-withdraw">Points Withdraw</option>
				            				<option value="cash-deposit">Cash Deposit from Venti to Wallet</option>
				            				<option value="cash-withdraw">Cash Withdraw from Wallet to Venti</option>
				            			</select>
				            		</div>
				            		<div class="form-group">
				            			<label class="form-label">Select Transaction Amount</label>
				            			<input type="number" name="amount" id="amount" class="form-control required" step=".01">
				            		</div>
				            		<div class="form-group">
				            			<label class="form-label">Note</label>
				            			<br>
				            			<div class="button-group">
				            				<button type="button" class="btn btn-black" id="addCreditNote">+Credit</button>
				            			</div>
				            			<br>
				            			<textarea id="note" name="note" class="form-control"></textarea>
				            		</div>
				            		<div class="form-group">
				            			<label class="form-label">Send Email</label>
				            			<input type="checkbox" name="email" value="1">
				            		</div>
				            		<div class="form-group">
				            			<label class="form-label">Taxable</label>
				            			<input type="checkbox" name="taxable" value="1">
				            		</div>
				            		<div class="form-group">
				            			<input type="submit" class="btn btn-success" value="Submit">
				            		</div>
				            	</form>
			            	</div>
		            	</div>
		            </div>
		        </div>
		    </div>
		</div>
	</div>
@endsection

@section('js')
	<script>
		$("#addCreditNote").click(function(){
			$("#note").val("VIP Initial Deposit Credit");
			$("#amount").val(20);
		})
	</script>
@endsection