@extends('layouts.curator-master')

@section('css')
	<style type="text/css">
		.card{
			background: transparent;
			border: none;
		}
		.nav-pills{
			display: block;
		}
		.nav-item .nav-link{
			width: 100%;
			text-align: left;
			color: black;
			max-width: 200px;
		}
		
		table > *{
			font-size: 12px;
		}

	</style>
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.13.6/css/dataTables.bootstrap5.min.css">
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/2.4.1/css/buttons.bootstrap5.min.css">
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.5.0/css/responsive.dataTables.min.css">
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/rowreorder/1.4.1/css/rowReorder.dataTables.min.css"/>
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/2.2.3/css/buttons.dataTables.min.css">
<style>
	tr.text-grey > *{
		color: grey;
	}
</style>
@endsection

@php



@endphp

@section('content')
	<div id="page-content">
	    <div class="gradient-wrap">
		    <div class="container hero-section mb-4 mt-4">
		    	<div class="row justify-content-center mb-4">
	        		<div class="col-md-12">
	        			<ul class="nav nav-pills navtab-bg nav-justified" style="margin-bottom:10px">
					        <li class="nav-item" style="max-width:100px;">
					            <a href="/admin/" class="nav-link">
					                CUSTOMERS
					            </a>
					        </li>
					        <li class="nav-item" style="max-width:100px;">
					            <a href="/admin/users" class="nav-link">
					                USERS
					            </a>
					        </li>
					        <li class="nav-item" style="max-width:100px;">
					            <a href="/admin/transactions" class="nav-link">
					                TRANSACTIONS
					            </a>
					        </li>
					    </ul>
	        		</div>
	        	</div>
		        <div class="row justify-content-center">
		            <div class="col-md-8">
		            	<div class="row">
		            		<div class="col-md-12">
		            			<div class="card bg-white">
		            		<div class="card-body">
		            			<div class="tab-content" id="pills-tabContent">
								<div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
									<table class="table" id="transactions">
										<thead>
											<th>Customer</th>
											<th class="text-left">Transaction ID</th>
											<th>Amount</th>
											<th>From</th>
											<th>To</th>
											<th>Created At</th>
										</thead>
										<tbody>
											@foreach($transactions as $transaction)
												@php
													$statement = $transaction["statement"] ?? "false";
													if($statement == "statement"){
														$statement = "statement";
													}
												@endphp
												<tr>
													<td><a href="/admin/user/{{ $transaction['uid']}}" target="_blank">{{ substr($transaction["uid"],0,8) }}...</a></td>
													<td>{{ $statement }}</td>
													<td><a href="./snapshots/delete/{{ $transaction['snapshotID'] }}" target="_blank">delete {{ $transaction['snapshotID'] }}</a></td>
												</tr>
											@endforeach
										</tbody>
									</table>
								</div>
								<div class="tab-pane fade" id="pills-tools" role="tabpanel" aria-labelledby="pills-profile-tab">

								</div>
								<div class="tab-pane fade" id="pills-community" role="tabpanel" aria-labelledby="pills-contact-tab">
								</div>
								<div class="tab-pane fade" id="pills-settings" role="tabpanel" aria-labelledby="pills-contact-tab">

								</div>
							</div>
		            		</div>
		            	</div>
		            		</div>
		            	</div>
		            	
		            </div>
		            <div class="col-md-4">
		            	<div class="row">
		            		<div class="col-md-12 pt-3">
				            	<div class="card bg-white">
				            		<div class="card-body">
				            			<ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
										  <li class="nav-item" role="presentation">
										    <button class="nav-link active" id="pills-home-tab" data-bs-toggle="pill" data-bs-target="#pills-home" type="button" role="tab" aria-controls="pills-home" aria-selected="true">HOME</button>
										  </li>
										  <li class="nav-item" role="presentation">
										    <button class="nav-link" id="pills-tools-tab" data-bs-toggle="pill" data-bs-target="#pills-tools" type="button" role="tab" aria-controls="pills-tools" aria-selected="false">BOOK</button>
										  </li>
										  <li class="nav-item" role="presentation">
										    <button class="nav-link" id="pills-community-tab" data-bs-toggle="pill" data-bs-target="#pills-community" type="button" role="tab" aria-controls="pills-community" aria-selected="false">ORDERS</button>
										  </li>
										  <li class="nav-item" role="presentation">
										    <button class="nav-link" id="pills-settings-tab" data-bs-toggle="pill" data-bs-target="#pills-settings" type="button" role="tab" aria-controls="pills-settings" aria-selected="false">SETTINGS</button>
										  </li>
										</ul>
				            		</div>
				            	</div>
			            	</div>
		            	</div>
		            </div>
		        </div>
		    </div>
		</div>
	</div>
@endsection

@section('js')
	<script type="text/javascript" src="/assets/js/apex.init.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/2.2.3/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/2.2.3/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/2.2.3/js/buttons.print.min.js"></script>
    <script type="text/javascript">
        $(document).ready( function () {
            $('#transactions').DataTable({
            	orderCellsTop: true,
        		fixedHeader: true,
            	"pageLength": 50,
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                order: [[5, 'desc']]
            });
        });
    </script>
@endsection