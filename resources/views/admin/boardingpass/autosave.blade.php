@extends('layouts.curator-master')

@section('css')
	<style type="text/css">
		.card{
			background: transparent;
			border: none;
		}

		.nav-item .nav-link{
			width: 100%;
			text-align: left;
			color: black;
		}
		
		table > *{
			font-size: 12px;
		}

	</style>
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.13.6/css/dataTables.bootstrap5.min.css">
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/2.4.1/css/buttons.bootstrap5.min.css">
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.5.0/css/responsive.dataTables.min.css">
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/rowreorder/1.4.1/css/rowReorder.dataTables.min.css"/>
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/2.2.3/css/buttons.dataTables.min.css">
<style>
	tr.text-grey > *{
		color: grey;
	}
</style>
@endsection

@php



@endphp

@section('content')
	<div id="page-content">
	    <div class="gradient-wrap">
		    <div class="container hero-section mb-4 mt-4">
		    	@include('admin.nav')
		        <div class="row justify-content-center">
		            <div class="col-md-12">
		            	<div class="row">
		            		<div class="col-md-12">
		            			<div class="card bg-white">
		            		<div class="card-body">
		            			<div class="tab-content" id="pills-tabContent">
								<div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
									<div class="inline-flex mb-4">
							            <a href="/admin/transactions/statements" class="btn btn-primary">
							                STATEMENTS
							            </a>
							            <a href="/admin/transactions/create" class="btn btn-primary">
							                MAKE TRANSACTION
							            </a>
							            <a href="/admin/transactions/orders/refund" class="btn btn-primary">
							                REFUND ORDER
							            </a>
							        </div>
									<table class="table" id="transactions">
										<thead>
											<th>Customer</th>
											<th class="text-left">Frequency</th>
											<th>Source</th>
											<th>Amount</th>
											<th>Start Date</th>
											<th>Created At</th>
											<th>Last Successful Run</th>
										</thead>
										<tbody>
											@foreach($autosaves as $autosave)
												<tr>
													<td><a href="/admin/user/{{ $autosave['uid'] }}" target="_blank">{{ $autosave["uid"] }}</a></td>
													<td class="text-left">{{ $autosave["frequency"] }}</td>
													<td>{{ $autosave["source"] }}</td>
													<td>${{ $autosave["amount"] }}</td>
													<td>{{ $autosave["startDate"] }}</td>
													<td></td>
													<td>{{ $autosave["lastSuccessfulRun"] }}</td>
												</tr>
											@endforeach
										</tbody>
									</table>
								</div>
								<div class="tab-pane fade" id="pills-tools" role="tabpanel" aria-labelledby="pills-profile-tab">

								</div>
								<div class="tab-pane fade" id="pills-community" role="tabpanel" aria-labelledby="pills-contact-tab">
								</div>
								<div class="tab-pane fade" id="pills-settings" role="tabpanel" aria-labelledby="pills-contact-tab">

								</div>
							</div>
		            		</div>
		            	</div>
		            		</div>
		            	</div>
		            </div>
		        </div>
		    </div>
		</div>
	</div>
@endsection

@section('js')
	<script type="text/javascript" src="/assets/js/apex.init.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/2.2.3/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/2.2.3/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/2.2.3/js/buttons.print.min.js"></script>
    <script type="text/javascript">
        $(document).ready( function () {
            $('#transactions').DataTable({
            	orderCellsTop: true,
        		fixedHeader: true,
            	"pageLength": 50,
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                order: [[5, 'desc']]
            });
        });
    </script>
@endsection