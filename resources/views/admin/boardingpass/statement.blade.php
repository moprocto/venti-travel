@extends('layouts.boardingpass.master')

@section('css')
	<style type="text/css">
		.card{
			background: transparent;
			border: none;
		}
		
		.nav-item .nav-link{
			width: 100%;
			text-align: left;
			color: black;
		}
		.apexcharts-toolbar{
			display: none !important;
		}
		table > *{
			font-size: 12px;
		}

	</style>
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/2.2.3/css/buttons.dataTables.min.css">
<style>
	tr.text-grey > *{
		color: grey;
	}
</style>
@endsection

@php


@endphp

@section('content')
	<div id="page-content">
	    <div class="gradient-wrap">
		    <div class="container hero-section mb-4 mt-4">
		    	@include('admin.nav')
		        <div class="row justify-content-center">
		            <div class="col-md-8 justify-content-center">
		            	<div class="card bg-white">
		            		<div class="card-body">
				            	<form method="POST" action="/admin/transactions/statements/{{ $statement['snapshotID'] }}">
				            		@CSRF
				            		{{ \Carbon\Carbon::parse($statement["timestamp"])->format("F, d Y") }}
				            		<input type="hidden" name="id" value="{{ $statement['snapshotID'] }}">
				            		<div class="form-group">
				            			<label class="form-label">Month</label>
				            			<input type="text" name="month" id="month" class="form-control required" required="" value="{{ $statement['month'] ?? '' }}">
				            		</div>
				            		<div class="form-group">
				            			<label class="form-label">Year</label>
				            			<input type="text" name="year" id="year" class="form-control required" required="" value="{{ $statement['year'] ?? '' }}">
				            		</div>
				            		<div class="form-group">
				            			<label class="form-label">Timestamp</label>
				            			<input type="text" name="timestamp" id="timestamp" class="form-control required" required="" value="{{ $statement['timestamp'] ?? '' }}">
				            		</div>
				            		<div class="form-group">
				            			<input type="submit" name="submit" class="btn btn-success" value="SAVE">
				            		</div>
				            		<button type="button" id="dec1" class="btn btn-black mt-4">December 1</button>
				            	</form>
			            	</div>
		            	</div>
		            </div>
		        </div>
		    </div>
		</div>
	</div>
@endsection

@section('js')
	<script>
		$("#dec1").click(function(){
			$("#month").val("November");
			$("#year").val("2023");
			$("#timestamp").val("{{ \Carbon\Carbon::parse('December 1, 2023')->timestamp }}");
		})
	</script>
@endsection