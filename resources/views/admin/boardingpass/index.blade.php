@extends('layouts.boardingpass.master')

@section('css')
	<style type="text/css">
		.card{
			background: transparent;
			border: none;
		}
		.nav-pills{
			display: block;
		}
		.nav-item .nav-link{
			width: 100%;
			text-align: left;
			color: black;
			max-width: 200px;
		}
		.apexcharts-toolbar{
			display: none !important;
		}
		table > *{
			font-size: 12px;
		}

	</style>
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/2.2.3/css/buttons.dataTables.min.css">
<style>
	tr.text-grey > *{
		color: grey;
	}
</style>
@endsection

@php

$facebook = 0;
$instagram = 0;
$referrals = 0;

foreach($vips as $vip){
	if(array_key_exists("Source", $vip["fields"])){
		if($vip["fields"]["Source"] == "facebook" || $vip["fields"]["Source"] == "{{site_source_name}}" ){
			$facebook++;
		}
		if($vip["fields"]["Source"] == "instagram"){
			$instagram++;
		}
		if(strlen($vip["fields"]["Source"]) >= 15){
			$referrals++;
		}


	}

}

@endphp

@section('content')
	<div id="page-content">
	    <div class="gradient-wrap">
		    <div class="container hero-section mb-4 mt-4">
		        <div class="row justify-content-center">
		            <div class="col-md-8">
		            	<div class="row mb-3">
		            		<div class="col-md-3">
		            			<div class="card bg-white">
		            				<div class="card-body text-center">
		            					<h2>{{ sizeof($vips) }}</h2>
		            					<p>Sign-Ups</p>
		            				</div>
		            			</div>
		            		</div>
		            		<div class="col-md-3">
		            			<div class="card bg-white">
		            				<div class="card-body text-center">
		            					<h2>{{ $facebook }}</h2>
		            					<p>From Facebook</p>
		            				</div>
		            			</div>
		            		</div>
		            		<div class="col-md-3">
		            			<div class="card bg-white">
		            				<div class="card-body text-center">
		            					<h2>{{ $instagram }}</h2>
		            					<p>From Instagram</p>
		            				</div>
		            			</div>
		            		</div>
		            		<div class="col-md-3">
		            			<div class="card bg-white">
		            				<div class="card-body text-center">
		            					<h2>{{ $referrals }}</h2>
		            					<p>From Referrals</p>
		            				</div>
		            			</div>
		            		</div>
		            	</div>
		            	<div class="row">
		            		<div class="col-md-12">
		            			<div class="card bg-white">
		            		<div class="card-body">
		            			<div class="tab-content" id="pills-tabContent">
								<div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
									<table class="table" id="users">
										<thead>
											<th>ID</th>
											<th>Email</th>
											<th>Sign-Up Source</th>
											<th>Location</th>
											<th>Referral Code</th>
											<th>Referrals</th>
											<th>Created At</th>
										</thead>
										<tbody>
											@foreach($vips as $vip)
												@php

												$fields = $vip["fields"]; 
												$refs = 0;
												$code = $vip["code"];

												if(array_key_exists("Source", $fields)){
													foreach($vips as $vipl){
														if(array_key_exists("Source", $vipl["fields"])){
															if($vipl["fields"]["Source"] == $code){
																$refs++;
															}
														}
													}
												}

												@endphp
												<tr>
													<td>{{ $vip["id"] }}</td>
													<td>{{ $vip["email"] }}</td>
													<td>{{ $fields["Source"] ?? "N/A" }}</td>
													<td>{{ $fields["Location"] ?? "N/A" }}</td>
													<td><a href="/boardingpass/track/{{$code}}" target="_blank">{{ $code }}</a></td>
													<td>{{ $refs }}</td>
													<td>{{ $vip["createdAt"] ?? "N/A" }}</td>
												</tr>
											@endforeach
										</tbody>
									</table>
								</div>
								<div class="tab-pane fade" id="pills-tools" role="tabpanel" aria-labelledby="pills-profile-tab">

								</div>
								<div class="tab-pane fade" id="pills-community" role="tabpanel" aria-labelledby="pills-contact-tab">
								</div>
								<div class="tab-pane fade" id="pills-settings" role="tabpanel" aria-labelledby="pills-contact-tab">

								</div>
							</div>
		            		</div>
		            	</div>
		            		</div>
		            	</div>
		            	
		            </div>
		            <div class="col-md-4">
		            	<div class="row">
		            		<div class="col-md-12 pt-3">
				            	<div class="card bg-white">
				            		<div class="card-body">
				            			<ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
										  <li class="nav-item" role="presentation">
										    <button class="nav-link active" id="pills-home-tab" data-bs-toggle="pill" data-bs-target="#pills-home" type="button" role="tab" aria-controls="pills-home" aria-selected="true">HOME</button>
										  </li>
										  <li class="nav-item" role="presentation">
										    <button class="nav-link" id="pills-tools-tab" data-bs-toggle="pill" data-bs-target="#pills-tools" type="button" role="tab" aria-controls="pills-tools" aria-selected="false">BOOK</button>
										  </li>
										  <li class="nav-item" role="presentation">
										    <button class="nav-link" id="pills-community-tab" data-bs-toggle="pill" data-bs-target="#pills-community" type="button" role="tab" aria-controls="pills-community" aria-selected="false">ORDERS</button>
										  </li>
										  <li class="nav-item" role="presentation">
										    <button class="nav-link" id="pills-settings-tab" data-bs-toggle="pill" data-bs-target="#pills-settings" type="button" role="tab" aria-controls="pills-settings" aria-selected="false">SETTINGS</button>
										  </li>
										</ul>
				            		</div>
				            	</div>
			            	</div>
		            	</div>
		            </div>
		        </div>
		    </div>
		</div>
	</div>
@endsection

@section('js')
	<script type="text/javascript" src="/assets/js/apex.init.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/2.2.3/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/2.2.3/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/2.2.3/js/buttons.print.min.js"></script>
    <script type="text/javascript">
        $(document).ready( function () {
            $('#users').DataTable({
            	orderCellsTop: true,
        		fixedHeader: true,
            	"pageLength": 50,
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]
            });
        });
    </script>
@endsection