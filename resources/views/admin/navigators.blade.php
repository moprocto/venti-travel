@extends("layouts.curator-master")


@section('css')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.12.1/css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/2.2.3/css/buttons.dataTables.min.css">
<style>
	tr.text-grey > *{
		color: grey;
	}
</style>
@endsection

@section('content')
<div id="page-content">
    <div class="gradient-wrap">
        <div class="container hero-section mb-4 mt-4">
	    	<div class="row justify-content-center mb-4">
	        	<div class="col-md-3">
    				<div class="card">
    					<div class="card-body text-center">
    						<h2>{{ sizeof($navigators) - $invited - $waitlisted }}<br>To Review</h2>
    						<ul>
    							<li>Applications: {{ sizeof($navigators) }}</li>
    							<li>Allstars: {{ $allstars }}</li>
    							<li>Pro: {{ $pro }}</li>
    							<li>Invited: {{ $invited }}</li>
    							<li>Waitlisted: {{ $waitlisted}}</li>
    						</ul>
    						<p></p>
    						<p></p>
    					</div>
    				</div>
    			</div>
    			<div class="col-md-3">
    				<div class="card">
    					<div class="card-body text-center">
    						<h2>${{ number_format($pro  * 9.99,0) }}<br><span style="font-size:18px;">P-MRR at $9.99</span></h2>
    						<ul>
    							@for($i = 9.99; $i < 19.99; $i++)
    								<li>${{ number_format($pro  * $i * 12,0) }} P-ARR at ${{ $i }}</li>
    							@endfor
    						</ul>
    						<p></p>
    						<p></p>
    					</div>
    				</div>
    			</div>
	    	</div>
		    <div class="row justify-content-center mt-4">
            	<div class="col-md-12 text-center">
            		<div class="card">
            			<div class="card-body">
            				<table class="table" id="users">
		            			<thead>
		            				<th class="text-left">Applicant</th>
		            				<th class="text-left">Countries</th>
		            				<th>Experience</th>
		            				<th>Allstar</th>

		            				<th>Created At</th>
		            				<th>Pro</th>
		            				<th>Status</th>
		            				<th></th>
		            			</thead>
		            			<tbody>
		            				@foreach($navigators as $navigator)
		            					<tr class="@if($navigator['status'] != 'pending') text-grey @endif">
		            						<td class="text-left">
		            							{{ $navigator["name"] }}<br>
		            							{{ $navigator["email"] }}<br>
		            							{{ $navigator["country"] ?? "No Country" }}<br>
		            							{{ $navigator["website"] }}<br>
		            							{{ $navigator["friends"] ?? "No Friends" }}
		            						</td>
		            						<td class="text-left">{{ $navigator["countries"] }}</td>
		            						<td class="text-left">{{ $navigator["experience"] }}</td>
		            						<td>{{ $navigator["allstar"] }}</td>

		            						<td>{{ $navigator["applied"] ?? "N/A" }}</td>
		            						<td class="text-center">{{ $navigator["pro"] ?? "N/A" }}</td>
		            						<td>{{ $navigator["status"] }} </td>
		            						<td class="text-center">
		            							@if($navigator["status"] == "pending")
		            							<a href="./navigators/schedule/{{ $navigator['applied'] .'-'. $navigator['email'] }}" target="_blank">👍</a>
		            							@endif
		            							<br>
		            							<a href="./navigators/waitlist/{{ $navigator['applied'] .'-'. $navigator['email'] }}" target="_blank">⏰</a>
		            							@if($navigator["status"] == "invited")
		            							<br>
		            							<a href="./navigators/accept/{{ $navigator['applied'] .'-'. $navigator['email'] }}" target="_blank">[Y]</a>
		            							@endif
		            							<!--
		            							<br>
		            							<a href="./navigators/delete/{{ $navigator['applied'] .'-'. $navigator['email'] }}" target="_blank">[X]</a>
		            						-->
		            						</td>
		            					</tr>
		            				@endforeach
		            			</tbody>
		            		</table>
            			</div>
            		</div>
            	</div>
	        </div>
	    </div>
	</div>
</div>
@endsection

@section("js")
    <script type="text/javascript" src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/2.2.3/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/2.2.3/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/2.2.3/js/buttons.print.min.js"></script>
    <script type="text/javascript">
        $(document).ready( function () {
            $('#users').DataTable({
            	orderCellsTop: true,
        		fixedHeader: true,
            	"pageLength": 50,
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]
            });
        });
    </script>
@endsection