@extends('layouts.curator-master')

@section('css')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.12.1/css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/2.2.3/css/buttons.dataTables.min.css">
@endsection

@section('content')
<div id="page-content">
    <div class="gradient-wrap">
        <div class="container hero-section mb-4 mt-4">
        	<div class="row justify-content-center mb-4">
        		<div class="col-md-12">
        			<ul class="nav nav-pills navtab-bg nav-justified" style="margin-bottom:10px">
				        <li class="nav-item" style="max-width:100px;">
				            <a href="/admin/" class="nav-link">
				                CUSTOMERS
				            </a>
				        </li>
				        <li class="nav-item" style="max-width:100px;">
				            <a href="/admin/users" class="nav-link">
				                USERS
				            </a>
				        </li>
				        <li class="nav-item" style="max-width:100px;">
				            <a href="/admin/transactions" class="nav-link">
				                TRANSACTIONS
				            </a>
				        </li>
				        <li class="nav-item" style="max-width:100px;">
				            <a href="/admin/searches" class="nav-link">
				                SEARCHES
				            </a>
				        </li>
				    </ul>
        		</div>
        	</div>
	        

		    	<div class="row justify-content-center mb-4">
		        	<div class="col-md-3">
	    				<div class="card">
	    					<div class="card-body text-center">
	    						<h2>{{ sizeof($users) }}<br>Users</h2>
	    					</div>
	    				</div>
	    			</div>
		    	</div>
		    	<div class="row justify-content-center mt-4">
	            	<div class="col-md-12 text-center">
	            		<div class="card">
	            			<div class="card-body">
	            				<table class="table" id="users">
			            			<thead>
			            				<th class="text-left">User</th>
			            				<th class="text-center">App Paid</th>
			            				<th class="text-center">Email Verified</th>
			            				<th class="text-center">SMS Verified</th>
			            				<th class="text-center">Tier Selected</th>
			            				<th class="text-center">Created At</th>
			            				<th class="text-center">Last Login</th>
			            			</thead>
			            			<tbody>
			            				@foreach($users as $user)
			            					@php
			            						$customClaims = $user["customClaims"] ?? false;
			            						$appPaid = $customClaims["appPaid"] ?? false;
			            						$referralKey = "N/A";
			            						

			            						if(array_key_exists("referralKey", $customClaims)){
			            							$referralKey = $customClaims["referralKey"];
			            						}
			            						$lastLogin = $user["metadata"]->lastLoginAt ? $user["metadata"]->lastLoginAt->format("m-d-Y") : false;
			            						$smsVerified = $customClaims["smsVerified"] ?? false;
			            					@endphp
			            					<tr>
			            						<td class="text-left">
			            							<a href="/admin/user/{{ $user['uid'] }}" target="_blank">{{ $user['displayName'] }}</a> <br>
			            							{{ $user["email"] }}
			            						</td>
			            						<td class="text-center">
			            								{{ $appPaid ? "PAID" : "UNPAID" }}
			            						</td>
			            						<td>{{ $user["emailVerified"] ? "Y" : "N" }}</td>
			            						<td>{{ $smsVerified ? "Y" : "N" }}</td>
			            						<td class="text-center">{{ $customClaims["subscription"] ?? "N/A" }}</td>
			            						<td class="text-center">{{ $user["metadata"]->createdAt->format("m-d-Y") }}</td>
			            						<td class="text-center">{{ $lastLogin }}</td>
			            					</tr>
			            				@endforeach
			            			</tbody>
			            		</table>
	            			</div>
	            		</div>
	            	</div>
		        </div>
	    </div>
	</div>
</div>
@endsection

@section("js")
    <script type="text/javascript" src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/2.2.3/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/2.2.3/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/2.2.3/js/buttons.print.min.js"></script>
    <script type="text/javascript">
        $(document).ready( function () {

        	$.ajaxSetup({
		        headers: {
		            'X-CSRF-TOKEN': '{{ csrf_token() }}'
		        }
		    });

            $('#users').DataTable({
            	"pageLength": 50,
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                "drawCallback": function( settings ) {
                	grabProfiles();
			    }
            });

            

            $("body").on("click",".paginate_button", function(){
            	queryMailchimp();
            });

            function grabProfiles(){

            }

            function grabDwolla(){
            	
            }

            function calculateUserProgress(){
            	


            }

            function calculateTopBalances(){
            	
            }

            function queryMailchimp(){
            	/*
	            	$(".getEmailStatus").each(function(){
		            	var span = $(this);
		            	var email = span.attr("data-email");

		            	$.ajax({ 
		                    type: 'GET', 
		                    url: "/admin/mailchimpStatus/" + email,
		                    dataType: 'json',
		                    success: function (data) { 
		                    	if(data == true){
		                    		span.text("Y");
		                    	}
		                    }
		                });
		            });
		            */
            }
        });
    </script>
@endsection