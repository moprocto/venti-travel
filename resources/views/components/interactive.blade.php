<div class=" text-left">
	<div class="col-md-12 mb-4 ">
		<input type="text" id="pool-amount" class="calculate" 
          data-provide="slider"
          data-slider-ticks="[0, 1, 2, 3, 4]"
          data-slider-ticks-labels='["$0", "$500", "$1000", "$2000", "$3000+"]'
          data-slider-min="1"
          data-slider-max="6"
          data-slider-step="1"
          data-slider-value="0"
          data-slider-tooltip="hide" />
    </div>
    <div class="col-md-12">
    	<h5 id="reward">Slide to reveal</h5>
    </div>
</div>