<section class="elementor-section elementor-inner-section elementor-element elementor-element-ff376e1 elementor-section-boxed elementor-section-gap-beside-yes elementor-section-height-default elementor-section-height-default elementor-section-column-vertical-align-stretch" data-id="fcaac27" data-element_type="section">
   <div class="elementor-container elementor-column-gap-extended">
      <div class="animated-fast col col-sm-12 col-md-6 order-md-2 order-lg-1 elementor-column elementor-inner-column elementor-element elementor-element-aaf2a44" data-id="5abff54" data-element_type="column" data-settings="{&quot;animation&quot;:&quot;salaFadeInUp&quot;}">
         <div class="elementor-widget-wrap elementor-element-populated">
            <div class="elementor-element elementor-element-3161a9a animated-fast salasvg elementor-invisible elementor-widget elementor-widget-image" data-id="3161a9a" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_delay&quot;:500}" data-widget_type="image.default">
               <div class="elementor-widget-container">
                  <img width="460" height="460" src="/assets/savings/img/oval.svg" class="attachment-full size-full" alt="" loading="lazy"/>                              
               </div>
            </div>
            <div class="elementor-element elementor-element-2e7e20b elementor-widget__width-initial animated-fast elementor-absolute elementor-invisible elementor-widget elementor-widget-image animated zoomIn" data-id="2e7e20b" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;zoomIn&quot;,&quot;_animation_delay&quot;:500,&quot;_position&quot;:&quot;absolute&quot;,&quot;motion_fx_motion_fx_scrolling&quot;:&quot;yes&quot;,&quot;motion_fx_translateX_effect&quot;:&quot;yes&quot;,&quot;motion_fx_translateX_speed&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:0.1000000000000000055511151231257827021181583404541015625,&quot;sizes&quot;:[]},&quot;motion_fx_translateX_direction&quot;:&quot;left&quot;}" data-widget_type="image.default">
               <div class="elementor-widget-container">
                  <img width="242" height="120" src="https://sala.uxper.co/wp-content/uploads/2021/09/cloud@2x.png" class="attachment-full size-full" alt="" loading="lazy" />                             
               </div>
            </div>
            <div class="elementor-element elementor-element-465fb05 elementor-widget__width-initial animated-fast elementor-absolute elementor-invisible elementor-widget elementor-widget-image" data-id="465fb05" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;salaFadeInUp&quot;,&quot;_animation_delay&quot;:300,&quot;_position&quot;:&quot;absolute&quot;,&quot;motion_fx_motion_fx_scrolling&quot;:&quot;yes&quot;,&quot;motion_fx_translateX_effect&quot;:&quot;yes&quot;,&quot;motion_fx_translateX_direction&quot;:&quot;right&quot;,&quot;motion_fx_translateX_speed&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:0.200000000000000011102230246251565404236316680908203125,&quot;sizes&quot;:[]}}" data-widget_type="image.default">
               <div class="elementor-widget-container">
                  <img width="1016" height="736" src="/assets/savings/img/step-three-b.png" class="attachment-full size-full" alt="" loading="lazy" />                              
               </div>
            </div>
         </div>
      </div>
      <div class="animated-fast animated-fast col col-sm-12 col-md-6 order-md-1 order-lg-2 elementor-column elementor-inner-column elementor-element elementor-element-aaf2a44 elementor-element-779b462" data-id="8e94c7c" data-element_type="column" data-settings="{&quot;animation&quot;:&quot;salaFadeInUp&quot;}">
         <div class="elementor-widget-wrap elementor-element-populated">
            <div class="elementor-element elementor-element-96a3846 elementor-widget elementor-widget-sala-heading" data-id="96a3846" data-element_type="widget" data-widget_type="sala-heading.default">
               <div class="elementor-widget-container">
                  <div class="sala-modern-heading">
                     <div class="heading-secondary-wrap">
                        <h6 class="heading-secondary elementor-heading-title">Step Three</h6>
                     </div>
                     <div class="heading-primary-wrap">
                        <h2 class="heading-primary elementor-heading-title">Craft Your Budget</h2>
                     </div>
                     <div class="heading-description-wrap">
                        <div class="heading-description">
                           <p>For each destination, venti will provide a recommended budget, savings plan, and an outline of all the expenses you should prepare for. Recommendations are tailored to your travel preferences and can be adjusted by you at any time.</div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>