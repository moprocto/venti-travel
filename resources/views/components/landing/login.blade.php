<a class="btn btn-primary" style="display: block; font-size: 13px; border-color: #3b5998; border-radius: 20px; background-color:#3b5998 !important" href="/login/facebook/redirect"><i class="fab fa-facebook"></i> LOG IN WITH FACEBOOK</a>
<br>
<form method="POST" action="{{ route('create-login') }}">
	@csrf
	<div class="form-group">
		<input type="text" name="email" class="form-control" placeholder="Email" required="">
	</div>
	<div class="form-group">
		<input type="password" name="password" class="form-control" placeholder="Password" required="">
		<a href="{{ route('password.request') }}" class="mt-2" style="font-size:12px; color:black;">Forgot Password?</a>
	</div>
	<div class="form-group">
		<input type="submit" name="submit" class="btn btn-primary" value="LOG IN">
	</div>
	<div class="form-group text-center">
		<a href="/register">Create an Account</a>
	</div>
</form>
