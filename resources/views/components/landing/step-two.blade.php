<section class="elementor-section elementor-inner-section elementor-element elementor-element-ff376e1 elementor-section-boxed elementor-section-gap-beside-yes elementor-section-height-default elementor-section-height-default elementor-section-column-vertical-align-stretch" data-id="fcaac27" data-element_type="section">
   <div class="elementor-container elementor-column-gap-extended">
      <div class="animated-fast col col-sm-12 col-md-6 order-md-1 order-lg-1 elementor-column elementor-inner-column elementor-element elementor-element-aaf2a44" data-id="3315ceb" data-element_type="column" data-settings="{&quot;animation&quot;:&quot;salaFadeInUp&quot;}">
         <div class="elementor-widget-wrap elementor-element-populated">
            <div class="elementor-element elementor-element-04c0adc elementor-widget elementor-widget-sala-heading" data-id="04c0adc" data-element_type="widget" data-widget_type="sala-heading.default">
               <div class="elementor-widget-container">
                  <div class="sala-modern-heading">
                     <div class="heading-secondary-wrap">
                        <h6 class="heading-secondary elementor-heading-title">Step Two</h6>
                     </div>
                     <div class="heading-primary-wrap">
                        <h2 class="heading-primary elementor-heading-title">Create Your Vision Board</h2>
                     </div>
                     <div class="heading-description-wrap">
                        <div class="heading-description">
                           <p>List all the places you'd like to visit and venti will estimate a budget based on your travel preferences for each destination. We'll also make trip recommendations if you need inspiration!</p>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="animated-fast col col-sm-12 col-md-6 order-md-2 order-lg-2" data-id="ff5be1a" data-element_type="column">
         <div class="elementor-widget-wrap elementor-element-populated">
            <div class="elementor-element elementor-element-4f3abe9 animated-fast salasvg elementor-invisible elementor-widget elementor-widget-image" data-id="4f3abe9" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_delay&quot;:500}" data-widget_type="image.default">
               <div class="elementor-widget-container">
                  <img width="460" height="460" src="/assets/savings/img/oval.svg" class="attachment-full size-full" alt="" loading="lazy" />                              
               </div>
            </div>
            <div class="elementor-element elementor-element-be84f41 elementor-widget__width-initial animated-fast elementor-absolute elementor-invisible elementor-widget elementor-widget-image" data-id="be84f41" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;salaFadeInRight&quot;,&quot;_animation_delay&quot;:300,&quot;_position&quot;:&quot;absolute&quot;}" data-widget_type="image.default">
               <div class="elementor-widget-container">
                  <img width="868" height="1092" src="/assets/savings/img/hand-phone-2.png" class="attachment-full size-full" alt="" loading="lazy" />                              
               </div>
            </div>
            <div class="elementor-element elementor-element-d0aa9e4 elementor-widget__width-initial animated-fast elementor-absolute elementor-widget-mobile__width-initial elementor-invisible elementor-widget elementor-widget-image" data-id="d0aa9e4" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;zoomIn&quot;,&quot;_animation_delay&quot;:500,&quot;_position&quot;:&quot;absolute&quot;}" data-widget_type="image.default">
               <div class="elementor-widget-container">
                  <img width="736" height="608" src="/assets/savings/img/step-two-b.png" class="attachment-full size-full"  />                             
               </div>
            </div>
         </div>
      </div>
   </div>
</section>