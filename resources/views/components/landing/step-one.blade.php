<section class="elementor-section elementor-inner-section elementor-element elementor-element-ff376e1 elementor-section-boxed elementor-section-gap-beside-yes elementor-section-height-default elementor-section-height-default elementor-section-column-vertical-align-stretch" data-id="ff376e1" data-element_type="section">
   <div class="elementor-container elementor-column-gap-extended">
      <div class="col col-sm-12 col-md-6 order-md-2 order-lg-1 elementor-column elementor-inner-column elementor-element elementor-element-aaf2a44" data-id="aaf2a44" data-element_type="column">
         <div class="elementor-widget-wrap elementor-element-populated">
            <div class="elementor-element elementor-element-fc377ba animated-fast salasvg elementor-invisible elementor-widget elementor-widget-image" data-id="fc377ba" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_delay&quot;:500}" data-widget_type="image.default">
               <div class="elementor-widget-container">
                  <img width="460" height="460" src="/assets/savings/img/oval.svg" class="attachment-full size-full" alt="" loading="lazy" />                                           
               </div>
            </div>
            <div class="elementor-element elementor-element-dfb8109 elementor-widget__width-initial animated-fast elementor-absolute elementor-invisible elementor-widget elementor-widget-image" data-id="dfb8109" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;salaFadeInLeft&quot;,&quot;_animation_delay&quot;:300,&quot;_position&quot;:&quot;absolute&quot;}" data-widget_type="image.default">
               <div class="elementor-widget-container">
                  <img width="878" height="1140" src="/assets/savings/img/hand-phone-1.png" class="attachment-full size-full" alt="" loading="lazy" />                                            
               </div>
            </div>
            <div class="elementor-element elementor-element-411ad41 elementor-widget__width-initial animated-fast elementor-absolute elementor-invisible elementor-widget elementor-widget-image" data-id="411ad41" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;zoomIn&quot;,&quot;_animation_delay&quot;:500,&quot;_position&quot;:&quot;absolute&quot;}" data-widget_type="image.default">
               <div class="elementor-widget-container">
                  <img width="528" height="528" src="/assets/savings/img/step-one.png" class="attachment-full size-full" alt="" loading="lazy"/>                                           
               </div>
            </div>
         </div>
      </div>
      <div class="animated-fast col col-sm-12 col-md-6 order-md-1 order-lg-2 elementor-column elementor-inner-column elementor-element elementor-element-779b462 animated-fast" data-id="779b462" data-element_type="column" data-settings="{&quot;animation&quot;:&quot;salaFadeInUp&quot;}">
         <div class="elementor-widget-wrap elementor-element-populated">
            <div class="elementor-element elementor-element-1a3e034 elementor-widget elementor-widget-sala-heading" data-id="1a3e034" data-element_type="widget" data-widget_type="sala-heading.default">
               <div class="elementor-widget-container">
                  <div class="sala-modern-heading">
                     <div class="heading-secondary-wrap">
                        <h6 class="heading-secondary elementor-heading-title">STEP ONE</h6>
                     </div>
                     <div class="heading-primary-wrap">
                        <h2 class="heading-primary elementor-heading-title">Find Your Style
                        </h2>
                     </div>
                     <div class="heading-description-wrap">
                        <div class="heading-description">
                           <p>Complete our brief travel quiz so venti knows how you like to have a good time. We'll use your results to customize our recommendations.</p>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>