@extends('layouts.curator-master')

@section('content')

<!-- Start FAQs -->
    <section id="faqs" style="margin-top: 4em;">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 offset-lg-3 col-md-8 offset-md-2 section-head text-center">
                    <h2 class="section-head-title">Venti's Content Creator Contest</h2>
                    <p class="section-head-body">Terms & Conditions</p>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-10 col-md-12 faqs-container offset-lg-1">
                    <p>
1. Description. The Venti Content Creative Contest (the “Contest”) begins at 08:00 pm ET on July 11, 2022, and ends at 11:59 pm ET on September 14, 2022 (the “Contest Period”). By participating in the Contest, participants (or, if a minor in their place of residence, the participant’s parent or legal guardian on his or her behalf) unconditionally accept and agree to comply with and abide by these Official Rules and the decisions of Venti Financial, Inc. (the “Sponsor”), which shall be final and binding in all respects. Receipt of a prize is contingent upon fulfilling all requirements set forth herein.</p>
<p>
2. Eligibility. Must be eighteen (18) years of age or older at the time of entry and a legal resident of the fifty (50) United States (including the District of Columbia). Employees, officers, and directors of Sponsor, their respective parents, subsidiaries, and agents, as well as the immediate family (defined as parents, spouses, children, siblings, and grandparents) and household members of each such employee, officer, and director are not eligible. Void where prohibited by law. All federal, state and local laws, rules and regulations apply.</p>
<p>
3. How to Enter. To enter, you must: upload a photo either (i) through our official <a href="https://form.typeform.com/to/oboDXYCt" target="_blank">online form</a>. Incomplete entries and those containing invalid e-mail addresses will be disqualified and removed from the Contest. One entry per Instagram Account provided. Any attempt to enter using multiple/different email addresses, identities, registrations and logins, or any other methods will void all of an entrant’s entries and that entrant will be disqualified. Use of any automated system to participate is prohibited and will result in disqualification.</p>
<p>
Entries become the exclusive property of Sponsor and/or its affiliates and will not be acknowledged or returned. By entering the Contest, you grant Sponsor a perpetual, worldwide, royalty-free, irrevocable, non-exclusive license to reproduce, distribute, display, exhibit, transmit, broadcast, televise, digitize, and otherwise use the Photo in any manner, form, or format now or hereinafter created, including on the Internet, and for any purpose, including, but not limited to, advertising or promotion of Sponsor and its goods or services, and to use your name, likeness, and photograph in connection therewith, all without further consent from or payment to you. You further acknowledge that notwithstanding the foregoing sentence, Sponsor has no obligation to use your Photo in any manner or for any purpose.</p>
<p>
4. Representations and Warranties. By participating in the Contest, you warrant and represent that: (a) the Photo is your original work, and you are the sole and exclusive owner of all intellectual property rights in and to the Photo; (b) use of the Photo as described in these Official Rules will not infringe the intellectual property or other rights of any third party, or violate any federal, state, or local laws or ordinances; (c) the Photo is not obscene or libelous, and does not contain anything defamatory or derogatory; (d) the Photo does not contain any virus, spyware, malware, trap door, worm, or any other device, mechanism or code that is injurious or damaging to software or hardware used in conjunction with the Contest; and (e) you have the right to grant the license in Section 3 above. Entries not satisfying these criteria in any respect will be disqualified and removed from the Contest.</p>
<p>
5. Winner Selection. One (1) First Prize Winner will be selected by a panel of judges designated by Sponsor from among all eligible entries received during the Contest Period. Concluding the contest period, judges will self-select photos based on perceived quality and adherence to eligibility criteria. On September 7 at 11:59 pm ET, semi-finalists will have one entry posted to the Venti Financial, Inc. Instagram page (<a href="https://www.instagram.com/venti.travel/" target="_blank">@venti.travel</a>) where entries will accrue points based on the following criteria:
<br><br>
a.) 10 points for the submitting account to follow and remain a follower of venti.travel<br>
b.) 1 point for each like received by the entry by the end of the Contest Period.<br>
c.) 15 points each time the entry is shared to an instagram user's Instagram Story other than you. <br>
c.) 5 points when you share your entry in your Instagram Story and tagging our Instagram: @venti.travel<br>
<br>
 Prize winners will be notified by e-mail, may be required to complete and return an eligibility affidavit and liability/publicity release. If the potential winner cannot be contacted within three (3) days after selection, does not meet the eligibility criteria, does not fully comply with these Official Rules, or fails to sign and return the affidavit and/or release, then the potential winner forfeits the prize and an alternate winner will be selected.</p>
<p>
If a prize winner is considered a minor in his or her jurisdiction of residence, the prize will be awarded in the name of, or to, the winner’s parent or legal guardian, who must accept the prize, execute any required documents, and agree to all obligations and undertakings of the winner, both on behalf of himself/herself and winner, or the prize may be forfeited and awarded to an alternate winner. Under no circumstances will a prize be awarded as the result of an entry by an individual under 13 years of age.</p>
<p>
	The use of bot farms, paid engagement services, or other forms of abuse to artificially inflate your entry points will result in instant disqualification. Venti Financial, Inc. reserves the right to disqualify any entry for any reason.
</p>
<p>
6. Prizes. One (1) Grand Prize: consisting of a $500 Amazon Gift Card to the email address of the winning entry. Sponsor, in its sole discretion, may substitute a prize (or prize component) of equal or greater value due to unavailability of prize (or prize component) for any reason.</p>
<p>
7. Assignment. As a condition of receiving the Grand Prize, the First Price, or the Second Prize, the winners will be required to irrevocably grant, transfer, convey and assign to Sponsor the entirety of his or her rights, title, and interest of every kind and nature whatsoever, including, without limitation, all copyrights, and all rights incidental, subsidiary, ancillary or allied thereto (including, without limitation, all derivative rights), in and to the Photo submitted as part of his or her entry for exploitation throughout the world, and the right to secure patent, trademark, and/or copyright registrations thereto in perpetuity including, without limitation, the rights to use the Photo for any and all purposes in any and all media whether now known or hereafter developed, throughout the universe, in perpetuity and without compensation. If the Grand Prize winner fails or refuses to execute an agreement consist with this Section 7, the Sponsor may select an alternate Grand Prize winner. Notwithstanding the foregoing or anything else, Sponsor shall have no obligation to use the Photo, the Grand Prize winner’s entry, or the Grand Prize winner’s name or likeness in any manner whatsoever and, if used, shall not be obligated to pay compensation of any kind.</p>
<p>
8. General Conditions. All taxes and other expenses are the sole responsibility of the winner. Neither Sponsor nor Instagram are responsible or liable for, and entrants (or, if a minor in their place of residence, the participant’s parent or legal guardian on his or her behalf) completely release and hold Sponsor and Instagram harmless against: (a) illegible, incomplete, damaged, misdirected, stolen, late, or lost Entry Forms; (b) network, internet, or computer malfunctions or damage related to or resulting from participating in the Contest; (c) any condition caused by events beyond the control of Sponsor which may cause the Contest to be disrupted or corrupted; or (d) any injuries, losses or damages of any kind caused by the prize or resulting from acceptance or use of the prize, or from participation in the Contest. Any entrant attempting to defraud or in any way tamper with this Contest, including but not limited to using automated processes for entry, will be ineligible. If for any reason the Contest is not capable of running as originally planned, including but not limited to technical corruption or non-authorized human intervention, Sponsor in its sole discretion reserve the right to modify or cancel the Contest. CAUTION: ANY ATTEMPT BY AN ENTRANT OR ANY OTHER INDIVIDUAL TO DELIBERATELY DAMAGE ANY WEB SITE OR UNDERMINE THE LEGITIMATE OPERATION OF THE CONTEST MAY BE A VIOLATION OF CRIMINAL AND CIVIL LAWS AND SHOULD SUCH AN ATTEMPT BE MADE, SPONSOR RESERVES THE RIGHT TO PROSECUTE ANY SUCH INDIVIDUAL TO THE FULLEST EXTENT PERMITTED BY LAW.</p>
<p>
9. Release. By receipt of any prize, winner (or, if a minor in his/her place of residence, the winner’s parent or legal guardian on his or her behalf) agrees that Sponsor or Instagram, their respective parents, subsidiaries and affiliated companies, and the agents, employees, directors and officers of these companies, are not liable whatsoever for any injuries, losses or damages of any kind resulting in whole or in part, directly or indirectly, from the acceptance, possession, use or misuse of any prize or from winner’s participation in the Contest.</p>
<p>
10. Winners List. Names of prize winners may be requested by sending a self-addressed, stamped envelope by October 31st, 2022 to Sponsor to the attention of “2022 Venti Content Creator Contest”</p>
<p>
11. Sponsor/Administrator. The Sponsor and Administrator of this Contest is Venti Financial, Inc. (“Sponsor”).</p>
<p>
12. Privacy Policy. By participating in this Contest, entrants (or, if a minor in their place of residence, the participant’s parent or legal guardian on his or her behalf) agree to the collection and use of their information in accordance with Sponsor’s Privacy Policy, available at www.venti.co, including receiving marketing messages as set forth in such policy.</p>
                </div>
            </div>
        </div>
    </section>
@endsection
