@extends('layouts.curator-master')

@section('css')
	<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
	<style type="text/css">
		line{
			display: block;
			border: 2px dashed black;
			height: 100px;
			width: 1px;
		}
		line.horizontal{
			height: 1px;
			width: 100px;
		}
		.mx-drop{
			max-width: 300px;
		}
		.mx-drop-align{
			display:flex;
			justify-content:space-between;
			align-items:center;
			height: inherit;
		}
		h1{
			font-size: 3.75rem
		}
		.text-bold{
			font-weight: 600;
		}
		#stepOne select{
  			font-family: 'sofia', 'FontAwesome'
		}
		.select2-container {
		  max-width: 300px; /* Adjust the value as needed */
		}
		.select2-container--default .select2-selection--single,.select2-selection__rendered,.select2-container--default .select2-selection--single .select2-selection__arrow {
		    height: 46px; /* Set the desired height */
		}
	</style>
@endsection

@section('content')
	<div class="gradient-wrap" style="background: transparent; width:100%;">
        <!--  HERO -->
            <div class="section-wrapper black position-relative">
                <div class="row" style="align-items:center;">
                	<div class="col-md-8">
						<h1><span style="font-size:2.75rem; display:block;">Maximize</span> Product Adoption</h1>
						<h2>With Rewards Automation</h2>
						<p>Financial institutions use Venti to remove the headaches<br>of managing rewards and redemption without any changes <br>to their tech stack.</p>
						<div>
							<a href="" class="btn btn-primary text-bold">View Demo</a>
							<a href="" class="btn btn-black ml-4">Get Pricing</a>
						</div>
					</div>
					<div class="col-md-4">
						<h4>Step 1: Select Product</h4>
						<div id="stepOne">
							<select id="productSelect" style="width:100%; max-width: 300px;"></select>
						</div>
						<h4 class="mt-4">Step 2: Select Activity</h4>
						<div id="stepTwo">
							<select class="form-control mx-drop" id="triggerSelect"></select>
						</div>
						<h4 class="mt-4">Step 3: Select Reward</h4>
						<div id="stepThree">
							<select class="form-control mx-drop" id="rewardSelect"></select>
						</div>
						<button type="button" class="btn btn-success mt-4">Explore in Playground <i class="fa fa-arrow-up-right-dots"></i></button>
					</div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
	<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
	<script type="text/javascript" src="/assets/js/schema.js"></script>
	<script>

	$(document).ready( function () {
		//createProductDropdown("#stepOne","#stepTwo", "#stepThree");

		// Format function (same as before, but using the 'icon' property)

		$('#productSelect').select2({
			data: selectOptions,
		    templateResult: formatOption, // Function to customize option display
		    templateSelection: formatOption
		});

		$('#triggerSelect').select2({
		    templateResult: formatOption,
		    templateSelection: formatOption
		});

		$('#rewardSelect').select2({
			data: rewardOptions,
		    templateResult: formatOption,
		    templateSelection: formatOption,
		    minimumResultsForSearch: Infinity
		});
	});

</script>
@endsection