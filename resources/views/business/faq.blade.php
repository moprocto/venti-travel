<section id="faqs" style="padding-bottom:50">
    <div class="container">
        <div class="row">
            <div class="col-md-12  text-center pd-20 text-center" style="padding-bottom: 0; margin-bottom: -3px; padding:40px;" >
                <h2 class="section-head-title text-dark mb-3 section-title" style="margin:40px; margin-top:20px">FAQs</h2>
                <h5 style="font-weight: 800; font-size: 16px; text-transform:uppercase;">It's in our DNA to be as transparent as possible</h5>
            </div>
        </div>
        <div class="accordion accordion-flush" id="faqs" style="padding-bottom:0">
            <div class="accordion-item">
                <h2 class="accordion-header" id="flush-headingTwo">
                  <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseTwo" aria-expanded="false" aria-controls="flush-collapseTwo">
                    Why not just give customer cash back?
                  </button>
                </h2>
                <div id="flush-collapseTwo" class="accordion-collapse collapse" aria-labelledby="flush-headingTwo" data-bs-parent="#accordionFlushExample">
                  <div class="accordion-body">
                    <p>Simple cashback programs are not exciting to young people. </p>
                  </div>
                </div>
            </div>
            <div class="accordion-item">
                <h2 class="accordion-header" id="flush-headingFive">
                  <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseFive" aria-expanded="false" aria-controls="flush-collapseFive">
                    What techn
                  </button>
                </h2>
                <div id="flush-collapseFive" class="accordion-collapse collapse" aria-labelledby="flush-headingFive" data-bs-parent="#accordionFlushExample">
                  <div class="accordion-body"><p>Any vested points belong to the employee and cannot be rescinded via our platform. Unvested points will be returned to your option pool to be used for other employees or can be refunded to your company.</p></div>
                </div>
            </div>
            
            <div class="accordion-item">
                <h2 class="accordion-header" id="flush-headingOne">
                  <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseOne" aria-expanded="false" aria-controls="flush-collapseOne">
                    Is there a minimum number of enrollees or points to get started?
                  </button>
                </h2>
                <div id="flush-collapseOne" class="accordion-collapse collapse" aria-labelledby="flush-headingOne" data-bs-parent="#accordionFlushExample">
                  <div class="accordion-body"><p>There is no minimum. However, We recommend conducting a pilot to determine compatibility before a full-scale deployment. </p></div>
                </div>
            </div>
            <div class="accordion-item">
                <h2 class="accordion-header" id="flush-headingThree">
                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseThree" aria-expanded="false" aria-controls="flush-collapseThree">
                How is my company billed?</button>
              </h2>
              <div id="flush-collapseThree" class="accordion-collapse collapse" aria-labelledby="flush-headingThree" data-bs-parent="#accordionFlushExample">
                <div class="accordion-body"><p>You are billed a referral fee when we direct someone to your institution. For points, you are billed based on your preference of "up front" via our "pay as you go " option. Invoices and receipts are delivered monthly when applicable.</p></div>
              </div>
            </div>
          
          <div class="accordion-item">
            <h2 class="accordion-header" id="flush-headingFour">
              <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseFour" aria-expanded="false" aria-controls="flush-collapseFour">
                Do points expire?
              </button>
            </h2>
            <div id="flush-collapseFour" class="accordion-collapse collapse" aria-labelledby="flush-headingFour" data-bs-parent="#accordionFlushExample">
              <div class="accordion-body"><p>Points do not expire for customers.</p></div>
            </div>
          </div>
          <!--
          <div class="accordion-item">
            <h2 class="accordion-header" id="flush-headingSix">
              <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseSix" aria-expanded="false" aria-controls="flush-collapseSix">
                What are the fees for this program?
              </button>
            </h2>
            <div id="flush-collapseSix" class="accordion-collapse collapse" aria-labelledby="flush-headingSix" data-bs-parent="#accordionFlushExample">
              <div class="accordion-body"><p>Origination fees may vary from 0.5% to 2% of the approved amount. We are waiving origination fees for those that join our VIP list. The annual fee for this program is 1% of the credit line available. The fee is waived for Business and First Class <a href="/boardingpass">Boarding Pass</a> members.</p> 
                <p>Fees subject to change.</p>
              </div>
            </div>
          </div>
          -->
        </div>
    </div>
</section>