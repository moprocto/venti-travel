@extends('layouts.enterprise')

@section('css')
    <link rel="stylesheet" href="https://unpkg.com/tippy.js@5/dist/backdrop.css" />
    <link rel="stylesheet"  href="https://unpkg.com/tippy.js@4/themes/light.css" />
    <link rel="stylesheet" href="https://unpkg.com/tippy.js@4/themes/light-border.css"/>
    <link rel="stylesheet" href="https://unpkg.com/tippy.js@4/themes/google.css"/>
    <link rel="stylesheet" href="https://unpkg.com/tippy.js@4/themes/translucent.css"/>
    <link rel="stylesheet" href="https://unpkg.com/tippy.js@4/animations/scale.css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/switchery/0.8.2/switchery.min.css" integrity="sha512-uyGg6dZr3cE1PxtKOCGqKGTiZybe5iSq3LsqOolABqAWlIRLo/HKyrMMD8drX+gls3twJdpYX0gDKEdtf2dpmw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" type="text/css" href="/assets/css/enterprise.css">
    <style type="text/css">
        .accordion-button:focus{
            border-color: transparent !important;
            box-shadow: none !important;
        }
        .success-list{
            list-style:none;
            margin-left:10px;
            padding-left:0;
        }
        .success-list li{
            margin-bottom: 15px;
        }
        ul .btn-sm.btn-success{
            border-radius: 50%;
            margin-right: 10px;
            background: #34BD1F !important;
            color: white;
        }
        
        .vertical-rotator {
          height: 200px; /* Adjust based on the height of one element */
          overflow: hidden;
          position: relative;
        }

        .rotator-container {
          position: absolute;
          width: 100%;
        }

        .rotator-element {
          height: 60px; /* Should match the height of .vertical-rotator */
          display: flex;
          align-items: center;
          justify-content: space-between;
          padding: 0 15px;
          background-color: #f5f5f5;
          border: 2px solid #e0e0e0;
          border-radius: 30px;
          margin-bottom: 10px;
        }

        .toggle-text {
          font-size: 16px;
          color: #333;
        }

        .gradient-div {
            width: 100%;
            height: 50px;
            position: absolute;
            left: 0;
            z-index: 1;
        }
        .gradient-div.top{
            background-image: linear-gradient(to top, rgba(242, 242, 246, 0), rgb(242, 242, 246, 1));
        }
        .gradient-div.bottom{
            bottom: 0;
            background-image: linear-gradient(to bottom, rgba(242, 242, 246, 0), rgb(242, 242, 246, 1));
        }
        
    </style>
@endsection

@section('content')
    <div class="orb orb1"></div>
    <div class="orb orb2"></div>
	<div class="gradient-wrap" style="background: transparent; width:100%; min-height: 600px;">
        <!--  HERO -->
        <div class="section-wrapper black position-relative">
            <div class="row" style="align-items:center;">
                <div class="col-md-7">
                    <h1 class="text-xl"><span class="text-md d-block">AI-Driven</span> Rewards Automation</h1>
                    <h5 class="mt-4" style="max-width:500px;">How forward-looking airlines, hotel chains, financial institutions, and fintechs completely transform engagement with intelligent and adaptive</strong> rewards</h5>
                    <br><br>
                    <div class="col-md-12 mt-4">
                        <a href="#contact" class="btn btn-primary btn-lg">Talk to an Expert</a>
                    </div>
                </div>
                <div class="col-md-5 text-center sm-spacing">
                    <div class="card frost tilt-right" style="margin-bottom: 30px; max-width:300px">
                        <div class="card-body">
                            <img src="/assets/img/mike-member-a.jpg" style=" position: absolute; left: -40px; top: -60px;width:75px; border-radius:20px; border: 2px solid white;">
                            Mike gets rewarded when he saves up for a down payment and completes first-time homebuyer courses
                        </div>
                    </div>
                    <div class="card frost md-mr-30 tilt-left" style="margin-top: 30px; max-width: 300px; float:right;">
                        <div class="card-body ">
                            <img src="/assets/img/jake-sara-member-b.jpg" style=" position: absolute; right: 0px; top: -60px; width:75px; border-radius:20px; border: 2px solid white;">
                            Sara and Jake get rewarded for refinancing debt and making on-time payments as they rebuild their credit scores
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <section>
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h2 class="text-lg">How it Works</h2>
                    <h5>We distribute a <span class="text-success" style=" font-weight:bold">reward</span> when a <span style="color:#9e7de9; font-weight: bold;"><i class="fa fa-bolt"></i> trigger</span> is activated on a financial product or other source</h5>
                    <div class="orb orb3"></div>
                </div>
            </div>
        </div>
        
    </section>

    <section style="margin-top:250px">
        <div class="container">
            
            <div class="row justify-content-center mb-4">
                <div class="col-md-3" style="margin-top:-100px">
                    <div class="card frost circle">
                        <div class="card-body text-center">
                            <h5 class="pb-0">Loan Accounts</h5>
                            <ul class="pill-list">
                                <li class="list-pill bg-shadow input loan-1 loan-2">Mortgage</li>
                                <li class="list-pill bg-shadow input loan-1 loan-2">Vehicle</li>
                                <li class="list-pill bg-shadow input loan-1 loan-2">HELOC</li>
                                <li class="list-pill bg-shadow input loan-1 loan-2">Personal</li>
                                <li class="list-pill bg-shadow input loan-1 loan-2">Student</li>
                                <li class="list-pill bg-shadow input loan-1 loan-3">Credit Card</li>
                            </ul>
                        </div>
                    </div>
                    <div class="rotating-circle first"></div>
                </div>
                <div class="col-md-3" style="padding:30px">
                    <div class="card frost circle">
                        <div class="card-body text-center">
                            <h5 class="pb-0">Deposit Accounts</h5>
                            <ul class="pill-list">
                                <li class="list-pill bg-shadow input deposit-1">Checking</li>
                                <li class="list-pill bg-shadow input deposit-1">Savings</li>
                                <li class="list-pill bg-shadow input deposit-1">Money Market</li>
                                <li class="list-pill bg-shadow input deposit-1">IRAs</li>
                                <li class="list-pill bg-shadow input deposit-1">CDs</li>
                                <li class="list-pill bg-shadow input deposit-1">HSAs</li>
                                <li class="list-pill bg-shadow input deposit-1">Debit Card</li>
                            </ul>
                        </div>
                    </div>
                    <div class="rotating-circle lg" style="top: -200px; left: -180px;"></div>
                </div>
                <div class="col-md-4 mob-c" style="margin-top:100px; padding: 30px">
                    <div class="card frost circle">
                        <div class="card-body text-center">
                            <h5 class="pb-0">Digital Activity</h5>
                            <ul class="pill-list">
                                <li class="list-pill bg-shadow input activity-1">Online Course</li>
                                <li class="list-pill bg-shadow input activity-1">Survey</li>
                                <li class="list-pill bg-shadow input activity-1">Referral</li>
                                <li class="list-pill bg-shadow input activity-3">Profile</li>
                                <li class="list-pill bg-shadow input activity-1">QR Code</li>
                                <li class="list-pill bg-shadow input activity-1">API Call</li>
                                <li class="list-pill bg-shadow input activity-1 activity-2">Credit Score</li>
                            </ul>
                        </div>
                    </div>
                    <div class="rotating-circle second"></div>
                </div>
            </div>
            <div class="row justify-content-center how-to" style="margin-top:100px;">
                <div class="col-md-7 text-center">
                    <h3 class="mb-4">Launch Fast With Templates</h3>
                    <h5 class="mb-4">Dozens of popular templates for common reward flows</h5>
                    <div class="vertical-rotator" style="margin: 50px 0">
                    <div class="gradient-div top"></div>
                      <div class="rotator-container">
                      </div>
                      <div class="gradient-div bottom"></div>
                    </div>
                    <div style="display: none; margin-bottom: 50px;  flex-direction:row; justify-content:space-around; align-items:center;">
                        <ul class="pill-list">
                            <li class="list-pill bg-shadow ">Referred by Friend</li>
                        </ul>
                        <i class="fa fa-arrow-right text-dark"></i>
                        <ul class="pill-list">
                            <li class="list-pill bg-shadow ">Completes $300+ Direct Deposit</li>
                        </ul>
                        <i class="fa fa-arrow-right text-dark"></i>
                        <ul class="pill-list">
                            <li class="list-pill bg-shadow ">5X Debit Card Purchases</li>
                        </ul>
                        <i class="fa fa-arrow-right text-dark"></i>
                        <ul class="pill-list">
                            <li class="list-pill bg-shadow  bg-success text-light">100 Points</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center how-to">
                <div class="col-md-7 text-center">
                    <h3 class="mb-4">Build Your Own</h3>
                    <h5 class="mb-4">Create custom reward flows with ease. Add multiple triggers and constraints.</h5>
                    <div style="margin-top: 75px; margin-bottom: 50px; display: flex; flex-direction:row; justify-content:space-around; align-items:center;">
                        <ul class="pill-list">
                            <li class="list-pill bg-shadow bg-transparent-pill">Input</li>
                            <div class="list-pill bg-shadow" style="position: absolute; transform: rotate(-10deg); margin-left:-25px; margin-top: 40px;"><img src="/assets/img/icons/grab-cursor.png" style="position: absolute; width:20px; margin-left:-15px; margin-top: 15px;">Credit Card</div>
                        </ul>
                        <i class="fa fa-arrow-right text-dark"></i>
                        <ul class="pill-list">
                            <li class="list-pill bg-shadow bg-transparent-pill">Trigger</li>
                            <div class="list-pill bg-shadow" style="position: absolute; transform: rotate(15deg); margin-left:5px; margin-top: -30px;"><img src="/assets/img/icons/grab-cursor.png" style="position: absolute; width:20px; margin-left:-15px; margin-top: 15px;"><i class="fa fa-bolt"></i> Purchase</div>
                        </ul>
                        <i class="fa fa-arrow-right text-dark"></i>
                        <ul class="pill-list">
                            <li class="list-pill bg-shadow bg-transparent-pill">Constraint</li>
                            <div class="list-pill bg-shadow" style="position: absolute;  margin-left:5px; margin-top: 35px;"><img src="/assets/img/icons/grab-cursor.png" style="position: absolute; width:20px; margin-left:35px; margin-top: 14px;"><i class="fa fa-location-dot"></i> Mike's Pizza</div>
                        </ul>
                        <i class="fa fa-arrow-right text-dark"></i>
                        <ul class="pill-list">
                            <li class="list-pill bg-shadow bg-transparent-pill text-success">Reward</li>
                            <div class="list-pill bg-shadow bg-success text-light" style="position: absolute; transform: rotate(-10deg); ;margin-left:-25px; margin-top: -35px;"><img src="/assets/img/icons/grab-cursor.png" style="position: absolute; width:20px; margin-left:-5px; margin-top: 20px;">2% Cash Back</div>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center how-to">
                <div class="col-md-7 text-center">
                    <h3 class="mb-4">Chain Them Together</h3>
                    <h5 class="mb-4">Require flows to be utilized before enabling new ones</h5>
                    <div style="margin-bottom: 50px; display: inline-flex; flex-direction:row; justify-content:center; align-items:center;">
                        <ul class="pill-list">
                            <li class="list-pill bg-shadow "><i class="fa fa-check"></i> Rewards Flow 1</li>
                        </ul>
                        <i class="fa fa-arrow-right text-dark"></i>
                        <ul class="pill-list">
                            <li class="list-pill bg-shadow "><i class="fa fa-lock-open" style="transform:rotateY(180deg);"></i> Rewards Flow 2</li>
                        </ul>
                        <i class="fa fa-arrow-right text-dark"></i>
                        <ul class="pill-list">
                            <li class="list-pill bg-shadow bg-success text-light">Reward</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center" style="margin-top:0px;">
                <div class="col-md-10 offset-md-1">
                    <div class="row">
                        <div class="col-md-12">
                            <hr style="margin: 100px auto;">
                        </div>
                        <div class="col-md-6 mb-4">
                            <h2 class="text-lg">Distribution + Redemption</h2>
                            <h5 class="text-md">all in one place</h5>
                        </div>
                        <div class="col-md-6 text-center mt-4">
                            
                            <div style="display: flex; flex-direction:row; justify-content:center;">
                                <div class="icon-circle">
                                    <img src="/assets/img/icons/airplane.png">
                                </div>
                                <div class="icon-circle">
                                    <img src="/assets/img/icons/queen_bed.png">
                                </div>
                                <div class="icon-circle">
                                    <img src="/assets/img/icons/smart-luggage.png">
                                </div>
                                <div class="icon-circle">
                                    <img src="/assets/img/icons/deposit.png">
                                </div>
                            </div>
                            <h5>Exchange points for travel, cash back, and more directly on Venti or within your app</h5>
                            <br>
                            <a href="/flights" class="btn btn-black" target="_blank">View Flights</a> &nbsp; <a href="/hotels" class="btn btn-black" target="_blank">View Hotels</a>
                        </div>
                        <div class="col-md-12">
                            <hr style="margin: 100px auto;">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="lg-spacing" style="margin-top:150px; display: none;">
        <div class="container">
            <div class="row mb-4">
                <div class="col-md-12 text-center justify-content-center">
                    
                </div>
            </div>
            <div class="row" style="margin-top:75px;">
                <div class="col-md-8 offset-md-2">
                    <div class="row">
                        <div class="col-md-4 mb-4 text-center">
                            <div class="card curvy-card">
                                <div class="card-body great-radius text-center">
                                    <p class="text-bold">Put Members First</p>
                                    <p>Receive real-time insights on how well your rewards are helping members achieve their financial goals.</p>
                                </div>
                            </div>
                            <img src="/assets/img/icons/chevron-down.png" class="mt-4 mb-4" style="width:25px; margin:0 auto;">
                            <div class="card card-success curvy-card">
                                <div class="card-body text-center">
                                    <p class="mb-0">Boost member satisfaction and net promoter scores</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 mb-4 text-center">
                            <div class="card curvy-card">
                                <div class="card-body great-radius text-center">
                                    <p class="text-bold">Differentiate</p>
                                    <p></p>
                                </div>
                            </div>
                            <img src="/assets/img/icons/chevron-down.png" class="mt-4 mb-4" style="width:25px; margin:0 auto;">
                            <div class="card card-success curvy-card">
                                <div class="card-body text-center">
                                    <p class="mb-0">Lower member acquisition and retention costs</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 mb-4 text-center">
                            <div class="card curvy-card">
                                <div class="card-body great-radius text-center">
                                    <p class="text-bold">Grow Revenues</p>
                                    <p>Increase product adoption across verticals with predictive insights and reinforcement marketing</p>
                                </div>
                            </div>
                            <img src="/assets/img/icons/chevron-down.png" class="mt-4 mb-4" style="width:25px; margin:0 auto;">
                            <div class="card card-success curvy-card">
                                <div class="card-body text-center">
                                    <p class="mb-0">Increase loan book and deposit growth</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div style="position:relative;">
                        <div class="orb orb4" style="position: absolute;"></div>
                        <div class="orb orb6" style="position: absolute;"></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
     <section class="lg-spacing" style="margin-top:50px">
        <div class="container">

          <div class="row">
            <div class="col-md-10 offset-md-1">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <h2 class="text-lg">Transformative Impact at Scale</h2>
                        <h5 style="margin-bottom:50px; margin-top: 10px;">Example use cases that maximize ROI and unlock additional soures of revenue</h5>
                        <div class="row">
                            <div class="col-md-12 col-lg-7 col-xl-7 mb-4 text-left">
                                <div class="accordion" id="customAccordion">
                                    <div class="accordion-item first">
                                      <h2 class="accordion-header">
                                        <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                          Attract and Engage Alumni at Scale
                                        </button>
                                      </h2>
                                      <div id="collapseOne" class="accordion-collapse collapse show" data-bs-parent="#customAccordion">
                                        <div class="accordion-body">
                                            <img src="/assets/img/alumni-college.jpg" class="hidden-lg display-md mb-4" style="border-radius:20px; width:100%;">
                                            <!--
                                            <p class="text-bold">Scenario</p>
                                            <p>As students graduate, they often leave behind their old furniture, books, and credit union membership (if they had one). College/university-affiliated credit unions want to increase alumni engagement and adoption.</p>
                                            <p class="text-bold">Solution:</p>
                                             -->
                                            <p>Offer rewards to alumni to drive increased engagment, local spend, and donations</p>
                                            <p class="text-bold">Example Rewards Flow</p>
                                            <div style="margin-bottom: 50px; display: flex; flex-direction:row; justify-content:space-around; align-items:center;">
                                                <ul class="pill-list">
                                                    <li class="text-small list-pill bg-shadow ">Creates New Account</li>
                                                </ul>
                                                <i class="fa fa-arrow-right text-dark"></i>
                                                <ul class="pill-list" style="display:flex; flex-direction: column;">
                                                    <li class="text-small list-pill bg-shadow ">Completes Profile</li>
                                                    <li class="text-small list-pill bg-shadow ">Makes Donation</li>
                                                </ul>
                                                <i class="fa fa-arrow-right text-dark"></i>
                                                <ul class="pill-list">
                                                    <li class="text-small list-pill bg-shadow  bg-success text-light">100 Points</li>
                                                </ul>
                                            </div>
                                            <p class="text-bold">Outcomes:</p>
                                            <ul class="success-list">
                                                <li><span class="btn btn-success btn-sm"><i class="fa fa-check"></i></span> Deposit growth</li>
                                                <li><span class="btn btn-success btn-sm"><i class="fa fa-check"></i></span> Increase interchange revenue</li>
                                                <li><span class="btn btn-success btn-sm"><i class="fa fa-check"></i></span> Reduced acquisition costs</li>
                                            </ul>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="accordion-item">
                                      <h2 class="accordion-header">
                                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                          Differentiate Financial Products
                                        </button>
                                      </h2>
                                      <div id="collapseTwo" class="accordion-collapse collapse" data-bs-parent="#customAccordion">
                                        <div class="accordion-body">
                                            <img src="/assets/img/new-homeowners.jpg" class="hidden-lg display-md mb-4" style="border-radius:20px; width:100%;">
                                            <!--
                                            <p class="text-bold">Scenario</p>
                                            <p>Rates are falling and demand for mortgage products is heating up. It's hard to stand out in a crowded field of flashier competitors.</p>
                                            <p class="text-bold">Solution:</p>
                                        -->
                                            <p>Connect rewards to popular loan products such as mortgages to drive savings to homeowners and would-be buyers in ways that matter to them.</p>
                                            <p class="text-bold">Example Rewards Flow</p>
                                            <div style="margin-bottom: 50px; display: flex; flex-direction:row; justify-content:space-around; align-items:center;">
                                                <ul class="pill-list">
                                                    <li class="text-small list-pill bg-shadow ">Completes Mortgage Application</li>
                                                </ul>
                                                <i class="fa fa-arrow-right text-dark"></i>
                                                <ul class="pill-list" style="display:flex; flex-direction: column;">
                                                    <li class="text-small list-pill bg-shadow ">Activates Automatic Payment</li>
                                                    <li class="text-small list-pill bg-shadow ">Enables Paperless Billing</li>
                                                </ul>
                                                <i class="fa fa-arrow-right text-dark"></i>
                                                <ul class="pill-list">
                                                    <li class="text-small list-pill bg-shadow  bg-success text-light">1000 Points</li>
                                                </ul>
                                            </div>
                                            <p class="text-bold">Outcomes:</p>
                                            <ul class="success-list">
                                                <li><span class="btn btn-success btn-sm"><i class="fa fa-check"></i></span> Loan growth</li>
                                                <li><span class="btn btn-success btn-sm"><i class="fa fa-check"></i></span> Reduced acquisition costs</li>
                                                <li><span class="btn btn-success btn-sm"><i class="fa fa-check"></i></span> Increased Net Promoter scores</li>
                                            </ul>
                                        </div>
                                      </div>
                                    </div>

                                    <div class="accordion-item ">
                                      <h2 class="accordion-header">
                                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                          Promote Behavior Change With Rewards
                                        </button>
                                      </h2>
                                      <div id="collapseFour" class="accordion-collapse collapse" data-bs-parent="#customAccordion">
                                        <div class="accordion-body">
                                            <img src="/assets/img/financial-advisor.jpg" class="hidden-lg display-md mb-4" style="border-radius:20px; width:100%;">
                                            <!--
                                            <p class="text-bold">Scenario:</p>
                                            <p>Members want to feel like their bank is a true partner in achieving their financial goals and life ambitions. They want to be rewarded for demonstrating positive financial behaviour instead of simply for spending money.</p>
                                            <p class="text-bold">Solution:</p>
                                            -->
                                            <p>Encourage members to learn financial literacy and put their knowledge into practice with your products.</p>
                                            <p class="text-bold">Rewards Flow</p>
                                            <div style="margin-bottom: 50px; display: flex; flex-direction:row; justify-content:space-around; align-items:center;">
                                                <ul class="pill-list" style="display:flex; flex-direction: column;">
                                                    <li class="text-small list-pill bg-shadow text-center">Course Completion</li>
                                                    <li class="text-small list-pill bg-shadow text-center">Financial Advisor Chat</li>
                                                    <li class="text-small list-pill bg-shadow text-center">Emergency Savings</li>
                                                    
                                                    <li class="text-small list-pill bg-shadow text-center">Credit Monitoring</li>
                                                </ul>
                                                <i class="fa fa-arrow-right text-dark"></i>
                                                <ul class="pill-list" style="display:flex; flex-direction:column;">
                                                    <li class="text-small list-pill bg-shadow ">Automated Deposit</li>
                                                    <li class="text-small text-small list-pill bg-shadow ">Credit Score Increases</li>
                                                </ul>
                                                <i class="fa fa-arrow-right text-dark"></i>
                                                <ul class="pill-list">
                                                    <li class="text-small list-pill bg-shadow  bg-success text-light" style="width:fit-content;">100 Points</li>
                                                </ul>
                                            </div>
                                            <p class="text-bold">Outcomes:</p>
                                            <ul class="success-list">
                                                <li><span class="btn btn-success btn-sm"><i class="fa fa-check"></i></span> Increased product adoption</li>
                                                <li><span class="btn btn-success btn-sm"><i class="fa fa-check"></i></span> Greater member satisfaction</li>
                                                <li><span class="btn btn-success btn-sm"><i class="fa fa-check"></i></span> Increased Net Promoter scores</li>
                                            </ul>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="accordion-item last">
                                      <h2 class="accordion-header">
                                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                          Add Rewards to Your App
                                        </button>
                                      </h2>
                                      <div id="collapseThree" class="accordion-collapse collapse" data-bs-parent="#customAccordion">
                                        <div class="accordion-body">
                                            <img src="/assets/img/app-fun.jpg" class="hidden-lg display-md mb-4" style="border-radius:20px; width:100%;">
                                            <p>Leverage our API to make your app fun and rewarding by tying specific end user accomplishments to easily redeemable rewards. Track balances, transactions, and more. </p>
                                            <p class="text-bold">Example Rewards Flow</p>
                                            <div style="margin-bottom: 50px; display: flex; flex-direction:row; justify-content:space-around; align-items:center;">
                                                <ul class="pill-list">
                                                    <li class="text-small list-pill bg-shadow ">API Call</li>
                                                </ul>
                                                <i class="fa fa-arrow-right text-dark"></i>
                                                <ul class="pill-list">
                                                    <li class="text-small list-pill bg-shadow  bg-success text-light">100 Points</li>
                                                </ul>
                                                <i class="fa fa-arrow-right text-dark"></i>
                                                <ul class="pill-list">
                                                    <li class="text-small list-pill bg-shadow ">Webhook</li>
                                                </ul>
                                            </div>
                                            <p class="text-bold">Outcomes:</p>
                                            <ul class="success-list">
                                                <li><span class="btn btn-success btn-sm"><i class="fa fa-check"></i></span> Reduced acquisition costs</li>
                                                <li><span class="btn btn-success btn-sm"><i class="fa fa-check"></i></span> Increased Net Promoter scores</li>
                                            </ul>
                                        </div>
                                      </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-5 hidden-md">
                                <!-- STATIC TEXT I WILL ADD HERE -->
                                <div class="image-rotator-container">
                                    <img src="/assets/img/alumni-college.jpg" id="image-rotator" style="width: 100%; max-width: 400px; border-radius: 20px;">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
          </div>
        </div>
    </section>
     <section class="lg-spacing">
        <div class="container full-width">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h2 class="text-lg">Fraud Prevention</h2>
                    <h5  style="margin-bottom:50px; margin-top: 10px;">We detect high-risk behavior and potential abuse before it costs you</h5>
                    <div style="position:relative;">
                        <img id="magnifier" src="/assets/img/icons/magnifying-glass.png" style="width:50px"></i>
                        <canvas id="dotCanvas" style="position:absolute; left:0; margin-top:50px"></canvas>
                        <div class="orb orb5" style="position:absolute;"></div>
                        <div class="orb orb6" style="position:absolute; margin-left:100px;"></div>
                    </div>
                    <div class="row fraud-row-adjust-sm" style="margin-bottom:100px;">
                        <div class="col-md-8 offset-md-2">
                            <div class="row">
                                <div class="col-md-4 mb-4">
                                    <div class="card frost" style="min-height: 100px; min-width:300px;">
                                        <div class="card-body">
                                            <h3><i class="fa fa-eye"></i></h3>
                                            <h5>Always Watching</h5>
                                            <p>Continuous AI-driven monitoring of all activity to keep you steps ahead of bad actors</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 mb-4">
                                    <div class="card frost" style="min-height: 100px; min-width:300px;">
                                        <div class="card-body">
                                            <h3><i class="fa fa-sliders"></i></h3>
                                            <h5>Customized Controls</h5>
                                            <p>Apply unlimited constraints and manual approval requirements to any reward flow</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 mb-4">
                                    <div class="card frost" style="min-height: 100px; min-width:300px;">
                                        <div class="card-body">
                                            <h3><i class="fa-regular fa-thumbs-up"></i></h3>
                                            <h5>No Fault Policy</h5>
                                            <p>If our system fails to prevent fraud by an end user, you are reimbursed the cost of redemption and fees</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
     </section>
    <section class="lg-spacing" style="display:none;">
        <div class="container full-width">
            <div class="row">
                <div style="position: relative;">
                    <div class="orb orb6" style="position:absolute; left:0; margin:0; top:-100px; opacity:0.5"></div>
                </div>
                <div class="col-md-8 offset-md-2 text-center">
                    <h2 class="text-lg"><span class="d-block text-md"><s>Pricing</s></span>Partnership Model</h2>
                    <h4>Not an expense. A source of revenue.</h4>
                </div>
            </div>
        </div>
    </section>
    <section class="lg-spacing">
        <div class="container">
            <div class="row">
                <div class="col-md-6"  style="display:none;">
                    <h2 class="text-lg">Plug and Play </h2>
                    <h4>Deploys in minutes. Not months.</h4>
                    <p class="mb-4">Connects with Fiserv and Plaid so you spend more time crafting winning member experencies and evaluating key insights. All data is maintained on your systems to comply with data security and privacy policies. 
                    <br><br>Not using Fiserv? We can still deploy a dynamic rewards solution with your existing tech stack using our API.</p>
                    <div class="mob-flex-first" style="display: flex; flex-direction:row; justify-content:center; align-items:center;">
                        <img src="/assets/img/fiserv-logo.png" style="width:100px" class="mr-4">
                        <i class="fa fa-plus" style="font-size:40px"></i>
                        <img src="/assets/img/plaid-logo.png" style="width:100px;" class="ml-4 mr-4">
                        <i class="fa fa-plus" style="font-size:40px"></i>
                        <img src="/assets/img/salesforce-logo.png" style="width:100px;" class="ml-4">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card frost" id="contact" style="border-radius:20px">
                        <div class="card-body" style="display:block;">
                            <div class="row mb-4">
                                <div class="col-md-12 text-center">
                                    <h4 class="section-head-title text-dark mb-3 section-title" style="margin:40px; margin-top:20px">Let's Connect</h4>
                                    <h5>Have questions? Ready for a demo? We're ready when you are</h5>
                                </div>
                            </div>
                            <div class="row mb-4">
                                <div class="col-md-6 text-left">
                                    <label class="form-label">Your Name:</label>
                                    <input type="text" class="form-control required" id="full_name" placeholder="Frequent Flyer">
                                </div>
                                <div class="col-md-6 text-left">
                                    <label class="form-label">Your Email:</label>
                                    <input type="text" class="form-control required" id="email" placeholder="email@company.com">
                                </div>
                            </div>
                            <div class="row mb-4">
                                <div class="col-md-6 text-left">
                                    <label class="form-label">Your Organization:</label>
                                    <input type="text" class="form-control required" id="company" placeholder="ACME Inc.">
                                </div>
                                <div class="col-md-6 text-left">
                                    <label class="form-label">Your Website:</label>
                                    <input type="text" class="form-control required" id="website" placeholder="www.website.com">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 text-center mb-4">
                                    <button id="contact_us" class="btn btn-success">Contact Us</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
            
        </div>

    </section>

@endsection('content')

@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/switchery/0.8.2/switchery.min.js" integrity="sha512-lC8vSUSlXWqh7A/F+EUS3l77bdlj+rGMN4NB5XFAHnTR3jQtg4ibZccWpuSSIdPoPUlUxtnGktLyrWcDhG8RvA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="https://unpkg.com/popper.js@1"></script>
    <script src="https://unpkg.com/tippy.js@5"></script>
    <script src="/assets/js/tooltips.js"></script>
    <script>
        $(document).ready(function() {
            $.ajaxSetup({
                 headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                 }
              });

            $("#contact_us").click(function(){
                var btn = $(this);
                btn.addClass("disable-while-loading");
                var email = $("#email").val();
                var company = $("#company").val();
                var name = $("#full_name").val();
                var website = $("#website").val();

                $(".required").each(function(){
                    var formVal = $(this).val();
                    if(formVal == undefined || formVal == "" || formVal.trim() == ""){
                        btn.removeClass("disable-while-loading");
                        $(this).addClass("invalid-feedback");
                        return swal({
                            icon: 'error',
                            title: 'Email is required'
                        });
                    }
                });

                $.ajax({
                    url: "{{ route('business-contact') }}",
                    processData: true,
                    dataType: 'json',
                    type: 'post',
                    data: {
                        email: email,
                        company: company,
                        name: name,
                        website: website
                    },
                    success: function (data) {
                        if(data == 200){
                            btn.removeClass("disable-while-loading");
                            $(".required").each(function(){
                                $(this).removeClass("invalid-feedback");
                                $(this).val("");
                            });
                            swal({

                                icon: 'success',
                                title: "Our team will be in touch soon",
                                text: "We sent a confirmation email to " + email
                            });
                        }
                        else {
                            btn.removeClass("disable-while-loading");
                            swal({
                                icon: 'warning',
                                title: "There was a problem",
                                text: "The email is either invalid or already on our VIP list. If you believe this was an error, please try again. Otherwise, please contact us at admin@venti.co."
                            });
                        }
                    },
                    error: function(data){
                        swal({
                            icon: 'warning',
                            title: "There was a problem",
                            text: "The email is either invalid or already on our VIP list. If you believe this was an error, please try again. Otherwise, please contact us at admin@venti.co."
                        });
                        btn.removeClass("disable-while-loading");
                    }
                });
            });

            var images = ["/assets/img/alumni-college.jpg","/assets/img/new-homeowners.jpg","/assets/img/financial-advisor.jpg", "/assets/img/app-fun.jpg"];

            function changeImage(index) {
                $('#image-rotator').fadeTo(500, 0, function() {
                  $(this).attr('src', images[index]).fadeTo(500, 1);
                });
            }

              // No need to set initial image, as it's already set in HTML

            $('.accordion-button').on('click', function() {
                var $button = $(this);

                setTimeout(function() {
                  if (!$button.hasClass('collapsed')) {
                    var index = $button.parents('.accordion-item').index();
                    changeImage(index);
                  }
                }, 30);
            });

            const canvas = document.getElementById('dotCanvas');
            const ctx = canvas.getContext('2d');
            const PADDING = 5;
            const EDGE_BUFFER = 10; // 10px buffer from edges
            const magnifier = document.getElementById('magnifier');

            function resizeCanvas() {
                canvas.width = window.innerWidth;
                canvas.height = 350 + 2 * PADDING;
            }

            const dots = [];
            const numberOfDots = 100;

            class Dot {
                constructor() {
                    this.reset();
                }

                reset() {
                    this.x = Math.random() * canvas.width;
                    this.y = PADDING + EDGE_BUFFER + Math.random() * (canvas.height - 2 * PADDING - 2 * EDGE_BUFFER);
                    this.speed = 2 + Math.random() * 4;
                }

                update() {
                    this.x += this.speed;
                    if (this.x > canvas.width) {
                        this.reset();
                        this.x = 0;
                    }
                }

                draw() {
                    ctx.beginPath();
                    ctx.arc(this.x, this.y, 2.5, 0, Math.PI * 2);
                    ctx.fillStyle = 'black';
                    ctx.fill();
                }
            }

            function createDots() {
                for (let i = 0; i < numberOfDots; i++) {
                    dots.push(new Dot());
                }
            }

            function animate() {
                ctx.clearRect(0, 0, canvas.width, canvas.height);
                
                dots.forEach(dot => {
                    dot.update();
                    dot.draw();
                });

                requestAnimationFrame(animate);
            }

            function moveMagnifier() {
                const x = Math.random() * (canvas.width - 50) + 25;
                const y = PADDING + EDGE_BUFFER + Math.random() * (canvas.height - 2 * PADDING - 2 * EDGE_BUFFER - 50) + 25;
                
                magnifier.style.left = `${x}px`;
                magnifier.style.top = `${y}px`;
            }

            function initMagnifier() {
                moveMagnifier(); // Initial position
                setInterval(moveMagnifier, 4000); // Move every 3 seconds
            }

            // Initialize
            window.addEventListener('resize', resizeCanvas);
            resizeCanvas();
            createDots();
            animate();
            initMagnifier();

            // vertical rotator

            

            var templates = [
                    {
                        "label" : "2% Points for each successful student loan payment",
                        "color" : "primary"
                    },
                    {
                        "label" : "100 Points for new student loan refinance account",
                        "color" : "primary"
                    },
                    {
                        "label" : "100 Points for new student loan account",
                        "color" : "primary"
                    },
                    {
                        "label" : "1% cash back for all VISA signature credit card purchases",
                        "color" : "success"
                    },
                    {
                        "label" : "20 Points for completing First Time Home Buyer course",
                        "color" : "primary"
                    },
                    {
                        "label" : "10 Points for meeting with a financial advisor",
                        "color" : "primary"
                    },
                    {
                        "label" : "2% cash back for using VISA signature credit card at local merchant",
                        "color" : "success"
                    },
                    {
                        "label" : "$50 for referring a friend who deposits at least $500",
                        "color" : "success"
                    },
                    {
                        "label" : "$50 for participating in Day of Service",
                        "color" : "success"
                    },
                    {
                        "label" : "0.5% Points for completing a mortgage refinance",
                        "color" : "primary"
                    },
                    {
                        "label" : "2% Points for completing an auto loan refinance",
                        "color" : "primary"
                    },
                    {
                        "label" : "5% Points for all gasoline purchases with a VISA signature credit card",
                        "color" : "primary"
                    },
                    {
                        "label" : "2.5% Points for all restaurant and grocery purchases VISA signature credit card",
                        "color" : "primary"
                    }
            ];

            for(i = 0; i < templates.length; i++){
                if(i <= 1){
                    $(".rotator-container").append('<div class="rotator-element"><span class="toggle-text text-small">' + templates[i].label + '</span><input type="checkbox" class="js-switch ' + templates[i].color +'" checked></div>');
                } else {
                    $(".rotator-container").append('<div class="rotator-element"><span class="toggle-text text-small">' + templates[i].label + '</span><input type="checkbox" class="js-switch ' + templates[i].color +'"></div>');
                }
                
            }

            function initToggleSwitchAnimation(){
                // Initialize Switchery toggles
                var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));

                elems.forEach(function(elem, index) {
                    // Create the Switchery element  !important

                    var color = elem.classList.contains('success') ? '#34BD1F' : '#0dcaf0';

                      // Create the Switchery element with the appropriate color
                      var switchery = new Switchery(elem, { color: color, size: 'small' });

                    // Set a timeout to toggle the switch after a delay
                    setTimeout(function() {
                        if(!elem.checked){
                            switchery.setPosition(true);
                        }
                    }, (index-1) * 3500);
                });
            }

            

            var $container = $('.rotator-container');
            var $elements = $('.rotator-element');
            var elementHeight = $elements.first().outerHeight(true);
            var totalHeight = elementHeight * $elements.length;

            // Clone elements for seamless looping
            $elements.clone().appendTo($container);

            function rotateElements() {
                $container.animate({
                  top: -totalHeight
                }, {
                  duration: totalHeight * 50, // Adjust speed here
                  easing: 'linear',
                  complete: function() {
                    $container.css('top', 0);
                    rotateElements();
                  },
                  step: function(now) {
                    // Fade out top element and fade in bottom element
                    var topElementIndex = Math.floor(-now / elementHeight);
                    var bottomElementIndex = (topElementIndex + $elements.length) % $elements.length;
                  }
                });
            }

            rotateElements();
            initToggleSwitchAnimation();

        });
    </script>
@endsection