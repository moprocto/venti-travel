<section>
        <div class="container full-width">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 full-width">
                    <div class="row" style="padding: 0 25px;">
                        <div class="col-md-12 text-center pd-20 text-center" style=" margin-bottom: -3px; padding:40px 20px; padding-bottom: 20px;">
                            <h2 class="section-head-title text-dark mb-3 section-title" style="margin:40px; margin-top:20px">Pricing</h2>
                            <h5>Simple and Predictable</h5>
                        </div>
                    </div>
                    <div class="row mb-4" style="padding:0 50px;">
                        <div class="col-md-6"> 

                                <label class="form-label d-block">Number of Program Enrollees<span class="text-small d-block">Pricing begins at $5 per enrollee depending on volume.</span></label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                      <div class="input-group-text"><i class="fa fa-user"></i></div>
                                    </div>
                                    <input type="number" class="form-control updateBalances" placeholder="10000" value="1" step="1" inputmode="numeric" id="employees" />
                                </div>

                        </div>
                        <div class="col-md-6">
                            <div class="text-left">
                                <div class="col-md-12 mb-4 ">
                                    <label class="form-label d-block">Program Fee Upcharge<span class="text-small d-block">You can pass the above fee onto enrollees, subsidize it, or upcharge (recommended).</span></label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                          <div class="input-group-text"><i class="fa fa-dollar-sign"></i></div>
                                        </div>
                                        <input type="number" class="form-control updateBalances" placeholder="0.5" step="0.5" inputmode="numeric" id="benefits" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label class="form-label d-block">Payment Preference <span class="text-small d-block">1 Point = $1 of Purchasing Power on Venti. Save 2% when you purchase Points upfront. Otherwise, we'll add a 3% convenience fee each billing cycle for Points redeemed on Venti.</span></label>
                            <select class="form-control updateBalances" id="preference">
                              <option value="upfront">Pay Upfront and Save 2%</option>
                              <option value="ongoing">Pay as You Go with 3% Fee</option>
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label class="form-label d-block">Payment Preference <span class="text-small d-block">1 Point = $1 of Purchasing Power on Venti. Save 2% when you purchase Points upfront. Otherwise, we'll add a 3% convenience fee each billing cycle for Points redeemed on Venti.</span></label>
                            <select class="form-control updateBalances" id="preference">
                              <option value="upfront">Pay Upfront and Save 2%</option>
                              <option value="ongoing">Pay as You Go with 3% Fee</option>
                            </select>
                        </div>
                    </div>
                    <div class="row" style="padding: 0 50px;">
                        <div class="col-md-6 text-center"><div style="background-color:rgba(49, 127, 131, 0.2); padding:20px; border-radius: 20px; font-weight:bold; margin-bottom: 10px;"><span style="font-size:36px">$<span id="balance" >100</span></span><br><span style="font-size:12px;">Cost to You</span></div>
                        </div>
                        <div class="col-md-6 text-center"><div style="background-color:rgba(252, 166, 76, 0.2); padding:20px; border-radius: 20px; font-weight:bold; ">
                            <span style="font-size: 36px;">$<span id="points">0</span></span><br><span style="font-size:12px; line-height: 0.9"> In Travel to Employees</span></div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 full-width">
                    <img src="/assets/img/business-calc.jpg" style="width: 100%;" class="">
                </div>
            </div>
        </div>
    </section>