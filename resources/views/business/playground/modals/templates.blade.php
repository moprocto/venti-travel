<div class="modal fade" id="searchTemplatesModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true" style="background-image: linear-gradient(112.1deg, rgba(125, 110, 112,0.5) 11.4%, rgba(152, 135, 125,0.8) 70.2%); ">
	  <div class="modal-dialog modal-dialog-centered" style="max-width:800px;">
	    <div class="modal-content text-left" style="padding-bottom: 10px">
		    <div class="modal-header mb-4" style="width: 100%; position: absolute; right: 0; top: 10px; border-bottom: transparent; z-index: 99;">
		    	<div class="d-flex flex-row justify-content-between pr-4" style="align-items: center; width: 50%;">
		    		<h5 style="font-size:18px; padding:0; margin:0">Use Popular Reward Flows</h5>
		    		<span class="btn btn-dark btn-sm">Create With AI <i class="fa fa-wand-magic-sparkles"></i></span>
		    	</div>
		        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
		    </div>
		    <div class="modal-body" style="margin-top:60px;">
		    	<div class="form-group">
		      		<input type="text" class="form-control" placeholder="Search Templates" name="">
		      	</div>
		      	<div style="max-height: 400px; overflow-y:scroll;">
		      	@php

		      	$workflows = [
		      		[
		      			"label" => "2% Points for each successful student loan payment",
		      			"color" => "primary"
		      		],
		      		[
		      			"label" => "100 Points for new student loan refinance account",
		      			"color" => "primary"
		      		],
		      		[
		      			"label" => "100 Points for new student loan account",
		      			"color" => "primary"
		      		],
		      		[
		      			"label" => "1% cash back for all VISA signature credit card purchases",
		      			"color" => "success"
		      		],
		      		[
		      			"label" => "20 Points for completing First Time Home Buyer course",
		      			"color" => "primary"
		      		],
		      		[
		      			"label" => "10 Points for meeting with a financial advisor",
		      			"color" => "primary"
		      		],
		      		[
		      			"label" => "2% cash back for using VISA signature credit card at local merchant",
		      			"color" => "success"
		      		],
		      		[
		      			"label" => "$50 for referring a friend who deposits at least $500",
		      			"color" => "success"
		      		],
		      		[
		      			"label" => "$50 for participating in Day of Service",
		      			"color" => "success"
		      		],
		      		[
		      			"label" => "0.5% Points for completing a mortgage refinance",
		      			"color" => "primary"
		      		],
		      		[
		      			"label" => "2% Points for completing an auto loan refinance",
		      			"color" => "primary"
		      		],
		      		[
		      			"label" => "5% Points for all gasoline purchases with a VISA signature credit card",
		      			"color" => "primary"
		      		],
		      		[
		      			"label" => "2.5% Points for all restaurant and grocery purchases VISA signature credit card",
		      			"color" => "primary"
		      		]
		      	];

		      	@endphp

		      	@foreach($workflows as $workflow)
			      	<div class="form-group">
			      		<div class="input-group">
			      			<span class="btn btn-{{ $workflow['color'] }}" style="pointer-events: none; border:1px solid #ced4da"><i class="fas fa-arrows-split-up-and-left rotate-90" style="margin-top:8px;"></i></span>
			      			<input type="text" class="form-control" value="{{ $workflow['label'] }}">
			      			<button class="btn btn-secondary">OPEN</button>
			      		</div>
			      	</div>
		      	@endforeach
		      </div>
			</div>
	    </div>
	</div>
</div>