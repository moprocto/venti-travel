<div class="col-md-3" style="max-width: 300px;">
	<div class="card" style="padding:0">
		<div class="card-body" style="padding:0; padding-bottom: 50px;">
			<ul class="list-nav">
				<li>
					<a href="/"><i class="fa fa-home"></i> <span class="hasChildren"><span class="text-right">Home</span><i class="fa fa-chevron-up transparent"></i></span></a>
				</li>
				<li>
					<a href="/"><i class="fa fa-users"></i>  <span class="hasChildren"><span>Members</span><i class="fa fa-chevron-up transparent"></i></span></a>
				</li>
				<li>
					<a href="/" class="active"><i class="fa-solid fa-arrows-split-up-and-left rotate-90"></i>  <span class="hasChildren"><span>Rewards</span><i class="fa fa-chevron-down"></i></span></a>
					<ul class="list-nav sub-nav">
						<li>
							<a href="/"><i class="fa fa-plane-departure"></i> <span class="hasChildren"><span>Redemptions</span><i class="fa fa-chevron-up transparent"></i></span></a>
						</li>
						<li>
							<a href="/"><i class="fa-solid fa-cubes"></i>  <span class="hasChildren"><span>Products</span> <i class="fa fa-chevron-down transparent"></i></span></span></a>
						</li>
						<li>
							<a href="/"><i class="fa-solid fa-qrcode"></i>  <span class="hasChildren"><span>QR Codes</span> <i class="fa fa-chevron-down transparent"></i></span></span></a>
						</li>
					</ul>
				</li>
				<li>
					<a href="/"><i class="fa fa-file-waveform"></i> <span class="hasChildren"><span>Reports</span><i class="fa fa-chevron-up"></i></span></a>
				</li>
				<li>
					<a href="/"><i class="fa fa-wand-magic-sparkles"></i> <span class="hasChildren"><span>Insights</span><i class="fa fa-chevron-up"></i></span></a>
				</li>
				<li>
					<a href="/"><i class="fa fa-bell"></i> <span class="hasChildren"><span>Inbox</span><i class="fa fa-chevron-up transparent"></i></span></a>
				</li>
				<li>
					<a href="/"><i class="fa fa-paintbrush"></i> <span class="hasChildren"><span>Branding</span><i class="fa fa-chevron-up transparent"></i></span></a>
				</li>
				<li>
					<a href="/"><i class="fa fa-shield-halved"></i> <span class="hasChildren"><span>Security</span><i class="fa fa-chevron-up transparent"></i></span></a>
				</li>
				<li>
					<a href="/"><i class="fa fa-gears"></i> <span class="hasChildren"><span>Settings</span><i class="fa fa-chevron-up transparent"></i></span></a>
				</li>
			</ul>
		</div>
		<div class="card-body text-center">
			<a href="/" class="text-danger text-bold">LOG OUT</a>
		</div>
	</div>
</div>

<style>
.list-nav span {
	text-transform: none; /* Remove any default uppercase styling */
}
</style>
