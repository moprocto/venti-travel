@extends("business.playground.template")

@section('css')
	
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/jerosoler/Drawflow@0.0.48/dist/drawflow.min.css">
	<script src="https://cdn.jsdelivr.net/gh/jerosoler/Drawflow/dist/drawflow.min.js"></script>
	<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
	<style type="text/css">
		.text-bold{
			font-weight: 600;
		}
		#drawflow {
			height: 600px;
		}
		.drawflow{
			box-shadow: 0 1px 1px hsl(0deg 0% 0% / 0.015),
		      0 2px 2px hsl(0deg 0% 0% / 0.015),
		      0 4px 4px hsl(0deg 0% 0% / 0.015),
		      0 8px 8px hsl(0deg 0% 0% / 0.015),
		      0 16px 16px hsl(0deg 0% 0% / 0.015)
		}
		.drawflow .drawflow-node{
		  width: 245px; /* Adjust the width as needed */
		  /* Additional styling (optional) */
		  min-height: 100px; /* Set a minimum height if you have content */
		  padding: 15px;
		  border: 1px solid whitesmoke;
		  border-radius: 5px;
		  background: white;
		}
		.select2-container {
		  width: 245px !important; /* Adjust the value as needed */
		  font-size: 14px;
		}
		.select2-container--default .select2-selection--single,.select2-selection__rendered,.select2-container--default .select2-selection--single .select2-selection__arrow {
		    height: 40px; /* Set the desired height */
		    font-size: 14px;
		}
		.mx-drop-align{
			display:flex;
			justify-content:space-between;
			align-items:center;
			height: inherit;
		}
		.drawflow .drawflow-node.selected{
			background: white !important;
			border-color: #6EA2DE !important;
    		box-shadow: 0px 0px 10px #6EA2DE !important;
		}
		.github4 .select2-container{
			font-size: 16px;
		}

		.github .outputs {
		  /* Rotate inputs 90 degrees and move to the top */
		  transform-origin: left top; 
		                 /* Set rotation origin to top-left corner */
		}
		.github .outputs .output{
			top: 60px;
			right: 110px;
		}
		.github2.drawflow-node, .github3.drawflow-node,.github4.drawflow-node,.github5.drawflow-node{
			width: 200px !important;
		}
		.github3 .outputs .output{
			top: 60px;
			right: 90px;
		}
		.drawflow .drawflow-node .input{
			background: white;
		}
		.stepper{
			width: 300px;
			height: 600px;
			margin-top: 20px;
		}
		h4.text-small{
			font-size: 13px;
			font-weight: bold;
		}
		p.text-small, label.text-small{
			font-size: 12px;
		}
		.circle{
			border:1px solid black;
			border-radius: 50%;
			width: 10px;
			padding: 1px 6.5px;
			margin-right: 3px;
		}
		.input-transparent{
			background: transparent;
			border: transparent;
			border-radius: 0;
			border-bottom: 3px solid black;
			max-width: 600px;
			display: block;
			font-size: 24px;
		}
		.input-transparent:active, .input-transparent:focus{
			background: transparent;
			outline: none !important;
			outline-color: transparent;
  			outline-style: none;
		}
		.mx-icon-list{
			list-style:none;
			padding: 0;
			margin: 0;
		}
		.mx-icon-list li{
			display: flex;
			flex-direction: row;
			justify-content: space-between;
			align-items: center;
			font-size: 13px;
			padding-bottom: 10px;
		}
		.stepone-connector{
			z-index: 1;
		    bottom: 224px;
		    height: 57px;
		    position: relative;
		    left: 246px;
		    opacity: .7;
		}
		.steptwo-connector{
			z-index: 1;
		    top: -284px;
		    height: 57px;
		    position: relative;
		    left: 246px;
		    opacity: .7;
		}
		.stepthree-connector{
			z-index: 1;
		    bottom: 254px;
		    height: 57px;
		    position: relative;
		    left: 246px;
		    opacity: .7;
		}
		.card .form-control{
			height: 42px;
			font-size: 14px;
			text-align: right;
		}
		.list-nav{
			list-style:none;
			padding: 0;
			margin: 0;
		}
		.list-nav li a {
			text-transform: uppercase;
			font-size: 12px;
			font-weight: 600;
			padding: 14px 20px;
			display: flex;
			flex-direction: row;
			justify-content: space-between;
			border-bottom: 1px solid rgba(0, 0, 0, 0.1);
			color: #2e2e2e;
			align-items: center;
		}

		.list-nav li a i{
			font-size: 14px;
		}

		.list-nav li a:hover{
			background: rgba(0,0,0,0.05);
			transition: 0.5s;
			cursor: pointer;
		}

		.list-nav li a.active{
			color: #0d6efd;
		}
		.rotate-90{
			transform: rotate(90deg);
		}
		.list-nav a .hasChildren{
			display: flex;
			flex-direction: row;
			width: 89px;
			justify-content: flex-end;
			align-items: center;
			align-content: flex-end;
		}
		.list-nav a .hasChildren i {
			margin-left: 15px;
			font-size: 12px;
		}
		.stepperHeader{
			display: flex;
			flex-direction: row;
			justify-content: space-between;
			align-items: center;
			margin-bottom: .5rem;
		}
		.stepperHeader h4, .justify-space-between label{
			margin: 0;
		}
		.justify-space-between{
			justify-content: space-between;
		}
		.align-center{
			align-items: center;
		}
		.hidden{
			display: none;
		}
		.stepThree .card-body, .stepFour .card-body{
			border: 1px dashed orange;
			border-radius: 0.24rem;
		}
		.list-nav.sub-nav li a{
			padding-left: 48px;
		}
		.transparent{
			opacity: 0;
		}
		.full-width {
	    max-width: 9999px !important;

	    padding-right: 0;
	}
	</style>


@endsection

@section('content')
	<div class="container mt-4 full-width" style="margin-left: 0;">
		<div class="row">
			@include('business.playground.sidenav')
			<div class="col-md-9">
				<div class="row">
					<div class="col-md-7" style="display: flex; flex-direction:column;">
						<label class="form-label d-block text-small text-bold">NAME THIS REWARDS FLOW:</label>
						<div class="col-md-12 pl-0">
							<input type="text" class="form-control input-transparent d-block" placeholder="Local pizza cashback rewards flow">
						</div>
					</div>
					<div class="col-md-5" style="align-content: flex-end;">
						<div class="d-flex flex-row" style="justify-content:flex-end;">
							<button type="button" class="btn btn-primary btn-md"><i class="fa-solid fa-magnifying-glass-chart"></i> Cost Analysis</button>
							<button type="button" class="btn btn-dark  btn-md ml-2" href=";javascript" type="button" data-bs-toggle="modal" data-bs-target="#searchTemplatesModal" ><i class="fa fa-folder-open"></i> Templates</button>
							<button type="button" class="btn btn-success btn-md ml-2"><i class="fa fa-check"></i> Save &nbsp; <i class="fa fa-chevron-down"></i></button>
						</div>
					</div>
				</div>
				<div class="row" style="display:inline-flex; overflow-x: auto; flex-wrap: nowrap; width:100%">
					<div class="stepper stepOne">
						<div class="stepperHeader">
							<h4 class="text-small"><span class="circle" style="">1</span>SELECT PRODUCT</h4>
							<a href="#"><i class="fa fa-gear text-dark text-small"></i></a>
						</div>
						<div class="card">
							<div class="card-body">
								<label class="form-label text-bold text-small">We'll activate this flow when we detect activity on a specific product or trigger.</label>
								<select id="productSelect" style="width:100%; max-width: 300px;"></select>
								<hr>
								<label class="form-label text-bold text-small">Limit this rewards flow to a specific activity.</label>
								<select id="triggerSelect" style="width:100%; max-width: 300px;"></select>
								<hr>
								<label class="form-label text-bold text-small mt-1 mb-1">The following data will be sent to <span class="text-bold">Step 2</span></label>
								<ul class="mx-icon-list mt-2" id="stepOneData">
									<li><i class="fa fa-credit-card"></i> Balance</li>
									<li><i class="fa fa-rectangle-list"></i> Transactions</li>
								</ul>
							</div>
						</div>

						<div class="stepone-connector">
						<svg version="1.0" xmlns="http://www.w3.org/2000/svg"
							 width="80.000000pt" height="75.000000pt" viewBox="0 0 88.000000 140.000000"
							 preserveAspectRatio="xMidYMid meet">

							<g transform="translate(0.000000,140.000000) scale(0.100000,-0.100000)"
							fill="#000000" stroke="none">
							<path d="M745 1386 c-29 -12 -55 -51 -55 -80 0 -9 -21 -16 -65 -21 -36 -3 -65
							-9 -65 -11 0 -3 5 -14 10 -24 8 -15 4 -29 -20 -67 -34 -53 -82 -194 -99 -290
							-7 -33 -16 -130 -21 -215 -17 -274 -93 -491 -197 -559 -37 -25 -38 -24 -53 11
							-19 45 -63 65 -112 51 -90 -28 -85 -154 8 -177 33 -8 90 18 99 45 3 11 19 22
							36 26 38 8 98 65 133 127 63 108 116 323 116 468 0 160 37 334 96 458 35 73
							47 85 58 57 9 -25 20 -17 49 36 l29 50 23 -25 c32 -34 70 -42 110 -23 66 31
							68 129 4 162 -34 18 -45 18 -84 1z m90 -31 c14 -13 25 -36 25 -50 0 -14 -11
							-37 -25 -50 -13 -14 -36 -25 -50 -25 -33 0 -75 42 -75 75 0 14 11 37 25 50 13
							14 36 25 50 25 14 0 37 -11 50 -25z m-690 -1210 c32 -31 32 -69 0 -100 -31
							-32 -69 -32 -100 0 -32 31 -32 69 0 100 13 14 36 25 50 25 14 0 37 -11 50 -25z"/>
							</g>
						</svg>
						</div>
					</div>
					
					<div class="stepper stepTwo ml-4">
						<div class="stepperHeader">
							<h4 class="text-small"><span class="circle" style="">2</span>DEFINE REWARD</h4>
							<a href="#"><i class="fa fa-gear text-dark text-small"></i></a>
						</div>
						<div class="card">
							<div class="card-body">
								<label class="form-label text-bold text-small">What type of reward should be issued?</label>
								<select id="rewardTypeSelect">
									<option value="0">Points</option>
									<option value="1">Cash</option>
								</select>
								<hr>
								<label class="form-label text-bold text-small">How should the reward amount be calculated?</label>
								<select id="rewardSelect" style="width:100%; max-width: 300px;"></select>
								<div id="rewardInput">
									
								</div>
							</div>
						</div>
						<div class="stepthree-connector">
							<svg version="1.0" xmlns="http://www.w3.org/2000/svg"
								 width="80.000000pt" height="75.000000pt" viewBox="0 0 88.000000 140.000000"
								 preserveAspectRatio="xMidYMid meet">

								<g transform="translate(0.000000,140.000000) scale(0.100000,-0.100000)"
								fill="#000000" stroke="none">
								<path d="M745 1386 c-29 -12 -55 -51 -55 -80 0 -9 -21 -16 -65 -21 -36 -3 -65
								-9 -65 -11 0 -3 5 -14 10 -24 8 -15 4 -29 -20 -67 -34 -53 -82 -194 -99 -290
								-7 -33 -16 -130 -21 -215 -17 -274 -93 -491 -197 -559 -37 -25 -38 -24 -53 11
								-19 45 -63 65 -112 51 -90 -28 -85 -154 8 -177 33 -8 90 18 99 45 3 11 19 22
								36 26 38 8 98 65 133 127 63 108 116 323 116 468 0 160 37 334 96 458 35 73
								47 85 58 57 9 -25 20 -17 49 36 l29 50 23 -25 c32 -34 70 -42 110 -23 66 31
								68 129 4 162 -34 18 -45 18 -84 1z m90 -31 c14 -13 25 -36 25 -50 0 -14 -11
								-37 -25 -50 -13 -14 -36 -25 -50 -25 -33 0 -75 42 -75 75 0 14 11 37 25 50 13
								14 36 25 50 25 14 0 37 -11 50 -25z m-690 -1210 c32 -31 32 -69 0 -100 -31
								-32 -69 -32 -100 0 -32 31 -32 69 0 100 13 14 36 25 50 25 14 0 37 -11 50 -25z"/>
								</g>
								</svg>
						</div>
					</div>

					<div class="stepper stepThree ml-4">
						<div class="stepperHeader">
							<h4 class="text-small"><span class="circle" style="">3</span>DEFINE CONSTRAINTS</h4>
							<a href="#"><i class="fa fa-gear text-dark text-small"></i></a>
						</div>
						<div class="card">
							<div class="card-body">
								<label class="form-label text-bold text-small">What limitations should be applied to this rewards flow?</label>
								@include('business.playground.widgets.integer-input',["inputLabel" => "Minimum Balance", "inputIcon" => "fa-dollar", "foldable" => true, "foldID" => rand(999,99999)])

								@include('business.playground.widgets.integer-input',["inputLabel" => "Minimum Spend (30 days)", "inputIcon" => "fa-dollar", "foldable" => true, "foldID" => rand(999,99999)])

								@include('business.playground.widgets.integer-input',["inputLabel" => "# of Claims Per Account", "inputIcon" => "fa-hashtag"])

								@include('business.playground.widgets.date-input', ["inputLabel" => "Rewards Availability Window"])

								@include('business.playground.widgets.transaction-input', ["inputLabel" => "Merchant Category Code"])

								@include('business.playground.widgets.rule-input', ["inputLabel" => "Select Rule(s)"])
							</div>
						</div>
						
					</div>

					<div class="stepper stepFour ml-4">
						
						<div class="stepperHeader">
							<h4 class="text-small"><span class="circle" style="">4</span>DEFINE COMPLETION</h4>
							<a href="#"><i class="fa fa-gear text-dark text-small"></i></a>
						</div>
						<div class="card">
							<div class="card-body">
								<label class="form-label text-bold text-small">What should happen after this rewards flow is completed?</label>
								<hr>
								<div class="d-flex flex-row justify-space-between align-center mb-1">
									<label class="form-label text-bold text-small">Send Email</label>
									<input type="checkbox" class="">
								</div>
								<hr>
								<div class="d-flex flex-row justify-space-between align-center mb-1">
									<label class="form-label text-bold text-small">Send SMS</label>
									<input type="checkbox" class="">
								</div>
								<hr>
								<div class="d-flex flex-row justify-space-between align-center mb-1">
									<label class="form-label text-bold text-small">Alert System Admin</label>
									<input type="checkbox" class="">
								</div>
								<hr>
								<div class="d-flex flex-row justify-space-between align-center mb-1">
									<label class="form-label text-bold text-small">Flag for Manual Review</label>
									<input type="checkbox" class="">
								</div>
								<hr>
								<div class="d-flex flex-row justify-space-between align-center mb-1">
									<label class="form-label text-bold text-small">Activate Another Rewards Flow</label>
									<input type="checkbox" class="">
								</div>
								<hr>
								<div class="d-flex flex-row justify-space-between align-center mb-1">
									<label class="form-label text-bold text-small">Stop Processing Additional Rewards</label>
									<input type="checkbox" class="">
								</div>
							</div>
						</div>
						<div class="stepthree-connector" style="left: -63px; top: -241px; bottom: 0; transform: rotate(180deg); transform: scaleY(-1);">
						<svg version="1.0" xmlns="http://www.w3.org/2000/svg"
							 width="80.000000pt" height="75.000000pt" viewBox="0 0 88.000000 140.000000"
							 preserveAspectRatio="xMidYMid meet" style="width: 80px;">

							<g transform="translate(0.000000,140.000000) scale(0.100000,-0.100000)"
							fill="#000000" stroke="none">
							<path d="M745 1386 c-29 -12 -55 -51 -55 -80 0 -9 -21 -16 -65 -21 -36 -3 -65
							-9 -65 -11 0 -3 5 -14 10 -24 8 -15 4 -29 -20 -67 -34 -53 -82 -194 -99 -290
							-7 -33 -16 -130 -21 -215 -17 -274 -93 -491 -197 -559 -37 -25 -38 -24 -53 11
							-19 45 -63 65 -112 51 -90 -28 -85 -154 8 -177 33 -8 90 18 99 45 3 11 19 22
							36 26 38 8 98 65 133 127 63 108 116 323 116 468 0 160 37 334 96 458 35 73
							47 85 58 57 9 -25 20 -17 49 36 l29 50 23 -25 c32 -34 70 -42 110 -23 66 31
							68 129 4 162 -34 18 -45 18 -84 1z m90 -31 c14 -13 25 -36 25 -50 0 -14 -11
							-37 -25 -50 -13 -14 -36 -25 -50 -25 -33 0 -75 42 -75 75 0 14 11 37 25 50 13
							14 36 25 50 25 14 0 37 -11 50 -25z m-690 -1210 c32 -31 32 -69 0 -100 -31
							-32 -69 -32 -100 0 -32 31 -32 69 0 100 13 14 36 25 50 25 14 0 37 -11 50 -25z"/>
							</g>
						</svg>
					</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@include('business.playground.modals.templates')
@endsection

@section('js')
	<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
	<script type="text/javascript" src="/assets/js/schema.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
		//var id = document.getElementById("drawflow");
		//const editor = new Drawflow(id, null, null, null, false); 

		/*

		var html = document.createElement("div");
		var data = { "name": 'Select Product', "id":"product"};
		html.innerHTML =  "<h5>Select Product</h5><select id='productSelect'></select>";
		editor.registerNode('github', html);
		editor.addNode('github', 0, 1, 0, 200, 'github', data, 'github', true, false, false);

		var html2 = document.createElement("div");
		var data2 = { "name": 'Select Activity', "id": "activity" };
		html2.innerHTML =  "<h5>Select Activity</h5><select id='triggerSelect'></select>";
		editor.registerNode('github2', html2);
		editor.addNode('github2', 1, 1, 200, 350, 'github2', data2, 'github2', true);

		var html3 = document.createElement("div");
		var data3 = { "name": 'Select Reward', "id":"reward" };
		html3.innerHTML =  "<h5>Select Reward</h5><select id='rewardSelect'></select>";
		editor.registerNode('github3', html3);
		editor.addNode('github3', 1, 1, 450, 200, 'github3', data3, 'github3', true);

		var html4 = document.createElement("div");
		var data4 = { "name": 'Define Constraints', "id":"reward" };
		html4.innerHTML =  "<h5>Define Contraints</h5><select id='constraintsSelect'><option>One Time</option></select>";
		editor.registerNode('github4', html4);
		editor.addNode('github4', 1, 1, 625, 350, 'github4', data4, 'github4', true);

		var html5 = document.createElement("div");
		var data5 = { "name": 'Completion', "id":"reward" };
		html5.innerHTML =  "<h5>Completion</h5><select id='completionSelect'><option>Send Email</option></select>";
		editor.registerNode('github5', html5);
		editor.addNode('github4', 1, 0, 875, 200, 'github4', data5, 'github5', true);

		//editor.addConnection("test", "test2",0,0);

		*/

		$('#productSelect').select2({
			data: selectOptions,
		    templateResult: formatOption, // Function to customize option display
		    templateSelection: formatOption
		});

		$('#triggerSelect').select2({
		    templateResult: formatOption,
		    templateSelection: formatOption
		});

		$('#rewardTypeSelect').select2({
		    templateResult: formatOption,
		    templateSelection: formatOption
		});

		$('#rewardSelect').select2({
			data: rewardOptions,
		    templateResult: formatOption,
		    templateSelection: formatOption,
		    minimumResultsForSearch: Infinity
		});

		$('#constraintsSelect').select2({
		    templateResult: formatOption,
		    templateSelection: formatOption
		});

		$('#completionSelect').select2({
		    templateResult: formatOption,
		    templateSelection: formatOption
		});

		// Event listener for changes in the first dropdown

	    $('#productSelect').on('select2:select', function(e) {
		  var selectedProductId = e.params.data.id || e.params.value; 
		  updateTriggerOptions(selectedProductId);
		  updateDataAvailableList(selectedProductId);
		});

		updateRewardsInput(0);

		$("#rewardSelect").on('select2:select', function(e){
			var selectedRewardType = e.params.data.id || e.params.value; 
			updateRewardsInput(selectedRewardType)
		});

		$("#constraintsSelect").on('select2:select', function(e){
			var selectedConstrainType = e.params.data.id || e.params.value; 
			updateContraintsInput(selectedConstrainType)
		});
		$(".input-checkbox").change(function() {
			var hr = $(this).parent().next(); // Find the nearest hr element

		    if (this.checked) {
		    	hr.removeClass("hidden");
		    } else {
		        hr.addClass("hidden");
		    }
		});
	});
	</script>
@endsection