<hr>
<label class="form-label text-bold text-uppercase text-small">Minimum Balance</label>
<div class="input-group">
	<span class="btn btn-white" style="pointer-events: none; border:1px solid #ced4da"><i class="fa fa-dollar" style="padding-top:8px"></i></span>
	<input type="number" class="form-control">
</div>