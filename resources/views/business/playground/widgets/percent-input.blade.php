<hr>
<label class="form-label text-bold text-small">{{ $inputLabel }}</label>
<div class="input-group">
	<span class="btn btn-white" style="pointer-events: none; border:1px solid #ced4da"><i class="fa fa-percent" style="padding-top:8px"></i></span>
	<input type="number" class="form-control">
</div>