<hr>
<div class="d-flex flex-row justify-space-between align-center mb-1">
	<label class="form-label text-bold text-small">{{ $inputLabel }}</label>
	<input type="checkbox" class="input-checkbox">
</div>
<div class="rules-input-group input-group hidden">
	<span class="btn btn-white" style="pointer-events: none; border:1px solid #ced4da"><i class="fa-solid fa-arrows-split-up-and-left rotate-90" style="padding-top: 1px;
    padding-left: 3px;"></i></span>
	<input type="text" class="form-control" placeholder="Select Rewards Flow">
</div>