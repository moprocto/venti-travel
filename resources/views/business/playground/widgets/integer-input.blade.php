<div id="{{ strtolower(str_replace(' ','_',$inputLabel)) }}">
	<hr>
	<div class="d-flex flex-row justify-space-between align-center mb-1">
		<label class="form-label text-bold text-small">{{ $inputLabel }}</label>
		<input type="checkbox" class="input-checkbox">
	</div>
	<div class="input-group hidden">
		<span class="btn btn-white" style="pointer-events: none; border:1px solid #ced4da"><i class="fa {{ $inputIcon }}" style="padding-top:8px"></i></span>
		<input type="number" class="form-control">
	</div>
</div>