@extends('layouts.curator-master')

@php
    $selectable = false;
@endphp

@section('css')
    <style type="text/css">
        html{
            overflow-x: hidden;
        }
        @media(max-width: 1368px ){
            .tb-full-width{
                width: 100%;
            }
        }
        .hamburger-menu-list li a{
            color: white;
        }
        .mobile-nav-open .hamburger-menu-list li a{
            color: black;
        }
        .btn-white{
            color: black !important;
        }
        .mobile-nav-open #backdrop{
            margin-top: 0 !important;
        }
        .about-body{
            justify-content: center;
        }
        @media(min-width: 1367px){
            {
                margin-left: 22px;
            }
            .row.about-body{
                margin-bottom: 22px;
            }
            .desktop-hide{
                display: none;
            }
        }
        @media(max-width: 800px){
            .mb-pd-20{
                padding-bottom: 20px;
            }
        }
        @media(max-width: 1368px){
            .mobile-block{
                display: block !important;
            }
            .mobile-hide{
                display: none;
            }
            #about .about-body .about-body-item .about-body-icon-cnt{
                justify-content: left !important;
                margin-bottom: 10px;
            }
            .pills-item{
                background-position-y: inherit !important;
            }
            .sign-up-box{
                padding-top: 80px;
                padding-bottom:  0;
            }
        }
        .about-body-icon-cnt{
            border: 1px solid black;
            border-radius: 20px;
            padding: 5px 10px;
        }
        

        .font-light{
            font-weight: 300 !important;
        }
        .full-width{
            max-width: 9999px !important;
            padding-left:  0;
            padding-right: 0;
        }
        #about{
            padding-top: 0;
        }
        .pd-20{
            padding: 80px 40px;
        }
        i.text-small{
            font-size: 16px;
        }
        .about-body-icon-cnt, .about-body-text-head{
            padding-left: 14px;
        }
        .about-body-text-head{
            padding-right: 15px;
            font-weight: bold;
        }
        @media(min-width: 1367px){
            {
                margin-left: 22px;
            }
            .row.about-body{
                margin-bottom: 22px;
            }
            .desktop-hide{
                display: none;
            }
        }
        @media(max-width: 768px){
            .mobile-hide{
                display: none;
            }
        }
        @media(max-width: 468px){
            #minutes, #seconds{
                display: none;
            }
        }   
        @media(max-width: 1368px){
            .mh-500-sm{
                max-height: 500px;
            }
            .mobile-block{
                display: block !important;
            }
            .mobile-hide{
                display: none;
            }
            #about .about-body .about-body-item .about-body-icon-cnt{
                justify-content: left !important;
                margin-bottom: 10px;
            }
            .pills-item{
                background-position-y: inherit !important;
            }
            .sign-up-box{
                padding-top: 80px;
                padding-bottom:  0;
            }
            .preview-mh{
                width: 143% !important;
                margin-left: -73px;
            }
        }

        .text-gold{
            color: #9D8A7C !important;
        }

        .text-large{
            font-size: 20px;
        }

        ol.pills-list{
            display: block;
            padding: 0 0 0 26px;
            list-style: none;
            overflow: hidden;
        }

        .pills-list li{
            display: flex;
            justify-content: space-between;
            align-items: center;
            margin-bottom: 10px;
            padding: 10px 20px 5px 20px;
        }
        .sand-badge{
            background-color: rgba(157, 138, 124, 0.7);
            opacity: 0.8;
            color: white;
            font-size: 22px;
            border-radius: 50%;
            width: 42px;
            height: 40px;
            letter-spacing: 1;
        }
        .pills-item{
            padding: 7px 20px;
            border-radius: 10px;
            margin-bottom: 20px;
            background-color: whitesmoke;
        }
        .pills-item div{
            padding-top: 10px;
        }
        .pills-item p{
            padding-top: 3px;
        }
        .pills-item-1{
            background-position-y: 0;
            background-color: rgba(170, 203, 210, 0.6) !important;
        }

        .pills-item-2{
            background-position-y: -90px;
            background-color: rgba(170, 203, 210, 0.4) !important;
        }
        .pills-item-3{
            background-position-y: -180px;
            background-color: rgba(170, 203, 210, 0.2) !important;

        }
        .pills-item-4{
            background-position-y: -220px;
        }
        li.pills-item-3{
            background: #d7d0ca;
        }

        .list-pill{
            margin: 5px;
            align-items:center;
            background-color:white;
            padding: 10px 20px;
            border-radius: 20px;
        }

        #mc_embed_signup .text-small{
            font-size: 12px !important;
        }

        #subscribe .text-small{
            margin-top: 15px;
            
        }
        .about-body-item{
            margin-bottom: 10px;
        }.slider-horizontal{
            width: 100% !important;
        }
        #faqs
        {
        padding-bottom: 100px;
        }
        #faqs .faqs-container .faq
        {
            border-top: 1px solid var(--color-border);
            padding: 15px;
        }
        #faqs .faqs-container .faq .faq-head
        {
            padding: 15px 0px;
            position: relative;
        }
        #faqs .faqs-container .faq .faq-head h5
        {
            width: 90%;
            cursor: pointer;
        }
        #faqs .faqs-container .faq .faq-head h5::before
        {
            content: 'keyboard_arrow_down';
            text-align: center;
            font-size: 20px;
            line-height: 27px;
            font-family: 'Material Icons Round';
            height: 25px;
            width: 25px;
            color: #fefefe;
            background: var(--color-sec);
            position: absolute;
            top: 50%;
            right: 0;
            transform: translateY(-50%);
            border-radius: 50%;
            transition: 0.5s;
        }
        #faqs .faqs-container .faq .faq-head h5[aria-expanded=true]::before
        {
            content: 'keyboard_arrow_up';
        }
        .form-container .emailoctopus-form-wrapper .emailoctopus-success-message{
            color:  orange !important;
            font-size: 18px;
            padding-bottom: 20px;
        }
        .accordion-item{
            margin-bottom: 10px;
        }
        #countdown{
            position: fixed;
            z-index: 999;
            height: 50px;
            background: white;
            width: 100%;
            bottom: 0;
            left: 0;
            display: flex;
            flex-direction: row;
            justify-content: center;
            padding: 10px 20px;
            font-weight: bold;
            font-size: 0.9em;
            align-items: center;
        }
        #countdown div.descrtiption{
            text-align: center;
            font-size: 0.8em;
            padding: 10px;
        }
        #timer{
            font-weight: bold;
            display: flex;
            justify-content: space-between;
        }
        #timer *{
            padding-right: 4px;
        }

        .about-body-icon-cnt{
            border: 1px solid rgba(0,0,0,0.1);
        }
        .footnote{
            width: 30px;
            display: inline-block;
        }
        html body{
            background: #F2F2F6 !important;
        }
        .features ul {
            list-style: none;
            padding: 0;
        }
        .features ul li{
            padding: 10px;
            border-bottom: 1px solid whitesmoke;
            font-size: 13px;
        }
        .text-bold{
            font-weight: bold;
        }
        .pill-list{
            list-style: none;
            justify-content: center;
            display:flex;
            flex-wrap: wrap;
            max-width: 800px;
            margin: 0 auto
        }
        .text-small{
            font-size: 12px;
        }
        .list-pill.text-small{
            border-width: 1px;
        }
        .list-pill.text-small{
            padding: 5px 10px;

        }
        .border-dang{
            border: 1px solid #55696f;
        }
        .border-info{
            border: 1px solid #65afff;
        }
        .border-super{
            border: 1px solid #ff6474;
        }
        .form-label{
            font-weight: bold;
        }
        @media(min-width: 767px){
            /*
            .works-2{
                margin-top: 100px;
            }
            .works-3{
                margin-top: 200px;
            }
            */
        }
        footer{
            padding-top: 100px !important;
        }
    </style>
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/11.0.2/css/bootstrap-slider.min.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons+Round" rel="stylesheet">
    <!-- Main CSS -->
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>

@endsection

@php
    $vips = env('VIPS');
    $vip = number_format($vips, 0);
    $counter = $vips * 20;
@endphp

@section('content')
<div id="backdrop" style="margin-top: -130px; width: 100%; z-index: 1; display: flex; align-items: center; min-height: 800px; background-position: center;  background-size: cover; margin-bottom: -64px; background-image:url('/assets/img/business-bg.jpg')">
        <div class="gradient-wrap" style="background: transparent; width:100%;">
            <!--  HERO -->
            <div class="section-wrapper black position-relative">
                <div class="row" style="align-items:center;">
                    <div class="col-lg-5 col-md-12 col-sm-12 col-12 top-header" style="display: flex; align-items:center;">
                        <div id="hero-text-wrapper" style="max-width:900px;">
                            <h1 class="mb-3 section-title" style="line-height: 4rem; color: white; font-weight:600 !important; font-size: 3.8rem;">
                            Make Everything Rewardable
                            <span style="font-size: 0.45em; display:block;"></span>
                            </h1>
                            <h2 class="section-subtitle mb-4" style="color: white; max-width:900px;">A completely new way to build loyalty with members</h2>
                         <p class="section-head-body" style="color:white;"></p>
                         <a href="#contact" class="btn btn-primary">Contact Us</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<!-- Cover End -->
 <!-- About Start -->

 <section id="" style="padding-top: 100px; padding-bottom: 50px;">
        <div class="container full-width">
            <div class="row">
                <div class="col-md-12  text-center pd-20 text-center" style="padding-bottom: 0; margin-bottom: -3px; padding:40px 20px;" >
                    <h2 class="section-head-title text-dark mb-1 section-title" style="margin:40px; margin-top:20px">Your Members are Unique</h2>
                    <h3 class="section-head-body" style="font-size:1.25rem; max-width:800px; margin: 0 auto; margin-bottom:40px;">So why give them an identical rewards experience?</h3>
                    <h4>Leverage AI so every member's rewards journey feels authentic and empowering</h4>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="card">
                                <div class="card-body text-center">
                                    <img src="/assets/img/icons/superhero-c.png" class="d-block" style="width:100%; max-width:150px; margin: 0 auto;">
                                    <h5>Mark gets rewarded for:</h5>
                                    <ul class="pill-list">
                                        <li class="list-pill text-small border-dang">Opening a Checking Account</li>
                                        <li class="list-pill text-small border-dang">Improving Their Credit</li>
                                        <li class="list-pill text-small border-dang">Opening an Investment Account</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="card">
                                <div class="card-body text-center">
                                    <img src="/assets/img/icons/superhero-b.png" class="d-block" style="width:100%; max-width:150px; margin: 0 auto;">
                                    <h5>Ellie wants rewards for:</h5>
                                    <ul class="pill-list">
                                        <li class="list-pill text-small border-super">Refinancing a Student Loan</li>
                                        <li class="list-pill text-small border-super">Adding to Savings</li>
                                        <li class="list-pill text-small border-super">Paying with a Card</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="card">
                                <div class="card-body text-center">
                                    <img src="/assets/img/icons/superhero-a.png" class="d-block" style="width:100%; max-width:150px; margin: 0 auto;">
                                    <h5>Greg for:</h5>
                                    <ul class="pill-list">
                                        <li class="list-pill text-small border-info">Getting an Auto Loan</li>
                                        <li class="list-pill text-small border-info">Chatting with an Advisor</li>
                                        <li class="list-pill text-small border-info">Visiting a Branch</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-md-8 text-center">
                    <h2 class="section-head-title text-dark mb-1 mt-4 section-title" style="margin:40px; padding-top: 100px;">Maximize Engagement</h2>
                    <h3 class="section-head-body" style="font-size:1.25rem; margin: 0 auto; margin-bottom:40px;">Transform every member activity into a rewardable one</h3>
                </div>
                <div class="col-md-8">
                    <ul class="pill-list">
                        <li class="list-pill bg-shadow">Open a Checking Account</li>
                        <li class="list-pill bg-shadow">Chat with an Advisor</li>
                        <li class="list-pill bg-shadow">Improve Their Credit</li>
                        <li class="list-pill bg-shadow">Pay with Card</li>
                        <li class="list-pill bg-shadow">Pay a Bill On Time</li>
                        <li class="list-pill bg-shadow">Refer a Friend</li>
                        <li class="list-pill bg-shadow">Add to Savings</li>
                        <li class="list-pill bg-shadow">Maintain a Balance</li>
                        <li class="list-pill bg-shadow">Visit a Branch</li>
                        <li class="list-pill bg-shadow">Refinance Student Loans</li>
                        <li class="list-pill bg-shadow">Get an Auto Loan</li>
                        <li class="list-pill bg-shadow">Open Investment Account</li>
                        <li class="list-pill bg-shadow bg-success text-light">Whatever You Decide</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <section id="about" style="padding-top:100px; padding-bottom: 100px; background-color: #f7f9ff;">
        <div class="container full-width">
            <div class="row justify-content-center" >
                <div class="col-md-9 text-center">
                    <h2 class="section-head-title text-dark mb-1 mt-4 section-title" >How it Works</h2>
                    <h3 class="section-head-body" style="font-size:1.25rem; margin: 0 auto; margin-bottom:40px;">Integrates with your existing tech. No extra staff required.</h3>
                </div>
                <div class="col-md-9">
                    <div class="row">
                        <div class="col-md-4 text-center pd-20 works-1" style="padding-bottom: 0; margin-bottom: -3px; padding:40px;" >

                            <div class="card">
                                <div class="card-body">
                                    <img src="/assets/img/icons/compliant.png" class="d-block" style="width:100px; padding: 10px; margin: 0 auto;" />
                                    <p>Define what member activities are eligible for rewards. Their data is safe and never leaves your system. Our tool integrates with your existing technology.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 text-center pd-20 works-2" style="padding-bottom: 0; margin-bottom: -3px; padding:40px;" >

                            <div class="card">
                                <div class="card-body">
                                    <img src="/assets/img/icons/mobile-banking.png" class="d-block" style="width:100px; padding: 10px; margin: 0 auto;"/>
                                    <p>We use AI to detect rewardable activities and disburse points that will lead to maximum engagement. You control the min/max points for each activity.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 text-center pd-20 works-3" style="padding-bottom: 0; margin-bottom: -3px; padding:40px;" >

                            <div class="card">
                                <div class="card-body">
                                    <img src="/assets/img/tourism.png" class="d-block" style="width:100px; padding: 10px; margin: 0 auto;"/>
                                    <p>When members earn enough points, they can redeem directly on Venti in exchange for flights and hotels up to 100% of retail price.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>
    <section>
       <div class="container full-width" style="background-color:white;">
          <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 full-width">
                <div class="row" style="padding: 0 25px;">
                   <div class="col-md-12 text-center pd-20 text-center" style="padding:40px 20px; padding-bottom: 0; margin-bottom: -3px; ">
                      <h2 class="section-head-title text-dark mb-3 section-title" style="margin:40px; margin-top:20px">Redemption Made Simple</h2>
                      <h5>Easy for Staff. Easy for Members.</h5>
                      <div class="row text-left">
                         <div class="col-md-12">
                            <p class="text-dark" style="padding:20px;">Vent handles calculation for all points and is the primary source of redemption for end users. </p>
                         </div>
                      </div>
                      <div class="row text-dark text-left">
                         <div class="col-md-12">
                            <div style="padding:20px;" >
                                <h5>For Staff</h5>
                                <p class="text-dark" >Staff at your institution can interact directly with a member's rewards profile. With just a few clicks, they can disburse/deduct points from a member's profile.
                                </p>
                                <p><strong>Use Cases</strong></p>
                                <ul>
                                    <li>Reward members for visiting a branch and talking to an advisor</li>
                                    <li>Allow a member to buy down Points on a HELOC or mortgage with points</li>
                                    <li>Allow members to use Points as collateral to secure a loan</li>
                                    <li>Reward members for attending a physical event, such as volunteering for a clean-up at a local park.</li>
                                    <li>Deduct points from member profiles when they violate program rules</li>
                                </ul>
                                <br>
                                <p>Do all the above individually, in batches, or at scale with our API.</p>
                            </div>
                         </div>
                      </div>
                   </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 full-width">
                <img src="/assets/img/home-keys.jpg" style="width: 100%;" class="">
            </div>
          </div>
       </div>
    </section>
    <section>
       <div class="container full-width" style="background-color:white;">
          <div class="row">
             <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 full-width">
                <img src="/assets/img/business-dev.jpg" style="width: 100%;" class="">
            </div>
             <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 full-width">
                <div class="row" style="padding: 0 25px;">
                   <div class="col-md-12 text-center pd-20 text-center" style="padding:40px 20px; padding-bottom: 0; margin-bottom: -3px; ">
                      <h2 class="section-head-title text-dark mb-3 section-title" style="margin:40px; margin-top:20px">Pricing</h2>
                      <h5>Simple. Predictable. Scalable.</h5>
                      <div class="row text-dark text-left pills-item pills-item-1">
                         <div class="col-md-1 text-center"><span class="badge sand-badge">1</span></div>
                         <div class="col-md-9">
                            <p class="text-dark">Our pricing starts at $0.10 per enrolled member per month based on volume. No account minimums. Billed annually.</p>
                         </div>
                         <div class="col-md-2 text-center mb-pd-20" style="display: flex; align-items: center; justify-content:center;"><img src="/assets/img/earn-rewards.png" style="width:50px;"></div>
                      </div>
                      <div class="row text-dark text-left pills-item pills-item-2">
                         <div class="col-md-1 text-center"><span class="badge sand-badge">2</span></div>
                         <div class="col-md-9">
                            <p class="text-dark">Purchase reward points at a rate of 1 point = $1. Purchase points upfront or pay via invoicing with a 3% convenience fee.</p>
                         </div>
                         <div class="col-md-2 text-center mb-pd-20" style="display: flex; align-items: center; justify-content:center;"><img src="/assets/img/tourism.png" style="width:50px;"></div>
                      </div>
                      <div class="row text-dark text-left pills-item pills-item-3">
                         <div class="col-md-1 text-center"><span class="badge sand-badge">3</span></div>
                         <div class="col-md-9">
                            <p class="text-dark">Enrolled members exchange their points directly on Venti for flights, hotels, and travel swag.</p>
                         </div>
                         <div class="col-md-2 text-center mb-pd-20" style="display: flex; align-items: center; justify-content:center;"><img src="/assets/img/family.png" style="width:50px;"></div>
                      </div>
                   </div>
                </div>
                <div class="row" style="padding: 0 25px;">
                    <div class="col-md-6 text-center">
                        <div style="background-color:rgba(49, 177, 131, 0.2); padding:20px; border-radius: 20px; font-weight:bold; margin-bottom: 10px;">
                            <span style="font-size:18px" class="d-block">Lower member Acquisition by 75%</span>
                        </div>
                    </div>
                    <div class="col-md-6 text-center">
                        <div style="background-color:rgba(252, 166, 76, 0.2); padding:20px; border-radius: 20px; font-weight:bold; ">
                            <span style="font-size:18px" class="d-block">Increase member Retention by 90%</span>
                        </div>
                    </div>
                    <div class="col-md-12 text-center">
                        <p class="text-bold">Within six months, we helped <a href="https://www.veridiancu.org/" target="_blank">Veridian Credit Union</a><br>boost gross deposits by $400,000 with 90%+ retention</p>
                    </div>
                </div>
             </div>
          </div>
       </div>
    </section>
    <section style="padding-bottom: 0; background-color:#f7f9ff;"><div class="container full-width"><div class="row"><div class="col-md-6 offset-md-3 sm-p20"><div class="text-center" style=" background-image: url('/assets/img/travel-agent.jpg'); display: flex; align-items: center; background-position: center; justify-content: center; background-size:cover; flex-direction: column; padding: 30px; border-radius: 20px; margin: 80px auto; min-height: 400px;"><h2 class="section-head-title mb-3 section-title text-light d-block" style="margin:40px; margin-top:20px; text-shadow: 2px 3px 5px rgba(0,0,0,0.2);">Support at Scale</h2><br><h5 class="text-light d-block" style="text-shadow: 2px 3px 5px rgba(0,0,0,0.2);">Our team helps your members before, during, and after their trip</h5></div></div></div></div></section>
    <section id="contact" style="padding-top:100px; padding-bottom: 100px; background-color:white;">
        <div class="container" style="max-width:800px;" >
            <div class="row mb-4">
                <div class="col-md-12 text-center">
                    <h2 class="section-head-title text-dark mb-3 section-title" style="margin:40px; margin-top:20px">Contact Us.</h2>
                    <h5>Have questions? Ready for a demo? We're ready when you are</h5>
                </div>
            </div>
            <div class="row mb-4">
                <div class="col-md-6 text-left">
                    <label class="form-label">Your Name:</label>
                    <input type="text" class="form-control required" id="full_name" placeholder="Frequent Flyer">
                </div>
                <div class="col-md-6 text-left">
                    <label class="form-label">Your Email:</label>
                    <input type="text" class="form-control required" id="email" placeholder="email@company.com">
                </div>
            </div>
            <div class="row mb-4">
                <div class="col-md-6 text-left">
                    <label class="form-label">Your Organization:</label>
                    <input type="text" class="form-control required" id="company" placeholder="ACME Inc.">
                </div>
                <div class="col-md-6 text-left">
                    <label class="form-label">Your Website:</label>
                    <input type="text" class="form-control required" id="website" placeholder="www.website.com">
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 text-center">
                    <button id="contact_us" class="btn btn-primary">Contact Us</button>
                </div>
            </div>
        </div>
    </section>    

@endsection

@section("js")
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/typeahead.js/0.10.4/typeahead.bundle.min.js"></script>
    @if(env('APP_ENV') != "local")
        <!-- Meta Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s)
            {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};
            if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
            n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t,s)}(window, document,'script',
            'https://connect.facebook.net/en_US/fbevents.js');
            fbq('init', '1169331916805166');
            fbq('track', 'PageView');
        </script>
        <noscript><img height="1" width="1" style="display:none"
        src="https://www.facebook.com/tr?id=1169331916805166&ev=PageView&noscript=1"
        /></noscript>
        <!-- End Meta Pixel Code -->
    @endif
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/11.0.2/bootstrap-slider.min.js"></script>
    <script>
        $(document).ready(function() {
            $.ajaxSetup({
                 headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                 }
              });

            var interactable = false; 

             
            /**
             * Format the number, so it will be seperated by comma
             */
            function commaSeparateNumber(val){
                while (/(\d+)(\d{3})/.test(val.toString())){
                  val = val.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
                }
                return val;
            }

            toggleLogo("black","white");

            function toggleLogo(color, replaced){
                var logoSRC = $(".logo").attr("src");
                logoSRC = logoSRC.replace(color, replaced);
                console.log(logoSRC)
                $(".logo").attr("src", logoSRC);
            }

            $.fn.isInViewport = function() {
              var elementTop = $(this).offset().top;
              var elementBottom = elementTop + $(this).outerHeight();

              var viewportTop = $(window).scrollTop();
              var viewportBottom = viewportTop + $(window).height();

              return elementBottom > viewportTop && elementTop < viewportBottom;
            };

            function isElementVisible($elementToBeChecked)
            {
                var TopView = $(window).scrollTop();
                var BotView = TopView + $(window).height();
                var TopElement = $elementToBeChecked.offset().top;
                var BotElement = TopElement + $elementToBeChecked.height();
                return ((BotElement <= BotView) && (TopElement >= TopView));
            }

            

            $("#contact_us").click(function(){
                var btn = $(this);
                btn.addClass("disable-while-loading");
                var email = $("#email").val();
                var company = $("#company").val();
                var name = $("#full_name").val();
                var website = $("#website").val();

                $(".required").each(function(){
                    var formVal = $(this).val();
                    if(formVal == undefined || formVal == "" || formVal.trim() == ""){
                        btn.removeClass("disable-while-loading");
                        $(this).addClass("invalid-feedback");
                        return swal({
                            icon: 'error',
                            title: 'Email is required'
                        });
                    }
                });

                $.ajax({
                    url: "{{ route('business-contact') }}",
                    processData: true,
                    dataType: 'json',
                    type: 'post',
                    data: {
                        email: email,
                        company: company,
                        name: name,
                        website: website
                    },
                    success: function (data) {
                        if(data == 200){
                            btn.removeClass("disable-while-loading");
                            $(".required").each(function(){
                                $(this).removeClass("invalid-feedback");
                                $(this).val("");
                            });
                            swal({

                                icon: 'success',
                                title: "Our team will be in touch soon",
                                text: "We sent a confirmation email to " + email
                            });
                        }
                        else {
                            btn.removeClass("disable-while-loading");
                            swal({
                                icon: 'warning',
                                title: "There was a problem",
                                text: "The email is either invalid or already on our VIP list. If you believe this was an error, please try again. Otherwise, please contact us at admin@venti.co."
                            });
                        }
                    },
                    error: function(data){
                        swal({
                            icon: 'warning',
                            title: "There was a problem",
                            text: "The email is either invalid or already on our VIP list. If you believe this was an error, please try again. Otherwise, please contact us at admin@venti.co."
                        });
                        btn.removeClass("disable-while-loading");
                    }
                });
            });


            $(".nav-icon").each(function(){
                var src = $(this).attr("src");
                console.log(src)
                src = src.replace('.png', '-white-boarding.png');
                $(this).attr("src", src);
              });

            $("#hamburger-menu").click(function(){
                if($("#page_home__show").hasClass("mobile-nav-open")){
                    toggleLogo("white","black");
                } else {
                    toggleLogo("black","white");
                }
                
            });

           



        });
    </script>

@endsection