@extends('layouts.master')

@section('content')
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Captains Program</title>
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet">
  <style>
    .header {
      background-image: url('https://source.unsplash.com/1600x900/?airplane,travel');
      background-size: cover;
      height: 80vh;
      display: flex;
      justify-content: center;
      align-items: center;
    }

    .header-text {
      color: white;
      text-shadow: 2px 2px 5px rgba(0, 0, 0, 0.5);
      text-align: center;
    }
  </style>
</head>

<body>
  <header class="header">
    <h1 class="header-text display-4">Welcome to the Captains Program!</h1>
  </header>

  <div class="container mt-5">
    <div class="row">
      <div class="col-md-6">
        <h2>About the Captains Program</h2>
        <p>
          The Captains Program is an exclusive group travel initiative designed to help you connect with fellow travel enthusiasts and explore the world together. By joining the program, you'll enjoy numerous benefits, including professional training, networking opportunities, exclusive access to unique travel experiences, and more!
        </p>
      </div>
      <div class="col-md-6">
        <img src="https://source.unsplash.com/600x400/?group-travel" class="img-fluid" alt="Group Travel">
      </div>
    </div>

    <div class="row mt-5">
      <div class="col-md-6">
        <img src="https://source.unsplash.com/600x400/?airplane-ticket" class="img-fluid" alt="Airplane Ticket">
      </div>
      <div class="col-md-6">
        <h2>Subsidized Airfare: Your Ticket to Adventure</h2>
        <p>
          One of the main benefits of joining the Captains Program is the subsidized airfare. By organizing a group trip, you could earn significant discounts or even free airfare, making your travel dreams more accessible and affordable than ever before. So why wait? Join the Captains Program today and start exploring the world with like-minded adventurers!
        </p>
      </div>
    </div>
  </div>

  <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.6/dist/umd/popper.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.min.js"></script>
</body>

</html>

@endsection