<div class="row" style="justify-content: center;" id="demo">
    <div class="col-md-6 text-center" style="align-self: center;">
     	<h1 class="text-white">Preview</h1>
     	<h4 class="text-white">We've completely changed <br> how itineraries look and feel</h4>
     	<ul style="list-style:none; max-width:400px; margin:0 auto; margin-bottom:30px" class="text-left">
     		<li class="text-white" style="padding:10px 20px"><i class="fa fa-check" style="margin-right: 30px;"></i> Share Your Travel, Beautifully</li>
     		<li class="text-white" style="padding:10px 20px"><i class="fa fa-check" style="margin-right: 30px;"></i> Leave an Unforgettable Impression</li>
     		<li class="text-white" style="padding:10px 20px"><i class="fa fa-check" style="margin-right: 30px;"></i> Achieve to Your Financial Goals</li>
     	</ul>
     	<div class="row">
    		<div class="col-md-12 text-center">
				<ul class="nav nav-tabs" id="myTab" role="tablist" style="border-bottom:none; justify-content:space-between; max-width: 345px; margin: 0 auto;">
				  <li class="nav-item" role="presentation">
				    <button class="nav-link active" id="home-tab" data-toggle="tab" data-target="#home" type="button" role="tab" aria-controls="home" aria-selected="true" style="border-radius: 20px;">Timeline View</button>
				  </li>
				  <li class="nav-item" role="presentation">
				    <button class="nav-link" id="home-two-tab" data-toggle="tab" data-target="#homeTwo" type="button" role="tab" aria-controls="profile" aria-selected="false" style="border-radius: 20px;">Story View</button>
				  </li>
				</ul>
			</div>
    	</div>
    </div>
    <div class="col-md-6 text-center">
    	
    	<div class="row" style="margin-top:25px;">
			<div class="col-md-12 text-center tab-content" id="myTabContent">
				<div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
					<img src="/assets/img/mockups/screenshots/list-mode-start-iphone-13.png" style="height: 474px; width: 224px; margin-top: 12px;" id="screenshot-timeline-view">
					<img src="/assets/img/iphone-13-frame-adjusted.png" style="position: absolute; top: 0; margin: 0 auto; height: 500px; width: 246px; left: 50%; margin-left: -123px;">
				</div>
				<div class="tab-pane fade show" id="homeTwo" role="tabpanel" aria-labelledby="home-two-tab">
					<img src="/assets/img/mockups/screenshots/story-mode-start-iphone-13.png" style="height: 474px; width: 224px; margin-top: 12px;" id="screenshot-story-view">
					<img src="/assets/img/iphone-13-frame-adjusted.png" style="position: absolute; top: 0; margin: 0 auto; height: 500px; width: 246px; left: 50%; margin-left: -123px;">
				</div>
			</div>
		</div>
    </div>
</div>