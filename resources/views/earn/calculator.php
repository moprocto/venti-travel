<div class="row text-left calcrows" style="max-width: 730px;">
	<div class="col-md-6 mb-4 ">
    <div class="sliderContent">
  		<h5>Number of Trips</h5>
  		<input type="text" id="pool-amount" class="calculate" 
            data-provide="slider"
            data-slider-ticks="[1,4,7,10,13,16]"
            data-slider-ticks-labels='[1,4,7,10,13,16]'
            data-slider-min="1"
            data-slider-max="20"
            data-slider-step="3"
            data-slider-value="4"
            data-slider-tooltip="hide" />
      </div>
    </div>
    <div class="col-md-6 mb-4">
      <div class="sliderContent">
  		<h5>Avg. Nights per Trip</h5>
  		<input type="text"  id="pool-size" class="calculate" 
            data-provide="slider"
            data-slider-ticks="[1,3,5,7,9,11,13,15]"
            data-slider-ticks-labels='[1,3,5,7,9,11,13,15]'
            data-slider-min="1"
            data-slider-max="15"
            data-slider-step="2"
            data-slider-value="7"
            data-slider-tooltip="hide" />
      </div>
      </div>
    <div class="col-md-6 mb-4">
      <div class="sliderContent">
        <h5>Avg. Group Size per Trip</h5>
        <input type="text"  id="pool-group" class="calculate" 
          data-provide="slider"
          data-slider-ticks="[4,6,8,10,12,14,16,18]"
          data-slider-ticks-labels="[4,6,8,10,12,14,16,18]"
          data-slider-min="1"
          data-slider-max="18"
          data-slider-step="2"
          data-slider-value="8"
          data-slider-tooltip="hide" />
          <p>Each group must have a minimum of four people. </p>
        </div>
    </div>
    <div class="col-md-6 mb-4">
      <div class="sliderContent">
		<h5>Avg. Tip per Person</h5>
        
		<input type="text"  id="pool-rate" class="calculate" 
          data-provide="slider"
          data-slider-ticks="[0, 20, 40, 60, 80, 100]"
          data-slider-ticks-labels='[0, 20, 40, 60, 80, 100]'
          data-slider-min="0"
          data-slider-max="100"
          data-slider-step="20"
          data-slider-value="20"
          data-slider-tooltip="hide" />
          <p>Concluding each trip, we send out surveys to collect feedback. Trip mates have the option of leaving a gratuity.</p>
    </div>
  </div>
    <div class="col-md-12 text-center">
<div class="sliderContent">
    	<h2>$<span id="pool-payout">125.00</span> 🎉</h2>
    	<p style="font-size:10px;">(The above figure is an estimatation based on your decision to exchange points for cashback.)</p>
    </div>
  </div><div class="sliderContent">
</div>