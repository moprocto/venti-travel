@extends("layouts.curator-master")

@section("css")
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/11.0.2/css/bootstrap-slider.min.css">
	<link rel="stylesheet" type="text/css" href="/assets/css/earn.css">
@endsection

@section('content')
<div id="page-content">
   <div class="gradient-wrap">
      <!--  HERO -->
      <div class="gallery-holder desktop-gallery">
      	<img src="/assets/img/selfie-paradise.jpg" style="left:5%; top:100px">
      	<img src="/assets/img/selfie-water.jpg" style="left: 5%; bottom:0px;">
      	<img src="/assets/img/friends-beach.jpg" style="right: 5%; bottom: -50px;">
      	<img src="/assets/img/boots-river.jpg" style="right: 5%; top:200px;">
      </div>
	  <div class="black position-relative hero-section mb-4" style="margin: 20px 4em;">
	     <div class="row" style="justify-content: center; padding-top:10%">
	        <div class="col-md-12 text-center" style="max-width:700px; height:100%">
	           <h1>Monetize Your <br> Travel Experiences</h1>
	           <h4>Transform your content and brand into an effortless and reliable income stream</h4>
	           <br>
	           <a class="dark-on-light nav-links btn btn-primary" href="#apply">Apply Now</a>
	           <br>
	           <p style="margin-top:10px;">For Our Creators Program</p>
	           @error('success')
			        
	           <div id="emitter"></div>
			   <div class="alert alert-success">
	            	<span>Your application was submitted. Our team will try and provide an initial response via email as soon as possible.</span>
	        	</div>

			   @enderror
			   
	        </div>
	        <div class="gallery-holder mobile-gallery" style="margin-top:20px;">
		      	<img src="/assets/img/selfie-paradise.jpg" style="left:20px; top:100px; max-height:120px;">
		      	<img src="/assets/img/selfie-water.jpg" style="left: 20px; bottom:0px; max-height:140px">
		      	<img src="/assets/img/friends-beach.jpg" class="smaller"  style="right: 20px; bottom: -50px; max-height:120px;">
		     </div>
	     </div>
	  </div>
   </div>
   <div class="gradient-wrap bg-dark" style="padding:100px 20px;">
   		<div class="row" style="justify-content: center;">
	        <div class="col-md-12 text-center" style="max-width:900px; padding:0 30px;">
	           <h1 class="text-white">Here's How</h1>
	           <p class="text-white">Whether you're a travel blogger, advisor, or enthusiast, Venti can help you earn more from your passion. Grow and maintain your brand every step of the way when you list with Venti.</p>
	           	<div class="row text-left">
	           		<div class="col-md-4">
	           			<div class="card bg-dark">
	           				<div class="card-body bg-dark text-center" style="border-radius: 20px;">
	           					<img src="/assets/img/unordered-dots.png" style="width: 100px;">
	           					<h2 class="text-white">1</h2>
	           					<p class="text-white">We turn your travel-related TikToks, stories, reels, personal experiences, and travel plans from past clients into awesome trip itineraries.</p>
	           				</div>
	           			</div>
	           		</div>
	           		<div class="col-md-4">
	           			<div class="card bg-dark ">
	           				<div class="card-body bg-dark text-center" style=" border-radius: 20px;">
	           					<img src="/assets/img/ordered-dots.png" style="height:100px">
	           					<h2 class="text-white">2</h2>
	           					<p class="text-white">Your itineraries are marketed and sold online via our website and mobile app. Travelers access the info via a private link, not a PDF.</p>
	           				</div>
	           			</div>
	           		</div>
	           		<div class="col-md-4">
	           			<div class="card bg-dark">
	           				<div class="card-body bg-dark text-center" style=" border-radius: 20px;">
	           					<img src="/assets/img/split-dots.png" style="width: 100px;">
	           					<h2 class="text-white">3</h2>
	           					<p class="text-white">You receive a whopping 75% commission on the sale of each itinerary and can even include upsells like packing lists and custom content.</p>
	           				</div>
	           			</div>
	           		</div>
				</div>
	        </div>
	    </div>
   </div>
   <div class="gradient-wrap" style="padding:100px 20px;">
   		<div class="row" style="justify-content: center;">
	        <div class="col-md-12 text-center" style="max-width:700px;">
	         	<h1>How Much?</h1>
	         	<p>Our research shows that travelers will spend on average $50 for an off-the-shelf itinerary. Calculate your potential earnings below.</p>
	           	@include("earn.calculator")
	        </div>
	    </div>
   	</div>
   	<div class="gradient-wrap bg-dark" style="padding:100px 20px;">
   		@include('earn.demo')
   	</div>
	<div class="row" style="width: 100%; margin:0;">
		<div class="col-md-6 text-center" style="background-image: url('/assets/img/volleyball.jpeg'); 
		    padding: 100px 20px;
		    background-size: cover;
		    background-position: center;
		    background-color: rgba(0, 103, 205, 1);
		    background-blend-mode: overlay;">
			<h2 class="text-white" style="font-size: 2.5em; text-shadow: 2px 3px 5px  rgba(0,0,0,0.5);">Ready to Start Earning?</h2>
			<p class="text-white" style="font-weight: bold; font-size: 19px; text-shadow: 1px 2px 2px  rgba(0,0,0,0.8);">We could not be more excited to work with you</p>
			<a class="nav-links btn" href="#apply" style="background:black; color:white">Apply Now</a>
		</div>
		<div class="col-md-6 text-center" style="background-image: url('/assets/img/zipline.jpeg'); 
    padding: 100px 20px;
    background-size: cover;
    background-position: center;
    background-color: rgba(29, 126, 137, 0.7);
    background-blend-mode: overlay;">
			<h2 class="text-white" style="font-size: 2.5em; text-shadow: 2px 3px 5px rgba(0,0,0,0.5);">10 Ways To Maximize Profits</h2>
			<p class="text-white" style="font-weight: bold; font-size: 19px; text-shadow: 1px 2px 2px  rgba(0,0,0,0.8);">Some ways you can go above and beyond</p>
			<a class="nav-links btn" href="https://www.canva.com/design/DAFGPUFkHg8/HotzeWlQJ4v-HZ8InK_xCQ/view?utm_content=DAFGPUFkHg8&utm_campaign=designshare&utm_medium=link2&utm_source=sharebutton" target="_blank" style="background:white; color:black">View Slides</a>
		</div>
	</div>
	<div class="gradient-wrap" style="padding: 100px 20px; display: none;">
		<div class="row">
			<div class="col-md-6 text-center" style="align-self: center;">
				<h1>Creator Testimonials</h1>
				
				<p>We built out program from the ground up to support everyday creators whether its a full-time job or part-time hobby. You get a team ready to move mountains to help you succeed.</p>
			</div>
			<div class="col-md-6 text-center">
				<img src="/assets/img/advisors/trendy-traveler.jpeg" style="width: 100%; max-width: 240px; border-radius: 50%; margin: 30px">
				<h3>@trendytraveler</a></h3>
				<p><a href="https://www.tiktok.com/@trendytraveler" target="_blank">TikTok</a>: 112K Followers</p>
				<p><a href="https://instagram.com/trendytraveler__" target="_blank">Instagram</a>: 49K Followers</p>
			</div>
		</div>
	</div>
	<div class="gradient-wrap bg-dark" style="padding:100px 20px;">
   		<div class="row" style="justify-content: center;">
	        <div class="col-md-12 text-center" style="max-width:900px; padding:0 30px;">
	           <h1 class="text-white" id="apply">Apply</h1>
	           <p class="text-white">There are no fees to apply or join our Creators Program. Our application process ensures we admit those that seek to offer only the best experiences to travelers.</p>
	           <br>
	           @include("forms.apply")
	        </div>
	    </div>
   </div>
</div>
@endsection

@section("js")
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/11.0.2/bootstrap-slider.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/canvas-confetti@1.5.1/dist/confetti.browser.min.js"></script>
<script src="/assets/js/earn.js"></script>
@endsection