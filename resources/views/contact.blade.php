@extends("layouts.curator-master")


@section("content")

<div id="page-content">
    <div class="gradient-wrap">
	    <!--  HERO -->
	    	<div class="black position-relative hero-section mb-4" style="margin: 20px 4em;">
	        	<div class="row" style="justify-content: center;">
	            	<div class="col-md-4 text-center">
	                    <h1 class="mb-3" style="font-weight:600 !important;">
                    		Let's Connect
                    	</h1>
                    	<div class="card">
                    		<div class="card-body text-left">
                    			@error('success')
                					<div class="alert alert-success">
                						<span>Your message was sent!</span>
                					</div>
                				@enderror
                				@error('incorrectAnswer')
                					<div class="alert alert-danger">
                						<span>CAPTHa failed. Try again.</span>
                					</div>
                				@enderror
                    			<form method="POST" action="/contact" id="contactForm">
                    				<input type="hidden" name="random" id="random">
                    				@CSRF
                    				<div class="form-group">
                    					<label class="form-label">Email:</label>
                    					<input type="text" class="form-control" name="email" placeholder="your@email.com" required="" value="{{ old('email') }}">
                    				</div>
                    				<div class="form-group">
                    					<label class="form-label">You want to:</label>
                    					<select class="form-control" name="subject" required="">
                    						<option value="">-- Please Select --</option>
                    						<option value="Product Question" @if(old('subject') == 'Product Question') selected @endif>Product Question</option>
                    						<option value="Do not sell my info" @if(old('subject') == 'Do Not Sell My Info') selected @endif>Do not sell my info</option>
                    						<option value="Submit General Feedback" @if(old('subject') == 'Submit General Feedback') selected @endif>Submit General Feedback</option>
                    						<option value="Request Account Deletion" @if(old('subject') == 'Delete my Venti Account') selected @endif>Request Account Deletion</option>
                    						<option value="Report a User" @if(old('subject') == 'Report a User') selected @endif>Report a User</option>
                    					</select>
                    				</div>
                    				<div class="form-group">
                    					<label>Your Message:</label>
                    					<textarea class="form-control" name="message" required="">{{ old('message') }}</textarea>
                    				</div>
                    				<div class="form-group">
                    					  <div class="cf-turnstile" id="example-container" data-sitekey='{{ env("CLOUDFLARE_TURNSTYLE_SITEKEY") }}'></div>

                    				</div>
                    				<div class="form-group">
                    					<button type="submit" name="submit" class="btn btn-primary" id="submitContact">Submit</button>
                    				</div>
                    				<br>
                    				<p>Need help with something else? Reach our support team via email at admin@venti.co</p>
                    			</form>
                    		</div>
                    	</div>
                    </div>
	            </div>
	        </div>
	    </div>
	</div>
</div>

@endsection

@section('js')
	<script src="https://challenges.cloudflare.com/turnstile/v0/api.js?render=explicit"></script>
	<script type="text/javascript">
		// if using synchronous loading, will be called once the DOM is ready
		turnstile.ready(function () {
		    turnstile.render('#example-container', {
		        sitekey: '{{ env("CLOUDFLARE_TURNSTYLE_SITEKEY") }}',
		        callback: function(token) {
		            console.log(token);
		        	$("#random").val(token);
		            console.log(`Challenge Success ${token}`);
		        },
		    });
		});

		$("#button").click(function(){

		});
	</script>
@endsection