@extends("layouts.curator-master")


@section('content')

<div id="page-content">
    <div class="gradient-wrap">
        <div class="container hero-section mb-4 mt-4">
            <div class="row justify-content-center">
                <div class="col-md-12 text-center mt-4">
                    <h1>Time to Interview</h1>
                </div>
                <div class="col-md-8">
                     <div class="card">
                        <div class="card-body text-center">
                            <script type="text/javascript" async src="https://static.zcal.co/embed/v1/embed.js"></script>
							<div class="zcal-inline-widget"><a href="https://zcal.co/i/L-0Wyd_l">Venti Navigator Interview - Schedule a meeting</a></div>
                        </div>
                     </div>
               </div>
            </div>
        </div>
    </div>
</div>
@endsection