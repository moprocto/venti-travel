@extends('layouts.curator-master')

@section('css')
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" href="https://tiles.locationiq.com/v2/libs/gl-geocoder/4.5.1/locationiq-gl-geocoder.css?v=0.2.2" type="text/css" />
	<style type="text/css">
		.hide{
			display: none;
		}
		#plan{
			padding: 20px;
		}
		.btn-white{
			color: black !important;
		}
		#loader{
			display: none;
		}
		#map {
		  height: 100%;
		  min-height: 400px;
		  display: block;
		  overflow: inherit !important;
		  overflow-x: hidden !important;

		}
		#pac-input{
		display: block;
		left: 10px !important;
		position: absolute;
		margin-top: 10px;
		max-width: 350px;
		}
		.gmnoprint{
			top: 55px !important;
			
		}
		.form-label{
			font-weight: bold;
			
		}
</style>
<script src="https://polyfill.io/v3/polyfill.min.js?features=default"></script>
@endsection

@section('content')
	<div id="page-content">
	    <div class="gradient-wrap">
	        <div class="container hero-section mb-4 mt-4">
	            <div class="row justify-content-center">

	            	<div class="col-md-7">
	            		<div class="row">
	            			<div class="col-md-12 mb-4">
	            				<div class="card">
	            					<div class="card-body">
	            						<h4>About This Tool</h4>
	            						<p>You can use this simple editor to design a Navigated trip. We've included widgets on the right-hand side so you can augment your research with Google Maps and ChatGPT. We'll be adding more tools as we go along to make the process easier. As a reminder, your trips should start and end around an airport (preferably international airports) to make it easier for travelers.</p>
	            					</div>
	            				</div>
	            			</div>
	            		</div>
	            		<div class="card">
	            			<div class="card-body">
	            				<div id="save-status"></div>
	            				<label class="text-bold">Your Trip Plan</label>
								<textarea id="planner"></textarea>
							</div>
						</div>
	            	</div>
	            	<div class="col-md-5">
	            		<div class="row">
	            			<div class="col-md-12 mb-4">
	            				<div class="card">
	            					<div class="card-body">
	            						<label class="form-label">Map</label>
	            						<input
									      id="pac-input"
									      class="controls form-control"
									      type="text"
									      placeholder="Search Box"
									    />
									    <div id="map"></div>
	            					</div>
	            				</div>
	            			</div>
	            			<div class="col-md-12 mb-4">
	            				<div class="card">
	            					<div class="card-body">
	            						<label class="form-label">ASK ChatGPT</label>
	            						<input type="text" name="prompt" id="prompt" class="form-control" required="">
			            				<br>
			            				<button type="button" id="generate" class="btn btn-primary"><span id="loader-spin" class="hide"><i class="fa fa-spinner fa-spin"></i></span> SEARCH</button>
	            					</div>
	            				</div>
	            			</div>
	            			<div class="col-md-12">
	            				<div class="card">
		            				<div class="card-body">
		            					<label class="form-label">ChatGPT Response:</label>
		            					<div id="loader" style="width:100%; text-align:center;">
				                				<div class="loading-box">
				                					<img src="/assets/img/airplane.gif" style="width:200px;">
				                					<h3>Researching...</h3>
				                				</div>
				                			</div>
		            					<div class="response" id="response" style="min-height:100px;">

				                		</div>
		            				</div>
	            				</div>
	            			</div>
	            		</div>
	            	</div>
	            </div>
	        </div>
	    </div>
	</div>
@endsection

@section('js')
    <script src="https://cdn.tiny.cloud/1/f1cdeokivvyzyohrpb09kwayr4k53ovtgo1849tqerl354st/tinymce/6/tinymce.min.js" referrerpolicy="origin"></script>
    
	<script>
		jQuery(document).ready(function ($) {

		$.ajaxSetup({
         headers: {
            'X-CSRF-TOKEN': '{{ csrf_token() }}'
         }
      	});

        $("#generate").on("click", function(){
        	$("#loader").show();
        	$("#loader-spin").show();
        	$("#response").html("");

        	var prompt = $("#prompt").val();

            $.ajax({
                url: "{{ route('navigator-plannerGPT') }}",
                type: 'post',
                processData: true,
                data:{
                	prompt: prompt
                },
                dataType: 'json',
                success: function (data) {
                	if(data == 501){
                		$("#loader-spin").hide();
                		$("#loader").hide();

                		return alert("The operation timed out due to an error. It might be a connection issue. Please try again or contact admin@venti.co");
                	}

                	$("#loader").hide();
            		$("#loader-spin").hide();
            		$("#plan").show();
            		$("#response").html(data.answer)

                    return true;
            	}
            });

        });

        tinymce.init({
	        selector: 'textarea#planner',
	        menubar: false,
	        plugins: [
	          'link', 'lists'
	        ],
	        toolbar: 'undo redo saveChangesButton | formatselect | ' +
	        'bold italic underline alignleft aligncenter alignright | numlist bullist link | ' +
	        'removeformat',
	        content_style: 'body { font-family:Helvetica,Arial,sans-serif; font-size:14px }',
	        setup: function (editor) {
			    editor.ui.registry.addButton('saveChangesButton', {
			      image: 'http://p.yusukekamiyamane.com/icons/search/fugue/icons/calendar-blue.png',
			      tooltip: 'Save Changes',
			      disabled: false,
			      onAction: function (_) {
			        var myContent = tinymce.get("editor").getContent();
			        updateTripPlan(myContent);
			      }
			    });
			  }
	      });

 
		
    });
	</script>
	<script >
		function initAutocomplete() {
		  const map = new google.maps.Map(document.getElementById("map"), {
		    center: { lat: 38.8937255, lng: -77 },
		    zoom: 12,
		    mapTypeId: "roadmap",
		  });
		  // Create the search box and link it to the UI element.
		  const input = document.getElementById("pac-input");
		  const searchBox = new google.maps.places.SearchBox(input);

		  map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
		  // Bias the SearchBox results towards current map's viewport.
		  map.addListener("bounds_changed", () => {
		    searchBox.setBounds(map.getBounds());
		  });

		  let markers = [];

		  // Listen for the event fired when the user selects a prediction and retrieve
		  // more details for that place.
		  searchBox.addListener("places_changed", () => {
		    const places = searchBox.getPlaces();

		    if (places.length == 0) {
		      return;
		    }

		    // Clear out the old markers.
		    markers.forEach((marker) => {
		      marker.setMap(null);
		    });
		    markers = [];

		    // For each place, get the icon, name and location.
		    const bounds = new google.maps.LatLngBounds();

		    places.forEach((place) => {
		      if (!place.geometry || !place.geometry.location) {
		        console.log("Returned place contains no geometry");
		        return;
		      }

		      const icon = {
		        url: place.icon,
		        size: new google.maps.Size(71, 71),
		        origin: new google.maps.Point(0, 0),
		        anchor: new google.maps.Point(17, 34),
		        scaledSize: new google.maps.Size(25, 25),
		      };

		      // Create a marker for each place.
		      markers.push(
		        new google.maps.Marker({
		          map,
		          icon,
		          title: place.name,
		          position: place.geometry.location,
		        })
		      );
		      if (place.geometry.viewport) {
		        // Only geocodes have viewport.
		        bounds.union(place.geometry.viewport);
		      } else {
		        bounds.extend(place.geometry.location);
		      }
		    });
		    map.fitBounds(bounds);
		  });
		}

		window.initAutocomplete = initAutocomplete;
	</script>
	<script
      src="https://maps.googleapis.com/maps/api/js?key={{ env('GOOGLE_MAPS_KEY') }}&callback=initAutocomplete&libraries=places&v=weekly"
      defer
    ></script>
@endsection