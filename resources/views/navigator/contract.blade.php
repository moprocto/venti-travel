<p class="text-left">
<br><br>
This Navigator Agreement for the provision of services (hereinafter referred to as the User Agreement or the Agreement) is part of the public offer of the Venti Service (that is, an offer to conclude an Agreement on the specified terms) and determines the general terms and conditions and procedure for using the Venti mobile application, web services and materials located on the Venti Site , the rights to which belong to VENTI FINANCIAL, INC. and can be supplemented and specified by separate additional terms, conditions and rules and other documents regulating the operation of the Service and being an integral part of the public offer of the Venti Service.

<br><br>
1. Terms

<br><br>
1.1. Venti is:

<br><br>
(1) an online service hosted on the Site that allows Navigators to publish Announcements offering Navigator’s trips, as well as to negotiate, and make settlements with Travelers who intend to purchase or have purchased Navigator’s trips.

<br><br>
(2) an Internet resource, a set of programs owned by Venti, through which Venti provides Navigators with the following services:
(a) promotion of Navigator’s trips,
(b) ensuring the technical possibility of interaction between Navigators and Users, booking Travel Services and the organization of settlements with Travelers,
(c) to assist in resolving disputes between Users by providing information regarding their actions in connection with the conclusion and execution of transactions between them.

<br><br>
1.2. Site

<br><br>
An automated information system available on the internet at the address (including subdomains) Venti.co and venti.travel. The Site is used by Users to post Announcements of Navigator’s travels and conclude agreements for the provision of services of Navigator’s travels.

<br><br>
1.3. Administrator (Administration) 

<br><br>
Venti Financial, Inc. which owns all rights to the Site, Venti Service and domain venti.co and which provides services to Users on the terms of these General Terms and Conditions and the public offer of the Service. Venti is not: a travel organizer or a person providing travel services nor a seller of tourism products. By interacting using the Venti Service, Users enter into a contractual relationship directly with each other. Venti is not a party or other participant in any contractual relationship between Users, and is not a travel agent or insurer in relation to any insurance risks of Users. Venti does not act as an agent of the Users, except as provided in the Payment Services Terms. Navigators are not employees nor consultants of Venti.

<br><br>
1.4. User (Traveler)

<br><br>
A capable natural person who has reached the age of 18 and acts on their own behalf and is interested in receiving the Service. If the User is a legal entity, then the individual acting on its behalf is recognized as duly authorized by such a legal entity and is obliged to provide Venti and/or the User with whom it concludes a transaction with the necessary documents confirming the legal status of the legal entity and its powers. representative.

<br><br>
1.5. Navigators

<br><br>
Are approved, able-bodied individuals and legal entities that have placed an Advertisement on the Service with an offer of an Navigator’s trip on the basis of a public offer of the Service and that meet the Requirements for Navigators.

<br><br>
1.6. Account

<br><br>
A unique User account on the Site, which allows you to uniquely identify the User.

<br><br>
1.7. Personal Account

<br><br>
A set of secure pages of the Site, access to which is provided to the User using credentials (login and password) after creating an Account on the Site, and using which Venti, the User and Navigators have the opportunity to interact with each other in order to fulfill this Agreement.

<br><br>
1.8. Navigator's trip or Navigator's tour

<br><br>
The Navigator's service to accompany the Traveler in accordance with the Announcement and (or) the agreement concluded with the Traveler, containing the "Stay Program" agreed by the Navigator and the Traveler and corresponding to the Conditions and Procedure for Travel.

<br><br>
1.9. An order (transaction)

<br><br>
Is a civil law transaction in the form of a Service Agreement concluded between the Traveler and the Navigator, information about which is posted on the Venti website.

<br><br>
1.10. Announcement

<br><br>
An offer of the Navigator Service posted by them on the Venti service.

<br><br>
1.11. Booking  - the User's consent to the offer of the Navigator, confirmed by the payment/prepayment for the Services of the Navigator. Consent is formed on the terms of the Announcement or others agreed by the parties by agreeing on an independent contract, in the manner prescribed by the Procedure and conditions for concluding transactions between Users of the Venti Service. 

<br><br>
1.12. Content  - audiovisual, graphic, textual, photographic and other display of the Software activity, forming information in any form and form, posted on the Site by the Site Administration, Navigators, third parties.
<br><br> 
2. Subject of the Agreement

<br><br>
2.1. Venti grants the Navigator a non-exclusive license to use the Site, and the Navigator undertakes to comply with the terms of use of the Site.
<br><br> 
2.2. Under this Agreement, Venti provides information services and undertakes, on its own behalf, at its own expense, but in the interests of the Navigator, to provide information services for the placement of Navigator’s tours and the formation of Orders with Users, and the Navigator undertakes to accept the results of the information services provided and pay remuneration to Venti in the amount and in the manner prescribed by this Agreement.
<br><br> 
2.3. The Service provides information services to the Navigator for posting information about the Navigator’s tour on the Site, as well as for placing an Order for the Navigator’s tour with the User. Venti is not the organizer of Navigator’s tours, the seller of any goods or services. All obligations regarding the provision of Navigator’s tours, the sale and purchase of relevant goods or the provision of relevant services arise between the User and the Navigator and / or third parties in accordance with the terms of the Order. Venti is not responsible for the validity and enforceability of the specified obligations of Navigators. Venti is also not authorized by their actions to recommend any Users, Announcements or Navigator Trips. Any indication that the User is "confirmed" (or other similar definitions),
<br><br> 
2.4. The provisions of the Agreement and the User Agreement shall apply to the relations of the Parties insofar as they do not contradict the Agreement. 
<br><br> 
3. Registration

<br><br>
3.1. To register on the Site, the Navigator presses the "Register" button and fills out a questionnaire form, in which they indicate personal data in accordance with the fields. The Navigator guarantees that it provides reliable and non-violating information about itself.
<br><br> 
3.2. After the registration is completed, the Navigator receives a notification of the completion of the registration to the e-mail specified during registration.
<br><br> 
3.3. All actions on the Site under the created Account are considered to be performed by the Navigator personally or by an authorized representative pre-approved by Venti.
<br><br> 
3.4. The Navigator is obliged to indicate their bank details in the Navigator Management section of their profile and is responsible for all fees associated with receiving payments to their indicated bank.
<br><br> 
4. Offering Services

<br><br>
4.1. The Navigator places on the Website information about the Services, additional services, related products. A Navigator can edit information about the Services in the Personal Account.

<br><br>
4.2. The Navigator guarantees that the information about the Services related to the Navigator’s tours are true, complete and up-to-date both at the time of addition and in the future.
<br><br> 
4.3. In the description of the Services it is forbidden to use:

<br><br>
4.3.1. obscene expressions, insults;

<br><br>
4.3.2. contacts and personal data of the Navigator or their representative for placing an Order not on the Site; 

<br><br>
4.3.3. texts that make it difficult to perceive information, including those typed in uppercase, Latin, with a large number of errors, without punctuation marks or spaces.

<br><br>
4.4. Uploaded photos and videos, as well as description texts for Navigator’s tours, must belong to a Navigator, demonstrate the directly offered Service, additional services or related products. Navigator agrees to only utilize photos for which they have permission to use.

<br><br>
4.5. In case of successful verification of information about the Services, Venti forms a Navigator profile on the Site.
<br><br> 
4.6. In case of detection of violations of copyright, related rights and other rights to objects of intellectual property, Venti has the right to block the profile of a Navigator, delete their ads.

<br><br>
5. Rights and obligations of a Navigator

<br><br>
5.1. The Navigator undertakes to get acquainted with the current version of the Agreement. The Navigator undertakes to strictly fulfill the duties of the Navigator stipulated by the Order (Service Agreement).
<br><br> 
5.2. When posting information about the Navigator’s tour (provided Services), the Navigator undertakes to indicate the following information:

<br><br>
5.2.1. type of Navigator’s tour and its description, broken down by dates;

<br><br>
5.2.2. description of the route of the Navigator’s tour;

<br><br>
5.2.3. the number of participants in the Navigator’s tour;

<br><br>
5.2.4. description of additional services;

<br><br>
5.2.5. conditions for the return of funds, in case of cancellation of the Order by the User more than 24 hours in advance. These conditions must not contradict the Refund Policy posted on the Site. Venti has the right to withhold in its favor the remuneration for the generated Order, as well as the commission for the transfer;

<br><br>
5.2.6. a list of necessary/recommended clothing/equipment for the User (if necessary);

<br><br>
5.2.7. a list of equipment and equipment provided to the User (if necessary); 

<br><br>
5.2.8. on the requirements imposed by authorized bodies on documents (including for foreign citizens);

<br><br>
5.2.9. on the possibility of acquiring medical insurance and on the conditions of insurance;

<br><br>
5.2.10. about the rules of conduct during participation in the Navigator’s tour;

<br><br>
5.2.11. about possible difficulties associated with ignorance of the language of communication of the country (place) of residence;

<br><br>
5.2.12. on the state of the natural environment in the country (place) of stay and on the specifics of weather conditions;

<br><br>
5.2.13. sanitary and epidemiological situation in the country (place) of temporary residence;

<br><br>
5.2.14. about the dangers that may be encountered during participation in the Navigator’s tour;

<br><br>
5.2.15. about what medical measures (including vaccinations) must be taken to enter the country (place) of temporary residence and participate in the Navigator’s tour.
<br><br> 
5.3. The Navigator has the right to establish a fee structure for Navigator’s tours for Users. The fee structure must be approved by Venti.

<br><br>
5.4. The Navigator has the right to refuse service to any User deemed incompatible or unfit for Navigator’s tours.
<br><br> 
5.5. The Navigator undertakes to pay remuneration to Venti in accordance with the procedure and terms established by this Agreement. In cases (established by the User Agreement), when the User is refunded 100% of the funds paid by them, the Navigator's remuneration paid in favor of Venti is returned to them in full.
<br><br> 
5.5. The Navigator undertakes to provide Venti with information on the services actually provided by the Navigator for each Transaction (Order) with the User, to the extent required by Venti. The specified information is provided to Venti within 3 (three) working days after the request is received by the Navigator.

<br><br>
5.5.1. Information and documents confirming the expenses actually incurred by Navigator are provided no later than 14 (fourteen) calendar days from the date of sending the request to Navigator.
<br><br>
5.6. The Navigator undertakes to independently and properly fulfill the obligations to the User under the Transaction, as well as to independently settle the claims of the Users related to such obligations, including claims arising from the discrepancy between the amount of the Transaction and the current rates of the Navigator.
<br><br> 
5.7. The Navigator undertakes to be responsible for the legitimacy and legitimacy of the services provided by the Navigator within the framework of Navigator’s tours and Transactions.

<br><br>
5.8. The Navigator undertakes to notify Venti of any changes within 3 (three) calendar days, including: changes in its company name (last name, first name or patronymic), legal address, actual address, postal address, bank details, website address, the applicable taxation system, as well as passport data.
<br><br> 
5.9 If the information (content) posted by the Navigator is protected by copyright, the rights to such information are reserved by the Navigator who posted such information. At the same time, Navigator grants Users of the Venti Service a gratuitous non-exclusive right to use such content by viewing, reproducing (including copying), processing (including printing copies) and other rights solely for the purpose of personal non-commercial use, except in cases when such use causes or is likely to cause harm to the legally protected interests of the right holder.
<br><br> 
5.10. The Navigator undertakes, no later than 3 (three) days from the date of the request, to provide Venti with documents confirming the legitimacy and guarantees of the Navigator’s tours on the date indicated by the Navigator, as well as confirming the legitimacy and authority of the Navigator to make Transactions. 
<br><br> 
6. Conclusion and execution of the Service Agreement

<br><br>
6.1. The terms of the Services Agreement are collectively specified in: this Agreement; description of the Navigator’s tour; order.
<br><br> 
6.2. The User, using the Site, sends an Order, which is displayed in the Personal Account on the Site and in the application for Navigators.
<br><br> 
6.3. If the Navigator confirms the possibility of providing the Service in the Personal Account on the Website and in the application for Navigators, the Service Agreement is considered concluded. The User pays for the Services under the concluded agreement to a nominal account.
<br><br> 
6.4. The Navigator undertakes to properly fulfill the obligations under the Service Agreements (Transactions) concluded with the Users.
<br><br> 
6.5. The service can be paid by the User by means of internal installments according to the conditions of Venti's partner.
<br><br> 
6.6. Payment for the Services rendered by the Navigator is awarded by a points system (“Navigator Points”), which can be redeemed for United States Dollars at a base conversion of 1 point to $0.75 USD (“Conversion Rate”). Venti retains the right to adjust this conversion at any time and for any reason. Navigator is to receive their funds by the payment method details identified in their profile within 30 business days from the date of redemption.
<br><br> 

<br><br>
7. Conditions for changing and terminating the Agreement

<br><br>
7.1. The Navigator may cancel the Service Agreement (Order) within a period not exceeding 24 hours (96 hours for accounts with increased clearing) from the moment of receiving a request for the provision of Services within the Navigator’s tour, or until the User makes payment for such an Order. If the period for the Navigator to withdraw from the Agreement does not exceed 24 hours (96 hours for accounts with increased clearing), Venti automatically refunds the amount paid for the order to the User without the participation of the Navigator.
<br><br> 
7.2. In case of refusal of the User from the Agreement for the provision of services by Navigator:

<br><br>
7.2.1. Returns the funds to the User in full and transfers the commission for the transfer to Venti within 3 (Three) business days, unless otherwise specified in the description of the Navigator’s tour and the Order.

<br><br>
7.2.2. If the Navigator receives information about the impossibility of providing the booked and/or confirmed Service or about changing the parameters of the booked Service, Venti offers the User alternative Services.

<br><br>
7.3. If the Navigator did not provide the Service and/or additional service or did not provide the paid related products to the User, the funds shall be returned by the Navigator to the User in full within three working days.
<br><br> 
7.4. The cancellation conditions set by the expert independently and individually for each tour and relevant at the time of making the deposit, are not subject to change during the entire duration of the Tour.
<br><br> 
8. Prohibited actions of the Navigator. A Navigator is prohibited from:

<br><br>
8.1. Use the Site in ways not expressly provided for in the Agreement.
<br><br> 
8.2. Attempt to gain access to the personal information of other Navigators in any way, including, but not limited to, by deception, breach of trust or hacking of the Site.
<br><br> 
8.3. Take any actions, including technical ones, aimed at disrupting the normal functioning of the Site.
<br><br> 
8.4. Use any technical means to collect and process information on the Site, including personal data of Users and other Navigators.
<br><br> 
8.5. Copy, modify, prepare derivative works of, decompile, attempt to reverse engineer or otherwise modify the Site.
<br><br> 
8.6. Mislead other Navigators, Users, Administration in any way.
<br><br> 
8.7. Impersonate another person, their representative, without sufficient rights, including Venti or its employees, as well as use any other forms and methods of illegal representation of other persons.
<br><br> 
8.8. Use information about the received phones, postal addresses, e-mail addresses to send spam, i.e. messages of a commercial and non-commercial nature.
<br><br> 
8.9. Use logos, trademarks and / or other commercial designations of Venti without permission.
<br><br> 
8.10. Provide access to the Account to third parties. The Navigator is solely responsible for transferring access to the Account to third parties.
<br><br> 
8.11. Accept payment from Users at the place where the Services are provided and not pay remuneration to Venti.
<br><br> 
8.12. Change the Cancellation Conditions for the Tour if the deposit has already been received by the Navigator.
<br><br> 

<br><br>
9. Report

<br><br>
9.1. At the end of the reporting period, the Navigator confirms to Venti the information about the payments made by the Users to the Navigator.
<br><br> 
9.2. A report on completed Orders is reflected daily in the Navigator's Personal Account. The Order is considered to be executed, according to which the Navigator has provided the Service to the User and for which there are no complaints/comments from the User. In relation to the generated and executed Orders, Venti indicates the amount of remuneration for the information services provided to the Navigator.
<br><br> 
9.3. The report states:

<br><br>
9.3.1. numbers of executed Orders;

<br><br>
9.3.2. information about Users;

<br><br>
9.3.3. the amount of payment of the Navigator;

<br><br>
9.3.4. the amount of remuneration of Venti.
<br><br> 
9.4. The Navigator has the right to send their written objections to the report no later than 3 (three) business days from the date of its receipt. 
<br><br> 
9.5. If the Navigator does not send Venti written objections to Venti’s email address within the specified period, the report is considered accepted by the Navigator, and all primary documentation (acts, reports in the personal account) are considered automatically signed by the Navigator and are legally significant.
<br><br> 
10. Reward

<br><br>
10.1. In accordance with the United States and within the framework of this agreement, all operations are carried out using a nominal account (bank account) opened by Venti (account holder) for:
</p>
<ul class="text-left">
	<li>Settlements under the Service Agreements between the Navigator (the beneficiary of the nominal account) and the User; payment of remuneration to Venti;</li>
	<li>payment of commission for money transfer; transfer of funds to the settlement account of Venti and/or partners (paying agents) for settlements with Navigators who are tax non-residents of the United States;</li>
	<li>payment of an insurance premium in case of execution of an insurance contract in accordance with the conditions posted on the Site and / or in the description of the Navigator’s tour;</li>
	<li>payment of remuneration in case of registration by installments and payment of interest for the purchase of the Navigator’s tour in installments, according to the conditions posted on the Site and / or in the description of the Navigator’s tour.</li>
<ul>
<p class="text-left">
<br>
Nominal account movements are controlled by the account holder, the bank executes only the account holder's orders.

<br><br>
10.2. The rights to funds on a nominal account belong to the Navigator in the amount of obligations under the concluded Service Agreements with Users.

<br><br>
10.3. The nominal account is not used for the fulfillment by the account holder and the beneficiary of the account of their obligations to pay taxes. For these purposes, the parties use their current accounts in the usual manner.
<br><br> 
10.4. Not later than 3 (three) banking days from the date of confirmation of the Order by the Navigator, the funds received from the Users are transferred from the nominal account to the Navigator's current account, minus Venti's remuneration and transfer commission. The Navigator is responsible for fees inccured for currency exchanges.

<br><br>
10.5. Venti sends all the necessary information about the confirmation of the transaction, and the Navigator sends all the necessary data to receive funds. Venti does not guarantee the absence of errors and failures in relation to the provision of payment options.

<br><br>
10.6. Venti is not liable for calculating tax withholdings. The Navigator is responsible for determining their own tax liabilities based on their residency and national status.
<br><br> 
10.7. Payments are made to Venti in U.S. Dollars. The obligation of the Navigator is considered fulfilled from the moment the Services are provided to the Users and the remuneration is credited as Navigator Points to Venti's account.

<br><br>
10.8. Venti has the right not to transfer funds until the details are indicated by the Navigator in the Personal Account. Penalties for late payment are not charged and are not paid by Venti.

<br><br>
10.9. Navigator independently pays taxes on income received for Services, additional services, related products. When concluding the Agreement, the Navigator undertakes to provide Venti with reliable information about the taxation system used by the Navigator, and in case of receiving a relevant request from Venti, provide documents confirming the fact that the Navigator applies the relevant system.

<br><br>

11. Liability of the parties

<br><br>
11.1. In the event that Venti claims and claims from third parties in connection with a violation by the Navigator regarding the terms of the Agreement and / or the Service Agreement:

<br><br>
11.1.1. Venti informs the Navigator about the received requirements. The Navigator is obliged to independently, at its own expense and within a reasonable time, resolve these claims and disputes without involving Venti as a party to the dispute.

<br><br>
11.1.2. Venti has the right to pay the claims made on a voluntary basis and demand from the Navigator compensation for the losses incurred as a result of claims and lawsuits filed against it by third parties. Venti has the right to withhold in its favor the money to be transferred to the Navigator under the Service Agreement (Order).

<br><br>
11.1.3. Venti has the right to reimburse the incurred losses and / or transaction costs in the event of a refund to the User from any amounts received on the nominal account for the execution of the Order (service agreement) concluded by the User and the Navigator.

<br><br>
11.2. The Site is provided to the Navigator "AS IS" ("AS IS") in accordance with the principle generally accepted in international practice. This means that Venti is not responsible and does not reimburse the losses of the Navigator for problems that arise during the operation of the Site, including:

<br><br>
11.2.1. problems associated with the incorrect operation of the Site;

<br><br>
11.2.2. problems arising as a result of illegal actions of Venti staff, third parties, etc.;

<br><br>
11.2.3. inability to use the Site for reasons beyond the control of Venti.

<br><br>
11.3. Administration is not responsible for:

<br><br>
11.3.1. losses of the Navigator;

<br><br>
11.3.2. execution by the Navigator of the concluded Service Agreement (Order) with the User;

<br><br>
11.3.3. non-compliance of the cost of the Services indicated on the Site with the current price lists of the Navigator in the event that the Navigator did not notify about the change in the cost of the Services;

<br><br>
11.3.4. any actions of the User.
<br><br> 
11.4. In the event that the User paid for the Services, additional services or related products directly to the Navigator, and the Navigator did not pay Venti's remuneration, Venti has the right to terminate the Agreement with the Navigator unilaterally and delete the Navigator Account.
<br><br> 
11.5. Venti is an information intermediary in accordance with Article 1253.1 of the Civil Code of the United States and is not responsible for the violation of copyright, related rights and other intellectual property rights committed by Navigators when posting information about services on the Site. Venti presumes the conscientious attitude of Navigators to the information and images posted by them on the Site.

<br><br>
11.6. Venti reserves the right, in case of claims from any third parties for violation of copyright, related rights and other rights to intellectual property objects, to re-issue the specified requirements in full against the Navigator, as well as recover from them the losses caused by the specified violation.

<br><br>

11.7. Navigator shall not be liable for any illness, physical/bodily, medical, or mental injuries sustained during a Navigator trip unless proven that such injury was the direct result of negligence or intent to harm by the Navigator. In such instances, the Navigator and the connected User/third-party are to resolve such disputes at their own expense. Venti reserves the right to terminate a Navigator from our platform regardless of the result of such disputes.

<br><br>
11.8. Navigator shall direct all questions by Users regarding tourism eligibility, visas, customs, and other matters enacted by government tourism authorities to the Venti team. Failure to do so can result in the termination of a Navigator from the Venti platform.

<br><br>

12. Procedure for resolving disputes

<br><br>
12.1. All disputes that arise between the Parties in the course of the execution of the Agreement shall be resolved through negotiations.

<br><br>
12.2. The claim procedure for settling disputes arising from the performance of the Agreement is mandatory for the Parties. The term for consideration of the claim is 10 (ten) working days from the date of its receipt.
<br><br> 
12.3. If it is impossible to reach an agreement through negotiations or in a claim procedure, disputes are resolved in court in accordance with the legislation of the United States in the Arbitration Court of Maryland.
<br><br> 
13. Final provisions

<br><br>
13.1. The Agreement comes into force from the moment of acceptance and is valid until the removal of the Navigator Account.

<br><br>
13.2. Venti has the right to unilaterally, without notice, refuse to execute the Agreement and delete the Navigator Account in case of two-time improper execution by the Navigator of the Service Agreements concluded with the Users or repetitive conduct that is deemed inappropriate or violates our Terms of Service.
<br><br> 
13.3. The parties have agreed that reports, notifications, requests for documents and information, responses to them are sent to each other by e-mail:

<br><br>
13.3.1. by e-mail to Venti specified in the Details section of the Agreement;

<br><br>
13.3.2. by e-mail of the Navigator specified during registration on the Site.

<br><br>
13.4. Correspondence by e-mail, on the Site are legally binding, including in the event of legal proceedings. This way of exchanging documents and information is appropriate.
<br><br> 
13.5. The Parties undertake to notify each other of changes in details, including bank, postal, e-mail addresses, telephone numbers, within 3 (three) calendar days from the date of change. In the event that the Parties fail to fulfill this requirement, all notices, notifications sent to the details known to the other Party shall be considered properly sent.
<br><br> 
13.6. The Navigator's refusal to use the Service and/or the removal of the Content does not terminate Venti's non-exclusive rights to the Navigator's content, which was used by Venti until the decision was made to unilaterally terminate the Agreement by the Navigator.
<br><br> 
13.7. Legal relations between the Navigator and Venti arise from the moment of its accession to the terms of this Agreement and other documents that make up the public offer of the Venti Service, and exist for an indefinite period. The legal relations of the Parties shall be terminated if:

<br><br>
13.7.1. The Navigator has decided to terminate the use of the  Venti Service by sending the appropriate notice to Venti (by contacting the  Venti Service interface );

<br><br>
13.7.2. Venti will decide to terminate the legal relationship unilaterally out of court with the immediate termination of access and the ability to use the  Venti Service  and without reimbursement of any costs or losses, unless otherwise provided by applicable law. In particular, Venti has the right to make such a decision in the event of: the closure of the Service; any, including a single, violation by the Navigator of this Agreement and other documents constituting the public offer of the Venti Service.

<br><br>
13.8. Venti has the right to use information about the Services offered by the Navigator free of charge, in advertisements placed by Venti on the websites and services of third parties.
<br><br> 
13.9. By providing their personal data and expressing their consent to this agreement, the Navigator gives their consent to the processing, storage and use of their personal data on the basis of California Law for the following purposes:

<br><br>
13.9.1. registration of a Navigator on the website of the  Venti Service

<br><br>
13.9.2. implementation of customer support by the technical service of the site;

<br><br>
13.9.3. organizing settlements between the User and the Navigator in accordance with this agreement, transactions concluded between them in accordance with the procedure established by the current legislation;

<br><br>
13.9.4. fulfillment of obligations of the Navigator to its counterparties and Venti;

<br><br>
13.9.5. conducting audits and other internal studies by Venti in order to improve the quality of services provided;

<br><br>
13.9.6. providing information about them as a person who posted on the Service Content with copyright infringement of third parties in the event of a request from persons whose rights were violated by the Navigator.

<br><br>
13.10. If any provision of the Agreement is declared invalid or unenforceable by a court decision or other competent authority, this does not entail the invalidity of the Agreement as a whole and / or the remaining provisions of the Agreement.

<br><br>
13.11. Venti has the right to unilaterally amend the terms of the Agreement at any time. The changes come into force from the moment the new version of the Agreement is published on the Site. If the Navigator continues to use the Site, they agree to the changes in the Agreement.
</p>