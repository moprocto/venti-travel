<h2 class="mt-4">Welcome</h2>
<div class="card">
	<div class="card-body">
		<img class="mb-3" src="/assets/img/navigator-backdrop-sm.jpg" style="width:100%; max-width:900px; display:block;">
		<p>Additional resources will be added here over the coming weeks to maximize your success on our platform. Please contact markus@venti.co if you have any questions or concerns.</p>
	</div>
</div>