<h2 class="mt-4">Training Videos</h2>
<div class="row">
<div class="col-md-4">
	<div class="card ">
		<div class="card-body">
			<a href="https://us02web.zoom.us/rec/share/tobjwSG7Cz-16t5XTCnYj32f9kRVwAMjANLSv2xU73xO2QbK4w6Hp20dwCPBLdBy.dTHyMzsXSGhDEpYJ?startTime=1682876218000">
				<img src="/assets/img/venti-training-thumbnail.png" style="width:100%">
				<h4 class="mt-2">Overview Traning (1 hr 10 min.)</h4>
			</a>
		</div>
	</div>
</div>
<div class="col-md-4">
	<div class="card">
		<div class="card-body">
			<a href="https://us02web.zoom.us/rec/share/Cub2jBA4cg1Zxd3_AXdMceMf6vibGG5-CARMgXichxSI5mdUBRIM5IWI7bBgRQAj.EQfdajXspI2D_GXB?startTime=1682616358000" target="_blank">
			<img src="/assets/img/planner-training-thumbnail.png" style="width:100%">
			<h4 class="mt-2">Navigator Section + Planning Tool (10 min.)</h4>
		</a>
		</div>
	</div>
</div>
</div>