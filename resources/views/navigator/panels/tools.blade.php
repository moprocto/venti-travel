<h2 class="mt-4">Planner</h2>
<div class="card">
	<div class="card-body">
		<a href="{{ route('navigator-view-planner') }}" target="_blank">Plan A Trip</a>
	</div>
</div>

<h2 class="mt-4">Office Hours</h2>
<div class="card">
	<div class="card-body">
		<a href="https://zcal.co/i/TWkI8cxS" target="_blank">Need a Quick Chat?</a>
	</div>
</div>

@include('navigator.panels.training')