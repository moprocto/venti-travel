@extends("layouts.curator-master")

@section("css")
    <link rel="stylesheet" type="text/css" href="/assets/css/landing.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/store.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/steps.css">
    
    <style type="text/css">
        .bg-white{
            background: white;
        }
        .mob-shadow{
                box-shadow: rgb(100 100 111 / 30%) 0px 7px 29px 0px;
                position: absolute;
                height: 100%;
                width: 386px;
                background: transparent;
                right: 100px;
                bottom: 0;
                border-top-left-radius: 63px;
                border-top-right-radius: 63px;
        }
        @media (max-width: 767px){
            .mob-pt{
                padding-top: 3em;
            }
            .top-header{
                padding-top: 5em;
            }
            .mob-shadow{
                width: 333px;
                background: transparent;
                right: 20px;
            }
            .dark-on-light.nav-links{
                color: white;
            }
            .btn-white{
                color: black !important;
            }
        }
        
        #changethewords {
            display: inline !important;
        }
        h4.title{
         display: flex;
         justify-content: space-between;
         align-items: center;
         align-content: center;
      }
      .category{
         text-transform: uppercase;
         background: black;
         color: white;
         font-size: 12px;
         padding: 5px 10px;
         border-radius: 10px;
         margin-top: -3px;
      }
      body{
        background: black;
        background-color: black !important;
      }
      .gradient-wrap {
        margin-bottom: -64px;
      }
    span.fa-bars{
        background:rgba(255,255,255,0.9); 
        padding:8px 10px;
        border-radius:20px
    }
    .swiper-wrapper .grid-item{
        width: 100% !important;
        max-width: 100% !important;
    }
    .pills-tab{
    	border:1px solid black; padding: 5px 10px; border-radius:20px;
    	font-weight: bold;
    }
    .slider.slider-horizontal{
    	width: 87% !important;
    }
    .faq{
        margin-bottom: 20px;
        background-color: #F2F2F6;
        padding:20px;
        border-radius: 20px;
        padding-bottom:0;
        height: fit-content;
    }
    </style>
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/11.0.2/css/bootstrap-slider.min.css">
@endsection

@section("content")
<div id="page-content">
    <div id="backdrop" style="margin-top: -130px; width: 100%; z-index: 1; display: flex; align-items: center; min-height: 800px; background-position: center;  background-size: cover; margin-bottom: -64px; background-image:url('/assets/img/navigator-backdrop-t2.png')">
        <div class="gradient-wrap" style="background: transparent; width:100%;">
            <!--  HERO -->
            <div class="section-wrapper black position-relative">
                <div class="row">
                    <div class="col-md-4 col-sm-12 col-12 top-header" style="display: flex; align-items:center;">
                        <div id="hero-text-wrapper">
                          <h1 class="mb-3 section-title" style="color: white; font-weight:600 !important; font-size: 3.5em;">
                            You Are
                          <br />
                          Invited!
                          </h1>
                        <h3 class="section-subtitle mb-4" style="color: white;">
                            To earn from traveling as a<br>Venti Navigator
                        </h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="gradient-wrap bg-white" style="min-height: 650px;">
        <div class="row mt-4" >
            <div class="col-md-12 text-center" style="padding: 4em; ">
            	<div id="example-basic" style="padding-bottom:50px;">
					<h3>Electronic Delivery Consent</h3>
					<section style="min-height:500px; overflow-y:auto;">
				    	<h1 class="mb-3 section-title mt-4" style="font-weight:600 !important; font-size: ;">Before Proceeding</h1>
		                <p class="section-subtitle text-center" style="max-width:800px; margin:0 auto;">
		                    Please acknowledge that the Navigator Agreement is a virtual document. We need your permission to present and collect your digital signature. You must scroll to the bottom.
		                </p>
			            <br>
			            <div style="overflow-y: scroll; min-height: 400px;"> 
			                <p class="text-center">Terms and Conditions for Electronic Document Delivery</p>
							<p class="text-left">The parties to this agreement ("Agreement") are the prospective Navigator ("you") and the owner or manager of Venti Financial, Inc. (collectively "us" or "we"). The parties agree to the following:</p>

							<p class="text-left">
							1. Electronic Signature Agreement. By selecting the "I Accept" button, you are agreeing to sign this Agreement electronically. You agree your electronic signature is the legal equivalent of your manual signature on this Agreement. By selecting "I Accept" you consent to be legally bound by this Agreement's terms and conditions. You further agree that your use of a key pad, mouse or other device to select an item, button, icon or similar act/action, or to otherwise provide us instructions electronically, or making any acceptance of any agreement, acknowledgement, consent terms, disclosures or conditions constitutes your signature (hereinafter referred to as "eSignature"), acceptance and agreement as if actually signed by you in writing. You also agree that no certification authority or other third party verification is necessary to validate your eSignature and that the lack of such certification or third party verification will not in any way affect the enforceability of your eSignature or any resulting contract between you and us. You also represent that you are authorized to enter into this Agreement. You further agree that each use of your eSignature in obtaining a unit from us constitutes your agreement to be bound by the terms and conditions of the Agreement upon which it appears.
							</p>

							<p class="text-left">
							2. Electronic delivery. You specifically agree to receive and/or obtain your Navigator Agreement and any other forms and addenda that relate to the lease agreement (collectively "Agreement Documents") in electronic format. We will electronically deliver your Agreement Documents until either party modifies or cancels this agreement. You will receive your Navigator Agreements electronically in lieu of receiving hard copies through the U.S. Mail or via any other document delivery mechanism.
							</p>

							<p class="text-left">
							3. Notification. Your current valid email address is required in order for you to continue receiving your electronic Agreement Documents. You agree to immediately notify us if you change or delete the email address. You may modify the email address we have on file by emailing us at admin@venti.co. Because email is the agreed upon medium used to deliver your Agreement Documents it is crucial that you give us prompt notice of any changes.
							</p>

							<p class="text-left">
							4. Hardware, software and operating system. There are no special hardware or software requirements for viewing, accessing or retaining the Agreement Documents at this time other than: (i) having a personal computer or other device that is capable of accessing the Internet, (ii) having an Internet browser that is capable of supporting 128-bit SSL encrypted communications. You are responsible for installation, maintenance, and operation of your computer, browser and software. We are not responsible for errors or failures from any malfunction of your computer, browser or software. We are also not responsible for computer viruses or other related problems associated with use of an online system.
							</p>

							<p class="text-left">
							5. Legal effect. Electronic Agreement Documents have the same legal effect as hard copies. You are responsible to obtain access to the Agreement Documents, and to open and read them, if you need to access them for any reason. If you cannot open or access your Agreement Documents you must contact us to resolve this. By clicking the box below you agree that you have a functioning and operating email account to receive emails from us. If needed you must add us as a "safe sender" on your email account.
							</p>

							<p class="text-left">
							6. Amendments. At any time we may make amendments to the terms and conditions of this Agreement. You agree to check for updated versions of this Agreement, and, if you do not agree to these new terms and conditions, terminate this Agreement by any of the methods mentioned above.
							</p>

							<p class="text-left">
							7. Delivery and access errors. Errors in delivering and accessing your electronic Agreement Documents may occur. We are not liable for anything that may arise from problems in accessing or retrieving your Agreement Documents that may occur from problems associated with your telecommunications provider, for your failure to provide updated and current email addresses and contact information, or for any equipment malfunctions that are outside of our control.
							</p>

							<p class="text-left">
							8. Controlling Agreement. This Agreement supplements and modifies other agreements for Electronic Document Delivery that you may have with us. To the extent that this Agreement and another agreement contain conflicting provisions, the provisions in this Agreement will control. All other obligations of the parties remain subject to the terms and conditions of any other agreement. If any provision in this Agreement is found to be unlawful or unenforceable to any extent in a court of competent jurisdiction, then the meaning of said provision shall be construed, to the extent feasible, so as to render the provision enforceable. In the alternative, such term, condition or provision shall be severed from the remaining terms, conditions and provisions, which remaining terms shall continue to be valid and enforceable to the fullest extent permitted by law.
							</p>

							<p class="text-left">
							9. Acceptance. If you accept the terms and conditions in this Agreement you will click the box below. By clicking the box below you acknowledge that you have read and understand this Agreement. This Agreement becomes effective when we receive your acceptance.
							</p>

							<p class="text-center">
								<strong>Agreement to Conduct a Transaction by Electronic Means</strong>
							</p>
							<p class="text-left">
							I AGREE to conduct this transaction by electronic means. I understand that by clicking the box above I am conducting an electronic transaction and agree to use and receive communications through electronic means. I agree to enter into this Navigator Agreement electronically via the use of the Internet, and to be notified regarding this agreement and application electronically through the email address I have provided.
							</p>
						</div>
						<input type="checkbox"  name="electronic" id="electronic" value="1"> Yes, I accept to the Terms and Conditions of Electronic Document Delivery.
					</section>
						<h3>Venti Navigator Agreement</h3>
					<section style="min-height: 800px; overflow-y: auto;">
						<h1 class="mb-3 section-title mt-4" style="font-weight:600 !important; font-size: ;">The Agreement</h1>
                        <p class="section-subtitle text-left" style="max-width:800px; margin:0 auto;">
                            The parties to this agreement ("Agreement") are the prospective Navigator ("you") and the owner or manager of Venti Financial, Inc. (collectively "us" or "we"). The parties agree to the following:
                        </p>

                        @include('navigator.contract')
					</section>
                    <h3>Signature</h3>
                    <section style="min-height: 800px; overflow-y: auto;">
                        <h1 class="mb-3 section-title mt-4" style="font-weight:600 !important; font-size: ;">Digital Signature</h1>
                        <p>The details you provide below must match your Government Issued ID</p>
                        <form method="POST" action="/navigator/agreement" id="applyform" style="min-height: 800px; overflow-y: auto;">
                            @CSRF
                            <div class="row">
                                <div class="form-group text-left col-md-6">
                                    <label class="form-label text-left">Legal First Name</label>
                                    <input type="text" class="form-control" name="f_name" required="" value="{{ Session::get('user')->displayName }}" id="f_name" />
                                </div>
                                <div class="form-group text-left col-md-6">
                                    <label class="form-label text-left">Legal Last Name</label>
                                    <input type="text" class="form-control" name="l_name" required="" value="" id="l_name"/>
                                </div>
                            </div>
                            <div class="form-group text-left">
                                <label class="form-label">Your Address: Please specify country on the last line</label>
                                <textarea type="text" class="form-control" name="address" id="address" required=""></textarea>
                            </div>
                            <div class="row">
                                <div class="form-group text-left col-md-6">
                                    <label class="form-label">Your Preferred Email:</label>
                                    <input type="email" class="form-control" name="email" required="" value="{{ Session::get('user')->email }}" />
                                </div>
                                <div class="form-group text-left col-md-6">
                                    <label class="form-label">Your Phone Number: Please include country code.</label>
                                    <input type="text" class="form-control" name="phone" required="" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group text-left col-md-12">
                                    <label class="form-label"><input type="checkbox" name="agree" required="" id="agree" /> I agree with the terms and conditions outlined in the Venti Navigator Agreement.</label> 
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group text-left col-md-12">
                                    <label>Your Digital Signature: Type Your Full Name that matches your Government ID:</label>
                                    <input type="text" class="form-control" name="signature" required="" id="signature" />
                                </div>
                            </div>
                        </form>
                    </section>
				</div>
            </div>
        </div>
    </div>
  </div>

@endsection

@section("js")
    <script type="text/javascript" src="/assets/js/jquery.steps.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/swiper@8/swiper-bundle.min.js"></script>
    <script>
        $(document).ready(function() {
            $.ajaxSetup({
		         headers: {
		            'X-CSRF-TOKEN': '{{ csrf_token() }}'
		         }
		     });

            toggleLogo("black","white");

            function toggleLogo(color, replaced){
                var logoSRC = $(".logo").attr("src");
                logoSRC = logoSRC.replace(color, replaced);
                console.log(logoSRC)
                $(".logo").attr("src", logoSRC);
            }

            $(".nav-icon").each(function(){
                var src = $(this).attr("src");
                console.log(src)
                src = src.replace('.png', '-white.png');
                $(this).attr("src", src);
              });

            $("#hamburger-menu").click(function(){
                if($("#page_home__show").hasClass("mobile-nav-open")){
                    toggleLogo("white","black");
                } else {
                    toggleLogo("black","white");
                }
                
            });

            $("#example-basic").steps({
			    headerTag: "h3",
			    bodyTag: "section",
			    transitionEffect: "slideLeft",
			    autoFocus: true,
			    onStepChanging: function (event, currentIndex, priorIndex) { 
			    	var electronic = $("#electronic:checked").val();
			    	if(electronic != 1){
		    			swal({
                              icon: 'error',
                              title: 'You missed something',
                              text: 'You must consent to electronic delivery of the Navigator Agreement to continue.',
                            })
		    			return false;
		    		}

			    	console.log(electronic);
			    	return true;
			    	
			    },
			    onFinished: function(event, currentIndex){

                    if($("#f_name").val() != "" && $("#l_name").val() != ""){
                        return $("#applyform").submit();
                    } else {
                        console.log("incomplete");
                        return false;
                    }
			    	return false;
			    }
			});

            @error('success')
		  		swal({
					  icon: 'success',
					  title: 'All Set!',
					  text: 'Your agreement has been digitaly signed. We have sent you a confirmation email',
				})
		  	@enderror
        });
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/11.0.2/bootstrap-slider.min.js"></script>
@endsection