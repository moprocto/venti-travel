@extends('layouts.curator-master')

@section('css')
<style type="text/css">
	.nav-pills{
		display: block;
	}
	.nav-item .nav-link{
		width: 100%;
		text-align: left;
		color: black;
	}
	.tab-pane{
		display: none;
		background: transparent;
	}
	.tab-pane.active{
		display: block;
	}
</style>

@endsection

@section('content')

<div id="page-content">
	<div class="gradient-wrap">
		<div class="black position-relative hero-section mb-4" style="margin: 2em 4em;">
		   	<div class="row">
		   		<div class="col-md-2">
		   			<ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
					  <li class="nav-item" role="presentation">
					    <button class="nav-link active" id="pills-home-tab" data-bs-toggle="pill" data-bs-target="#pills-home" type="button" role="tab" aria-controls="pills-home" aria-selected="true">HOME</button>
					  </li>
					  <li class="nav-item" role="presentation">
					    <button class="nav-link" id="pills-tools-tab" data-bs-toggle="pill" data-bs-target="#pills-tools" type="button" role="tab" aria-controls="pills-tools" aria-selected="false">TOOLS</button>
					  </li>
					  <li class="nav-item" role="presentation">
					    <button class="nav-link" id="pills-community-tab" data-bs-toggle="pill" data-bs-target="#pills-community" type="button" role="tab" aria-controls="pills-community" aria-selected="false">COMMUNITY</button>
					  </li>
					  <li class="nav-item" role="presentation">
					    <button class="nav-link" id="pills-navigator-tab" data-bs-toggle="pill" data-bs-target="#pills-navigator" type="button" role="tab" aria-controls="pills-navigator" aria-selected="false">PRO NAVIGATOR</button>
					  </li>
					  <li class="nav-item" role="presentation">
					    <button class="nav-link" id="pills-settings-tab" data-bs-toggle="pill" data-bs-target="#pills-settings" type="button" role="tab" aria-controls="pills-settings" aria-selected="false">SETTINGS</button>
					  </li>
					</ul>
		   		</div>
		   		<div class="col-md-7">
					<div class="tab-content" id="pills-tabContent">
						  <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
						  	@include('navigator.panels.welcome')
						  </div>
						  <div class="tab-pane fade" id="pills-tools" role="tabpanel" aria-labelledby="pills-profile-tab">
						  	@include('navigator.panels.tools')
						  </div>
						  <div class="tab-pane fade" id="pills-community" role="tabpanel" aria-labelledby="pills-contact-tab">
						  	@include('navigator.panels.community')
						  </div>
						  <div class="tab-pane fade text-center" id="pills-navigator" role="tabpanel" aria-labelledby="pills-profile-tab">
						  	@include('navigator.panels.pro')
						  </div>
						  <div class="tab-pane fade" id="pills-settings" role="tabpanel" aria-labelledby="pills-contact-tab">
						  	@include('navigator.panels.settings')
						  </div>

					</div>
	   			</div>
	   			<div class="col-md-3">
	   				<div class="card card-rounded rounded mb-2">
		   				<div class="card-body text-center" style="width:100%">
		   					<h2>0</h2>
		   					<h4>Points</h4>
		   				</div>
		   			</div>
	   			</div>
	   		</div>
	   	</div>
	</div>
</div>

@endsection

@section('js')

@endsection