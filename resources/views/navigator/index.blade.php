@extends("layouts.curator-master")

@section("css")
	<link rel="stylesheet" type="text/css" href="/assets/css/earn.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/landing.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper@8/swiper-bundle.min.css"/>
    <link rel="stylesheet" type="text/css" href="/assets/css/store.css">
    
    <style type="text/css">
        .bg-white{
            background: white;
        }
        .mob-shadow{
                box-shadow: rgb(100 100 111 / 30%) 0px 7px 29px 0px;
                position: absolute;
                height: 100%;
                width: 386px;
                background: transparent;
                right: 100px;
                bottom: 0;
                border-top-left-radius: 63px;
                border-top-right-radius: 63px;
        }
        @media (max-width: 767px){
            .mob-pt{
                padding-top: 3em;
            }
            .top-header{
                padding-top: 5em;
            }
            .mob-shadow{
                width: 333px;
                background: transparent;
                right: 20px;
            }
            .dark-on-light.nav-links{
                color: white;
            }
            .btn-white{
                color: black !important;
            }
            .swiper-slide {
              width: 100%;
              max-width: 380px;
            }
            .swiper{
                max-width: 380px !important;
            }
        }

        .swiper-wrapper{
                min-height: 500px !important;
            }
        
        #changethewords {
            display: inline !important;
        }
        h4.title{
         display: flex;
         justify-content: space-between;
         align-items: center;
         align-content: center;
      }
      .category{
         text-transform: uppercase;
         background: black;
         color: white;
         font-size: 12px;
         padding: 5px 10px;
         border-radius: 10px;
         margin-top: -3px;
      }
      body{
        background: black;
        background-color: black !important;
      }
      .gradient-wrap {
        margin-bottom: -64px;
      }
    span.fa-bars{
        background:rgba(255,255,255,0.9); 
        padding:8px 10px;
        border-radius:20px
    }
    .swiper-wrapper .grid-item{
        width: 100% !important;
        max-width: 100% !important;
    }
    .pills-tab{
    	border:1px solid black; padding: 5px 10px; border-radius:20px;
    	font-weight: bold;
    }
    .slider.slider-horizontal{
    	width: 87% !important;
    }
    .faq{
        margin-bottom: 20px;
        background-color: #F2F2F6;
        padding:20px;
        border-radius: 20px;
        padding-bottom:0;
        height: fit-content;
    }



    .swiper-slide {
      text-align: center;
      font-size: 18px;
      background: #fff;
      display: flex;
      justify-content: center;
      align-items: center;
      flex-direction: column;
      background: none;

    }

    .swiper-slide img {
      
    }


    
    </style>
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/11.0.2/css/bootstrap-slider.min.css">
@endsection

@section("content")
<div id="page-content">
    <div id="backdrop" style="margin-top: -130px; width: 100%; z-index: 1; display: flex; align-items: center; min-height: 800px; background-position: center;  background-size: cover; margin-bottom: -64px; background-image:url('/assets/img/navigator-backdrop.jpg')">
        <div class="gradient-wrap" style="background: transparent; width:100%;">
            <!--  HERO -->
            <div class="section-wrapper black position-relative">
                <div class="row">
                    <div class="col-md-4 col-sm-12 col-12 top-header" style="display: flex; align-items:center;">
                        <div id="hero-text-wrapper">
                          <h1 class="mb-3 section-title" style="color: white; font-weight:600 !important; font-size: 3.5em;">
                            Get Paid to
                          <br />
                          Travel
                          </h1>
                        <h3 class="section-subtitle mb-4" style="color: white;">
                            Earn up to $500+ Per Trip as a Navigator
                        </h3>
                          <a href="#apply" style="width:100px;" class="btn btn-white">Apply</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="gradient-wrap">
        <!--  HERO -->
        <div class="section-wrapper black position-relative mb-4" style="padding: 6em 0;">     
            <div class="row" style="min-height: 400px;">
                <div class="col-md-6" style="align-self: center;">
                    <img src="/assets/img/navigator-t1.jpg" style="width:100%;">
                </div>
                <div class="col-md-6 text-left mob-pt" style="display: grid; align-content: center;">
                    <h1 class="mb-3 section-title" style="font-weight:600 !important; font-size: ;">About our Program</h1>
                    <h3 class="section-subtitle mb-4" >
                         Venti is a social platform for travel and adventure. Anyone can join and create or join informal trips. If you love to travel and wish to share your passion while making friends, becoming a Navigator might be perfect for you!
                    </h3>
                    <ul>
                    	<li>Navigators post "organized" trips onto the Venti platform and serve as an on-ground tour guide. You get to be as creative and original as you want!</li>
                    	<li>Travel whenever and wherever you want, almost for free, depending on your planning skills and schedule.</li>
                    	<li>We handle fee collection and accommodation booking so the group can focus on having fun. We work with you to determine the appropriate fee for each traveler.</li>
                    	<li>You don't need to be a history buff or a perfect planner.</li>
                    	<li>Gain access to AI-powered tools to create unique trips and find travel companions.</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="gradient-wrap bg-white" style="height: fit-content;
    margin-bottom: -25px;
    padding-bottom: 10px;">
        <div class="row mt-4" >
            <div class="col-md-12 text-center" style="padding: 4em; ">
                <h1 class="mb-3 section-title mt-4" style="font-weight:600 !important; font-size: ;">How Does it Work?</h1>
                <h3 class="section-subtitle text-center" style="max-width:700px; margin:0 auto;">
                    Venti is a social-first travel platform. We make it easy for organizers and travel seekers to connect and explore together. The below outlines how you'll operate as a Venti Navigator.
                </h3>
                <br>
            </div>
        </div>
        <div class="row" style="max-width: 1100px; margin:0 auto; height:fit-content;">
            <div class="col-md-3 text-center">
                <i class="fa-solid fa-leaf" style="font-size:36px; margin-bottom:10px;"></i>
                <h4>Ideate</h4>
                <p>Design a trip outline and post it to the Venti platform. At this stage, you should leave some room for flexibility. Aim for ideas you can't easily find elsewhere.</p>
            </div>
            <div class="col-md-3 text-center">
                <i class="fa-solid fa-handshake-angle" style="font-size:36px; margin-bottom:10px;"></i>
                <h4>Connect</h4>
                <p>Travelers on our platform will request to join your trip. You can accept/reject who can join based on criteria you define to maximixe group chemistry.</p>
            </div>
            <div class="col-md-3 text-center">
                <i class="fa-solid fa-earth-africa" style="font-size:36px; margin-bottom:10px;"></i>
                <h4>Customize</h4>
                <p>Utilize our chat and video calling features to discuss the trip with everyone. During this phase, you can modify the trip plan based on ideas and suggestions.</p>
            </div>
            <div class="col-md-3 text-center">
                <i class="fa-solid fa-person-hiking" style="font-size:36px; margin-bottom:10px;"></i>
                <h4>Finalize</h4>
                <p>We assign a cut-off date to calculate traveler fees and issue invoices. Only those that pay their deposit can continue with the group and travel with you.</p>
            </div>
            <div class="col-md-12 mt-4 text-center">
                <p>We'll be honest and say we're not looking for a "Spring Break Bash in Cancun" type of trip for our program.</p>
            </div>
        </div>
    </div>
    <div class="gradient-wrap">
        <div class="row mt-4" style="min-height: 350px;">
            <div class="col-md-12 text-center" style="padding: 4em; ">
                <h1 class="mb-3 section-title mt-4" style="font-weight:600 !important; font-size: ;">Calculate Your Earnings</h1>
                <h3 class="section-subtitle text-center" style="max-width:800px; margin:0 auto;">
                    Earn 10 or more Navigator Points per person per night of the trip. We work with you to determine how many points you can earn for each trip based on location, trip length, and complexity. Earn cash tips when trip mates leave a review.
                </h3>
                <br>
                <div style="margin:0 auto; display:flex;justify-content: center;">
                	@include("earn.calculator")
            	</div>
                <br><br>

            </div>
            <div class="col-md-12 mt-4">
                <br><br>
                <a href="#apply" style="width:150px;" class="btn btn-success">APPLY TODAY</a>
            </div>
        </div>
    </div>
    <div class="gradient-wrap bg-white">
        <!--  HERO -->
        <div class="section-wrapper black position-relative mb-4" style="padding: 6em 0;">     
            <div class="row" style="min-height: 400px;">
            	<div class="col-md-6 text-left mob-pt" style="display: grid; align-content: center;">
                    <h1 class="mb-3 section-title" style="font-weight:600 !important; font-size: ;">Fine Print</h1>
                    <h3 class="section-subtitle mb-4" >
                         Our Navigator Program is not for everyone. We're looking for passionate travelers that are organized and want to use travel as way to make meaningful connections. Here are some baseline limitations of our program:
                    </h3>
                    <ul>
                    	<li><strong>You cannot directly charge a fee or collect funds for any trip.</strong> All fee collection takes place on the Venti platform. Fees have to be approved by the Venti team.</li>
                    	<li>You'll be interviewed by our team and have to undergo advanced verification. There is no application fee.</li>
                    	<li>You must be fluent in English. Most travelers on our platform are from the U.S., Canada, and Europe.</li>
                    	<li>Points are awarded after your trip is completed. Venti collects 50% of all tips to cover marketing costs related to your trips.</li>
                    </ul>
                </div>
                <div class="col-md-6" style="align-self: center;">
                    <img src="/assets/img/navigator-t2.jpg" style="width:100%;">
                </div>
                
            </div>
        </div>
    </div>
    <div class="gradient-wrap">
        <div class="row mt-4" style="min-height: 350px;">
            <div class="col-md-12 text-center" style="padding: 4em; ">
                <h1 class="mb-3 section-title mt-4" style="font-weight:600 !important; font-size: ;">Climb the Leaderboards</h1>
                <h3 class="section-subtitle text-center" style="max-width:800px; margin:0 auto;">
                    After each trip, you can select to receive a cashback OR redeem points for high-quality travel products, free trips, free flights, and other perks. As you accumulate points, you can ascend the ranks and unlock additional perks for you and for your family and friends.
                </h3>
                <br>
                <span class="pills-tab" style="background-color:white; ">Navigator</span> → <span class="pills-tab">Pilot</span> → <span class="pills-tab" style="background-color:silver">Lead</span> → <span class="pills-tab text-light" style="background-color:#555">Captain</span> → <span class="pills-tab text-light" style="background-color:#2e2e2e;">Chief</span> → <span class="pills-tab text-light" style="background-color:black">Hero</span>
                <br><br>
            </div>
        </div>
    </div>
    <div class="gradient-wrap bg-white" id="pro">
        <div class="row mt-4" style="min-height: 350px;">
            <div class="col-md-12 text-center" style="padding: 4em; ">
                <h1 class="mb-3 section-title mt-4" style="font-weight:600 !important; font-size: ;">Become a <span style="text-decoration: underline;">Pro</span> Navigator (Optional)</h1>
                <h3 class="section-subtitle text-center" style="max-width:800px; margin: auto;">
                    Supercharge your earning potential with our exclusive AI-powered travel dashboard that provides you with everything you need to design the best, in-demand trips. You can pay for your subscription using just your Navigator Points.</strong> 
                </h3>
                <br>
                <h2 class="section-subtitle text-center">Access to our <em>optional</em> Pro Navigator dashboard is <strong>$9.99</strong> <strike>$19.99</strike> per month.</h2>
                <br>
                <img src="/assets/img/dashboard.png" style="width:100%; max-width:700px;">
            </div>
        </div>
    </div>
    <div class="gradient-wrap">
        <div class="section-wrapper black position-relative mb-4" style="padding: 6em 0;">     
            <div class="row" style="min-height: 400px;">
                <div class="col-md-12">
                <h1 class="mb-3 section-title mt-4 text-center" id="apply" style="font-weight:600 !important; width: 100%; margin-bottom:20px">Meet Our Navigators</h1>
                <h4 class="section-subtitle text-center" style="display: block; max-width: 800px; margin:0 auto; margin-bottom:50px;">Our team has reviewed over 1,100 applications, and we could not be more excited to share our first group of Navigators. These inspiring individuals were interviewed by our team, and cannot wait to share their passion for travel and culture with others.<br>Will you be next?</h4>
                </div>
                <div class="swiper" style="max-width:900px;">
                  <!-- Additional required wrapper -->
                  <div class="swiper-wrapper">
                        <div class="swiper-slide text-center">
                            <img src="https://firebasestorage.googleapis.com/v0/b/venti-344622.appspot.com/o/vigner.jpg?alt=media&token=4ed0bba0-634f-460e-88a4-b14dd4e988bf" style="width:100%; max-width: 240px; border-radius: 20px;">
                                <br>
                                <h4 class="mt-4">Vigner <img src="/assets/img/navigator-verified.png" style="width:15px; margin-top: -5px;"></h4>
                                <p>Specializes in multi-day lodging and camping treks in-and-around Cusco, Peru</p>

                        </div>
                        <div class="swiper-slide text-center">
                            <img src="https://storage.googleapis.com/download/storage/v1/b/venti-344622.appspot.com/o/9002351-80379698_10215960893152006_6687132817081499648_n.jpeg?generation=1680041768893431&alt=media" style="width:100%; max-width: 240px; border-radius: 20px;">
                                <br>
                                <h4 class="mt-4">Cara <img src="/assets/img/navigator-verified.png" style="width:15px; margin-top: -5px;"></h4>
                                <p>Expert traveler with 84 countries visited and is eager to design your next trip</p>

                        </div>
                        <div class="swiper-slide text-center">
                            <img src="https://storage.googleapis.com/download/storage/v1/b/venti-344622.appspot.com/o/1220788-Foto-mia.jpg?generation=1680193499875429&alt=media" style="width:100%; max-width: 240px; border-radius: 20px;">
                                <br>
                                <h4 class="mt-4">Walter <img src="/assets/img/navigator-verified.png" style="width:15px; margin-top: -5px;"></h4>
                                <p>Over 20 years of experience designing culinary tours in South America</p>

                        </div>
                        <div class="swiper-slide text-center">
                            <img src="https://storage.googleapis.com/download/storage/v1/b/venti-344622.appspot.com/o/8310712-20220215_105640.jpg?generation=1679506838399266&alt=media" style="width:100%; max-width: 240px; border-radius: 20px;">
                                <br>
                                <h4 class="mt-4">Dru <img src="/assets/img/navigator-verified.png" style="width:15px; margin-top: -5px;"></h4>
                                <p>Your go-to guide for unique experiences in California, Italy, and Spain</p>

                        </div>
                        <div class="swiper-slide text-center">
                            <img src="https://storage.googleapis.com/download/storage/v1/b/venti-344622.appspot.com/o/1872586-B5D44461-BBF4-4CDB-ACC7-4F746CBA39B1.jpeg?generation=1680188002585023&alt=media" style="width:100%; max-width: 240px; border-radius: 20px;">
                                <br>
                                <h4 class="mt-4">Mercy <img src="/assets/img/navigator-verified.png" style="width:15px; margin-top: -5px;"></h4>
                                <p>Creates luxurious and tasteful expeditions around South Africa and beyond</p>

                        </div>
                        <div class="swiper-slide text-center">
                            <img src="https://storage.googleapis.com/download/storage/v1/b/venti-344622.appspot.com/o/648392-FE66C687-F806-4CE6-8342-447C8864EC34.jpeg?generation=1680655819829413&alt=media" style="width:100%; max-width: 240px; border-radius: 20px;">
                            <br>
                            <h4 class="mt-4">Dr. Nabil <img src="/assets/img/navigator-verified.png" style="width:15px; margin-top: -5px;"></h4>
                            <p>50+ countries visited with expertise in humanitarian-first travel</p>
                        </div>
                        <div class="swiper-slide text-center">
                            <img src="https://storage.googleapis.com/download/storage/v1/b/venti-344622.appspot.com/o/7908555-1664468843390.jpg?generation=1680910113093725&alt=media" style="width:100%; max-width: 240px; border-radius: 20px;">
                                <br>
                                <h4 class="mt-4">Leya <img src="/assets/img/navigator-verified.png" style="width:15px; margin-top: -5px;"></h4>
                                <p>Ready to share guided adventures in San Francisco and Northern California</p>
                        </div>
                        <div class="swiper-slide text-center">
                            <img src="https://storage.googleapis.com/download/storage/v1/b/venti-344622.appspot.com/o/6309199-IMG-20221223-WA0012.jpg?generation=1680913591586209&alt=media" style="width:100%; max-width: 240px; border-radius: 20px;">
                            <br>
                            <h4 class="mt-4">Suzan <img src="/assets/img/navigator-verified.png" style="width:15px; margin-top: -5px;"></h4>
                            <p>Helping women navigate adventure with nature and culture in Indonesia</p>
                        </div>
                        <div class="swiper-slide text-center">
                            <img src="https://storage.googleapis.com/download/storage/v1/b/venti-344622.appspot.com/o/5542692-photo_2023-04-03_14-13-58.jpg?generation=1680542102405751&alt=media" style="width:100%; max-width: 240px; border-radius: 20px;">
                                <br>
                                <h4 class="mt-4">Dimi <img src="/assets/img/navigator-verified.png" style="width:15px; margin-top: -5px;"></h4>
                                <p>Wants to show you the best hikes and views of southern Brazil and West Canada</p>

                        </div>
                        <div class="swiper-slide text-center">
                            <img src="https://storage.googleapis.com/download/storage/v1/b/venti-344622.appspot.com/o/3868457-F48C8F78-9A74-428D-B353-2CCBC934F1BA.JPG?generation=1681032649354543&alt=media" style="width:100%; max-width: 240px; border-radius: 20px;">
                            <br>
                            <h4 class="mt-4">Yasmine <img src="/assets/img/navigator-verified.png" style="width:15px; margin-top: -5px;"></h4>
                            <p>Ready to show you the best of Egypt's history and culture from temples to beaches</p>
                        </div>
                        <div class="swiper-slide text-center">
                            <img src="https://firebasestorage.googleapis.com/v0/b/venti-344622.appspot.com/o/20221028_180037706_iOS.jpg?alt=media&token=3ab8f872-9546-4192-a6e0-955d14a2df56" style="width:100%; max-width: 240px; border-radius: 20px;">
                            <br>
                            <h4 class="mt-4">Andrzej <img src="/assets/img/navigator-verified.png" style="width:15px; margin-top: -5px;"></h4>
                            <p>Culture and nature enthusiast with expertise in Europe, Brazil, and beyond</p>
                        </div>
                        <div class="swiper-slide text-center">
                            <img src="https://firebasestorage.googleapis.com/v0/b/venti-344622.appspot.com/o/abdul.jpg?alt=media&token=eca4c780-c25c-4c18-97d1-f59d197b0474" style="width:100%; max-width: 240px; border-radius: 20px;">
                            <br>
                            <h4 class="mt-4">Abdul <img src="/assets/img/navigator-verified.png" style="width:15px; margin-top: -5px;"></h4>
                            <p>Student in University and student of the world with expertise in central and southern Europe</p>
                        </div>
                  </div>
                  <!-- If we need pagination -->
                  <div class="swiper-pagination"></div>

                  <!-- If we need scrollbar -->
                  <div class="swiper-scrollbar"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="gradient-wrap bg-white">
        <div class="section-wrapper black position-relative mb-4" style="padding: 6em 0;">     
            <div class="row" style="min-height: 400px;">
            	<div class="col-md-12">
            	<h1 class="mb-3 section-title mt-4 text-center" id="apply" style="font-weight:600 !important; width: 100%; margin-bottom:20px">Submit Application</h1>
            	<h4 class="section-subtitle text-center" style="display: block; max-width: 800px; margin:0 auto; margin-bottom:50px;">We're moving fast and want to start empowering passionate travelers as soon as possible. Becoming a Navigator early means you get first dibs on popular destinations!</h4>
            	</div>
            	<div class="col-md-12">
            		@include('forms.apply')
            	</div>
            </div>
        </div>
    </div>
    <div class="gradient-wrap">
        <div class="section-wrapper black position-relative mb-4" style="padding: 6em 0;">     
            <div class="row" style="min-height: 400px;">
                <div class="col-md-12">
                <h1 class="mb-3 section-title mt-4 text-center" id="apply" style="font-weight:600 !important; width: 100%; margin-bottom:20px">FAQs</h1>
                <h4 class="section-subtitle text-center" style="display: block; max-width: 800px; margin:0 auto; margin-bottom:50px;">We've already interviewed so many applicants for our Navigator program. Here are the answers to some questions that we think will help you:</h4>
                </div>
                <div class="col-md-12 faq">
                    <h5>Am I responsible for making sure visas, tourism status, and other logistics are confirmed for each traveler?</h5>
                    <p>We politely request that you direct all questions and concerns from potential travelers regarding documentation, customs, vaccination, visa requirements, and other state or tourism department related matters to the Venti team. We do not expect you to be an expert in such matters.</p>
                </div>
                <div class="col-md-12 faq">
                    <h5>Will my trips be insured?</h5>
                    <p>We include travel insurance in the fee assessed for each trip. Trip fees also cover your travel insurance. By default, you are not liable if someone on your trip sustains any sickness, monetary, physical, or emotional damages <strong>not caused by you</strong>. Our Navigator Contract will provide more details.</p>
                </div>
                <div class="col-md-12 faq">
                    <h5>Does becoming a Navigator mean I work for Venti?</h5>
                    <p>No, Navigators are not employees of Venti. We are simply a technology platform that makes it easier for you to render travel-related services.</p>
                </div>
                <div class="col-md-12 faq">
                    <h5>Do I pay for my own lodging, food, etc.?</h5>
                    <p>We work with you to determine the fee that each traveler will have to pay to join your trip. We <strong>can</strong> increase that fee to cover more of your expenses. However, we caution pushing all your expenses onto travelers as that will significantly lower interest and overall participation. Our unique points system allows you to be reimbursed.</p>
                </div>
                <div class="col-md-12 faq">
                    <h5>Can I have a friend, partner, coworker help me lead a trip?</h5>
                    <p>You are more than welcome to invite anyone that you believe will be beneficial to the trip. For safety reasons, you are not allowed to outsource the tour guiding responsibility to anyone else that has not been vetted by the Venti team. </p>
                </div>
            </div>
        </div>
    </div>
  </div>

@endsection

@section("js")
    @if(env('APP_ENV') != "local")
        <!-- Meta Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s)
            {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};
            if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
            n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t,s)}(window, document,'script',
            'https://connect.facebook.net/en_US/fbevents.js');
            fbq('init', '1169331916805166');
            fbq('track', 'PageView');
        </script>
        <noscript><img height="1" width="1" style="display:none"
        src="https://www.facebook.com/tr?id=1169331916805166&ev=PageView&noscript=1"
        /></noscript>
        <!-- End Meta Pixel Code -->
    @endif
    <script type="text/javascript" src="/assets/js/changethewords.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/swiper@8/swiper-bundle.min.js"></script>
    <script>
        $(function() {
          $("#changethewords").changeWords({
            time: 2000,
            animate: "flipInX",
            selector: "span",
            repeat:true
          });
        });
        

        $(document).ready(function() {
            $.ajaxSetup({
         headers: {
            'X-CSRF-TOKEN': '{{ csrf_token() }}'
         }
      });

            toggleLogo("black","white");

            function toggleLogo(color, replaced){
                var logoSRC = $(".logo").attr("src");
                logoSRC = logoSRC.replace(color, replaced);
                console.log(logoSRC)
                $(".logo").attr("src", logoSRC);
            }

            $(".nav-icon").each(function(){
                var src = $(this).attr("src");
                console.log(src)
                src = src.replace('.png', '-white.png');
                $(this).attr("src", src);
              });

            $("#hamburger-menu").click(function(){
                if($("#page_home__show").hasClass("mobile-nav-open")){
                    toggleLogo("white","black");
                } else {
                    toggleLogo("black","white");
                }
                
            });

            var width = $(window).width();
            var numSlides = 3;
            if(width < 600){
                numSlides = 1;
            }

            const swiper = new Swiper('.swiper', {
                  // Optional parameters
                  direction: 'horizontal',
                  loop: false,
                  spaceBetween: 30,
                  slidesPerView: numSlides,

                  // If we need pagination
                  pagination: {
                    el: '.swiper-pagination',
                  },

                  // Navigation arrows
                  navigation: {
                    nextEl: '.swiper-button-next',
                    prevEl: '.swiper-button-prev',
                  },

                  
            });


            @error('success')
		  		swal({
					  icon: 'success',
					  title: 'All Set!',
					  text: 'Your application has been submitted. We have sent you a confirmation email',
					})
		  	@enderror

            @error('incorrectAnswer')
                    swal({
                      icon: 'error',
                      title: 'Uh oh!',
                      text: 'Your answer to our match question was incorrect. Please scroll downto retry.',
                    })
            @enderror
        });
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/11.0.2/bootstrap-slider.min.js"></script>
    <script src="/assets/js/earn.js"></script>
@endsection