@extends('layouts.curator-master')

@php
    $selectable = false;
@endphp

@section('css')
    <style type="text/css">
        html{
            overflow-x: hidden;
        }
        @media(max-width: 1368px ){
            .tb-full-width{
                width: 100%;
            }
        }
        .hamburger-menu-list li a{
            color: white;
        }
        .mobile-nav-open .hamburger-menu-list li a{
            color: black;
        }
        .btn-white{
            color: black !important;
        }
        .mobile-nav-open #backdrop{
            margin-top: 0 !important;
        }
        .about-body{
            justify-content: center;
        }
        @media(min-width: 1367px){
            {
                margin-left: 22px;
            }
            .row.about-body{
                margin-bottom: 22px;
            }
            .desktop-hide{
                display: none;
            }
        }
        @media(max-width: 800px){
            .mb-pd-20{
                padding-bottom: 20px;
            }
        }
        @media(max-width: 1368px){
            .mobile-block{
                display: block !important;
            }
            .mobile-hide{
                display: none;
            }
            #about .about-body .about-body-item .about-body-icon-cnt{
                justify-content: left !important;
                margin-bottom: 10px;
            }
            .pills-item{
                background-position-y: inherit !important;
            }
            .sign-up-box{
                padding-top: 80px;
                padding-bottom:  0;
            }
        }
        .about-body-icon-cnt{
            border: 1px solid black;
            border-radius: 20px;
            padding: 5px 10px;
        }
        

        .font-light{
            font-weight: 300 !important;
        }
        .full-width{
            max-width: 9999px !important;
            padding-left:  0;
            padding-right: 0;
        }
        #about{
            padding-top: 0;
        }
        .pd-20{
            padding: 80px 40px;
        }
        i.text-small{
            font-size: 16px;
        }
        .about-body-icon-cnt, .about-body-text-head{
            padding-left: 14px;
        }
        .about-body-text-head{
            padding-right: 15px;
            font-weight: bold;
        }
        @media(min-width: 1367px){
            {
                margin-left: 22px;
            }
            .row.about-body{
                margin-bottom: 22px;
            }
            .desktop-hide{
                display: none;
            }
        }
        @media(max-width: 768px){
            .mobile-hide{
                display: none;
            }


        }
        @media(max-width: 468px){
            #minutes, #seconds{
                display: none;
            }
        }   
        @media(max-width: 1368px){
            .mh-500-sm{
                max-height: 500px;
            }
            .mobile-block{
                display: block !important;
            }
            .mobile-hide{
                display: none;
            }
            #about .about-body .about-body-item .about-body-icon-cnt{
                justify-content: left !important;
                margin-bottom: 10px;
            }
            .pills-item{
                background-position-y: inherit !important;
            }
            .sign-up-box{
                padding-top: 80px;
                padding-bottom:  0;
            }
            .preview-mh{
                width: 143% !important;
                margin-left: -73px;
            }
        }

        .text-gold{
            color: #9D8A7C !important;
        }

        .text-large{
            font-size: 20px;
        }

        ol.pills-list{
            display: block;
            padding: 0 0 0 26px;
            list-style: none;
            overflow: hidden;
        }

        .pills-list li{
            display: flex;
            justify-content: space-between;
            align-items: center;
            margin-bottom: 10px;
            padding: 10px 20px 5px 20px;
        }
        .sand-badge{
            background-color: rgba(157, 138, 124, 0.7);
            opacity: 0.8;
            color: white;
            font-size: 22px;
            border-radius: 50%;
            width: 42px;
            height: 40px;
            letter-spacing: 1;
        }
        .pills-item{
            padding: 7px 20px;
            border-radius: 10px;
            margin-bottom: 20px;
            background-color: whitesmoke;
        }
        .pills-item div{
            padding-top: 10px;
        }
        .pills-item p{
            padding-top: 3px;
        }
        .pills-item-1{
            background-position-y: 0;
        }

        .pills-item-2{
            background-position-y: -90px;
        }
        .pills-item-3{
            background-position-y: -180px;
        }
        .pills-item-4{
            background-position-y: -220px;
        }
        li.pills-item-3{
            background: #d7d0ca;
        }
        #mc_embed_signup .text-small{
            font-size: 12px !important;
        }

        #subscribe .text-small{
            margin-top: 15px;
            
        }
        .about-body-item{
            margin-bottom: 10px;
        }.slider-horizontal{
            width: 100% !important;
        }
        #faqs
        {
        padding-bottom: 100px;
        }
        #faqs .faqs-container .faq
        {
            border-top: 1px solid var(--color-border);
            padding: 15px;
        }
        #faqs .faqs-container .faq .faq-head
        {
            padding: 15px 0px;
            position: relative;
        }
        #faqs .faqs-container .faq .faq-head h5
        {
            width: 90%;
            cursor: pointer;
        }
        #faqs .faqs-container .faq .faq-head h5::before
        {
            content: 'keyboard_arrow_down';
            text-align: center;
            font-size: 20px;
            line-height: 27px;
            font-family: 'Material Icons Round';
            height: 25px;
            width: 25px;
            color: #fefefe;
            background: var(--color-sec);
            position: absolute;
            top: 50%;
            right: 0;
            transform: translateY(-50%);
            border-radius: 50%;
            transition: 0.5s;
        }
        #faqs .faqs-container .faq .faq-head h5[aria-expanded=true]::before
        {
            content: 'keyboard_arrow_up';
        }
        .form-container .emailoctopus-form-wrapper .emailoctopus-success-message{
            color:  orange !important;
            font-size: 18px;
            padding-bottom: 20px;
        }
        .accordion-item{
            margin-bottom: 10px;
        }
        #countdown{
            position: fixed;
            z-index: 999;
            height: 50px;
            background: white;
            width: 100%;
            bottom: 0;
            left: 0;
            display: flex;
            flex-direction: row;
            justify-content: center;
            padding: 10px 20px;
            font-weight: bold;
            font-size: 0.9em;
            align-items: center;
        }
        #countdown div.descrtiption{
            text-align: center;
            font-size: 0.8em;
            padding: 10px;
        }
        #timer{
            font-weight: bold;
            display: flex;
            justify-content: space-between;
        }
        #timer *{
            padding-right: 4px;
        }

        .about-body-icon-cnt{
            border: 1px solid rgba(0,0,0,0.1);
        }
        .footnote{
            width: 30px;
            display: inline-block;
        }
        html body{
            background: #F2F2F6 !important;
        }
        .features ul {
            list-style: none;
            padding: 0;
        }
        .features ul li{
            padding: 10px;
            border-bottom: 1px solid whitesmoke;
            font-size: 13px;
        }
        .text-bold{
            font-weight: bold;
        }
    </style>
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/11.0.2/css/bootstrap-slider.min.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons+Round" rel="stylesheet">
    <!-- Main CSS -->
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>

@endsection

@php
    $vips = env('VIPS');
    $vip = number_format($vips, 0);
    $counter = $vips * 20;
@endphp

@section('content')
<div id="backdrop" style="margin-top: -130px; width: 100%; z-index: 1; display: flex; align-items: center; min-height: 800px; background-position: center;  background-size: cover; margin-bottom: -64px; background-image:url('/assets/img/navigator-backdrop-t9.jpg')">
        <div class="gradient-wrap" style="background: transparent; width:100%;">
            <!--  HERO -->
            <div class="section-wrapper black position-relative">
                <div class="row" style="align-items:center;">
                    <div class="col-lg-5 col-md-12 col-sm-12 col-12 top-header" style="display: flex; align-items:center;">
                        <div id="hero-text-wrapper" style="max-width:900px;">
                            <h1 class="mb-3 section-title" style="line-height: 4rem; color: white; font-weight:600 !important; font-size: 3.8rem;">
                            We Help Gen Z Build Credit
                            <span style="font-size: 0.45em; display:block;">While They Travel</span>
                            </h1>
                            <h2 class="section-subtitle mb-4" style="color: white; max-width:900px;">
                            Our low-interest, secured line of credit up to $100,000 for all your upcoming adventures.</h2>
                         <p class="section-head-body" style="color:white;">We're launching Jet Bridge in 2024. No minimum credit score. APR may range from 0% APR to 15% depending on credit history and borrowing terms.<sup>*</sup></p>
                         <a href="#waitlist" class="btn btn-primary">Join VIP List</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<!-- Cover End -->
 <!-- About Start -->

 <section id="" style="padding-top: 100px; padding-bottom: 50px;">
        <div class="container full-width">
            <div class="row">
                <div class="col-md-12  text-center pd-20 text-center" style="padding-bottom: 0; margin-bottom: -3px; padding:40px 20px;" >
                    <h2 class="section-head-title text-dark mb-3 section-title" style="margin:40px; margin-top:20px">First, What is a Line of Credit?</h2>
                    <h3 class="section-head-body" style="font-size:1.25rem; max-width:800px; margin: 0 auto; margin-bottom:40px;">A flexible, on-demand loan that allows you to cover unforseen expenses and make purchases. Unlike a traditional loan, any amount you repay can be re-used without having to formally apply for another loan, which means no additional credit inquries. <br><br><strong>With Jet Bridge, you can place ALL your trips on a low-interest, monthly payment plan.</strong></h3>
                </div>
            </div>
        </div>
    </section>


    <section id="about" style="padding-bottom: 0;">
        <div class="container full-width">
            <div class="row" style="background-color: #f7f9ff;">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 text-center pd-20" style="padding-bottom: 0; margin-bottom: -3px; padding:40px;" >
                    <h2 class="section-head-title text-dark mb-3 section-title" style="margin:40px; margin-top:20px">How it Works</h2>
                    <div class="row text-dark text-left pills-item pills-item-1">
                        <div class="col-md-1 text-center">
                            <span class="badge sand-badge">1</span>
                        </div>
                        <div class="col-md-9">
                            <p>You complete an online application for pre-approval. Within two minutes, you'll be presented with a credit line <strong>up to $100,000</strong>. At this step, only a soft pull is conducted, which has no impact on your credit score.<p>
                        </div>
                        <div class="col-md-2 text-center mb-pd-20" style="display: flex; align-items: center; justify-content:center;">
                            <img src="/assets/img/virtual-wallet.png" style="width:50px;">
                        </div>
                    </div>
                    <div class="row text-dark text-left pills-item pills-item-2">
                        <div class="col-md-1 text-center">
                            <span class="badge sand-badge">2</span>
                        </div>
                        <div class="col-md-9">
                            <p>Your offer will detail the total size of your credit line and how much cash you must collateralize with Venti. You may be asked to provide proof of income, bank statements, prior travel history, and other documents to support your application.</p>
                        </div>
                        <div class="col-md-2 text-center mb-pd-20" style="display: flex; align-items: center; justify-content:center;">
                            <img src="/assets/img/earn-rewards.png" style="width:50px;">
                        </div>
                    </div>
                    <div class="row text-dark text-left pills-item pills-item-3">
                        <div class="col-md-1 text-center">
                            <span class="badge sand-badge">3</span>
                        </div>
                        <div class="col-md-9">
                            <p class="">Once you accept your credit line offer, funds will be deposited into a "travel savings account." You are invoiced each month based on the amount spent from your credit line plus interest ranging from 5% APR to 15% APR<sup>*</sup>.</p>
                        </div>
                        <div class="col-md-2 text-center mb-pd-20" style="display: flex; align-items: center; justify-content:center;">
                            <img src="/assets/img/tourism.png" style="width:50px;">
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 full-width">
                    <img src="/assets/img/navigator-backdrop-t8.jpg" style="width: 100%;" class="">
                </div>
            </div>
            <div class="row" style="background-color:white;">
                <div class="col-md-12 text-center" style="padding:90px 20px; padding-bottom:0;">
                    <h3></h3>
                    <h2 class="section-head-title text-dark mb-3 section-title" style="margin:40px; margin-top:20px">The Best Part</h2>
                    <img src="/assets/img/score-improver.jpg" style="width:300px; margin:30px auto;" class="d-block">
                    <p style="max-width:800px; margin:0 auto; font-size:1.2em; margin-bottom:50px">Venti can help you build credit while you travel as we mainly report positive activities to the major credit bureaus. There are no late fees if overdue payments are paid back within 60 days. We do not report delinquencies to the credit bureau if overdue payments are resolved within 90 days.</p>
                    <br>
                    <br>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 full-width mobile-hide">
                    <img src="/assets/img/family-bg.jpg" style="width: 100%; height:100%;">
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 text-center" style="padding-bottom: 0; margin-bottom: -3px; padding:40px;" >
                    <h2 class="section-head-title text-dark mb-3 section-title" style="margin:40px; margin-top:20px">Travel Without Limits</h2>
                    <h5 style="font-weight: 800; font-size: 16px; text-transform:uppercase; margin-bottom:20px">Save Big When You Book for the Entire Family On Venti</h5>
                    <div class="row about-body">
                        <div class="about-body-item d-flex mobile-block align-items-center">
                            <div class="tb-full-width">
                                <div class="about-body-icon-cnt">
                                    <i class="material-icons-round about-body-icon fas fa-plane text-small"></i>
                                    <span class="about-body-text-head">AIRFARE</span>
                                </div>
                            </div>
                        </div>
                        <div class="about-body-item d-flex mobile-block align-items-center">
                            <div class="tb-full-width">
                                <div class="about-body-icon-cnt">
                                    <i class="material-icons-round about-body-icon fas fa-ship text-small"></i>
                                    <span class="about-body-text-head">CRUISES</span>
                                </div>
                            </div>
                        </div>
                        <div class="about-body-item d-flex mobile-block align-items-center">
                            <div class="tb-full-width">
                                <div class="about-body-icon-cnt">
                                    <i class="material-icons-round about-body-icon fas fa-hotel text-small"></i>
                                    <span class="about-body-text-head">LODGING</span>
                                </div>
                            </div>
                        </div>
                        <div class="about-body-item d-flex mobile-block align-items-center">
                            <div class="tb-full-width">
                                <div class="about-body-icon-cnt">
                                    <i class="material-icons-round about-body-icon fas fa-car text-small"></i>
                                    <span class="about-body-text-head">RENTALS</span>
                                </div>
                            </div>
                        </div>
                        <div class="about-body-item d-flex mobile-block align-items-center">
                            <div class="tb-full-width">
                                <div class="about-body-icon-cnt">
                                    <i class="material-icons-round about-body-icon fas fa-binoculars text-small"></i>
                                    <span class="about-body-text-head">TOURS</span>
                                </div>
                            </div>
                        </div>
                        <div class="about-body-item d-flex mobile-block align-items-center">
                            <div class="tb-full-width">
                                <div class="about-body-icon-cnt">
                                    <i class="material-icons-round about-body-icon fas fa-skiing text-small"></i>
                                    <span class="about-body-text-head">PACKAGES</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    @include('tools.credit-calculator')
                    <br>
                    <h5>Don't Have a Credit Score?</h5>
                    <p>We'll build a plan customized to you.</p>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 desktop-hide full-width">
                    <img src="/assets/img/pays-to-save.jpg" style="width: 100%;">
                </div>
            </div>
            
        </div>
    </section>
    <!-- About End -->

    <!-- End Preview -->

    <section>
        <div class="container full-width" style="background-image:url('/assets/img/rio-bg.jpg'); background-position: center; background-size:cover; padding-bottom:100px; padding-top:100px">
            <div class="row" >
                <div class="col-md-12  text-center pd-20 full-width" style="margin-bottom: -3px; padding:40px; justify-content: center; padding-bottom: 0;">
                    <h4 class="text-light mb-3">
                        Technology will help travelers journey amongst the stars<br>
                        <span class="text-light pt-4 d-block" style="font-size: 1.5em;">Venti will make it affordable</span>
                    </h4>
                    <h2 class="section-head-title text-dark mb-3 section-title" style="font-size: 4rem; display: none;margin-top:20px; color:white !important">$<span class="" id="currency">10</span></h2>
                    <h5 class="section-head-body" style="color:white; max-width: 800px; justify-content:center; margin: 0 auto;">Join our fast-growing community of over travel savers.</h5>
                </div>
            </div>
            <div class="row" style="justify-content: center;">
                <a href="#waitlist" class="btn btn-primary mt-4 mb-4">🚀 Become VIP</a>
            </div>
            <br>
        </div>
    </section>

    <!-- Start FAQs -->
    <section id="faqs" style="padding-bottom:0">
        <div class="container">
            <div class="row">
                <div class="col-md-12  text-center pd-20 text-center" style="padding-bottom: 0; margin-bottom: -3px; padding:40px;" >
                    <h2 class="section-head-title text-dark mb-3 section-title" style="margin:40px; margin-top:20px">FAQs</h2>
                    <h5 style="font-weight: 800; font-size: 16px; text-transform:uppercase;">It's in our DNA to be as transparent as possible</h5>
                </div>
            </div>

            <div class="accordion accordion-flush" id="faqs" style="padding-bottom:0">
                <div class="accordion-item">
                    <h2 class="accordion-header" id="flush-headingTwo">
                      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseTwo" aria-expanded="false" aria-controls="flush-collapseTwo">
                        Why use this over a travel credit card?
                      </button>
                    </h2>
                    <div id="flush-collapseTwo" class="accordion-collapse collapse" aria-labelledby="flush-headingTwo" data-bs-parent="#accordionFlushExample">
                      <div class="accordion-body"><p>Our APR will generally be much lower than the typical credit card, and we do not negatively impact your credit if you fall slightly behind on payments. We also offer much higher borrowing limits to maximize flexbility for your needs.</p></div>
                    </div>
                </div>
                <div class="accordion-item">
                    <h2 class="accordion-header" id="flush-headingFive">
                      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseFive" aria-expanded="false" aria-controls="flush-collapseFive">
                        What happens if my initial credit line is too low?
                      </button>
                    </h2>
                    <div id="flush-collapseFive" class="accordion-collapse collapse" aria-labelledby="flush-headingFive" data-bs-parent="#accordionFlushExample">
                      <div class="accordion-body"><p>No worries! Over time, we will continue to monitor your credit line utilization, travel habits, and repayments gradually increase your credit line.</p></div>
                    </div>
                </div>
                <div class="accordion-item">
                    <h2 class="accordion-header" id="flush-headingOne">
                      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseOne" aria-expanded="false" aria-controls="flush-collapseOne">
                        Why is a cash reserve required?
                      </button>
                    </h2>
                    <div id="flush-collapseOne" class="accordion-collapse collapse" aria-labelledby="flush-headingOne" data-bs-parent="#accordionFlushExample">
                      <div class="accordion-body"><p>Lending money is risky, especially when there is no tangible or physical item that can be sold to pay back the loan in case of default. A cash reserve helps us lower the risk of Jet Bridge along with a lower APR compared to a traditional credit card. At this time, Jet Bridge credit lines cannot be secured using homes, cars, and other assets. Cash only.</p></div>
                    </div>
                </div>
              
              <div class="accordion-item">
                <h2 class="accordion-header" id="flush-headingThree">
                  <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseThree" aria-expanded="false" aria-controls="flush-collapseThree">
                    Will there be a hard check on my credit?
                  </button>
                </h2>
                <div id="flush-collapseThree" class="accordion-collapse collapse" aria-labelledby="flush-headingThree" data-bs-parent="#accordionFlushExample">
                  <div class="accordion-body"><p>A hard check on your credit will be required <strong>if you accept</strong> your pre-approval offer. We only perform a soft check to generate your initial offer, which does not lower your credit score.</p></div>
                </div>
              </div>
              <div class="accordion-item">
                <h2 class="accordion-header" id="flush-headingFour">
                  <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseFour" aria-expanded="false" aria-controls="flush-collapseFour">
                    Can I "cash advance" my credit line to my bank?
                  </button>
                </h2>
                <div id="flush-collapseFour" class="accordion-collapse collapse" aria-labelledby="flush-headingFour" data-bs-parent="#accordionFlushExample">
                  <div class="accordion-body"><p>No, Jet Bridge funds can only be used to complete purchases on Venti.</p></div>
                </div>
              </div>
              <div class="accordion-item">
                <h2 class="accordion-header" id="flush-headingSix">
                  <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseSix" aria-expanded="false" aria-controls="flush-collapseSix">
                    What are the fees for this program?
                  </button>
                </h2>
                <div id="flush-collapseSix" class="accordion-collapse collapse" aria-labelledby="flush-headingSix" data-bs-parent="#accordionFlushExample">
                  <div class="accordion-body"><p>Origination fees may vary from 0.5% to 2% of the approved amount. We are waiving origination fees for those that join our VIP list. The annual fee for this program is 1% of the credit line available. The fee is waived for Business and First Class <a href="/boardingpass">Boarding Pass</a> members.</p> 
                    <p>Fees subject to change.</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="row" id="waitlist">
                <div class="col-md-12 text-center" style="padding:90px 20px; padding-bottom:0;">
                    <h3></h3>
                    <h2 class="section-head-title text-dark mb-3 section-title" style="margin:40px; margin-top:20px">Achieve VIP Status</h2>
                    <p style="max-width:800px; margin:0 auto; font-size:1.2em">We're disrupting travel financing in 2024. Join our waitlist, and we'll award your account with $20 worth of Travel Points when you open a Jet Bridge credit line.</p>
                    <br>
                    <div>
                        <input type="email" class="form-control d-block" placeholder="someone@somewhere.com" id="waitlistEmail" style="width: 300px; margin:0 auto;">
                        <br>
                        <button id="joinWaitlist" class="btn btn-success"><i class="fa fa-sync fa-spin"></i> Sign Up</button>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12" style="padding-top:100px; padding-bottom:0">
                    <p>
                        <span class="footnote">*</span> Annual Percentage Rate (APR) is accurate as of 2/1/2024. Select markets only. APR is compounded and charged monthly. Offering rates may change.
                        <br>
                        <span class="footnote">**</span> Calculator is designed with certain limits and is for illustrative purposes only. Credit line and cash reserve amounts are determined on an case-by-case basis.<br>
                    </p>
                    <p>Venti Financial does not discriminate based on race, ethnicity, national origin, religion, age, sex, sexual orientation, gender identity, family/marital status, disability or medical or genetic condition.</p>
                </div>
            </div>
        </div>
    </section>
    <!-- End FAQs -->

    

@endsection


    
@section("js")
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/typeahead.js/0.10.4/typeahead.bundle.min.js"></script>
    @if(env('APP_ENV') != "local")
        <!-- Meta Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s)
            {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};
            if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
            n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t,s)}(window, document,'script',
            'https://connect.facebook.net/en_US/fbevents.js');
            fbq('init', '1169331916805166');
            fbq('track', 'PageView');
        </script>
        <noscript><img height="1" width="1" style="display:none"
        src="https://www.facebook.com/tr?id=1169331916805166&ev=PageView&noscript=1"
        /></noscript>
        <!-- End Meta Pixel Code -->
    @endif
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/11.0.2/bootstrap-slider.min.js"></script>
    <script>
        $(document).ready(function() {
            $.ajaxSetup({
                 headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                 }
              });

            var interactable = false; 

             
            /**
             * Format the number, so it will be seperated by comma
             */
            function commaSeparateNumber(val){
                while (/(\d+)(\d{3})/.test(val.toString())){
                  val = val.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
                }
                return val;
            }

            toggleLogo("black","white");

            function toggleLogo(color, replaced){
                var logoSRC = $(".logo").attr("src");
                logoSRC = logoSRC.replace(color, replaced);
                console.log(logoSRC)
                $(".logo").attr("src", logoSRC);
            }

            $.fn.isInViewport = function() {
              var elementTop = $(this).offset().top;
              var elementBottom = elementTop + $(this).outerHeight();

              var viewportTop = $(window).scrollTop();
              var viewportBottom = viewportTop + $(window).height();

              return elementBottom > viewportTop && elementTop < viewportBottom;
            };

            


            function isElementVisible($elementToBeChecked)
            {
                var TopView = $(window).scrollTop();
                var BotView = TopView + $(window).height();
                var TopElement = $elementToBeChecked.offset().top;
                var BotElement = TopElement + $elementToBeChecked.height();
                return ((BotElement <= BotView) && (TopElement >= TopView));
            }

            

            $("#joinWaitlist").click(function(){
                var btn = $(this);
                btn.addClass("disable-while-loading");
                var email = $("#waitlistEmail").val();
                var creditScoreValue = -1;
                var savingsBalance = -1;

                if(email == "" || email == undefined){
                    $("#waitlistEmail").addClass("invalid-feedback");
                    btn.removeClass("disable-while-loading");
                    return swal({
                        icon: 'error',
                        title: 'Email is required'
                    });
                }

                if(interactable == true){
                    var creditScoreValue = parseInt($("#pool-amount").val());
                    var savingsBalance = $("#pool-deposits").val();
                }

                $.ajax({
                    url: "{{ route('jetbridge-join-waitlist') }}",
                    processData: true,
                    dataType: 'json',
                    type: 'post',
                    data: {
                        email: email,
                        creditScoreValue: creditScoreValue,
                        savingsBalance: savingsBalance,
                        product: "jetbridge"
                    },
                    success: function (data) {
                        if(data == 200){
                            btn.removeClass("disable-while-loading");
                            $("#waitlistEmail").val("");
                            swal({
                                icon: 'success',
                                title: "You're now VIP",
                                text: "We sent a confirmation email to " + email + " and outlined some next steps."
                            });
                        }
                        else {
                            btn.removeClass("disable-while-loading");
                            swal({
                                icon: 'warning',
                                title: "There was a problem",
                                text: "The email is either invalid or already on our VIP list. If you believe this was an error, please try again. Otherwise, please contact us at admin@venti.co."
                            });
                        }
                    },
                    error: function(data){
                        swal({
                            icon: 'warning',
                            title: "There was a problem",
                            text: "The email is either invalid or already on our VIP list. If you believe this was an error, please try again. Otherwise, please contact us at admin@venti.co."
                        });
                        btn.removeClass("disable-while-loading");
                    }
                });
            });


            $(".nav-icon").each(function(){
                var src = $(this).attr("src");
                console.log(src)
                src = src.replace('.png', '-white-boarding.png');
                $(this).attr("src", src);
              });

            $("#hamburger-menu").click(function(){
                if($("#page_home__show").hasClass("mobile-nav-open")){
                    toggleLogo("white","black");
                } else {
                    toggleLogo("black","white");
                }
                
            });

            $(".calculate").on("change", function(){
                interactable = true;
                var creditScoreValue = parseInt($("#pool-amount").val());
                var savingsBalance = $("#pool-deposits").val();

                if(savingsBalance == 0){
                    savingsBalance = 10000;
                } else{
                    savingsBalance = savingsBalance * 20000;
                }

                

                var result = calculateCreditLineAndCollateral(creditScoreValue, savingsBalance);
                console.log([savingsBalance, creditScoreValue, result]);

                $("#balance").html(result.creditLine.toLocaleString());
                $("#points").html(result.cashCollateral.toLocaleString());
            });

    function calculateCreditLineAndCollateral(creditScoreValue, savingsBalance) {
    // Define the maximum credit line and sliding scale for collateral percentage
    const maxCreditLine = 100000;
    const collateralThreshold = { min: 70, max: 100 }; // Collateral percentage ranges from 70% to 100%

    // Credit score array to determine collateral percentage
    // Lower credit scores require higher collateral
    const creditScoreCollateral = [
        { score: 0, collateral: 90 }, // "580-620" => 90% collateral
        { score: 1, collateral: 85 }, // "621-660" => 85% collateral
        { score: 2, collateral: 80 }, // "661-700" => 80% collateral
        { score: 3, collateral: 75 }, // "701-740" => 75% collateral
        { score: 4, collateral: 72 }, // "741-780" => 72% collateral
        { score: 5, collateral: 70 }  // "781+" => 70% collateral
    ];

    // Credit score weights, with "781+" given the highest weight
    const creditScoreWeights = [0.1, 0.2, 0.3, 0.5, 0.7, 1];

    // Function to determine the base credit line from savings balance
    function getBaseCreditLineFromSavings(savingsBalance) {
        const savingsThresholds = [1000, 2000, 10000, 25000, 40000, 50000, 60000, 100000];
        const baseCreditLines = [10000, 15000, 20000, 25000, 40000, 60000, 80000, 100000];

        let baseCreditLine = maxCreditLine;
        for (let i = savingsThresholds.length - 1; i >= 0; i--) {
            if (savingsBalance >= savingsThresholds[i]) {
                baseCreditLine = baseCreditLines[i];
                break;
            }
        }
        return baseCreditLine;
    }

    // Get the weight for the credit score
    const creditScoreWeight = creditScoreWeights[creditScoreValue];

    // Get the base credit line for the savings balance
    const baseCreditLine = getBaseCreditLineFromSavings(savingsBalance);

    // Calculate the credit line based on credit score weight and base credit line from savings
    const creditLine = Math.min(creditScoreWeight * baseCreditLine, maxCreditLine);

    // Determine collateral requirement based on credit score
    const collateralEntry = creditScoreCollateral.find(item => item.score === creditScoreValue);
    const collateralPercentage = collateralEntry.collateral;

    return {
        creditLine: Math.round(creditLine), // Round to nearest whole number for the credit line
        cashCollateralRequirement: collateralPercentage, // Percentage (not rounded)
        cashCollateral: Math.round(creditLine * (collateralPercentage / 100)) // Calculate cash collateral in dollars
    };
}


          

            @if(Session::get('subscribeError') !== null)
                swal({
                    icon: 'error',
                    title: 'Oops!',
                    text: 'We ran into a problem and could not add you to our list. Try again or send an email to markus@venti.co'
                })
            @endif

            makeTimer();

            function makeTimer() {

            var endTime = new Date("31 December 2023 23:59:49 GMT-04:00");          
            endTime = (Date.parse(endTime) / 1000);

            var now = new Date();
            now = (Date.parse(now) / 1000);

            var timeLeft = endTime - now;

            var days = Math.floor(timeLeft / 86400); 
            var hours = Math.floor((timeLeft - (days * 86400)) / 3600);
            var minutes = Math.floor((timeLeft - (days * 86400) - (hours * 3600 )) / 60);
            var seconds = Math.floor((timeLeft - (days * 86400) - (hours * 3600) - (minutes * 60)));
  
            if (hours < "10") { hours = "0" + hours; }
            if (minutes < "10") { minutes = "0" + minutes; }
            if (seconds < "10") { seconds = "0" + seconds; }

            $("#days").html(days + "<span> DAYS</span>");
            $("#hours").html(hours + "<span> HOURS</span>");
            $("#minutes").html(minutes + "<span> MINUTES</span>");
            $("#seconds").html(seconds + "<span style='padding-right:0'> SECONDS</span>");       

    }

    setInterval(function() { makeTimer(); }, 1000);

        });
    </script>

@endsection