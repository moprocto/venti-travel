@extends('layouts.curator-master')

@section('css')
    <script
    type="text/javascript"
    src="//cdn.dwolla.com/v2.2.0/dwolla-web.js"
  ></script>
    <link rel="stylesheet" type="text/css" href="/assets/css/steps.css">
    <link rel="stylesheet" href="/assets/css/intlTelInput.min.css">
    <style>
        input.disabled{
            pointer-events: none;
        }
        .wizard>.content>.body{
            height: 100%;
            position: relative;
        }
        .wizard>.content{
            min-height: 120px;
        }
        .wizard .steps{
            display: none;
        }

@media (max-width: 767px){
    .black.hero-section {
    margin-left: 0 !important;
    margin-right: 0 !important;
    }
}
.col-form-label{
    font-weight: bold !important;
}
form > *{
    font-weight: 300;
}

.slim-p{
    margin: 0;
    padding: 0;
}
.isVerified{
    display: none;
}

</style>
@endsection

@section('content')
<div id="page-content">
    <div class="gradient-wrap">
        <div class="container black position-relative hero-section mb-4">
            <div class="row justify-content-center" >
                <div class="col-md-12 text-center mt-4">
                    <h2 class="mt-2">Connect Your Bank</h2>
                </div>
                <div class="col-md-4">
                    <div class="card soft-shadow" style="border:none;">
                        <div class="card-body">
                            <div class="row mt-4 text-center">
                                <div class="col-md-12 text-center">
                                    <i class="fa fa-bank" style="font-size:84px"></i>
                                </div>
                            </div>
                            <div id="example-basic">
                                <h3></h3>
                                <section>
                                    <div class="row mb-3 justify-content-center">
                                        <div class="col-md-12">
                                            <h5 class="text-center"><label for="name" class="col-form-label text-left text-bold">Easy. Fast. Secure.</label></h5>
                                            <p></p> 
                                            <p>You begin depositing funds onto your Boarding Pass, you'll need to connect a bank account. We've partnered with Dwolla, a reputable digital banking provider to make this process easy.</p>
                                        </div>
                                        <div class="col-md-12 text-center">
                                            <p style="margin:0"></p>
                                            <span style="font-size:12px;"></span>
                                            Get Started
                                        </div>
                                    </div>
                                </section>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
    <script type="text/javascript" src="/assets/js/jquery.steps.min.js"></script>
    <script src="/assets/js/intlTelInput.js"></script>

    <script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': "{{ csrf_token() }}"
        }
    });

    var input = document.querySelector("#phoneNumber");

    window.intlTelInput(input, {
      formatOnDisplay: true,
      utilsScript: "/assets/js/utils.js",
    });

    iti = intlTelInput(input);

    var phoneNumber;

    $("#sendSMSVerification").click(function(){
        var button = $(this);
        var phoneNumber = iti.getNumber(); //input.getNumber(); //$("#phoneNumber").val();
        if(phoneNumber.length < 10 || phoneNumber == ""){
            alert("Hmmm the phone number doesn't seem right");
        } else {

            if(!phoneNumber.includes("+")){
                var countryData = iti.getSelectedCountryData();
                phoneNumber = "+" + countryData.dialCode + phoneNumber;
            }

            $.ajax({
                url: "{{ route('send-sms-verification') }}",
                type: 'post',
                processData: true,
                dataType: 'json',
                data:{
                    phoneNumber: phoneNumber
                },
                success: function (data) {
                    console.log(data);
                    if(data != "error"){

                        button.parent().html("<br><input type='number' class='form-control' placeholder='123456' id='smscode' minlength='6' style='width:200px'><button type='button' data-phoneNumber='" + data.phoneNumber +"' data-service-id='" + data.serviceID + "' style='margin-left:5px;' class='btn btn-primary' id='verifySMS'><i class='fa fa-check'></i></button>");
                        button.parent().parent().append("<p style='Enter the code we texted you. It may take a few seconds to arrive.'></p>");
                    } else{
                        alert("There was an error sending a text message to: " + $("#phoneNumber").val() + ". Please make sure the phone number is correct and the appropriate country code is applied.");
                    }
                    
                },
               error: function(jqXhr, textStatus, errorMessage){
                  console.log("Error: ", errorMessage);
               }
            });

            $("#full_number").text("We sent a txt to: " + phoneNumber);
        }
        
    });

    $("body").on("click", "#verifySMS", function(){
        var button = $(this);
        var smsCode = $("#smscode").val();
        var serviceID = $(this).attr("data-service-id");
        var phoneNumber = $(this).attr("data-phoneNumber");

        if(phoneNumber.length < 10 || serviceID == ""){
            alert("Hmmm the phone number doesn't seem right");
        }

        else {
            $.ajax({
                url: "{{ route('verify-sms-code') }}",
                type: 'post',
                processData: true,
                dataType: 'json',
                data:{
                    phoneNumber: phoneNumber,
                    smsCode: smsCode,
                    serviceID: serviceID
                },
                success: function (data) {
                    if(data == "ok"){
                        button.parent().html("<p>Phone number verified <i class='fa fa-check'></i> <br><a href='/boardingpass/onboarding/continue' class='btn btn-primary'>Continue</a></p>");
                    }
                    else {
                        alert("That code isn't correct.");
                    }
                }
            });
        }
    });

    </script> 
@endsection


