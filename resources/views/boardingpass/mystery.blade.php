@extends('layouts.curator-master')

@section('css')
	<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/pretty-checkbox@3.0/dist/pretty-checkbox.min.css">
	<style type="text/css">
		.card {
			box-shadow: 0 1px 1px rgba(0,0,0,0.01), 
      0 2px 2px rgba(0,0,0,0.01), 
      0 4px 4px rgba(0,0,0,0.01), 
      0 8px 8px rgba(0,0,0,0.01),
      0 16px 16px rgba(0,0,0,0.01);
		}
		.disabled{
			opacity: 0.4;
			pointer-events: none;
		}
		p, span{
			font-weight: 300;
		}
		.form-label{
			font-weight: 600;
		}
		.invalid-feedback{
			margin-top: 0;
			display: block;
			color: inherit;
			border-color: #dc3545;
		}
		.full-width .nav-link{
			max-width: 100%;
		}
		.mb-xs-3{
			margin-bottom: 1rem!important;
		}
		div.dt-buttons .dt-button{
			padding: 3px 10px;
    	font-size: 12px;
		}

		.tab-content tbody td{
			font-size: 12px;
		}

		button.nav-link{
			box-shadow: none !important;
		}
		@media(max-width: 768px){
			.welcome{
				padding-top: 0 !important;
			}
			.hero-section{
				margin-top: 0 !important;
			}
			.logoutRow{
				display: block;
			}
			body{
				padding-bottom: 150px;
			}
			#products-nav-link{
				display: none;
			}
			#footer{
				display: none;
			}
			.dtr-details{
				list-style: none;
    		padding: 0;
    		padding-left: 10px;
    		width: 100%;
			}
			.dtr-details li.text-center{
				text-align: left !important;
			}
		}
		@media(min-width: 768px){
			.nav-justified .nav-item, .nav-justified>.nav-link{
				flex-grow: 0.25;
			}
		}


		#mysterybox{
			width: 200px;
			margin: 4em auto;
		}

		.mysterybox-closed{
			animation: wiggle 2.5s infinite;
		}

		.box-1 {
			
			animation-delay: 0.5s;
		}
		.box-2 .mysterybox-closed{

			animation-delay: 1.5s;
		}
		.box-3 .mysterybox-closed{

			animation-delay: 3.5s;
		}
		#shaketray{
			justify-content: center;
		}
		#shaketray.default{
			justify-content:space-between !important;
		}
.text-small{
	font-size: 12px;
}

.no-shadow{
	box-shadow: none !important;
}

.card-body{
	background: white;
	border-radius: 0.35rem;
}

@keyframes wiggle {
    0% { transform: rotate(0deg); }
   80% { transform: rotate(0deg); }
   85% { transform: rotate(5deg); }
   95% { transform: rotate(-5deg); }
  100% { transform: rotate(0deg); }
}

#winsContainer{
	position: absolute;
	left: 10px;
	background: rgba(0,0,0,0.8);
	padding: 5px 10px;
	border-radius:10px; 
	font-weight: bold;
	color: white;
}

#pointsContainer{
	position: absolute;
	right: 10px;
	background: rgba(0,0,0,0.1);
	padding: 5px 10px;
	border-radius:10px; 
	font-weight: bold;
}
.disable-while-loading .d-block{
	display: none !important;
}
#drama{
	display: none;
	top: 0;
	left: 0;
	height: 100%;
	width: 100%;
	position: fixed;
	background: rgba(0, 0, 0, 0);
}
#drama.effect{
	display: block;
	background: rgba(0, 0, 0, 0.8);
	transition: all 5s ease-out allow-discrete;
}
.box-container{
	position: absolute;
}
.box-1{
	left: 20px;
}
.box-2{
	left: 50%;
	margin-left: -100px;
	top: 80px;
	z-index: 3;
}
.box-3{
	right: 20px;
}
#boxes{
	min-height: 400px;
}
.opened{
	opacity: 0.3;
}
</style>
@endsection

@section('content')
	<div id="page-content">
    <div class="gradient-wrap">
    	<div class="container hero-section mb-4 mt-4">
    		<div class="col-md-12">
		    	<div class="welcome pt-4 text-center">
						<h2>It's Your Lucky Day, {{ getFirstName($user->displayName) }}!</h2>
						<p>What's in the box? See the full list of prizes, winning odds, and past winners <a href="/about/mysterybox" target="_blank">here</a>.<br><strong class="font-weight-bold">We've Awarded {{ env('MYSTERY_WINS') ?? 511 }} Points in Total</strong></p>
					</div>
				</div>
				<div class="row" style="justify-content:center;">
					<div class="col-md-6 col-md-offset-3">
						<div class="card mb-4">
							<div class="card-body text-center" style="min-height:400px;">
								<div id="pointsContainer">My Points: <span id="points" style="font-weight:bold;"><i class="fa fa-sync fa-spin"></i></span></div>
								<div id="boxes">
								@for($i=1;$i<=3;$i++)
									<div class="box-container box-{{ $i }}">
										<img id="mysterybox" src="/assets/img/icons/mystery-box.png" class="mysterybox-closed mysterybox-{{ $i }}">
									</div>
								@endfor
							</div>
							<div id="termsBox" style="display: flex; flex-direction: row; justify-content:center;">
								
								<div><p><strong>Prize odds updated on Aug 14, 2024 at 9:45 A.M. EDT</strong></p>By opening Mystery Box, you agree to our <a href="/about/mysterybox" target="_blank">Terms and Conditions</a></div>
							</div>
							</div>
						</div>

						<div id="shaketray" class="default" style="display: flex; flex-direction: row; text-align: center; display: none;">
							<button class="btn btn-primary openbox" data-wager="1" style="min-width: 30%;">
								<span class="d-block" style="min-width: 30%; font-size:30px; font-weight:800">Open</span>
								<i class="fa fa-sync fa-spin" style="font-size:30px"></i>
								1 Point
							</button>
							<button class="btn btn-success openbox" data-wager="3" style="min-width: 30%;">
								<span class="d-block" style=" font-size:30px; font-weight:800">Shake</span>
								<i class="fa fa-sync fa-spin" style="font-size:30px"></i>
								3 Points
							</button>
							<button class="btn btn-black openbox" data-wager="5" style="min-width: 30%;">
								<span class="d-block" style="font-size:30px; font-weight:800">Crush</span>
								<i class="fa fa-sync fa-spin" style="font-size:30px"></i>
								5 Points
							</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('js')
	<script>
		$.ajaxSetup({
				headers: {
			    	'X-CSRF-TOKEN': '{{ csrf_token() }}'
				}
		});

		getPoints();
		boxesleft({{ env('MYSTERY_LIMIT') - $opens['opens24Hours'] }});

		function getPoints(){
			$.ajax({
		      url: "{{ route('wallet-get-points-balance') }}",
					type: 'POST',
					async: true,
					processData: true,
	        dataType: 'json',
          success: function (data) {
          	if(data == 0){
          		$("#shaketray").removeClass("default");
          		$("#shaketray").html("<strong style='text-align:center'>Points are required to open the Mystery Box</strong>")
          	}
          	$("#points").text(data.toLocaleString(undefined, {minimumFractionDigits: 2}))
          }
			});
		}

		$(".openbox").click(function(){
			var wager = $(this).data("wager");
			var btn = $(this);

			btn.addClass("disable-while-loading");


      // send POST request

      $.ajax({
		      url: "{{ route('mystery-box-open') }}",
					type: 'POST',
					async: true,
					processData: true,
	        dataType: 'json',
          data:{
              wager: wager
          },
          success: function (data) {
          	$("#mysterybox").removeClass("mysterybox-closed");
          	
          	btn.removeClass("disable-while-loading");
          	setTimeout(function(){
          		
          		if(data.status == 200){
	          		return swal({
				          icon: 'success',
				          title: 'You won!',
				          text: data.message
				        });
	          	}
	          	if(data.status == 201){
	          		return swal({
				          title: 'Better luck next time',
				          text: data.message
				        });
	          	}
	          	return swal({
	          			icon: 'error',
				          title: 'Uh oh',
				          text: data.message
				       });
          	}, 500);
          	getPoints();
          	remainingAttempts();

          },
          error: function(data){
          	swal({
				          icon: 'warning',
				          title: 'Something went wrong!',
				          text: "An error occured during your request. If this issue persists, please contact support via admin@venti.co",
				        });
          	btn.removeClass("disable-while-loading");
          }
			});

    });

    function remainingAttempts(){
				$.ajax({
		      url: "{{ route('mystery-box-opens') }}",
					type: 'POST',
					async: true,
					processData: true,
	        dataType: 'json',
          success: function (data) {
          	var boxes = env('MYSTERY_LIMIT') - parseInt(data.opens24Hours);
          	boxesleft(boxes);
          },
          error: function(data){
          	console.log(data)
          }
			});
		}
		function boxesleft(boxes){
				if(boxes <= 2){
          		$(".mysterybox-1").attr("src", "/assets/img/icons/mystery-box-open.png");
          		$(".mysterybox-1").addClass("opened");
          		$(".mysterybox-1").removeClass("mysterybox-closed");
          	}
      	if(boxes <= 1){
      		$(".mysterybox-2").attr("src", "/assets/img/icons/mystery-box-open.png");
      		$(".mysterybox-2").addClass("opened");
      		$(".mysterybox-2").removeClass("mysterybox-closed");
      	}
      	if(boxes <= 0){
      		$(".mysterybox-3").attr("src", "/assets/img/icons/mystery-box-open.png");
      		$(".mysterybox-3").addClass("opened");
      		$(".mysterybox-3").removeClass("mysterybox-closed");

      		$("#shaketray").html('<h2>No more boxes available. Try again tomorrow.</h2>');
      		$("#termsBox").hide();
      	}
			}
	</script>
@endsection
