@extends('layouts.curator-master')

@section('css')
    <style type="text/css">
        @media(max-width: 1368px ){
            .tb-full-width{
                width: 100%;
            }
        }
        #footer{
            margin-top: 100px;
        }
        .about-body{
            justify-content: center;
        }
    </style>
@endsection

@section('content')
<div id="backdrop" style="margin-top: -130px; width: 100%; z-index: 1; display: flex; align-items: center; min-height: 800px; background-position: center;  background-size: cover; margin-bottom: -64px; background-image:url('/assets/img/navigator-backdrop-t2.png')">
        <div class="gradient-wrap" style="background: transparent; width:100%;">
            <!--  HERO -->
            <div class="section-wrapper black position-relative">
                <div class="row" style="align-items:center;">
                    <div class="col-md-5 col-sm-12 col-12 top-header" style="display: flex; align-items:center;">
                        <div id="hero-text-wrapper" style="max-width:900px;">
                            <h1 class="mb-3 section-title" style="line-height: 3rem; color: white; font-weight:600 !important; font-size: 3.8rem;">
                            SUCCESS!</sup>
                            <span style="font-size: 0.45em; display:block; text-shadow: 2px 3px 5px rgba(0,0,0,0.25);">You're now VIP</span>
                            </h1>
                            <h2 class="section-subtitle mb-4" style="color: white; max-width:900px; text-shadow: 2px 3px 5px rgba(0,0,0,0.25);">
                            We just sent you a confirmation email. We'll notify you when it's your turn to create your Boarding Pass.</h2>

                            <div class="card" style="border-color: transparent; border-radius:10px; background-color:rgba(255,255,255,0.9)">
                                <div class="card-body">
                                    <p style="font-weight:bold">Share your referral link with a friend, and you both will be credited $10 onto your Boarding Passes with your first deposits:</p>

                                    <div class="btn-group">
                                        <span class="form-control" id="link" style="min-width: 250px; padding:10px; border-color:transparent;">https://venti.co/s/{{$referral}}</span>
                                        <button class="btn btn-black" onclick="copyLink();"><i class="fa fa-regular fa-copy text-light txt-white"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<!-- Cover End -->
@endsection


    
@section("js")
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/typeahead.js/0.10.4/typeahead.bundle.min.js"></script>
    @if(env('APP_ENV') != "local")
        <!-- Meta Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s)
            {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};
            if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
            n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t,s)}(window, document,'script',
            'https://connect.facebook.net/en_US/fbevents.js');
            fbq('init', '1169331916805166');
            fbq('track', 'PageView');
        </script>
        <noscript><img height="1" width="1" style="display:none"
        src="https://www.facebook.com/tr?id=1169331916805166&ev=PageView&noscript=1"
        /></noscript>
        <!-- End Meta Pixel Code -->
    @endif
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/11.0.2/bootstrap-slider.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $.ajaxSetup({
                 headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                 }
              });

            toggleLogo("black","white");

            function toggleLogo(color, replaced){
                var logoSRC = $(".logo").attr("src");
                logoSRC = logoSRC.replace(color, replaced);
                console.log(logoSRC)
                $(".logo").attr("src", logoSRC);
            }

            

        });
        function copyLink(){
              console.time('time1');
                var temp = $("<input>");
              $("body").append(temp);
             temp.val($('#link').text()).select();
              document.execCommand("copy");
              temp.remove();
                console.timeEnd('time1');
                $('#link').parent().find(".btn-black").removeClass("btn-black").addClass("btn-success");
                $('#link').parent().find(".btn-success").find(".fa").removeClass("fa-regular").removeClass("fa-copy").addClass("fa-check");

            return swal({
              icon: 'success',
              title: 'All Set!',
              text: 'Your referral link has been copied',
            })
        }

    </script>
@endsection