<div class="card" style="background-image:  linear-gradient(112.1deg, rgba(242, 242, 246,0) 11.4%, rgba(255, 255, 255, 0.7) 70.2%); background-color: rgba(252, 166, 76, 0.2);">
    <div class="card-body features">
        <h4>Business Class</h4>
        <p class="text-bold">Starting at $4.99 per Account per Month.</p>
        <p>Extend travel benefits to your employees, clients, and customers.</p>
        <a href="/business" class="btn btn-black">Learn More</a>
    </div>
</div>