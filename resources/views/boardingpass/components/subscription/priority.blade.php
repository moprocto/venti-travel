<div class="card" style="background-image:  linear-gradient(112.1deg, rgba(242, 242, 246,0) 11.4%, rgba(255, 255, 255, 0.7) 70.2%); background-color: rgba(50, 50, 20, 0.1);">
    <div class="card-body features">
        <h4>Priority</h4>
        <p class="text-bold">Discounted to ${{ number_format(env('VENTI_PRIORITY_ANNUAL_FEE')/100,2) }}<br><u>No Annual Fee</u>.</p>
        <p>Great for solo travelers, couples, travel buddies, and small families that travel together.</p>
        <ul>
            <li>Earn 9% APY on Deposits</li>
            <li>Your name must be on bookings</li>
            <li>Max Points Balance: ∞</li>
            <li>Max Cash Savings Balance: ${{ number_format(env("VENTI_PRIORITY_DEPOSIT_CAP", 0)) }}</li>
            <li>Eligible for our <a href="/tailwind" target="_blank">Venti Travel Rewards Card</a></li>
        </ul>
        @if($selectable)
            <a href="/home/change/priority" class="btn btn-success">Select</a>
        @endif
    </div>
</div>