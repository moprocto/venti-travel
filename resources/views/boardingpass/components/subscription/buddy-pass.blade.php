<div class="card">
    <div class="card-body features">
        <h4>Pro</h4>
        <p class="text-bold">Earn Cash and Points. Available Jan. 3</p>
        <p>Free when you create a savings account with a Credit Union on our <a href="/about/pro" target="_blank">favorites list</a>.</p>
        <ul>
            <li>Earn cash interest and Points</li>
            <li>Your name must be on bookings</li>
            <li>Max Points Balance: ∞</li>
        </ul>
        <a href="/about/pro" class="btn btn-black">Learn More</a>
    </div>
</div>