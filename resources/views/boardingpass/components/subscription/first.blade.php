<div class="card" style="background-image:  linear-gradient(112.1deg, rgba(242, 242, 246,0) 11.4%, rgba(255, 255, 255, 0.7) 70.2%); background-color: rgba(49, 127, 131, 0.2);">
    <div class="card-body features">
        <h4>First Class</h4>
        <p class="text-bold">Discounted to ${{ number_format(env('VENTI_FIRST_ANNUAL_FEE')/100,2) }} for first year. Then $500 per year.</p>
        <p>Perfect for frequent flyers and corporate travel. Free upgrades and other perks.</p>
        <ul>
            <li>Earn 9% APY on Deposits</li>
            <li>Can purchase on behalf of others</li>
            <li>Max Points Balance: ∞</li>
            <li>Max Cash Savings Balance: ${{ number_format(env("VENTI_FIRST_DEPOSIT_CAP", 0)) }}</li>
            <li>Eligible for our <a href="/tailwind" target="_blank">Venti Travel Rewards Card</a></li>
            <li>Coming Soon:
                <ul>
                    <li>Annual Travel Insurance</li>
                    <li>TSA Pre-Check Reimbursement</li>
                    <li>Discounted Airport Lounges</li>
                </ul>
            </li>
        </ul>
        @if($selectable)
            <a href="/home/change/first" class="btn btn-success">Select</a>
        @endif
    </div>
</div>