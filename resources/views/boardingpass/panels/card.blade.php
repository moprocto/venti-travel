<div class="modal fade" id="addCardModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true" style="background-image: linear-gradient(112.1deg, rgba(125, 110, 112,0.5) 11.4%, rgba(152, 135, 125,0.8) 70.2%); ">
	  <div class="modal-dialog modal-dialog-centered">
	    <div class="modal-content">
	      <div class="modal-header" style="position: absolute;
	    right: 0;
	    top: 10px; border-bottom: transparent; z-index: 99;">
	        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
	      </div>
	      <div class="modal-body">
			<form id="addCardForm" method="POST">
				@CSRF
				<h5 class="mb-2">Add Card</h5>
				<span class="d-block" style="font-size:12px;">Your card details are encrypted and securely stored with Stripe, a reputable credit card processor.</span>
				<div class="row mt-3">
					<div class="col-md-12">
						<label class="form-label">Name on Card <sup>*</sup><br></label>
						<input type="text" name="cardName" id="cardName" class="form-control required" value="{{ cleanName($user->displayName) }}">
					</div>
				</div>
				<div class="row mt-3">
					<div class="col-md-12">
						<label class="form-label">Card Nickname<span class="d-block text-small">In the future, we'll provide suggestions on which card you should use to maximize rewards.</span></label>
						<input type="text" name="cardNickname" id="cardNickname"  class="form-control" placeholder="Freedom Express">
					</div>
				</div>
				<div id="new-card-element" style="margin: 25px 0;"></div>
				<p style="font-size:11px;margin-top:10px;">I agree that I am an authorized user for this card to conduct payments on Venti and will abide by the Card Purchasing agreement outlined in Venti's Terms of Service. Venti reserves the right to call the card issuer to verify ownership. Verification failure may result in account suspension.</p>
				<button type="submit" class="btn btn-primary" style="max-width: 250px;" id="addCard"><i class="fa fa-sync fa-spin"></i> Agree and Add Card</button>
			</form>
			</div>
	    </div>
	</div>
</div>