<div class="modal fade" id="depositFunds" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true" style="background-image: linear-gradient(112.1deg, rgba(125, 110, 112,0.5) 11.4%, rgba(152, 135, 125,0.8) 70.2%); ">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header" style="position: absolute; right: 0; top: 10px; border-bottom: transparent; z-index: 99;">
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div class="tab-content pt-3" id="pills-tabContentDeposit">
          <div class="tab-pane fade show active" id="pills-deposit" role="tabpanel" aria-labelledby="pills-share-tab">
            <ul class="nav nav-pills nav-fill flex d-flex mb-3">
              <li class="nav-item">
                <a class="nav-link active text-center" aria-current="page" href="#pills-onetime-deposit" data-bs-toggle="pill" data-bs-target="#pills-onetime-deposit" type="button" role="tab" aria-controls="pills-onetime-deposit">One Time</a>
              </li>
              <li class="nav-item">
                <a class="nav-link text-center" aria-current="page" href="#pills-recurring-deposit" data-bs-toggle="pill" data-bs-target="#pills-recurring-deposit" type="button" role="tab" aria-controls="pills-recurring-deposit">Recurring</a>
              </li>
            </ul>
          </div>
          <div id="pills-onetime-deposit" class="tab-pane fade in active show" style="min-height:385px;">
            @include("boardingpass.panels.transactions.onetime-deposit")
          </div>
          <div id="pills-recurring-deposit" class="tab-pane fade in" style="min-height:385px;">
            @include("boardingpass.panels.transactions.recurring-deposit")
          </div>
        </div>
      </div>
    </div>
  </div>
</div>