<div class="pt-3">
	<div class="card bg-white">
		<div class="card-body">
			<p>A decision was made to suspend your account for one of the following reasons:
            </p>
            <ul style="font-weight: 300;">
                <li>Your social security number has been blocked for usage on our platform.</li>
                <li>You added funding sources that have been blocked for usage on our platform.</li>
                <li>You attempted a transaction on our platform that was flagged for suspicious activity or abuse.</li>
                <li>Too many deposits failed due to insufficient funds or unauthorized returns.</li>
                <li>An unauthorized return code indicates fraud or other banking violation.</li>
                <li>You were invoiced for insufficient funds or an authorized return and did not pay the invoice.</li>
            </ul>
            <p style="">While your account is suspended, you are not able to add funding sources, make deposits, or withdraw funds.</p>
            <p>If you believe this is an error, please <strong style="font-weight:500">submit an appeal to <a href="mailto:admin@venti.co">admin@venti.co</a></strong>. 
            <p>In your appeal, you may include:</p>
            <ul>
                <li>A <strong>letter from your bank(s) that acknowledges you are the legitimate owner</strong> of the funding sources connected to your Venti account</li>
                <li>OR a photo of you holding your state ID next to your face. For the second option, your name, date of birth, document expiry, and your face must be clearly visible.</li>
            </ul>
            <p>Failure to complete the above recommendations will lengthen the time it takes to review your account.</p>
            <p></p>
		</div>
	</div>
</div>