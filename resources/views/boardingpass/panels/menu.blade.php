<ul class="nav nav-pills mb-3 full-width" id="pills-tab" role="tablist">
  <li class="nav-item" role="presentation">
    <button class="nav-link active" id="pills-home-tab" data-bs-toggle="pill" data-bs-target="#pills-home" type="button" role="tab" aria-controls="pills-home" aria-selected="true">Boarding Pass</button>
  </li>
  @if($customer)
  <li class="nav-item" role="presentation">
    <button class="nav-link" id="pills-funding-tab" data-bs-toggle="pill" data-bs-target="#pills-funding" type="button" role="tab" aria-controls="pills-funding" aria-selected="false">Wallet</button>
  </li>
  <li class="nav-item" role="presentation">
    <button class="nav-link" id="pills-orders-tab" data-bs-toggle="pill" data-bs-target="#pills-orders" type="button" role="tab" aria-controls="pills-orders" aria-selected="false">Ledger</button>
  </li>
  @endif
  <li class="nav-item" role="presentation">
    <button class="nav-link" id="pills-settings-tab" data-bs-toggle="pill" data-bs-target="#pills-settings" type="button" role="tab" aria-controls="pills-settings" aria-selected="false">My Account</button>
  </li>
  <li class="nav-item" role="presentation">
    <a href="{{ route('logout') }}" class="nav-link text-danger">Log Out <i class="fa fa-lock"></i></a>
  </li>
</ul>

