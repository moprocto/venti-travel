<div class="modal fade" id="spendingExplainerModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true" style="background-image: linear-gradient(112.1deg, rgba(126, 124, 224,0.5) 11.4%, rgba(11, 143, 251,0.8) 70.2%);">
	<div class="modal-dialog modal-dialog-centered">
	    <div class="modal-content" >
			<div class="modal-header" style="border-bottom:0; padding-bottom:0">
				<h5 style="margin:0; padding:0">Spending Power</h5>
				<button type="button" class="btn-close text-white" data-bs-dismiss="modal" aria-label="Close" style=""></button>
			</div>
		    <div class="modal-body">
				<p style="">This is the sum of your current Cash Balance and Boarding Pass Points, which reflects the total amount of funds you have available to purchase with. Your spending power does not include transactions that have not cleared.</p>
			</div>
	    </div>
	</div>
</div>