<div class="tab-content pt-3" id="pills-tabContent">
	<div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
		<div class="card bg-white small-shadow">
			<div class="card-body">
				<p>We have securely submitted your information to our ACH payments service provider. Your Boarding Pass is now pending approval. We will email you the status of your Boarding Pass soon.</p>
			</div>
		</div>
	</div>
</div>