<div class="card bg-white">
	<div class="card-body">
		@if(!$retry)
			<p>{{ $intro }}</p>
			@else
			<p>Our ACH payments service provider is requesting that you resubmit your application with your full social security number. This can happen if you've recently changed addresses, or is something in your prior submission didn't fully pass verification, like a typo. Try again and make sure all details are correct.</p>
		@endif
		@if($subscription == "buddy")
		<div class="alert" style="border:1px solid rgba(50, 0, 0, 1);">
			<h6>You Selected <u>Buddy Pass</u>: Free Plan</h6>
			<p>A Buddy Pass is our <strong>free-tier membership</strong> that allows you to receive Points from verified Priority, Business, and First Class members. You cannot deposit funds and your account will not be able to earn up to 9% APY. 
		</div>
			<div class="row">
				
				<div class="col-md-4 text-center">
					@include('boardingpass.components.subscription.priority', ["selectable" => true])
				</div>
				<!--
				<div class="col-md-4 text-center">
					@include('boardingpass.components.subscription.business', ["selectable" => true])
				</div>
			-->
				<div class="col-md-4">
					@include('boardingpass.components.subscription.first', ["selectable" => true])
				</div>
			</div>
		@endif
		
		@if($subscription == "priority")
			<div class="alert" style="border:1px solid rgba(80, 80, 20, 0.8);">
				<h6>You Selected <u>Priority Membership</u>: No Annual Fee</h6>
				<p>A great value if you're not a frequent traveler. Earn up to 9% APY on deposits in the form of Points. If you'd like to change membership, you can do so now.</p>
			</div>
			<div class="row">
				<!--
					<div class="col-md-4">
						@include('boardingpass.components.subscription.buddy-pass', ["selectable" => true])
					</div>
				-->
				<div class="col-md-4">
					@include('boardingpass.components.subscription.priority', ["selectable" => false])
				</div>
				<div class="col-md-4">
					@include('boardingpass.components.subscription.first', ["selectable" => true])
				</div>
			</div>
		@endif
		<!--
		@if($subscription == "business")
			<div class="alert" style="border:1px solid rgba(252, 166, 76, 0.8);">
				<h6>You Selected <u>Business Class Membership</u></h6>
				<p>We think Business Class is great if you travel more than four times a year and regularly budget for your trips. Earn up to 7% APY on deposits in the form of Points. If you'd like to change membership, you can do so now.</p>
			</div>
			<div class="row">
				<div class="col-md-4">
					@include('boardingpass.components.subscription.buddy-pass', ["selectable" => true])
				</div>
				<div class="col-md-4">
					@include('boardingpass.components.subscription.priority', ["selectable" => true])
				</div>
				<div class="col-md-4">
					@include('boardingpass.components.subscription.first', ["selectable" => true])
				</div>
			</div>
		@endif
		-->
		@if($subscription == "first")
			<div class="alert" style="border:1px solid rgba(49, 127, 131, 0.8);">
				<h6>You Selected <u>First Class Membership</u></h6>
				<p>This is as good as it gets. Earn up to 9% APY on deposits in the form of Points.</p>
				<p class="alert alert-success">If you'd like to change membership, you can do so now.</p>
			</div>
			<div class="row">
				<!--
				<div class="col-md-4">
					@include('boardingpass.components.subscription.buddy-pass', ["selectable" => true])
				</div>
				-->
				<div class="col-md-4">
					@include('boardingpass.components.subscription.priority', ["selectable" => true])
				</div>

				<div class="col-md-4">
					@include('boardingpass.components.subscription.first', ["selectable" => false])
				</div>

			</div>
		@endif
		<br><sup>*</sup> for non-discounted accounts only.</p>
	</div>
</div>