<div class="modal fade" id="referralModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true" style="background-image: linear-gradient(112.1deg, rgba(28, 187, 156,0.4) 11.4%, rgba(78, 199, 171, 0.8) 70.2%);">
	<div class="modal-dialog modal-dialog-centered">
	    <div class="modal-content" >
			<div class="modal-header" style="border-bottom:0; padding-bottom:0;">
				<button type="button" class="btn-close text-white" data-bs-dismiss="modal" aria-label="Close" style=""></button>
			</div>
		    <div class="modal-body">
		    	<div class="tab-content pt-3" id="pills-tabContentShare">
					<div class="tab-pane fade show active" id="pills-share" role="tabpanel" aria-labelledby="pills-share-tab">
						<ul class="nav nav-pills nav-fill flex d-flex mb-3">
						  <li class="nav-item">
						    <a class="nav-link active text-center" aria-current="page" href="#pills-share-points" data-bs-toggle="pill" data-bs-target="#pills-share-points" type="button" role="tab" aria-controls="pills-share-points">Share Points</a>
						  </li>
						  <li class="nav-item">
						    <a class="nav-link text-center" aria-current="page" href="#pills-share-code" data-bs-toggle="pill" data-bs-target="#pills-share-code" type="button" role="tab" aria-controls="pills-share-code">Referral Code</a>
						  </li>
						</ul>
						<div class="tab-content" >
							<div id="pills-share-points" class="tab-pane fade in active show" style="min-height:385px;">

								@if($subscription != "buddy")
									<p style="font-size:13px">Give the gift of travel. To send your Points to someone else, they will need a Venti account to receive and use Points when purchasing travel. A subscription is not required to receive Points. To donate your points to charity, please contact admin@venti.co.</p>
									<div class="form-group">
										<label>Recipient's Email<br><span style="font-size:12px; font-weight:400">They must have an active Venti account to receive points.</span></label>
										<input type="email" class="form-control required" id="pointsRecipient" placeholder="someone@somewhere.com">
									</div>
									<div class="form-group">
										<label>Amount of Points<br><span style="font-size:12px; font-weight:400">Buddy Pass members cannot have a Points balance greater than 250.</span></label>
										<input type="number" inputmode="numeric" class="form-control required" step="1" id="pointsToSend" placeholder="10">
									</div>
									
									<div class="form-group">
										<label></label>
										<button class="btn btn-success" id="sendPoints"><i class="fa fa-sync fa-spin"></i> Send Points</button>
									</div>
									<p style="font-size:12px">There is a 2% processing fee, which will be deducted from the total amount of Points sent. This transaction will be labeled as a taxable event in our system. Only send Points to someone you know personally. This transaction is non-reversible.</p>
								@else

									<h3 style="text-align:center; margin-top: 2em;">You need to upgrade your account to send points.</h3>
									<p style="text-align:center;">Send an email to admin@venti.co to upgrade.</p>

								@endif

							</div>
							<div id="pills-share-code" class="tab-pane fade" style="min-height:455px;">
								<p>Share this link with a friend, and you both will be credited $10 in points onto your Boarding Passes when they make their first deposit:</p>
								<p id="sharing-loader" style="display:block; width:100%; text-align:center;">
								<i style="font-size: 200%;" class="fa fa-sync fa-spin text-large text-dark"></i>
								</p>
								<div class="btn-group sharing-link hide">
									<span class="form-control" id="link" style="background: whitesmoke;min-width: 250px; padding:10px; border-color:transparent;"></span></span>
									<button class="btn btn-black" id="copyLink"><i class="fa fa-regular fa-copy txt-light text-light txt-white"></i></button>
								</div>
						  	</div>
						</div>
					</div>
				</div>
			</div>
	    </div>
	</div>
</div>