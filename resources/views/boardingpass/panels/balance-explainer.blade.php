<div class="modal fade" id="balanceExplainerModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true" style="background-image: linear-gradient(112.1deg, rgba(28, 187, 156,0.4) 11.4%, rgba(78, 199, 171, 0.8) 70.2%);">
	<div class="modal-dialog modal-dialog-centered">
	    <div class="modal-content" >
			<div class="modal-header" style="border-bottom:0; padding-bottom:0;">
				<h5 style="padding:0; margin:0">Cash Balance</h5>
				<button type="button" class="btn-close text-white" data-bs-dismiss="modal" aria-label="Close" style=""></button>
			</div>
		    <div class="modal-body">
		    	@php
		    		$subscription = strtoupper($user->customClaims["subscription"] ?? "BUDDY");
                	$depositCap = number_format(env("VENTI_" . $subscription . "_DEPOSIT_CAP"),0);
		    	@endphp
				<p style="">The maximum Cash Balance you can hold is ${{ $depositCap }}. Your balance is available for withdrawal at any time but does not include deposits and withdrawals that have yet to clear.</p>
				<p>It may take up to one business day before your Cash Balance reflects transactions with your bank. All payments are made over the ACH banking network.</p>
			</div>
	    </div>
	</div>
</div>