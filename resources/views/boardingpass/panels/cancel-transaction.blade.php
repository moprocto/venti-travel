<div class="modal fade" id="cancelTransactionModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true" style="background-image: linear-gradient(112.1deg, rgba(125, 110, 112,0.5) 11.4%, rgba(152, 135, 125,0.8) 70.2%); ">
	  <div class="modal-dialog modal-dialog-centered">
	    <div class="modal-content">
	      <div class="modal-header" style="position: absolute;
	    right: 0;
	    top: 10px; border-bottom: transparent; z-index: 99;">
	        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
	      </div>
	      <div class="modal-body">
			<form>
				@CSRF
				<div class="row">
		            <div class="col-md-12 mb-3 text-center" style="flex-direction: row;
		              display: flex;
		              padding-top: 20px;
		              padding-bottom: 20px;
		              justify-content: space-between;
		              align-items: center;
		              width: 300px;
		              margin: 0 auto;">
		              <div>
		                <img src="/assets/img/icons/bank.png" style="width: 100%; max-width:85px;">
		              </div>
		              <div>
		                <i class="fa fa-times" style="font-size:36px;"></i>
		              </div>
		              <div>
		                <img src="/assets/img/icons/bank.png" style="width: 100%; max-width:85px;">
		              </div>
		            </div>
		          </div>
				
			</form>
			</div>
	    </div>
	</div>
</div>