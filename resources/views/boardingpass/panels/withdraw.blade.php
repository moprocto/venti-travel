<div class="modal fade" id="withdrawFunds" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true" style="background-image: linear-gradient(112.1deg, rgba(125, 110, 112,0.5) 11.4%, rgba(152, 135, 125,0.8) 70.2%); ">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header" style="position: absolute;
    right: 0;
    top: 10px; border-bottom: transparent; z-index: 99;">
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <form id="withdrawForm">
          <div class="row">
            <div class="col-md-12 mb-3 text-center" style="flex-direction: row;
              display: flex;
              padding-top: 20px;
              padding-bottom: 20px;
              justify-content: space-between;
              align-items: center;
              width: 300px;
              margin: 0 auto;">
              <div>
                <img src="/assets/img/icons/boarding-pass.png" style="width: 100%; max-width:85px;">
              </div>
              <div>
                <i class="fa fa-arrow-right" style="font-size:36px;"></i>
              </div>
              <div>
                <img src="/assets/img/icons/bank.png" style="width: 100%; max-width:85px;">
              </div>
            </div>
          </div>
          <div class="row mb-3">
            <div class="col-md-12">
              <label class="form-label">Amount to Withdraw (Max: $ <span class="max-withdraw-amount"></span>)<br><span style="font-size:12px; font-weight:400">Transfers are limited to $10,000 per week.</span></label>
              <div class="input-group">
                <span class="btn btn-white" style="border:1px solid #ced4da"><i class="fa fa-dollar" style="padding-top:8px"></i></span>
                <input type="number" id="withdrawAmount" name="withdrawAmount" class="form-control required" placeholder="10" min="0" max="0" maxlength="5" inputmode="numeric" oninput="this.value=this.value.slice(0,this.maxLength)">
                <button class="btn btn-black" id="withdrawMaxAmount" type="button">MAX</button>
              </div>
            </div>
          </div>
          <div class="row mb-3">
            <div class="col-md-12">
              <label class="form-label">Destination</label>
              <select class="form-control required" id="withdrawSource" required="">
                <option value="">---Please Select---</option>
                @foreach($fundingSources as $fundingSource)
                  @if(!$fundingSource->removed &&
                  isset($fundingSource->bankAccountType) &&
                  isset($fundingSource->id) &&
                  $fundingSource->status == "verified")
                  <option value="{{ $fundingSource->id }}">{{ $fundingSource->name }} | {{ ucfirst($fundingSource->bankAccountType) }}</option>
                  @endif
                @endforeach
              </select>
            </div>
          </div>
          <div class="row mb-3">
              <div class="col-md-12">
                <label class="form-label">Select Transaction Speed</label>
                <select class="form-control required" id="withdrawSpeed" name="withdrawSpeed" required="">
                  <option value="standard" selected="">Standard ACH (Free)</option>
                  <!--
                  <option value="next-available">Expedited ACH ($2 Fee from Boarding Pass)</option>
                  -->
                </select>
              </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <span style="display:block; width:100%; margin-bottom: 10px;">
                We estimate your funds to arrive <span id="withdrawFundEstimate" style="">in 3 to 5 business days</span>.<sup>*</sup>
              </span>
              <button class="btn btn-success" type="button" id="finalizeWithdraw"><i class="fa fa-sync fa-spin"></i> Send <span id="withdrawFundAmount">Funds</span> to My Bank</button>
              <p style="font-size:11px;margin-top:10px;"><strong style="font-weight:500">Fees are collected from funds already on your Boarding Pass.</strong> By submitting this withdraw request, you reaffirm your agreement to Venti Financial's Terms & Conditions. Transaction speeds vary by bank and are not guaranteed. Your withdraw amount may vary due to pending transactions on your account.</p>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>