<div class="tab-content pt-3" id="pills-tabContent">
	<div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
		<ul class="nav nav-pills nav-fill flex d-flex mb-3 nav-justified">
		  <li class="nav-item">
		    <a class="nav-link active  text-center" aria-current="page" href="#pills-all-banks" data-bs-toggle="pill" data-bs-target="#pills-all-banks" type="button" role="tab" aria-controls="pills-all-banks">Bank Accounts</a>
		  </li>
		  <li class="nav-item">
		    <a class="nav-link text-center" aria-current="page" href="#pills-all-cards" data-bs-toggle="pill" data-bs-target="#pills-all-cards" type="button" role="tab" aria-controls="pills-all-cards">Cards</a>
		  </li>
		</ul>
		<div class="tab-content">
			<div id="pills-all-banks" class="tab-pane fade in active show" role="tabpanel">
				@include('boardingpass.panels.funding.banks')
			</div>
			<div id="pills-all-cards" class="tab-pane fade in" role="tabpanel">
				@include('boardingpass.panels.funding.cards')
			</div>
		</div>
	</div>
</div>

