<style>ul.fee-breakdown{list-style: none;} span.dollar{margin-right: 20px; text-align: left;} span.amount{margin-left: 20px; text-align: right;}</style>
<div class="card bg-white small-shadow">
	<div class="card-body">
			<table class="table table-striped data-table" id="transactionsTable" style="width:100% !important">
				<thead>
					<tr>
						<th>Description</th>
						<th>Type</th>
						<th>From</th>
						<th>To</th>
						<th>Amount</th>
						<th>Date</th>
						<th>Status</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
				</tbody>
				<tfoot>
					<tr>
						<th></th>
						<th>Type</th>
						<th>From</th>
						<th>To</th>
						<th></th>
						<th></th>
						<th>Status</th>
						<th></th>
					</tr>
				</tfoot>
			</table>
	</div>
</div>