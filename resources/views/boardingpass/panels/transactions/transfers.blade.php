<div class="card bg-white small-shadow">
	<div class="card-body">
		@if(sizeof($transactions) == 0)
			There are no orders yet for your account
		@else
			<table class="no-wrap">
				<thead>
					<tr>
						<th>Description</th>
						<th>From</th>
						<th>To</th>
						<th>Amount</th>
						<th>Date</th>
						<th>Status</th>
					</tr>
				</thead>
				<tbody>
					@foreach($transactions as $transaction)
						@if($transaction["type"] == "withdraw" || $transaction["type"] == "deposit" || $transaction["type"] == "withdraw-canceled" || $transaction["type"] == "deposit-canceled")
							<tr>
								<td class="text-left">
									{{ $transaction["note"] ?? "" }}<br>
									ID: {{ $transaction["transactionID"] ?? "" }}<br>
									Type: ACH {{ ucfirst($transaction["type"] ?? "n/a") }}
								</td>
								<td>{{ $transaction["from"] ?? "" }}</td>
								<td>{{ $transaction["to"] ?? "" }}</td>
								<td>${{ $transaction["total"]}}</td>
								<td class="text-left">
									<span></span>
									{{ Carbon\Carbon::parse($transaction["timestamp"])->timezone($user->customClaims["timezone"])->format("M. d, Y") }}<br>
										{{ Carbon\Carbon::parse($transaction["timestamp"])->timezone($user->customClaims["timezone"])->format("h:i A") }}
								</td>
								<td><span>{{ ucfirst($transaction["status"]) }}</span></td>
							</tr>
						@endif
					@endforeach
				</tbody>
			</table>
		@endif
	</div>
</div>