@php
	$line = 1;
@endphp
<html>
	<head>
		<title>Venti Statement of Account for {{ $month . ", " . $year }}</title>
		<link rel="stylesheet" href="/assets/css/bootstrap.min.css">
		<link rel="icon" href="/assets/img/venti-financial-logo-50.png" sizes="32x32" />
		<link rel="icon" href="/assets/img/venti-financial-logo-192.png" sizes="192x192" />
		<style type="text/css">
			.text-right{
				text-align: right;
			}
			td{
				font-size: 11px;
			}
			@media print {
			    .pagebreak { page-break-after: always; } /* page-break-after works, as well */
			}
		</style>
	</head>
	<body>
		<div class="container">
			<div class="row" style="padding-top:50px">
				<div class="col-md-8">
					<img src="https://venti.co/assets/img/venti-trademark-logo-black.png" style="width:100px;">
						<ul style="list-style:none; font-size: 12px; padding:0;padding-top:10px">
							<li>Venti Financial, Inc.</li>
							<li>800 N King Street</li>
							<li>Suite 304 1552</li>
							<li>Wilmington, DE 19801</li>
						</ul>
				</div>
				<div class="col-md-4">
					<h5 style="padding-bottom: 0; margin-bottom:0;padding-top: 44px">Statement of Account</h5>
					<p style="font-size: 12px;">For: {{ cleanName(Session::get('user')->displayName) }} <br>Statement Period: {{ $month . ", " . $year }}</p>
					<br>
				</div>
			</div>
			<div class="row" style="padding-top:10px; padding-bottom:10px">
				<div class="col-md-6 text-left">
					<strong><p>Net Cash Change: ${{ $transactionValue }}</strong></p>
				</div>
				<div class="col-md-6 text-left">
					<strong><p>Net Points Change: {{ $pointsValue }}</strong></p>
				</div>
			</div>
			
			<table class="table">
				<thead>
					<th style="width:14%">Date</th>
					<th>Description</th>
					<th>From</th>
					<th>To</th>
					<th style="width:5%">Total</th>
				</thead>
				<tbody>

					@foreach($transactions as $transaction)
						@php
							$dollar = "$";
							if(str_contains($transaction["type"], "points")){
								$dollar="";
							}
							$minus = "-";
							if(!str_contains($transaction["type"], "withdraw")){
								$minus = "";
							}
							$decimals = 3;
							if($dollar == "$"){
								$decimals = 2;
							}
						@endphp
						<tr class="@if($line == 17 || $line == 40) pagebreak @endif">
							<td style="width:14%">{{ \Carbon\Carbon::parse($transaction["date"])->format("Y-m-d") }}<!-- <br>{{ ucfirst($transaction["status"]) }} --></td>
							<td class="text-left">{{ ucfirst(str_replace("-"," ",$transaction["type"])) }}<br>{{ $transaction["note"] }}</td>
							<td>{{ $transaction["from"] }}</td>
							<td>{{ $transaction["to"] }}</td>
							<td class="text-right">{{ $minus . $dollar. number_format($transaction["total"], $decimals) }}</td>
						</tr>
						@php $line++; @endphp
					@endforeach
				</tbody>
			</table>
			<p style="padding-top:100px;">Points are rounded to the nearest third decimal place.</p>
			<h5>Think You Found a Mistake on Your Statement?</h5>
			<br>
			<p>You can send a letter to Venti Financial, Inc. 800 N King Street Suite 304 1552, Wilmington, DE 19801. You may also contact us at <a href="https://venti.co">venti.co</a> or send an email to admin@venti.co</p>
			<div class="col-md-8" style="padding-bottom:50px;">
				<img src="https://venti.co/assets/img/venti-trademark-logo-black.png" style="width:100px;">
			</div>
		</div>
	</body>
</html>
