<div class="modal fade" id="receiptModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true" style="background-image: linear-gradient(112.1deg, rgba(125, 110, 112,0.5) 11.4%, rgba(152, 135, 125,0.8) 70.2%); ">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" style="position: absolute;
    right: 0;
    top: 10px; border-bottom: transparent; z-index: 99;">
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <img src="/assets/img/venti-trademark-logo-black.png" style="width:150px; margin:50px auto 50px auto; display: block;">
        <br>
        <p>
          Transaction ID: <span id="receipt-transaction-id"></span><br>
          Date: <span id="receipt-transaction-date"></span><br>
          Status: <span id="receipt-transaction-status"></span><br>
          From: <span id="receipt-transaction-from"></span><br>
          To: <span id="receipt-transaction-to"></span><br>
          Type: <span id="receipt-transaction-type"></span><br>
        </p>
        <table class="table">
          <thead></thead>
          <tbody>
            <tr>
              <td id="receipt-transaction-note" class="text-left"></td>
              <td id="receipt-transaction-amount" class="text-right"></td>
            </tr>
            <tr>
              <td class="text-left">Fee:</td>
              <td id="receipt-transaction-fee" class="text-right"></td>
            </tr>
            <tr>
              <td class="text-right text-bold">Total:</td>
              <td id="receipt-transaction-total" class="text-right text-bold"></td>
            </tr>
          </tbody>
        </table>
        <br>
        <p id="fee-disclaimer"></p>

        <div class="row mb-3">
          <br>
          <p id="can-cancel" class="hidden">A bank transfer is cancellable up until 5 p.m. EST on that same business day if the transfer was initiated prior to 5 p.m. EST. If a transfer was initiated after 5 p.m. EST, it can be cancelled before 5 p.m. EST on the following business day.
          <br><br>
          <button type="button" id="cancelTransactionConfirm" class="btn btn-black"><i class="fa fa-sync fa-spin"></i> Confirm Cancelation</button>
          </p>
          <p id="cannot-cancel" class="hidden">The cancellation window for this transfer has expired. A bank transfer is cancellable up until 5 pm EST on that same business day if the transfer was initiated prior to 5 pm EST. If a transfer was initiated after 5 pm EST, it can be cancelled before 5 p.m. EST on the following business day. If an expedited transfer was requested, it can only be canceled the same day business day it was requested and prior to 5 p.m. EST.
            <br><br>
          </p>
          <p class="text-small">Transaction fees are non-refundable.</p>
        </div>
      </div>
    </div>
  </div>
</div>