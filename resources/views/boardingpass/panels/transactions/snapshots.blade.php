<style>ul.fee-breakdown{list-style: none;} span.dollar{margin-right: 20px; text-align: left;} span.amount{margin-left: 20px; text-align: right;}</style>
<div class="card bg-white small-shadow">
	<div class="card-body">
		<table class="table table-striped data-table no-wrap" id="snapshotsTable" style="width:100% !important">
			<thead>
				<tr>
					<th>Description</th>
					<th>Cash Balance</th>
					<th>Points Balance</th>
					<th>Date</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
</div>