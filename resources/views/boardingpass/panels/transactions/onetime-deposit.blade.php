<form id="depositForm">
  <div class="row">
    <div class="col-md-12 mb-3 text-center" style="flex-direction: row;
      display: flex;
      padding-top: 20px;
      padding-bottom: 20px;
      justify-content: space-between;
      align-items: center;
      width: 300px;
      margin: 0 auto;">
      <div>
        <img src="/assets/img/icons/bank.png" style="width: 100%; max-width:85px;">
      </div>
      <div>
        <img src="/assets/img/icons/right-arrow.png" style="width: 100%; max-width:36px;">
      </div>
      <div>
        <img src="/assets/img/icons/boarding-pass.png" style="width: 100%; max-width:85px;">
      </div>
    </div>
  </div>
  <div class="row mb-3">
    <div class="col-md-12">
      @php
        $subscription = strtoupper($user->customClaims["subscription"] ?? "BUDDY");
        $depositCap = number_format(env("VENTI_" . $subscription . "_DEPOSIT_CAP"),0);
      @endphp
      <label class="form-label">Amount to Deposit (Max: $7,500)<br><span style="font-size:12px; font-weight:300">Cash Balance after deposit cannot exceed ${{ $depositCap }}. Transfers are limited to $10,000 per week.</span></label>
      <div class="input-group">
        <span class="btn btn-white" style="border:1px solid #ced4da"><i class="fa fa-dollar" style="padding-top:8px"></i></span>
        <input type="number" id="depositAmount" name="depositAmount" class="form-control required" placeholder="10" max="5000" maxlength="4" inputmode="numeric" oninput="this.value=this.value.slice(0,this.maxLength)">
      </div>
    </div>
  </div>
  <div class="row mb-3">
    <div class="col-md-12">
      <label class="form-label">Select Funding Source</label>
      <select class="form-control required" id="depositSource">
        <option value="">---Please Select---</option>
        @foreach($fundingSources as $fundingSource)
          @if(!$fundingSource->removed && 
          isset($fundingSource->bankAccountType) && 
          isset($fundingSource->id) &&
          $fundingSource->status == "verified"
          )
          <option value="{{ $fundingSource->id }}">{{ $fundingSource->name }} | {{ ucfirst($fundingSource->bankAccountType) }}</option>
          @endif
        @endforeach
      </select>
    </div>
  </div>
  <div class="row mb-3">
      <div class="col-md-12">
        <label class="form-label">Select Transaction Speed</label>
        <select class="form-control required" id="depositSpeed" name="depositSpeed">
          <option value="standard" selected="">Standard ACH (Free)</option>
          <!--
          <option value="next-available">Expedited ACH ($2 Fee from Boarding Pass)</option>
          -->
        </select>
      </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <span style="display:block; width:100%; margin-bottom: 10px;">
        We estimate your funds to arrive <span id="fundEstimate" style="">in 3 to 5 business days</span>.<sup>*</sup>
      </span>
      <button class="btn btn-success" type="button" id="finalizeDeposit"><i class="fa fa-sync fa-spin"></i> Deposit <span id="fundAmount">Funds</span> From My Bank</button>
      <p style="font-size:11px;margin-top:10px;"><strong style="font-weight:500">Fees are collected from funds already on your Boarding Pass.</strong> By submitting this deposit request, you reaffirm your agreement to Venti Financial's Terms &amp; Conditions, which includes a $25 fee for any returned ACH requests due to insufficient funds or unauthorized returns. Transaction speeds vary by bank and are not guaranteed.</p>
    </div>
  </div>
</form>