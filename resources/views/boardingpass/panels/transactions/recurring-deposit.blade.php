<div class="row" id="recurringDepositEnabled">
  <div class="col-md-12">
    <div class="alert alert-info">
      <div>
        <p id="recurringSummary"></p>
        <button class="btn btn-white text-small" id="deleteRecurring" data-autosave-id="" style="min-width">DELETE <i class="fa fa-times"></i></button>
      </div>
    </div>
  </div>
</div>
<form id="recurringDepositForm">
  <input type="hidden" id="recurringDepositSpeed" value="standard">
  
  <div class="row">
    <div class="col-md-12 mb-3 text-center" style="flex-direction: row;
      display: flex;
      padding-top: 20px;
      padding-bottom: 20px;
      justify-content: space-between;
      align-items: center;
      width: 300px;
      margin: 0 auto;">
      <div>
        <img src="/assets/img/icons/bank.png" style="width: 100%; max-width:85px;">
      </div>
      <div>
        <img src="/assets/img/icons/recurring.png" style="width: 100%; max-width:36px;">
      </div>
      <div>
        <img src="/assets/img/icons/boarding-pass.png" style="width: 100%; max-width:85px;">
      </div>
    </div>
  </div>
  <div class="row mb-3">
    <div class="col-md-12">
      @php
        $subscription = strtoupper($user->customClaims["subscription"] ?? "BUDDY");
        $depositCap = number_format(env("VENTI_" . $subscription . "_DEPOSIT_CAP"),0);
      @endphp
      <label class="form-label">Amount to Deposit (Min: $25, Max: $7,500)<br><span style="font-size:12px; font-weight:300">Cash Balance after deposit cannot exceed ${{ $depositCap }}.</span></label>
      <div class="input-group">
        <span class="btn btn-white" style="border:1px solid #ced4da"><i class="fa fa-dollar" style="padding-top:8px"></i></span>
        <input type="number" id="recurringDepositAmount" name="depositAmount" class="form-control required" placeholder="25" min="" max="7500" maxlength="4" inputmode="numeric" oninput="this.value=this.value.slice(0,this.maxLength)">
      </div>
    </div>
  </div>
  <div class="row mb-3">
    <div class="col-md-12">
      <label class="form-label">Select Funding Source</label>
      <select class="form-control required" id="recurringDepositSource">
        <option value="">---Please Select---</option>
        @foreach($fundingSources as $fundingSource)
          @if(!$fundingSource->removed && 
          isset($fundingSource->bankAccountType) && 
          isset($fundingSource->id) &&
          $fundingSource->status == "verified"
          )
          <option value="{{ $fundingSource->id }}" data-name="{{ $fundingSource->name }}">{{ $fundingSource->name }} | {{ ucfirst($fundingSource->bankAccountType) }}</option>
          @endif
        @endforeach
      </select>
    </div>
  </div>
  <div class="row mb-3">
      <div class="col-md-12">
        <label class="form-label">Select Transaction Frequency<br></label>
        <select id="depositFrequency" class="form-control required" required="">
          <option value="everyWeek" data-helper-text="every week">Every Week</option>
          <option value="biWeekly" data-helper-text="every other Friday">Every Other Friday</option>
          <option value="semiMonthly" data-helper-text="every 15th and last day of each month">Semi Monthly</option>
          <option value="monthly" data-helper-text="every month" selected="">Monthly</option>
        </select>
      </div>
  </div>
  <div class="row mb-3">
      <div class="col-md-12">
        <label class="form-label">Select Deposit Start Date<br></label>
        <input type="date" id="depositStartDate"  class="form-control required" required="" max="30">
      </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <span id="depositReward"></span>
      <span id="depositSummary" class="d-block mb-3" style="font-size:11px"></span>
      <button class="btn btn-success" type="button" id="finalizeRecurringDeposit"><i class="fa fa-sync fa-spin"></i> Create Recurring Deposit of <span id="fundAmountRecurring">Funds</span></button>
      <p style="font-size:11px;margin-top:10px;"><strong style="font-weight:500">Fees are collected from funds already on your Boarding Pass.</strong> By submitting this deposit request, you reaffirm your agreement to Venti Financial's Terms &amp; Conditions, which includes a $25 fee for any returned ACH requests due to insufficient funds or unauthorized returns. Transaction speeds vary by bank and are not guaranteed.</p>
    </div>
  </div>
</form>