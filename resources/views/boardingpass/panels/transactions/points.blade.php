<div class="card bg-white small-shadow">
	<div class="card-body">
		@if(sizeof($transactions) == 0)
			There are no orders yet for your account
		@else
			<table class="table data-table">
				<thead>
					<tr>
						<th>Description</th>
						<th>From</th>
						<th>To</th>
						<th>Amount</th>
						<th>Date</th>
						<th>Status</th>
					</tr>
				</thead>
				<tbody>
					@foreach($transactions as $transaction)
						@if($transaction["type"] == "points-deposit" || $transaction["type"] == "points-withdraw" || $transaction["type"] == "points-refund")
							<tr>
								<td class="text-left">
									{{ $transaction["note"] ?? "" }}<br>
									ID: {{ $transaction["transactionID"] ?? "" }}<br>
									Type: {{ ucfirst($transaction["type"] ?? "n/a") }}
								</td>
								<td>{{ $transaction["from"] ?? "" }}</td>
								<td>{{ $transaction["to"] ?? "" }}</td>
								<td>${{ $transaction["total"]}}</td>
								<td>{{ Carbon\Carbon::parse($transaction["timestamp"])->timezone($user->customClaims["timezone"])->format("M d, Y H:i:s g") }}</td>
								<td>{{ ucfirst($transaction["status"]) }}</td>
							</tr>
						@endif
					@endforeach
				</tbody>
			</table>
		@endif
	</div>
</div>