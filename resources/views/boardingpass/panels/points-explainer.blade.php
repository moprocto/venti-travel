<div class="modal fade" id="pointsExplainerModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true" style="background-image: linear-gradient(112.1deg, rgba(253, 118, 78,0.4) 11.4%, rgba(254, 174, 27,0.7) 70.2%);">
	<div class="modal-dialog modal-dialog-centered">
	    <div class="modal-content" >
			<div class="modal-header" style="border-bottom:0; padding-bottom:0">
				<h5 style="padding:0; margin:0">Boarding Pass Points</h5>
				<button type="button" class="btn-close text-white" data-bs-dismiss="modal" aria-label="Close" style=""></button>
			</div>
		    <div class="modal-body">
		    	@php
		    		$subscription = strtoupper($user->customClaims["subscription"] ?? "BUDDY");
		    		$subscriptionCapitalized = ucfirst($user->customClaims["subscription"] ?? "Buddy");
                	$APYrate = number_format(env("DWOLLA_APY_" . $subscription),2);
		    	@endphp
		    	<p style="font-weight: 500;">1 Point = $1 Toward Travel</p>
		    	@if($subscription != "BUDDY")
		    		<p style="">Points are awarded on the 1<sup>st</sup> day of each month based on your average Spending Power of the prior month. Your Cash Balance must be greater than $0 to be eligible for interest disbursement. {{ $subscriptionCapitalized }} Class members currently earn Points at a rate of {{ $APYrate }}% APY.</p>
					<p style="">Points are transferrable, but can only be sent to verified Venti members. Points are not currently redeemable for cash and can only be used towards purchases on the Venti platform.</p>
				@else
					<p>Buddy Pass holders can receive Points from Boarding Pass members but cannot earn or send Points.</p>
		    	@endif
			</div>
	    </div>
	</div>
</div>