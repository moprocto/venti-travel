<div class="modal fade" id="verifyFunds" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true" style="background-image: linear-gradient(112.1deg, rgba(125, 110, 112,0.5) 11.4%, rgba(152, 135, 125,0.8) 70.2%); ">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header" style="position: absolute;
    right: 0;
    top: 10px; border-bottom: transparent; z-index: 99;">
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <form id="depositVerifyForm">
          <input type="hidden" name="" id="verifyingBank">
          <div class="row">
            <div class="col-md-12 mb-3 text-center" style="flex-direction: row;
              display: flex;
              padding-top: 10px;
              padding-bottom: 10px;
              justify-content: center;
              align-items: center;
              width: 100%;
              margin: 0 auto;">
              <div>
                <img src="/assets/img/icons/bank.png" style="width: 100%; max-width:85px;">
                <p style="font-size: 24px;
    font-weight: 400;
    padding: 0;
    margin: 0;" id="bankName"></p>
              </div>
            </div>
          </div>
          <div class="row mb-3">
            <div class="col-md-12">
              <label class="form-label">First Deposit<br><span style="font-size:12px; font-weight:300">Enter a number between 1 and 20 (01 = 1 cent)</span></label>
              <div class="input-group">
                <span class="btn btn-white" style="pointer-events: none; border:1px solid #ced4da"><i class="fa fa-dollar" style="padding-top:8px"></i>0.</span>
                <input type="number" id="firstDeposit" name="firstDeposit" class="form-control required" placeholder="01" max="20" maxlength="2" inputmode="numeric" oninput="this.value=this.value.slice(0,this.maxLength)">
              </div>
            </div>
          </div>
          <div class="row mb-3">
            <div class="col-md-12">
              <label class="form-label">Second Deposit<br><span style="font-size:12px; font-weight:300">Enter a number between 1 and 20 (20 = 20 cents)</span></label>
              <div class="input-group">
                <span class="btn btn-white" style="pointer-events: none; border:1px solid #ced4da"><i class="fa fa-dollar" style="padding-top:8px"></i>0.</span>
                <input type="number" id="secondDeposit" name="secondDeposit" class="form-control required" placeholder="20" max="20" maxlength="2" inputmode="numeric" oninput="this.value=this.value.slice(0,this.maxLength)">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <span style="display:block; width:100%; margin-bottom: 10px;">
                Please check your bank account to confirm you have received the deposits before submitting this verification request. You are limited to three attempts. After the third attempt, this funding source will be permanently blocked. It is against our Terms of Use to use a bank account that does not belong to you.
              </span>
              <button class="btn btn-success" type="button" id="depositVerification"><i class="fa fa-sync fa-spin"></i> Verify Bank</button>
              <p style="font-size:11px;margin-top:10px;"></p>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>