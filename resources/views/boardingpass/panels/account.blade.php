<div class="tab-content pt-3" id="pills-tabContent">
	<div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">

		<div class="col-md-12">
			<div class="card bg-white small-shadow">
				<div class="card-body">
					Please contact us at <a href="mailto:admin@venti.co">admin@venti.co</a> if you need to change your legal name, email, or phone number, as we are required re-verify your account. 
				</div>
			</div>
		</div>
		<div class="col-md-12">
			<h5 class="mt-4">Membership: {{ ucfirst($subscription) }}</h5>
			@if($subscription == "first")
				<div class="card bg-white small-shadow">
					<div class="card-body">
						First Class members enjoy our highest deposit limit of $100,000 and APY of 9%. To downgrade, contact admin@venti.co.
					</div>
				</div>
			@endif
			@if($subscription == "buddy")
				<div class="card bg-white small-shadow">
					<div class="card-body">
						Buddy Pass members cannot earn points. Upgrade your account by selecing one of the options below
						<div class="row mt-4">
							
								<div class="col-md-4 text-center">
									<a href="/home/upgrade/priority" class="btn btn-white border-primary">Upgrade to Priority</a>
									<br>
									<p>Earn Points at 9% APY<br>Max Deposit: $7,500</p>
								</div>
								<!--
								<div class="col-md-4 text-center">
									<a href="/home/upgrade/business" class="btn btn-white border-primary">Upgrade to Business Class</a>
									<br>
									<p>Earn Points at 7% APY<br>Max Deposit: $50,000</p>
								</div>
							-->
							<div class="col-md-4 text-center">
								<a href="/home/upgrade/first" class="btn btn-white border-primary">Upgrade to First Class</a>
								<br>
								<p>Earn Points at 9% APY<br>Max Deposit: $250,000</p>
							</div>
						</div>
					</div>
				</div>
			@endif
		</div>
		<div class="col-md-12">
			<h5 class="mt-4">My Information</h5>
			<div class="card bg-white small-shadow">
				<div class="card-body">
					<div class="row">
						<div class="col-md-4">
							<div class="form-group">
								<label class="form-label">First Name:</label>
								<input type="email" name="fname" class="form-control disabled" value="{{ getFirstName($user->displayName) }}">
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label class="form-label">Middle Name<br></label>
								<input type="text" name="mname" class="form-control disabled" value="{{ $user->customClaims['middleName'] ?? '' }}">
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label class="form-label">Last Name:</label>
								<input type="email" name="lname" class="form-control disabled" value="{{ getLastName($user->displayName) }}">
							</div>
						</div>
						<div class="col-md-6">
							@php
								$regions = array(
							    'Africa' => DateTimeZone::AFRICA,
							    'America' => DateTimeZone::AMERICA,
							    'Antarctica' => DateTimeZone::ANTARCTICA,
							    'Aisa' => DateTimeZone::ASIA,
							    'Atlantic' => DateTimeZone::ATLANTIC,
							    'Europe' => DateTimeZone::EUROPE,
							    'Indian' => DateTimeZone::INDIAN,
							    'Pacific' => DateTimeZone::PACIFIC
							);

							$timezones = array();
							foreach ($regions as $name => $mask)
							{

							    $zones = DateTimeZone::listIdentifiers($mask);
							    foreach($zones as $timezone)
							    {
									// Lets sample the time there right now
									$time = new DateTime(NULL, new DateTimeZone($timezone));

									// Us dumb Americans can't handle millitary time
									$ampm = $time->format('H') > 12 ? ' '. $time->format('g:i a'). '' : $time->format('g:i a');

									// Remove region name and add a sample time

									$timezones[$name][$timezone] = substr($timezone, strlen($name) + 1) . ' - ' . $ampm;
								}
							}
							@endphp


							<label class="form-label">My Timezone</label>
							<p>Venti transactions are recorded in U.S. New York EST/EDT. Changing your preferred timezone will only change how transaction data is displayed relative to you.</p>
							<select id="timezone" class="form-control select2">
								@foreach($timezones as $region => $list)
									<optgroup label="{{ $region }}">
										@foreach($list as $timezone => $name)
											@php
												$mytime = $user->customClaims['timezone'] ?? false;
											@endphp
											<option value="{{ $timezone }}" @if($mytime == $timezone) selected @endif>{{ str_replace("_"," ",$name) }}</option>
										@endforeach
									</optgroup>
								@endforeach
							</select>
						</div>
					</div>
				</div>
			</div>
			<br>
			<button class="btn btn-success" type="button" id="updateAccountButton"><i class="fa fa-sync fa-spin"></i> Update Account</button>
			<br>
		</div>

		<div class="col-md-12">
			<h5 class="mb-0 mt-4">Account Security</h5>
			<div class="card bg-white small-shadow mt-3">
				<div class="card-body">
					<div class="form-group">
						<label class="form-label">Email:</label>
						<input type="email" name="email" class="form-control disabled" value="{{ $user->email }}">
					</div>
					<div class="form-group">
						<label class="form-label">My Cell Phone:</label>
						<input type="tel" name="email" class="form-control disabled" value="{{ $user->customClaims['phoneNumber'] ?? '' }}">
					</div>
					<div class="form-group">
						<label class="form-label">Update Password:<br><span class="d-block text-small">Use the button below to request a password reset.</span></label>
						<br>
						<button class="btn btn-black" id="requestNewPassword">Password Change</button>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-12">
			<h5 class="mb-0 mt-4">Email Marketing Preferences</h5>
			<span>Your selection will not impact your ability to receive transactional emails from Venti Financial or Dwolla.</span>
			<div class="card bg-white small-shadow mt-3">
				<div class="card-body">
					<div class="form-group text-bold align-center d-md-flex d-sm-block" style="align-items:center;">
						<div style="width: 10%;">
							<input type="checkbox" class="js-switch" id="tag_blogs" />
						</div>
						<span style="font-weight:500; width: 30%;">Blog Posts:</span>
						<span class="text-small" style="width:60%">From travel hacks, saving tips, economic news, and how-to guides, we'll email you when we have new articles to share that will help you make the best of Venti, your upcoming trips, and destinations of interest.</span>
					</div>
					<div class="form-group text-bold align-center d-md-flex d-sm-block" style="align-items:center;">
						<div style="width: 10%;"/>
							<input type="checkbox" class="js-switch" id="tag_surveys" >
						</div>
						<span style="font-weight:500;  width: 30%;">Survey Opportunities:</span>
						<span class="text-small" style="width:60%">Earn Points for completing surveys that help us deliver a better product experience. We'll notify you when your opinion is needed.</span>
					</div>
					<div class="form-group text-bold align-center d-md-flex d-sm-block" style="align-items:center;">
						<div style="width: 10%;"/>
							<input type="checkbox" class="js-switch" id="tag_promos" />
						</div>
						<span style="font-weight:500;  width: 30%;">Special Deals and Promotions:</span>
						<span class="text-small" style="width:60%">We'll keep you posted when we have special offers that will help you save money or win free flights, hotel room nights, and vacations. This includes emails that encourage you to contribute to your savings.</span>
					</div>
					<div class="form-group text-bold align-center d-md-flex d-sm-block" style="align-items:center;">
						<div style="width: 10%;"/>
							<input type="checkbox" class="js-switch" id="tag_product"/>
						</div>
						<span style="font-weight:500;  width: 30%;">Product Releases:</span>
						<span class="text-small" style="width:60%">We'll let you know when new features and products are available that can help you travel smart and save money.</span>
					</div>
				</div>
			</div>
		</div>		
		<div class="col-md-12 logoutRow mt-3 mb-3">
			<a href="{{ route('logout') }}" class="btn btn-white nav-link text-danger" style="padding-top:10px; padding-bottom:10px;">Log Out <i class="fa fa-lock"></i></a>
		</div>
		<div class="col-md-12">
			<h5 class="mb-0 mt-4">Close My Account</h5>
			<div class="card bg-white small-shadow mt-3">
				<div class="card-body">
					<!--
					<div class="form-group">
						<label class="form-label">Reason:</label>
						<select class="form-control" required="" name="reason">
							<option value="">Why are you deleting your account?</option>
						</select>
					</div>
					<div class="form-group">
						<label class="form-label">What should we do with your data?:<br><span>We cannot delete transaction records</span></label>
						<select class="form-control" required="" name="reason">
							<option value="keep">Keep my data in case I return</option>
							<option value="delete">Delete my data</option>
						</select>
					</div>
					-->
					<div class="form-group">
						<p>To proceed with account deletion, please use the button below. Our team will evaluate your account to determine if there are any outstanding orders/balances. @if($customer)<strong>You must provide your Member ID: {{ getMemberID($user->uid, $customer["customerID"]) }} </strong>@endif</p>
						<a class="btn btn-warning" type="button" href="mailto:admin@venti.co?subject=Delete My Account">Request Deletion</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>	