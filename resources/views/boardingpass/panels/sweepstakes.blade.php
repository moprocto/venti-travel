<div class="modal fade" id="sweepStakesTracker" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true" style="background-image: linear-gradient(112.1deg, rgba(125, 110, 112,0.5) 11.4%, rgba(152, 135, 125,0.8) 70.2%); ">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header" style="position: absolute;
    right: 0;
    top: 10px; border-bottom: transparent; z-index: 99;">
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <img src="/assets/img/icons/ticket.png" class="d-block" style="width:100px; margin:0 auto;">
        <h1 class="text-center"><span class="sweepstakeEntries"><i class="fa fa-sync fa-spin"></i></span> Entries</h1>
        <p>Boarding Pass members are automatically entered to win free flights, hotel room nights, and vacations. The more entries you have, the greater the chances you'll win! <a href="/sweepstakes" target="_blank">Learn More</a></p>
        <p>2024 Sweepstakes Schedule:</p>
        @include('sweepstakes.schedule')
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="leaderTracker" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true" style="background-image: linear-gradient(112.1deg, rgba(125, 110, 112,0.5) 11.4%, rgba(152, 135, 125,0.8) 70.2%); ">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header" style="position: absolute;
    right: 0;
    top: 10px; border-bottom: transparent; z-index: 99;">
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <img src="/assets/img/icons/trophy.png" class="d-block" style="width:100px; margin:0 auto; margin-bottom:20px;">
        @if($ranking != "X")
        <h1 class="text-center">You're in {{ number_format($ranking ?? 0,0) }}<sup>{{ ordinal($user->customClaims["leaderboard"] ?? 0) }}</sup> Place</h1>
        @else
          <h1 class="text-center">Get Ranked</h1>
          <p>Make a deposit to see how your savings stack up.</p>
        @endif
        <p>Our leaderboard is organized by the number of entries you have in the next sweepstakes drawing. We calculate your position nightly based on your membership tier, Cash Balance, and Points Balance.</p>
      </div>
    </div>
  </div>
</div>