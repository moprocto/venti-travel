<div class="container">
	<div class="row mb-3">
		<div class="col-md-3">
			
		</div>
		<div class="col-md-3">
			
		</div>
		<div class="col-md-3">
			
		</div>
		<div class="col-md-3">
			
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12">

			<div class="row">
				@if($customer["status"] != "unverified")
				<div class="col-xl-4 col-lg-6 col-md-12 col-sm-12 mb-4">
					<div class="card bg-white p-3 text-center" style="background-image: linear-gradient(112.1deg, rgb(126, 124, 224) 11.4%, rgb(11, 143, 251) 70.2%);">
						<button href=";javascript" type="button" data-bs-toggle="modal" data-bs-target="#spendingExplainerModal" style="background: none; border: none;">
							<span id="spending-power-balance"><i style="font-size: 200%;" class="fa fa-sync fa-spin text-large text-light"></i></span>
						</button>
					</div>
				</div>
				<div class="col-xl-4 col-lg-6 col-md-6 col-sm-6 col-xs-6 mb-4">
					<div class="card bg-white p-3 text-center" style="background-image: linear-gradient(112.1deg, rgb(96, 224, 210) 11.4%, rgb(4, 227, 151) 70.2%);">
						<button href=";javascript" type="button" data-bs-toggle="modal" data-bs-target="#balanceExplainerModal" style="background: none; border: none;">
							<span id="cash-balance"><i style="font-size: 200%;" class="fa fa-sync fa-spin text-large text-light"></i></span>
					</button>
					</div>
				</div>
				@endif
				<div class="col-xl-4 col-lg- col-md-6 col-sm-6 col-xs-6 mb-4">
					<div class="card bg-white p-3 text-center" style="background-image: linear-gradient(112.1deg, rgb(253, 118, 78) 11.4%, rgb(254, 174, 27) 70.2%);">
						<button href=";javascript" type="button" data-bs-toggle="modal" data-bs-target="#pointsExplainerModal" style="background: none; border: none;">
							<span id='points-balance'><i style="font-size: 200%;" class="fa fa-sync fa-spin text-large text-light"></i></span>
						</button>
					</div>
				</div>
				<!--
					<div class="col-md-12 mb-4">
						<div class="card bg-white p-3 mb-3 text-center">
							<div id="apex-pie-2" class="apex-charts pt-3" data-colors="#f672a7"></div>
						</div>
					</div>
				-->
			</div>
		</div>
		<div class="col-lg-12 col-md-12 col-sm-12">
			<div class="row">
				@if($transactable)
					@if($subscription != "buddy")
						<div class="col-xl-3 col-lg-12 col-md-4 col-sm-12 mb-3 withdrawButton disabled">
							<div class="card bg-white p-3 text-center">
								<span id="withdraw-balance-button" style="font-weight: 500;"><i style="font-size: 200%;" class="fa fa-sync fa-spin text-large text-dark"></i></span>
							</div>
						</div>
					@endif
					@if($subscription != "buddy")
						@include('boardingpass.panels.deposit')
						@include('boardingpass.panels.withdraw')
						@include('boardingpass.panels.warp-drive')
						<!--
						<div class="col-xl-3 col-lg-12 col-md-4 col-sm-12 mb-3 depositButton">
							<div class="card bg-white p-3 text-center">
								<a href="#" data-bs-toggle="modal" data-bs-target="#warpDriveFunds"><h1><img src="/assets/img/icons/warp-drive.png" style="width: 100%; max-width:55px;"></h1><span style="font-weight: 500;">Warp Drive</span>
								</a>
							</div>
						</div>
						-->
					@endif
				@endif
					<!--
					<div class="col-xl-3 col-lg-12 col-md-4 col-sm-12 mb-3 mysteryButton disabled">
						<div class="card bg-white p-3 text-center">
							<a href="{{ route('mystery-box-closed') }}"><h1><img src="/assets/img/icons/mystery-box-closed.png" style="width: 100%; max-width:55px;"></h1><span style="font-weight: 500;">Mystery Box</span>
							</a>
						</div>
					</div>
					-->
					<div class="col-xl-3 col-lg-12 col-md-4 col-sm-12 mb-3 depositButton">
						<div class="card bg-white p-3 text-center">
							<a href="#" data-bs-toggle="modal" data-bs-target="#referralModal"><h1><img src="/assets/img/icons/refer.png" style="width: 100%; max-width:55px;"></h1><span style="font-weight: 500;">Share</span>
							</a>
						</div>
					</div>
			</div>
		</div>
		<!--
		@if(sizeof($transactions) > 0)
			<div class="col-md-6">
				<div class="card bg-white p-3 text-center">
					<h5 style="font-size:18px;">Spending Power</h5>
					<div id="morris-bar-stacked" style="height: 350px;" class="morris-chart" data-colors="#feae1b,#60e0d2"></div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="card bg-white p-3 text-center">
					<h5 style="font-size:18px;">Recent Activity</h5>
					<table class="table">
						<thead>
							<th class="text-left">Description</th>
							<th>Date</th>
						</thead>
						<tbody>
							<tr>
								<td colspan="2"><p style="padding-top:100px; padding-bottom:100px">Nothing to show here, yet!</p></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		@endif
		-->
	</div>
</div>
