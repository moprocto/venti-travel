<div class="modal fade" id="addSourceModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true" style="background-image: linear-gradient(112.1deg, rgba(125, 110, 112,0.5) 11.4%, rgba(152, 135, 125,0.8) 70.2%); ">
	  <div class="modal-dialog modal-dialog-centered">
	    <div class="modal-content">
	      <div class="modal-header" style="position: absolute;
	    right: 0;
	    top: 10px; border-bottom: transparent; z-index: 99;">
	        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
	      </div>
	      <div class="modal-body">
	      	@if($subscription != "buddy")
				<form id="addFundingForm">
					@CSRF
					<h5 class="mb-0">Add Bank Account</h5>
					<span style="font-size:12px">Items marked with <sup>*</sup> are securely stored on Venti</span>
					<div class="card bg-white no-shadow mt-3">
						<div class="card-body p-0">
							<div class="row mb-3">
								<div class="col-md-12">
									<label class="form-label">Account Type<br></label>
									<select name="accountType" id="accountType" class="form-control required">
										<option value="">--- Please Select ---</option>
										<option value="checking">Checking</option>
										<option value="savings">Savings</option>
									</select>
								</div>
							</div>
							<div class="row mb-3">
								<div class="col-md-12">
									<label class="form-label">Name on Account <sup>*</sup><br></label>
									<input type="text" name="accountName" id="accountName" class="form-control required" value="{{ cleanName($user->displayName) }}">
								</div>
							</div>
							<div class="row mb-3">
								<div class="col-md-6">
									<label class="form-label">Account Number <sup>*</sup><br></label>
									<input type="tel" name="accountNumber" id="accountNumber" inputmode="numeric" class="form-control required" placeholder="•••••••••••">
								</div>
								<div class="col-md-6">
									<label class="form-label">Confirm Account Number<br></label>
									<input type="tel" name="accountNumberConfirm" id="accountNumberConfirm" inputmode="numeric" class="form-control required" placeholder="•••••••••••">
								</div>
							</div>
							<div class="row mb-3">
								<div class="col-md-12">
									<label class="form-label">Routing Number<br>
									<span class="text-small">Some banks have multiple routing numbers. Verify with your bank which one is tied to your account.</span>
									</label>
									<input type="tel" name="routingNumber" id="routingNumber" inputmode="numeric" class="form-control required" placeholder="•••••••••••" minlength="9" maxlength="9" oninput="this.value=this.value.slice(0,this.maxLength)">
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<label class="form-label">Account Nickname <sup>*</sup><br></label>
									<input type="text" name="accountNickname" id="accountNickname"  class="form-control required">
								</div>
							</div>
							<div class="row mb-3">
								<div class="col-md-12">
									<p style="font-size:11px;margin-top:10px;">I agree that future payments to Venti Financial will be processed by the Dwolla payment system from the selected account above. In order to cancel this authorization, I will change my payment settings within my Venti account. All bank funding sources require verification before they can be used for deposits and withdrawals. <strong>Venti reserves the right to call this bank to verify ownership. Verification failure may result in account suspension.</strong> Deposit speeds vary by bank.</p>
									<button type="button" class="btn btn-primary" style="max-width: 250px;" id="addBankAccount"><i class="fa fa-sync fa-spin"></i> Agree and Add Account</button>
								</div>
							</div>
						</div>
					</div>
				</form>
			@endif
			</div>
	    </div>
	</div>
</div>