<!--
<style>
#chatBubble{
	transition: 0.25s;
}
#chatBubble .opened{
	background: white;
	box-shadow: none;
	
}
#chatWindow, #chatWindow > *{
	height: 0;
	width: 0;
	display: none;
	transition: 0.25s;
}

#chatWindow.opened{
	display: block;
	background: white;
	transition: 0.25s;
	z-index: 999;
}

#chatWindow.opened #toolbar{
	display: flex !important;
	padding: 20px;
	width: 100%;
	align-self: flex-end;
	margin: 0 10px;
	flex-direction: row;
	justify-content: space-between;
}
#chatEntry{
	display: none;
    background: whitesmoke;
    width: 94%;
    position: absolute;
    justify-content: space-between;
    bottom: 10px;
    /* margin: 0 10px; */
    height: 43px;
    z-index: 2;
    align-items: center;
    padding: 0 9px;
    right: 0;
    margin-left: 10px;
    margin-right: 10px;
    border-radius: 10px;
    color: rgba(0, 0, 0, 0.7);
}
#chatWindow.opened #chatEntry{
	display: inline-flex !important;
}
#chatWindow.opened #chats{
	display: block;
    position: absolute;
    left: 20px;
    top: 75px;
    width: 100%;
}
.shared{
	position: relative; /* Setup a relative container for our psuedo elements */
  max-width: 255px;
  margin-bottom: 15px;
  padding: 10px 20px;
  line-height: 24px;
  word-wrap: break-word; /* Make sure the text wraps to multiple lines if long */
  border-radius: 25px;

}
.shared:before{
	width: 20px;
}
.shared:after{
	width: 26px;
    background-color: whitesmoke; /* All tails have the same bg cutout */
    position: absolute;
    bottom: 0;
    height: 25px; /* height of our bubble "tail" - should match the border-radius above */
    content: '';
}
.shared:before, .shared:after{
	
}
.received {
	height: fit-content;
  align-self: flex-start;
  color: black;
  background: whitesmoke;
 }
 .received:before {
    left: -7px;
    background-color: whitesmoke;
    border-bottom-right-radius: 16px 14px;
  }
.received:after {
    transform: rotate(7deg);
    left: 1px;
    z-index: -2;
    margin-top: 11px;
    border-bottom-right-radius: 10px;
  }
</style>
<div id="chatBubble" class="bg-shadow" style="cursor: pointer; position:fixed; bottom:50px; right:50px; background:black; width:75px; height: 75px; border-radius: 100%; display: flex; justify-content:center; align-items:center;">
	<img src="/assets/img/icons/venti-financial-icon-white.png" style="width:50px;">
</div>
<div id="chatWindow" class="bg-shadow" style="position:fixed; bottom: 50px; right:50px;  background:white; border-radius: 20px; display: flex; flex-direction:column; justify-content:space-between;">
	<div id="toolbar">
		<p style="margin-left:15px"><i class="fa fa-expand"></i></p>
		<p id="closeChat" style="cursor:pointer;"><i class="fa fa-times"></i></p>
	</div>
	<div id="chats">
		<div class="shared received" style="flex-direction:column; justify-content:space-between">
			<div class="author">
				<img src="/assets/img/icons/venti-financial-icon-black.png" style="width:20px;">
			</div>
			<p style="padding-bottom: 0; margin-bottom: 0;">How can I help you today?</p>
		</div>
	</div>
	<div id="chatEntry">
		<span>Ask a question</span>
		<i class="fa fa-paper-plane"></i>
	</div>
</div>
-->