<div class="tab-content pt-3" id="pills-tabContent">
	<div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
		<div class="card bg-white small-shadow">
			<div class="card-body">
				<p>Our ACH payments service provider has requested additional documents to complete your verification. There is no fee associated with completing this step.</p>
			</div>
		</div>
		<h5 class="mb-0 mt-4">Secure Document Upload</h5>
		<p>Your verification documents are not stored on Venti and are encrypted when we send them to our services provider.</p>
		<form id="uploadDocumentForm">
		<div class="card bg-white small-shadow">
			<div class="card-body">
				<div class="col-md-12">
					<div class="form-group">
						<label class="form-label">Select Document Type</label>
						<select class="form-control required" id="document-type" name="document-type">
							<option value="license" selected="">U.S. Driver's License</option>
							<option value="passport">U.S. Passport</option>
							<option value="idCard">Other Government Issued ID</option>
						</select>
					</div>
					<div class="form-group">
						<label class="form-label" style="margin-bottom:-20px">Select File<br>
						<span class="text-small"><ul style="list-style:none;"><li>Your file cannot exceed 10MB and must in one of the following formats: .jpg, .jpeg, .png</li><li>Ensure high photo quality by ensuring the file is </li></ul></span></label>
					</div>
					<input type="hidden" id="file-url" name="file-url">
					<input type="file" class="form-control required" id="file-upload" name="file-upload" style="height: 38px; padding: 6px;">
				</div>
			</div>
		</div>
		<br>
		<button type="button" class="btn btn-success" id="uploadDocument"><i class="fa fa-sync fa-spin"></i> Upload Document</button>
		</form>
	</div>
</div>