@php

$intro = "You have successfully created your Venti account. The next step is to create your Boarding Pass, which is required to make deposits, earn interest, and make purchases on the Venti platform. We need more information to verify your identity and get approval from our ACH payments service provider, Dwolla.";

if($subscription == "buddy"){
	$intro = 'You have successfully created your Venti account. Here\'s your chance to upgrade and unlock more features. If not, simply scroll to the bottom and press "Skip Upgrade."';
}

@endphp


@include('boardingpass.panels.subscription-select')


<div class="pt-3">
	<form id="secureForm">
		@CSRF
		<h5 class="@if($subscription == "buddy") hide @endif">Your Info</h5>

		<span class="@if($subscription == "buddy") hide @endif">This data is saved on Venti and will be securely submitted to Dwolla for verification purposes. If you see an error with any of this information, please send an email to admin@venti.co to have it corrected. Nicknames are likely to fail verification.</span>
		<div class="card bg-white small-shadow mt-3 mb-3 @if($subscription == "buddy")hide @endif"  style="">
			<div class="card-body">
				<div class="welcome">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label class="form-label">First Name<br></label>
								<input type="text" class="form-control disabled" value="{{ getFirstName($user->displayName) }}">
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label class="form-label">Last Name<br></label>
								<input type="text" class="form-control disabled" value="{{ getLastName($user->displayName) }}">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label class="form-label">Email<br></label>
								<input type="text" class="form-control disabled" value="{{ $user->email }}">
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label class="form-label">Phone Number<br></label>
								<input type="text" class="form-control disabled" value="{{ $user->customClaims['phoneNumber'] ?? 'N/A Please Contact Venti Support' }}">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		@if($subscription != "buddy")
		<h5 class="mb-0">Your Address</h5>
		<span>This data is not saved on Venti.</span>
		<div class="card bg-white small-shadow mt-3">
			<div class="card-body">
				<div class="welcome">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label class="form-label">Address Line 1<br></label>
								<input type="text" name="address1" id="address1" class="form-control required" required="">
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label class="form-label">Address Line 2<br></label>
								<input type="text" name="address2" id="address2" class="form-control">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-4">
							<div class="form-group">
								<label class="form-label">City<br></label>
								<input type="text" name="city" id="city" class="form-control required" required="">
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label class="form-label">State<br></label>
								<select class="form-control required" name="state" id="state" required="">
									<option value="">--- Please Select---</option>
									<option value="AL">Alabama</option>
									<option value="AK">Alaska</option>
									<option value="AZ">Arizona</option>
									<option value="AR">Arkansas</option>
									<option value="CA">California</option>
									<option value="CO">Colorado</option>
									<option value="CT">Connecticut</option>
									<option value="DE">Delaware</option>
									<option value="DC">District Of Columbia</option>
									<option value="FL">Florida</option>
									<option value="GA">Georgia</option>
									<option value="HI">Hawaii</option>
									<option value="ID">Idaho</option>
									<option value="IL">Illinois</option>
									<option value="IN">Indiana</option>
									<option value="IA">Iowa</option>
									<option value="KS">Kansas</option>
									<option value="KY">Kentucky</option>
									<option value="LA">Louisiana</option>
									<option value="ME">Maine</option>
									<option value="MD">Maryland</option>
									<option value="MA">Massachusetts</option>
									<option value="MI">Michigan</option>
									<option value="MN">Minnesota</option>
									<option value="MS">Mississippi</option>
									<option value="MO">Missouri</option>
									<option value="MT">Montana</option>
									<option value="NE">Nebraska</option>
									<option value="NV">Nevada</option>
									<option value="NH">New Hampshire</option>
									<option value="NJ">New Jersey</option>
									<option value="NM">New Mexico</option>
									<option value="NY">New York</option>
									<option value="NC">North Carolina</option>
									<option value="ND">North Dakota</option>
									<option value="OH">Ohio</option>
									<option value="OK">Oklahoma</option>
									<option value="OR">Oregon</option>
									<option value="PA">Pennsylvania</option>
									<option value="RI">Rhode Island</option>
									<option value="SC">South Carolina</option>
									<option value="SD">South Dakota</option>
									<option value="TN">Tennessee</option>
									<option value="TX">Texas</option>
									<option value="UT">Utah</option>
									<option value="VT">Vermont</option>
									<option value="VA">Virginia</option>
									<option value="WA">Washington</option>
									<option value="WV">West Virginia</option>
									<option value="WI">Wisconsin</option>
									<option value="WY">Wyoming</option>
									<option value="AS">American Samoa</option>
									<option value="GU">Guam</option>
									<option value="MP">Northern Mariana Islands</option>
									<option value="PR">Puerto Rico</option>
									<option value="UM">United States Minor Outlying Islands</option>
									<option value="VI">Virgin Islands</option>
								</select>
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label class="form-label">Zip Code<br></label>
								<input type="number" name="zipcde" id="zipcode" class="form-control required" placeholder="•••••" inputmode="numeric" maxlength="5" minlength="5" oninput="this.value=this.value.slice(0,this.maxLength)">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<h5 class="mb-0 mt-4">PATRIOT Act Disclosure</h5>
		<p class="d-block">We are required by U.S. federal law to support compliance with the <a href="https://www.fincen.gov/resources/statutes-regulations/usa-patriot-act" target="_blank">PATRIOT Act</a> to help the government fight the funding of terrorism and money laundering activities. Federal law requires all financial institutions to obtain, verify, and record information that identifies each person opening an account. Before we can create your Boarding Pass, we'll need to provide details about you, including your name, address, date of birth, SSN, and/or any other documentation to our ACH payments service provider. Venti does not perform credit or background checks.</p>

		<p class="d-block">This information is also used to open your account at Veridian Credit Union and to comply with requirements for deposit insurance eligibility.</p>

		<p>Your subscription will cover the costs associated with conducting necessary verification for federal compliance. If for whatever reason your verification fails, or if additional documentation is required, our support team will work with you to re-submit at no additional cost. <strong>This data is not saved on Venti.</strong></p>
		<div class="card bg-white small-shadow mt-3">
			<div class="card-body">
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label class="form-label">Your Date of Birth <br></label>
							<input type="date" name="dob" id="dob" class="form-control required" required="">
						</div>
					</div>
					<div class="col-md-6">
							@if(!$retry)
							<div class="form-group">
								<label class="form-label">Last Four of Your Social Security Number<br></label>
								<input type="password" class="form-control required" name="ssn" id="ssn" required="" placeholder="••••" minlength="3" maxlength="4" inputmode="numeric" oninput="this.value=this.value.slice(0,this.maxLength)">
							</div>
							@else
								<div class="form-group">
								<label class="form-label">Enter Your Nine-Digit Social Security Number<br></label>
								<input type="password" class="form-control required" name="ssn" id="ssn" required="" placeholder="•••••••••" maxlength="9" inputmode="numeric" minlength="7" oninput="this.value=this.value.slice(0,this.maxLength)">
							</div>
							@endif
					</div>
				</div>
			</div>
		</div>
		@endif
		@if($subscription != "buddy")
			@if($appPaid == "false")
				<h5 class="mt-4">Your Total Today: ${{ env("VENTI_" . strtoupper($subscription) . "_ANNUAL_FEE")/100 }}</h5>
				<p>This fee is partially refundable <strong>within 48 hours of purchase</strong>. If your verification fails permanently or if you no longer wish to use our platform within 48 hours, you'll be refunded 
				@if($subscription == "first")
					${{ number_format((env("VENTI_" . strtoupper($subscription) . "_ANNUAL_FEE")/100) - 10,2) }}.
				@endif
				@if($subscription == "priority")
					${{ number_format((env("VENTI_" . strtoupper($subscription) . "_ANNUAL_FEE")/100) - 4,2) }}.
				@endif
				</p>
				<p><strong>Your credit card information is not saved on Venti.</strong> Join {{ number_format(env("VIPS"),0) }}+ other Venti savers and create your travel savings account.</p>
			<div class="row">
				<div class="col-md-6">
					<div class="card bg-white small-shadow mt-3" id="card-entry">
						<div class="card-body">
							<div class="form-group" style="margin:0;">
								<div id="card-element">
								    <!-- Elements will create input elements here -->
								</div>

								  <!-- We'll put the error messages in this element -->
								<div id="card-errors" role="alert" style="margin-top:20px;"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
			@endif

			@if($appPaid == "true")
				<div class="card bg-success small-shadow mt-3" id="card-success" style="background-image:none !important; @if($appPaid) display: block; @endif">
					<div class="card-body" style="background-image:none !important;">
						<div class="row">
							<div class="col-md-12 text-center">
								<h5 class="text-white" style="margin:0;"><i class="fa fa-check"></i> Payment Received</h5>
							</div>
						</div>
					</div>
				</div>
			@endif
		@endif
		<br>
		<button type="button" class="btn btn-primary" @if(!$retry) id="customerCreate" @else id="customerUpdate" @endif style="max-width: 162px; font-weight: 400;"><i class="fa fa-sync fa-spin"></i> @if($subscription == "buddy") Skip Upgrade @else Submit @endif</button>
	</form>
</div>