<div class="row">
	<div class="col-lg-12 col-md-12 mb-xs-3">
		<div style="display:flex; flex-direction: column;">
			<div class="col-md-12 col-lg-12 col-xl-6 card bg-white small-shadow lg-topside">
				<div class="card-body text-center" style="display: flex; flex-direction:row">
					<div style="display: flex; flex-direction: row; justify-content:center; align-items: center;">
						<img src="/assets/img/icons/credit-card.png" style="height: fit-content; max-height: 55px; width: 100%; max-width:55px;">
						<span style="padding: 0 5px; text-align: left; font-size: 13px;">Link a card for more payment flexibility when booking. 3% processing fee added at checkout.</span>
					</div>
					<button type="button" class="btn btn-success btn-small btn-sm btn-square"  data-bs-toggle="modal" data-bs-target="#addCardModal" style="height:45px; min-width: 45px;"><i class="fa fa-plus" style="font-size:2em"></i></button>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="row mt-4 sm-hide">
	<p></p>
</div>
<div class="row" id="cardList"></div>