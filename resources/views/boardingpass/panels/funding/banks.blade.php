@if(sizeof($fundingSources) == 1)
	<div class="card bg-white small-shadow mb-3">
		<div class="card-body">
			<p>We are not linking new bank accounts at this time.</p>
		</div>
	</div>
@endif

		@if(sizeof($fundingSources) >= 1 || $customer["status"] == "unverified")
			<div class="row">
				<div class="col-lg-12 col-md-12">
					<div style="display:flex; flex-direction: column;">
						<div class="col-md-12 col-lg-12 col-xl-6 card bg-white small-shadow mb-3 lg-topside">
							<div class="card-body text-center" style="display: flex; flex-direction:row">
								@if(
								$fundingSourcesCount == 0 ||
								($fundingSourcesCount >= 1 && $accountAge >= 30)
								)
									@if($subscription != "buddy")
										
									@endif
								@endif
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row mt-4 sm-mt-0"> 
				@foreach($fundingSources as $fundingSource)
					@if(!$fundingSource->removed && isset($fundingSource->bankAccountType) && isset($fundingSource->id))
						<div class="col-md-12 col-lg-12 col-xl-4 mb-xs-3 mt-4 sm-mt-0">
							<div class="card bg-white small-shadow">
								<div class="card-body text-center">
									<div style="display: flex; justify-content: space-between; width:100%;">
										<span style="font-size:11px;">
											{{ $fundingSource->bankName }}
										</span>
										<span style="margin-top:-4px">
											<a href="#" title="Delete Bank" class="deleteSource" style="color:black; text-decoration:none" data-funding-id="{{ $fundingSource->id }}"><i class="fa fa-times"></i></a>
										</span>
									</div>
									
									<h1 style="padding-top:20px; padding-bottom:20px;"><img src="/assets/img/icons/bank.png" style="width: 100%; max-width:55px;"></h1>
									<h5>{{ $fundingSource->name }} | {{ ucfirst($fundingSource->bankAccountType) }} </h5>

									<p>Added: {{ Carbon\Carbon::parse($fundingSource->created)->format("M. d, Y") }} </p>
									@if($fundingSource->status == "unverified")
										<button class="btn btn-warning btn-small btn-sm toggleFundingVerify" data-bs-toggle="modal" data-bs-target="#verifyFunds"  data-source-id="{{ $fundingSource->id }}" data-bank-name="{{ $fundingSource->name }}">Begin Verification</button>
									@endif

									@if($fundingSource->status == "verified")
										<label class="btn btn-white btn-small btn-sm" style="color: #198754;font-weight: 400;">Verified</label>
									@endif
								</div>
							</div>
						</div>
					@endif
				@endforeach
				<div id="linkedBanks" style="display:contents;"></div>
			</div>
		@endif