<div class="modal fade" id="warpDriveFunds" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true" style="background-image: rgba(0,0,0,0.9); ">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header" style="position: absolute;
    right: 0;
    top: 10px; border-bottom: transparent; z-index: 99;">
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <form id="">
          <div class="row">
            <div class="col-md-12 mb-3 text-center" style="
              padding-top: 20px;
              padding-bottom: 20px;
              align-items: center;
              width: 300px;
              margin: 0 auto;">
              <div>
                <img src="/assets/img/icons/teleport-forward.png" style="width: 100%; max-width:200px;">
              </div>
            </div>
          </div>
          <div class="row mb-3">
            <div class="col-md-12">
              <label class="form-label">Travel to the Future<br><span style="font-size:12px; font-weight:400">Purchase Points to take advantage of travel savings. You are limited to 25% of your Cash Balance per order and one order per week. <strong>Current Limit $<span class="warpLimit" style="font-weight: bold;">0</span></strong></span></label>
              <div class="input-group">
                <span class="btn btn-white" style="border:1px solid #ced4da"><i class="fa fa-dollar" style="padding-top:8px"></i></span>
                <input type="number" id="inputWarpAmount" name="inputWarpAmount" class="form-control required" placeholder="10" max="5000" maxlength="4" inputmode="numeric" oninput="this.value=this.value.slice(0,this.maxLength)">
                <!--
                <span class="btn btn-black" style="border:1px solid #ced4da; padding-top: 10px">MAX</span>
              -->
              </div>
            </div>
          </div>
          
          <div class="row">
            <div class="col-md-12">
              <div class="alert alert-danger hidden" id="overWarpLimit" style="width:100%; margin-bottom: 10px;">
                The amount you've entered exceeds 25% of your Cash Balance. Please enter a lower amount.
              </div>
              <span style="display:block; width:100%; margin-bottom: 10px;">
                Your account will be credited instantly. Boarding Pass points are transferable.
              </span>
              <div style="display:flex; justify-content:center; flex-direction:row;">
                <input type="checkbox" id="termsWarp" value="1" />
                <p style="font-size:13px; padding-left:20px">I understand that I am purchasing <span class="warpAmount"></span> Boarding Pass Points with funds from my Cash Balance and that this transaction is non-refundable.</p>
              </div>
              @if($canPointsPurchasePoints)
              <button class="btn btn-success" type="button" id="finalizeWarp"><i class="fa fa-sync fa-spin"></i> Purchase <span class="warpAmount"></span> Points</button>
              @else
                <div class="alert alert-info">You have to wait a full week to purchase more Points.</div>
              @endif
              <span style="display: block; font-size:13px;">10% premium for purchasing Points</span>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
  <!--
  <section id="shootingStars">
    <span></span>
    <span></span>
    <span></span>
    <span></span>
    <span></span>
    <span></span>
    <span></span>
    <span></span>
    <span></span>
    <span></span>
</section>
-->

<div id='holder' style='background-color:#000; width:100%; top: 0; height:100%; left: 0; position:absolute;'>

  <div id='effect' style='position:absolute; width:100%; top: 0; height:100%;'></div>

</div>
</div>
