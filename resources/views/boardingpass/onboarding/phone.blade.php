@extends('layouts.curator-master')

@section('css')
    <link rel="stylesheet" type="text/css" href="/assets/css/steps.css">
    <link rel="stylesheet" href="/assets/css/intlTelInput.min.css">
    <style>
        input.disabled{
            pointer-events: none;
        }

        @media (max-width: 767px){
            .black.hero-section {
            margin-left: 0 !important;
            margin-right: 0 !important;
            }
        }
        .col-form-label{
            font-weight: bold !important;
        }
        form > *{
            font-weight: 300;
        }

        .slim-p{
            margin: 0;
            padding: 0;
        }
        .isVerified{
            display: none;
        }
        .fa-spinner{
            display: none;
        }
        .loading .fa-spinner{
            display: inherit !important;
        }

        @if(Session::get('smsRecipient') === null)
            .new{
                display: none;
            }
        @endif
        
        input.code-input {
            font-size: 1.8em;
            width: 1em;
            text-align: center;
            flex: 1 0 1em;
            border-color: rgb(0,0,0,0.1);
            border-radius: 7px;
            width: 55px;
            height: 75px;
        }
    </style>
@endsection

@php
    $smsVerified = Session::get('user')->customClaims["smsVerified"];
@endphp

@section('content')
<div id="page-content">
    <div class="gradient-wrap">
        <div class="container black position-relative hero-section mb-4">
            <div class="row justify-content-center" >
                <div class="col-md-12 text-center mt-4">
                    <h2 class="mt-2">Two-Factor Security</h2>
                </div>
                <div class="col-md-4 text-center">
                    <div class="card soft-shadow" style="border:none;">
                        <div class="card-body">
                            <div class="row mt-4 text-center">
                                <div class="col-md-12 text-center">
                                    <img src="/assets/img/icons/mfa.png" style="width: 130px;">
                                </div>
                            </div>
                            <div id="example-basic">
                                <h3></h3>
                                <section>
                                    <div class="row mb-3 justify-content-center">
                                        <div class="col-md-12">
                                            <h5 class="text-center"><label for="name" class="col-form-label text-left text-bold">Add Your Number</label></h5>
                                            <p></p> 
                                            <p class="text-left">For your security, we are requiring all accounts to add a phone number to enable Two-Factor Authentication.
                                            </p>
                                            <p class="text-left">Please add your cell number to receive a six-digit code to complete account setup. Messaging and data rates may apply. At this time, we only allow U.S. (including territories) and Canada numbers.</p>
                                        </div>
                                        <div class="col-md-12 text-center">
                                            <p style="margin:0"></p>
                                            <span style="font-size:12px;"></span>
                                            

                                            @if(!$smsVerified)
                                                @if(Session::get('smsRecipient') !== null)
                                                    <br><div><fieldset class='number-code'><legend>Security Code</legend><div><input name='code' class='code-input' placeholder='' required/><input name='code' class='code-input' placeholder='' required/><input name='code' class='code-input' placeholder='' required/><input name='code' class='code-input' placeholder='' required/><input name='code' class='code-input' placeholder='' required/><input name='code' class='code-input' placeholder='' required/></div></fieldset><br><button type='button' data-phoneNumber="{{ Session::get('smsRecipient') }}" data-service-id="{{ Session::get('serviceID') }}" style='margin:0 auto; font-weight:500' class='btn btn-primary' id='verifySMS'><i class='fa fa-check'></i> Verify</button></div>
                                                    <span id="full_number" style="font-size:12px;">We sent a txt to: {{ Session::get('smsRecipient') }}</span>
                                                @else
                                                    <div class="form-group mt-2 justify-content-center" style="display: flex; align-items: center;">
                                                        <input id="phoneNumber" class="form-control" name="phone" type="tel" style="max-width:300px;" placeholder="Enter Your Cell Number">
                                                        <button type='button' id="sendSMSVerification" class="btn btn-primary" style="margin-left:5px;"><i class="fa fa-paper-plane"></i></button>
                                                    </div>
                                                @endif
                                                <span id="full_number" style="font-size:12px;"></span>
                                                <div class="col-md-12 mt-4 text-center new">
                                                    <button class="btn btn-black mt-4" id="newCode"><i class="fa fa-sync fa-spin"></i> Request New Code</button>
                                                </div>
                                            @else
                                                <div class="col-md-12 text-center">
                                                    <a href="/boardingpass/onboarding/continue" class="btn btn-primary" id='continueButton'>Continue</a>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </section>
                            </div>
                        </div>
                    </div>
                    <br><br>
                    <a href="/user/logout" class="btn btn-white text-danger"><i class="fa fa-lock"></i> Log Out</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
    <script type="text/javascript" src="/assets/js/jquery.steps.min.js"></script>
    @if(Session::get('smsRecipient') === null)
    <script src="/assets/js/intlTelInput.js"></script>
    @endif

    <script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': "{{ csrf_token() }}"
        }
    });

    @if(Session::get('smsRecipient') === null)

    var input = document.querySelector("#phoneNumber");

    window.intlTelInput(input, {
      formatOnDisplay: true,
      utilsScript: "/assets/js/utils.js",
    });

    iti = intlTelInput(input);

    @else

    initCodeInput();

    @endif

    var phoneNumber;

    $("#sendSMSVerification").click(function(){
        sendSMSVerification($(this), true);
    });

    $("#newCode").click(function(){
            sendSMSVerification($(this), false);
    });

    $("#continueButton").click(function(){
        $(this).addClass("disable-while-loading");
    });

    function sendSMSVerification(button, addInput){
        
        if(button.hasClass("btn-black")){
            button.addClass("disable-while-loading");
        }
        
        @if(Session::get('smsRecipient') !== null)

            var phoneNumber = "{{ Session::get('smsRecipient') }}";
            
            @else

            var phoneNumber = iti.getNumber(); //input.getNumber(); //$("#phoneNumber").val();
        @endif

        console.log(phoneNumber);

        if(phoneNumber.length < 12 || phoneNumber == "" || phoneNumber.length > 12){
            return swal({
                                icon: 'warning',
                                title: "Try a different number",
                                text: 'The phone number you provided is either too long, too short, or includes non numbers. 1-800 numbers are not accepted. Do not include the country code. Only U.S. numbers are accepted at this time.'
                            });
        } else {

            if(!phoneNumber.includes("+")){
                var countryData = iti.getSelectedCountryData();
                phoneNumber = "+" + countryData.dialCode + phoneNumber;
            }
            $(".new").show();
            $.ajax({
                url: "{{ route('send-sms-verification') }}",
                type: 'post',
                processData: true,
                dataType: 'json',
                data:{
                    phoneNumber: phoneNumber
                },
                success: function (data) {


                    if(data != 500 && data != 201){

                        if(addInput){
                            $("#full_number").text("We sent a txt to: " + phoneNumber);
                            button.parent().html("<br><div><fieldset class='number-code'><legend>Security Code</legend><div><input name='code' class='code-input' placeholder='' required/><input name='code' class='code-input' placeholder='' required/><input name='code' class='code-input' placeholder='' required/><input name='code' class='code-input' placeholder='' required/><input name='code' class='code-input' placeholder='' required/><input name='code' class='code-input' placeholder='' required/></div></fieldset><br><button type='button' data-phoneNumber='" + data.phoneNumber +"' data-service-id='" + data.serviceID + "' style='margin:0 auto; font-weight:500' class='btn btn-primary' id='verifySMS'><i class='fa fa-check'></i> Verify</button></div>");
                            button.parent().parent().append("<p style='Enter the code we texted you. It may take a few seconds to arrive.'></p>");
                                initCodeInput();
                        } else{
                            swal({
                                icon: 'success',
                                title: "New code sent",
                                text: 'We just sent your new code.',
                            });

                            button.parent().html("<br><div><fieldset class='number-code'><legend>Security Code</legend><div><input name='code' class='code-input' placeholder='' required/><input name='code' class='code-input' placeholder='' required/><input name='code' class='code-input' placeholder='' required/><input name='code' class='code-input' placeholder='' required/><input name='code' class='code-input' placeholder='' required/><input name='code' class='code-input' placeholder='' required/></div></fieldset><br><button type='button' data-phoneNumber='" + data.phoneNumber +"' data-service-id='" + data.serviceID + "' style='margin:0 auto; font-weight:500' class='btn btn-primary' id='verifySMS'><i class='fa fa-check'></i> Verify</button></div>");
                        }
                    }
                    else {
                        if(data == 201){
                            swal({
                                icon: 'warning',
                                title: "Please wait two minutes",
                                text: "Before requesting another code",
                            });

                        } else {
                            swal({
                                icon: 'error',
                                title: "Something went wrong",
                                text: "There was an error sending a text message to: " + $("#phoneNumber").val() + ". Please make sure the phone number is correct and the appropriate country code is applied.",
                            });

                            button.removeClass("disable-while-loading");
                        }
                        
                    }

                    
                    
                },
               error: function(jqXhr, textStatus, errorMessage){
                  console.log("Error: ", errorMessage);
               }
            });

            button.removeClass("disable-while-loading");
        }
    }

    $("body").on("click", "#verifySMS", function(){
        var button = $(this);
        var smsCode = "";

        $(".code-input").each(function(){
            var digit = $(this).val();
            if(digit == "" || digit == null || isNaN(digit)){
                return swal({
                    icon: 'warning',
                    title: "Something's wrong",
                    text: 'Your code must be six digits',
                });
            }

            smsCode += digit;
        });

        var serviceID = $(this).attr("data-service-id");
        var phoneNumber = $(this).attr("data-phoneNumber");

        if(phoneNumber.length < 10 || serviceID == ""){
            swal({
                icon: 'warning',
                title: "Something's wrong",
                text: 'The phone number seems incorrect',
            });
        }

        else {
            $.ajax({
                url: "{{ route('verify-sms-code') }}",
                type: 'post',
                processData: true,
                dataType: 'json',
                data:{
                    phoneNumber: phoneNumber,
                    smsCode: smsCode,
                    serviceID: serviceID
                },
                success: function (data) {
                    if(data == "ok"){
                        $("#full_number").hide();
                        $(".new").hide();
                        button.parent().html("<p>Phone number verified <i class='fa fa-check'></i> <br><br><a href='/boardingpass/onboarding/continue' class='btn btn-primary' id='continueButton'>Continue</a></p>");
                    }
                    else {
                        swal({
                            icon: 'warning',
                            title: "Something's wrong",
                            text: 'That code was incorrect. Please double-check the number we sent you via txt.',
                        });
                    }
                }
            });
        }
    });

    
    function initCodeInput(){

        const inputElements = [...document.querySelectorAll('input.code-input')]

        inputElements.forEach((ele,index)=>{
          ele.addEventListener('keydown',(e)=>{
            // if the keycode is backspace & the current field is empty
            // focus the input before the current. Then the event happens
            // which will clear the "before" input box.
            if(e.keyCode === 8 && e.target.value==='') inputElements[Math.max(0,index-1)].focus()
          })
          ele.addEventListener('input',(e)=>{
            // take the first character of the input
            // this actually breaks if you input an emoji like 👨‍👩‍👧‍👦....
            // but I'm willing to overlook insane security code practices.
            const [first,...rest] = e.target.value
            e.target.value = first ?? '' // first will be undefined when backspace was entered, so set the input to ""
            const lastInputBox = index===inputElements.length-1
            const didInsertContent = first!==undefined
            if(didInsertContent && !lastInputBox) {
              // continue to input the rest of the string
              inputElements[index+1].focus()
              inputElements[index+1].value = rest.join('')
              inputElements[index+1].dispatchEvent(new Event('input'))
            }
          })
        });
    }


// mini example on how to pull the data on submit of the form
function onSubmit(e){
  e.preventDefault()
  const code = inputElements.map(({value})=>value).join('')
  console.log(code)
}
    
    </script> 
@endsection


