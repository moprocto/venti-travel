@extends('layouts.curator-master')

@section('css')
    <link rel="stylesheet" type="text/css" href="/assets/css/steps.css">
    <style>
        input.disabled{
            pointer-events: none;
        }
        .wizard>.content>.body{
            height: 100%;
            position: relative;
        }
        .wizard>.content{
            min-height: 120px;
        }
        .wizard .steps{
            display: none;
        }

        @media (max-width: 767px){
            .black.hero-section {
            margin-left: 0 !important;
            margin-right: 0 !important;
            }
        }
        .col-form-label{
            font-weight: bold !important;
        }
        form > *{
            font-weight: 300;
        }

        .slim-p{
            margin: 0;
            padding: 0;
        }
        .isVerified{
            display: none;
        }
        .fa-spinner{
            display: none;
        }
        .loading .fa-spinner{
            display: inherit !important;
        }
    </style>
@endsection

@section('content')
<div id="page-content">
    <div class="gradient-wrap">
        <div class="container black position-relative hero-section mb-4">
            <div class="row justify-content-center" >
                <div class="col-md-12 text-center mt-4">
                    <h2 class="mt-2">Confirm Email</h2>
                </div>
                <div class="col-md-4 text-center">
                    <div class="card soft-shadow" style="border:none;">
                        <div class="card-body">
                            <div class="row mt-4 text-center">
                                <div class="col-md-12 text-center">
                                    <i class="fa fa-envelope" style="font-size:84px"></i>
                                </div>
                            </div>
                            <div id="example-basic">
                                <h3></h3>
                                <section>
                                    <div class="row mb-3 justify-content-center">
                                        <div class="col-md-12">
                                            <h5 class="text-center"><label for="name" class="col-form-label text-left text-bold">Verify Your Email</label></h5>
                                            <p></p> 
                                            <p class="text-left">Click the verification link in the email we just sent to: <strong>{{ Session::get('user')->email }}</strong>. If you have not received it in your inbox, please check your spam folder. This step is required to continue. Depending on your provider, our email can take up to 3 minutes to appear.</p>
                                        </div>
                                        <div class="col-md-12 text-center">
                                        	<a href="/airlock/home" class="btn btn-primary isVerified">Continue</a>
                                        </div>
                                        <div class="col-md-12 mt-4 text-center">
                                        	<button class="btn btn-black mt-4" id="newEmail"><i class="fa fa-sync fa-spin"></i> Request New Email</button>
                                        </div>
                                    </div>
                                </section>
                            </div>
                        </div>
                    </div>
                    <br><br>
                    <a href="/user/logout" class="btn btn-white text-danger"><i class="fa fa-lock"></i> Log Out</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
    <script type="text/javascript" src="/assets/js/jquery.steps.min.js"></script>
    <script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': "{{ csrf_token() }}"
        }
    });

    $("#newEmail").click(function(){
        var btn = $(this);
        btn.addClass("disable-while-loading");
        $.ajax({
            url: "{{ route('send-verification-email') }}",
            processData: true,
            dataType: 'json',
            data:{
                email: "{{ Session::get('user')->email }}"
            },
            type: 'post',
            success: function (data) {
                if(data == 201){
                    swal({
                        icon: 'warning',
                        title: 'Please wait five minutes',
                        text: 'Check your spam folder first. Then try again later to receive another verification email.',
                    });
                }
                if(data == 200){
                    swal({
                        icon: 'success',
                        title: 'Verification email sent',
                        text: '',
                    });
                }
                btn.removeClass("disable-while-loading");
            }
        }); 
    });

    function checkEmailStatus(){
    	$.ajax({
    		url: "{{ route('verify-email-validation') }}",
    		processData: true,
    		dataType: 'json',
    		data:{
    		    email: "{{ Session::get('user')->email }}"
    		},
    		type: 'post',
    		success: function (data) {
    		    console.log(data);
    		    if(data != 201){
    		        // email is verified
                    $("#newEmail").hide();
    		    }
                if(data == 200){
                    $(".isVerified").show();
                }
    		}
    	});   

        setTimeout(checkEmailStatus, 5000);
    }

    checkEmailStatus();
    

    </script> 
@endsection


