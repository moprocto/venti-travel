@extends('layouts.curator-master')

@section('css')
    <style type="text/css">
        @media(max-width: 1368px ){
            .tb-full-width{
                width: 100%;
            }
        }
        #footer{
            margin-top: 100px;
        }
        .about-body{
            justify-content: center;
        }
        .meter {
          box-sizing: content-box;
          height: 20px; /* Can be anything */
          position: relative;
          margin: 30px 0 20px 0; /* Just for demo spacing */
          background: #555;
          border-radius: 25px;
          padding: 10px;
          box-shadow: inset 0 -1px 1px rgba(255, 255, 255, 0.3);
        }
        .meter > span {
          display: block;
          height: 100%;
          border-top-right-radius: 8px;
          border-bottom-right-radius: 8px;
          border-top-left-radius: 20px;
          border-bottom-left-radius: 20px;
          background-color: rgb(43, 194, 83);
          background-image: linear-gradient(
            center bottom,
            rgb(43, 194, 83) 37%,
            rgb(84, 240, 84) 69%
          );
          box-shadow: inset 0 2px 9px rgba(255, 255, 255, 0.3),
            inset 0 -2px 6px rgba(0, 0, 0, 0.4);
          position: relative;
          overflow: hidden;
        }
.meter > span:after,
.animate > span > span {
  content: "";
  position: absolute;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
  background-image: linear-gradient(
    -45deg,
    rgba(255, 255, 255, 0.2) 25%,
    transparent 25%,
    transparent 50%,
    rgba(255, 255, 255, 0.2) 50%,
    rgba(255, 255, 255, 0.2) 75%,
    transparent 75%,
    transparent
  );
  z-index: 1;
  background-size: 50px 50px;
  animation: move 2s linear infinite;
  border-top-right-radius: 8px;
  border-bottom-right-radius: 8px;
  border-top-left-radius: 20px;
  border-bottom-left-radius: 20px;
  overflow: hidden;
}

.animate > span:after {
  display: none;
}

@keyframes move {
  0% {
    background-position: 0 0;
  }
  100% {
    background-position: 50px 50px;
  }
}

.orange > span {
  background-image: linear-gradient(#f1a165, #f36d0a);
}

.red > span {
  background-image: linear-gradient(#f0a3a3, #f42323);
}

.nostripes > span > span,
.nostripes > span::after {
  background-image: none;
}


    </style>
@endsection



@section('content')

@php
    $referrals = sizeof($referrals);
    $add = 0;


    $codelength = strlen($me["fields"]["Source"] ?? "n");

    if($codelength >= 15){
        // I was referred by someone, so I get $10 too
        $add = 10;
    }
@endphp

<div id="backdrop" style="margin-top: -130px; width: 100%; z-index: 1; display: flex; align-items: center; min-height: 800px; background-position: center;  background-size: cover; margin-bottom: -64px; background-image:url('/assets/img/bali-waterfall.jpg')">
        <div class="gradient-wrap" style="background: transparent; width:100%;">
            <!--  HERO -->
            <div class="section-wrapper black position-relative">
                <div class="row" style="align-items:center;">
                    <div class="col-md-5 col-sm-12 col-12 top-header" style="display: flex; align-items:center;">
                        <div id="hero-text-wrapper" style="max-width:900px;">
                            <h1 class="mb-3 section-title" style="margin-top: 2em; line-height: 3rem; color: white; font-weight:600 !important; font-size: 3.8rem; margin-bottom: 10px;">
                            YOU'RE VIP!</sup>
                            <span style="font-size: 0.45em; display:block;">So far, you've earned <strong>${{ ($referrals * 10) + 20 + $add }}</strong></span>
                            </h1>
                            <span style="color:white; font-weight:bold; font-size: 18px;">Bookmark this page to track your rewards</span>
                            <h2 class="section-subtitle mb-4" style="color: white; max-width:900px;">
                            </h2>

                            <div class="card" style="border-color: transparent; border-radius:10px; background-color:rgba(255,255,255,0.9)">
                                <div class="card-body">
                                    @if((100 - $referrals) > 0)
                                        <h5 style="margin:0">Invite {{ 100 - $referrals }} more friends and family to claim all eligible credits.</h5>
                                    @else
                                        <h5 style="margin:0">Congratulations! You've maxed out your referral credits for this year.</h5>
                                    @endif
     								<div class="meter animate">
									    <span style="width: 0%; max-width:470px;"><span></span></span>
									  </div>

									<p style="font-weight:bold">Share your referral link with a friend, and you both will be credited $10 onto your Boarding Passes with your first deposits:</p>
                                    <div class="btn-group">
                                        <span class="form-control" id="link" style="min-width: 250px; padding:10px; border-color:transparent;">https://venti.co/s/{{$code}}</span>
                                        <button class="btn btn-black" onclick="copyLink();"><i class="fa fa-regular fa-copy txt-light text-light txt-white"></i></button>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <p style="color:white;">Venti is a travel savings account with an 9% APY. Interest is accrued in the form of transferrable points that can only be spent on travel-related purchases.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<!-- Cover End -->
@endsection


    
@section("js")
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/typeahead.js/0.10.4/typeahead.bundle.min.js"></script>
    @if(env('APP_ENV') != "local")
        <!-- Meta Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s)
            {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};
            if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
            n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t,s)}(window, document,'script',
            'https://connect.facebook.net/en_US/fbevents.js');
            fbq('init', '1169331916805166');
            fbq('track', 'PageView');
        </script>
        <noscript><img height="1" width="1" style="display:none"
        src="https://www.facebook.com/tr?id=1169331916805166&ev=PageView&noscript=1"
        /></noscript>
        <!-- End Meta Pixel Code -->
    @endif
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/11.0.2/bootstrap-slider.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $.ajaxSetup({
                 headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                 }
              });

            toggleLogo("black","white");

            function toggleLogo(color, replaced){
                var logoSRC = $(".logo").attr("src");
                logoSRC = logoSRC.replace(color, replaced);
                console.log(logoSRC)
                $(".logo").attr("src", logoSRC);
            }

            

        });
        function copyLink(){
          console.time('time1');
            var temp = $("<input>");
          $("body").append(temp);
         temp.val($('#link').text()).select();
          document.execCommand("copy");
          temp.remove();
            console.timeEnd('time1');
            $('#link').parent().find(".btn-black").removeClass("btn-black").addClass("btn-success");
            $('#link').parent().find(".btn-success").find(".fa").removeClass("fa-regular").removeClass("fa-copy").addClass("fa-check");
        
            return swal({
			  icon: 'success',
			  title: 'All Set!',
			  text: 'Your referral link has been copied',
			})

        }

        $(".meter > span").each(function () {
		  $(this)
		    .data("origWidth", $(this).width())
		    .width(0)
		    .animate(
		      {
		        width: $(this).data("origWidth")
		      },
		      1200
		    );
		});

		$(".meter > span").each(function () {
		  $(this)
		    .data("origWidth", $(this).width())
		    .width(0)
		    .animate(
		      {
		        width: "{{ ($referrals/100) * 100 }}%"
		      },
		      1200
		    );
		});

    </script>
@endsection