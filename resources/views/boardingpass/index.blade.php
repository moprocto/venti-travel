@extends('layouts.curator-master')

@php
    $selectable = false;
@endphp

@section('css')
    <style type="text/css">
        html{
            overflow-x: hidden;
        }
        @media(max-width: 1368px ){
            .tb-full-width{
                width: 100%;
            }
        }
        .hamburger-menu-list li a{
            color: white;
        }
        .mobile-nav-open .hamburger-menu-list li a{
            color: black;
        }
        .btn-white{
            color: black !important;
        }
        .mobile-nav-open #backdrop{
            margin-top: 0 !important;
        }
        .about-body{
            justify-content: center;
        }
        @media(min-width: 1367px){
            {
                margin-left: 22px;
            }
            .row.about-body{
                margin-bottom: 22px;
            }
            .desktop-hide{
                display: none;
            }
        }
        @media(max-width: 800px){
            .mb-pd-20{
                padding-bottom: 20px;
            }
        }
        @media(max-width: 1368px){
            .mobile-block{
                display: block !important;
            }
            .mobile-hide{
                display: none;
            }
            #about .about-body .about-body-item .about-body-icon-cnt{
                justify-content: left !important;
                margin-bottom: 10px;
            }
            .pills-item{
                background-position-y: inherit !important;
            }
            .sign-up-box{
                padding-top: 80px;
                padding-bottom:  0;
            }
        }
        .about-body-icon-cnt{
            border: 1px solid black;
            border-radius: 20px;
            padding: 5px 10px;
        }
        

        .font-light{
            font-weight: 300 !important;
        }
        .full-width{
            max-width: 9999px !important;
            padding-left:  0;
            padding-right: 0;
        }
        #about{
            padding-top: 0;
        }
        .pd-20{
            padding: 80px 40px;
        }
        i.text-small{
            font-size: 16px;
        }
        .about-body-icon-cnt, .about-body-text-head{
            padding-left: 14px;
        }
        .about-body-text-head{
            padding-right: 15px;
            font-weight: bold;
        }
        @media(min-width:  768px){
            a.nav-links{
                border-radius: 0 !important;
            }
            a.nav-links:hover{
                color: white !important;
                border-bottom: 1px solid white !important;
                transition: 0.5s;
            }
        }
        @media(min-width: 1367px){
            {
                margin-left: 22px;
            }
            .row.about-body{
                margin-bottom: 22px;
            }
            .desktop-hide{
                display: none;
            }
        }
        @media(max-width: 768px){
            .mobile-hide{
                display: none;
            }


        }
        @media(max-width: 468px){
            #minutes, #seconds{
                display: none;
            }
            .sm-block{
                display: block;
            }
        }   
        @media(max-width: 1368px){
            .mh-500-sm{
                max-height: 500px;
            }
            .mobile-block{
                display: block !important;
            }
            .mobile-hide{
                display: none;
            }
            #about .about-body .about-body-item .about-body-icon-cnt{
                justify-content: left !important;
                margin-bottom: 10px;
            }
            .pills-item{
                background-position-y: inherit !important;
            }
            .sign-up-box{
                padding-top: 80px;
                padding-bottom:  0;
            }
            .preview-mh{
                width: 143% !important;
                margin-left: -73px;
            }
        }

        .text-gold{
            color: #9D8A7C !important;
        }

        .text-large{
            font-size: 20px;
        }

        ol.pills-list{
            display: block;
            padding: 0 0 0 26px;
            list-style: none;
            overflow: hidden;
        }

        .pills-list li{
            display: flex;
            justify-content: space-between;
            align-items: center;
            margin-bottom: 10px;
            padding: 10px 20px 5px 20px;
        }
        .sand-badge{
            background-color: rgba(157, 138, 124, 0.7);
            opacity: 0.8;
            color: white;
            font-size: 22px;
            border-radius: 50%;
            width: 42px;
            height: 40px;
            letter-spacing: 1;
        }
        .pills-item{
            padding: 7px 20px;
            border-radius: 10px;
            margin-bottom: 20px;
            background-color: whitesmoke;
        }
        .pills-item div{
            padding-top: 10px;
        }
        .pills-item p{
            padding-top: 3px;
        }
        .pills-item-1{
            background-position-y: 0;
        }

        .pills-item-2{
            background-position-y: -90px;
        }
        .pills-item-3{
            background-position-y: -180px;
        }
        .pills-item-4{
            background-position-y: -220px;
        }
        li.pills-item-3{
            background: #d7d0ca;
        }
        #mc_embed_signup .text-small{
            font-size: 12px !important;
        }

        #subscribe .text-small{
            margin-top: 15px;
            
        }
        .about-body-item{
            margin-bottom: 10px;
        }.slider-horizontal{
            width: 100% !important;
        }
        #faqs
        {
        padding-bottom: 100px;
        }
        #faqs .faqs-container .faq
        {
            border-top: 1px solid var(--color-border);
            padding: 15px;
        }
        #faqs .faqs-container .faq .faq-head
        {
            padding: 15px 0px;
            position: relative;
        }
        #faqs .faqs-container .faq .faq-head h5
        {
            width: 90%;
            cursor: pointer;
        }
        #faqs .faqs-container .faq .faq-head h5::before
        {
            content: 'keyboard_arrow_down';
            text-align: center;
            font-size: 20px;
            line-height: 27px;
            font-family: 'Material Icons Round';
            height: 25px;
            width: 25px;
            color: #fefefe;
            background: var(--color-sec);
            position: absolute;
            top: 50%;
            right: 0;
            transform: translateY(-50%);
            border-radius: 50%;
            transition: 0.5s;
        }
        #faqs .faqs-container .faq .faq-head h5[aria-expanded=true]::before
        {
            content: 'keyboard_arrow_up';
        }
        .form-container .emailoctopus-form-wrapper .emailoctopus-success-message{
            color:  orange !important;
            font-size: 18px;
            padding-bottom: 20px;
        }
        .accordion-item{
            margin-bottom: 10px;
        }
        #countdown{
            position: fixed;
            z-index: 999;
            height: 50px;
            background: white;
            width: 100%;
            bottom: 0;
            left: 0;
            display: flex;
            flex-direction: row;
            justify-content: center;
            padding: 10px 20px;
            font-weight: bold;
            font-size: 0.9em;
            align-items: center;
        }
        #countdown div.descrtiption{
            text-align: center;
            font-size: 0.8em;
            padding: 10px;
        }
        #timer{
            font-weight: bold;
            display: flex;
            justify-content: space-between;
        }
        #timer *{
            padding-right: 4px;
        }
        .about-body-icon-cnt{
            border: 1px solid rgba(0,0,0,0.1);
        }
        .footnote{
            width: 30px;
            display: inline-block;
        }
        html body{
            background: #F2F2F6 !important;
        }
        .features ul {
            list-style: none;
            padding: 0;
        }
        .features ul li{
            padding: 10px;
            border-bottom: 1px solid whitesmoke;
            font-size: 13px;
        }
        .text-bold{
            font-weight: bold;
        }
        
    </style>
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/11.0.2/css/bootstrap-slider.min.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons+Round" rel="stylesheet">
    <!-- Main CSS -->
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>

@endsection

@php
    $vips = env('VIPS');
    $vip = number_format($vips, 0);
    $counter = $vips * 20;
@endphp

@section('content')
<div id="backdrop" style="margin-top: -130px; width: 100%; z-index: 1; display: flex; align-items: center; min-height: 800px; background-position: right;  background-size: cover; margin-bottom: -64px; background-image:url('/assets/img/boarding-pass-bg5.jpg')">
        <div class="gradient-wrap" style="background: transparent; width:100%;">
            <!--  HERO -->
            <div class="section-wrapper black position-relative">
                <div class="row" style="align-items:center;">
                    <div class="col-lg-5 col-md-12 col-sm-12 col-12 top-header" style="display: flex; align-items:center;">
                        <div id="hero-text-wrapper" style="max-width:900px;">
                            <h1 class="mb-4 section-title" style="line-height: 4rem; color: white; font-weight:600 !important; font-size: 3.8rem;"><span></span>
                            Boarding Pass
                            </h1>
                            <h2 class="section-subtitle mb-4" style="color: white; max-width:900px;">
                            We help you travel for free. Enjoy the best deals on airfare, hotels, and more when you build your travel budget with Venti. No annual fee required.</h2>
                         <a href="/register" class="btn btn-primary text-bold">GET STARTED</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<!-- Cover End -->
 <!-- About Start -->

    <section id="" style="padding-top: 100px; padding-bottom: 50px;">
        <div class="container full-width">
            <div class="row">
                <div class="col-md-12  text-center pd-20 text-center" style="padding-bottom: 0; margin-bottom: -3px; padding:40px 20px;" >
                    <h5 style="font-weight: 800; font-size: 16px">WE'RE HELPING TRAVELERS SAVE</h5>
                    <h2 class="section-head-title text-dark mb-3 section-title" style="margin-top:0.5em; font-size: 5rem;">$1 Billion</h2>
                    <h3 class="section-head-body d-block" style="max-width:450px; margin:0 auto;">By offering a savings rate that beats <strong><u>every</u></strong> bank</h3><br>
                    <img src="/assets/img/apy-comparison.png" class="d-block" style="width:100%; max-width:600px; margin:0 auto;">
                    <img src="/assets/img/save-big.jpg" style="width:100%; max-width: 800px; margin-bottom:-87px;">
                </div>
            </div>
        </div>
    </section>

    <section id="about" style="padding-bottom: 0;">
        <div class="container full-width">
            <div class="row" style="background-color: #f7f9ff;">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 text-center pd-20" style="padding-bottom: 0; margin-bottom: -3px; padding:40px;" >
                    <h2 class="section-head-title text-dark mb-3 section-title" style="margin:40px; margin-top:20px">How it Works</h2>
                    <h5 style="font-weight: 800; font-size: 16px; text-transform:uppercase; margin-bottom:20px">SAVING FOR TRAVEL JUST GOT EASIER</h5>
                    <div class="row text-dark text-left pills-item pills-item-1">
                        <div class="col-md-1 text-center">
                            <span class="badge sand-badge">1</span>
                        </div>
                        <div class="col-md-9">
                            <p class="text-dark">Open your digital savings account and make regular deposits to grow your travel budget. With no lock-up period, you can withdraw every dollar from your account at any time.</p>
                        </div>
                        <div class="col-md-2 text-center mb-pd-20" style="display: flex; align-items: center; justify-content:center;">
                            <img src="/assets/img/virtual-wallet.png" style="width:50px;">
                        </div>
                    </div>
                    <div class="row text-dark text-left pills-item pills-item-2">
                        <div class="col-md-1 text-center">
                            <span class="badge sand-badge">2</span>
                        </div>
                        <div class="col-md-9">
                            <p class="text-dark">As your savings grow, we'll reward you with Points weekly. <strong>1 Point = $1 towards travel</strong>. The longer you save, the more Points you earn.</p>
                        </div>
                        <div class="col-md-2 text-center mb-pd-20" style="display: flex; align-items: center; justify-content:center;">
                            <img src="/assets/img/earn-rewards.png" style="width:50px;">
                        </div>
                    </div>
                    <div class="row text-dark text-left pills-item pills-item-3">
                        <div class="col-md-1 text-center">
                            <span class="badge sand-badge">3</span>
                        </div>
                        <div class="col-md-9">
                            <p class="text-dark">Use your Points when you make purchases on Venti. Points never expire and can be transferred to friends and family. <a href="/about/points" target="_blank">Learn more</a>.</p>
                        </div>
                        <div class="col-md-2 text-center mb-pd-20" style="display: flex; align-items: center; justify-content:center;">
                            <img src="/assets/img/tourism.png" style="width:50px;">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <p style="max-width:450px; margin:0 auto; font-size: 12px;">Deposits stored at <a href="https://www.veridiancu.org/faq/12331/is-my-money-insured-through-ncua" target="_blank">Veridian Credit Union</a>, member NCUA. Must be a U.S. resident over the age of 18 to open an account.</p>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 full-width">
                    <img src="/assets/img/saving-money-jar.png" style="width: 100%;" class="">
                </div>
            </div>
            <div class="row" style="background-color:white;">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 full-width mobile-hide">
                    <img src="/assets/img/pays-to-save.jpg" style="width: 100%; height:100%;">
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 text-center" style="padding-bottom: 0; margin-bottom: -3px; padding:40px;" >
                    <h2 class="section-head-title text-dark mb-3 section-title" style="margin:40px; margin-top:20px">It Pays to Save</h2>
                    <h5 style="font-weight: 800; font-size: 16px; text-transform:uppercase; margin-bottom:20px">USE CASH AND POINTS TO GET THESE PRODUCTS UP TO 100% OFF</h5>
                    <div class="row about-body">
                        <div class="about-body-item d-flex mobile-block align-items-center">
                            <div class="tb-full-width">
                                <div class="about-body-icon-cnt">
                                    <i class="material-icons-round about-body-icon fas fa-plane text-small"></i>
                                    <span class="about-body-text-head">AIRFARE</span>
                                </div>
                            </div>
                        </div>
                        <div class="about-body-item d-flex mobile-block align-items-center">
                            <div class="tb-full-width">
                                <div class="about-body-icon-cnt">
                                    <i class="material-icons-round about-body-icon fas fa-ship text-small"></i>
                                    <span class="about-body-text-head">CRUISES</span>
                                </div>
                            </div>
                        </div>
                        <div class="about-body-item d-flex mobile-block align-items-center">
                            <div class="tb-full-width">
                                <div class="about-body-icon-cnt">
                                    <i class="material-icons-round about-body-icon fas fa-hotel text-small"></i>
                                    <span class="about-body-text-head">LODGING</span>
                                </div>
                            </div>
                        </div>
                        <div class="about-body-item d-flex mobile-block align-items-center">
                            <div class="tb-full-width">
                                <div class="about-body-icon-cnt">
                                    <i class="material-icons-round about-body-icon fas fa-car text-small"></i>
                                    <span class="about-body-text-head">RENTALS</span>
                                </div>
                            </div>
                        </div>
                        <div class="about-body-item d-flex mobile-block align-items-center">
                            <div class="tb-full-width">
                                <div class="about-body-icon-cnt">
                                    <i class="material-icons-round about-body-icon fas fa-binoculars text-small"></i>
                                    <span class="about-body-text-head">TOURS</span>
                                </div>
                            </div>
                        </div>
                        <div class="about-body-item d-flex mobile-block align-items-center">
                            <div class="tb-full-width">
                                <div class="about-body-icon-cnt">
                                    <i class="material-icons-round about-body-icon fas fa-skiing text-small"></i>
                                    <span class="about-body-text-head">PACKAGES</span>
                                </div>
                            </div>
                        </div>
                        <div class="about-body-item d-flex mobile-block align-items-center">
                            <div class="tb-full-width">
                                <div class="about-body-icon-cnt">
                                    <i class="material-icons-round about-body-icon fas fa-stethoscope text-small"></i>
                                    <span class="about-body-text-head">INSURANCE</span>
                                </div>
                            </div>
                        </div>
                        <div class="about-body-item d-flex mobile-block align-items-center">
                            <div class="tb-full-width">
                                <div class="about-body-icon-cnt">
                                    <i class="material-icons-round about-body-icon fas fa-headphones text-small"></i>
                                    <span class="about-body-text-head">ELECTRONICS</span>
                                </div>
                            </div>
                        </div>
                        <div class="about-body-item d-flex mobile-block align-items-center">
                            <div class="tb-full-width">
                                <div class="about-body-icon-cnt">
                                    <i class="material-icons-round about-body-icon fas fa-suitcase-rolling text-small"></i>
                                    <span class="about-body-text-head">ACCESSORIES</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    @include('tools.points-calculator')
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 desktop-hide full-width">
                        <img src="/assets/img/pays-to-save.jpg" style="width: 100%;">
                    </div>
                </div>
        </div>
    </section>
    <!-- About End -->

    <section id="preview" style="padding-bottom: 0; background:white; display: none;">
        <div class="container full-width">
            <div class="row" >
                <div class="col-md-6 text-center pd-20" style="flex-direction: column; display: flex; align-self: center; padding-bottom: 0; margin-bottom: -3px; padding:40px;" >
                    <h2 class="section-head-title text-dark mb-3 section-title" style="margin:40px; margin-top:20px">Why Use This <br> Over My Credit Cards?</h2>
                    <h5 style="font-weight: 800; font-size: 16px; text-transform:uppercase; " class=" text-center text-dark">⌛ The Old Way to Earn Points</h5>
                    <div class="row text-dark text-center pills-item pills-item-1" style="max-width: 500px; margin: 0 auto; margin-top: 0px;background-color:rgba(157, 138, 124, 0.2)">
                        <div class="col-md-12">
                            <p class="text-dark ">66 percent of our members have three or more travel credit cards and engage in "Points stacking," meaning they take advantage of the best deals wherever they are, including on Venti.</p>
                        </div>
                    </div>
                    <h5 style="font-weight: 800; font-size: 16px; text-transform:uppercase;" class=" text-center text-dark mt-4">✔️ A New Way to Earn Points</h5>
                    <div class="row text-dark text-center pills-item pills-item-1" style="max-width: 500px; margin: 0 auto; margin-top: 0px;background-color:rgba(157, 138, 124, 0.2)">
                        <div class="col-md-12">
                            <p class="text-dark">Boarding Pass allows you to earn Points from your savings in a way that is easy, fun, rewarding, and serves as a financially healthy alternative. Our Points can be used for any travel product (flight, hotel chain, destination, etc.) on our platform.</p>
                        </div>
                    </div>
                    <br>
                    <div>
                        <h5 style="font-weight: 800; font-size: 16px; text-transform:uppercase;" class=" text-center text-dark">Use Venti Points With</h5>
                        <div class="row">
                            <div class="col-md-4 col-xs-12">
                                <div class="card" style="padding:5px">
                                    <div class="card-body">
                                        <h5 style="padding:0; margin:0;"><i class="fa fa-plane"></i><br><span class="sm-block">130+ Airlines</span></h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-xs-12">
                                <div class="card" style="padding:5px;">
                                    <div class="card-body">
                                        <h5 style="padding:0; margin:0;"><i class="fa fa-hotel"></i><br><span class="sm-block">1+ Million Properties</span></h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-xs-12">
                                <div class="card" style="padding:5px;">
                                    <div class="card-body">
                                        <h5 style=" padding:0; margin:0;"><i class="fa fa-clock"></i><br><span class="sm-block">No Expiration</span></h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 full-width">
                    <img src="/assets/img/credit-card-compare.jpg" style="width: 100%;" class="preview-mh">
                </div>
            </div>
        </div>
    </section>

    <!-- End Preview -->
    <section>
        <div class="container full-width" style="padding-bottom:100px; padding-top: 100px;">
            <h2 class="section-head-title text-dark mb-3 section-title text-center" style="margin:40px; margin-top:0px">Save Big With Points</h2>
            <h5 style="font-weight: 800; font-size: 16px; text-transform:uppercase; " class="mb-4 text-center text-dark">We'll Even Price Match</h5>
            <p class="text-dark text-large mb-4 text-center" style="max-width:600px; margin:0 auto;">If you find a flight or hotel cheaper somewhere else, <strong>we'll match their price</strong> so Venti is always the best deal for you. <br><a href="/about/price-match">Terms and conditions apply</a></p>
            <div class="row justify-content-center" style="padding: 20px; margin-bottom: 50px !important;">
                <div class="col-md-3 mb-3">
                    <div class="card">
                        <div class="card-body">
                            <div style="display:flex; justify-content:space-between; align-items: center;">
                                <img src="https://assets.duffel.com/img/airlines/for-light-background/full-color-logo/TK.svg" style="max-height:45px;">
                                <h2 style="margin:0" class="text-right"><s class="d-block" style="font-size:14px;">$1,166.59</s>$916.60</h2>
                            </div>
                            <p>Round trip flight to Singapore from Miami for one passenger.</p>
                            <a href="/flights/search?from=(MIA)%20Miami%20International%20Airport&to=(SIN)%20Singapore%20Changi%20Airport&type=round-trip&class=economy&start=3/16/2025&end=3/23/2025&adults=1&children=0&infants=0" class="btn btn-white border-primary" target="_blank">Try This Search <i class="fa fa-arrow-right"></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 mb-3">
                    <div class="card">
                        <div class="card-body">
                            <div style="display:flex; justify-content:space-between; align-items: center;">
                                <img src="https://assets.duffel.com/img/airlines/for-light-background/full-color-logo/AA.svg" style="max-height:45px;">
                                <h2 style="margin:0" class="text-right"><s class="d-block" style="font-size:14px;">$524.78</s>$274.78</h2>
                            </div>
                            <p>Round trip flight to Paris from New York for one passenger.</p>
                            <a href="/flights/search?from=(LGA)%20LaGuardia%20Airport&to=(CDG)%20Paris%20Charles%20de%20Gaulle%20Airport&type=round-trip&class=economy&start=3/16/2025&end=3/23/2025&adults=1&children=0&infants=0" class="btn btn-white border-primary" target="_blank">Try This Search <i class="fa fa-arrow-right"></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 mb-3">
                    <div class="card">
                        <div class="card-body">
                            <div style="display:flex; justify-content:space-between; align-items: center;">
                                <img src="https://assets.duffel.com/img/airlines/for-light-background/full-color-logo/UA.svg" style="max-height:45px;">
                                <h2 style="margin:0" class="text-right"><s class="d-block" style="font-size:14px;">$58.48</s>$0</h2>
                            </div>
                            <p>One-way flight to Las Vegas from San Francisco for one passenger.</p>
                            <a href="/flights/search?from=(SFO)%20San%20Francisco%20International%20Airport&to=(LAS)%20Harry%20Reid%20International%20Airport&type=one-way&class=economy&start=3/16/2025&end=undefined&adults=1&children=0&infants=0" class="btn btn-white border-primary" target="_blank">Try This Search <i class="fa fa-arrow-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-1"></div>
                <div class="col-md-5 text-dark text-center pills-item pills-item-1 mb-4 mt-4">
                    <div style="background-color:rgba(157, 138, 124, 0.2); border-radius: 0.325em; padding:20px;">
                        <p class="text-dark">For 2024, all flights below $250 MSRP (direct carrier price) can be purchased for $0 with Points.</p>
                        <a href="/flights" class="btn btn-link border-dark text-dark text-center mb-4 text-bold" style="margin:0 auto;">Search Flights (Beta) <i class="fa fa-plane"></i></a>
                    </div>
                </div>
                <div class="col-md-5 text-dark text-center pills-item pills-item-1 mb-4 mt-4">
                    <div style="background-color:rgba(57, 138, 124, 0.2); border-radius: 0.325em; padding:20px;">
                        <p class="text-dark">For 2024, all hotel bookings under $120 MSRP (direct vendor price) can be purchased for $1 or less. At least 115 Points is required to achieve the discount.</p>
                        <a href="/hotels" class="btn btn-link border-dark text-dark text-center mb-4 text-bold" style="margin:0 auto;">Search Hotels (Beta) <i class="fa fa-hotel"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="waitlist" style="display: none;">
        <div class="container full-width" style="background-image:url('/assets/img/rio-bg.jpg'); background-position: center; background-size:cover; padding-bottom:100px; padding-top:100px">
            <div class="row" >
                <div class="col-md-12  text-center pd-20 full-width" style="margin-bottom: -3px; padding:40px; justify-content: center; padding-bottom: 0;">
                    <h4 class="text-light mb-3">
                        Technology will help travelers journey amongst the stars<br>
                        <span class="text-light pt-4 d-block" style="font-size: 1.5em;">Venti will make it affordable</span>
                    </h4>
                    <h2 class="section-head-title text-dark mb-3 section-title" style="font-size: 4rem; display: none;margin-top:20px; color:white !important">$<span class="" id="currency">10</span></h2>
                    
                </div>
            </div>
            <div class="row" style="justify-content: center;">
                <a href="/register" class="btn btn-primary mt-4 mb-4">🚀 Get Started</a>
            </div>
            <br>
        </div>
    </section>
    <section id="pricing">
        <div class="container full-width" style="padding:50px; background-color: #f7f9ff;">
            <div class="row justify-content-center" style="">
                <div class="col-md-12 text-center">
                    <h2 class="section-head-title text-dark mb-3 section-title" style="font-size: 4rem; margin-top:20px;">Pricing</h2>
                    
                    <h5 class="section-head-body pb-4 justify-content-center text-center" style="max-width: 800px; justify-content:center; margin: 0 auto;">We'll credit your Boarding Pass with 20 Points when you make your first deposit. Receive $10 in Points for each person you invite that makes an initial deposit with your referral link.</h5>
                </div>
                <div class="col-md-11">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-6 col-xl-3 mb-2">
                            @include('boardingpass.components.subscription.buddy-pass')
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-6 col-xl-3 mb-2">
                            @include('boardingpass.components.subscription.priority')
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-6 col-xl-3 mb-2">
                            @include('boardingpass.components.subscription.business')
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-6 col-xl-3 mb-2">
                            @include('boardingpass.components.subscription.first')
                        </div>
                    </div>
                    <p class="text-center pt-4">Benefits marked with <sup>*</sup> are limited to non-discounted accounts only. Rewards back and APY percentages subject to change before and after account creation.</p>
                </div>
            </div>
        </div>
    </section>
    
    
    <!-- About End -->
    <section id="preview-two" style="padding-bottom: 0; background:white; display:none;">
        <div class="container full-width">
            <div class="row" >
                <div class="col-md-6 full-width">
                    <img src="/assets/img/raffle-preview.jpg" style="width: 100%;" class="preview-mh">
                </div>
                <div class="col-md-6 text-center pd-20" style="flex-direction: column; display: flex; align-self: center; padding-bottom: 0; margin-bottom: -3px; padding:40px;" >
                    <h2 class="section-head-title text-dark mb-3 section-title" style="margin:40px; margin-top:20px">Vacation Giveaways</h2>
                    <h5 style="font-weight: 800; font-size: 16px; text-transform:uppercase;">Members are automatically entered to win</h5>
                    <div class="row text-dark text-center pills-item pills-item-1" style="max-width: 500px; margin: 0 auto; margin-top: 20px; background-color: transparent;">
                        <div class="col-md-12">
                            <p class="text-dark text-large">Being a Boarding Pass member means you could win free flights, hotel room nights, and travel-expense paid trips to the best destinations on the planet. There is no entry fee.</p>
                            <a href="/sweepstakes" class="btn btn-primary" target="_blank">2024 Orlando Sweepstakes</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="press" style="padding-bottom:40px; background:white; display: none;">
        <div class="container">
            <div class="row">
                <div class="col-md-12  text-center pd-20 text-center" style="padding-bottom: 0; margin-bottom: -3px; padding:40px;" >
                    <h2 class="section-head-title text-dark mb-3 section-title" style="margin:40px; margin-top:20px">We've Been Featured</h2>
                    <h5 style="font-weight: 800; font-size: 16px; text-transform:uppercase;">SPECIAL THANKS TO OUR FRIENDS AND FELLOW SAVERS</h5>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 text-center">
                    <img src="/assets/img/doctorofcredit.jpg" style="width: 100%; max-width:200px;">
                </div>
                <div class="col-md-4 text-center">
                    <img src="/assets/img/pointsbuzz.jpg" style="width: 100%;  max-width:200px;">
                </div>
                <div class="col-md-4 text-center">
                    <img src="/assets/img/frugalflyer.jpg" style="width: 100%;  max-width:200px;">
                </div>
            </div>
        </div>
    </section>

    <!-- Start FAQs -->
    <section id="faqs" style="padding-bottom:0">
        <div class="container">
            <div class="row">
                <div class="col-md-12  text-center pd-20 text-center" style="padding-bottom: 0; margin-bottom: -3px; padding:40px;" >
                    <h2 class="section-head-title text-dark mb-3 section-title" style="margin:40px; margin-top:20px">FAQs</h2>
                    <h5 style="font-weight: 800; font-size: 16px; text-transform:uppercase;">It's in our DNA to be as transparent as possible</h5>
                </div>
            </div>

            <div class="accordion accordion-flush" id="faqs" style="padding-bottom:0">
              
                <div class="accordion-item">
                <h2 class="accordion-header" id="flush-headingFive">
                  <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseFive" aria-expanded="false" aria-controls="flush-collapseFive">
                    Where exactly are my funds kept? Are my funds FDIC insured?
                  </button>
                </h2>
                <div id="flush-collapseFive" class="accordion-collapse collapse" aria-labelledby="flush-headingFive" data-bs-parent="#accordionFlushExample">
                  <div class="accordion-body"><p>When you make a deposit onto your Boarding Pass, your funds are stored with a trusted third-party custodian that banks with <strong><a href="https://www.veridiancu.org/faq/12331/is-my-money-insured-through-ncua" target="">Veridian Credit Union</a></strong>. By using Venti, you are indirectly a customer of Veridian, which is Federally Insured by the National Credit Union Administration (NCUA), a U.S. Government Agency. Deposits at Veridian are insured up to $250,000. For questions, please contact admin@venti.co</p></div>
                </div>
              </div>
              <div class="accordion-item">
                <h2 class="accordion-header" id="flush-headingEight">
                  <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseEight" aria-expanded="false" aria-controls="flush-collapseEight">
                    Can I add my airline/hotel loyalty rewards number?
                  </button>
                </h2>
                <div id="flush-collapseEight" class="accordion-collapse collapse" aria-labelledby="flush-headingEight" data-bs-parent="#accordionFlushExample">
                  <div class="accordion-body"><p>Absolutely! We strongly suggest you added your rewards account number during checkout to earn additional Points/miles with your favorite brands.</p>
                </div>
                </div>
              </div>
              
              
              <div class="accordion-item">
                <h2 class="accordion-header" id="flush-headingThree">
                  <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseThree" aria-expanded="false" aria-controls="flush-collapseThree">
                    What are the fees for this program?
                  </button>
                </h2>
                <div id="flush-collapseThree" class="accordion-collapse collapse" aria-labelledby="flush-headingThree" data-bs-parent="#accordionFlushExample">
                  <div class="accordion-body"><p>A Boarding Pass initiation fee of (non-recurring) is required for account verification, which is paid via credit card, and varies by package. There are no transaction fees.</p></div>
                </div>
              </div>
              <div class="accordion-item">
                <h2 class="accordion-header" id="flush-headingTen">
                  <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseTen" aria-expanded="false" aria-controls="flush-collapseTen">
                    Do I have to pay taxes on points?
                  </button>
                </h2>
                <div id="flush-collapseTen" class="accordion-collapse collapse" aria-labelledby="flush-headingTen" data-bs-parent="#accordionFlushExample">
                  <div class="accordion-body"><p>We consider a taxable event is created the moment Boarding Pass Points are used to complete a purchase. We will provide a ledger in your account so you can track export for filing purposes. We recommend you speak to a tax professional to review your individual circumstances.</p></div>
                </div>
              </div>
              <div class="accordion-item">
                <h2 class="accordion-header" id="flush-headingNine">
                  <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseNine" aria-expanded="false" aria-controls="flush-collapseNine">
                    Can I exchange my Points for cash?
                  </button>
                </h2>
                <div id="flush-collapseNine" class="accordion-collapse collapse" aria-labelledby="flush-headingNine" data-bs-parent="#accordionFlushExample">
                  <div class="accordion-body"><p>When you purchase <a href="/insurance" target="_blank">travel insurance</a> through our portal at least three times within a calendar year, you'll be able to exchange 100 Points for $100 in Cash Back.</p></div>
                </div>
              </div>
            </div>
            <div class="row">
                <div class="col-md-12 text-center" style="padding:90px 20px; padding-bottom:0;">
                    <h3></h3>
                    <h2 class="section-head-title text-dark mb-3 section-title" style="margin:40px; margin-top:20px">Still Have Questions?</h2>
                    <p style="max-width:800px; margin:0 auto; font-size:1.2em">Say hello by reaching out via our <a href="/contact">contact page</a>.</p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12" style="padding-top:100px; padding-bottom:0">
                    <p>
                        <span class="footnote">*</span> Annual Percentage Yield (APY) is accurate as of 4/3/2024. Select markets only. APY is compounded and credited monthly in the form of Boarding Pass Points. Fees may reduce earnings. Rates are variable and subject to change before and after account opening.
                        <br>
                        <span class="footnote">**</span> Calculated values assume principal and interest remain on deposit and are rounded to the nearest cent. In addition, calculated values use the current APY, which is variable and may change before and after account opening; your savings will be based on APR equivalence, so savings could be less. While there is no limit on interest earned, calculator is designed with certain limits and is for illustrative purposes only.<br>
                        <span class="footnote">***</span> Maximum referral credit (points) is valued at $250 per year with a lifetime cap of $1,000.<br>
                    </p>
                </div>
            </div>
        </div>
    </section>
    <!-- End FAQs -->

    <div id="countdown" style="display:none;">
        <div id="timer">
          <div id="days"></div>
          <div id="hours"></div>
          <div id="minutes"></div>
          <div id="seconds"></div>
        </div>
        <div class="description">
            LEFT TO <a href="/register">JOIN AS VIP</a>
        </div>
    </div>

@endsection


    
@section("js")
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/typeahead.js/0.10.4/typeahead.bundle.min.js"></script>
    @if(env('APP_ENV') != "local")
        <!-- Meta Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s)
            {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};
            if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
            n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t,s)}(window, document,'script',
            'https://connect.facebook.net/en_US/fbevents.js');
            fbq('init', '1169331916805166');
            fbq('track', 'PageView');
        </script>
        <noscript><img height="1" width="1" style="display:none"
        src="https://www.facebook.com/tr?id=1169331916805166&ev=PageView&noscript=1"
        /></noscript>
        <!-- End Meta Pixel Code -->
    @endif
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/11.0.2/bootstrap-slider.min.js"></script>
    <script>
        $(document).ready(function() {
            $.ajaxSetup({
                 headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                 }
              });

            var currency = $("#currency"); //[make sure this is a unique variable name]

             
            /**
             * Format the number, so it will be seperated by comma
             */
            function commaSeparateNumber(val){
                while (/(\d+)(\d{3})/.test(val.toString())){
                  val = val.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
                }
                return val;
            }

            toggleLogo("black","white");

            function toggleLogo(color, replaced){
                var logoSRC = $(".logo").attr("src");
                logoSRC = logoSRC.replace(color, replaced);
                console.log(logoSRC)
                $(".logo").attr("src", logoSRC);
            }

            $.fn.isInViewport = function() {
              var elementTop = $(this).offset().top;
              var elementBottom = elementTop + $(this).outerHeight();

              var viewportTop = $(window).scrollTop();
              var viewportBottom = viewportTop + $(window).height();

              return elementBottom > viewportTop && elementTop < viewportBottom;
            };

            function isElementVisible($elementToBeChecked)
            {
                var TopView = $(window).scrollTop();
                var BotView = TopView + $(window).height();
                var TopElement = $elementToBeChecked.offset().top;
                var BotElement = TopElement + $elementToBeChecked.height();
                return ((BotElement <= BotView) && (TopElement >= TopView));
            }

            $({someValue: 0}).delay(3000).animate({someValue: {{ $counter }} }, {

                duration: 4500,
                easing:'swing', // can be anything
                step: function() { // called on every step
                  // Update the element's text with rounded-up value:
                  currency.text(commaSeparateNumber(Math.round(this.someValue)));
                },
                done: function() {

                }

            });


            $(".nav-icon").each(function(){
                var src = $(this).attr("src");
                console.log(src)
                src = src.replace('.png', '-white-boarding.png');
                $(this).attr("src", src);
              });

            $("#hamburger-menu").click(function(){
                if($("#page_home__show").hasClass("mobile-nav-open")){
                    toggleLogo("white","black");
                } else {
                    toggleLogo("black","white");
                }
                
            });

            $(".calculate").on("change", function(){
                var principal = $("#pool-amount").val() * 2000;

                const time = 1/12; // 1 year
                const rate = 0.0865; // 9% APY
                const n = 1; // compounded monthly
                var withdrawable = principal;
                var points = 0;
                var deposits = ($("#pool-deposits").val() * 50);

                for(i = 0; i < 12; i++){
                    principal += deposits
                    var compoundInterestAdded = compoundInterest(principal, time, rate, n);
                    principal += compoundInterestAdded;
                    points += compoundInterestAdded;
                    withdrawable += deposits;
                }

                $("#balance").text(withdrawable.toLocaleString(undefined, {
                  minimumFractionDigits: 0,
                  maximumFractionDigits: 0
                }));

                $("#points").text(points.toLocaleString(undefined, {
                  minimumFractionDigits: 0,
                  maximumFractionDigits: 0
                }));

                $("#deposit").val($("#pool-amount").val() * 2000);
                $("#contribution").val(deposits);
                $("#balanceFinal").val(withdrawable);
                $("#pointsFinal").val(points);
                
            });

            function compoundInterest(p, t, r, n){
               const amount = p * (Math.pow((1 + (r / n)), (n * t)));
               const interest = amount - p;
               return interest;
            };


          function calculatePayout(poolAmount, poolSize, poolRate){
            console.log("size " + poolSize);
            console.log("amount " + poolAmount);
            console.log("rate " + poolRate);

            var top = poolSize * poolAmount;
            var bottom = poolSize * poolRate;

            console.log("top " + top);
            console.log("bottom " + bottom);

            return top/bottom;
          }

            @if(Session::get('subscribeError') !== null)
                swal({
                    icon: 'error',
                    title: 'Oops!',
                    text: 'We ran into a problem and could not add you to our list. Try again or send an email to markus@venti.co'
                })
            @endif

            makeTimer();

            function makeTimer() {

            var endTime = new Date("1 January 2024 3:59:49 GMT-04:00");          
            endTime = (Date.parse(endTime) / 1000);

            var now = new Date();
            now = (Date.parse(now) / 1000);

            var timeLeft = endTime - now;

            var days = Math.floor(timeLeft / 86400); 
            var hours = Math.floor((timeLeft - (days * 86400)) / 3600);
            var minutes = Math.floor((timeLeft - (days * 86400) - (hours * 3600 )) / 60);
            var seconds = Math.floor((timeLeft - (days * 86400) - (hours * 3600) - (minutes * 60)));
  
            if (hours < "10") { hours = "0" + hours; }
            if (minutes < "10") { minutes = "0" + minutes; }
            if (seconds < "10") { seconds = "0" + seconds; }

            $("#days").html(days + "<span> DAYS</span>");
            $("#hours").html(hours + "<span> HOURS</span>");
            $("#minutes").html(minutes + "<span> MINUTES</span>");
            $("#seconds").html(seconds + "<span style='padding-right:0'> SECONDS</span>");       

    }

    setInterval(function() { makeTimer(); }, 1000);

        });
    </script>

@endsection