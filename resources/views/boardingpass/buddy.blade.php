@extends('layouts.curator-master')

@php
    $selectable = false;
@endphp

@section('css')
    <style type="text/css">
        html{
            overflow-x: hidden;
        }
        @media(max-width: 1368px ){
            .tb-full-width{
                width: 100%;
            }
        }
        .hamburger-menu-list li a{
            color: white;
        }
        .mobile-nav-open .hamburger-menu-list li a{
            color: black;
        }
        .btn-white{
            color: black !important;
        }
        .mobile-nav-open #backdrop{
            margin-top: 0 !important;
        }
        .about-body{
            justify-content: center;
        }
        @media(min-width: 1367px){
            {
                margin-left: 22px;
            }
            .row.about-body{
                margin-bottom: 22px;
            }
            .desktop-hide{
                display: none;
            }
        }
        @media(max-width: 800px){
            .mb-pd-20{
                padding-bottom: 20px;
            }
        }
        @media(max-width: 1368px){
            .mobile-block{
                display: block !important;
            }
            .mobile-hide{
                display: none;
            }
            #about .about-body .about-body-item .about-body-icon-cnt{
                justify-content: left !important;
                margin-bottom: 10px;
            }
            .pills-item{
                background-position-y: inherit !important;
            }
            .sign-up-box{
                padding-top: 80px;
                padding-bottom:  0;
            }
        }
        .about-body-icon-cnt{
            border: 1px solid black;
            border-radius: 20px;
            padding: 5px 10px;
        }
        

        .font-light{
            font-weight: 300 !important;
        }
        .full-width{
            max-width: 9999px !important;
            padding-left:  0;
            padding-right: 0;
        }
        #about{
            padding-top: 0;
        }
        .pd-20{
            padding: 80px 40px;
        }
        i.text-small{
            font-size: 16px;
        }
        .about-body-icon-cnt, .about-body-text-head{
            padding-left: 14px;
        }
        .about-body-text-head{
            padding-right: 15px;
            font-weight: bold;
        }
        @media(min-width: 1367px){
            {
                margin-left: 22px;
            }
            .row.about-body{
                margin-bottom: 22px;
            }
            .desktop-hide{
                display: none;
            }
        }
        @media(max-width: 768px){
            .mobile-hide{
                display: none;
            }


        }
        @media(max-width: 468px){
            #minutes, #seconds{
                display: none;
            }
        }   
        @media(max-width: 1368px){
            .mh-500-sm{
                max-height: 500px;
            }
            .mobile-block{
                display: block !important;
            }
            .mobile-hide{
                display: none;
            }
            #about .about-body .about-body-item .about-body-icon-cnt{
                justify-content: left !important;
                margin-bottom: 10px;
            }
            .pills-item{
                background-position-y: inherit !important;
            }
            .sign-up-box{
                padding-top: 80px;
                padding-bottom:  0;
            }
            .preview-mh{
                width: 143% !important;
                margin-left: -73px;
            }
        }

        .text-gold{
            color: #9D8A7C !important;
        }

        .text-large{
            font-size: 20px;
        }

        ol.pills-list{
            display: block;
            padding: 0 0 0 26px;
            list-style: none;
            overflow: hidden;
        }

        .pills-list li{
            display: flex;
            justify-content: space-between;
            align-items: center;
            margin-bottom: 10px;
            padding: 10px 20px 5px 20px;
        }
        .sand-badge{
            background-color: rgba(157, 138, 124, 0.7);
            opacity: 0.8;
            color: white;
            font-size: 22px;
            border-radius: 50%;
            width: 42px;
            height: 40px;
            letter-spacing: 1;
        }
        .pills-item{
            padding: 7px 20px;
            border-radius: 10px;
            margin-bottom: 20px;
            background-color: whitesmoke;
        }
        .pills-item div{
            padding-top: 10px;
        }
        .pills-item p{
            padding-top: 3px;
        }
        .pills-item-1{
            background-position-y: 0;
        }

        .pills-item-2{
            background-position-y: -90px;
        }
        .pills-item-3{
            background-position-y: -180px;
        }
        .pills-item-4{
            background-position-y: -220px;
        }
        li.pills-item-3{
            background: #d7d0ca;
        }
        #mc_embed_signup .text-small{
            font-size: 12px !important;
        }

        #subscribe .text-small{
            margin-top: 15px;
            
        }
        .about-body-item{
            margin-bottom: 10px;
        }.slider-horizontal{
            width: 100% !important;
        }
        #faqs
        {
        padding-bottom: 100px;
        }
        #faqs .faqs-container .faq
        {
            border-top: 1px solid var(--color-border);
            padding: 15px;
        }
        #faqs .faqs-container .faq .faq-head
        {
            padding: 15px 0px;
            position: relative;
        }
        #faqs .faqs-container .faq .faq-head h5
        {
            width: 90%;
            cursor: pointer;
        }
        #faqs .faqs-container .faq .faq-head h5::before
        {
            content: 'keyboard_arrow_down';
            text-align: center;
            font-size: 20px;
            line-height: 27px;
            font-family: 'Material Icons Round';
            height: 25px;
            width: 25px;
            color: #fefefe;
            background: var(--color-sec);
            position: absolute;
            top: 50%;
            right: 0;
            transform: translateY(-50%);
            border-radius: 50%;
            transition: 0.5s;
        }
        #faqs .faqs-container .faq .faq-head h5[aria-expanded=true]::before
        {
            content: 'keyboard_arrow_up';
        }
        .form-container .emailoctopus-form-wrapper .emailoctopus-success-message{
            color:  orange !important;
            font-size: 18px;
            padding-bottom: 20px;
        }
        .accordion-item{
            margin-bottom: 10px;
        }
        #countdown{
            position: fixed;
            z-index: 999;
            height: 50px;
            background: white;
            width: 100%;
            bottom: 0;
            left: 0;
            display: flex;
            flex-direction: row;
            justify-content: center;
            padding: 10px 20px;
            font-weight: bold;
            font-size: 0.9em;
            align-items: center;
        }
        #countdown div.descrtiption{
            text-align: center;
            font-size: 0.8em;
            padding: 10px;
        }
        #timer{
            font-weight: bold;
            display: flex;
            justify-content: space-between;
        }
        #timer *{
            padding-right: 4px;
        }

        .about-body-icon-cnt{
            border: 1px solid rgba(0,0,0,0.1);
        }
        .footnote{
            width: 30px;
            display: inline-block;
        }
        html body{
            background: #F2F2F6 !important;
        }
        .features ul {
            list-style: none;
            padding: 0;
        }
        .features ul li{
            padding: 10px;
            border-bottom: 1px solid whitesmoke;
            font-size: 13px;
        }
        .text-bold{
            font-weight: bold;
        }
    </style>
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/11.0.2/css/bootstrap-slider.min.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons+Round" rel="stylesheet">
    <!-- Main CSS -->
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>

@endsection

@php
    $vips = env('VIPS');
    $vip = number_format($vips, 0);
    $counter = $vips * 20;
@endphp

@section('content')
<div id="backdrop" style="margin-top: -130px; width: 100%; z-index: 1; display: flex; align-items: center; min-height: 800px; background-position: center;  background-size: cover; margin-bottom: -64px; background-image:url('/assets/img/boarding-pass-bg2.jpg')">
        <div class="gradient-wrap" style="background: transparent; width:100%;">
            <!--  HERO -->
            <div class="section-wrapper black position-relative">
                <div class="row" style="align-items:center;">
                    <div class="col-lg-5 col-md-12 col-sm-12 col-12 top-header" style="display: flex; align-items:center;">
                        <div id="hero-text-wrapper" style="max-width:900px;">
                            <h1 class="mb-3 section-title" style="line-height: 4rem; color: white; font-weight:600 !important; font-size: 3.8rem;">
                            Up to 2.5% Back<sup>*</sup>
                            <span style="font-size: 0.45em; display:block;">On All Travel Purchases with a Buddy Pass</span>
                            </h1>
                            <h2 class="section-subtitle mb-4" style="color: white; max-width:900px;">
                            Your gat</h2>
                            <a href="/register" class="btn btn-primary">REGISTER</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<!-- Cover End -->
 <!-- About Start -->

 <!-- Start FAQs -->
    <section id="" style="padding-top: 100px; padding-bottom: 50px;">
        <div class="container full-width">
            <div class="row">
                <div class="col-md-12  text-center pd-20 text-center" style="padding-bottom: 0; margin-bottom: -3px; padding:40px 20px;" >
                    <h5 style="font-weight: 800; font-size: 16px">WE'RE ON A MISSION TO SAVE TRAVELERS</h5>
                    <h2 class="section-head-title text-dark mb-3 section-title" style="margin-top:0.5em; font-size: 5rem;">$1 Billion</h2>
                    <h3 class="section-head-body">With the best discounts anywhere</h3>
                </div>
            </div>
        </div>
    </section>
    <!-- End FAQs -->

    <section id="about" style="padding-bottom: 0;">
        <div class="container full-width">
            <div class="row" style="background-color: #f7f9ff;">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 text-center pd-20" style="padding-bottom: 0; margin-bottom: -3px; padding:40px;" >
                    <h2 class="section-head-title text-dark mb-3 section-title" style="margin:40px; margin-top:20px">How it Works</h2>
                    <div class="row text-dark text-left pills-item pills-item-1">
                        <div class="col-md-1 text-center">
                            <span class="badge sand-badge">1</span>
                        </div>
                        <div class="col-md-9">
                            <p class="text-dark text-large">You create a free account and purchase flights, hotels, cruises, etc. using your bank account. All purchases are made via ACH. By ditching credit card fees, we pass on extra savings to you.</p>
                        </div>
                        <div class="col-md-2 text-center mb-pd-20" style="display: flex; align-items: center; justify-content:center;">
                            <img src="/assets/img/virtual-wallet.png" style="width:50px;">
                        </div>
                    </div>
                    <div class="row text-dark text-left pills-item pills-item-2">
                        <div class="col-md-1 text-center">
                            <span class="badge sand-badge">2</span>
                        </div>
                        <div class="col-md-9">
                            <p class="text-dark text-large">You can up earn up to $2.50 for every $100 you spend on travel in the form of Points. <strong>1 Point = $1 Towards Travel</strong>.</p>
                        </div>
                        <div class="col-md-2 text-center mb-pd-20" style="display: flex; align-items: center; justify-content:center;">
                            <img src="/assets/img/earn-rewards.png" style="width:50px;">
                        </div>
                    </div>
                    <div class="row text-dark text-left pills-item pills-item-3">
                        <div class="col-md-1 text-center">
                            <span class="badge sand-badge">3</span>
                        </div>
                        <div class="col-md-9">
                            <p class="text-dark text-large">Use your points when you make purchases on Venti. Points never expire and can help you buy down the cost of travel by up to 100% <a href="/about/points" target="_blank">Learn more</a>.</p>
                        </div>
                        <div class="col-md-2 text-center mb-pd-20" style="display: flex; align-items: center; justify-content:center;">
                            <img src="/assets/img/tourism.png" style="width:50px;">
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 full-width">
                    <img src="/assets/img/how-it-works-m2.jpg" style="width: 100%;" class="">
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 full-width mobile-hide">
                    <img src="/assets/img/pays-to-save.jpg" style="width: 100%; height:100%;">
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 text-center" style="padding-bottom: 0; margin-bottom: -3px; padding:40px;" >
                    <h2 class="section-head-title text-dark mb-3 section-title" style="margin:40px; margin-top:20px">It Pays to Save</h2>
                    <h5 style="font-weight: 800; font-size: 16px; text-transform:uppercase; margin-bottom:20px">Buy down the cost of these travel products on Venti by up to 100%.</h5>
                    <div class="row about-body">
                        <div class="about-body-item d-flex mobile-block align-items-center">
                            <div class="tb-full-width">
                                <div class="about-body-icon-cnt">
                                    <i class="material-icons-round about-body-icon fas fa-plane text-small"></i>
                                    <span class="about-body-text-head">AIRFARE</span>
                                </div>
                            </div>
                        </div>
                        <div class="about-body-item d-flex mobile-block align-items-center">
                            <div class="tb-full-width">
                                <div class="about-body-icon-cnt">
                                    <i class="material-icons-round about-body-icon fas fa-ship text-small"></i>
                                    <span class="about-body-text-head">CRUISES</span>
                                </div>
                            </div>
                        </div>
                        <div class="about-body-item d-flex mobile-block align-items-center">
                            <div class="tb-full-width">
                                <div class="about-body-icon-cnt">
                                    <i class="material-icons-round about-body-icon fas fa-hotel text-small"></i>
                                    <span class="about-body-text-head">LODGING</span>
                                </div>
                            </div>
                        </div>
                        <div class="about-body-item d-flex mobile-block align-items-center">
                            <div class="tb-full-width">
                                <div class="about-body-icon-cnt">
                                    <i class="material-icons-round about-body-icon fas fa-car text-small"></i>
                                    <span class="about-body-text-head">RENTALS</span>
                                </div>
                            </div>
                        </div>
                        <div class="about-body-item d-flex mobile-block align-items-center">
                            <div class="tb-full-width">
                                <div class="about-body-icon-cnt">
                                    <i class="material-icons-round about-body-icon fas fa-binoculars text-small"></i>
                                    <span class="about-body-text-head">TOURS</span>
                                </div>
                            </div>
                        </div>
                        <div class="about-body-item d-flex mobile-block align-items-center">
                            <div class="tb-full-width">
                                <div class="about-body-icon-cnt">
                                    <i class="material-icons-round about-body-icon fas fa-skiing text-small"></i>
                                    <span class="about-body-text-head">PACKAGES</span>
                                </div>
                            </div>
                        </div>
                        <div class="about-body-item d-flex mobile-block align-items-center">
                            <div class="tb-full-width">
                                <div class="about-body-icon-cnt">
                                    <i class="material-icons-round about-body-icon fas fa-stethoscope text-small"></i>
                                    <span class="about-body-text-head">INSURANCE</span>
                                </div>
                            </div>
                        </div>
                        <div class="about-body-item d-flex mobile-block align-items-center">
                            <div class="tb-full-width">
                                <div class="about-body-icon-cnt">
                                    <i class="material-icons-round about-body-icon fas fa-headphones text-small"></i>
                                    <span class="about-body-text-head">ELECTRONICS</span>
                                </div>
                            </div>
                        </div>
                        <div class="about-body-item d-flex mobile-block align-items-center">
                            <div class="tb-full-width">
                                <div class="about-body-icon-cnt">
                                    <i class="material-icons-round about-body-icon fas fa-suitcase-rolling text-small"></i>
                                    <span class="about-body-text-head">ACCESSORIES</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                        
                    </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 desktop-hide full-width">
                    <img src="/assets/img/pays-to-save.jpg" style="width: 100%;">
                </div>
            </div>
            
        </div>
    </section>
    <!-- About End -->

    <!-- End Preview -->

    <section id="waitlist">
        <div class="container full-width" style="background-image:url('/assets/img/rio-bg.jpg'); background-position: center; background-size:cover; padding-bottom:100px; padding-top:100px">
            <div class="row" >
                <div class="col-md-12  text-center pd-20 full-width" style="margin-bottom: -3px; padding:40px; justify-content: center; padding-bottom: 0;">
                    <h4 class="text-light mb-3">
                        Technology will help travelers journey amongst the stars<br>
                        <span class="text-light pt-4 d-block" style="font-size: 1.5em;">Venti will make it affordable</span>
                    </h4>
                    <h2 class="section-head-title text-dark mb-3 section-title" style="font-size: 4rem; display: none;margin-top:20px; color:white !important">$<span class="" id="currency">10</span></h2>
                    <h5 class="section-head-body" style="color:white; max-width: 800px; justify-content:center; margin: 0 auto;"></h5>
                </div>
            </div>
            <div class="row" style="justify-content: center;">
                <a href="/register" class="btn btn-primary mt-4 mb-4">🚀 Get Started</a>
            </div>
            <br>
        </div>
    </section>
    <section id="pricing">
        <div class="container full-width" style="padding:50px">
            <div class="row justify-content-center">
                <div class="col-md-12 text-center">
                    <h2 class="section-head-title text-dark mb-3 section-title" style="font-size: 4rem; margin-top:20px;">Our Plans</h2>
                    <h5 class="section-head-body pb-4 justify-content-center text-center" style="max-width:750px; margin:0 auto;">A Boarding Pass is required to purchase flights, hotels, and more on our platform. VIP Members get one year of First Class APY when they join by Dec. 31, 2023. Depending on your travel style, you're almost guaranteed to cover the cost of each plan with your first flight or hotel purchase. A subscription will be required to open a savings account in 2024. Priority and Business Class will launch in early 2024.</h5>
                </div>
                <div class="col-md-11">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-6 col-xl-3 mb-2">
                            @include('boardingpass.components.subscription.buddy-pass')
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-6 col-xl-3 mb-2">
                            @include('boardingpass.components.subscription.priority')
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-6 col-xl-3 mb-2">
                            @include('boardingpass.components.subscription.business')
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-6 col-xl-3 mb-2">
                            @include('boardingpass.components.subscription.first')
                        </div>
                    </div>
                    <p class="text-center pt-4">Benefits marked with <sup>*</sup> are limited to paid accounts only. Rewards back and APY percentages subject to change before and after account creation.</p>
                </div>
            </div>
        </div>
    </section>
    <section id="preview" style="padding-bottom: 0;">
        <div class="container full-width">
            <div class="row" style="background-color: #f7f9ff;">
                <div class="col-md-6 text-center pd-20" style="flex-direction: column; display: flex; align-self: center; padding-bottom: 0; margin-bottom: -3px; padding:40px;" >
                    <h2 class="section-head-title text-dark mb-3 section-title" style="margin:40px; margin-top:20px">Track. Book. Save.</h2>
                    <h5 style="font-weight: 800; font-size: 16px; text-transform:uppercase;">Always on the go. Just like you.</h5>
                    <div class="row text-dark text-center pills-item pills-item-1" style="max-width: 500px; margin: 0 auto; margin-top: 20px;background-color:rgba(157, 138, 124, 0.2)">
                        <div class="col-md-12">
                            <p class="text-dark text-large">Track your savings progress, redeem points, and send/receive funds right from a convenient and secure mobile app. <br><br> <span style="font-weight:bold">Coming soon to iOS and Android</span></p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 full-width">
                    <img src="/assets/img/app-preview.png" style="width: 100%;" class="preview-mh">
                </div>
            </div>
            
        </div>
    </section>
    <!-- About End -->
    <div id="countdown">
        <div id="timer">
          <div id="days"></div>
          <div id="hours"></div>
          <div id="minutes"></div>
          <div id="seconds"></div>
        </div>
        <div class="description">
            LEFT TO <a href="/boardingpass">JOIN AS VIP</a>
        </div>
    </div>

@endsection


    
@section("js")
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/typeahead.js/0.10.4/typeahead.bundle.min.js"></script>
    @if(env('APP_ENV') != "local")
        <!-- Meta Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s)
            {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};
            if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
            n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t,s)}(window, document,'script',
            'https://connect.facebook.net/en_US/fbevents.js');
            fbq('init', '1169331916805166');
            fbq('track', 'PageView');
        </script>
        <noscript><img height="1" width="1" style="display:none"
        src="https://www.facebook.com/tr?id=1169331916805166&ev=PageView&noscript=1"
        /></noscript>
        <!-- End Meta Pixel Code -->
    @endif
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/11.0.2/bootstrap-slider.min.js"></script>
    <script>
        $(document).ready(function() {
            $.ajaxSetup({
                 headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                 }
              });

            var currency = $("#currency"); //[make sure this is a unique variable name]

             
            /**
             * Format the number, so it will be seperated by comma
             */
            function commaSeparateNumber(val){
                while (/(\d+)(\d{3})/.test(val.toString())){
                  val = val.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
                }
                return val;
            }

            toggleLogo("black","white");

            function toggleLogo(color, replaced){
                var logoSRC = $(".logo").attr("src");
                logoSRC = logoSRC.replace(color, replaced);
                console.log(logoSRC)
                $(".logo").attr("src", logoSRC);
            }

            $.fn.isInViewport = function() {
              var elementTop = $(this).offset().top;
              var elementBottom = elementTop + $(this).outerHeight();

              var viewportTop = $(window).scrollTop();
              var viewportBottom = viewportTop + $(window).height();

              return elementBottom > viewportTop && elementTop < viewportBottom;
            };

            


            function isElementVisible($elementToBeChecked)
            {
                var TopView = $(window).scrollTop();
                var BotView = TopView + $(window).height();
                var TopElement = $elementToBeChecked.offset().top;
                var BotElement = TopElement + $elementToBeChecked.height();
                return ((BotElement <= BotView) && (TopElement >= TopView));
            }

            $({someValue: 0}).delay(3000).animate({someValue: {{ $counter }} }, {

                duration: 4500,
                easing:'swing', // can be anything
                step: function() { // called on every step
                  // Update the element's text with rounded-up value:
                  currency.text(commaSeparateNumber(Math.round(this.someValue)));
                },
                done: function() {

                }

            });


            $(".nav-icon").each(function(){
                var src = $(this).attr("src");
                console.log(src)
                src = src.replace('.png', '-white-boarding.png');
                $(this).attr("src", src);
              });

            $("#hamburger-menu").click(function(){
                if($("#page_home__show").hasClass("mobile-nav-open")){
                    toggleLogo("white","black");
                } else {
                    toggleLogo("black","white");
                }
                
            });

            $(".calculate").on("change", function(){
                var principal = $("#pool-amount").val() * 2000;

                const time = 1/12; // 1 year
                const rate = 0.0865; // 9% APY
                const n = 1; // compounded monthly
                var withdrawable = principal;
                var points = 0;
                var deposits = ($("#pool-deposits").val() * 50);

                for(i = 0; i < 12; i++){
                    principal += deposits
                    var compoundInterestAdded = compoundInterest(principal, time, rate, n);
                    principal += compoundInterestAdded;
                    points += compoundInterestAdded;
                    withdrawable += deposits;
                }

                $("#balance").text(withdrawable.toLocaleString(undefined, {
                  minimumFractionDigits: 0,
                  maximumFractionDigits: 0
                }));

                $("#points").text(points.toLocaleString(undefined, {
                  minimumFractionDigits: 0,
                  maximumFractionDigits: 0
                }));

                $("#deposit").val($("#pool-amount").val() * 2000);
                $("#contribution").val(deposits);
                $("#balanceFinal").val(withdrawable);
                $("#pointsFinal").val(points);
                
            });

            function compoundInterest(p, t, r, n){
               const amount = p * (Math.pow((1 + (r / n)), (n * t)));
               const interest = amount - p;
               return interest;
            };


          function calculatePayout(poolAmount, poolSize, poolRate){
            console.log("size " + poolSize);
            console.log("amount " + poolAmount);
            console.log("rate " + poolRate);

            var top = poolSize * poolAmount;
            var bottom = poolSize * poolRate;

            console.log("top " + top);
            console.log("bottom " + bottom);

            return top/bottom;
          }

            @if(Session::get('subscribeError') !== null)
                swal({
                    icon: 'error',
                    title: 'Oops!',
                    text: 'We ran into a problem and could not add you to our list. Try again or send an email to markus@venti.co'
                })
            @endif

            makeTimer();

            function makeTimer() {

            var endTime = new Date("31 December 2023 23:59:49 GMT-04:00");          
            endTime = (Date.parse(endTime) / 1000);

            var now = new Date();
            now = (Date.parse(now) / 1000);

            var timeLeft = endTime - now;

            var days = Math.floor(timeLeft / 86400); 
            var hours = Math.floor((timeLeft - (days * 86400)) / 3600);
            var minutes = Math.floor((timeLeft - (days * 86400) - (hours * 3600 )) / 60);
            var seconds = Math.floor((timeLeft - (days * 86400) - (hours * 3600) - (minutes * 60)));
  
            if (hours < "10") { hours = "0" + hours; }
            if (minutes < "10") { minutes = "0" + minutes; }
            if (seconds < "10") { seconds = "0" + seconds; }

            $("#days").html(days + "<span> DAYS</span>");
            $("#hours").html(hours + "<span> HOURS</span>");
            $("#minutes").html(minutes + "<span> MINUTES</span>");
            $("#seconds").html(seconds + "<span style='padding-right:0'> SECONDS</span>");       

    }

    setInterval(function() { makeTimer(); }, 1000);

        });
    </script>

@endsection