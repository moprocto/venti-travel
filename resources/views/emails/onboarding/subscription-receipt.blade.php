@extends("emails.layouts.generic")

@section("header")


<a href="https://venti.co/airlock" style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; color: #3d4852; font-size: 19px; font-weight: bold; text-decoration: none; display: inline-block;">
<img src="https://venti.co/assets/img/airlock-email-receipt.jpg" class="logo" alt="Venti Logo" style="border-top-left-radius: 20px; border-top-right-radius: 20px; box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif,'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; max-width: 100%; border: none;  max-width: 570px;">
</a>

@endsection

@section("body")
<h1 style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; color: #3d4852; font-size: 34px; font-weight: bold; margin-top: 0; text-align: left;">Thank You for Your Purchase!</h1>
<p style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; font-size: 16px; line-height: 1.5em; margin-top: 0; text-align: left;">Your Airlock Protection Plan is now active. Here's a summary of your subscription:</p>

<table width="100%" cellpadding="0" cellspacing="0" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
    <tbody>
        <tr>
            <td style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; border-top-width: 1px; border-top-color: #eee; border-top-style: solid; margin: 0; padding: 5px 0; text-align:left;" valign="top">
                Plan Type: {{ ucfirst($data["product"]) }}
            </td>
            <td class="alignright" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: right; border-top-width: 1px; border-top-color: #eee; border-top-style: solid; margin: 0; padding: 5px 0;" align="right" valign="top">
                $ {{ $data["fee"] }}/{{ $data["frequency"] }}
            </td>
        </tr>
        <tr class="total">
            <td colspan="2" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; border-top-width: 2px; border-top-color: #333; border-top-style: solid; margin: 0; padding: 5px 0;" valign="top">
                <p>Transaction Date: {{ Date("F d, Y") }}</p>
                <p>Plan ID: {{ $data["plan_id"] }}</p>
            </td>
        </tr>
    </tbody>
</table>

<p style="margin-top: 20px;">You can manage your subscription and view your protection details by logging into your Airlock dashboard.</p>
@endsection

@section("footer")
<table class="inner-body" align="center" width="333">
	
</table>
@endsection