@extends("emails.layouts.generic")

@section("header")


<a href="https://venti.co" style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; color: #3d4852; font-size: 19px; font-weight: bold; text-decoration: none; display: inline-block;">
<img src="https://venti.co/assets/img/boarding-pass-email-receipt.jpg" class="logo" alt="Venti Logo" style="border-top-left-radius: 20px; border-top-right-radius: 20px; box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif,'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; max-width: 100%; border: none;  max-width: 570px;">
</a>

@endsection

@section("body")
<h1 style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; color: #3d4852; font-size: 34px; font-weight: bold; margin-top: 0; text-align: left; margin-bottom: 0; padding-bottom:0;">Order Confirmed!</h1>
<p style="padding:0; margin:0"><strong>Confirmation ID: {{ $data["booking_reference"] }}</strong></p>
<p style="padding:0; margin:0"><strong>Order Date: {{ \Carbon\Carbon::parse($data["timestamp"])->timezone($data["timezone"])->format("F d, Y")  }}</strong></p>
<br>
<p style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; font-size: 16px; line-height: 1.5em; margin-top: 0; text-align: left;">You have successfully booked your flight with {{ $data["order"]["owner"]["name"] }} to {{ $data["destination"] }}. You can track this <a href="{{ env('APP_URL') }}/trips/{{ $data['refID'] }}" target="_blank">booking online</a> for updates and to request changes.</p>
<table width="100%" cellpadding="0" cellspacing="0" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;"><tbody>
<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;"><td class="content-block" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top">
	<h2 class="aligncenter" style="font-family: 'Helvetica Neue',Helvetica,Arial,'Lucida Grande',sans-serif; box-sizing: border-box; font-size: 24px; color: #000; line-height: 1.2em; font-weight: 400; text-align: center; margin: 10px 0 0;" align="center"></h2>
</td>
</tr>
<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
	<td class="content-block aligncenter" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: center; margin: 0; padding: 0 0 20px;" align="center" valign="top">
		<table class="invoice-items" cellpadding="0" cellspacing="0" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; width: 100%; margin: 0;">
			<tbody>
				<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
					<td style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; border-top-width: 1px; border-top-color: #eee; border-top-style: solid; margin: 0; padding: 5px 0; text-align:left;" valign="top">Base Fare:</td>
					<td class="alignright" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: right; border-top-width: 1px; border-top-color: #eee; border-top-style: solid; margin: 0; padding: 5px 0;" align="right" valign="top">$ {{ number_format($data["base_amount"],2) }}</td>
				</tr>
				<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
					<td style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; border-top-width: 1px; border-top-color: #eee; border-top-style: solid; margin: 0; padding: 5px 0; text-align:left;" valign="top">Taxes:</td>
					<td class="alignright" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: right; border-top-width: 1px; border-top-color: #eee; border-top-style: solid; margin: 0; padding: 5px 0;" align="right" valign="top">$ {{ number_format($data["tax_amount"],2) }}</td>
				</tr>
				@if($data["seatUpgrades"])
					@foreach($data["seatUpgrades"] as $seatUpgrade)
						<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
							<td style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; border-top-width: 1px; border-top-color: #eee; border-top-style: solid; margin: 0; padding: 5px 0; text-align:left;" valign="top">Seat Selection: {{ $seatUpgrade["metadata"]["designator"] ?? "" }}</td>
							<td class="alignright" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: right; border-top-width: 1px; border-top-color: #eee; border-top-style: solid; margin: 0; padding: 5px 0;" align="right" valign="top">$ {{ number_format($seatUpgrade['newPrice'],2) }}</td>
						</tr>
					@endforeach
				@endif
				@if($data["bagUpgrades"])
					@foreach($data["bagUpgrades"] as $bagUpgrade)
						<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
							<td style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; border-top-width: 1px; border-top-color: #eee; border-top-style: solid; margin: 0; padding: 5px 0; text-align:left;" valign="top">Additional Checked Bag</td>
							<td class="alignright" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: right; border-top-width: 1px; border-top-color: #eee; border-top-style: solid; margin: 0; padding: 5px 0;" align="right" valign="top">$ {{ number_format($bagUpgrade['newPrice'],2) }}</td>
						</tr>
					@endforeach
				@endif
				@if($data["airlock"])
					<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
						<td style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; border-top-width: 1px; border-top-color: #eee; border-top-style: solid; margin: 0; padding: 5px 0; text-align:left;" valign="top">Airlock Flight Protection</td>
						<td class="alignright" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: right; border-top-width: 1px; border-top-color: #eee; border-top-style: solid; margin: 0; padding: 5px 0;" align="right" valign="top">$ {{ number_format($data["airlockPrice"],2) }}</td>
					</tr>
				@endif
				<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
					<td style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; border-top-width: 2px; border-top-color: #eee; border-top-style: solid; margin: 0; padding: 5px 0; text-align:left;" valign="top">Amount Paid From {{ $data["from"] ?? "My Boarding Pass" }}:</td>
					<td class="alignright" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: right; border-top-width: 2px; border-top-color: #eee; border-top-style: solid; margin: 0; padding: 5px 0;" align="right" valign="top">$ {{ number_format($data["paidAmount"],2) }}</td>
				</tr>
				@if($data["pointsApplied"] > 0)
					<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
						<td style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; border-top-width: 1px; border-top-color: #eee; border-top-style: solid; margin: 0; padding: 5px 0; text-align:left;" valign="top">Amount Paid From Points Balance</td>
						<td class="alignright" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: right; border-top-width: 1px; border-top-color: #eee; border-top-style: solid; margin: 0; padding: 5px 0;" align="right" valign="top">{{ number_format($data["pointsApplied"],2) }}</td>
					</tr>
				@endif
				<tr class="total" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
					<td class="alignright" width="80%" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: right; border-top-width: 2px; border-top-color: #333; border-top-style:solid; font-weight: 700; margin: 0; padding: 5px 0;" align="right" valign="top">Total @if($data["pointsApplied"] > 0)<br><span style="font-size:10px;">Includes Cash-Equivalent Value of Points</span>@endif</td>
					<td class="alignright" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: right; border-top-width: 2px; border-top-color: #333; border-top-style: solid; font-weight: 700; margin: 0; padding: 5px 0;" align="right" valign="top">$ {{ number_format($data["paidAmount"] + $data["pointsApplied"],2) }}</td>
				</tr>
			</tbody>
		</table>
	</td>

</tr>
</tbody>
</table>
<p style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; font-size: 12px; line-height: 1.5em; margin-top: 0; text-align: right;">This email serves as your receipt. For post-booking support, please send an email to admin@venti.co, and include your confirmation ID.</p>
@endsection

@section("content")
	
	@if($data["airlock"])
		<table class="inner-body" align="center" width="570" cellpadding="0" cellspacing="0" role="presentation" style="margin-top: 30px; box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; -premailer-cellpadding: 0; -premailer-cellspacing: 0; -premailer-width: 570px; background-color: #ffffff; border-color: #e8e5ef; border-radius: 2px; border-width: 1px; box-shadow: 0 2px 0 rgba(0, 0, 150, 0.025), 2px 4px 0 rgba(0, 0, 150, 0.015); margin: 0 auto; padding: 0; width: 570px;">
       <!-- Body content -->
	       <tr>
	          <td class="content-cell" style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position:relative; max-width: 100vw; padding: 32px;">
	             <h2>Airlock Flight Protection</h2>
	          </td>
	          <td>
	          	<img src="https://venti.co/assets/img/icons/airlock-active.png" style="width:75px; height:75px;">
	          </td>
	       </tr>
	       <tr>
	       		<td colspan="2">
	       			<p style="padding: 33px; padding-top: 0; box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; font-size: 18px; line-height: 1.5em; margin-top: 0;">Congratulations! You protected this purchase with our premium flight protection. Life happens, and whenever you need to cancel your trip and get a refund, simply initiate cancelation from your <a href="https://venti.co/trips" target="_blank">Trips Page</a>. No need to call, email, or fight with a call center.<br><br>
	       			Airlock protection is non-refundable. You cannot activate Airlock after your trip's departure date. If the airline changes the departure date for your flight, your Airlock expiry will be updated to match the new departure date and time.</p>
	       		</td>		
	       </tr>
	    </table>
	    <br>
	@endif
	
	 <table class="inner-body" align="center" width="570" cellpadding="0" cellspacing="0" role="presentation" style="margin-top: 30px; box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; -premailer-cellpadding: 0; -premailer-cellspacing: 0; -premailer-width: 570px; background-color: #ffffff; border-color: #e8e5ef; border-radius: 2px; border-width: 1px; box-shadow: 0 2px 0 rgba(0, 0, 150, 0.025), 2px 4px 0 rgba(0, 0, 150, 0.015); margin: 0 auto; padding: 0; width: 570px;">
       <!-- Body content -->
       <tr>
          <td class="content-cell" style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position:relative; max-width: 100vw; padding: 32px;">
             <h2>Itinerary</h2>
             @include("flights.components.flight-plan",[
             	"baggages" => 0,
             	"numSlices" => sizeof($data["order"]["slices"]),
             	"i" => 0,
             	"data" => [
             		"slices" => $data["order"]["slices"],
             		"total_amount" => 0,
             		"iata_destination" => $data["destination"],
             		"destination" => $data["destination"]
             	]
             ])
             <p>Note: Airlines may initiate itinerary changes leading up to departure. Any changes to your itinerary will be sent via email to {{ $data["user"]->email }} and made available for view on your <a href="https://venti.co/trips" target="_blank">Trips Page</a>.</p>
          </td>
       </tr>
    </table>
    <br>
    <table class="inner-body" align="center" width="570" cellpadding="0" cellspacing="0" role="presentation" style="margin-top: 30px; box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; -premailer-cellpadding: 0; -premailer-cellspacing: 0; -premailer-width: 570px; background-color: #ffffff; border-color: #e8e5ef; border-radius: 2px; border-width: 1px; box-shadow: 0 2px 0 rgba(0, 0, 150, 0.025), 2px 4px 0 rgba(0, 0, 150, 0.015); margin: 0 auto; padding: 0; width: 570px;">
       <!-- Body content -->
       <tr>
          <td class="content-cell" style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position:relative; max-width: 100vw; padding: 32px;">
             	<h2>Need Travel Insurance?</h2>
             	<p style="padding:0; margin:0; color:black;">
             		We've partnered with Visitors Coverage so you can get an affordable insurance quote in about 5 minutes. Boarding Pass members can exchange 100 Points for $100 when they purchase insurance for any trip (on or off Venti) at least three times within a calendar year. Members may submit their exchange request to admin@venti.co after the completion of their trips. Simply provide your confirmation IDs <strong>{{ $data["booking_reference"] }}</strong> and your policy numbers from Visitors Coverage. <a href="https://www.visitorscoverage.com/?affid=36a20d3544aad">Get a Quote</a>
             	</p>
             	<br>
             	<a href="https://www.visitorscoverage.com/?affid=36a20d3544aad">
            		<img src="https://venti.co/assets/img/visitors-coverage-logo.png" style="width:100%; max-width:600px;">
            	</a>
          </td>
       </tr>
    </table>
@endsection



