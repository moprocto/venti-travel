

@extends("emails.layouts.generic")

@section("header")

<a href="https://venti.co" style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; color: #3d4852; font-size: 19px; font-weight: bold; text-decoration: none; display: inline-block;">
<img src="https://venti.co/assets/img/boarding-pass-email-header.jpg" class="logo" alt="Venti Logo" style="border-top-left-radius: 20px; border-top-right-radius: 20px; box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif,'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; max-width: 100%; border: none;  max-width: 570px;">
</a>

@endsection

@section("body")
<h1 style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; color: #3d4852; font-size: 26px; font-weight: bold; margin-top: 0; text-align: left;">Account Suspended</h1>
<p style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; font-size: 16px; line-height: 1.5em; margin-top: 0; text-align: left;"></p>
<table class="inner-body" align="center" width="505">
    <tr>
        <td>
            <p style="">On {{ $data["date"] }}, a decision was made to suspend your account for one of the following reasons:</p>
            <ul>
                <li>Your social security number has been blocked for usage on our platform.</li>
                <li>You added funding sources that have been blocked for usage on our platform.</li>
                <li>You attempted a transaction on our platform that was flagged for suspicious activity or abuse.</li>
                <li>Too many deposits failed due to insufficient funds or unauthorized returns.</li>
                <li>An unauthorized return code indicates fraud or other banking violation.</li>
                <li>You were invoiced for insufficient funds or an authorized return and did not pay the invoice.</li>
            </ul>
            <p style="">While your account is suspended, you are not able to add funding sources, make deposits, or withdraw funds.</p>
            <p>If you believe this is an error, please <strong>submit an appeal to admin@venti.co</strong>. In your appeal, you may include:</p>
            <ul>
                <li>A <strong>letter from your bank(s) that acknowledges you are the legitimate owner</strong> of the funding sources connected to your Venti account</li>
                <li>OR a photo of you holding your state ID next to your face. For the second option, your name, date of birth, document expiry, and your face must be clearly visible.</li>
            </ul>
            <p>Failure to complete the above recommendations will lengthen the time it takes to review your account.</p>
            <br>
            <h4 style="text-align:center; margin:0; padding:0;"><a href="https://venti.co/home" target="_blank" style="text-decoration:none; background:black; color:white; padding:10px 20px; border-radius:10px;">Log In</a></h4>
            <br>
            <p style="color:#2e2e2e; text-align:center; width:100%;">This is not a marketing email. If you believe you are receiving this email in error, please contact support via admin@venti.co</p>
        </td>
    </tr>
</table>
<p style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; font-size: 18px; line-height: 1.5em; margin-top: 0; text-align: right;"></p>
@endsection

@section("footer")
<table class="inner-body" align="center" width="333">
    
</table>
@endsection
