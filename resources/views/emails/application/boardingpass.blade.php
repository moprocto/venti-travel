@extends("emails.layouts.generic")

@section("header")

<a href="https://venti.co/boardingpass" style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; color: #3d4852; font-size: 19px; font-weight: bold; text-decoration: none; display: inline-block;">
<img src="https://venti.co/assets/img/boarding-pass-social.jpg" class="logo" alt="Venti Logo" style="border-top-left-radius: 20px; border-top-right-radius: 20px; box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif,'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; max-width: 100%; border: none;  max-width: 570px;">
</a>

@endsection

@section("body")
<h1 style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; color: #3d4852; font-size: 26px; font-weight: bold; margin-top: 0; text-align: left;">You're Now VIP!</h1>
<p style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; font-size: 16px; line-height: 1.5em; margin-top: 0; text-align: left;"></p>
<table class="inner-body" align="center" width="505">
	<tr>
		<td>
            <p style="">Thank you for officially joining the VIP list for Venti's <a href="https://venti.co/boardingpass" target="_blank" style="text-decoration:none;">Boarding Pass</a>: the highest-yielding travel savings account boasting an APY of 9%. <br><br>For a limited time, you can earn $10 in travel points for each person you invite via your <strong>referral link below</strong>.
        	</p>

        	<h2 style="font-size: 1.2em; border: 1px dashed black;  padding:10px 20px; border-radius:30px; text-align:center">https://venti.co/s/{{ $referral }}</h2>

        	<p style="">
        	The best part? You both will receive a $10 credit when you each make your first deposits. Share the savings for a win-win.
            </p>
            <br><br>
            <h4 style="text-align:center; margin:0; padding:0;"><a href="https://venti.co/boardingpass/track/{{ $referral }}" target="_blank" style="text-decoration:none; background:black; color:white; padding:10px 20px; border-radius:10px;">Track Your Referral Progress Here</a></h4>
            <br>
            <p style="color:#2e2e2e; text-align:center; width:100%; font-size: 11px;">This is not a marketing email. You will only receive this once to confirm your VIP status.</p>
		</td>
	</tr>
</table>
<p style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; font-size: 18px; line-height: 1.5em; margin-top: 0; text-align: right;"></p>
@endsection

@section("footer")
<table class="inner-body" align="center" width="333">
	
</table>
@endsection
