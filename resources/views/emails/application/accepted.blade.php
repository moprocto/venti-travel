@extends("emails.layouts.generic")

@section("header")

<a href="https://venti.co" style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; color: #3d4852; font-size: 19px; font-weight: bold; text-decoration: none; display: inline-block;">
	@if($data["isTravel"])
		<img src="{{ $data['trip']->imageURL }}" class="logo" alt="Venti Logo" style="border-top-left-radius: 20px; border-top-right-radius: 20px; box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif,'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; max-width: 100%; border: none;  max-width: 570px;">
	@endif
</a>

@endsection

@section("body")

<h1 style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; color: #3d4852; font-size: 36px; font-weight: bold; margin-top: 0; text-align: center;">They Said Yes!</h1>
<p style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; font-size: 16px; line-height: 1.5em; margin-top: 0; text-align: left;"></p>
<table class="inner-body" align="center" width="505">
	<tr>
		<td style="text-align:center;">
			@if($data['user']->photoUrl == "/assets/img/profile.jpg")
				<a href="https://venti.co/u/{{ $data['user']->customClaims['username'] }}"><img src="https://venti.co/assets/img/profile.jpg" style="width:100px; height:100px; border-radius: 50%;"></a>
			@else 
				<a href="https://venti.co/u/{{ $data['user']->customClaims['username'] }}">
					<img src="{{ $data['user']->photoUrl }}" style="width:100px; height:100px; border-radius: 50%;">
				</a>
			@endif
			<br>
			</td>
	</tr>
	@if($data["isTravel"])
		<tr>
			<td>
				<p><span style=" font-weight: bold;">{{ getFirstName($data['user']->displayName) }}</span> accepted your invite to join your group to <span style=" font-weight: bold;">{{ getTripLocation($data['trip']->destination, $data['trip']->country) }}</span></p>
				<br>
				
			</td>
		</tr>
		@else
		<tr>
			<td>
				<p><span style=" font-weight: bold;">{{ getFirstName($data['user']->displayName) }}</span> accepted your invite to join your group: {{ $data['trip']->title }}</span></p>
				<br>
			</td>
		</tr>
	@endif
	<tr>
		<td style="text-align:center;"><a href="https://venti.co/group/{{ $data['trip']->tripID }}" style="padding: 10px 20px; background:black; color:white; text-decoration:none; border-radius:20px;">View Group Details</a>
			<br><br></td>
	</tr>
	<tr>
		<td>
			Respond to this invitation by logging in at <a href="https://venti.co">https://venti.co</a>
		</td>
	</tr>
</table>
<p style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; font-size: 18px; line-height: 1.5em; margin-top: 0; text-align: right;"></p>
@endsection

@section("footer")
<table class="inner-body" align="center" width="333">
	<tr>
		<td style="text-align:center;"><a href="https://instagram.com/venti.travel"><img src="https://venti.co/assets/img/instagram-logo.png" width="33"></a></td>
		<td style="text-align:center;"><a href="https://www.tiktok.com/@venti.travel"><img src="https://venti.co/assets/img/tiktok-logo.png" width="33"></a></td>
		<td style="text-align:center;"><a href="https://www.pinterest.com/ventitravel"><img src="https://venti.co/assets/img/pinterest-logo.png" width="33"></a></td>
		<td style="text-align:center;"><a href="https://www.facebook.com/venti.travel"><img src="https://venti.co/assets/img/facebook-logo.png" width="33"></a></td>
	</tr>
</table>
@endsection
