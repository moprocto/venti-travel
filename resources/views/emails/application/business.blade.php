@extends("emails.layouts.generic")

@section("body")
<h1 style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; color: #3d4852; font-size: 26px; font-weight: bold; margin-top: 0; text-align: left;">Thank you!</h1>
<p style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; font-size: 16px; line-height: 1.5em; margin-top: 0; text-align: left;"></p>
<table class="inner-body" align="center" width="505">
	<tr>
		<td>
            <p style="font-size: 14px;">A member of our team will be in touch soon. We collected the following information:</p>
            <ul>
            	<li>Name: {{ $name }}</li>
            	<li>Email: {{ $email }}</li>
            	<li>Organization: {{ $company }}</li>
            	<li>Website: {{ $website }}</li>
            </ul>
		</td>
	</tr>
</table>
<table class="inner-body" align="center" width="505">
	<tr>
		<td>
            <p style="color:#2e2e2e; text-align:center; width:100%; font-size: 11px;">This is not a marketing email.</p>
		</td>
	</tr>
</table>
<p style="box-sizing: border-box; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; position: relative; font-size: 18px; line-height: 1.5em; margin-top: 0; text-align: right;"></p>
@endsection


