<!-- FETURES START -->
    <div class="homefeture_1 pt-115 pb-130">
        <div class="container">
            <div class="app_left_shape">
                <img class="leftanimation d-none d-sm-block" src="https://themepure.net/template/appz-preview/appz/assets/img/shape/left.png" alt="leftshape"/>
                <img src="https://themepure.net/template/appz-preview/appz/assets/img/shape/shape7.png" alt="leftshape"/>
                <img class="downsahpe d-none d-sm-block" src="https://themepure.net/template/appz-preview/appz/assets/img/shape/shape3.png" alt="leftshape"/>
            </div>
            <div class="section_title_wrapper text-center wow fadeInUp mb-70" data-wow-delay="0.3s" style="visibility: visible; animation-delay: 0.3s; animation-name: fadeInUp;">
                <h2 class="section-title">Shine the new <br> light on the digital world</h2>
            </div>
            <div class="row">
                <div class="col-xl-3 col-lg-3 col-md-6">
                    <div class="feabox mb-30">
                        <div class="feabox__img mb-50">
                            <img src="https://themepure.net/template/appz-preview/appz/assets/img/fetures/Forma1.png" alt="form1"/>
                        </div>
                        <div class="feabox__content">
                            <h3 class="feabox-title tcolor0">Premium Plugins</h3>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-6">
                    <div class="feabox mb-30 clr1">
                        <div class="feabox__img img1 mb-50">
                            <img src="https://themepure.net/template/appz-preview/appz/assets/img/fetures/Forma2.png" alt="form2"/>
                        </div>
                        <div class="feabox__content">
                            <h3 class="feabox-title tcolor1">Security Potential </h3>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-6">
                    <div class="feabox mb-30 clr2">
                        <div class="feabox__img img2 mb-50">
                            <img src="https://themepure.net/template/appz-preview/appz/assets/img/fetures/Forma3.png" alt="form3"/>
                        </div>
                        <div class="feabox__content">
                            <h3 class="feabox-title tcolor2">Share Online</h3>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-6">
                    <div class="feabox mb-30 clr3">
                        <div class="feabox__img img3 mb-50">
                            <img src="https://themepure.net/template/appz-preview/appz/assets/img/fetures/Forma4.png" alt="form4"/>
                        </div>
                        <div class="feabox__content">
                            <h3 class="feabox-title tcolor3">Risk Protectable</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<!-- FETURES END -->

<!-- FETURES CONTENT START -->
    <div class="homefeture_2">
        <div class="container">
            <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-12">
                    <div class="features pt-105 wow fadeInLeft" data-wow-delay=".3s" style="visibility: visible; animation-delay: 0.2s; animation-name: fadeInLeft;">
                        <div class="features__icon">
                            <img src="https://themepure.net/template/appz-preview/appz/assets/img/fetures/leftIcon.png" alt=""/>
                        </div>
                        <div class="features__content mb-30">
                            <h2 class="section-title">We Create innovative solution <br> that works pragmatically.</h2>
                            <p>You mug dropped a clanger barmy David brown <br>bread bleeding crikey say chimney pot me old <br>mucker bugger super.</p>
                            <a href="service.html">Read More<i class="fal fa-arrow-right"></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-xl-6 col-lg-6 col-md-12">
                    <div class="app-image wow fadeInRight" data-wow-delay=".3s" style="visibility: visible; animation-delay: 0.3s; animation-name: fadeInRight;">
                        <img src="https://themepure.net/template/appz-preview/appz/assets/img/about/about1.png" alt=""/>
                    </div>
                </div>
            </div>
    </div>
    </div>
<!-- FETURES CONTENT END -->

<!-- FETURES CONTENT START -->
    <div class="app_image pt-150 pb-140">
        <div class="container">
            <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-12">
                    <div class="app-image wow fadeInUp" data-wow-delay=".3s" style="visibility: visible; animation-delay: 0.2s; animation-name: fadeInUp;">
                        <img src="https://themepure.net/template/appz-preview/appz/assets/img/about/about2.png" alt=""/>
                    </div>
                </div>
                <div class="col-xl-6 col-lg-6 col-md-12">
                    <div class="features pt-80 feturesCommon wow fadeInUp" data-wow-delay=".3s" style="visibility: visible; animation-delay: 0.2s; animation-name: fadeInUp;">
                        <div class="features__icon bgclr">
                            <img src="https://themepure.net/template/appz-preview/appz/assets/img/fetures/leftIcon2.png" alt=""/>
                        </div>
                        <div class="features__content">
                            <h2 class="section-title">Features that aren’t so commonplace.</h2>
                            <p>You mug dropped a clanger barmy David brown <br>bread bleeding crikey say chimney pot me old <br>mucker bugger super.</p>
                            <a href="service.html">Read More<i class="fal fa-arrow-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<!-- FETURES CONTENT START -->

<!-- TEAM START -->
<div class="team-area pb-200">
    <div class="container">
        <div class="team_content text-center">
            <h2 class="section-title team_title text-dark mb-65">About Our Team Member We have<br> Powerful User Experience.</h2>
        </div>
        <div class="team-active tp-dot-style">
            <div class="col-xl-4">
                <div class="single-team text-center">
                    <div class="team-item team-padding d-inline-block">
                        <div class="team_item_four hover_bg">
                            <img src="https://themepure.net/template/appz-preview/appz/assets/img/team/team1.jpg" alt="">
                            <div class="team_content">
                                <h5><a href="team.html">Ahsan Riad</a></h5>
                                <p>Software Developer</p>
                                <div class="team_social_icon">
                                    <a href="#"><i class="fab fa-twitter"></i></a>
                                    <a href="#"><i class="fab fa-pinterest-p"></i></a>
                                    <a href="#"><i class="fab fa-vimeo-v"></i></a>
                                    <a href="#"><i class="fab fa-facebook-f"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4">
                <div class="single-team text-center">
                    <div class="team-item team-padding d-inline-block">
                        <div class="team_item_four hover_bg">
                            <img src="https://themepure.net/template/appz-preview/appz/assets/img/team/team3.jpg" alt="">
                            <div class="team_content">
                                <h5><a href="team.html">Parsley Montana</a></h5>
                                <p>Designer</p>
                                <div class="team_social_icon">
                                    <a href="#"><i class="fab fa-twitter"></i></a>
                                    <a href="#"><i class="fab fa-pinterest-p"></i></a>
                                    <a href="#"><i class="fab fa-vimeo-v"></i></a>
                                    <a href="#"><i class="fab fa-facebook-f"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4">
                <div class="single-team text-center">
                    <div class="team-item team-padding d-inline-block">
                        <div class="team_item_four hover_bg">
                            <img src="https://themepure.net/template/appz-preview/appz/assets/img/team/team2.jpg" alt="">
                            <div class="team_content"> 
                                <h5><a href="team.html">Joss Sticks</a></h5>
                                <p>Software Developer</p>
                                <div class="team_social_icon">
                                    <a href="#"><i class="fab fa-twitter"></i></a>
                                    <a href="#"><i class="fab fa-pinterest-p"></i></a>
                                    <a href="#"><i class="fab fa-vimeo-v"></i></a>
                                    <a href="#"><i class="fab fa-facebook-f"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4">
                <div class="single-team text-center">
                    <div class="team-item team-padding d-inline-block">
                        <div class="team_item_four hover_bg">
                            <img src="https://themepure.net/template/appz-preview/appz/assets/img/team/team1.jpg" alt="">
                            <div class="team_content">
                                <h5><a href="team.html">Rodney Artichoke</a></h5>
                                <p>Software Developer</p>
                                <div class="team_social_icon">
                                    <a href="#"><i class="fab fa-twitter"></i></a>
                                    <a href="#"><i class="fab fa-pinterest-p"></i></a>
                                    <a href="#"><i class="fab fa-vimeo-v"></i></a>
                                    <a href="#"><i class="fab fa-facebook-f"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4">
                <div class="single-team text-center">
                    <div class="team-item team-padding d-inline-block">
                        <div class="team_item_four hover_bg">
                            <img src="https://themepure.net/template/appz-preview/appz/assets/img/team/team3.jpg" alt="">
                            <div class="team_content">
                                <h5><a href="team.html">Desmond Eagle</a></h5>
                                <p>Software tester</p>
                                <div class="team_social_icon">
                                    <a href="#"><i class="fab fa-twitter"></i></a>
                                    <a href="#"><i class="fab fa-pinterest-p"></i></a>
                                    <a href="#"><i class="fab fa-vimeo-v"></i></a>
                                    <a href="#"><i class="fab fa-facebook-f"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4">
                <div class="single-team text-center">
                    <div class="team-item team-padding d-inline-block">
                        <div class="team_item_four hover_bg">
                            <img src="https://themepure.net/template/appz-preview/appz/assets/img/team/team2.jpg" alt="">
                            <div class="team_content">
                                <h5><a href="team.html">Rodney Artichoke</a></h5>
                                <p>Software Developer</p>
                                <div class="team_social_icon">
                                    <a href="#"><i class="fab fa-twitter"></i></a>
                                    <a href="#"><i class="fab fa-pinterest-p"></i></a>
                                    <a href="#"><i class="fab fa-vimeo-v"></i></a>
                                    <a href="#"><i class="fab fa-facebook-f"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        </div>
    </div>
<!-- TEAM ENDE -->

<!-- APP DETAILS START -->
    <div class="app_details pt-120 pb-20">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-xl-7 col-lg-6 col-md-12">
                    <div class="details_content  pb-70">
                        <h2 class="section-title section_title_large">A new way to manage your work, tasks and projects.</h2>
                    </div>
                    <div class="row">
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6">
                            <div class="feature3">
                                <div class="feature3__image">
                                    <img class="pb-30" src="https://themepure.net/template/appz-preview/appz/assets/img/service/s7.png" alt="details"/>
                                </div>
                                <div class="feature3__content">
                                    <h4 class="content_title">Security Maintenance</h4>
                                    <p>The little rotter bevvy I gormless <br>  mush golly gosh cras.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6">
                            <div class="feature3 pl-40">
                                <div class="feature3__image febg1">
                                    <img class="pb-30" src="https://themepure.net/template/appz-preview/appz/assets/img/fetures/s2.png" alt="details"/>
                                </div>
                                <div class="feature3__content">
                                    <h4 class="content_title">Backup Database</h4>
                                    <p>The little rotter bevvy I gormless <br>  mush golly gosh cras.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6">
                            <div class="feature3">
                                <div class="feature3__image febg2">
                                    <img class="pb-30" src="https://themepure.net/template/appz-preview/appz/assets/img/fetures/s3.png" alt="details"/>
                                </div>
                                <div class="feature3__content">
                                    <h4 class="content_title">Server Maintenance</h4>
                                    <p>The little rotter bevvy I gormless  mush golly gosh cras.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6">
                            <div class="feature3 pl-40">
                                <div class="feature3__image febg3">
                                    <img class="pb-30" src="https://themepure.net/template/appz-preview/appz/assets/img/fetures/s4.png" alt="details"/>
                                </div>
                                <div class="feature3__content">
                                    <h4 class="content_title">No Risk Protectable</h4>
                                    <p>The little rotter bevvy I gormless  mush golly gosh cras.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-5 col-lg-6 col-lg-6 col-md-12 d-md-none d-lg-block d-sm-none d-md-block d-none d-sm-block">
                    <div class="details_image wow zoomIn" data-wow-delay=".3s" style="visibility: visible; animation-delay: 0.2s; animation-name: zoomIn;">
                        <img src="https://themepure.net/template/appz-preview/appz/assets/img/fetures/mockup.png" alt="blogshape"/>
                    </div>
                </div>
            </div>
        </div>
    </div>
<!-- APP DETAILS END -->

<!-- DOWNLOAD START  -->
    <div class="download-area pt-50 mt-100">
        <div class="container">
            <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-4 col-sm-12 d-sm-none d-md-block">
                    <div class="download_image">
                        <img class="dw d-none d-sm-block" src="https://themepure.net/template/appz-preview/appz/assets/img/shape/iphon2.png" alt="download_shape"/>
                        <img class="downloadpos  d-md-none d-lg-block d-none d-sm-block" src="https://themepure.net/template/appz-preview/appz/assets/img/shape/iphone3.png" alt="download_shape"/>
                        <img class="downloadpos2 d-md-none d-lg-block d-none d-sm-block" src="https://themepure.net/template/appz-preview/appz/assets/img/shape/circle.png" alt="download_shape"/>
                    </div>
                </div>
                <div class="col-xl-6 col-lg-6 col-md-8 col-sm-12">
                    <div class="download_content pl-105 pt-90">
                        <h2 class="section-title">Download Your Amazing App Available Now.</h2>
                        <p>Horse play argy-bargy me old mucker boot bog show off show off pick your nose and blow off cack, cras buggered say.</p>
                        <div class="download-btn">
                            <a class="tp_download" href="#"><i class="fab fa-apple"></i></a>
                            <a class="bg-android" href="#"><i class="fal fa-user-robot"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<!-- DOWNLOAD END  -->

<!-- TESTIMONIAL START  -->
<div class="testimonial-area  bg1 pt-110">
    <div class="testimonial_shape">
        <img class="t-1" src="https://themepure.net/template/appz-preview/appz/assets/img/shape/shape8.png" alt="shape"/>
        <img class="t-2" src="https://themepure.net/template/appz-preview/appz/assets/img/shape/shape2.png" alt="shape"/>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="section-wrapper">
                    <h2 class="section-title">About Our Team Member We have<br> Powerful User Experience.</h2>
                </div>
            </div>
        </div>
        <div class="row testimonial-active tp-dot-style">
            <div class="col-xl-4 col-lg-4">
                <div class="testimonial-item pt-55">
                    <div class="item">
                        <p>Matie boy it's your round amongst bodge vagabond absolutely bladdered crikey well off his nut have it, goal you mug loo don't super.</p>
                        <div class="clients_meta">
                            <div class="clients_image">
                                <img src="https://themepure.net/template/appz-preview/appz/assets/img/testimonial/test1.jpg" alt="clients_image"/>
                            </div>
                            <div class="clients_info">
                                <h4>Hilary Ouse</h4>
                                <span>Ui/Ux Designer</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-lg-4">
                <div class="testimonial-item pt-55">
                    <div class="item">
                        <p>Matie boy it's your round amongst bodge vagabond absolutely bladdered crikey well off his nut have it, goal you mug loo don't super.</p>
                        <div class="clients_meta">
                            <div class="clients_image">
                                <img src="https://themepure.net/template/appz-preview/appz/assets/img/testimonial/test3.jpg" alt="clients_image"/>
                            </div>
                            <div class="clients_info">
                                <h4>Brian Cumin</h4>
                                <span>Ui/Ux Designer</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-lg-4">
                <div class="testimonial-item pt-55">
                    <div class="item">
                        <p>Matie boy it's your round amongst bodge vagabond absolutely bladdered crikey well off his nut have it, goal you mug loo don't super.</p>
                        <div class="clients_meta">
                            <div class="clients_image">
                                <img src="https://themepure.net/template/appz-preview/appz/assets/img/testimonial/test2.jpg" alt="clients_image"/>
                            </div>
                            <div class="clients_info">
                                <h4>Indigo Violet</h4>
                                <span>Ui/Ux Designer</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-lg-4">
                <div class="testimonial-item pt-55">
                    <div class="item">
                        <p>Matie boy it's your round amongst bodge vagabond absolutely bladdered crikey well off his nut have it, goal you mug loo don't super.</p>
                        <div class="clients_meta">
                            <div class="clients_image">
                                <img src="https://themepure.net/template/appz-preview/appz/assets/img/testimonial/test3.jpg" alt="clients_image"/>
                            </div>
                            <div class="clients_info">
                                <h4>Indigo Violet</h4>
                                <span>Ui/Ux Designer</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-lg-4">
                <div class="testimonial-item pt-55">
                    <div class="item">
                        <p>Matie boy it's your round amongst bodge vagabond absolutely bladdered crikey well off his nut have it, goal you mug loo don't super.</p>
                        <div class="clients_meta">
                            <div class="clients_image">
                                <img src="https://themepure.net/template/appz-preview/appz/assets/img/testimonial/test2.jpg" alt="clients_image"/>
                            </div>
                            <div class="clients_info">
                                <h4>Indigo Violet</h4>
                                <span>Ui/Ux Designer</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- TESTIMONIAL END -->

<!-- TRY START  -->
<div class="try-area pt-100 mb-40">
    <div class="container">
        <div class="row">
            <div class="col-xl-12 col-lg-12">
                <div class="try_box">
                    <div class="try-shape">
                        <img class="tryshape" src="https://themepure.net/template/appz-preview/appz/assets/img/shape/shape3.png" alt="shape"/>
                    </div>
                    <div class="row">
                        <div class="col-xl-7 col-lg-7">
                            <h2>Try Our New Appz</h2>
                            <p>Horse play argy-bargy me old mucker boot bog show off show off pick your nose and blow off sloshed my cack buggered.</p>
                        </div>
                        <div class="col-xl-5 col-lg-5">
                            <div class="try_btn_center">
                                <div class="try_btn">
                                    <a class="btn btnfree" href="contact.html">Free Trail</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- TRY END  -->