@extends('layouts.curator-master', ["admin" => true])

@section('css')

@endsection

@section('content')
<div class="container hero-section mb-4 mt-4">
    <div class="row justify-content-center">
        <div class="col-md-12 mb-4 mt-4">
            <a href="{{ route('dashboard-clients-add') }}" class="btn btn-main btn-success">Create Client</a>
        </div>
        <div class="col-md-12">
            
            <div class="card">
                <div class="card-body">
                    <table class="table" id="transactions">
                        <thead>
                            <tr>
                                <th>Client</th>
                                <th>Email</th>
                                <th>Website</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($clients as $client)
                            <tr>
                                <td>{{ $client["name"] }}</td>
                                <td>{{ $client["email"] }}</td>
                                <td>{{ $client["website"] }}</td>
                                <td><a href="{{ route('dashboard-clients-read', ['client' => $client['id']]) }}" target="_blank"><i class="fa fa-pencil"></i></a></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection