@extends('layouts.curator-master', ["admin" => true])

@section('css')

@endsection

@section('content')
<div id="page-content">
    <div class="gradient-wrap">
    <div class="container hero-section mb-4 mt-4">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <h2>Create Client</h2>
            </div>
            <div class="col-md-12">
                <form method="POST" action="{{ route('dashboard-clients-create') }}">
                    @CSRF
                    <div class="card mb-4">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-9">
                                    <div class="form-group">
                                        <label>Name</label>
                                        <input type="text" name="name" class="form-control" value="" required="">
                                    </div>
                                    <div class="form-group">
                                        <label>Avatar</label>
                                        <input type="text" name="avatar" class="form-control" value="">
                                    </div>
                                </div>
                                <div class="col-md-3 text-center">
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label>About</label>
                                <textarea type="text" name="bio" class="form-control" required=""></textarea> 
                            </div>
                            <div class="form-group">
                                <label>Email</label>
                                <input type="email" name="email" class="form-control" value="" required="">
                            </div>
                            <div class="form-group">
                                <label>Website</label>
                                <input type="text" name="website" class="form-control" value="" required="">
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Payment Method</label>
                                        <input type="text" name="paymentMethod" class="form-control" value="" required="">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Payment Handle</label>
                                        <input type="text" name="paymentHandle" class="form-control" value="" required="">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="form-label">Expertise</label>
                                <input type="text" name="expertise" class="form-control" value="" required="">
                            </div>
                            <div class="form-group">
                                <input type="submit" name="submit" value="CREATE" class="btn btn-success">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection