@extends('layouts.builder')

@section('css')
	<style type="text/css">
		body, html, iframe, div{
			height: 100%;
			min-height: 600px;
		}
	</style>
@endsection

@section('content')
	<div class="row">
		<div class="col-md-12">
			<div id="app" style="width: 100%; height: 100%;"></div>
		</div>
</div>
@endsection

@section('js')
<script src="https://d5aoblv5p04cg.cloudfront.net/editor-3/loader/build.js" type="text/javascript"></script>
<script>
const TOPOL_OPTIONS = {
    id: "#app",
    authorize: {
        apiKey: "{{ env('TOPAL_IO_TOKEN') }}",
        userId: "UserID",
    },
    hideSettingsTab: false, 
    title: "Itinerary Builder", // Title shown on the top of the main menu
    language: "en",
    removeTopBar: false,
    contentBlocks: {
        social: {
            disabled: true,
            disabledText: 'Text In Tooltip',
            hidden: true
        },
        product: {
            disabled: true,
            disabledText: 'Text In Tooltip',
            hidden: true
        },
    },
    customFileManager: true,
    topBarOptions: [
        "undoRedo",
        "changePreview",
        "previewSize",
        "previewTestMail",
        "saveAndClose",
        "save"
    ],
        callbacks: {
        onSaveAndClose: function (json, html) {
            // HTML of the email
            console.log(html);
            // JSON object of the email
            console.log(json);
            // Implement your own close callback
            // Data variable contains the response data of the save request
        },
        onSave: function (json, html) {
            // HTML of the email
            console.log(html);
            // JSON object of the email
            console.log(JSON.stringify(json));
        },
        onTestSend: function (email, json, html) {
            // HTML of the email
            console.log(html);
            // JSON object of the email
            console.log(json);
            // Email of the recipient
            console.log(email);
            // Callback when send test email button is clicked
        },
        onOpenFileManager: function () {
            // Implement your own file manager open callback
        },
        onBlockSave(json) {
            var name = window.prompt('Enter block name:')
            if (name !== null) {
                console.log('saving block', json)
            }
        },
        onBlockRemove(id) {
            if (window.confirm('Are you sure?')) {
                console.log('removing block', id)
            }
        },
        onBlockEdit(id) {
            var name = window.prompt('Block name:', 'My block 001')
            if (name !== null) {
                console.log('saving edited block', id)
            }
        },
        onPreview(html) {
          //do something with the html
        },
        onInit() {
          //Called when editor is loaded
        },
        onAlert(notification) {

        },
        onClose() {
          // called when clicked close button in WindowBar
        }
    },
    premadeBlocks: {
        'content': [
            {
                'img': 'https://cdn-icons-png.flaticon.com/512/2693/2693560.png', // Image url
                'definition': [{ "tagName": "mj-section", "attributes": { "full-width": false, "padding": "9px 0px 9px 0px", "background-color": "#fff" }, "type": null, "children": [{ "tagName": "mj-column", "attributes": { "width": "33.333333%", "vertical-align": "top" }, "children": [{ "tagName": "mj-social", "attributes": { "display": "facebook:url twitter:url google:url", "padding": "10px 10px 10px 30px", "text-mode": "false", "icon-size": "25px", "base-url": "https://s3-eu-west-1.amazonaws.com/ecomail-assets/editor/social-icos/simplewhite/", "facebook-href": "https://www.facebook.com/PROFILE", "facebook-icon-color": "none", "facebook-alt": "Sdílet", "twitter-href": "https://www.twitter.com/PROFILE", "twitter-icon-color": "none", "twitter-alt": "", "google-href": "https://plus.google.com/PROFILE", "google-icon-color": "none", "google-alt": "", "instagram-icon-color": "none", "linkedin-icon-color": "none", "align": "left", "youtube-icon-color": "none", "youtube-alt": "", "youtube-icon": "https://s3-eu-west-1.amazonaws.com/ecomail-assets/editor/social-icos/simplewhite/youtube.png", "youtube-href": "https://www.youtube.com", "containerWidth": 200 }, "uid": "H1lqIiX4lm" }], "uid": "SJ3I0XVx7" }, { "tagName": "mj-column", "attributes": { "width": "33.333333%", "vertical-align": "top" }, "children": [{ "tagName": "mj-raw", "attributes": { "mj-content": "https://storage.googleapis.com/jan50/blackberrylogo.png", "padding": "19px 10px 10px 10px", "alt": "", "href": "", "containerWidth": 200, "width": 100, "widthPercent": 50 }, "uid": "rkEyL-HeQ" }], "uid": "r1e280m4xQ" }, { "tagName": "mj-column", "attributes": { "width": "33.333333%", "vertical-align": "top" }, "children": [{ "tagName": "mj-spacer", "attributes": { "height": 15, "containerWidth": 200 }, "uid": "rJfqLiXEgm" }], "uid": "B1W380QVxX" }], "layout": 1, "backgroundColor": "#000000", "backgroundImage": "", "paddingTop": 0, "paddingBottom": 0, "paddingLeft": 0, "paddingRight": 0, "uid": "rkqIjQNe7" } // MJML JSON
                ]
            }]
    },

};

TopolPlugin.init(TOPOL_OPTIONS);

fetch('https://tlapi.github.io/topol-editor/templates/1.json')
    .then(response => response.text())
    .then(template => {
        //Load the template in Editor
                TopolPlugin.load("{\"tagName\":\"mj-global-style\",\"attributes\":{\"h1:color\":\"#000\",\"h1:font-family\":\"Helvetica, sans-serif\",\"h2:color\":\"#000\",\"h2:font-family\":\"Ubuntu, Helvetica, Arial, sans-serif\",\"h3:color\":\"#000\",\"h3:font-family\":\"Ubuntu, Helvetica, Arial, sans-serif\",\":color\":\"#000\",\":font-family\":\"Ubuntu, Helvetica, Arial, sans-serif\",\":line-height\":\"1.5\",\"a:color\":\"#24bfbc\",\"button:background-color\":\"#e85034\",\"containerWidth\":600,\"fonts\":\"Helvetica,sans-serif,Ubuntu,Arial\",\"mj-text\":{\"line-height\":1.5,\"font-size\":15,\"font-family\":\"Ubuntu, sans-serif\"},\"mj-button\":{\"font-size\":\"13px\"}},\"children\":[{\"tagName\":\"mj-body\",\"attributes\":{\"background-color\":\"#FFFFFF\",\"containerWidth\":600},\"children\":[{\"tagName\":\"mj-section\",\"attributes\":{\"full-width\":false,\"padding\":\"9px 0px 9px 0px\"},\"children\":[{\"tagName\":\"mj-column\",\"attributes\":{\"width\":\"100%\",\"vertical-align\":\"top\"},\"children\":[{\"tagName\":\"mj-text\",\"attributes\":{\"align\":\"left\",\"padding\":\"15px 15px 15px 15px\",\"line-height\":1.5,\"containerWidth\":600},\"uid\":\"bcn1xKvQD\",\"content\":\"<h2>07/03/2022</h2>\"},{\"tagName\":\"mj-text\",\"attributes\":{\"align\":\"left\",\"padding\":\"15px 15px 15px 15px\",\"line-height\":1.5,\"containerWidth\":600},\"uid\":\"uA58a-oGY\",\"content\":\"<h3><strong>Arrival | Check-in</strong></h3>\"},{\"tagName\":\"mj-text\",\"attributes\":{\"align\":\"left\",\"padding\":\"15px 15px 15px 15px\",\"line-height\":1.5,\"containerWidth\":600},\"uid\":\"Ze1P9_0vq\",\"content\":\"<p>Check-in for one night in heart of downtown Anchorage at Hotel Captain Cook. As part of Historic Hotels and Preferred, this property offers 96 suite options, astounding views from their rooftop restaurant, boutique shopping, and is with in walking distance of the train station.</p>\"},{\"tagName\":\"mj-text\",\"attributes\":{\"align\":\"left\",\"padding\":\"15px 15px 15px 15px\",\"line-height\":1.5,\"containerWidth\":600},\"uid\":\"wQ4O4RXsj\",\"content\":\"<h3><strong>Explore</strong></h3>\"},{\"tagName\":\"mj-text\",\"attributes\":{\"align\":\"left\",\"padding\":\"15px 15px 15px 15px\",\"line-height\":1.5,\"containerWidth\":600},\"uid\":\"LXbfNt4g2\",\"content\":\"<p>Explore the Anchorage Microbrew scene on a four-hour afternoon craft brewery tour with Big Swig<br />Tours. Your Hoperator will escort you around town and off the beaten path as you explore 3 or 4<br />of Anchorage's finest breweries. Your $140 ticket includes beer tastings, appetizers from Midnight<br />Sun Brewing, and all transportation.</p>\"},{\"tagName\":\"mj-text\",\"attributes\":{\"align\":\"left\",\"padding\":\"15px 15px 15px 15px\",\"line-height\":1.5,\"containerWidth\":600},\"uid\":\"qCEjqxPKQ\",\"content\":\"<h3><strong>Lunch</strong></h3>\"},{\"tagName\":\"mj-text\",\"attributes\":{\"align\":\"left\",\"padding\":\"15px 15px 15px 15px\",\"line-height\":1.5,\"containerWidth\":600},\"uid\":\"Z9rjHmF9h\",\"content\":\"<p>Enjoy lunch at the unexpected hit, International House of Hot Dogs, right next door to your hotel. This eclectic and affordable Polish-style beef hotdog stand Specializes in gourmet wieners and even offers an Alaskan hot dog made with reindeer or buffalo and topped with sauteed onion and chipotle sauce. don't forget to get a full order of their cilantro fries.</p>\"},{\"tagName\":\"mj-text\",\"attributes\":{\"align\":\"left\",\"padding\":\"15px 15px 15px 15px\",\"line-height\":1.5,\"containerWidth\":600},\"uid\":\"V19Eoo_iE\",\"content\":\"<h3><strong>Dinner</strong></h3>\"},{\"tagName\":\"mj-text\",\"attributes\":{\"align\":\"left\",\"padding\":\"15px 15px 15px 15px\",\"line-height\":1.5,\"containerWidth\":600},\"uid\":\"1VOVvrfb9\",\"content\":\"<p>If you're still hungry after your adventures, head to Fletcher's Pub inside your hotel for a cup of seafood chowder, a chopped salad, or crispy spinach or reindeer sausage pizza. But don't linger,<br />an early morning awaits.</p>\"},{\"tagName\":\"mj-text\",\"attributes\":{\"align\":\"left\",\"padding\":\"15px 15px 15px 15px\",\"line-height\":1.5,\"containerWidth\":600},\"uid\":\"gsuTwTkrm\",\"content\":\"<h2>07/04/2022</h2>\"},{\"tagName\":\"mj-text\",\"attributes\":{\"align\":\"left\",\"padding\":\"15px 15px 15px 15px\",\"line-height\":1.5,\"containerWidth\":600},\"uid\":\"tO17B75Ma\",\"content\":\"<h3><strong>Departure | Check-Out</strong></h3>\"},{\"tagName\":\"mj-text\",\"attributes\":{\"align\":\"left\",\"padding\":\"15px 15px 15px 15px\",\"line-height\":1.5,\"containerWidth\":600},\"uid\":\"eHTmAimH1\",\"content\":\"<p>Alaska Railroad&rsquo;s Coastal Classic Train links Anchorage to Seward. Check-in at the rail depot one hr. before your departure time of 6:45am. Adventure Class seating offers picture windows and dome viewing to enjoy the amazing sights of Turnagain Arm and the steep mountains of the Chugach Range, which plummets directly to the sea. After a brief stop in Girdwood, the train veers into the wild where you're offered a glimpse of glaciers as you head into the Kenai Fjords National Park. National Geographic ranked this route in the top ten most senic North American train rides.</p>\"},{\"tagName\":\"mj-text\",\"attributes\":{\"align\":\"left\",\"padding\":\"15px 15px 15px 15px\",\"line-height\":1.5,\"containerWidth\":600},\"uid\":\"ZI1pIUfHc\",\"content\":\"<p>Dining is offered onboard but credit card only. Checked and carry-on luggage is allowed and will be tagged directly to your hotel. Total travel time is 4 hours and 20 minutes with arrival in Seward at 11:05am. Where transportation is provided. with port fees, the total cost for the train is less than 100$pp.</p>\"},{\"tagName\":\"mj-text\",\"attributes\":{\"align\":\"left\",\"padding\":\"15px 15px 15px 15px\",\"line-height\":1.5,\"containerWidth\":600},\"uid\":\"yUq0E6b2a\",\"content\":\"<h3><strong>Arrival | Check-In</strong></h3>\"},{\"tagName\":\"mj-text\",\"attributes\":{\"align\":\"left\",\"padding\":\"15px 15px 15px 15px\",\"line-height\":1.5,\"containerWidth\":600},\"uid\":\"Bs0ZfVCm1\",\"content\":\"<p>Check-in for 2 nights at the Seward Windsong Lodge ($255/per room). The tran quil and rustic retreat is set alongside the Resurrection River. Styled with a modern wilderness vibe,the newest rooms can be found in the Juniper building. Free transportation is provided.</p>\"},{\"tagName\":\"mj-text\",\"attributes\":{\"align\":\"left\",\"padding\":\"15px 15px 15px 15px\",\"line-height\":1.5,\"containerWidth\":600},\"uid\":\"ZSpiFsWPD\",\"content\":\"<h3><strong>Explore</strong></h3>\"},{\"tagName\":\"mj-text\",\"attributes\":{\"align\":\"left\",\"padding\":\"15px 15px 15px 15px\",\"line-height\":1.5,\"containerWidth\":600},\"uid\":\"WCTHryZLA\",\"content\":\"<h3><strong>Dinner</strong></h3>\"},{\"tagName\":\"mj-text\",\"attributes\":{\"align\":\"left\",\"padding\":\"15px 15px 15px 15px\",\"line-height\":1.5,\"containerWidth\":600},\"uid\":\"VrtS4tlw6\",\"content\":\"<p>Tonight, enjoy an easy dinner on-property at the Roadhouse Grill. The lodge serves up a limited menu of fresh Alaskan seafood, like Smoked Salmon Spread and Grilled Baguette, locally sourced Halibut, and creamy seafood chowder.</p>\"},{\"tagName\":\"mj-text\",\"attributes\":{\"align\":\"left\",\"padding\":\"15px 15px 15px 15px\",\"line-height\":1.5,\"containerWidth\":600},\"uid\":\"0XLT5egxF\",\"content\":\"<h2>07/05/2022</h2>\"},{\"tagName\":\"mj-text\",\"attributes\":{\"align\":\"left\",\"padding\":\"15px 15px 15px 15px\",\"line-height\":1.5,\"containerWidth\":600},\"uid\":\"LYumAoDIv\",\"content\":\"<p>Upon arrival, you'll head out on a Naturalist Hiking tour of the Exit Glacier, the jewel of the Resurrection River Valley. Expect to spend about 2.5 hours exploring this magnificent chunk of ice. 1:15 pm departure from the hotel at $40pp.</p>\"}],\"uid\":\"HJQ8ytZzW\"}],\"layout\":1,\"backgroundColor\":null,\"backgroundImage\":null,\"paddingTop\":0,\"paddingBottom\":0,\"paddingLeft\":0,\"paddingRight\":0,\"uid\":\"Byggju-zb\"}]}],\"style\":{\"h1\":{\"font-family\":\"Ubuntu, sans-serif\",\"font-size\":\"22px\"},\"a\":{\"color\":\"#0000EE\"},\"h2\":{\"font-family\":\"Ubuntu, Helvetica, Arial\",\"font-size\":\"17px\"},\"h3\":{\"font-family\":\"Ubuntu, Helvetica, Arial\",\"font-size\":\"13px\"},\"p\":{\"font-family\":\"Ubuntu, sans-serif\",\"font-size\":\"11px\"},\"ul\":{\"font-size\":\"11px\",\"font-family\":\"Ubuntu, sans-serif\"},\"li\":{\"font-size\":\"11px\",\"font-family\":\"Ubuntu, sans-serif\"},\"ol\":{\"font-size\":\"11px\",\"font-family\":\"Ubuntu, sans-serif\"}},\"fonts\":[\"Ubuntu, sans-serif\",\"Ubuntu, Helvetica, Arial\"]}");

    }
);

    $("#save").click(function(){
    	TopolPlugin.save();
    });
</script>
@endsection