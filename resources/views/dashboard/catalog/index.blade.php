@extends('layouts.curator-master', ["admin" => true])

@section('css')

@endsection

@section('content')
<div class="container hero-section mb-4 mt-4">
    <div class="row justify-content-center">
        <div class="col-md-12 mb-4 mt-4">
            <a href="{{ route('dashboard-catalog-create') }}" class="btn btn-main btn-success">Create Trip</a>
        </div>
        <div class="col-md-12">
            <table class="table" id="transactions">
                <thead>
                    <tr>
                        <th>Product</th>
                        <th>Author</th>
                        <th>Dates</th>
                        <th class="text-center">Worldee</th>
                        <th class="text-center">Status</th>
                        <th></th>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($groups as $group)
                    <?php
                        $agenda = $group;

                        $departure = Carbon\Carbon::createFromFormat('m-d-Y', str_replace("/","-",$group["departure"]))->timezone('America/New_York')->format("m-d-Y");
                        $return = Carbon\Carbon::createFromFormat('m-d-Y', str_replace("/","-",$group["departure"]))->addDays($group["days"] - 1)->timezone('America/New_York')->format("m-d-Y");

                        $schedule = null;
                        $itineraryList = [];

                    
                        $key = 0;
                    ?>
                    <tr>
                        <td>{{ $group["title"] }}</td>
                        <td>{{ $group["author"] ?? "N/A" }}</td>
                        <td>{{ $departure . " to " . $return }}</td>
                        <td class="text-center">@if(isset($group["worldee"]))<a href="https://www.worldee.com/trip/detail?tripId={{ $group['worldee'] }}" target="_blank"><i class="fa fa-globe"></i></a> @else UNSET @endif</td>
                        <td class="text-center">@if($group["archived"] == 0) ACTIVE @else ARCHIVED @endif</td>
                        <td><a href="{{ route('dashboard-catalog-read', ['trip' => $group['tripID']]) }}" target="_blank"><i class="fa fa-pencil"></i></a></td>
                        <td><a href="{{ route('dashboard-catalog-preview', ['trip' => $group['tripID']]) }}" target="_blank"><i class="fa fa-eye"></i></a></td>
                        <td><a href="{{ route('shop-item', ['tripID' => $group['tripID']]) }}" target="_blank"><i class="fa fa-shop"></i></a></td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection