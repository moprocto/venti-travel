@extends('layouts.curator-master', ["admin" => true])

@section('css')
<link href="/css/landing.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.12.1/css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/2.2.3/css/buttons.dataTables.min.css">
@endsection

@section('content')
<div id="page-content">
    <div class="gradient-wrap">
    <div class="container hero-section mb-4 mt-4">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <h2>Sales</h2>
            </div>
            <div class="col-md-12" style="margin-bottom: 20px">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table" id="transactions">
                                    <thead>
                                        <tr>
                                            <th>Product</th>
                                            <th>Author</th>
                                            <th>Customer</th>
                                            <th class="text-right">Charged</th>
                                            <th class="text-center">Upgraded?</th>
                                            <th>Date</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php
                                            $revenue = $personalizedCount = 0;
                                        @endphp 
                                        @foreach($transactions as $transaction)
                                            <?php 
                                                $date = \Carbon\Carbon::createFromTimestamp($transaction["paymentDate"])->timezone('America/New_York');
                                                $date = $date->format("M d, Y - h:i A");
                                                $revenue += $transaction["charged"];
                                                $transaction["personalized"] ? $personalizedCount++ : $personalizedCount;
                                            ?> 
                                            <tr>
                                                <td><a href="/dashboard/reports/product/{{ $transaction['tripID'] }}" target="_blank">{{ $transaction["Title"] }}</a></td>
                                                <td><a href="/dashboard/reports/author/{{ $transaction['Author'] }}" target="_blank">{{ $transaction["Author"] }}</a></td>
                                                <td><a href="/dashboard/reports/user/{{ $transaction['userID'] }}" target="_blank">{{ $transaction["userID"] }}</a></td>
                                                <td class="text-right">${{ $transaction["charged"] }}</td>
                                                <td class="text-center">{{ $transaction["personalized"] ? "Y" : "N" }}</td>
                                                <td><span style="display: none;">{{ $transaction["paymentDate"] }}</span>{{ $date }}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <h2>Metrics</h2>
            </div>
            <div class="col-md-3">
                <div class="card">
                    <div class="card-body text-center">
                        <h2>{{ sizeof($transactions) }}</h2>
                        <p># of Sales</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card">
                    <div class="card-body text-center">
                        <h2>${{ $revenue }}</h2>
                        <p>Total Revenue</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card">
                    <div class="card-body text-center">
                        <h2>${{ round($revenue/sizeof($transactions),2) }}</h2>
                        <p>Avg. Sale</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card">
                    <div class="card-body text-center">
                        <h2>{{ $personalizedCount }}</h2>
                        <p># of Upgrades</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@endsection

@section("js")
    <script type="text/javascript" src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/2.2.3/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/2.2.3/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/2.2.3/js/buttons.print.min.js"></script>
    <script type="text/javascript">
        $(document).ready( function () {
            $('#transactions').DataTable({
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]
            });
        });
    </script>
@endsection