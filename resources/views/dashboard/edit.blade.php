@extends('layouts.app')

@section('css')
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <style type="text/css">
        .form-group{
            margin-bottom: 10px;
        }
        .form-label{
            font-size: 12px;
        }
        .select2-results {
    max-height: none;
}
.results{
    display: flex;
    flex-wrap:wrap;
    max-height: 600px;
    overflow-y: scroll;
    margin-top: 20px;
}
.results .card img{
    height: 150px;
}
.results .card .card-text{
    font-size: 12px;
    height: 75px;
    overflow: hidden;
}
.viewable-itinerary{
    display: none;
}
#itinerary .viewable-itinerary{
    display: block;
}
article{
    cursor: move;
}
#widget4.active{
    display:inline-table;
}
#widget4 article{
    display: inline-block;
    text-align: center;
}
.timeline .card.col-md-6{
    display: -webkit-inline-box;
}
.timeline .card.col-md-6 .img-fluid{
    width: 135px;
}
.timeline .card.col-md-6 .card-body{
    display: table-caption;
    width: 300px;   
}
.col-md-4 .tab-content .card-body, .tab-content .card{
    padding: 5px;
}
.searchResult{
    background: transparent;
}
.searchResult .card-body{
    background-color: white;
    padding: 12px;
    border-bottom-left-radius: 12px;
    border-bottom-right-radius: 12px;
}
.searchResult .card-img-top{
    border-top-left-radius: 12px;
    border-top-right-radius: 12px;
}
.disabled{
    pointer-events: none;
    background: rgba(0,0,0,0.1);
}
    </style>
    <script src='https://api.mapbox.com/mapbox-gl-js/v1.8.0/mapbox-gl.js'></script>
<link href='https://api.mapbox.com/mapbox-gl-js/v1.8.0/mapbox-gl.css' rel='stylesheet' />

<script src='https://tiles.locationiq.com/v2/js/liq-styles-ctrl-gl.js?v=0.1.6'></script>
<link href='https://tiles.locationiq.com/v2/css/liq-styles-ctrl-gl.css?v=0.1.6' rel='stylesheet' />
    <script src="https://tiles.locationiq.com/v2/libs/gl-geocoder/4.5.1/locationiq-gl-geocoder.min.js?v=0.2.2"></script>
<link rel="stylesheet" href="https://tiles.locationiq.com/v2/libs/gl-geocoder/4.5.1/locationiq-gl-geocoder.css?v=0.2.2" type="text/css" />
@endsection

@section('content')
<div class="container" style="padding-bottom:300">
    <a href="/dashboard">Dashboard</a>
    <form method="POST" action="/dashboard/catalog/edit/" id="groupForm" class="form-vertical">
        {{ CSRF_FIELD() }}
        <input type="hidden" name="tripID" value="{{ $trip['tripID'] }}">
        <input type="hidden" name="archived" value="1" id="willArchive">
        <input type="hidden" name="country" id="country" id="setcountry" value="{{ $trip['country'] }}">
        <input type="hidden" name="latitude" id="latitude" value="{{ $trip['latitude'] }}">
        <input type="hidden" name="longitude" id="longitude" value="{{ $trip['longitude'] }}">
        <input type="hidden" name="destination" id="destination" value="{{ $trip['destination'] }}">
        <div class="row">
            <div class="col-md-8">
                <div class="row">
                    <div class="col-md-12">
                        <ul class="nav nav-pills navtab-bg nav-justified">
                            <li class="nav-item">
                                <a href="#step1" data-bs-toggle="tab" aria-expanded="false" class="nav-link active">
                                    Setup
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="#step2" data-bs-toggle="tab" aria-expanded="true" class="nav-link">
                                    Itinerary
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="#step3" data-bs-toggle="tab" aria-expanded="false" class="nav-link">
                                    Pricing
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="tab-content">
                    <div class="tab-pane active" id="step1">
                        <div class="col-md-12" style="margin-bottom: 20px; padding:0;">
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="header-title">Setup Details</h4>
                                    <div class="row">
                                        
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="form-label">Marketing Title</label>
                                                <input type="text" class="form-control" name="title" value="{{ $trip['title'] }}" required="">
                                            </div>
                                            <div class="form-group">
                                                <label class="form-label">Marketing Description</label>
                                                <input type="text" class="form-control" name="description" value="{{ $trip['description'] }}" required="">
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-md-4">
                                                    <label class="form-label">Destination: {{ $trip['destination'] }}</label>
                                                    <div class="search-box" style="width: 100%" name="destination" value="{{ $trip['destination'] }}"></div>
                                                </div>
                                                <div class="col-md-4">
                                                    <label class="form-label">Start of Trip</label>
                                                    <input type="date" name="departure" id="date" class="form-control form-date detectDayChange" required="" value="{{ Carbon\Carbon::createFromFormat('m/d/Y',$trip['departure'])->format('Y-m-d') }}">
                                                </div>
                                                <div class="col-md-4">
                                                    <label class="form-label">Trip Length (Days)</label>
                                                    <input type="number" min="1" step="1" class="form-control detectBudgetChange skip detectDayChange" name="days" id="days" value="{{ $trip['days'] }}" required="">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-md-12"> 
                                                    <h5>Trip Calendar</h5>
                                                    <div id="calendar" class="row" style="padding:50px 30px;">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="form-group row">
                                                    <div class="col-md-12">
                                                        <h5>Daily Budgets (USD)</h5>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <label class="form-label">Food & Drink</label>
                                                        <input type="number" class="form-control detectBudgetChange" name="foodBudget" value="{{ $trip['foodBudget'] ?? 0 }}" required="">
                                                    </div>
                                                    <div class="col-md-3">
                                                        <label class="form-label">Lodging</label>
                                                        <input type="number" class="form-control detectBudgetChange" name="lodgingBudget" value="{{ $trip['lodgingBudget'] ?? 0 }}" required="">
                                                    </div>
                                                    <div class="col-md-3">
                                                        <label class="form-label">Transportation</label>
                                                        <input type="number" min="0" class="form-control detectBudgetChange" name="transportationBudget" value="{{ $trip['transportationBudget'] ?? 0 }}" required="">
                                                    </div>
                                                    <div class="col-md-3">
                                                        <label class="form-label">Entertainment/Activities</label>
                                                        <input type="number" class="form-control detectBudgetChange" name="activityBudget" value="{{ $trip['activityBudget'] ?? 0 }}" required="">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    
                                                </div>
                                                <div class="form-group row">
                                                    <label>Budget Tracker</label>
                                                    <br>
                                                    <h5>$<span id="budgetTotal">0</span> Total</h5> 
                                                    <br>
                                                    <h6>$<span id="budgetDaily">0</span> Per Day</h6>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="card">
                                
                                <div class="card-body">
                                    <h4 class="header-title">Marketing Tags</h4>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <select class="js-example-basic-multiple form-control" name="tags[]" multiple="multiple" data-minimum-selection-length="2" data-maximum-selection-length="4" required="">
                                                <option value="Plenty of Exercise" @if(in_array("Plenty of Exercise", $trip['tags'])) selected @endif>Plenty of Exercise</option>
<option value="Light Activity" @if(in_array("Light Activity", $trip['tags'])) selected @endif>Light Activity</option>
<option value="Nature" @if(in_array("Nature", $trip['tags'])) selected @endif>Nature Adventure</option>
<option value="Major Savings" @if(in_array("Major Savings", $trip['tags'])) selected @endif>Major Savings</option>
<option value="Ecotourism" @if(in_array("Ecotourism", $trip['tags'])) selected @endif>Ecotourism</option>
<option value="Educational/Cultural" @if(in_array("Educational/Cultural", $trip['tags'])) selected @endif>Educational/Cultural</option>
<option value="Animals/Ecological" @if(in_array("Animals/Ecological", $trip['tags'])) selected @endif>Animals/Ecological</option>
<option value="Foodie" @if(in_array("Foodie", $trip['tags'])) selected @endif>Foodie</option>
<option value="Relaxing" @if(in_array("Relaxing", $trip['tags'])) selected @endif>Relaxing</option>
<option value="Water Fun" @if(in_array("Water Fun", $trip['tags'])) selected @endif>Water Fun</option>
<option value="Artsy" @if(in_array("Artsy", $trip['tags'])) selected @endif>Artsy</option>
<option value="Backpacker" @if(in_array("Backpacker", $trip['tags'])) selected @endif>Backpacker</option>
<option value="For Couples" @if(in_array("For Couples", $trip['tags'])) selected @endif>For Couples</option>
<option value="For Friends" @if(in_array("For Friends", $trip['tags'])) selected @endif>For Friends</option>
<option value="Quick Getaway" @if(in_array("Quick Getaway", $trip['tags'])) selected @endif>Quick Getaway</option>
<option value="Multiple Cities" @if(in_array("Multiple Cities", $trip['tags'])) selected @endif>Multiple Cities</option>
<option value="Holiday" @if(in_array("Holiday", $trip['tags'])) selected @endif>Holiday</option>
<option value="Girls Trip" @if(in_array("Girls Trip", $trip['tags'])) selected @endif>Girls Trip</option>
<option value="Guys Trip" @if(in_array("Guys Trip", $trip['tags'])) selected @endif>Guys Trip</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12" style="margin:20px 0;">
                            <div class="card">
                               
                                <div class="card-body">
                                    <h4 class="header-title">Images</h4>
                                    <div class="row" id="imageBox" style="flex-wrap:wrap; width: 100%;">

                                    </div>
                                    <div class="col-md-12">
                                            <a href="" target="_blank">
                                                <img src="" style="width:100%">
                                            </a>
                                            <br><br>
                                            <div class="form-group">
                                                <label class="form-label">Image URL</label>
                                                <input type="text" name="imageURL" id="imageURL" class="form-control" value="{{ $trip['imageURL'] }}" >
                                            </div>
                                            <div class="form-group">
                                                <label class="form-label">Image Author</label>
                                                <input type="text" name="imageAuthor" id="imageAuthor" class="form-control" value="{{ $trip['imageAuthor'] }}">
                                            </div>
                                            <div class="form-group">
                                                <label class="form-label">Image Author URL</label>
                                                <input type="text" name="imageAuthorProfile" id="imageAuthorProfile" class="form-control" value="{{ $trip['imageAuthorProfile'] }}">
                                            </div>
                                        </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="step2">
                        <div class="row" data-plugin="dragula" data-containers='["itinerary", "widget4"]'>
                            <div class="card">
                                <div class="card-body" id="itinerary">
                                    <div class="form-group">
                                        <h4 class="header-title">Author</h4>
                                        <select class="form-control" name="clientID" required="">
                                            <option value="">Please Select</option>
                                            @foreach($clients as $clientListItem)
                                                <option value="{{ $clientListItem['id'] }}" @if($trip['clientID'] == $clientListItem['id']) selected @endif>{{ $clientListItem['name'] }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <h4 class="header-title">Worldee Key</h4>
                                        <input type="text" class="form-control" name="worldee" value="{{ $trip['worldee'] ?? '' }}">
                                    </div>
                                    <div class="form-group">
                                        <h4 class="header-title">Itinerary Notes</h4>
                                        <small>This is provided to the customer after purchase to help them get the most out of the itinerary. You can place your contact information here along with your services.</small>
                                        <textarea type="text" class="form-control" name="purchaseNote" id="purchaseNote">{{ $trip['purchaseNote'] ?? '' }}</textarea>
                                    </div>
                                    <div class="form-group">
                                        <h4 class="header-title">Notes for Personalization</h4>
                                        <small>This is additional instruction provided to those who decide to purchase their first hour of consultation.</small>
                                        <textarea type="text" class="form-control" name="personalizationNote" id="personalizationNote">{{ $trip['personalizationNote'] ?? '' }}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="step3">
                        <div class="row" data-plugin="dragula" data-containers='["itinerary", "widget4"]'>
                            <div class="card">
                                <div class="card-body">
                                    <div class="form-group">
                                        <h4 class="header-title">Purchase Price (USD)</h4>
                                        <p>Our recommended pricing:</p>
                                        <ul>
                                            <li>1-5 Days: $1 to $10</li>
                                            <li>5-7 Days: $10 to $15</li>
                                            <li>7-10 Days: $15 to $20</li>
                                            <li>10-14+ Days: $20 to $25</li>
                                        </ul>
                                        <p>Place a 0 for each box to denote free:</p>
                                        <input type="number" required="" name="purchasePrice" class="form-control" value="{{ $trip['purchasePrice'] }}" step="1">
                                    </div>
                                    <div class="form-group">
                                        <h4 class="header-title">Concierge Pricing</h4>
                                        <p>This is the price a customer is expected to pay if they would like this itinerary to be personalized.</p>
                                        <input type="number" required="" name="personalizationCost" class="form-control" value="{{ $trip['personalizationCost'] ?? 0 }}" step="1">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <p>Upon submission, you will be directed to editing page where you can further define the trip's agenda and make additional changes before publishing</p>
                            <br>
                            <button type="submit" class="btn btn-warning" id="draft" style="margin-top: 10px">SAVE DRAFT</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4" id="Setup-Search">
                <div class="row">
                    <div class="col-md-12">
                        <ul class="nav nav-pills navtab-bg nav-justified">
                            <li class="nav-item">
                                <a href="#widget1" data-bs-toggle="tab" aria-expanded="false" class="nav-link active">
                                    Attractions
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="#widget2" data-bs-toggle="tab" aria-expanded="true" class="nav-link">
                                    Lodging
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="#widget3" data-bs-toggle="tab" aria-expanded="false" class="nav-link">
                                    Dining
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="#widget4" data-bs-toggle="tab" aria-expanded="false" class="nav-link">
                                    Budget
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="row">
                    <div class="card" style="background-color:transparent;">
                        <div class="card-body" style="padding:0px;">
                            <div class="tab-content">
                                <div class="tab-pane active" id="widget1">
                                    <div class="input-group">
                                        <input type="text" class="form-control query" placeholder="Search attractions" aria-label="Search attractions">
                                        <button class="btn input-group-text btn-dark waves-effect waves-light querySubmit" id="querySubmit" data-category="attractions" type="button">Search</button>
                                    </div>
                                    <div class="loading text-center" style="margin-top:100px; display:none;">
                                        <div class="spinner-border avatar-lg text-primary m-2" role="status"></div>
                                        <p>Loading...</p>
                                    </div>
                                    <div class="results" id="draggable-results1"></div>
                                </div>
                                <div class="tab-pane" id="widget2">
                                    <div class="input-group">
                                        <input type="text" class="form-control query" placeholder="Search hotels" aria-label="Search attractions">
                                        <button class="btn input-group-text btn-dark waves-effect waves-light querySubmit" id="querySubmit" data-category="hotels" type="button">Search</button>
                                    </div>
                                    <div class="loading text-center" style="margin-top:100px; display:none;">
                                        <div class="spinner-border avatar-lg text-primary m-2" role="status"></div>
                                        <p>Loading...</p>
                                    </div>
                                    <div class="results" id="draggable-results1"></div>
                                </div>
                                <div class="tab-pane" id="widget3">
                                    <div class="input-group">
                                        <input type="text" class="form-control query" placeholder="Search dining" aria-label="Search dining">
                                        <button class="btn input-group-text btn-dark waves-effect waves-light querySubmit" id="" data-category="restaurants" type="button">Search</button>
                                    </div>
                                    <div class="loading text-center" style="margin-top:100px; display:none;">
                                        <div class="spinner-border avatar-lg text-primary m-2" role="status"></div>
                                        <p>Loading...</p>
                                    </div>
                                    <div class="results" id="draggable-results1"></div>
                                </div>
                                <div class="tab-pane" id="widget4" style="width:100%">
                                    <div class="col-md-12" id="budgets">

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
@endsection

@section('js')
  <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.3/moment.min.js"></script>
  <!-- Loading buttons js -->
  <script src="/assets/js/dragula/dragula.min.js"></script>
  <script>
        jQuery(document).ready(function ($) {
            $('.js-example-basic-multiple').select2({
                placeholder: "Select tags",
                maximumSelectionLength: 4,
                closeOnSelect: false,
                allowClear: true
            });

            tinymce.init({
              selector: 'textarea#purchaseNote',
              height: 200,
              menubar: false,
              plugins: [
                'link'
              ],
              toolbar: 'undo redo | formatselect | ' +
              'bold italic link | ' +
              'removeformat',
              content_style: 'body { font-family:Helvetica,Arial,sans-serif; font-size:14px }'
            });

            tinymce.init({
              selector: 'textarea#personalizationNote',
              height: 200,
              menubar: false,
              plugins: [
                'link',
              ],
              toolbar: 'undo redo | formatselect | ' +
              'bold italic link | ' +
              'removeformat',
              content_style: 'body { font-family:Helvetica,Arial,sans-serif; font-size:14px }'
            });

            $("body").on("click", ".selectImage", function(){
                $("#imageURL").val($(this).val());
                $("#imageAuthor").val($(this).attr("data-author"));
                $("#imageAuthorProfile").val($(this).attr("data-author-profile"));
            });

            runCalendar();
            runBudget();
            fetchImages($("#destination").val());
            fetchBudgetData($("#country").val());

            $(".detectDayChange").on("change", function(){
                runCalendar();
            });

            function runCalendar(){
                $("#calendar").html("");
                let date = $("#date").val();
                var days = $("#days").val();

                if(date !== undefined && date != ""){
                    var startDate = new moment(date).format("YYYY-MM-DD");
                    var endDate = new moment(date).add(days ,"days").format("YYYY-MM-DD");

                    let caldays = [];

                    for (var m = new moment(startDate); m.isBefore(endDate); m.add(1, 'days')) {
                        caldays.push(m.format('dddd, MM-DD').replace(",", "</span><br>"));
                    }

                    jQuery.each(caldays, function(i, value) {
                        if(i == 0){
                            $("#calendar").append("<div class='col-md-2 text-center' style='width:13%; margin:0 3px; padding:20px 10px; border:1px solid black; border-radius:20px;'><p style='font-size:12px;'><span style='font-weight:bold; font-size:14px;'>" + value + "</p><p>Check-In:<br>3 PM</p></div>");
                        }
                        if(i == caldays.length - 1 && days > 1){
                            $("#calendar").append("<div class='col-md-2 text-center' style='width:13%; margin:0 3px; padding:20px 10px; border:1px solid black; border-radius:20px;'><p style='font-size:12px;'><span style='font-weight:bold; font-size:14px;''>" + value + "</p><p>Check-Out:<br>11 AM</p></div>");
                        }
                        if (i > 0 && i != caldays.length - 1 && days > 1){
                            $("#calendar").append("<div class='col-md-2 text-center' style='width:13%; margin:0 3px; padding:20px 10px; border:1px solid black; border-radius:20px;'><p style='font-size:12px;'><span style='font-weight:bold; font-size:14px;''>" + value + "</p></div>");   
                        }
                    });
                }
            }

            function runBudget(){
                var budget = 0;
                var days = 1;
                var daily = $("#days").val();

                $(".detectBudgetChange").each(function(){
                    if(!$(this).hasClass("skip")){
                        budget += (parseInt($(this).val()) * daily);
                    }
                });

                $("#budgetTotal").text(budget);

                if(daily > 0){
                    var daily = Math.round(budget/parseInt(daily));
                    $("#budgetDaily").text(daily);
                }
            }

            function fetchImages(name){
                $.ajax({
                    url: "https://api.unsplash.com/search/photos?client_id={{ env('UNSPLASH_KEY') }}&per_page=10&query=" + name + " landscape scenery",
                    type: 'get',
                    success: function (data) {
                        $("#imageBox").html("");
                        jQuery.each(data.results, function(name, value) {
                            console.log(value.urls.regular);
                            if(value.user.name == "{{ $trip['imageAuthor'] }}"){
                                $("#imageBox").append("<div class='col-md-3 text-center'><img src='" + value.urls.small + "' style='width:100%;'><br><input type='radio' class='selectImage' name='selectedImage' value='" + value.urls.regular +"' data-author='" + value.user.name +"' data-author-profile='" + value.user.links.html + "' selected='' checked='' ></div>");
                            } else {
                                $("#imageBox").append("<div class='col-md-3 text-center'><img src='" + value.urls.small + "' style='width:100%;'><br><input type='radio' class='selectImage' name='selectedImage' value='" + value.urls.regular +"' data-author='" + value.user.name +"' data-author-profile='" + value.user.links.html + "' required='' ></div>");
                            }
                            
                        });
                    }
                });
            }

            function fetchBudgetData(country){
                $.ajax({
                    url: "https://apiv1.venti.co/api/search/groups/budgets" ,
                    headers: {
                        'Content-Type':'application/json',
                        "x-api-key": "2432432ferf",
                        "country": country
                    },
                    processData: true,
                    dataType: 'json',
                    type: 'post',
                    success: function (data) {
                        $("#budgets").html("");
                        
                        jQuery.each(data, function(i, value) {
                            
                            formatBudgetBox(value).then(() => {
                                
                            });
                        });
                    }
                });
            }

            $(".detectBudgetChange").on("change", function(){
                runBudget();

            });

        });

        //Add your LocationIQ Maps Access Token here (not the API token!)
        locationiq.key = "{{ env("LOCATIONIQ_KEY") }} ";

        //Add Geocoder control to the map
        var geocoder = new MapboxGeocoder({
            accessToken: locationiq.key,
            limit: 5,
            dedupe: 1,
            name: "destination",
            id:"destination",
            getItemValue: function (item) {


                var name = item.address.country;

                if(item.address.city !== undefined){
                    if(item.address.country == "USA" || item.address.country == "United States" || item.address.country == "United States of America"){
                        name = item.address.name;
                    } else {
                        name = item.address.name + " " + item.address.country;
                    }
                }
                $("#destination").val(name);
                $("#country").val(item.address.country);
                $("#latitude").val(item.lat);
                $("#longitude").val(item.lon);

                fetchImages(name);

                fetchBudgetData(item.address.country);

                return item.place_name
            }
        });

        async function formatBudgetBox(value){
            
            var html = "";  
        
            jQuery.each(value, function(x, val) {
                if(x == "Category"){
                    html = html + "<div class='card' ><div class='card-body'><h4>" + x + ": " + val + "</h4>";

                }
                if(x != "Category" && x != "Budget"){
                    html = html + "<p>" + x + ": $" + val + "</p>";
                }
                if(x == "Budget"){
                    html = html + "<p><strong>" + x + ": $" + val + " per Day</strong></p></div></div>";
                }
            });

            $("#budgets").append(html);
        }

        geocoder.addTo('.search-box');

        $(".querySubmit").click(function(){
            var button = $(this);
            var destination = $("#destination").val();
            var country = $("#country").val();
            var latitude = $("#latitude").val();
            var longitude = $("#longitude").val();
            var category = button.attr("data-category");
            var query = button.parent().find(".query").val();

            if(latitude == "" || longitude == ""){
                return alert("Destination required prior to search");
            }
            if(query == ""){
                query = category;
            }
            button.parent().parent().find(".loading").show();
            button.parent().parent().find(".results").html("");
            console.log(query);
            console.log(category);

            $.ajax({
                url: "/api/destinations/search/tripadvisor",
                processData: true,
                dataType: 'json',
                data:{
                    latitude: latitude,
                    longitude: longitude,
                    category: category,
                    search: query
                },
                type: 'post',
                success: function (data) {
                    button.parent().parent().find(".loading").hide();
                    $.each(data, function(i, item) {
                      button.parent().parent().find(".results").append('<div class="card col-md-6 searchResult"><img class="card-img-top img-fluid" src="' + item.image + '" alt="Card image cap"><div class="card-body"><h5 class="card-title">' + item.name +' (' + item.rating + ')</h5><p class="card-text">' + item.description + '</p><a href="' + item.url + '" target="_blank" class="card-link text-custom">Learn More</a></div></div>');
                    });
                }
            });
            
        });
    </script>
@endsection