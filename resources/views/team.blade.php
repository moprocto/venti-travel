@extends("layouts.curator-master")

@section("css")
   <link href="/css/landing.css" rel="stylesheet">
   <style type="text/css">
      .card-header{
         height: 400px;
         width: 100%;
         background-size: cover;
         background-position: bottom;
         border-top-left-radius: 20px;
         border-top-right-radius: 20px;
      }
      .card-body, .tile-footer{
         background: white;
         padding: 20px;
      }
      .pills-tab{
         display: inline-block;
         width: auto;
         margin: 3px 2px;
         padding-right: 10px;
         padding-left: 10px;
         border-radius: 10px;
         background-color: #ecebea;
         font-size: 13px;
         line-height: 20px;
         font-weight: 400;
         text-align: center;
         -o-object-fit: fill;
         object-fit: fill;
      }
      .table td{

      }
      a.btn-primary {
          background-color: #1d7e89;
          border-color: #1d7e89;
          color: #fff;
      }
      .col-md-8 .col-md-4{
         margin-bottom: 20px;
      }
   </style>
@endsection

@section("content")

<div id="page-content">
    <div class="gradient-wrap">
      <!--  HERO -->
      <div class="black position-relative hero-section mb-4" style="margin: 60px 4em;">
         <div class="row">
            <div class="col-md-4">
               <h1 class="mb-3 section-title" style="font-weight:600 !important;">
                  Our Team
                </h1>
                <h3 class="section-subtitle mb-4">
                  Our vision is to empower everyone with the knowledge and confidence to travel to at least 20 countries during their lifetime. Discovering your next adventure with Venti.
                </h3>
            </div>
            <div class="col-md-8">
               <div class="row">
                  <div class="col-md-4">
                     <div class="card" style="border-radius:20px;">
                        <img src="/assets/img/team/markus-proctor.jpg" style="width:100%; border-top-left-radius: 20px; border-top-right-radius: 20px;">
                        <div class="card-body" style="border-bottom-left-radius:20px; border-bottom-right-radius:20px;">
                           <h3 style="margin-bottom:0px;">Markus</h3>
                           <p style="padding-bottom:2px; font-weight:bold;">Founder, CEO & CTO</p>
                           <p>7 countries and counting! Find me cycling, writing code, mentoring, or planning my next trip.</p>
                           <strong>Ask Me About:</strong>
                           <p>🇧🇷 🇨🇦 🇨🇷 🇨🇭 🇫🇷 🇹🇷 🇺🇸 🇧🇸</p>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-4">
                     <div class="card" style="border-radius:20px;">
                        <img src="https://ttwo.dk/wp-content/uploads/2017/08/person-placeholder.jpg" style="width:100%; border-top-left-radius: 20px; border-top-right-radius: 20px;">
                        <div class="card-body" style="border-bottom-left-radius:20px; border-bottom-right-radius:20px;">
                           <h3 style="margin-bottom:0px;">Kaylen</h3>
                           <p style="padding-bottom:2px; font-weight: bold;">Dir. Marketing & Client Success</p>
                           <p>7 countries and counting! Find me cycling, writing code, or planning my next trip.</p>
                           <strong>Ask Me About:</strong>
                           <p>🇧🇷 🇨🇦 🇨🇷 🇨🇭 🇫🇷 🇹🇷 🇺🇸 🇧🇸</p>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
    </div>
  </div>

@endsection

@section("js")
<script src="/js/lightbox/lightbox.min.js"></script>
@endsection