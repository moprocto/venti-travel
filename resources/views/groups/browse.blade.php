@extends("layouts.curator-master")

@section("css")
   <link rel="stylesheet" type="text/css" href="/assets/css/steps.css">
   <link href="/css/jRange/jrange.css" rel="stylesheet" />
   <link href="/css/lightbox/lightbox.min.css" rel="stylesheet" />
   <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
   <link href="/assets/css/store.css" rel="stylesheet" />
   <style type="text/css">
      @media screen and (max-width:  600px){
         .createSection{
            margin: 25px auto;
         }
         .groupFiltersContainer .card-row{
            margin: 0;
         }
         .grid{
            min-height: 300px;
         }
         
      }
      label.form-label{
            width: 100%;
         }
      .loading-box{
         display: none;
      }
      .loading .loading-box{
         width: 100%;
         display: block !important;
         text-align: center;
      }
      .card-container{
         border-radius: 20px;
         box-shadow: 0 1px 4px rgba(0,0,0,0.15);
      }
      select{
         text-align: left;
      }
      .month{
         font-weight: bold;
      }
      .month.strongChemistry{
         background: #0fa517;
      }
      .switch {
  position: relative;
  display: inline-block;
  width: 60px;
  height: 34px;
}

.switch input { 
  opacity: 0;
  width: 0;
  height: 0;
}

.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 26px;
  width: 26px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #2196F3;
}

input:focus + .slider {
  box-shadow: 0 0 1px #2196F3;
}

input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}
.wizard>.content>.body ul>li{
   list-style: none;
}
.wizard>.content{
            min-height: 360px;
        }
        .wizard .steps{
            display: none;
        }
.groupType{
   width: 50px;
   margin: 15px auto
}
</style>
@endsection

@section("content")
   <div id="page-content">
      <div class="gradient-wrap">
         <!--  HERO -->
         <div class="black position-relative hero-section mb-4" style="margin: 2em 4em;">
            <h1>Groups</h1>
            <p>Find the perfect trip or activity</p>
            <div class="row">
               <div class="col-md-3 groupFiltersContainer">
                  <div class="row text-center mb-4">
                     <div class="col-md-12">
                     @if(Session::get('user') !== null)
                        @if(!isProfileComplete(Session::get('user'))[0])
                           <h5>{!! isProfileComplete(Session::get('user'))[1] !!} to create a travel group!</h5>
                        @else
                           <a href="{{ route('create-group') }}" class="btn btn-primary btn-wide" style="border-radius: 10px;"><img src="/assets/img/add-users.png" style="width:22px; height:22px; margin-top:-5px"> CREATE GROUP</a>
                        @endif

                     @else
                        <h5><a href="{{ route('login') }}">Log in</a> to create a travel group!</h5>
                        <p style="color:black">OR</p>
                         <a href="https://apps.apple.com/us/app/venti-friends-and-adventure/id1620282387">
                             <img src="/assets/img/app-store.png" style="width:125px">
                         </a>
                     @endif
                     </div>
                  </div>
                  <div class="row card-row mb-4" style="padding:0">
                     @include('groups.components.filters')
                  </div>
               </div>
               <div class="col-md-9" style="padding: 0 20px;">
                  <div class="row grid loading" id="groupsGrid"><div class="loading-box"><img src="/assets/img/airplane.gif" style="width:200px;"><h3>Fetching groups...</h3></div></div>
               </div>
           </div>
         </div>
       </div>
     </div>
     </div>
    </div>
@endsection

@section("js")
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script type="text/javascript" src="/assets/js/jquery.steps.min.js"></script>
<script src="/js/jRange/jrange.min.js"></script>
<script src="/js/isotope/isotope.js"></script>
<script src="/js/lightbox/lightbox.min.js"></script>
<script src="/assets/js/store.js"></script>
<script>
   jQuery(document).ready(function ($) {
      $.ajaxSetup({
         headers: {
            'X-CSRF-TOKEN': '{{ csrf_token() }}'
         }
      });

      $.ajax({
        url: "{{ route('groups-browse-ajax', ['limit' => 100, 'cache' => 'Y']) }}",
        type: 'post',
        processData: true,
          dataType: 'json',
            success: function (data) {
            $.each(data, function(i, item) {
               $("#groupsGrid").append(item);
            });
            $("#groupsGrid").removeClass("loading");
            
            var $grid = $('.grid').isotope({
                 getSortData: {
                   price: '[data-price] parseInt',
                   length: '[data-length] parseInt',
                   departure: '[data-departure] parseInt'
                 },
                 itemSelector: '.grid-item'
               });
              $grid.isotope('reloadItems');  
              $grid.isotope({ filter: "*" });

              countriesSelect();
              $(".groupAvatars").addClass("opened");
              $("#accordionExample").show();
        }
      });

      $("#compatInfo").click(function(){
         swal({
           icon: 'info',
           title: "What's This?",
           text: "With information you've provided Venti, we highlight certain trips based on how well you'd get along with the overall group. This is only an estimation. We encourage everyone to conduct necessary due diligence before traveling with others.",
         });
      });

      $("#example-basic").steps({
        headerTag: "h3",
        bodyTag: "section",
        autoFocus: true
     });

      $(".groupType").on("change", function(){
         if($(this).val() == "travel"){
            $("#explanation").html("<p>For organizing trips with new people or with your existing friends.</p>");
         }
         c
         if($(this).val() == "chat"){
            $("#explanation").html("<p>To connect with like-minded people and share tips related to travel and adventure.</p>");
         }
         if($(this).val() == "none"){
            $("#explanation2").html("<p>Anyone can see your group and request to join. This is the best way to meet new people.</p>");
         }
         if($(this).val() == "some"){
            $("#explanation2").html('<p>Your group <strong>is visible</strong> to others but they cannot request to join. Group members must be invited by email or username.</p>');
         }
         if($(this).val() == "full"){
            $("#explanation2").html('<p>Your group is <strong>completely hidden</strong>. Only invited group members will be able to view and participate.</p>');
         }

      });

   });
</script>
@endsection