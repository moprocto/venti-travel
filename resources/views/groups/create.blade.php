@extends("layouts.curator-master")

@section('css')
	<link rel="stylesheet" type="text/css" href="/assets/css/steps.css">
	<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
	<script src='https://api.mapbox.com/mapbox-gl-js/v1.8.0/mapbox-gl.js'></script>
	<link href='https://api.mapbox.com/mapbox-gl-js/v1.8.0/mapbox-gl.css' rel='stylesheet' />
	<script src='https://tiles.locationiq.com/v2/js/liq-styles-ctrl-gl.js?v=0.1.6'></script>
	<link href='https://tiles.locationiq.com/v2/css/liq-styles-ctrl-gl.css?v=0.1.6' rel='stylesheet' />
    <script src="https://tiles.locationiq.com/v2/libs/gl-geocoder/4.5.1/locationiq-gl-geocoder.min.js?v=0.2.2"></script>
	<link rel="stylesheet" href="https://tiles.locationiq.com/v2/libs/gl-geocoder/4.5.1/locationiq-gl-geocoder.css?v=0.2.2" type="text/css" />
	<style type="text/css">
		.button-group input, .button-group span{
			display: inline-block !important;
		}
		.button-group{
			max-width: 300px;
    		display: flex;
		}
		.button-group span{
			margin-left: -42px;
		}
		.select2-results {
		    max-height: none;
		}
		.wizard>.content>.body ul>li.select2-selection__choice{
		    display: inline-block !important;
		    max-width: 200px;
		}
				.searchResult{
		    background: transparent;
		}
		.searchResult .card-body{
		    background-color: white;
		    padding: 12px;
		    border-bottom-left-radius: 12px;
		    border-bottom-right-radius: 12px;
		}
		.searchResult .card-img-top{
		    border-top-left-radius: 12px;
		    border-top-right-radius: 12px;
		}
		.form-label{
			font-weight: bold;
		}
		.wizard .steps{
            display: none;
        }
        .select2{
        	width: 100% !important;
        }
	</style>
@endsection

@section('content')

<div id="page-content">
	<div class="gradient-wrap">
		<div class="black position-relative hero-section mb-4" style="margin: 2em 4em;">
			<h1>Create</h1>
		    <div class="row">
		    	<div class="col-md-9">
		    		<form method="POST" id="newGroup" action="{{ route('add-group') }}" >
		    			@CSRF
		    			<input type="hidden" name="tripID" id="tripID" value="{{ Date('mmddYHiss') }}">
				        <input type="hidden" name="archived" value="1" id="willArchive">
				        <input type="hidden" name="country" id="country" id="setcountry">
				        <input type="hidden" name="latitude" id="latitude">
				        <input type="hidden" name="longitude" id="longitude">
				        <input type="hidden" name="destination" id="destination">
				        <input type="hidden" name="imageURL" id="imageURL">
				        <input type="hidden" name="imageAuthorProfile" id="imageAuthorProfile">
				        <input type="hidden" name="imageAuthor" id="imageAuthor">
				    	<div id="example-basic">
						    <h3></h3>
						    <section>
						    	@if(Session::get('destinationError') !== null)
						    	<div class="alert alert-danger">
						    		There was a problem creating your trip. Please try again.
						    	</div>
						    	@endif
						    	<div class="form-group">
						    		<label class="form-label">Type of Group
						    		<span style="font-size: 10px; line-height: 1; display: block;">This can't be changed afterwards</span>
						    		</label>
						    		<select class="form-control groupType"  name="type" id="type" required="">
						    			<option value="travel">Travel</option>
						    			<option value="activity">Activity</option>
						    		</select>
						    		<p id="explanation" class="mt-2">For organizing trips with new people or with your existing friends and family.</p>
						    	</div>
						    	@if((Session::get('user')->customClaims["nav"] ?? null) == true)
							    	<div class="form-group row">
							    		<div class="col-md-12">
							    			<input type="checkbox" name="navigated" value="1"> Navigated
							    		</div>
							    		<p class="mt-2">A Navigated trip means that you are the lead tour guide and that payment will be required for members to travel with you. </p>
							    	</div>
						    	@endif
						        <div class="form-group row">
						        	<div class="col-md-4">
						        		<label class="form-label">Title
						        		<span style="font-size: 10px; line-height: 1; display: block;">Keep this short</span></label>
						        		<input type="text" class="form-control" name="title" id="title" maxlength="35" placeholder="Backpacking in Southern Europe" required="">
						        	</div>
						        	<div class="col-md-4">
                                		<label class="form-label">Preferred Group Size
                                		<span style="font-size: 10px; line-height: 1; display: block;">&nbsp;</span></label>
                                    	<input type="number" name="sizePref" id="sizePref" class="form-control" required="" min="1" step="1" placeholder="3" value="{{ $size }}">
                                	</div>
                                	<div class="col-md-4">
                                		<label class="form-label">Privacy
                                		<span style="font-size: 10px; line-height: 1; display: block;">&nbsp;</span></label>
	                                    <select class="form-control" name="privacy" id="privacy">
	                                    	<option value="Anyone Can Request to Join">Anyone Can Request to Join</option>
	                                    	<option value="Invitation Only. Visible to Others">Invitation Only. Visible to Others</option>
	                                    	<option value="Invitation Only. Invisible to Others">Invitation Only. Invisible to Others</option>
	                                    </select>
                                	</div>
						        </div>
						    </section>
						    <h3></h3>
						    <section>
						    	<div class="form-group">
						        	<label class="form-label">Destination:<br><span style="font-size:12px;">Enter the starting city</span></label>
						        	<div class="container">
						        		<div class="search-box" id="search-box" style="width: 100%" name="destination"></div>
							        	<div id="result"></div>
							        </div>
						        </div>
						        <div class="form-group">
                                    <label class="form-label">Preferred Start Date</label>
                                    <input type="date" name="departure" id="date" class="form-control form-date detectDayChange"  min="{{ Date('Y-m-d') }}">
                                </div>
                                <div class="form-group">
                                	<div class="row">
                                		<div class="col-md-12">
                                			<label class="form-label">Preferred Number of Nights</label>
                                    		<input type="number" name="nights" id="nights" class="form-control"  min="1" step="1" placeholder="3" value="3">
                                		</div>
                                	</div>
                                </div>
						    </section>
						    <h3></h3>
						    <section>
						    	<div class="form-group">
                                	<label class="form-label">Tags (Max: 4)
                                		<span style="font-size: 10px; line-height: 1; display: block;">These help others determine if your trip will be a good fit before sending a join request.</label>
                                			<br>
                                	<select class="js-example-basic-multiple form-control" name="tags[]" multiple="multiple" data-minimum-selection-length="0" data-maximum-selection-length="4" >
                                        <option value="Plenty of Exercise">Plenty of Exercise</option>
                                        <option value="Light Activity">Light Activity</option>
                                        <option value="Nature">Nature Adventure</option>
                                        <option value="Major Savings">Major Savings</option>
                                        <option value="Ecotourism">Ecotourism</option>
                                        <option value="Educational/Cultural">Educational/Cultural</option>
                                        <option value="Animals/Ecological">Animals/Ecological</option>
                                        <option value="Foodie">Foodie</option>
                                        <option value="Relaxing">Relaxing</option>
                                        <option value="Water Fun">Water Fun</option>
                                        <option value="Backpacker">Backpacker</option>
                                        <option value="Artsy">Artsy </option>
                                        <option value="For Couples">For Couples</option>
                                        <option value="For Friends">For Friends</option>
                                        <option value="Quick Getaway">Quick Getaway</option>
                                        <option value="Multiple Cities">Multiple Cities</option>
                                        <option value="Holiday">Holiday</option>
                                        <option value="Girls Trip">Girls Trip</option>
                                        <option value="Guys Trip">Guys Trip</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                	<label class="form-label">Description
                                	<span style="font-size: 10px; line-height: 1; display: block;">Optional, but we highly recommend adding something here</span></label>
                                	<textarea class="form-control" placeholder="What's the purpose of this group?" name="description"></textarea>
                                </div>
						    </section>
						</div>
					</form>
				</div>
		    </div>
		</div>
	</div>
</div>

@endsection

@section('js')
<script type="text/javascript" src="/assets/js/jquery.steps.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script>
	jQuery(document).ready(function ($) {

	$("#example-basic").steps({
	    headerTag: "h3",
	    bodyTag: "section",
	    transitionEffect: "slideLeft",
	    autoFocus: true,
	    onStepChanging: function (event, currentIndex, priorIndex) { 
	    	if(currentIndex == 0){
	    		var title = $("#title").val();
	    		var type = $("#type").val();

	    		if(title == "" || title.length > 35){
	    			alert("Title cannot be blank or longer than 35 characters.");
	    			return false;
	    		}
	    		if(type == "travel"){
	    			return true;
	    		}
	    		if(type == "activity" && title != ""){
			    	$("#newGroup").submit();
	    		}

	    	}

	    	if(currentIndex == 1 && priorIndex != 0){
	    		var destination = $("#destination").val();
	    		if(destination == ""){
		    		alert("Enter a destination before proceeding.");
		    		return false;
		    	}
		    	var date = $("#date").val();
		    	if(date == ""){
		    		alert("Enter a preferred start date");
		    		return false;
		    	}
		    	return true;
	    	}
	    	
	    	
	    	
	    	
	    	
	    },
	    onFinished: function(event, currentIndex){
	    	var destination = $("#destination").val();
	    	var date = $("#date").val();
	    	if(destination != "" && date != ""){
	    		$("#newGroup").submit();
	    	}
	    }
	});

	$('.js-example-basic-multiple').select2({
        placeholder: "Select tags",
        maximumSelectionLength: 4,
        closeOnSelect: false,
        allowClear: true
    });

	//Add your LocationIQ Maps Access Token here (not the API token!)
        locationiq.key = "{{ env('LOCATIONIQ_KEY') }} ";

        //Add Geocoder control to the map
        var geocoder = new MapboxGeocoder({
            accessToken: locationiq.key,
            limit: 5,
            dedupe: 1,
            name: "destination",
            id:"destination",
            getItemValue: function (item) {
                var name = item.address.name;

                if(item.address.city !== undefined){
                    if(item.address.country == "USA" || item.address.country == "United States" || item.address.country == "United States of America"){
                        name = item.address.name;
                    } else {
                        name = item.address.name + " " + item.address.country;
                    }
                }

                $("#tripJoin").text($("#tripID").val());
                $("#destination").val(name);
                $("#country").val(item.address.country);
                $("#latitude").val(item.lat);
                $("#latJoin").text(cleanNumber(item.lat));
                $("#longitude").val(item.lon);
                $("#longJoin").text(cleanNumber(item.lat));


                $.ajax({
                    url: "https://api.unsplash.com/search/photos?client_id={{ env('UNSPLASH_KEY') }}&per_page=10&query=" + name + " landscape scenery",
                    type: 'get',
                    success: function (data) {
                        $("#imageBox").html("");
                        jQuery.each(data.results, function(name, value) {
                            $("#imageURL").val(value.urls.regular);
                        	$("#imageAuthor").val(value.user.name);
                        	$("#imageAuthorProfile").val(value.user.links.html);
                        	return false;
                        });
                        
                    }
                });

                return item.place_name
            }
        });

        geocoder.addTo('#search-box');

        function cleanNumber(text){
        	text = text.replace(".", "");
        	text = text.replace("-", "");
        	return text;
        }

        function createTrip(){
        	$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': $("#newGroup input:eq(0)").val()
				}
			});

        	$.ajax({
	                url: "{{ route('add-group') }}",
	                processData: true,
	                dataType: 'json',
	                data:{
	                	tripID: $("#tripID").val(),
	                    latitude: $("#latitude").val(),
	                    longitude: $("#longitude").val(),
	                    country: $("#country").val(),
	                    destination: $("#destination").val(),
	                    departure: $("#date").val(),
	                    nights: $("#nights").val(),
	                    sizePref: $("#sizePref").val(),
	                    privacy: $("#privacy").val(),
	                    imageURL: $("#imageURL").val(),
	                    imageAuthor: $("#imageAuthor").val(),
	                    imageAuthorProfile: $("#imageAuthorProfile").val()
	                },
	                type: 'post',
	                success: function (data) {
	                    
	                }
	        });
        }

        $("#sendEmailInvite").click(function(){
        	var email = $("#emailInvite").val();

        	if(email != ""){
        		$.ajaxSetup({
					headers: {
						'X-CSRF-TOKEN': $("#newGroup input:eq(0)").val()
					}
				});

        		$.ajax({
	                url: "{{ route('group-invite') }}",
	                processData: true,
	                dataType: 'json',
	                data:{
	                	tripID: $("#tripID").val(),
	                	email: email
	                },
	                type: 'post',
	                success: function (data) {
	                    
	                }
	            });
        	}
        });
        $(".groupType").on("change", function(){
         if($(this).val() == "travel"){
            $("#explanation").html("<p>For organizing trips with new people or with your existing friends.</p>");
            $("#title").attr("placeholder", "Backpacking in Southern Europe");
         }
         if($(this).val() == "activity"){
            $("#explanation").html("<p>For trying out new activities, restaurants, or just go exploring with new people or existing friends.</p>");
         	$("#title").attr("placeholder", "Wine Tasting in Napa Valley");
         }
     	});
    });
</script>
@endsection
