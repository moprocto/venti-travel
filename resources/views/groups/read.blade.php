@extends("layouts.curator-master")

@section('css')
	<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
	<link href="/assets/css/store.css" rel="stylesheet" />
	<link href="/assets/css/trip.css" rel="stylesheet" />
	<link href="/assets/css/chat.css" rel="stylesheet" />
	<style type="text/css">
		.image-background{
			width: 100%;
			min-height: 300px;
			border-top-left-radius: 20px;
			border-top-right-radius: 20px;
			background-size: cover;
			background-position: center;
		}
		#minimizeVideoFrame{
			display: none;
			cursor: pointer;
		}
	</style>
@endsection

@section('content')
<div id="page-content">
	<div class="gradient-wrap">
		<div class="black position-relative hero-section mb-4" style="margin: 2em 4em;">
		   	<div class="row">
		   		<div class="col-md-7">
		   			@if($isTravel)
			   			<div class="card mb-4" style="border-radius: 20px">
			   				<div class="image-background" style="background-image: url('{{ $group['imageURL'] }}')"></div>
			   				
			   				<div class="card-body">
			   					@if(isset($group['title']) && $group["title"] != "")
			   						<h1>{{ $group['title'] }}</h1>
			   					@else
			   						<h1>{{ $group['days'] }} Days in {{ $group['destination'] }}</h1>
			   					@endif
			   					<ul style="list-style:none;padding:0;margin: 10px 0;display: block; text-align:left;">
			   						
			   						<li><span class="px-2"><i class="fa fa-location-dot"></i></span>{{ getTripLocation($group["destination"], $group["country"]) }}</li>
			   						<li><span class="px-2"><i class="fa fa-calendar"></i></span>{{ Date('M d, Y',strtotime($group["departure"])) }} ➜ {{ Date('M d, Y',strtotime($group["checkOut"])) }}</li>
			   						
			   						<li><span class="px-2"><i class="fa fa-users"></i></span>Preferred Group Gize: {{ $group["sizePref"] }}</li>
			   					</ul>
			   					<div class="tile-footer">
						            <div style="flex-wrap: flex;">
						               @if(array_key_exists("tags", $group))
						                  @foreach((array)$group['tags'] as $tag)
						                  <div class="pills-tab" style="background-color:rgba({{ strlen($tag['label'] ?? $tag) * 20  }}, 168, {{ strlen($tag['label'] ?? $tag) * 10  }}, 0.45)">{{ $tag['label'] ?? $tag }}</div>
						                  @endforeach
						               @endif
						            </div>
						      </div>
			   					@if(isset($group["description"]))
			   						<p>{!! displayTextWithLinks($group["description"]) !!}</p>

			   					@endif
			   				</div>
			   				
			   			</div>
		   			@else
		   				<div class="card mb-4 text-left" style="padding:2em; border-radius: 20px; background: {{ $group['color'] ?? '#000000'  }}">
		   					<h1 style="color:white;">{{ $group['title'] }}</h1>
		   					<p style="color:white;">{{ $group['description'] ?? "Details coming soon..." }}</p>
		   					<div class="tile-footer" style="padding:0;">
						            <div style="flex-wrap: flex;">
						               @if(array_key_exists("tags", $group))
						                  @foreach((array)$group['tags'] as $tag)
						                  <div class="pills-tab" style="padding: 10px 20px; border-radius: 20px; font-size: 14px; background-color:white; color: black; font-weight: bold;">{{ $tag }}</div>
						                  @endforeach
						               @endif
						            </div>
						      </div>
		   				</div>
		   			@endif

		   			<h2 class="mt-4">Who's In</h2>
		   			<div class="row members" style="border-radius: 20px; padding:0;">
	   					@if(Session::get('user') === null)

	   						@if($group["privacy"] == "none" || $group["privacy"] == "Anyone Can Request to Join")
	   						<div class="col-md-3">
	   							<div class="card" style="border-radius: 20px; text-align: center; width: 100%; min-height: 200px; display: flex; justify-content: center; padding: 20px;">
	   								<p style="font-size:20px; font-weight: bold; margin-bottom:20px">Want In?</p>
	   								<a href="/login" class="btn btn-primary">LOG IN</a>
	   								<span>OR</span>
	   								<a href="https://apps.apple.com/us/app/venti-friends-and-adventure/id1620282387">
	   									<img src="/assets/img/app-store.png" style="width:100%">
	   								</a>
	   							</div>
	   						</div>
	   						@endif
	   					@endif
	   					@foreach($members as $member)
	   					@if($member["member"] == null)
	   						@php  continue; @endphp
	   					@endif
	   					@if($member["role"] != "invited" && $member["role"] != "requested" && $member["member"]->uid != "mnTmABt3MDOWyy76pwG7V0QACpD2")
	   						<div class="col col-md-3 col-xs-6">
	   							
	   								@include('users.components.profile-widget', ["user" => $member["member"], "pdf" => false])
	   							
	   						</div>
	   						@endif
	   					@endforeach
		   			</div>
		   			<h2 class="mt-4">Notes</h2>
		   			<div class="text-right">
		   			<p class="text-left">Only members of the group can see notes.</p>
			   			@if(Session::get('user') !== null && ($myRole == "owner" || $myRole == "viewer"))
			   				@if($group["clientID"] == Session::get('user')->uid)
			   					
			   					@include('groups.components.editor')
			   					
			   					@else

			   					<div class="card" style="border-radius: 20px">
			   						<div class="card-body" style="border-radius: 20px;">
			   							
			   							{!! $group["itinerary"] !!}
			   						</div>
			   					</div>
			   				@endif
			   			@endif
		   			</div>
		   		</div>
		   		@if(Session::get('user') !== null)
			   		@if($group["clientID"] == Session::get('user')->uid)
			   			<div class="col-md-5 tripWidget">
			   				@include('groups.components.owner')
			   				<div class="col-md-12 mt-4">
			   					
			   				</div>
	   					</div>
			   		@else
			   			@if($group['privacy'] == 'none' || $group['privacy'] == 'Anyone Can Request to Join')
			   				<div class="col-md-5 tripWidget">
				   				@include('groups.components.nonmember')
				   				<div class="col-md-12 mt-4">
				   					
				   				</div>
		   					</div>
			   			@else
			   			@if($recommended == true)
			   				<div class="col-md-5 tripWidget">
				   				@include('groups.components.nonmember')
				   				<div class="col-md-12 mt-4">
				   					
				   				</div>
		   					</div>
			   			@else
			   				<div class="col-md-5 tripWidget">
			   					<div class="col-md-12">
			   						<div class="card" style="border-radius: 20px">
							   				<div class="card-body">
							   					This group is not taking join requests at this time
							   				</div>
						   			</div>
			   					</div>
					   		</div>
					   	@endif
			   			@endif
			   		@endif

			   	@else
		   		@endif
		    </div>
		</div>
	</div>
	@include('groups.components.chat')
</div>
@endsection

@section('js')
	<script src="{{ asset('js/app.js') }}" defer></script>
	<script src="https://cdn.tiny.cloud/1/f1cdeokivvyzyohrpb09kwayr4k53ovtgo1849tqerl354st/tinymce/6/tinymce.min.js" referrerpolicy="origin"></script>
	<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    @if($myRole == "owner" || $myRole == "viewer")
    	@include('groups.components.chatJS')
    @endif
	<script>
	jQuery(document).ready(function ($) {

		$("body").click(function(e){
			
        if(e.target.classList.contains("chatOpener")){

          if($("#chatBubble").hasClass("opener")){
            $("#chatBubble").addClass("opened");
            scrollChatList();
          }
        }
        if(e.target.classList.contains("chatCloser")){
          closeChat();
        }
    });

    function closeChat(){
    	if($("#chatBubble").hasClass('opened')) {
        $("#chatBubble").removeClass("opened");
      }
    }

	  function scrollChatList(){
	    setTimeout(function(){
	      var height = 0;

	    $(".chat-body ul li").each(function(){
	        height += $(this).height() * 2;
	    });
	      $(".chat-body ul").animate({ scrollTop: height }, "fast");
	    }, 1000);
	  }

        var name = '{{ $group["destination"] . " " . $group["country"] }}';

    $.ajax({
      url: "https://api.unsplash.com/search/photos?client_id={{ env('UNSPLASH_KEY') }}&per_page=10&query=" + name + " landscape scenery",
      type: 'get',
      success: function (data) {
          $("#imageBox").html("");
          jQuery.each(data.results, function(name, value) {
              $("#imageBox").append("<div class='col-md-3 text-center' style='padding:0'><img src='" + value.urls.small + "' style='width:100%;'><br><input type='radio' class='selectImage' name='selectedImage' value='" + value.urls.regular +"' data-author='" + value.user.name +"' data-author-profile='" + value.user.links.html + "'></div>");
          });
      }
  	});

	    

	    $('.js-example-basic-multiple').select2({
	        placeholder: "Select tags",
	        maximumSelectionLength: 4,
	        closeOnSelect: false,
	        allowClear: true
	    });
	    
	    $("#sendInvite").click(function(){
	    	  var user = $("#userInvite").val();

	    	  if(user != "" && user.length > 3){
		    	  	$.ajax({
				        url: "{{ route('group-invite') }}",
				        type: 'post',
				        headers: {
									'X-CSRF-TOKEN': '{{ csrf_token() }}'
								},
				        processData: true,
		                dataType: 'json',
		                data:{
		                	tripID: "{{ $group['tripID'] }}",
		                	user: user
			            },
				        success: function (data) {
				        	console.log(data);
				            if(data < 500){
				            	$("#userInvite").val("");
				            	$("#members").append('<tr><td width="65"><a href="/u/' + data.username + '"><img src="' + data.photoUrl +'" style="width:50px; height:50px; border-radius: 50%;"></a></td><td>' + data.displayName + '</td><td class="text-center">Invited</td><td class="text-right"><button type="button" class="removeMember btn btn-dark" data-uid="' + data.uid + '"><i class="fa fa-times"></i></button></td></tr>');
				            }
				            
				            if(data == 500){
				            	alert("Entry cannot be blank :)");
				            }
				            if(data == 501){
				            	alert("You cannot add yourself :)");
				            }
				            if(data == 502){
				            	alert("This user cannot be added");
				            }
				            if(data == 503){
				            	alert("You cannot add someone that is already in your group :)");
				            }
				            if(data == 504){
				            	alert("We could not find that user.");
				            }
				            if(data == 505){
				            	alert("This user is not accepting invites at this time.");
				            }
				            if(data == 506){

				            	alert("We could not find an account belonging to " + $("#userInvite").val() + ", but we sent them an email with you in copy!");
				            	$("#userInvite").val("");
				            }
				            if(data == 507){
				            	alert("You've already sent more than 10 invites for this group. Please wait for people to respond. We will automatically remove inactive invites.");
				            }
				        }
			    	});
	    	 	}
	    });

	    

	    $("body").on("click", ".removeMember", function(){
    		var element = $(this);
    		var user = $(this).attr("data-uid");
    		$.ajax({
				        url: "{{ route('group-member-remove') }}",
				        type: 'post',
				        headers: {
									'X-CSRF-TOKEN': '{{ csrf_token() }}'
								},
				        processData: true,
		                dataType: 'json',
		                data:{
		                	tripID: "{{ $group['tripID'] }}",
		                	user: user
		            },
				        success: function (data) {
				        	if(data == 200){
				        		element.parent().parent().remove();
				        	}
				        }
		    	});
	    });
	    
	    

		  $("body").on("click", ".replyInvite", function(){
		  	  

	        var tripID = $(this).attr('data-tripID');
	        var decide = $(this).attr('data-decide');
	        var uid = $(this).attr('data-uid');
	        var row = $(this).parent().parent();
	        var reason = $("#declineReason").val();
	        // accept invite

	        if(decide == "decline"){
	        	if(reason == ""){
	        		return alert("Please let us know why you're declining this invite so we can improve your experience on Venti.");
	        	}
	        }


	        $.ajax({
	            url: "{{ route('group-invite-reply') }}",
	            processData: true,
	            dataType: 'json',
	            data:{
	                tripID: tripID,
	                decide: decide,
	                uid: uid,
	                reason: reason
	            },
	            type: 'post',
	            headers: {
								'X-CSRF-TOKEN': '{{ csrf_token() }}'
							},
	            success: function (data) {
	            		if(data == 200){
	            			row.find(".member-role").text("Viewer");
	            			@if(Session::get('user') !== null)
	            				if(uid == "{{ Session::get('user')->uid }}"){
	            					row.find(".member-status").html('');
	            				} else {
	            					row.find(".member-status").html('<button type="button" class="removeMember btn btn-dark" data-uid="'+uid+'"><i class="fa fa-times"></i></button>');
	            				}
	            			@endif
	            		}
	            		if(data == 201){
	            			row.remove();
	            			if(decide == "decline"){
	            				window.location.reload();
	            			}
	            		}
	            }
	        });
	    });

		  @if($myRole == "owner")
		    tinymce.init({
	        selector: 'textarea#editor',
	        menubar: false,
	        plugins: [
	          'link', 'lists'
	        ],
	        toolbar: 'undo redo saveChangesButton | formatselect | ' +
	        'bold italic underline alignleft aligncenter alignright | numlist bullist link | ' +
	        'removeformat',
	        content_style: 'body { font-family:Helvetica,Arial,sans-serif; font-size:14px }',
	        setup: function (editor) {
				    editor.ui.registry.addButton('saveChangesButton', {
				      image: 'http://p.yusukekamiyamane.com/icons/search/fugue/icons/calendar-blue.png',
				      tooltip: 'Save Changes',
				      disabled: false,
				      onAction: function (_) {
				        var myContent = tinymce.get("editor").getContent();
				        updateTripPlan(myContent);
				      }
				    });
				  }
	      });

	      function updateTripPlan(myContent){
	      	$.ajax({
		        url: "{{ route('group-planner', ['tripID' => $group['tripID']]) }}",
		        type: 'post',
		        headers: {
							'X-CSRF-TOKEN': '{{ csrf_token() }}'
						},
		        processData: true,
            dataType: 'json',
            data:{
            	tripID: "{{ $group['tripID'] }}",
            	plan: myContent
            },
		        success: function (data) {
		            if(data == 200){
		            	$("#save-status").html("<div class='alert alert-success'>Changes Saved Successfully!</div>");
		            } else{

		            }
		        }
					});
	      }

	      $("body").on("click", ".submitDetailsForm" , function(e){
	      		var myContent = tinymce.get("editor").getContent();
	      		updateTripPlan(myContent);

	      		if($("#date").val() == "" || $("#nights").val() == ""){
	      			swal({
				  icon: 'error',
				  title: 'Oops...',
				  text: 'Departure date and preferred number of nights required!',
				})
	      		} else {
	      			$("#tripDetails").submit();
	      		}

	      		
	      		
	      });
	    


	  	@endif
	  	
	  	@if(Session::get('editGroupError') !== null)
	  		swal({
				  icon: 'error',
				  title: 'Oops...',
				  text: 'We could not save your changes. Check to make sure all group details are filled out. Contact admin@venti.co if you continue having problems.',
				})
	  	@endif
	  	@if(Session::get('editGroupSuccess') !== null)
	  		swal({
				  icon: 'success',
				  title: 'All Set!',
				  text: 'Your changes have been saved successfully',
				})
	  	@endif

	  	@if(($myRole == "owner" || $myRole == "viewer" ) && array_key_exists("VIDEOSDK_MEETING_ID", $group)) 
			  var script = document.createElement("script");
			  script.type = "text/javascript";
			  script.addEventListener("load", function (event) {

			    const meeting = new VideoSDKMeeting();
			    
			    const config = {
			      name: "{{ getFirstName(Session::get('user')->displayName) }}",
			      apiKey: "698b95f2-8fa5-4053-b6cf-772d3d2ae6e5", 
			      meetingId: "{{ $group['VIDEOSDK_MEETING_ID'] }}", 
			      micEnabled: true,
			      webcamEnabled: true,
			      redirectOnLeave: false,
			      participantCanLeave: true,
			      participantCanToggleSelfWebcam: true,
			      participantCanToggleSelfMic: true,
			      screenShareEnabled: true,
			      pollEnabled: false,
			      chatEnabled: false,
			      whiteboardEnabled: true,
			      raiseHandEnabled: true,
			      roomId: "{{ $group['tripID'] }}",
			      participantId: "{{ Session::get('user')->uid }}",
			      brandLogoURL: "https://venti.co/assets/img/venti-trademark-logo-white.png",
			      containerId: "videoframe",
			      joinScreen: {
			        visible: false, // Show the join screen ?
			        title: "Chat: {{ $group['title'] }}" , // Meeting title
			        meetingUrl: window.location.href, // Meeting joining url
			      },
			    };

			    $("#startVideoCall").click(function(){
				  	if($(this).hasClass("on")){
				  		$("#videoframe").toggle();
				  		$("#startVideoCall").html('<img src="/assets/img/video-off.png" style="width:24px">');
				  		$(this).removeClass("on");
				  		$("#chatBubble").removeClass("mb-5");
				  		leaveCall();

					  	} else {
					  		$("#videoframe").toggle();
					  		meeting.init(config);
					  		
					  		$("#startVideoCall").html('<img src="/assets/img/video-hide.png" style="width:24px">');
					  		$(this).addClass("on");
					  		$("#chatBubble").addClass("mb-5");
					  		closeChat();

					  		// get the room ID

					  		var participantId = "{{ Session::get('user')->uid }}";
				  			var roomId = "{{ $group['tripID'] }}";

				  			
				  			var bodyJSON = JSON.stringify({"participantId" : participantId,"roomId" : roomId});

					  		$.ajax({
					        url: "https://api.videosdk.live/v2/sessions/?roomId=" + roomId,
					        type: 'get',
					        processData: true,
			            dataType: 'json',
			            headers:{
											"Authorization": "{{ Session::get($group['tripID'] . '-JWT') }}",
											"Content-Type": "application/json"
			            },
			            body: bodyJSON,
			            data: bodyJSON,
					        success: function (data) {
					        	$("#startVideoCall").attr("data-roomId", data.data[0].roomId);
					        }
							});

					  	}
					  });

			  });
  		
	  		var mediaControls = setInterval(loadMediaControls, 1000);

	  		function loadMediaControls(){
	  			if($("#startVideoCall").hasClass("on")){
	  				$("#minimizeVideoFrame").show();
	  			} else {
	  				$("#minimizeVideoFrame").hide();
	  			}
	  		}

	  		$("#minimizeVideoFrame").click(function(){
	  			$(this).hide();
	  			$("#videoframe").toggle();
					$("#startVideoCall").html('<img src="/assets/img/video-off.png" style="width:24px">');
	  			$("#startVideoCall").removeClass("on");
	  		});

	  		script.src = "https://sdk.videosdk.live/rtc-js-prebuilt/0.3.21/rtc-js-prebuilt.js";
	  		document.getElementsByTagName("head")[0].appendChild(script);


	  		function leaveCall(){
	  			var participantId = "{{ Session::get('user')->uid }}";
	  			var roomId = $("#startVideoCall").attr("data-roomId");
	  			
	  			var bodyJSON = JSON.stringify({"participantId" : participantId,"roomId" : roomId});

	  			console.log(bodyJSON);

	  			$.ajax({
			        url: "https://api.videosdk.live/v2/sessions/participants/remove",
			        type: 'post',
			        processData: true,
	            dataType: 'json',
	            headers:{
									"Authorization": "{{ Session::get($group['tripID'] . '-JWT') }}",
									"Content-Type": "application/json"
	            },
	            body: bodyJSON,
	            data: bodyJSON,
			        success: function (data) {

			        }
					});
	  		}

  		@endif

  		@if($isMember)
	  		$("#declineInvite").click(function(){

	  			var tripID = $(this).attr("data-tripID");
	  			var myhtml = document.createElement("div");
	  			var declineOptions = "{!! declineOptions($myRole, $group['type']) !!}";
	  			html = "<h4>Why are you declining this invite?</h4><p>Your response is private and will help us improve your experience on Venti.</p><select class='form-control' id='declineReason' required><option value=''>-- Please Select --</option>" + declineOptions + "</select><br><button class='btn btn-danger replyInvite' data-uid='{{ Session::get('user')->uid }}' data-tripID='" + tripID +"' data-decide='decline' id='declineButton' >Decline Invite</button>";
	  			myhtml.innerHTML = html;
					swal({
							  title: "Uh oh...", 
							  content: myhtml,
							  buttons: {
							    cancel: true
							  }
					});
				});
			
			@endif

	});
	</script>
@endsection