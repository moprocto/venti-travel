<div class="col-md-4">
    <ul class="nav nav-pills navtab-bg nav-justified mb-4">
        <li class="nav-item">
            <a href="#group" data-bs-toggle="tab" aria-expanded="true" class="nav-link active">
                GROUP
            </a>
        </li>
        <li class="nav-item">
            <a href="#group" data-bs-toggle="tab" aria-expanded="true" class="nav-link">
                SETTINGS
            </a>
        </li>
    </ul>
    <div class="card" style="border-radius: 20px">
        <div class="card-body">
            <div class="tab-content">
            	<div class="tab-pane active text-center" id="group">
                    @include('groups.components.members-table')

                    @if($group['privacy'] == 'none' && $myRole != "denied")
                        <a href="/" class="btn btn-primary mt-4">Request to Join Group</a>
                    @else
                        
                    @endif
            	</div>
            </div>
        </div>
    </div>
</div>