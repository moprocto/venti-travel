@php
   $chemistry = null;
   if($data["groupChemistry"] > 85){
      $chemistry = "strongChemistry";
   }
@endphp
<div class="col-md-4 grid-item {{ $chemistry }} {{ substr($data['departure'], 5,2) }}month {{$data['days']}}days @if(array_key_exists('tags', $data)) @foreach((array)$data['tags'] as $tag){{ str_replace(' ','_',$tag['value'] ?? $tag) }} @endforeach @endif {{ cleanCountry($data['country']) }}" data-price='{{ $data["purchasePrice"] ?? 0 }}' data-length='{{ $data["days"] }}' data-departure='{{ str_replace("-","",$data["departure"])}}' onclick="window.location.href = '/group/{{ $data['tripID'] }}'" data-country='{{ $data["country"] }}' data-chemistry={{ $chemistry }}>
   <div class="card-container" style="border-radius:23px; border:2px solid #4cc0e1; box-shadow: 0 1px 4px rgba(76, 192, 225,0.15);">
      <div class="card-header" style="
      background-image:  
      linear-gradient(to bottom, rgba(10, 10, 10, 0.5), rgba(76, 192, 255, 0.3)),
    url('{{ $data['imageURL'] }}');

      background-position:center;
      min-height: 450px;
      border-bottom-right-radius: 20px;
      border-bottom-left-radius: 20px;
      "
      >
         <div class="groupAvatars">
            <img src='{{ $data["author"]["photoUrl"] }}' class="image-0 animatedable">
            @if(($data["author"]["customClaims"]['nav'] ?? null) === true)
               <img src="/assets/img/navigator-verified.png" style="width: 17px; position: absolute; top: 11px; left: -6px; z-index: 500;" title="Verified"/> 
            @endif
            @php
               $i = 1;
            @endphp
            @foreach($data["groupAvatars"] as $groupAvatar)
                  <img src='{{ $groupAvatar }}' class="image-{{$i}} animatedable">
                  @php 
                     $i++ 
                  @endphp
               @endforeach
         </div>
         <span class="month @if($chemistry == 'strongChemistry') strongChemistry @endif" @if($chemistry == 'strongChemistry') title='You might be a great addition'  @endif style="background:white; color:black;">{{ $data['days'] }} DAY(S)</span>
         <div class="card-body" style="background:none;">
            <h4 style="min-height: 48px; margin:0; color:white; margin-top:40px;">{{ getTripTitle($data["title"], $data["days"], $data["destination"]) }}</h4>
            <ul class="group-detail-list" style="color:white;">
               <li>
                  <div class="detail-list">
                     <p class="list-detail-icon"><i class="fa fa-location-dot"></i></p>
                     <p class="list-detail">{{ getTripLocation($data["destination"], $data["country"]) }}</p>
                  </div>
               </li>
               <li>
                  <div class="detail-list">
                     <p class="list-detail-icon"><i class="fas fa-plane-arrival"></i></p>
                     <p class="list-detail">{{ getTripDate($data["departure"]) }}</p>
                  </div>
               </li>
               <li>
                  <div class="detail-list">
                     <p class="list-detail-icon"><i class="fas fa-plane-departure"></i></p>
                     <p class="list-detail">{{ getTripDate($data["checkOut"]) }}</p>
                  </div>
               </li>
               <li>
                  <div class="detail-list">
                     <p class="list-detail-icon"><i class="fa fa-users"></i></p>
                     <p class="list-detail">{{ $data["groupSize"] }} Member(s) / {{ $data["sizePref"] }} Total</p>
                  </div>
               </li>
            </ul>
            <div class="tile-footer" style="background:none; padding:0">
               <div style="flex-wrap: flex;">
                  @if(array_key_exists("tags", $data))
                     @foreach((array)$data['tags'] as $tag)
                     <div class="pills-tab" style="color: white; font-weight: bold; background-color:rgba({{ strlen($tag['value'] ?? $tag) * 20  }}, 168, {{ strlen($tag['value'] ?? $tag) * 10  }}, 1)">{{ $tag['value'] ?? $tag }}</div>
                     @endforeach
                  @endif
               </div>
            </div>
         </div>
      </div>
      
      
   </div>
</div>
