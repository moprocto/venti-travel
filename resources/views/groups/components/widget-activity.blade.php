@php
   $bg = getRandomColor();
@endphp
<div class="col-md-4 col-xs-12 grid-item  @if(array_key_exists('tags', $group)) @foreach((array)$group['tags'] as $tag){{ str_replace(' ','_',$tag) }} @endforeach @endif" onclick="window.location.href = '/group/{{ $group['tripID'] }}'">
   <div class="card-container" style="background-color: {{ $group['color'] ?? $bg }}">
      <div class="card-header" style="height: 100px;">
         <img src='{{ $group["author"]["photoUrl"] }}' style="width: 50px; height: 50px; position: absolute; top: 15px; border-radius: 50%;">
      </div>
      <div class="card-body" style="background-color: {{ $group['color'] ?? $bg }}">
         <h1 style="color: white; min-height: 48px; margin:0;">{{ $group["title"] }}</h1>
         <ul class="group-detail-list">
            <li>
               <div class="detail-list">
                  <p class="list-detail-icon"><i class="fa fa-users text-light"></i></p>
                  <p class="list-detail text-light">{{ $group["groupSize"] }} Member(s) / {{ $group["sizePref"] }} Total</p>
               </div>
            </li>
         </ul>
      </div>
      <div class="tile-footer" style="background-color: {{ $group['color'] ?? $bg }}">
            <div style="flex-wrap: flex;">
               @if(array_key_exists("tags", $group))
                  @foreach((array)$group['tags'] as $tag)
                  <div class="pills-tab" style="padding: 8px 16px; border-radius: 20px; font-size: 12px; background-color:white; color: black; font-weight: bold;">{{ $tag }}</div>
                  @endforeach
               @endif
            </div>
      </div>
   </div>
</div>