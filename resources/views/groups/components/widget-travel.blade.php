<div class="col-md-4 col-xs-12 grid-item {{ substr($group['departure'], 5,2) }}month {{$group['days']}}days @if(array_key_exists('tags', $group)) @foreach((array)$group['tags'] as $tag){{ str_replace(' ','_',$tag['value'] ?? $tag) }} @endforeach @endif" data-price='{{ $group["purchasePrice"] ?? 0 }}' data-length='{{ $group["days"] }}' data-departure='{{ str_replace("/","",$group["departure"])}}' onclick="window.location.href = '/group/{{ $group['tripID'] }}'">
   <div class="card-container">
      <div class="card-header" style="background-image:  url('{{ $group['imageURL'] }}');">
         <span class="month">{{ $group['days'] }} DAY(S)</span>
      </div>
      <div class="card-body">
            <img src='{{ $group["author"]["photoUrl"] }}' style="width: 50px; height: 50px; position: absolute; top: 15px; border-radius: 50%;">
         <h4 style="min-height: 48px; margin:0;">{{ getTripTitle($group["title"], $group["days"], $group["destination"]) }}</h4>
         <ul class="group-detail-list">
            <li>
               <div class="detail-list">
                  <p class="list-detail-icon"><i class="fa fa-location-dot"></i></p>
                  <p class="list-detail">{{ getTripLocation($group["destination"], $group["country"]) }}</p>
               </div>
            </li>
            <li>
               <div class="detail-list">
                  <p class="list-detail-icon"><i class="fas fa-plane-arrival"></i></p>
                  <p class="list-detail">{{ getTripDate($group["departure"]) }}</p>
               </div>
            </li>
            <li>
               <div class="detail-list">
                  <p class="list-detail-icon"><i class="fas fa-plane-departure"></i></p>
                  <p class="list-detail">{{ getTripDate($group["checkOut"]) }}</p>
               </div>
            </li>
            <li>
               <div class="detail-list">
                  <p class="list-detail-icon"><i class="fa fa-users"></i></p>
                  <p class="list-detail">{{ $group["groupSize"] }} Member(s) / {{ $group["sizePref"] }} Total</p>
               </div>
            </li>
         </ul>
      </div>
      
      <div class="tile-footer">
            <div style="flex-wrap: flex;">
               @if(array_key_exists("tags", $group))
                  @foreach((array)$group['tags'] as $tag)
                  <div class="pills-tab" style="background-color:rgba({{ strlen($tag['value'] ?? $tag) * 20  }}, 168, {{ strlen($tag['value'] ?? $tag) * 10  }}, 0.45)">{{ $tag['value'] ?? $tag }}</div>
                  @endforeach
               @endif
            </div>
      </div>
      
   </div>
</div>