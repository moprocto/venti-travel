<div id="chatBubble" class="btn btn-primary opener chatOpener">
    <div class="chat-header">
        <!-- <button class="btn btn-white" id="chatMenu"><i class="fa fa-ellipsis text-dark" style="font-size: 24px; font-weight: 900;"></i></button>
        -->
        <p class="chat-title">GROUP CHAT</p>
        <div class="chatMenuList">
        </div>
        <button id="closeChat" class="closer chatCloser btn btn-white"><i class="fa fa-times text-dark chatCloser" style="font-size: 24px; font-weight:900"></i></button>
         @if($myRole == "owner" || $myRole == "viewer")
        <button id="startVideoCall" class="btn btn-white" style="float:left; margin-left:25px;"><img src="/assets/img/video-off.png" style="width:24px"></button>
        @endif
    </div>
    <div class="chat-body">
        @if($myRole == "owner" || $myRole == "viewer")
        <ul></ul>
        @else
            <h3 style="padding-top:50%;">You need to be a member of this group to view the chat</h3>
            <br>
            @if(Session::get('user') === null)
                <a href="/login" class="btn btn-primary">LOG IN</a>
                <p style="color:black">OR</p>
                                    <a href="https://apps.apple.com/us/app/venti-friends-and-adventure/id1620282387">
                                        <img src="/assets/img/app-store.png" style="width:125px">
                                    </a>
            @else
                @if($myRole == "invited" || $myRole == "requested")
                    <p class="text-dark">You'll be granted access once you're admitted into the group</p>
                @else
                    <a href="{{ route('read-group-join', ['tripID' => $group['tripID']] ) }}" class="mt-4 btn btn-primary">REQUEST TO JOIN</a>
                @endif
            @endif
        @endif
    </div>
    <div class="chat-toolbar">
        @if($myRole == "owner" || $myRole == "viewer")
        <input type="text" class="form-control" id="message" placeholder="Share something with the group..."><button class="btn btn-primary" id="chatSend"><i class="fa fa-paper-plane"></i></button>
        @endif
    </div>
    <i class="fa fa-comments text-white chatOpener" style="font-size: 1.9em;"></i>
</div>
<div id="videoframe"></div>
<div id="minimizeVideoFrame" style="position:fixed; top:20px; left:20px; z-index:9999;"><i class="fa-solid fa-down-left-and-up-right-to-center" style="font-size: 26px; color:white;"></i></div>
