<div class="accordion" id="accordionExample" style="width:100%; padding:0;">
  <div class="accordion-item" style="border-radius:20px;">
    <h2 class="accordion-header" id="headingOne" style="border-radius: 20px;">
      <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne" style="border-top-left-radius:20px; border-top-right-radius:20px;">FILTERS</button>
    </h2>
    <div id="collapseOne" class="accordion-collapse collapse show" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
      <div class="accordion-body">
         @if(Session::get('user') !== null)
         <div class="form-group row">
            <!--
            <div class="col-md-9">
               <label class="form-label">HIGH COMPATIBILIY <a href="#" id="compatInfo">[?]</a>:</label>
            </div>

            <div class="col-md-3">
               <label class="switch">
                 <input type="checkbox" class="filter" id="strongChemistry" value=".strongChemistry" @if(!isProfileComplete(Session::get('user'))[0]) disabled @endif>
                 <span class="slider round"></span>
               </label>
            </div>
            -->
            @if(!isProfileComplete(Session::get('user'))[0])
               <div class="col-md-12">
                  {!! isProfileComplete(Session::get('user'))[1] !!} to use this filter.
               </div>
            @endif
         </div>
         @endif
         <div class="form-group">
            <label class="form-label">TRIP LENGTH: <span id="trip-range"></span></label>
            <br>
            <div id="day-slider" class="slider-round"></div>
         </div>
         <div class="form-group">
            <label class="form-label">BY MONTH</label>
            <select class="form-control" id="month-select" style="padding:5px 20px; height:35px;">
               <option name="" value="">SHOW ALL</option>
               <option name="January" value="01month">JANUARY</option>
               <option name="February" value="02month">FEBRUARY</option>
               <option name="March" value="03month">MARCH</option>
               <option name="April" value="04month">APRIL</option>
               <option name="May" value="05month">MAY</option>
               <option name="June" value="06month">JUNE</option>
               <option name="July" value="07month">JULY</option>
               <option name="August" value="08month">AUGUST</option>
               <option name="September" value="09month">SEPTEMBER</option>
               <option name="October" value="10month">OCTOBER</option>
               <option name="November" value="11month">NOVEMBER</option>
               <option name="December" value="12month">DECEMBER</option>
            </select>
         </div>
         <div class="form-group">
            <label class="form-label">BY COUNTRY</label>
            <select class="form-control filter" id="countries">
               <option value="">SHOW ALL</option>
            </select>
         </div>
         <div class="form-group">
            <label class="form-label">BY TAGS</label>
            <select class="js-example-basic-multiple form-control" name="tags[]" multiple="multiple" data-minimum-selection-length="2" data-maximum-selection-length="4" required="">
                <option value=".Plenty_of_Exercise">PLENTY OF EXERCISE</option>
                <option value=".Light_Activity">LIGHT ACTIVITY</option>
                <option value=".Nature">NATURE ADVENTURE</option>
                <option value=".Major_Savings">MAJOR SAVINGS</option>
                <option value=".Ecotourism">ECOTOURISM</option>
                <option value=".Educational/Cultural">EDUCATIONAL/CULTURAL</option>
                <option value=".Animals/Ecological">ANIMALS/ECOLOGICAL</option>
                <option value=".Foodie">FOODIE</option>
                <option value=".Relaxing">RELAXING</option>
                <option value=".Water_Fun">WATER FUN</option>
                <option value=".Backpacker">BACKPACKER</option>
                <option value=".Artsy">ARTSY</option>
                <option value=".For_Couples">FOR COUPLES</option>
                <option value=".For_Friends">FOR FRIENDS</option>
                <option value=".Quick_Getaway">QUICK GETAWAY</option>
                <option value=".Multiple_Cities">MULTIPLE CITIES</option>
                <option value=".Holiday">HOLIDAY</option>
                <option value=".Girls_Trip">GIRLS TRIP</option>
                <option value=".Guys_Trip">GUYS TRIP</option>
            </select>
         </div>
         <div class="form-group">
            <p style="font-weight:bold;">SORT</p>
         </div>
         <div class="form-group">
            <select class="form-control" style="padding:5px 20px; height:35px;" id="sort">
               <option>SELECT</option>
               <option value="departureASC" data-sort="ASC">EARLIEST START</option>
               <option value="departureDESC" data-sort="DESC">LATEST START</option>
               <option value="lengthASC" data-sort="ASC">SHORTEST TRIPS</option>
               <option value="lengthDESC" data-sort="DESC">LONGEST TRIPS</option>
            </select>
         </div>
            </div>
    </div>
  </div>
</div>