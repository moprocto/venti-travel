<table style="width:100%;">
    <tbody id="members">
        @foreach($members as $member)
            @if(!is_null($member['member']) && $member["member"]->uid != "mnTmABt3MDOWyy76pwG7V0QACpD2")
            <tr>
                <td width="65"><a href="/u/{{ $member['member']->customClaims['username'] }}"><img src="{{ $member['member']->photoUrl }}" style="width:50px; height:50px; border-radius: 50%;"></a></td>
                <td>{{ getFirstName($member['member']->displayName) }}</td>
                <td class="text-center member-role">{{ ucfirst($member['role']) }}</td>
                <td class="text-right member-status">
                    @if(Session::get('user') !== null)
                        @if($member['member']->uid != Session::get('user')->uid)
                            @if($group['clientID'] == Session::get('user')->uid)
                                @switch($member['role'])
                                    @case('viewer')
                                        <button type="button" class="removeMember btn btn-dark" data-uid="{{ $member['member']->uid }}"><i class="fa fa-times"></i></button>
                                        @break
                                    @case('invited')
                                        <button type="button" class="removeMember btn btn-dark" data-uid="{{ $member['member']->uid }}"><i class="fa fa-times"></i></button>
                                        @break
                                    @case('requested')
                                        <button type="button" class="replyInvite btn btn-success" data-tripID="{{ $group['tripID'] }}" data-decide="approve" data-uid="{{ $member['member']->uid }}"><i class="fa fa-check"></i></button>
                                        <button type="button" class="btn btn-danger replyInvite" data-tripID="{{ $group['tripID'] }}" data-decide="decline" data-uid="{{ $member['member']->uid }}"><i class="fa fa-times"></i></button>
                                    @break
                                @endswitch
                            @endif
                        @endif
                        @if(($member['role'] == "invited" || $member['role'] == "recommended") && $member['member']->uid == Session::get('user')->uid)
                            <button type="button" class="replyInvite btn btn-success" data-tripID="{{ $group['tripID'] }}" data-decide="accept" data-uid="{{ $member['member']->uid }}"><i class="fa fa-check"></i></button>
                            <button type="button" id="declineInvite"  class="btn btn-danger" data-tripID="{{ $group['tripID'] }}" data-decide="decline" data-uid="{{ $member['member']->uid }}"><i class="fa fa-times"></i></button>

                        @endif
                    @endif
                </td>
            </tr>
            @endif
        @endforeach
    </tbody>
</table>