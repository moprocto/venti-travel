<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <form>
          <input type="hidden" name="tripID" id="tripID" value="{{ Date('mmddYHiss') }}">
          <input type="hidden" name="archived" value="1" id="willArchive">
          <input type="hidden" name="country" id="country" id="setcountry">
          <input type="hidden" name="latitude" id="latitude">
          <input type="hidden" name="longitude" id="longitude">
          <input type="hidden" name="destination" id="destination">
          <input type="hidden" name="imageURL" id="imageURL">
          <input type="hidden" name="imageAuthorProfile" id="imageAuthorProfile">
          <input type="hidden" name="imageAuthor" id="imageAuthor">
          <div id="example-basic">
            <h3></h3>
            <section>
                <div class="row mb-3 justify-content-center">
                    <div class="col-md-12 text-center mb-4">
                        <h4>What kind of group is this?</h4>
                    </div>
                    <div class="row justify-content-center" style="width:100%;">
                      <div class="col-md-4 text-center">
                        <ul class="text-center" style="list-style:none; border:1px solid black; border-radius:20px; padding: 20px; padding-bottom: 0;">
                          <li>
                            <img src="/assets/img/plane-departure.png" style="height:48px;">
                            <br>
                            <p class="mb-0">TRAVEL</p>
                          </li>
                          <li><input type="radio" name="type" required="" checked="" value="travel" class="groupType"></li>
                        </ul>
                      </div>
                      <div class="col-md-4 text-center">
                        <ul class="text-center" style="list-style:none; border:1px solid black; border-radius: 20px; padding: 20px; padding-bottom: 0;">
                          <li>
                            <i class="fa fa-hiking" style="font-size:48px;"></i>
                            <br>
                            <p class="mb-0">ACTIVITY</p>
                          </li>
                          <li><input type="radio" name="type" required="" value="activity" class="groupType"></li>
                          <li></li>
                        </ul>
                      </div>
                      <div class="col-md-4 text-center">
                        <ul class="text-center" style="list-style:none; border:1px solid black; border-radius: 20px; padding: 20px; padding-bottom: 0;">
                          <li>
                            <img src="/assets/img/comments.png" style="height:48px;">
                            <br>
                            <p class="mb-0">CHAT</p>
                          </li>
                          <li><input type="radio" name="type" required="" value="chat" class="groupType"></li>
                          <li></li>
                        </ul>
                      </div>
                    </div>
                    <div id="explanation" class="mt-4">
                      <p>For organizing trips with new people or with your existing friends.</p>
                    </div>
                </div>
            </section>
            <h3></h3>
            <section>
              <div class="row mb-3 justify-content-center">
                <div class="col-md-12 text-center mb-4">
                    <h4>What level of privacy?</h4>
                </div>
                <div class="row justify-content-center" style="width:100%;">
                      <div class="col-md-4 text-center">
                        <ul class="text-center" style="list-style:none; border:1px solid black; border-radius:20px; padding: 20px; padding-bottom: 0;">
                          <li>
                            <img src="/assets/img/unlocked.png" style="height:48px;">
                            <br>
                            <p class="mb-0">OPEN</p>
                          </li>
                          <li><input type="radio" name="privacy" required="" checked="" value="none" class="groupType"></li>
                        </ul>
                      </div>
                      <div class="col-md-4 text-center">
                        <ul class="text-center" style="list-style:none; border:1px solid black; border-radius: 20px; padding: 20px; padding-bottom: 0;">
                          <li>
                            <img src="/assets/img/letter.png" style="height:48px;">
                            <br>
                            <p class="mb-0">PARTIAL</p>
                          </li>
                          <li><input type="radio" name="privacy" required="" value="some" class="groupType"></li>
                          <li></li>
                        </ul>
                      </div>
                      <div class="col-md-4 text-center">
                        <ul class="text-center" style="list-style:none; border:1px solid black; border-radius: 20px; padding: 20px; padding-bottom: 0;">
                          <li>
                            <img src="/assets/img/locked.png" style="height:48px;">
                            <br>
                            <p class="mb-0">FULL</p>
                          </li>
                          <li><input type="radio" name="privacy" required="" value="full" class="groupType"></li>
                          <li></li>
                        </ul>
                      </div>
                    </div>
                    <div id="explanation2" class="mt-4">
                      <p>Anyone can see your group and request to join. This is the best way to meet new people.</p>
                    </div>
              </div>
            </section>
            <h3></h3>
            <section>
              <div class="form-group">
                <label class="form-label">Destination:<br><span style="font-size:12px;">Enter the starting city</span></label>
                <div class="container">
                  <div class="search-box" id="search-box" style="width: 100%" name="destination"></div>
                  <div id="result"></div>
                </div>
              </div>
              
            </section>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">CANCEL</button>
      </div>
    </div>
  </div>
</div>