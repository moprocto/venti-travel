<div class="col-md-12">
    <ul class="nav nav-pills navtab-bg nav-justified mb-4">
        <li class="nav-item">
            <a href="#group" data-bs-toggle="tab" aria-expanded="true" class="nav-link active">
                GROUP
            </a>
        </li>
        <li class="nav-item">
            <a href="#settings" data-bs-toggle="tab" aria-expanded="true" class="nav-link">
                SETTINGS
            </a>
        </li>
    </ul>
    <div class="card" style="border-radius: 20px">
        <div class="card-body">
            <div class="tab-content">
            	<div class="tab-pane active text-center" id="group">
                    @include('groups.components.members-table')
                    
                    @if(($group['privacy'] == 'none' || $group['privacy'] == 'Anyone Can Request to Join') && !$isMember && $myRole != "denied")
                        <div class="form-group mt-4">
                            @if(!isProfileComplete(Session::get('user'))[0])
                               <h5>{!! isProfileComplete(Session::get('user'))[1] !!} to join a travel group!</h5>
                            @else
                                <a href="{{ route('read-group-join', ['tripID' => $group['tripID']] ) }}" class="text-primary mt-4">I'm Interested!</a>
                            @endif
                        </div>
                    @else
                        @if($isMember)
                        <div class="form-group mt-4">
                            <a href="{{ route('read-group-leave', ['tripID' => $group['tripID']] ) }}" class="text-danger mt-4">Leave Group</a>
                        </div>
                        @endif
                    @endif
            	</div>
                <div class="tab-pane text-center" id="settings">
                    <a href="mailto:admin@venti.co?subject=Reporting Trip: {{ $group['tripID'] }}">Report Trip</a>
                </div>
            </div>
        </div>
    </div>
</div>