@php
   $chemistry = null;
   $bg = getRandomColor();
   if($data["groupChemistry"] > 85){
      $chemistry = "strongChemistry";
   }
@endphp
<div class="col-md-4 grid-item {{ $chemistry }}  @if(array_key_exists('tags', $data)) @foreach((array)$data['tags'] as $tag){{ str_replace(' ','_',$tag) }} @endforeach @endif {{ cleanCountry($data['country']) }}" onclick="window.location.href = '/group/{{ $data['tripID'] }}'" data-country="" data-chemistry={{ $chemistry }}>
   <div class="card-container" style="background-color: {{ $data['color'] ?? $bg }}">
      <div class="card-header" style="height: 100px;">
         <div class="groupAvatars">
            <img src='{{ $data["author"]["photoUrl"] }}' class="image-0 animatedable">
            @if(($data["author"]["customClaims"]['nav'] ?? null) === true)
               <img src="/assets/img/navigator-verified.png" style="width: 17px; position: absolute; top: 11px; left: -6px; z-index: 500;" title="Verified"/> 
            @endif
            @php
               $i = 1;
            @endphp
            @foreach($data["groupAvatars"] as $groupAvatar)
                  <img src='{{ $groupAvatar }}' class="image-{{$i}} animatedable">
                  @php 
                     $i++ 
                  @endphp
               @endforeach
         </div>
         
      </div>
      <div class="card-body" style="background-color: {{ $data['color'] ?? $bg }}">
         <h1 style="color: white; min-height: 48px; margin:0;">{{ $data["title"] }}</h1>
         <ul class="group-detail-list">
            <li>
               <div class="detail-list">
                  <p class="list-detail-icon"><i class="fa fa-users text-light"></i></p>
                  <p class="list-detail text-light">{{ $data["groupSize"] }} Member(s) / {{ $data["sizePref"] }} Total</p>
               </div>
            </li>
         </ul>
      </div>
      <div class="tile-footer" style="background-color: {{ $data['color'] ?? $bg }}">
            <div style="flex-wrap: flex;">
               @if(array_key_exists("tags", $data))
                  @foreach((array)$data['tags'] as $tag)
                  <div class="pills-tab" style="padding: 8px 16px; border-radius: 20px; font-size: 14px; background-color:white; color: black; font-weight: bold;">{{ $tag }}</div>
                  @endforeach
               @endif
            </div>
      </div>
   </div>
</div>
