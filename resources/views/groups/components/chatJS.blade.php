<script>
  
  var chatRefresh = setInterval(loadMessages, 2000);

  jQuery(document).ready(function ($){

    $("#chatSend").click(function(){
      sendMessage();
    });

    $("#message").on("keydown", function(event) {
      if(event.which == 13){
        sendMessage();
      }
    });

    function sendMessage(){
      var message = $("#message").val();

      $.ajax({
        url: "{{ route('chat-send', ['tripID' => $group['tripID']]) }}",
        type: 'post',
        processData: true,
            dataType: 'json',
            data:{
            msg: message,
        },
        headers: {
          'X-CSRF-TOKEN': '{{ csrf_token() }}'
        },
        success: function (data) {
          if(data == 200){
            loadMessages();
            $("#message").val("");
          }
        }
      });
    }
  });

  function loadMessages(){

    $.ajax({
        url: "{{ route('chat-pull', ['tripID' => $group['tripID']]) }}",
        type: 'post',
        processData: true,
            dataType: 'json',
            data:{
        },
        headers: {
          'X-CSRF-TOKEN': '{{ csrf_token() }}'
        },
        success: function (data) {
          if(data != "500"){

            var currentHTML = jQuery.parseHTML($(".chat-body ul").html().toString());
            var newHTML = jQuery.parseHTML( data );
            if(currentHTML.length != newHTML.length){
              
            $(".chat-body ul").html(data);
            } else{
            }
          }
        }
    });
  }
</script>