@extends('layouts.curator-master', ["fontless" => true])

@section('css')
	<style>
		nav, footer{
			display: none !important;
		}
		.hero-section,.card-body{
			margin: 0 !important;
			padding: 0 !important;
		}
	</style>
@endsection

@section('content')
	<div id="page-content">
		<div class="gradient-wrap">
			<div class="black position-relative hero-section mb-4">
			   	<div class="row">
			   		<div class="col-md-9">
			   			<div class="card">
			   				<div class="card-body" style="min-height:600px; padding:5em;">
			   					<h2 style="width:100%; text-align:center;"><a style="color:#074297" href="/group/{{ $group['tripID'] }}">{{ $group["title"] }}: Trip Plan</a></h2>
			   					<p style="width:100%; text-align:left; margin-top:50px;">{!! displayTextWithLinks($group["description"]) !!}</p>
								<ul style="list-style:none;padding:0;margin: 10px 0;display: block; text-align:left;">
									<li>{{ getTripLocation($group["destination"], $group["country"]) }}</li>
									<li></span>{{ Date('M d, Y',strtotime($group["departure"])) }} ➜ {{ Date('M d, Y',strtotime($group["checkOut"])) }}</li>
								</ul>
			   					<h2>Who's Going</h2>
			   					<div style="" class="row members">
			   					@foreach($members as $member)
			   						<div class="col col-md-3 col-xs-6">
			   							@if($member["role"] != "invited" && $member["role"] != "requested")
			   								@include('users.components.profile-widget', ["user" => $member["member"], "pdf" => false])
			   							@endif
			   						</div>
			   					</div>
			   					@endforeach
			   					<h2>The Plan</h2>
			   					<div id="plan">
			   						{!! $group["itinerary"] !!}
			   					</div>
			   				</div>
			   			</div>
			   		</div>
			   	</div>
			</div>
		</div>
	</div>
@endsection

@section('js')

@endsection