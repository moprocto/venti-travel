<div class="col-md-4 grid-item {{ substr($group['departure'], 5,2) }}month {{$group['days']}}days @if(array_key_exists('tags', $group)) @foreach((array)$group['tags'] as $tag){{ str_replace(' ','_',$tag['value'] ?? $tag) }} @endforeach @endif {{ cleanCountry($group['country']) }}" data-price='{{ $group["purchasePrice"] ?? 0 }}' data-length='{{ $group["days"] }}' data-departure='{{ str_replace("-","",$group["departure"])}}' onclick="window.location.href = '/group/{{ $group['tripID'] }}'" data-country='{{ $group["country"] }}' >
   <div class="card-container" style="border-radius:23px; border:2px solid #4cc0e1; box-shadow: 0 1px 4px rgba(76, 192, 225,0.15);">
      <div class="card-header" style="
      background-image:  
      linear-gradient(to bottom, rgba(10, 10, 10, 0.5), rgba(76, 192, 255, 0.3)),
      url('{{ $group['imageURL'] }}');
      background-position:center;
      min-height: 450px;
      border-bottom-right-radius: 20px;
      border-bottom-left-radius: 20px;
      "
      >
         <div class="groupAvatars">
            <img src='{{ $group["author"]["photoUrl"] }}' class="image-0" style="width: 50px; height: 50px; position: absolute; top: 15px; border-radius: 50%;">
            @if(($group["author"]["customClaims"]['nav'] ?? null) === true)
               <img src="/assets/img/navigator-verified.png" style="width: 17px; position: absolute; top: 11px; left: -6px; z-index: 500;" title="Verified"/> 
            @endif
         </div>
         <span class="month"  style="background:white; color:black; font-weight: bold;">{{ $group['days'] }} DAY(S)</span>
         <div class="card-body" style="background:none;">
            <h4 style="min-height: 48px; margin:0; color:white; margin-top:40px;">{{ getTripTitle($group["title"], $group["days"], $group["destination"]) }}</h4>
            <ul class="group-detail-list" style="color:white;">
               <li>
                  <div class="detail-list">
                     <p class="list-detail-icon"><i class="fa fa-location-dot"></i></p>
                     <p class="list-detail">{{ getTripLocation($group["destination"], $group["country"]) }}</p>
                  </div>
               </li>
               <li>
                  <div class="detail-list">
                     <p class="list-detail-icon"><i class="fas fa-plane-arrival"></i></p>
                     <p class="list-detail">{{ getTripDate($group["departure"]) }}</p>
                  </div>
               </li>
               <li>
                  <div class="detail-list">
                     <p class="list-detail-icon"><i class="fas fa-plane-departure"></i></p>
                     <p class="list-detail">{{ getTripDate($group["checkOut"]) }}</p>
                  </div>
               </li>
               <li>
                  <div class="detail-list">
                     <p class="list-detail-icon"><i class="fa fa-users"></i></p>
                     <p class="list-detail">{{ $group["groupSize"] }} Member(s) / {{ $group["sizePref"] }} Total</p>
                  </div>
               </li>
            </ul>
            <div class="tile-footer" style="background:none; padding:0">
               <div style="flex-wrap: flex;">
                  @if(array_key_exists("tags", $group))
                     @foreach((array)$group['tags'] as $tag)
                     <div class="pills-tab" style="color: white; font-weight: bold; background-color:rgba({{ strlen($tag['value'] ?? $tag) * 20  }}, 168, {{ strlen($tag['value'] ?? $tag) * 10  }}, 1)">{{ $tag['value'] ?? $tag }}</div>
                     @endforeach
                  @endif
               </div>
            </div>
         </div>
      </div>
      
      
   </div>
</div>
