<div class="col-md-12">
	<ul class="nav nav-pills navtab-bg nav-justified" style="margin-bottom:10px">
        <li class="nav-item">
            <a href="#basics" data-bs-toggle="tab" aria-expanded="false" class="nav-link active">
                DETAIL
            </a>
        </li>
        <li class="nav-item">
            <a href="#gallery" data-bs-toggle="tab" aria-expanded="false" class="nav-link">
                DESIGN
            </a>
        </li>
        <li class="nav-item">
            <a href="#group" data-bs-toggle="tab" aria-expanded="true" class="nav-link">
                GROUP
            </a>
        </li>
        <li class="nav-item">
            <a href="#admin" data-bs-toggle="tab" aria-expanded="false" class="nav-link">
                ADMIN
            </a>
        </li>
    </ul>
    <div class="card" style="border-radius: 20px">
        <div class="card-body">
            <form method="POST" action="/group/{{ $group['tripID'] }}" id="tripDetails">
            
            <div class="tab-content">
                <div class="tab-pane active" id="basics">
                        @CSRF
                        <div class="form-group">
                            <label class="form-label">Title</label>
                            <input type="text" class="form-control" max="60" name="title" @if(isset($group['title']) && $group['title'] != '') value="{{ $group['title'] }}" @else value="{{ ($group['days'] ) . ' Days in ' . $group['destination'] }}" @endif>
                        </div>
                        @if((Session::get('user')->customClaims["nav"] ?? null) == true)
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <input type="checkbox" name="navigated" value="1" @if(($group['navigated']) ?? null == 1) @endif disabled=""> Navigated (to change this, please contact admin@venti.co)
                                </div>
                                <p class="mt-2">A Navigated trip means that you are the lead tour guide and that payment will be required for members to travel with you. </p>
                            </div>
                        @endif
                        <div class="form-group">
                            <label class="form-label">Description<br>
                            <span style="font-size:12px;">This is public to everyone</span></label>
                            <textarea name="description" class="form-control">{{ $group["description"] ?? "" }}</textarea>
                        </div>
                        @if($isTravel)
                        <div class="form-group">
                            <label class="form-label">Preferred Start Date<br>
                            <span style="font-size:12px;">Changing this will not modify the itinerary</span></label>
                            <input type="date" name="departure" id="date" class="form-control form-date detectDayChange" required="" min="{{ Date('Y-m-d') }}" value="{{ $group['departure'] }}">
                        </div>
                        <div class="form-group">
                            <label class="form-label">Preferred Number of Nights<br>
                            <span style="font-size:12px;">Changing this will not modify the itinerary</span></label>
                            <input type="number" name="nights" id="nights" class="form-control" required="" min="1" step="1" placeholder="3" value="{{ $group['days'] - 1 }}">
                        </div>
                        @endif
                        <div class="form-group">
                            <label class="form-label">Preferred Max Group Size<br><span style="font-size:12px;">Changing this will not remove group members</span></label>
                            <input type="number" name="sizePref" id="sizePref" class="form-control" required="" min="1" step="1" placeholder="3" value="{{ $group['sizePref'] }}">
                        </div>
                        <div class="form-group">
                            <label class="form-label">Privacy</label>
                            <select class="form-control" name="privacy" id="privacy">
                                <option value="Anyone Can Request to Join" @if($group['privacy'] == 'Anyone Can Request to Join') selected @endif>Anyone Can Request to Join</option>
                                <option value="Invitation Only. Visible to Others" @if($group['privacy'] == 'Invitation Only. Visible to Others') selected @endif>Invitation Only. Visible to Others</option>
                                <option value="Invitation Only. Invisible to Others" @if($group['privacy'] == 'Invitation Only. Invisible to Others') selected @endif>Invitation Only. Invisible to Others</option>
                            </select>
                        </div>
                        
                        <div class="form-group">
                            <label class="form-label">Tags</label>
                            @php
                                $tags = [];

                                if(array_key_exists("tags", $group)){
                                    foreach($group["tags"] as $tag){
                                        array_push($tags, $tag["value"] ?? $tag);
                                    }
                                }
                            @endphp
                            <select class="js-example-basic-multiple form-control" name="tags[]" multiple="multiple" data-minimum-selection-length="2" data-maximum-selection-length="4" required="">
                                <option value="Plenty of Exercise" @if(in_array("Plenty of Exercise", $tags)) selected @endif>Plenty of Exercise</option>
                                <option value="Light Activity" @if(in_array("Light Activity", $tags)) selected @endif>Light Activity</option>
                                <option value="Nature" @if(in_array("Nature", $tags)) selected @endif>Nature Adventure</option>
                                <option value="Major Savings" @if(in_array("Major Savings", $tags)) selected @endif>Major Savings</option>
                                <option value="Ecotourism" @if(in_array("Ecotourism", $tags)) selected @endif>Ecotourism</option>
                                <option value="Educational/Cultural" @if(in_array("Educational/Cultural", $tags)) selected @endif>Educational/Cultural</option>
                                <option value="Animals/Ecological" @if(in_array("Animals/Ecological", $tags)) selected @endif>Animals/Ecological</option>
                                <option value="Foodie" @if(in_array("Foodie", $tags)) selected @endif>Foodie</option>
                                <option value="Relaxing" @if(in_array("Relaxing", $tags)) selected @endif>Relaxing</option>
                                <option value="Water Fun" @if(in_array("Water Fun", $tags)) selected @endif>Water Fun</option>
                                <option value="Artsy" @if(in_array("Artsy", $tags)) selected @endif>Artsy</option>
                                <option value="Backpacker" @if(in_array("Backpacker", $tags)) selected @endif>Backpacker</option>
                                <option value="For Couples" @if(in_array("For Couples", $tags)) selected @endif>For Couples</option>
                                <option value="For Friends" @if(in_array("For Friends", $tags)) selected @endif>For Friends</option>
                                <option value="Quick Getaway" @if(in_array("Quick Getaway", $tags)) selected @endif>Quick Getaway</option>
                                <option value="Multiple Cities" @if(in_array("Multiple Cities", $tags)) selected @endif>Multiple Cities</option>
                                <option value="Holiday" @if(in_array("Holiday", $tags)) selected @endif>Holiday</option>
                                <option value="Girls Trip" @if(in_array("Girls Trip", $tags)) selected @endif>Girls Trip</option>
                                <option value="Guys Trip" @if(in_array("Guys Trip", $tags)) selected @endif>Guys Trip</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <button type="button" class="btn btn-success submitDetailsForm">UPDATE DETAILS</button>
                        </div>
                </div>
                <div class="tab-pane" id="gallery">
                    @if($isTravel)
                    <div class="form-group">
                        <label class="form-label">Cover Image</label>
                        <div id="imageBox" class="row" style="padding:20px;"></div>
                    </div>
                    @else
                    <div class="form-group">
                        <label class="form-label">Background Color</label>
                        <br><br>
                        <input type="color" name="color" value="{{ $group['color'] ?? '#000000' }}">
                    </div>
                    @endif
                    <div class="form-group">
                        <button type="button" class="btn btn-success submitDetailsForm">UPDATE DETAILS</button>
                    </div>
                </div>
                <div class="tab-pane" id="group">
                    <div class="form-group">
                        <label class="form-label">Invite</label>
                        <div class="button-group">
                            <input type="email" name="userInvite" id="userInvite" class="form-control" placeholder="Email or Username">
                            <span class="btn btn-primary" id="sendInvite"><i class="fa fa-envelope"></i></span>
                        </div>
                    </div>
                    <p class="text-bold">Group Members</p>
                    @include('groups.components.members-table')
                </div>
                <div class="tab-pane" id="admin">
                    <div class="form-group">
                        @if($group["archived"] == 0)
                            <p>Archiving a trip will make it invisible to non-group members.</p>
                            <a href="{{ route('group-archive', ['tripID' => $group['tripID']]) }}" class="btn btn-warning">ARCHIVE</a>
                        @elseif($group["archived"] == 1)
                            <p>Restore this trip to its former glory.</p>
                            <a href="{{ route('group-republish', ['tripID' => $group['tripID']]) }}" class="btn btn-black">REPUBLISH</a>
                        @endif
                    </div>
                </div>
                
            </div>
            </form>
        </div>
    </div>
</div>