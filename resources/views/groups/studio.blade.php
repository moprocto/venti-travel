@extends('layouts.curator-master')

@section('css')
	<link href="/assets/css/chat.css" rel="stylesheet" />
@endsection

@section('content')
	<div id="page-content">
		<div class="gradient-wrap">
			<div class="black position-relative hero-section mb-4" style="margin: 2em 4em;">
			   	<div class="row">
			   		<div class="col-md-8">
			   			<div class="card">
			   				<div class="card-body" style="min-height:600px; padding:5em;">
			   					<h2 style="width:100%; text-align:center;"><a style="color:#074297" href="/group/{{ $group['tripID'] }}">{{ $group["title"] }}: Trip Plan</a></h2>

			   					<p style="width:100%; text-align:left; margin-top:50px;">{!! displayTextWithLinks($group["description"]) !!}</p>

								<ul style="list-style:none;padding:0;margin: 10px 0;display: block; text-align:left;">
									<li>{{ getTripLocation($group["destination"], $group["country"]) }}</li>
									<li></span>{{ Date('M d, Y',strtotime($group["departure"])) }} ➜ {{ Date('M d, Y',strtotime($group["checkOut"])) }}</li>
								</ul>
								<br><br>
			   					<h2>Who's Going</h2>
			   					<div class="row members">
				   					@foreach($members as $member)
				   					@if($member["role"] != "invited" && $member["role"] != "requested")
				   						<div class="col col-md-3 col-xs-6">
				   							
				   								@include('users.components.profile-widget', ["user" => $member["member"], "pdf" => false])
				   							
				   						</div>
				   						@endif
				   					@endforeach
			   					</div>
			   					<br><br>
			   					<h2>The Plan</h2>
			   					<div id="plan">
			   						{!! $group["itinerary"] !!}
			   					</div>
			   				</div>
			   			</div>
			   		</div>
			   		<div class="col-md-4">
			   					<a href="{{ route('studio-pdf', ['tripID' => $group['tripID']]) }}" target="_blank" class="btn btn-black" style="margin: 0 auto; width:250px; padding: 10px 20px; display: block;"><i class="fa fa-download"></i> DOWNLOAD PDF</a>
			   			</div>
			   		</div>
			   	</div>
			</div>
		</div>
	</div>
	@include('groups.components.chat')
@endsection

@section('js')
@include('groups.components.chatJS')
<script>
	jQuery(document).ready(function ($){
		$.ajaxSetup({
	      headers: {
	        'X-CSRF-TOKEN': '{{ csrf_token() }}'
	      }
	    });

	    $("body").click(function(e){
        if(e.target.classList.contains("chatOpener")){
          if($("#chatBubble").hasClass("opener")){
            $("#chatBubble").addClass("opened");
            scrollChatList();
          }
        }
        if(e.target.classList.contains("chatCloser")){
          if($("#chatBubble").hasClass('opened')) {
            $("#chatBubble").removeClass("opened");
          }
        }
      });

  function scrollChatList(){
        setTimeout(function(){
          var height = 0;

        $(".chat-body ul li").each(function(){
            height += $(this).height() * 2;
        });
          $(".chat-body ul").animate({ scrollTop: height }, "fast");
        }, 1000);
  }

	    var planRefresh = setInterval(loadPlan, 5000);

		function loadPlan(){

	    $.ajax({
	        url: "{{ route('studio-refresh', ['tripID' => $group['tripID']]) }}",
	        type: 'post',
	        processData: true,
	            dataType: 'json',
	            data:{
	        },
	        success: function (data) {
	          if(data != "500"){
	          	$("#plan").html(data);
	          }
	        }
	    });

	  }
	});
</script>
@endsection