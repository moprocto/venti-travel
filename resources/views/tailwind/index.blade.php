@extends('layouts.curator-master')

@php
    $selectable = false;
@endphp

@section('css')
    <style type="text/css">
        html{
            overflow-x: hidden;
        }
        @media(max-width: 1368px ){
            .tb-full-width{
                width: 100%;
            }
        }
        .hamburger-menu-list li a{
            color: white;
        }
        .mobile-nav-open .hamburger-menu-list li a{
            color: black;
        }
        .btn-white{
            color: black !important;
        }
        .mobile-nav-open #backdrop{
            margin-top: 0 !important;
        }
        .about-body{
            justify-content: center;
        }
        @media(min-width: 1367px){
            {
                margin-left: 22px;
            }
            .row.about-body{
                margin-bottom: 22px;
            }
            .desktop-hide{
                display: none;
            }
        }
        @media(max-width: 800px){
            .mb-pd-20{
                padding-bottom: 20px;
            }
            .gradient-wrap{
                margin-top: 100px;
            }
            .sm-text-center{
                text-align: center !important;
            }
        }
        @media(max-width: 1368px){
            .mobile-block{
                display: block !important;
            }
            .mobile-hide{
                display: none;
            }
            #about .about-body .about-body-item .about-body-icon-cnt{
                justify-content: left !important;
                margin-bottom: 10px;
            }
            .pills-item{
                background-position-y: inherit !important;
            }
            .sign-up-box{
                padding-top: 80px;
                padding-bottom:  0;
            }
        }
        .about-body-icon-cnt{
            border: 1px solid black;
            border-radius: 20px;
            padding: 5px 10px;
        }
        

        .font-light{
            font-weight: 300 !important;
        }
        .full-width{
            max-width: 9999px !important;
            padding-left:  0;
            padding-right: 0;
        }
        #about{
            padding-top: 0;
        }
        .pd-20{
            padding: 80px 40px;
        }
        i.text-small{
            font-size: 16px;
        }
        .about-body-icon-cnt, .about-body-text-head{
            padding-left: 14px;
        }
        .about-body-text-head{
            padding-right: 15px;
            font-weight: bold;
        }
        @media(min-width: 1367px){
            {
                margin-left: 22px;
            }
            .row.about-body{
                margin-bottom: 22px;
            }
            .desktop-hide{
                display: none;
            }
        }
        @media(max-width: 768px){
            .mobile-hide{
                display: none;
            }


        }
        @media(max-width: 468px){
            #minutes, #seconds{
                display: none;
            }
            .card-preview{
                margin-top: 50px;
                text-align: center !important;
            }
            .card-preview img{
                max-width: 350px;
            }
        }   
        @media(max-width: 1368px){
            .mh-500-sm{
                max-height: 500px;
            }
            .mobile-block{
                display: block !important;
            }
            .mobile-hide{
                display: none;
            }
            #about .about-body .about-body-item .about-body-icon-cnt{
                justify-content: left !important;
                margin-bottom: 10px;
            }
            .pills-item{
                background-position-y: inherit !important;
            }
            .sign-up-box{
                padding-top: 80px;
                padding-bottom:  0;
            }
            .preview-mh{
                width: 143% !important;
                margin-left: -73px;
            }
        }

        .text-gold{
            color: #9D8A7C !important;
        }

        .text-large{
            font-size: 20px;
        }

        ol.pills-list{
            display: block;
            padding: 0 0 0 26px;
            list-style: none;
            overflow: hidden;
        }

        .pills-list li{
            display: flex;
            justify-content: space-between;
            align-items: center;
            margin-bottom: 10px;
            padding: 10px 20px 5px 20px;
        }
        .sand-badge{
            background-color: rgba(157, 138, 124, 0.7);
            opacity: 0.8;
            color: white;
            font-size: 22px;
            border-radius: 50%;
            width: 42px;
            height: 40px;
            letter-spacing: 1;
        }
        .pills-item{
            padding: 7px 20px;
            border-radius: 10px;
            margin-bottom: 20px;
            background-color: whitesmoke;
        }
        .pills-item div{
            padding-top: 10px;
        }
        .pills-item p{
            padding-top: 3px;
        }
        .pills-item-1{
            background-position-y: 0;
        }

        .pills-item-2{
            background-position-y: -90px;
        }
        .pills-item-3{
            background-position-y: -180px;
        }
        .pills-item-4{
            background-position-y: -220px;
        }
        li.pills-item-3{
            background: #d7d0ca;
        }
        #mc_embed_signup .text-small{
            font-size: 12px !important;
        }

        #subscribe .text-small{
            margin-top: 15px;
            
        }
        .about-body-item{
            margin-bottom: 10px;
        }.slider-horizontal{
            width: 100% !important;
        }
        #faqs
        {
        padding-bottom: 100px;
        }
        #faqs .faqs-container .faq
        {
            border-top: 1px solid var(--color-border);
            padding: 15px;
        }
        #faqs .faqs-container .faq .faq-head
        {
            padding: 15px 0px;
            position: relative;
        }
        #faqs .faqs-container .faq .faq-head h5
        {
            width: 90%;
            cursor: pointer;
        }
        #faqs .faqs-container .faq .faq-head h5::before
        {
            content: 'keyboard_arrow_down';
            text-align: center;
            font-size: 20px;
            line-height: 27px;
            font-family: 'Material Icons Round';
            height: 25px;
            width: 25px;
            color: #fefefe;
            background: var(--color-sec);
            position: absolute;
            top: 50%;
            right: 0;
            transform: translateY(-50%);
            border-radius: 50%;
            transition: 0.5s;
        }
        #faqs .faqs-container .faq .faq-head h5[aria-expanded=true]::before
        {
            content: 'keyboard_arrow_up';
        }
        .form-container .emailoctopus-form-wrapper .emailoctopus-success-message{
            color:  orange !important;
            font-size: 18px;
            padding-bottom: 20px;
        }
        .accordion-item{
            margin-bottom: 10px;
        }
        #countdown{
            position: fixed;
            z-index: 999;
            height: 50px;
            background: white;
            width: 100%;
            bottom: 0;
            left: 0;
            display: flex;
            flex-direction: row;
            justify-content: center;
            padding: 10px 20px;
            font-weight: bold;
            font-size: 0.9em;
            align-items: center;
        }
        #countdown div .description{
            text-align: center;
            font-size: 14px;
            text-transform: uppercase;
            font-weight: bold;
        }
        #timer{
            margin-top: -2px;
            font-weight: bold;
            display: flex;
            justify-content: space-between;
        }
        #timer{
            align-items: center;
        }
        #timer *{
            padding-right: 4px;
        }

        .about-body-icon-cnt{
            border: 1px solid rgba(0,0,0,0.1);
        }
        .footnote{
            width: 30px;
            display: inline-block;
        }
        html body{
            background: #F2F2F6 !important;
        }
        .features ul {
            list-style: none;
            padding: 0;
        }
        .features ul li{
            padding: 10px;
            border-bottom: 1px solid whitesmoke;
            font-size: 13px;
        }
        .text-bold{
            font-weight: bold;
        }
        .pills-item-1{
            background-color: rgba(190, 223, 230, 1) !important;
        }
        .pills-item-2{
            background-color: rgba(190, 223, 230, 0.6) !important;
        }
        .pills-item-3{
            background-color: rgba(190, 223, 230, 0.4) !important;
        }
        .pills-item-4{
            background-color: rgba(190, 223, 230, 0.2) !important;
        }
        .slide.logos{
            display: flex;
            flex-direction: row;
            justify-content: center;
            align-items: center;
        }
        .slide.logos img{
            width: 100px;
        }

        @keyframes slide {
          from {
            transform: translateX(0);
          }
          to {
            transform: translateX(-100%);
          }
        }

        .logos {
          overflow: hidden;
          padding: 60px 0;
          background: white;
          white-space: nowrap;
          position: relative;
        }

        .logos:before, .logos:after {
          position: absolute;
          top: 0;
          width: 250px;
          height: 100%;
          content: "";
          z-index: 2;
        }

        .logos:before {
          left: 0;
          background: linear-gradient(to left, rgba(255, 255, 255, 0), white);
        }

        .logos:after {
          right: 0;
          background: linear-gradient(to right, rgba(255, 255, 255, 0), white);
        }

        .logos:hover .logos-slide {
          animation-play-state: paused;
        }

        .logos-slide {
          display: inline-block;
          animation: 38s slide infinite linear;
        }

        .logos-slide img {
          width: 90px;
          height: 90px;
          margin: 0 40px;
        }
    </style>
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/11.0.2/css/bootstrap-slider.min.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons+Round" rel="stylesheet">
    <!-- Main CSS -->
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>

@endsection

@php
    $vips = env('VIPS');
    $vip = number_format($vips, 0);
    $counter = $vips * 20;
@endphp

@section('content')
<div id="backdrop" style="margin-top: -130px; width: 100%; z-index: 1; display: flex; align-items: center; min-height: 800px; background-position: center;  background-size: cover; margin-bottom: -64px; background-image:url('/assets/img/navigator-backdrop-t9.jpg')">
        <div class="gradient-wrap" style="background: transparent; width:100%;">
            <!--  HERO -->
            <div class="section-wrapper black position-relative">
                <div class="row" style="align-items:center;">
                    <div class="col-lg-5 col-md-12 col-sm-12 col-12 top-header" style="display: flex; align-items:center;">
                        <div id="hero-text-wrapper" style="max-width:900px;">
                            <h1 class="mb-3 section-title" style="line-height: 3.5rem; color: white; font-weight:600 !important; font-size: 1.8rem; margin-top:100px">
                            The Best Travel 
                            <span style="font-size: 2.45em; display:block;">Rewards Card</span>
                            </h1>
                            <h2 class="section-subtitle mb-4" style="color: white; max-width:900px;">
                            Free flights, hotels, and more. Earn travel every time you spend, make, and save money. Up to 5x rewards on all purchases<sup>*</sup></h2>
                         <p class="section-head-body" style="color:white;"><strong>Receive a free domestic flight after opening your account<sup>**</sup></strong></p>
                         <a href="#waitlist" class="btn btn-primary">Become a Founder</a>
                        </div>
                    </div>
                    <div class="col-lg-7 card-preview" style="text-align: center;">
                        <img src="/assets/img/card-explorer.png" style="width:400px; opacity: 0.95">
                    </div>
                </div>
            </div>
        </div>
    </div>
<!-- Cover End -->
 <!-- About Start -->

    <section id="" style="padding-top: 50px; padding-bottom: 100px; background-color:white">
        <div class="container">
            <div class="row">
                <div class="col-md-12  text-center pd-20 text-center" style="padding-bottom: 0; margin-bottom: -3px; padding:40px 20px;" >
                    <h2 class="section-head-title text-dark mb-3 section-title" style="margin:40px; margin-top:20px">Earn Free Travel When You</h2>
                </div>
                <div class="col-md-4 text-right sm-text-center">
                    <div class="row">
                        <div class="col-md-12 mb-2">
                            <h4>Get Paid</h4>
                            <p>Paycheck, Cash App, Stipend</p>
                        </div>
                        <div class="col-md-12 mb-2">
                            <h4>Live a Little</h4>
                            <p>Concert tickets, Uber, AirBnb</p>
                        </div>
                        <div class="col-md-12">
                            <h4>Spend Responsibly</h4>
                            <p>Not spending more than you have</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 text-center">
                    <img src="/assets/img/card-founder.png" style="width: 300px; margin-bottom: 20px">
                    <h5>Founders Card</h5>
                    <p style="font-size:12px;">Includes silhouette of Mt. Fuji in Japan. Limited to VIPs.</p>
                </div>
                <div class="col-md-4 sm-text-center">
                    <div class="row">
                        <div class="col-md-12 mb-2">
                            <h4>Pay a Bill</h4>
                            <p>Netflix, Rent, Utilities, etc.</p>
                        </div>
                        <div class="col-md-12 mb-2">
                            <h4>Invite Friends</h4>
                            <p>Earn for each referral</p>
                        </div>
                        <div class="col-md-12">
                            <h4>Save for the Future</h4>
                            <p>With our <a href="/boardingpass">9% APY<sup>***</sup> travel savings account</a></p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12  text-center pd-20 text-center" style="padding-bottom: 0; margin-bottom: -3px; padding:40px 20px;" >
                    <h3 class="section-head-title text-dark mb-3" style="margin:40px; margin-top:20px">Earn Up to 5X Rewards</h3>
                    <h5>Shopping with your favorite brands.</h5>
                    <p>Send us your <a href="/contact" target="_blank">recommendations</a></p>
                </div>
                <div class="col-md-12  text-center pd-20 text-center" style="padding-bottom: 0; margin-bottom: -3px; padding:0px 20px;">
                    <div class="logos">
                      <div class="logos-slide">
                        <img src="/assets/img/logos/target.png" />
                        <img src="/assets/img/logos/711.png" />
                        <img src="/assets/img/logos/amazon.png" />
                        <img src="/assets/img/logos/uber.png" />
                        <img src="/assets/img/logos/dutyfree.png" />
                        <img src="/assets/img/logos/airbnb.png" />
                        <img src="/assets/img/logos/lyft.png" />
                        <img src="/assets/img/logos/chipotle.png" />
                        <img src="/assets/img/logos/shein.png" />
                        <img src="/assets/img/logos/netflix.png" />
                        <img src="/assets/img/logos/sephora.png" />
                        <img src="/assets/img/logos/amc.png" />
                      </div>

                      <div class="logos-slide" style="margin-left:100px">
                        <img src="/assets/img/logos/target.png" />
                        <img src="/assets/img/logos/711.png" />
                        <img src="/assets/img/logos/amazon.png" />
                        <img src="/assets/img/logos/uber.png" />
                        <img src="/assets/img/logos/ticketmaster.png" />
                        <img src="/assets/img/logos/dutyfree.png" />
                        <img src="/assets/img/logos/airbnb.png" />
                        <img src="/assets/img/logos/lyft.png" />
                        <img src="/assets/img/logos/chipotle.png" />
                        <img src="/assets/img/logos/shein.png" />
                        <img src="/assets/img/logos/netflix.png" />
                        <img src="/assets/img/logos/sephora.png" />
                        <img src="/assets/img/logos/amc.png" />
                      </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section>
        <div class="container full-width">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 full-width">
                    <img src="/assets/img/travel-calc.jpg" style="width: 100%;" class="">
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 full-width">
                    <div class="row" style="padding: 0 25px;">
                        <div class="col-md-12  text-center pd-20 text-center" style="padding-bottom: 0; margin-bottom: -3px; padding:40px 20px;" >
                            <h2 class="section-head-title text-dark mb-3 section-title" style="margin:40px; margin-top:20px">How Much Free Travel?</h2>
                            <h5>Use our calculator to determine how many free trips you'll earn each year<sup>****</sup></h5>
                        </div>
                        <div class="col-md-12 text-center" style="justify-content:center;">
                            <div class="form-group">
                                <label class="form-label text-bold">How much are you depositing each month (after taxes)?<br><span style="font-weight: 300;">To maximize travel rewards, you'll deposit your paychecks, side hustle money, and internship stipends into Venti.</span></label>
                                <select id="income" class="form-control" style="max-width: 300px; margin:0 auto;">
                                    <option value="1000">$0 - $1,000</option>
                                    <option value="2000">$1,000 - $2,000</option>
                                    <option value="3000">$2,000 - $3,000</option>
                                    <option value="5000">$3,000 - $5,000</option>
                                    <option value="8000">$5,000 - $8,000</option>
                                    <option value="12000">$8,000 - $12,000</option>
                                    <option value="20000">$12,000 - $20,000</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12 text-center" style="justify-content:center;">
                            <div class="form-group">
                                <label class="form-label text-bold">How much are you spending on rent/mortgage each month?<br><span style="font-weight: 300;">You'll pay using your virtual checking account on Venti</span></label>
                                <select id="rent" class="form-control" style="max-width: 300px; margin:0 auto;">
                                    <option value="0">$0 (I'm Lucky)</option>
                                    <option value="1000">$1,000</option>
                                    <option value="1400">$1,400</option>
                                    <option value="1800">$1,800</option>
                                    <option value="2000">$2,000</option>
                                    <option value="2500">$2,500</option>
                                    <option value="3000">$3,000</option>
                                    <option value="4000">$4,000</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12 text-center" style="justify-content:center;">
                            <div class="form-group">
                                <label class="form-label text-bold">How much are you spending on everything else?<br><span style="font-weight: 300;">Student loans, shopping, Uber rides, Netflix, Starbucks, etc.</span></label>
                                <select id="spending" class="form-control" style="max-width: 300px; margin:0 auto;">
                                    <option value="1000">$500</option>
                                    <option value="1000">$1,000</option>
                                    <option value="1500">$1,500</option>
                                    <option value="2000">$2,000</option>
                                    <option value="2500">$2,500</option>
                                    <option value="3000">$3,000</option>
                                    <option value="4000">$4,000</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12 text-center" style="justify-content:center;">
                            <button class="btn btn-success" type="button" id="calcFreeTravel"><i class="fa fa-sync fa-spin"></i> Calculate My Free Travel</button>
                        </div>
                        <div class="col-md-12 text-center" id="results"></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="about" style="padding-bottom: 0;">
        <div class="container full-width">
            <div class="row" style="background-color: #f7f9ff;">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 text-center pd-20" style="padding-bottom: 0; margin-bottom: -3px; padding:40px 80px;" >
                    <h2 class="section-head-title text-dark mb-3 section-title" style="margin:40px; margin-top:20px">Ready for the World</h2>
                    <div class="row text-dark text-center " style="justify-content:center;">
                        <div class="col-md-6 ">
                            <div class="pills-item pills-item-1">
                                <img src="/assets/img/icons/24-service.png" style="width:100%; max-width:50px; margin:20px auto;">
                                <h5>24/7 Protection</h5>
                                <p>Flag any transaction and get credited instantly. We fight the bad guys so you can focus on enjoying life.</p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="pills-item pills-item-2">
                                <img src="/assets/img/icons/smart-luggage.png" style="width:100%; max-width:50px; margin:20px auto;">
                                <h5>Travel Smart</h5>
                                <p>No more blocked transactions when traveling internationally. Free currency conversion for small-dollar purchases.</p>
                            </div>
                        </div>
                    </div>
                    <div class="row text-dark text-center " style="justify-content:center;">
                        <div class="col-md-6 ">
                            <div class="pills-item pills-item-3">
                                <img src="/assets/img/icons/mobile-banking.png" style="width:100%; max-width:50px; margin:20px auto;">
                                <h5>Digitally Managed</h5>
                                <p>Easily track every dollar spent or recevied from our convenient mobile app.</p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="pills-item pills-item-4">
                                <img src="/assets/img/icons/compliant.png" style="width:100%; max-width:50px; margin:20px auto;">
                                <h5>Peace of Mind</h5>
                                <p>No monthly, annual, or "gotcha" fees. Federal deposit insurance up to $250,000. </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 full-width">
                    <img src="/assets/img/freedom.jpg" style="width: 100%;" class="">
                </div>
            </div>
        </div>
    </section>
    <!-- About End -->

    <!-- End Preview -->

    <!-- Start FAQs -->
    @include('tailwind.faq')
    <section id="" style="padding-bottom:50; background-color:white;">
        <div class="container full-width">
            <div class="row" id="waitlist">
                <div class="col-md-12 text-center" style="padding:90px 20px; padding-bottom:0;">
                    <h3>More Than a Card</h3>
                    <h2 class="section-head-title text-dark mb-3 section-title" style="margin:40px; margin-top:20px">It's a Community</h2>
                    <p style="max-width:800px; margin:0 auto; font-size:1.2em">Venti is your gateway to exclusive experiences, connections, and giveaways. Join our Founder List to get maximum rewards, our limited-edition card, and a free domestic flight when you open your account.</p>
                    <br>
                    <div>
                        <input type="email" class="form-control d-block" placeholder="someone@somewhere.com" id="waitlistEmail" style="width: 300px; margin:0 auto;">
                        <br>
                        <button id="joinWaitlist" class="btn btn-primary"><i class="fa fa-sync fa-spin"></i> Join Founder List</button>
                    </div>
                    <br>
                </div>
            </div>
        </div>
    </section>
    <!-- End FAQs -->
    <div id="countdown">
        <div id="timer">
            <div id="days"></div>
            <div id="hours"></div>
            <div id="minutes"></div>
            <div id="seconds"></div>
            <div class="description"><span><a href="#waitlist">To Become a Founder</a></span></div>
        </div>
    </div>

    <section><div class="container full-width" style="background-image:url('/assets/img/boarding-pass-bg5.jpg'); background-position: center; background-size:cover; padding-bottom:100px; padding-top:100px"><div class="row"><div class="col-md-12 text-center pd-20 full-width" style="margin-bottom: -3px; padding:40px; justify-content: center; padding-bottom: 0;"><h4 class="text-light mb-3">Travelers are earning 9% APY right now<br><span class="text-light pt-4 d-block" style="font-size: 1.5em;">With our high-yield savings account</span></h4><h2 class="section-head-title text-dark mb-3 section-title" style="font-size: 4rem; display: none;margin-top:20px; color:white !important">$<span class="" id="currency">10</span></h2><h5 class="section-head-body" style="color:white; max-width: 800px; justify-content:center; margin: 0 auto;">Join our fast-growing community of over travel savers</h5></div></div><div class="row" style="justify-content: center;"><a href="/boardingpass" class="btn btn-primary mt-4 mb-4">Learn More</a></div><br></div></section>
    
    <section>
        <div class="container pt-4">
            <div class="row">
                <div class="col-md-12">
                    <p><sup>*</sup> Rewarded in the form of <a href="/about/points">travel points</a>.</p>
                    <p><sup>**</sup> Domestic flight must have its origin and destination in the contiguous U.S. Max $200 credit in the form of Points after qualifying activities.</p>
                    <p><sup>***</sup> Annual Percentage Yield (APY) is rewarded in the form of travel points.</p>
                    <p><sup>****</sup> Calculated values assume Venti is the primary technology for all customer deposits, withdrawals, and expenditures and is for illustrative purposes only.<br>
                </div>
            </div>
        </div>
    </section>
@endsection


    
@section("js")
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/typeahead.js/0.10.4/typeahead.bundle.min.js"></script>
    @if(env('APP_ENV') != "local")
        <!-- Meta Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s)
            {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};
            if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
            n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t,s)}(window, document,'script',
            'https://connect.facebook.net/en_US/fbevents.js');
            fbq('init', '1169331916805166');
            fbq('track', 'PageView');
        </script>
        <noscript><img height="1" width="1" style="display:none"
        src="https://www.facebook.com/tr?id=1169331916805166&ev=PageView&noscript=1"
        /></noscript>
        <!-- End Meta Pixel Code -->
    @endif
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/11.0.2/bootstrap-slider.min.js"></script>
    <script>
        $(document).ready(function() {
            $.ajaxSetup({
                 headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                 }
              });

            var interactable = false; 

             
            /**
             * Format the number, so it will be seperated by comma
             */
            function commaSeparateNumber(val){
                while (/(\d+)(\d{3})/.test(val.toString())){
                  val = val.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
                }
                return val;
            }

            toggleLogo("black","white");

            function toggleLogo(color, replaced){
                var logoSRC = $(".logo").attr("src");
                logoSRC = logoSRC.replace(color, replaced);
                console.log(logoSRC)
                $(".logo").attr("src", logoSRC);
            }

            $.fn.isInViewport = function() {
              var elementTop = $(this).offset().top;
              var elementBottom = elementTop + $(this).outerHeight();

              var viewportTop = $(window).scrollTop();
              var viewportBottom = viewportTop + $(window).height();

              return elementBottom > viewportTop && elementTop < viewportBottom;
            };

            


            function isElementVisible($elementToBeChecked)
            {
                var TopView = $(window).scrollTop();
                var BotView = TopView + $(window).height();
                var TopElement = $elementToBeChecked.offset().top;
                var BotElement = TopElement + $elementToBeChecked.height();
                return ((BotElement <= BotView) && (TopElement >= TopView));
            }

            

            $("#joinWaitlist").click(function(){
                var btn = $(this);
                btn.addClass("disable-while-loading");
                var email = $("#waitlistEmail").val();
                

                if(email == "" || email == undefined){
                    $("#waitlistEmail").addClass("invalid-feedback");
                    btn.removeClass("disable-while-loading");
                    return swal({
                        icon: 'error',
                        title: 'Email is required'
                    });
                }

                

                $.ajax({
                    url: "{{ route('jetbridge-join-waitlist') }}",
                    processData: true,
                    dataType: 'json',
                    type: 'post',
                    data: {
                        email: email,
                        product: "tailwind",
                        income: parseInt($("#income").val()),
                        rent: parseInt($("#rent").val()),
                        spending: parseInt($("#spending").val())
                    },
                    success: function (data) {
                        if(data == 200){
                            btn.removeClass("disable-while-loading");
                            $("#waitlistEmail").val("");
                            swal({
                                icon: 'success',
                                title: "You're now VIP",
                                text: "We sent a confirmation email to " + email + " and outlined some next steps."
                            });
                        }
                        else {
                            btn.removeClass("disable-while-loading");
                            swal({
                                icon: 'warning',
                                title: "There was a problem",
                                text: "The email is either invalid or already on our VIP list. If you believe this was an error, please try again. Otherwise, please contact us at admin@venti.co."
                            });
                        }
                    },
                    error: function(data){
                        swal({
                            icon: 'warning',
                            title: "There was a problem",
                            text: "We could not establish a connection with our registration system. Please try disabling ad blockers or checking your internet subscriptions. If issues persist, please contact us at admin@venti.co."
                        });
                        btn.removeClass("disable-while-loading");
                    }
                });
            });

            $("#calcFreeTravel").click(function(){
                var btn = $(this);
                btn.addClass("disable-while-loading");
                var income = parseInt($("#income").val()) * 12 * .0011;
                var rent = parseInt($("#rent").val()) * 12 * .0022;
                var spending = parseInt($("#spending").val()) * 12 * .0033;

                var points = income + rent + spending;
                
                var numFlights = Math.round(points / 50);
                var numNights = Math.round(points / 30);
                
                

                $("#results").html("<h4 class='mt-4 mb-4'>Each Year You'll Earn</h4><div class='row mt-4' style='justify-content:center;'><div class='col-md-4'><div class='card'><div class='card-body'><h3><i class='fa fa-plane'></i></h3>Up to " + numFlights + " flight(s)</div></div></div><div class='col-md-4'><div class='card'><div class='card-body'><h3><i class='fa fa-hotel'></i></h3>Up to " + numNights + " hotel nights</div></div></div><div class='col-md-12 mt-4'><br>Accelerate your travel goals when you combine our rewards card <br> with our 9% APY <a href='/boardingpass'>travel savings account</a>.</div></div></p>");

                btn.removeClass("disable-while-loading");

            });


            $(".nav-icon").each(function(){
                var src = $(this).attr("src");
                console.log(src)
                src = src.replace('.png', '-white-boarding.png');
                $(this).attr("src", src);
              });

            $("#hamburger-menu").click(function(){
                if($("#page_home__show").hasClass("mobile-nav-open")){
                    toggleLogo("white","black");
                } else {
                    toggleLogo("black","white");
                }
                
            });
    


          

            @if(Session::get('subscribeError') !== null)
                swal({
                    icon: 'error',
                    title: 'Oops!',
                    text: 'We ran into a problem and could not add you to our list. Try again or send an email to markus@venti.co'
                })
            @endif

            makeTimer();

            function makeTimer() {

            var endTime = new Date("31 December 2024 23:59:49 GMT-04:00");          
            endTime = (Date.parse(endTime) / 1000);

            var now = new Date();
            now = (Date.parse(now) / 1000);

            var timeLeft = endTime - now;

            var days = Math.floor(timeLeft / 86400); 
            var hours = Math.floor((timeLeft - (days * 86400)) / 3600);
            var minutes = Math.floor((timeLeft - (days * 86400) - (hours * 3600 )) / 60);
            var seconds = Math.floor((timeLeft - (days * 86400) - (hours * 3600) - (minutes * 60)));
  
            if (hours < "10") { hours = "0" + hours; }
            if (minutes < "10") { minutes = "0" + minutes; }
            if (seconds < "10") { seconds = "0" + seconds; }

            $("#days").html(days + "<span> DAYS</span>");
            $("#hours").html(hours + "<span> HOURS</span>");
            $("#minutes").html(minutes + "<span> MINUTES</span>");
            $("#seconds").html(seconds + "<span style='padding-right:0'> SECONDS</span>");       

    }

    setInterval(function() { makeTimer(); }, 1000);

        });
    </script>

@endsection