<section id="faqs" style="padding-bottom:50">
    <div class="container">
        <div class="row">
            <div class="col-md-12  text-center pd-20 text-center" style="padding-bottom: 0; margin-bottom: -3px; padding:40px;" >
                <h2 class="section-head-title text-dark mb-3 section-title" style="margin:40px; margin-top:20px">FAQs</h2>
                <h5 style="font-weight: 800; font-size: 16px; text-transform:uppercase;">It's in our DNA to be as transparent as possible</h5>
            </div>
        </div>
        <div class="accordion accordion-flush" id="faqs" style="padding-bottom:0">
            <div class="accordion-item">
                <h2 class="accordion-header" id="flush-headingTwo">
                  <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseTwo" aria-expanded="false" aria-controls="flush-collapseTwo">
                    When will this product be available?
                  </button>
                </h2>
                <div id="flush-collapseTwo" class="accordion-collapse collapse" aria-labelledby="flush-headingTwo" data-bs-parent="#accordionFlushExample">
                  <div class="accordion-body"><p>Our goal is to start distributing cards before the end of this year. For now, you can create a <a href="/boardingpass">travel savings account</a>.</p></div>
                </div>
            </div>
            <div class="accordion-item">
                <h2 class="accordion-header" id="flush-headingFive">
                  <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseFive" aria-expanded="false" aria-controls="flush-collapseFive">
                    Where can I swipe my card to earn 5X rewards?
                  </button>
                </h2>
                <div id="flush-collapseFive" class="accordion-collapse collapse" aria-labelledby="flush-headingFive" data-bs-parent="#accordionFlushExample">
                  <div class="accordion-body"><p>The full 5X rewards will be limited to Founders who swipe their Gold-series debit card at select retailers, such as Target, SHEIN, and Ticketmaster. The full list of retailers and the corresponding rewards for each card type will be released in the coming months.</p></div>
                </div>
            </div>
            <!--
            <div class="accordion-item">
                <h2 class="accordion-header" id="flush-headingOne">
                  <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseOne" aria-expanded="false" aria-controls="flush-collapseOne">
                    Why is a cash reserve required?
                  </button>
                </h2>
                <div id="flush-collapseOne" class="accordion-collapse collapse" aria-labelledby="flush-headingOne" data-bs-parent="#accordionFlushExample">
                  <div class="accordion-body"><p>Lending money is risky, especially when there is no tangible or physical item that can be sold to pay back the loan in case of default. A cash reserve helps us lower the risk of Jet Bridge along with a lower APR compared to a traditional credit card. At this time, Jet Bridge credit lines cannot be secured using homes, cars, and other assets. Cash only.</p></div>
                </div>
            </div>
            <div class="accordion-item">
                <h2 class="accordion-header" id="flush-headingThree">
                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseThree" aria-expanded="false" aria-controls="flush-collapseThree">
                Will there be a hard check on my credit?</button>
            </h2>
            <div id="flush-collapseThree" class="accordion-collapse collapse" aria-labelledby="flush-headingThree" data-bs-parent="#accordionFlushExample">
              <div class="accordion-body"><p>A hard check on your credit will be required <strong>if you accept</strong> your pre-approval offer. We only perform a soft check to generate your initial offer, which does not lower your credit score.</p></div>
            </div>
            </div>
          <div class="accordion-item">
            <h2 class="accordion-header" id="flush-headingFour">
              <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseFour" aria-expanded="false" aria-controls="flush-collapseFour">
                Can I "cash advance" my credit line to my bank?
              </button>
            </h2>
            <div id="flush-collapseFour" class="accordion-collapse collapse" aria-labelledby="flush-headingFour" data-bs-parent="#accordionFlushExample">
              <div class="accordion-body"><p>No, Jet Bridge funds can only be used to complete purchases on Venti.</p></div>
            </div>
          </div>
          <div class="accordion-item">
            <h2 class="accordion-header" id="flush-headingSix">
              <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseSix" aria-expanded="false" aria-controls="flush-collapseSix">
                What are the fees for this program?
              </button>
            </h2>
            <div id="flush-collapseSix" class="accordion-collapse collapse" aria-labelledby="flush-headingSix" data-bs-parent="#accordionFlushExample">
              <div class="accordion-body"><p>Origination fees may vary from 0.5% to 2% of the approved amount. We are waiving origination fees for those that join our VIP list. The annual fee for this program is 1% of the credit line available. The fee is waived for Business and First Class <a href="/boardingpass">Boarding Pass</a> members.</p> 
                <p>Fees subject to change.</p>
              </div>
            </div>
          </div>
          -->
        </div>
    </div>
</section>