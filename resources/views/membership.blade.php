@extends('layouts.loan-master')

@section('css')
    <style type="text/css">
        @media(max-width: 1368px ){
            .tb-full-width{
                width: 100%;
            }
        }
    
    tr.highlight{
        background: #282e2e;
        font-weight: bold;
    }
    .modal-header, .modal-body, .modal-footer{
        background: #181818;
        border: none;
    }
    .modal .about-body-icon-cnt{
        margin: 0 auto;
        padding-right: 10px;
    }
    .modal-header button{
        color: white;
    }
    #tripModal  .modal-content{
        background-size: cover;
    }
    #tripModal .modal-header, #tripModal .modal-body, #tripModal .modal-footer{
        background-color:  rgba(14, 14, 14, 0.55);
    }
    </style>
@endsection

@section('hero')

<!-- Cover Start -->
<section id="cover">
    <div class="container">
        <div class="row">
            <div class="col-md-7 cover-image align-self-center">
                <img src="/assets/loan/img/venti-loan-hero-square2.jpg" class="img-fluid" alt="product">
            </div>
            <div class="col-md-5 align-self-center cover-body">
                <h1 id="apply">Never Pay Full Price Again</h1>
                <p></p>
                <h5 class="font-light">Our travel pass unlocks the best deals you simply can't get anywhere else.</h5>
                <p class="section-head-body">We're launching in 2022. You don't want to miss this.</p>
                @include('forms.waitlist')
            </div>
        </div>
    </div>
</section>
<!-- Cover End -->

@endsection

@section('content')

 <!-- About Start -->
    <section id="about" style="padding-bottom: 0;">
        <div class="container full-width">
            <div class="row">
                <div class="col-md-6 full-width d-md-none d-lg-flex upperTile" bg-image="/assets/loan/img/maldives-bg-square.jpg">
                    <img src="/assets/loan/img/maldives-bg-square.jpeg" style="width: 100%;">
                </div>
                <div class="col-md-6 full-width d-none d-md-flex d-lg-none upperTile" bg-image="/assets/loan/img/maldives-bg-square.jpg">
                    <img src="/assets/loan/img/maldives-bg-tall.jpg" style="width: 100%;">
                </div>
                <div class="col-md-6 text-center pd-20">
                    <h2 class="section-head-title">Massive Savings. Guaranteed.</h2>
                    <p class="section-head-body">Whether it's a honeymoon in the Maldives, new year celebrations in Thailand, or trekking the hills of coastal Greece, we save you up to 60% with our fully-customizable packages. Every venti trip includes heavily discounted: </p>
                    <ul style="font-size: 20px; list-style: none; line-height: 2; margin-left:-32px;">
                        <li>Airfare</li>
                        <li>Lodging</li>
                        <li>Transportation</li>
                        <li>Food & Drinks</li>
                        <li>Tours & Activities</li>
                        <li>Trip Insurance</li>
                    </ul>

                    <br>

                    <p>The only thing that should be on your mind is having the best time of your life</p>
<!--
                    <div class="row about-body">
                        <div class="col-lg-3 col-md-12 col-sm-6 about-body-item d-flex mobile-block align-items-center">
                            <div class="tb-full-width">
                                <div class="about-body-icon-cnt" data-icon="fa-plane">
                                    <i class="material-icons-round about-body-icon fas fa-plane text-small"></i>
                                    <span class="about-body-text-head">Airfare</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-12 about-body-item d-flex mobile-block align-items-center desktop-ml-20">
                            <div class="tb-full-width">
                                <div class="about-body-icon-cnt" data-icon="fa-ship">
                                    <i class="material-icons-round about-body-icon fas fa-ship text-small"></i>
                                    <span class="about-body-text-head">Cruises</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-12 about-body-item d-flex mobile-block align-items-center desktop-ml-20">
                            <div class="tb-full-width">
                                <div class="about-body-icon-cnt" data-icon="fa-hotel">
                                    <i class="material-icons-round about-body-icon fas fa-hotel text-small"></i>
                                    <span class="about-body-text-head">Lodging</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row about-body">
                        <div class="col-lg-3 col-md-12 about-body-item d-flex mobile-block align-items-center">
                            <div class="tb-full-width">
                                <div class="about-body-icon-cnt" data-icon="fa-car">
                                    <i class="material-icons-round about-body-icon fas fa-car text-small"></i>
                                    <span class="about-body-text-head">Rentals</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-12 about-body-item d-flex mobile-block align-items-center desktop-ml-20">
                            <div class="tb-full-width">
                                <div class="about-body-icon-cnt" data-icon="fa-taco">
                                    <i class="material-icons-round about-body-icon fas fa-taco text-small"></i>
                                    <span class="about-body-text-head">Food</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-12 about-body-item d-flex mobile-block align-items-center">
                            <div class="tb-full-width">
                                <div class="about-body-icon-cnt" data-icon="fa-cocktail">
                                    <i class="material-icons-round about-body-icon fas fa-cocktail text-small"></i>
                                    <span class="about-body-text-head">Drinks</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row about-body desktop-hide">
                        <div class="col-md-12 text-center">
                            <br>
                                <h5>Shopping, activities, tours, cash, and so much more!</h5>
                        </div>
                    </div>

                    <div class="row about-body mobile-hide">
                        <div class="col-lg-3 col-md-12 about-body-item d-flex mobile-block align-items-center">
                            <div class="tb-full-width">
                                <div class="about-body-icon-cnt">
                                    <i class="material-icons-round about-body-icon fas fa-shopping-bag text-small"></i>
                                    <span class="about-body-text-head">Shopping</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-12 about-body-item d-flex mobile-block align-items-center desktop-ml-20">
                            <div class="tb-full-width">
                                <div class="about-body-icon-cnt">
                                    <i class="material-icons-round about-body-icon fas fa-shuttle-van text-small"></i>
                                    <span class="about-body-text-head">Shuttles</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-12 about-body-item d-flex mobile-block align-items-center desktop-ml-20">
                            <div class="tb-full-width">
                                <div class="about-body-icon-cnt">
                                    <i class="material-icons-round about-body-icon fas fa-skiing"></i>
                                    <span class="about-body-text-head">Activities</span>
                                </div>
                            </div>
                        </div>
                         
                    </div>
                    <div class="row about-body mobile-hide">
                        <div class="col-lg-3 col-md-12 about-body-item d-flex mobile-block align-items-center">
                            <div class="tb-full-width">
                                <div class="about-body-icon-cnt">
                                    <i class="material-icons-round about-body-icon fas fa-binoculars text-small"></i>
                                    <span class="about-body-text-head">Tours</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-12 about-body-item d-flex mobile-block align-items-center">
                            <div class="tb-full-width">
                                <div class="about-body-icon-cnt">
                                    <i class="material-icons-round about-body-icon fas fa-money-bill text-small"></i>
                                    <span class="about-body-text-head">Cash</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-12 about-body-item d-flex mobile-block align-items-center">
                            <div class="tb-full-width">
                                <div class="about-body-icon-cnt">
                                    <i class="material-icons-round about-body-icon fas fa-stethoscope text-small"></i>
                                    <span class="about-body-text-head">Insurance</span>
                                </div>
                            </div>
                        </div>
                    </div>
                -->
                </div>
            </div>
            <div class="row" style="background-color: #f7f9ff;">
                <div class="col-md-12 col-lg-6 text-center pd-20" style="padding-bottom: 0; margin-bottom: -3px;" >
                    <div class="section-head-tag">The ultimate travel experience for everyone</div>
                    <h2 class="section-head-title text-dark" ><strike style="font-size: 18px; color:grey;">$999</strike><br>$399 / Year</h2>
                    <div class="section-head-tag text-dark">Special pricing for those that join our <a href="#apply">waitlist</a> for free!</div>
                    <div class="row text-dark text-left pills-item pills-item-1">
                        <div class="col-md-11">
                            <p class="text-dark text-large">Why you should join:</p>
                            <ul class="text-large" style="margin-left:-10px;">
                                <li>Receive one free plane ticket each year*</li>
                                <li>Save up to 60% on travel packages designed by venti or you</li>
                                <li>Alerts for flights and hotel savings of up to 90% off</li>
                                <li>Trip insurance</li>
                                <li>Lost baggage protection</li>
                                <li>100% cashback on TSA Pre-Check</li>
                                <li>100% cashback on in-flight Wi-Fi</li>
                                <li>50% cashback on airport lounges</li>
                                <li>50% cashback on concierge services</li>
                                <li>Extend benefits to friends and family</li>
                            </ul>
                            <p class="text-dark">* Free flight is only for the continental U.S. Members can recoup the cost of their membership by purchasing one international flight through our program. </p>
                        </div>
                    </div>
                    <!--
                    <div class="row text-dark text-left pills-item pills-item-3">
                        <div class="col-md-12">
                            <p class="text-dark text-large">Put that high-interest credit card away! Members that opt to use our exclusive payment plans get:</p>
                            <ul class="text-large" style="margin-left:-10px;">
                                <li>Low-interest financing with payment plans up to 72 months</li>
                                <li>Complimentary seat and room upgrades when available</li>
                            </ul>
                        </div>
                    </div>
                -->
                </div>
                <div class="col-md-6 full-width d-md-none d-lg-flex upperTile" bg-image="/assets/loan/img/morocco-bg-square.jpg">
                    <img src="/assets/loan/img/morocco-bg.jpg" style="width: 100%;">
                </div>
                <div class="col-md-12 full-width d-none d-md-flex d-lg-none upperTile">
                    <img src="/assets/loan/img/morocco-bg-wide.jpg" style="width: 100%;">
                </div>
            </div>
            <!--
            <div class="row" id="Waitlist">
                <div class="col-md-6 full-width mobile-hide">
                    <img src="/assets/loan/img/iceland-bg.jpg" style="width: 100%;">
                </div>
                <div class="col-md-12 col-lg-6 section-head text-center sign-up-box" style="align-self:center;">
                    <div class="section-head-tag">See The World</div>
                    <h2 class="section-head-title">Travel Without Limits</h2>
                    <img src="/assets/loan/img/venti-card-image.png" style="width: 100%; max-width:200px; margin:0 auto;">
                    <br><br>
                    <p class="section-head-body">We're launching in 2022. <br> Early birds may get 0% APR for life with all the perks.</p>
                    <div class="col-lg-12 subscribe-container text-center" id="subscribe">
                        <div class="clear" id="Waitlist"></div>
                        @include('forms.waitlist')
                    </div>
                </div>
            </div>
            -->
        </div>
    </section>
    <!-- About End -->

    <section id="products" style="padding-bottom:0; background-color:#1d1d1f; display:none;">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 offset-lg-3 col-md-8 offset-md-2 text-center">
                    <h2 class="section-head-title">How Much Savings?</h2>
                    <p class="section-head-body">Here's a breakdown of how much <strong class="text-light">two people</strong> could save if they traveled to a 5-star resort in Canc�n, Mexico for seven days:
                </div>
            </div>
            <div class="row">
                <div class="col-12" style="margin-top:30px;">
                    <table class="table text-white text-center" style="max-width:650px; margin:0 auto;">
                        <thead>
                            <tr class="highlight" style="border-top:none !important;">
                                <th></th>
                                <th>Member</th>
                                <th>Non-Member</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="text-left">TSA Pre-Check</td>
                                <td>$0</td>
                                <td>$85</td>
                            </tr>
                            <tr>
                                <td class="text-left">In-Flight Wi-Fi</td>
                                <td>$0</td>
                                <td>$20</td>
                            </tr>
                            <tr>
                                <td class="text-left">Trip Insurance</td>
                                <td>$0</td>
                                <td>$60+</td>
                            </tr>
                            <tr>
                                <td class="text-left">Airport Lounge</td>
                                <td>$25+</td>
                                <td>$50+</td>
                            </tr>
                            <tr>
                                <td class="text-left">Concierge</td>
                                <td>$25</td>
                                <td>$75</td>
                            </tr>
                            <tr>
                                <td class="text-left">Resort Fees</td>
                                <td>$3,250</td>
                                <td>$5,000</td>
                            </tr>
                            <tr>
                                <td class="text-left">Activities, Food,<br> Drinks, etc.</td>
                                <td>$2,000</td>
                                <td>$2,000</td>
                            </tr>
                            <tr>
                                <td class="text-left">Finance Charges*</td>
                                <td>$1,935</td>
                                <td>$3,850</td>
                            </tr>
                            <tr>
                                <td class="text-left">Total Cost</td>
                                <td>$7,235</td>
                                <td>$11,140</td>
                            </tr>
                            <tr class="highlight">
                                <td class="text-left">Savings</td>
                                <td>$3,905</td>
                                <td>$0</td>
                            </tr>
                            <tr>
                                <td class="text-left">Monthly Payment*</td>
                                <td>$145.24</td>
                                <td>$293.28</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="col-md-12">
                    <br>
                    <p class="text-small" style="font-size:11px;">* Finance charges vary and are based on the interest rate you qualify for from our network of lending partners. Generally, venti members are subject to a lower interest rate if they decide to finance their trip instead of using their credit card.</p>
                </div>
            </div>
        </div>
    </section>

    <section id="products" style="padding-bottom:30px;">
        <div class="container">
            <div class="row">
                <div class="col-md-8 offset-md-2 section-head text-center">
                    <h2 class="section-head-title">Travel Packages Launching Soon</h2>
                    <!--
                    <p>We worked with the best of the best to design these customizable travel packages to offer out-of-this-world discounts and experiences for our members. Don't see your destination? No worries, our concierge team can create something custom for you. Need a private jet? We'll arrange that, too!</p>
                -->
                    <p>Finely crafted travel packages designed to save you thousands. Join our waitlist to gain early access to these savings!</p>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="products-slider">
                        @include('layouts.destination-rotator')
                    </div>
                </div>
                
            </div>
        </div>
    </section>
    <!-- End Products -->

    <section style="background-position: center; background-image: url('/assets/loan/img/brazil-mountians.jpg'); background-size: cover; padding: 100px 20px;">
        <div class="container full-width">
            <div class="row">
                <div class="col-12 text-center">
                    <div class="section-head-tag">Savings. Perks. Exclusivity.</div>
                    <h2 class="section-head-title">Get in Early!</h2>
                    @include('forms.waitlist')
                </div>
            </div>

        </div>
    </section>

    <!-- Start FAQs -->
    
    @include('layouts.faq')

    <!-- End FAQs -->

    @include('forms.feedback')


    @include('components.modals')

@endsection

@section('js')
    <script type="text/javascript">
        $(document).ready(function() {
            $(".about-body-icon-cnt").click(function(){
                var tileText = $(this).find(".about-body-text-head").text();
                var tileIcon = $(this).attr("data-icon");
                console.log(tileText);
                $("#tilesModal").find("#tilesTitle").text(tileText);
                $("#tilesModal").find(".material-icons-round").attr("class", "material-icons-round about-body-icon fas " + tileIcon);
                
                if(tileText == "Airfare" || tileText == "Cruises" || tileText == "Lodging" || tileText == "Rentals"){
                    $("#tilesModal").find("#tilesText").text("venti members can save up to 25% or more in airfare, cruises, lodging, and vehicle rentals by purchasing through one our packages.");
                    $("#tilesModal").modal("show");
                }

                if(tileText == "Food" || tileText == "Drinks"){
                    $("#tilesModal").find("#tilesText").text("venti's all-inclusive travel packages generally include unlimited food and drinks.");
                    $("#tilesModal").modal("show");
                }

                
            });

            $(".owl-item").click(function(){
                var bgImage = $(this).find(".img-fluid").attr("bg-image");
                $("#tripModal").find(".modal-content").attr("style", "background-image: url('" + bgImage + "');")
                $("#tripModal").modal("show");

            });
            $(".upperTile").click(function(){
                var bgImage = $(this).attr("bg-image");
                $("#tripModal").find(".modal-content").attr("style", "background-image: url('" + bgImage + "');")
                $("#tripModal").modal("show");
            });

            $("#submit-feedback").click(function(){
                if($("#feedback-text").val() == ""){
                    alert("Please provide some feedback");
                } else {
                    $("#feedback-form").submit();
                    $("#feedback-text").val("");
                    alert("Thank you so much! :)");
                }
            });

            
        });
    </script>
@endsection
    
