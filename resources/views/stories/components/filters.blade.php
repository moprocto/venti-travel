<div class="accordion" id="accordionExample" style="width:100%; padding:0;">
  <div class="accordion-item" style="border-radius:20px;">
    <h2 class="accordion-header" id="headingOne" style="border-radius: 20px;">
      <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne" style="border-top-left-radius:20px; border-top-right-radius:20px;">FILTERS</button>
    </h2>
    <div id="collapseOne" class="accordion-collapse collapse" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
      <div class="accordion-body">
         <div class="form-group">
            <label class="form-label">BY CATEGORY</label>
            <select class="form-control filter" style="padding:5px 20px; height:35px;" id="category">
               <option name="" value="">SHOW ALL</option>
               <option value=".review">REVIEWS</option>
               <option value=".tip">TRAVEL TIPS</option>
               <option value=".highlight">TRIP HIGHLIGHTS</option>
               <option value=".deal">TRAVEL DEALS</option>
               <option value=".postcard">POSTCARDS</option>
            </select>
         </div>
         <div class="form-group">
            <label class="form-label">BY COUNTRY</label>
            <select class="form-control filter" id="countries">
               <option value="">SHOW ALL</option>
            </select>
         </div>
      </div>
    </div>
  </div>
</div>