<div class="grid-item {{ $story['category'] }} {{ str_replace(' ','',$story['country']) }} storyCard" data-country='{{$story["country"] }}'>
	<div class="card" style="border-radius:20px">
      <div class="card-header" style="background-position: center; background-image: url('{{ $story['mediaUrl'] }}')"></div>
      <img src="" style="width:100%;">
      <div class="card-body" style="border-bottom-left-radius:20px; border-bottom-right-radius:20px;">
         <div class="mb-4" style="display: flex; justify-content:space-between; align-items:center">
            <a href="/u/{{ $story['author']->customClaims['username'] }}" style="display: flex; align-items:center; color: black; width:50%;">
               <div style="position:relative;">
               <img src="{{ $story['author']->photoUrl }}" style="width:50px; border-radius:50%;">
               @if(($story["author"]->customClaims['nav'] ?? null) === true)
                  <img src="/assets/img/navigator-verified.png" style="width: 17px; position: absolute; top: -3px; left: -6px; z-index: 500;" title="Verified"/> 
               @endif
               </div>
               <div style="padding-left:10px; min-width:100px;">
                  <span style="display:block; font-weight: bold;">{{ getFirstName($story['author']->displayName) }}</span>
                  <span style="display:block; font-size: 12px;">{{ getTripDate($story["createdAt"]) }}</span>
               </div>
            </a>
            <div style="display:flex;" class="like-box" data-story='{{ $story["storyID"] }}'>
               <img src="@if($story['liked']) /assets/img/smile-liked.png @else /assets/img/smile.png @endif" style="width:25px; border-radius:0;">
               <span class="count" style="padding-left: 5px;">{{ sizeof((array)$story['likes']) }}</span>
            </div>
         </div>
         <h4 class="title">
            {{ $story["title"] }}
         </h4>
         <p style="font-size:0.9rem;">{{ $story["content"] }}</p>
         <p style="width:100%; text-align:center"><i class="fa fa-location-dot"></i> {{ $story["destination"] . ", " . $story["country" ]}}</p>
      </div>
   </div>
</div>