<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <div class="card-body text-center">
                            <a href="https://apps.apple.com/us/app/venti-friends-and-adventure/id1620282387" style="display:block"><img style="max-width:600px; width: 100%;" src="/assets/img/900.jpg"></a>
                            <h3>To share a postcard, download our mobile app!</h3>
                            <a href="https://apps.apple.com/us/app/venti-friends-and-adventure/id1620282387" class="btn btn-primary"><i class="fab fa-apple mr-1"></i> Download Venti</a>
                        </div>
        <form method="POST" action="{{ route('share-story') }}" style="display:none;">
          @CSRF
          <h2 class="text-center mt-4 mb-4">What's Your Story?</h2>
          <div class="form-group">
            <label class="form-label">TITLE</label>
            <input name="title" class="form-control" required="" placeholder="Example: The Mayan Ruins won't disappoint!">
          </div>
          <div class="form-group">
              <label class="form-label">SELECT CATEGORY</label>
              <select class="form-control" name="category" required="">
                <option value="">PLEASE SELECT</option>
                <option value="review">REVIEW</option>
                <option value="tip">TRAVEL TIP</option>
                <option value="highlight">TRIP HIGHLIGHT</option>
                <option value="deal">TRAVEL DEAL</option>
                <option value="postcard">POSTCARD</option>
              </select>
          </div>
          <div class="form-group">
            <label class="form-label">CONTENT</label>
            <textarea name="content" class="form-control" placeholder="Our group visited the Mayan Ruins near..."></textarea>
          </div>
          <div class="form-group">
            <label class="form-label">MEDIA TYPE</label>
            <select name="media" class="form-control" required="">
                <option value="photo">PHOTO</option>
                <option value="video">SHORT VIDEO</option>
            </select>
          </div>
          <div class="form-group">
            <label class="form-label">LOCATION</label>
            <input type="text" class="form-control" name="location" placeholder="Oaxaca, Mexico" required="">
          </div>
          <div class="form-group">
            <input type="submit" name="submit" class="btn btn-primary" value="SUBMIT">
            <p style="font-size:12px; margin-top:15px;">All submissions will be reviewed by our content team before posting. We will respond via email regarding your submission.</p>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">CANCEL</button>
      </div>
    </div>
  </div>
</div>