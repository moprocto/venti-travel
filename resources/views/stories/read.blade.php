@extends("layouts.curator-master")

@section('css')
	<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
	<link href="/assets/css/store.css" rel="stylesheet" />
	<link href="/assets/css/trip.css" rel="stylesheet" />
	<link href="/assets/css/chat.css" rel="stylesheet" />
	<style type="text/css">
		.image-background{
			width: 100%;
			min-height: 300px;
			border-top-left-radius: 20px;
			border-top-right-radius: 20px;
			background-size: cover;
			background-position: center;
		}
		#minimizeVideoFrame{
			display: none;
			cursor: pointer;
		}
	</style>
@endsection

@section('content')
<div id="page-content">
	<div class="gradient-wrap">
		<div class="black position-relative hero-section mb-4" style="margin: 2em 4em;">
		   	<div class="row">
		   		<div class="col-md-7">
			   			<div class="card mb-4" style="border-radius: 20px">
			   				<div class="image-background" style="background-image: url('{{ $story['mediaUrl'] }}')"></div>
			   				<div class="card-body">
			   						<table style="margin-bottom:20px; width:100%;">
			   							<tr>
			   									<td style="width:10%"><a href="/u/{{ $author->customClaims['username'] }}" target="_blank"><img src="{{ $author->photoUrl}}" style="width:50px; height:50px; border-radius:25px;"></a></td>
			   									<td style="width:80%; text-align:left;">
			   										<strong>{{ getFirstName($author->displayName) }} • {{ $story["createdAt"] }}</strong><br>
			   										<span>{{ $story["destination"] }}, {{ $story["country"] }}</span>
			   									</td>
			   									<td>{{ sizeof($story['likes'] ?? []) }} <i class="fa fa-heart"></i></td>
			   							</tr>
			   						</table>
			   						<div style="flex-direction: row; flex:1;">
			   								
			   								<div style="flex-direction:column;">
			   									
			   								</div>
			   						</div>
			   						<h1>{{ $story['title'] }}</h1>
			   						<p>{{ $story["content"] }}</p>
			   				</div>
			   			</div>
		   		</div>
		    </div>
		</div>
	</div>
</div>
@endsection

@section('js')
	<script src="{{ asset('js/app.js') }}" defer></script>
	<script src="https://cdn.tiny.cloud/1/f1cdeokivvyzyohrpb09kwayr4k53ovtgo1849tqerl354st/tinymce/6/tinymce.min.js" referrerpolicy="origin"></script>
	<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
@endsection