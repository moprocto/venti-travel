@extends("layouts.curator-master")

@section("css")
   <link href="/css/jRange/jrange.css" rel="stylesheet" />
   <link href="/css/lightbox/lightbox.min.css" rel="stylesheet" />
   <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
   <link href="/assets/css/store.css" rel="stylesheet" />
   <style type="text/css">
      @media screen and (max-width:  600px){
         .grid{
            margin-top: 25px;
         }
         .card-row{
         	margin: 0;
         }
         .grid .card{
         	min-width: 170px !important;
         	min-height: 170px !important;

         }
         .gutter-sizer { width: 4%; }
         .grid-sizer { width: 22%; }
      }
       @media screen and (min-width:  600px){
         .grid{
         
         }
         .gutter-sizer { width: 1%; }
         .grid-sizer { width: 50%; }
         .grid-item{
            width: 33%;
            padding: 70px 10px;
         }

      }
      .grid .card{
         border: none;
         box-shadow: 0 1px 4px rgb(0 0 0 / 15%);
      }
      .grid .card img{
         border-top-left-radius: 20px;
         border-top-right-radius: 20px;
      }
      .grid .card, .grid .card .card-body{
         border-radius: 20px;
      }
      h4.title{
         display: flex;
         justify-content: space-between;
         align-items: center;
         align-content: center;
      }
      .category{
         text-transform: uppercase;
         background: black;
         color: white;
         font-size: 12px;
         padding: 5px 10px;
         border-radius: 10px;
         margin-top: -3px;
      }
      .grid{
         
      }
      .form-group .form-label{
         text-transform: uppercase;
    font-size: 10px;
    color: black;
    font-weight: 900;
    margin-bottom: 20px;
      }
   </style>
@endsection

@section("content")
	<div id="page-content">
      	<div class="gradient-wrap">
         <!--  HERO -->
			<div class="black position-relative hero-section mb-4" style="margin: 2em 4em;">
				<h1>Postcards</h1>
				<div class="row">
					<div class="col-md-3">
                  <p>Where travelers from all over inspire, inform, and support each other</p>
                  @if(Session::get('user') !== null)
                        @if(!isProfileComplete(Session::get('user'))[0])
                           <h5>{!! isProfileComplete(Session::get('user'))[1] !!} to share a postcard</h5>
                        @else
                           <button href="mailto:admin@venti.co?subject={{ Session::get('user')->displayName }} wants to share a postcard on Venti" class="btn btn-primary btn-wide" data-bs-toggle="modal" data-bs-target="#exampleModal" style="border-radius: 10px;"><img src="/assets/img/share.png" style="width:18px; height: 18px; margin-top:-5px"> SHARE POSTCARD</button>
                           <p class="mb-4 mt-1" style="font-size:10px; width: 100%; text-align:center;">For now, admin approval is required for all submissions</p>
                        @endif

                     @else
                        <h5><a href="{{ route('login') }}">Log in</a> to share a postcard</h5>
                  @endif
                  <div class="row card-row" style="padding:0; display:none;">
                     @include('stories.components.filters')
                  </div>
					</div>
					<div class="col-md-9" class="pb-30" style="min-height: 1400px;">
						<div class="row grid">
   						@foreach($stories as $story)
                        @if($story["type"] == "image")
                           @include("stories.components.image-card", ["story" => $story])
                        @endif

                        @if($story["type"] == "video")
                           @include("stories.components.video-card",["story", $story])
                        @endif
						   @endforeach
						</div>
					</div>
				</div>
        	</div>
    	</div>
	</div>
   @include('stories.components.create-modal')
@endsection

@section('js')
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script src="/js/isotope/isotope.js"></script>
<script src="/assets/js/stories.js"></script>
<script>
   jQuery(document).ready(function ($) {
      $.ajaxSetup({
         headers: {
            'X-CSRF-TOKEN': '{{ csrf_token() }}'
         }
      });

      $(".like-box").click(function(){
        var storyID = $(this).attr("data-story");
        var liker = $(this);
        var likeCount = parseInt(liker.find(".count").text());
         @if(Session::get('user') !== null)
            $.ajax({
               url: "{{ route('like-story') }}",
               type: 'post',
               processData: true,
               dataType: 'json',
               data:{
                  storyID: storyID
               },
               success: function (data) {
                  if(data == "200"){
                     liker.find("img").attr("src", "/assets/img/smile-liked.png");
                     likeCount++;
                     liker.find(".count").text(likeCount);
                  } else{
                     liker.find("img").attr("src", "/assets/img/smile.png");
                     likeCount--;
                     liker.find(".count").text(likeCount);
                  }
               }
            });
         @else
            var myhtml = document.createElement("div");
            myhtml.innerHTML = '<p>You need to be signed in to like a post!</p><a href="/login" class="btn btn-primary">LOG IN</a>';
            swal({
                    icon: 'warning',
                    title: 'Oh, hello!',
                    content: myhtml
            })
         @endif
      });

      @if(Session::get('shareSuccess') !== null)
         swal({
                    icon: 'success',
                    title: 'Thank you!',
                    text: 'Your story has been sent to our team!',
            })
      @endif
      @if(Session::get('shareError') !== null)
         swal({
                    icon: 'error',
                    title: 'Uh oh!',
                    text: 'We hit a snag and was not able to send your story to our team :(',
            })
      @endif

   });
</script>
@endsection
