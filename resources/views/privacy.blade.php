@extends('layouts.curator-master')

@section('content')

<section id="faqs" style="margin-top: 4em;">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 offset-lg-3 col-md-8 offset-md-2 section-head text-center">
                <h2 class="section-head-title">Venti Terms of Use & Privacy Policy</h2>
                <p class="section-head-body">It's in our DNA to be as transparent as possible</p>
            </div>
        </div>
        <div class="card bg-white mb-3">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12 faqs-container">
                        <h3 class="mb-3 mt-3">
                            Terms of use
                        </h3>
                        <p>
                        Welcome to the venti Mobile App (the “App”) operated by Venti Financial, Inc. (“venti”). Please read these Terms of Use (“TOU”) and ask us any related questions before you use the App. This App and the information content provided herein are granted for use to you (“you” or “user”) expressly conditioned on the acceptance of the following terms and conditions. By using this App you agree to be bound by the terms, conditions and notices that follow without modification herein, under a grant of a limited, non-transferable license to use this App in accordance with these TOU. If you do not agree to these terms and conditions, you must not use this App.
                        </p>
                        <p>
                        Be sure to return to this page periodically to review the most current version of the TOU. We reserve the right at any time, at our sole discretion, to change or otherwise modify the TOU without prior notice, and your continued access or use of this App signifies your acceptance of the updated or modified TOU.
                        </p>
                        <p>
                        1. Description of App Services
                        Through the App, venti provides users with tools for travel and leisure discovery, planning and booking (the “Services”) accessible through user’s mobile device.
                        </p>
                        <p>
                        2. Eligibility
                        This App can only by used by residents of the United States who are over the age of 18 and have the requisite power and authority to enter into and perform the obligations under these TOU, and are not barred from receiving our Services under the laws of the United States or other applicable jurisdiction.
                        </p>
                        <p>
                        3. App Access
                        Your mobile device must be connected to the internet for the App to function correctly. You are responsible for making all arrangements necessary for your device to have internet connectivity and are responsible for all sums your service provider may charge you arising out of the App transmitting and receiving data (including but not limited to data roaming charges). As further described in our Privacy Policy, the App will automatically transfer a small amount of data as part of its normal operation, including data about how you use the App, which content you access, and technical errors or problems which the App may encounter while being used. By using the App, you acknowledge, agree and consent to the automatic collection of this information.
                        </p>
                        <p>
                        4. License
                        venti grants you a limited license to access and make personal use of the App and the Services. This license does not include any downloading or copying of account information for the benefit of another vendor or any other third party; caching, unauthorized hypertext links to the App and the framing of any content available through the App; uploading, posting, or transmitting any material that contains software viruses or any other computer code, files or programs designed to interrupt, destroy or limit the functionality of any computer software or hardware or telecommunications equipment; any action that imposes or may impose (in venti’s sole discretion) an unreasonable or disproportionately large load on venti’s infrastructure; or any use of data mining, robots, scrapers, or similar data gathering and extraction tools. You may not bypass any measures used by venti to prevent or restrict access to the App. Any unauthorized use by you shall terminate the permission or license granted to you by venti.
                        </p>
                        <p>
                        5. Prohibited Activities
                        By using the App you agree not to: (i) use this App or its contents for any commercial purpose; (ii) make any speculative, false, or fraudulent reservation or any reservation in anticipation of demand; (iii) access, monitor or copy any content or information of this App using any robot, spider, scraper or other automated means or any manual process for any purpose without our express written permission; (iv) violate the restrictions in any robot exclusion headers on this App or bypass or circumvent other measures employed to prevent or limit access to this App; (v) take any action that imposes, or may impose, in our discretion, an unreasonable or disproportionately large load on our infrastructure; (vi) deep-link to any portion of this App (including, without limitation, the purchase path for any travel services) for any purpose without our express written permission; or (vii) “frame”, “mirror” or otherwise incorporate any part of this App into any other App without our prior written authorization.
                        </p>
                        <p>
                        You may not copy (except as permitted by these TOU), reverse-engineer, disassemble, attempt to derive the source code of, modify, or create derivative works of the App, any updates, or any part thereof (except as and only to the extent that any foregoing restriction is prohibited by applicable law or to the extent as may be permitted by the licensing terms governing use of any open-sourced components included with the App).
                        </p>
                        <p>
                        You may not transfer, redistribute, or sublicense the App. You may not use or otherwise export, or re-export, the App except as authorized by United States law and the laws of the jurisdiction in which you obtained the App. In particular, but without limitations, the App may not be exported or re-exported (a) into any U.S.-embargoed countries or (b) to anyone on the U.S. Treasury Department’s Specially Designated Nationals List or the U.S. Department of Commerce Denied Persons List or Entity List. By using the App, you represent and warrant that you are not located in any such country or on any such list.
                        </p>
                        <p>
                        6. Information Provided by You
                        In consideration of your use of the App, you agree to provide true, accurate, current and complete information about yourself as prompted by the App. If you provide any information that is untrue, inaccurate, not current or incomplete (or becomes untrue, inaccurate, not current or incomplete), or venti has reasonable grounds to suspect that such information is untrue, inaccurate, not current or incomplete, venti has the right to suspend or terminate any and all current or future use of the App. venti reserves the right to refuse service, terminate accounts, or remove or edit content in its sole discretion. Notwithstanding the above, we retain the right at our sole discretion to deny access to anyone to the App and the Services we offer, at any time and for any reason, including, but not limited to, for violation of these TOU or our Privacy Policy.
                        </p>
                        <p>
                        7. Disclaimer of Liability and Warranty
                        THE CONTENT, PRODUCTS, AND SERVICES PUBLISHED ON THIS APP MAY INCLUDE INACCURACIES OR ERRORS, INCLUDING PRICING ERRORS. WE DO NOT GUARANTEE THE ACCURACY OF, AND DISCLAIM ALL LIABILITY FOR ANY ERRORS OR OTHER INACCURACIES RELATING TO THE INFORMATION AND DESCRIPTION OF THE CONTENT, PRODUCTS, AND SERVICES. WE EXPRESSLY RESERVE THE RIGHT TO CORRECT ANY PRICING ERRORS IN THE APP AND/OR ON PENDING RESERVATIONS MADE UNDER AN INCORRECT PRICE.
                        </p>
                        <p>
                        venti MAKES NO REPRESENTATIONS ABOUT THE SUITABILITY OF THE INFORMATION, SOFTWARE, PRODUCTS, AND SERVICES CONTAINED, OR LINKED TO, IN THIS APP FOR ANY PURPOSE, AND THE INCLUSION OR OFFERING OF ANY PRODUCTS OR SERVICES ON THIS APP DOES NOT CONSTITUTE ANY ENDORSEMENT OR RECOMMENDATION OF SUCH PRODUCTS OR SERVICES. ALL SUCH INFORMATION, SOFTWARE, PRODUCTS, AND SERVICES ARE PROVIDED “AS IS” WITHOUT WARRANTY OF ANY KIND. venti AND THEIR RESPECTIVE SUPPLIERS DISCLAIM ALL WARRANTIES AND CONDITIONS THAT THIS APP, ITS SERVERS OR ANY EMAIL SENT FROM venti, ITS AFFILIATES, AND/OR THEIR RESPECTIVE SUPPLIERS ARE FREE OF VIRUSES OR OTHER HARMFUL COMPONENTS. venti, ITS AFFILIATES, AND THEIR RESPECTIVE SUPPLIERS HEREBY DISCLAIM ALL WARRANTIES AND CONDITIONS WITH REGARD TO THIS INFORMATION, SOFTWARE, PRODUCTS, AND SERVICES, INCLUDING ALL IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, TITLE, AND NONINFRINGEMENT.
                        </p>
                        <p>
                        THE SUPPLIERS PROVIDING TRAVEL OR OTHER SERVICES SOLD OR OFFERED THROUGH THIS APP ARE INDEPENDENT CONTRACTORS AND NOT AGENTS OR EMPLOYEES OF venti OR ITS AFFILIATES. venti AND ITS AFFILIATES ARE NOT LIABLE FOR THE ACTS, ERRORS, OMISSIONS, REPRESENTATIONS, WARRANTIES, BREACHES OR NEGLIGENCE OF ANY SUCH SUPPLIERS OR FOR ANY PERSONAL INJURIES, DEATH, PROPERTY DAMAGE, OR OTHER DAMAGES OR EXPENSES RESULTING THEREFROM venti AND ITS AFFILIATES HAVE NO LIABILITY AND WILL MAKE NO REFUND IN THE EVENT OF ANY DELAY, CANCELLATION, OVERBOOKING, STRIKE, FORCE MAJEURE OR OTHER CAUSES BEYOND THEIR DIRECT CONTROL, AND THEY HAVE NO RESPONSIBILITY FOR ANY ADDITIONAL EXPENSE, OMISSIONS, DELAYS, RE-ROUTING OR ACTS OF ANY GOVERNMENT OR AUTHORITY.
                        </p>
                        <p>
                        IN NO EVENT SHALL venti, ITS AFFILIATES, AND/OR THEIR RESPECTIVE SUPPLIERS BE LIABLE FOR ANY DIRECT, INDIRECT, PUNITIVE, INCIDENTAL, SPECIAL, OR CONSEQUENTIAL DAMAGES ARISING OUT OF, OR IN ANY WAY CONNECTED WITH, YOUR ACCESS TO, DISPLAY OF OR USE OF THIS APP OR WITH THE DELAY OR INABILITY TO ACCESS, DISPLAY OR USE THIS APP (INCLUDING, BUT NOT LIMITED TO, YOUR RELIANCE UPON OPINIONS APPEARING IN THIS APP; ANY COMPUTER VIRUSES, INFORMATION, SOFTWARE, LINKED APPS, PRODUCTS, AND SERVICES OBTAINED THROUGH THIS APP; OR OTHERWISE ARISING OUT OF THE ACCESS TO, DISPLAY OF OR USE OF THIS APP) WHETHER BASED ON A THEORY OF NEGLIGENCE, CONTRACT, TORT, STRICT LIABILITY, OR OTHERWISE, AND EVEN IF venti, ITS AFFILIATES AND/OR THEIR RESPECTIVE SUPPLIERS HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
                        </p>
                        <p>
                        8. Limitation of Liability
                        YOU EXPRESSLY UNDERSTAND AND AGREE THAT venti AND ITS, AFFILIATES, OFFICERS, EMPLOYEES, AGENTS, PARTNERS AND LICENSORS SHALL NOT BE LIABLE TO YOU FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, CONSEQUENTIAL OR EXEMPLARY DAMAGES, INCLUDING, BUT NOT LIMITED TO, DAMAGES FOR LOSS OF PROFITS, GOODWILL, USE, DATA OR OTHER INTANGIBLE LOSSES (EVEN IF venti HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES), RESULTING FROM USE OF THE APP, CONTENT OR ANY RELATED SERVICES.
                        </p>
                        <p>
                        If, despite the limitation above, venti or their respective suppliers are found liable for any loss or damage which arises out of or in any way connected with any of the occurrences described above, then the liability of venti and/or their respective suppliers will in no event exceed, in the aggregate, the greater of (a) the service fees you paid to venti in connection with such transaction(s) on this App, or (b) One-Hundred Dollars (US$100.00) or the equivalent in local currency.
                        </p>
                        <p>
                        The limitation of liability reflects the allocation of risk between the parties. The limitations specified in this section will survive and apply even if any limited remedy specified in these terms is found to have failed its essential purpose. The limitations of liability provided in these terms inure to the benefit of venti and/or its respective suppliers.
                        </p>
                        <p>
                        9. Indemnity
                        You agree to indemnify and hold venti (and its officers, directors, agents, subsidiaries, joint ventures, and employees) harmless from any and against any claims, causes of action, demands, recoveries, losses, damages, fines, penalties or other costs or expenses of any kind or nature, including reasonable attorneys’ fees, or arising out of or related to your breach of this TOU, your violation of any law or the rights of a third party, or your use of the App.
                        </p>
                        <p>
                        10. Communications
                        When you use the App or send emails to venti, you are communicating with venti electronically. You consent to receive communications from venti electronically. venti may communicate with you by email or by posting notices on the App. You agree that all agreements, notices, disclosures and other communications that we provide to you electronically satisfy any legal requirement that such communications be in writing.
                        </p>
                        <p>
                        venti may also communicate via SMS text messaging with users who opt-in to receiving SMS text messages from us. User certifies that they are the subscriber of all telephone numbers they have provided to venti. Standard rates and fees may apply from your telephone provider. You are not required to provide consent to receiving SMS text messages as a condition of receiving any service from venti, and have the right to revoke consent for any and all telephone numbers provided at any time. You may revoke that consent either by e-mailing us at admin@venti.co, or otherwise providing written notice to us at our address provided herein.
                        </p>
                        <p>
                        11. Links
                        The App and/or third parties may provide links on the World Wide Web or various resources. Because venti has no control over such websites and resources, you acknowledge and agree that venti is not responsible for the availability of such external websites or resources, and does not endorse and is not responsible or liable for any content, advertising, products or other materials on or available from such websites or resources. You further acknowledge and agree that venti shall not be responsible or liable, directly or indirectly, for any damage or loss caused or alleged to be caused by or in connection with use of or reliance on any such content, goods or services available on or through any such websites or resource.
                        </p>
                        <p>
                        12. Modification to Terms of Use.
                        venti reserves the right to make changes to the App, related policies and agreements, our Booking Terms, Privacy Policy and this TOU at any time. Always check the App for the most updated version before usage.
                        </p>
                        <p>
                        13. Trademarks
                        venti’s trademarks are important corporate assets and venti requires they are used properly. To preserve its reputation and protect its trademarks, venti diligently guards against any violation of its trademarks. venti acknowledges the desire of third parties to show affiliation with venti. Without written permission from venti, you should not use venti’s trademarks, service marks, or names in a manner suggesting any affiliation or association with venti. Only parties with written permission from venti are allowed to use venti’s trademarks in accordance with applicable terms.
                        venti trademarks take on various forms and may include letters, words, logos, designs, images, slogans, and colors associated with our company, App, and service offerings.
                        </p>
                        <p>
                        Certain activities may constitute infringement or dilution of venti’s trademarks, and are not permitted. Please review the following list of ways to avoid unauthorized use of venti’s trademark(s):
                        </p>
                        <p>
                        Do not use an venti trademark or name in a manner likely to cause confusion about the origin of any product, service, material, course, technology, program or other offerings.
                        Unless you have prior permission from venti, do not use an venti trademark or name in a manner likely to give the impression or otherwise imply an affiliation or association between you, your products or services, and venti, or any of its products, services, programs, materials, or other offerings.
                        Do not use the venti logo in any materials without the written permission of venti.
                        Do not use any venti trademarks as or as part of a company, product, service, solution, technology, or program name.
                        Do not use a venti trademark in a manner likely to dilute, defame, disparage, or harm the reputation of venti.
                        Do not use any trademark or designation confusingly similar to the venti name or any venti trademarks.
                        Do not copy or imitate any venti trade dress, type style, logo, product packaging, or the look, design, or overall commercial impression of any venti App, blog, or other materials.
                        Do not register or use any domain name which incorporates any venti mark or name.
                        Do not register, or seek to register, an venti trademark or any mark or name confusingly similar to an venti mark.
                        14. User-Contributed Content
                        a. Generally
                        We may, in our sole discretion, permit users to post, upload, publish, submit or transmit content through the App (referred to herein as “user-contributed content”). By posting or otherwise submitting any user-contributed content on or through the App, you hereby grant to venti a worldwide, irrevocable, perpetual, non-exclusive, transferable, royalty-free license, with the right to sublicense, to use, view, copy, adapt, modify, distribute, license, sell, transfer, publicly display, publicly perform, transmit, stream, broadcast, access, view, and otherwise exploit such user-contributed content on, through, or by means of the App or otherwise. venti does not claim any ownership rights in any such user-contributed content and nothing in these TOU will be deemed to restrict any rights that you may have to use and exploit any such user-contributed content.
                        </p>
                        <p>
                        You acknowledge and agree that you are solely responsible for all user-contributed content that you make available through the App, and venti takes no responsibility for that content. Accordingly, you represent and warrant that:
                        </p>
                        <p>
                        you either are the sole and exclusive owner of all user-contributed content that you make available through the App, or you have all rights, licenses, consents and releases that are necessary to grant to venti the rights in such user-contributed content as contemplated under these TOU; and
                        your posting, uploading, publication, submission or transmittal of the user-contributed content on, through or by means of the App does not infringe, misappropriate or violate any third party’s patent, copyright, trademark, trade secret, moral rights or other proprietary or intellectual property rights, rights of publicity or privacy, or violate any applicable laws or regulations.
                        By submitting a photograph to venti, user represents and warrants: (a) that any people in the photograph have given permission for their likeness to be displayed pursuant to these TOU, (b) that the photograph accurately and fairly represents the subject of the photograph and has not been altered in any manner that would mislead a viewer of that photograph, and (c) that user will indemnify and hold harmless venti from any cause of action arising from any misrepresentation with respect to any and all photographs user has submitted.
                        </p>
                        <p>
                        Users may not post or submit any content that:
                        </p>
                        <p>
                        makes use of offensive language or images;
                        is defamatory;
                        exploits images or the likeness of individuals under 18 years of age;
                        contains nudity or sexually explicit content;
                        depicts use of illicit drugs;
                        may disparage any ethnic, racial, sexual or religious group by stereotypical depiction or otherwise; or
                        provides a link to any commercial websites.
                        b. No Obligation to Post Content
                        We have no obligation to post any content from you or anyone else. In addition, we may, in our sole and unfettered discretion, edit, remove or delete any content that you post or submit.
                        </p>
                        <p>
                        15. Procedure for Claims of Intellectual Property Infringement
                        venti respects the intellectual property of others, and we ask our users to do the same. If you believe that your work has been copied in a way that constitutes copyright infringement, or your intellectual property rights have been otherwise violated, please provide venti’s Copyright Agent the following information:
                        </p>
                        <p>
                        (i) an electronic or physical signature of the person authorized to act on behalf of the owner of the copyright or other intellectual property interest; (ii) a description of the copyrighted work or other intellectual property that you claim has been infringed; (iii) a description of where the material that you claim is infringing is located on the App; (iv) your address, telephone number, and email address; (v) a statement by you that you have a good faith belief that the disputed use is not authorized by the copyright owner, its agent, or the law; or (vi) a statement by you, made under penalty of perjury, that the above information in your notice is accurate and that you are the copyright or intellectual property owner or authorized to act on the copyright or intellectual property owner’s behalf.
                        </p>
                        <p>
                        venti’s agent for notice of claims of copyright or other intellectual property infringement can be reached as follows:
                        </p>
                        <p>
                        By email:
                        </p>
                        <p>
                        admin@venti.co
                        </p>
                        <p>
                        16. Terms for iOS App
                        These TOU apply to your use of all the Services, including the iOS versions of the App (the “iOS App”) available via the Apple, Inc. (“Apple”) App Store, but the following additional terms also apply to the iOS App:
                        </p>
                        <p>
                        Both you and venti acknowledge that the TOU are concluded between you and venti only, and not with Apple, and that Apple is not responsible for the iOS App or any associated content;
                        The iOS App is licensed to you on a limited, non-exclusive, non-transferrable, non-sublicensable basis, solely to be used in connection with the Services for your private, personal, non-commercial use, subject to all the terms and conditions of these TOU as they are applicable to the Services;
                        You will only use the iOS App in connection with an Apple device that you own or control;
                        You acknowledge and agree that Apple has no obligation whatsoever to furnish any maintenance and support services with respect to the iOS App;
                        In the event of any failure of the iOS App to conform to any applicable warranty, including those implied by law, you may notify Apple of such failure; upon notification, Apple’s sole warranty obligation to you will be to refund to you the purchase price, if any, of the iOS App;
                        You acknowledge and agree that venti, and not Apple, is responsible for addressing any claims you or any third party may have in relation to the iOS App;
                        You acknowledge and agree that, in the event of any third-party claim that the iOS App or your possession and use of the iOS App infringes that third party’s intellectual property rights, venti, and not Apple, will be responsible for the investigation, defense, settlement and discharge of any such infringement claim;
                        You represent and warrant that you are not located in a country subject to a U.S. Government embargo, or that has been designated by the U.S. Government as a “terrorist supporting” country, and that you are not listed on any U.S. Government list of prohibited or restricted parties;
                        Both you and venti acknowledge and agree that, in your use of the iOS App, you will comply with any applicable third-party terms of agreement which may affect or be affected by such use; and
                        Both you and venti acknowledge and agree that Apple and Apple’s subsidiaries are third-party beneficiaries of these TOU, and that upon your acceptance of these TOU, Apple will have the right (and will be deemed to have accepted the right) to enforce these TOU against you as the third-party beneficiary hereof.
                        17. Severability and Survivability
                        If any provision, or portion of a provision, in these TOU shall be unlawful, void, or for any reason unenforceable, then that provision shall be deemed severable and shall not affect the validity and enforceability of any remaining provisions. The parties agree to substitute for such provision a valid provision which most closely approximates the intent and economic effect of such severed provision.
                        </p>
                        <p>
                        Notwithstanding any other provisions of these TOU, or any general legal principles to the contrary, any provision of these TOU that imposes or contemplates continuing obligations on a party will survive the expiration or termination of these TOU.
                        </p>
                        <p>
                        18. Disputes: Binding Arbitration, Governing Law, Jurisdiction, etc.
                        These TOU and the relationship between you and venti will be governed by the laws of the State of California without regard to its conflict of law provisions.
                        </p>
                        <p>
                        User and venti shall attempt in good faith to resolve any dispute concerning, relating, or referring to our Privacy Policy, the App, any literature or materials concerning venti, and these TOU, or the breach, termination, enforcement, interpretation or validity thereof, (hereinafter a “Dispute”) through good faith preliminary negotiations. If the Dispute is not resolved through good faith negotiation, all Disputes shall be resolved exclusively by binding arbitration held in Wilmington, Delaware, and presided over by one (1) arbiter. The arbitration shall be administered by JAMS pursuant to its Comprehensive Arbitration Rules and Procedures and in accordance with the Expedited Procedures in those Rules. The arbitrator’s decision shall be final and binding and judgment may be entered thereon. In the event a party fails to proceed with arbitration the other party is entitled to costs of suit including a reasonable attorney’s fee for having to compel arbitration. Nothing herein will be construed to prevent any party’s use of injunction, and/or any other prejudgment or provisional action or remedy. Any such action or remedy shall act as a waiver of the moving party’s right to compel arbitration of any dispute.
                        </p>
                        <p>
                        User and venti agree to submit to the personal jurisdiction of the federal and state courts located in Wilmington, Delaware with respect to any legal proceedings that may arise in connection with, or relate to, our Binding Arbitration clause and/or a Dispute. User and venti agree the exclusive venue for any and all legal proceedings that may arise in connection with, or relate to, our Binding Arbitration clause and/or a Dispute, shall be the federal and state courts located in Wilmington, Delaware and to irrevocably submit to the jurisdiction of any such court in any such action, suit or proceeding and hereby agrees not to assert, by way of motion, as a defense or otherwise, in any such action, suit or proceeding, any claim that (i) he, she or it is not subject personally to the jurisdiction of such court, (ii) the venue is improper, or (iii) this agreement or the subject matter hereof may not be enforced in or by such court. YOU RECOGNIZE, BY AGREEING TO THESE TERMS AND CONDITIONS, YOU AND venti ARE EACH WAIVING THE RIGHT TO A TRIAL BY JURY OR TO PARTICIPATE IN A CLASS ACTION WITH RESPECT TO THE CLAIMS COVERED BY THIS MANDATORY BINDING ARBITRATION PROVISION.
                        </p>
                        <p>
                        19. Attorney’s Fees, Costs, and Expenses of Suit
                        If any act of law or equity, including an action for declaratory relief or any arbitration proceeding, is brought to enforce, interpret or construe the provisions of these Terms of Use, our Privacy Policy, the App, or any literature or materials concerning venti, the prevailing party shall be entitled to recover actual attorney’s fees, costs, and expenses.
                        </p>
                        <p>
                        20. Waiver, Etc.
                        No delay or failure by either party to exercise or enforce at any time any right or provision hereof will be considered a waiver thereof of such party’s rights thereafter to exercise or enforce each and every right and provision hereof. No single waiver will constitute a continuing or subsequent waiver. venti does not guarantee it will take action against all breaches of these TOU. No waiver, modification or amendment of any provision hereof will be effective unless it is in a writing signed by both the parties.
                        </p>
                        <p>
                        venti Privacy Policy
                        Last updated: April 29, 2020
                        Venti Financial, Inc. (hereinafter referred to as “venti”, “we,” or “us”) is committed to protecting your privacy. This Privacy Policy explains how we collect, use, and disclose Personal Information when you use our website venti.co (the “Site”) and our Mobile Application (the “App”), referred to jointly herein as our “Services”, and your rights in relation to your Personal Information.
                        </p>
                        <p>
                        By continuing to use our Services, you acknowledge that you have had the chance to review and consider our Privacy Policy and agree to its terms, including the use of information and grounds for disclosure as described herein. If you do not understand this Privacy Policy, or do not agree to it, you should immediately cease your use of our Services.
                        </p>
                        <p>
                        Categories of Personal Information We May Collect
                        When you use our Services, we may collect the following kinds of Personal Information from you as needed:
                        </p>
                        <p>
                        Name
                        Birth date
                        Gender
                        Contact info, such as:
                        email address,
                        telephone number, and
                        home, business, and billing addressees
                        Government issued identification required for booking or identity verification, such as passport, or driver’s license information
                        Payment information such as payment card number, expiration date, and billing address
                        Travel-related preferences and requests, including:
                        preferences and favorite accommodations, flights, and/or activities etc.
                        special dietary requirements
                        accessibility needs
                        Geolocation (if user opts in)
                        Information we receive about you from third parties such as our business and affiliate partners and authorized service providers which may include updated contact information, demographic information, interests, and purchase history, which we may add to your account or profile and use for market research and analysis
                        Social media account ID and other publicly available information (we also may request you share of with us social media links, likes, and posts, but doing so will be specifically requested and sharing will require your consent)
                        Communications with us (such as recordings of calls with customer service representatives for quality assurance and training purposes)
                        Searches you conduct, transactions, and other interactions with you through our Services
                        Data you give us about other people, such as your travel companions or others for whom you are making a booking
                        Usage patterns
                        Non-Personally-Identifiable Information
                        We may use various technologies to collect Non-Personal Information about your access to our Services including:
                        </p>
                        <p>
                        1. Server Statistics. We may collect general statistics to track user trends on our Services. These statistics include hits to our server, the types of browsers used to access the Site, page views, and navigational patterns. The information that is collected in this manner does not provide us with any personally identifiable information about our users. It helps us track “where” our users are coming from. This allows us to pinpoint high traffic areas and determine the most effective ways to communicate with our users. In order to prevent the introduction of viruses and hackers into the Site, we may collect information, such as IP addresses, into a log file to be used to identify potential hackers of the Site.
                        </p>
                        <p>
                        2. Clear GIFs. We may use pixel tags (also called clear gifs) or other similar tracking technologies collect information about your navigation on our Services, and to enable us to know whether you have visited a web page or received a message. This information enables us to serve you and your interests better.
                        </p>
                        <p>
                        3. Cookies. To serve you faster and with better quality, we use “cookie” technology. Cookies are small pieces of text sent as files to your computer or mobile device when you visit most websites. Cookies may be delivered by us (first party cookies) or delivered by third-party suppliers (third-party cookies). Cookies are either session cookies or persistent cookies. Session cookies enable sites to recognize and link the actions of a user during a browsing session and expire at the end of each session. Persistent cookies help us recognize you as an existing user and these cookies are stored on your system or device until they expire, although you can delete them before the expiration date.
                        Cookies may be used to:
                        </p>
                        <p>
                        Determine which domain to measure
                        Distinguish unique users
                        Throttle the request rate
                        Remember the number and time of previous visits
                        Remember traffic source information
                        Determine the start and end of a session
                        Remember the value of visitor-level custom variables
                        You can set or amend your web browser controls to accept or refuse cookies whenever you like, but please remember if you do choose to reject cookies, your access to some of the functionality and areas of our site may be restricted. For further information and support, visit the specific help page for the web browser you are using.
                        </p>
                        <p>
                        venti’s Mobile Application
                        When you download and use our App, we collect certain technical information from your device to enable the App to work properly, optimize performance, and as otherwise described in this Privacy Policy. That technical information includes:
                        </p>
                        <p>
                        General device information, such as model, OS, brand, and orientation
                        RAM and disk size
                        CPU usage
                        Screen Size
                        Time zone
                        Carrier (based on Mobile Country and Network Code)
                        Radio/Network information (for example, WiFi, LTE, 3G)
                        Country (based on IP address)
                        Locale/language
                        Signal strength
                        Jailbroken or rooted status of device
                        Battery level and battery-charging state
                        App version
                        App foreground or background state
                        App package name
                        A pseudonymous app-instance identifier
                        Network URLs (not including URL parameters or payload content) and the following corresponding information:
                        Response codes (for example, 403, 200)
                        Payload size in bytes
                        Response times
                        Duration times for automated traces.
                        Permissions for Location-Based Services
                        Depending on your device’s settings and permissions and your choice to participate in certain programs, we may collect the location of your device by using GPS signals, cell phone towers, Wi-Fi signals, Bluetooth or other technologies. We will collect this information, if you opt in through the App or other program (either during your initial login or later) to enable certain location-based services available within the App (for example, locating available lodging closest to you). To disable location capabilities of the App, you can log off or change your mobile device’s settings.
                        </p>
                        <p>
                        Use of Personal Information
                        Depending on the particular Services you access, we may use your Personal Information for some of the various purposes described below:
                        </p>
                        <p>
                        1. Uses related to your use of our Services:
                        </p>
                        <p>
                        Booking requested travel, accommodations, activities, etc.
                        Providing services related to your booking(s)
                        Creating, maintaining, and updating user accounts and authenticating you as a user
                        Maintaining your search and travel history, accommodation and travel preferences, special needs, and similar information about your use of venti’s Services, and as otherwise described in this Privacy Policy
                        Enable and facilitate acceptance and processing of payments and other transactions
                        2. Uses related to user communications and/or marketing:
                        </p>
                        <p>
                        Responding to your questions and requests for information
                        Enabling communication between you and travel and activity suppliers
                        Contacting you (such as by text message, email, phone calls, mail, push notifications, or messages on other communication platforms) to provide information like travel booking confirmations and updates, for marketing purposes, or for other purposes as described in this Privacy Policy
                        Marketing our products and services, optimizing such marketing to be more relevant to you, and measuring and analyzing the effectiveness of our marketing and promotions
                        3. Other business and compliance related purposes:
                        </p>
                        <p>
                        Maintaining, improving, researching, and measuring the effectiveness of our Site and App and their functionality, performance, and effectiveness
                        Data analytics
                        Monitoring or recording calls, chats, and other communications with customer service and other representatives, as well as communications between or among suppliers and travelers for quality control, training, dispute resolution, and as described in this Privacy Policy
                        Promoting security, verifying identities of our customers, preventing and investigating fraud and unauthorized activities, defending against claims and other liabilities, and managing other risks
                        Complying with applicable laws, protect our and our users’ rights and interest, defend ourselves, and responding to law enforcement, other legal authorities, and requests that are part of a legal process
                        Complying with applicable security and anti-terrorism, anti-bribery, customs and immigration, and other such due diligence laws and requirements
                        Operating our business and other business related purposes permitted by law
                        Sharing of Personal Information
                        venti DOES NOT SELL YOUR PERSONALLY INDENTIFIABLE INFORMATION TO THIRD PARTIES. We share your Personal Information as described below, and elsewhere in this Privacy Policy, as permitted or required by applicable law:
                        </p>
                        <p>
                        1. Third-party service providers. We share Personal Information with third-parties in connection with the delivery of services to you and the operation of our business (for example, to provide credit card processing, customer service, business analytics, fraud prevention and compliance services, and so that we may serve you with advertising and promotions regarding our services that are tailored to your interests). These third-party service providers are required to protect Personal Information we share with them and may not use any directly identifying Personal Information other than to provide services we contracted them for. They are not allowed to use the Personal Information we share for purposes of their own direct marketing (unless you have separately consented with the third-party under the terms provided by the third-party).
                        </p>
                        <p>
                        2. Travel suppliers. We share Personal Information with travel-related suppliers such as hotels, airlines, car-rental companies, insurance, activity providers, and tour operators who fulfill your booking. Please note that travel suppliers may contact you to obtain additional information if and as required to facilitate your booking or to otherwise provide the travel or associated services.
                        </p>
                        <p>
                        3. Legal rights and obligations. We may disclose your Personal Information to enforce our policies, or where we are permitted (or believe in good faith that we are required) to do so by applicable law, such as in response to a request by a law enforcement or governmental authority, in connection with actual or proposed litigation, or to protect and defend our property, people and other rights or interests. We may also share your Personal Information pursuant to a subpoena or other legal request, or as necessary to remit certain taxes in the course of processing payments as required by law or legal process.
                        </p>
                        <p>
                        4. Sale of venti’s assets and similar transactions. We may share your Personal Information in connection with a corporate transaction, such as a divestiture, merger, consolidation, assignments or asset sale, or in the unlikely event of bankruptcy. In the case of any acquisition, we will inform the buyer it must use your Personal Information only for the purposes disclosed in this Privacy Policy.
                        </p>
                        <p>
                        Your Personal Information Related Rights and Choices
                        You have certain rights and choices with respect to your Personal Information:
                        </p>
                        <p>
                        You can update the accuracy of your information at any time by either logging into your account or contacting us at admin@venti.co
                        If you have an account with us, you may change your communication preferences by logging in and updating the information in your account
                        You can control our use of certain cookies by editing the settings in your web browser
                        If you no longer wish to receive marketing and promotional emails, you may unsubscribe by clicking the ‘unsubscribe’ link in the email. You can also log into your account to change communication settings. Please note that if you choose to unsubscribe from or opt out of marketing emails, we may still send you important transactional and account-related messages from which you will not be able to unsubscribe
                        For our App, you can view and manage notifications and preferences in the settings menus of the App and of your operating system
                        If we are processing or utilizing your Personal Information on the basis of consent, you may withdraw that consent at any time by contacting us. Withdrawing your consent will not affect the lawfulness of any processing that occurred before you withdrew consent and it will not affect our processing of your Personal Information that is conducted in reliance on a legal basis other than consent
                        Do-Not-Track Signals
                        Some web browsers may transmit “do-not-track” (DNT) signals to sites with which the browser communicates. Because of differences in how web browsers incorporate and activate this feature, it is not always clear whether users intend for these signals to be transmitted, or whether users are even aware of them. Until a standard for DNT is set, our Site will not support DNT directly. We have enacted various measures though to anonymize user data received from users that we recognize as transmitting DNT signals in an attempt to honor their request as much as practical.
                        </p>
                        <p>
                        Personal Information Submitted by Children
                        We do not knowingly collect personally identifiable information from children under 13. If a parent or guardian becomes aware that his or her child has provided us with Personal Information without their consent, he or she should contact us. If we become aware that a child under 13 has provided us with personal information, we will delete such information from our files.
                        </p>
                        <p>
                        <strong>Security</strong><br>
                        While no Internet site can guarantee absolute security, we use physical, electronic, and administrative safeguards to assist us in preventing unauthorized access, maintaining data accuracy, and correctly using your Personal Information. Any personal information is encrypted both in transit between the user’s device and our servers and at rest. Access to these servers is severely restricted based on role-based access control and the principle of least privilege. All operations performed on these servers generate auditable events that additionally track the authenticated user performing the operation.
                        </p>
                        <p>Venti is not responsible for the loss of funds within your account due to weak passwords, compromised email, or compromised phone numbers. You are responsible for ensuring your account is protected by the adequate safeguards.</p>
                        <p>
                        <strong>International Data Transfer</strong><br>
                        The Personal Information we process may be transmitted or transferred to countries other than the country in which you reside. Those countries may have data protection laws that are different from the laws of your country.
                        </p>
                        <p>
                        We are located in the United States, while third-party service providers operate in many countries around the world. When we collect your Personal Information, it may be processed in any of those countries. We have taken appropriate steps and put safeguards in place to help ensure that your Personal Information remains protected in accordance with this Privacy Policy.
                        </p>
                        <p>
                        <strong>Record Retention</strong><br>
                        We will retain your Personal Information in accordance with all applicable laws, for as long as it may be relevant to fulfill the purposes set forth in this Privacy Policy, unless a longer retention period is required or permitted by law. We will deidentify, aggregate, or otherwise anonymize your Personal Information if we intend to use it for analytical purposes or trend analysis over longer periods of time.
                        </p>
                        <p>
                        The criteria we use to determine our retention periods include:
                        </p>
                        <p>
                        The duration of our relationship with you, including any open accounts or recent bookings or other transactions you have made
                        Whether we have a legal obligation related to your Personal Information, such as laws requiring us to keep records of your transactions with us
                        Whether there are any current and relevant legal obligations affecting how long we will keep your Personal Information, including contractual obligations, litigation holds, statutes of limitations, and regulatory investigations.

                        <strong>Updates to Privacy Policy</strong><br>
                        venti may update our Privacy Policy in response to changing laws and/or technical or business developments. You can see when this Privacy Policy was last updated by checking the “last updated” date displayed at the bottom of this Statement. It is your responsibility to review venti’s Privacy Policy each time you provide Personal Information to venti as the Policy may have changed since the last time you used our services.
                        </p>
                        <p>
                        <strong>Contacting venti</strong><br>
                        If you have any questions or concerns about our use of your Personal Information, please contact us at: admin@venti.co or mail us 
                        </p>
                        <p>
                        <strong>Booking Terms</strong><br>
                        Last updated: October 15, 2023
                        The following terms and conditions (our “Booking Terms”) govern the reservation, purchase, and use of all Travel Services (as defined below) offered for sale by Venti Financial, Inc. (venti), a California-based company located at:<br>
                            <p>800 N King Street</p>
                            <p>Suite 304 1552</p>
                            <p>Wilmington, DE 19801</p>
                        </p>
                        <p>
                        All bookings of Travel Services are also subject to the Terms and Conditions of the Supplier of the Travel Service(s) incorporated in your reservation. By placing a reservation with venti, you agree to abide by all the Terms and Conditions of the applicable Suppliers, and to be bound by the limitations therein. If a Supplier’s Terms and Conditions are ever in conflict with our Booking Terms, venti’s Booking Terms will control all issues relating to the liabilities and responsibilities of venti.
                        </p>
                        <p>
                        Please read these Booking Terms carefully, ask us any questions you have about them, and/or consult an attorney if desired before you agree to be bound by them. You acknowledge that you have taken note of these Booking Terms before making a booking and have accepted the same by selecting the button marked “CONFIRM & BOOK”. Without this acceptance, the processing of a booking is not possible.
                        </p>
                        <p>
                        1. Definitions
                        This agreement and its terms and conditions are herein referred to as our “Booking Terms” or this “Agreement”
                        “venti” and/or “we” or “us” refer to Venti Financial, Inc. (doing business as “venti”)
                        “Application” and/or “App” refers to the venti mobile application
                        The “Site” is our website, located at http://www.venti.co
                        “Travel Service(s)” or just “Service(s)” encompass: accommodations, including hotel rooms, resorts, or other lodgings; airfare, including add-ons to airfare purchases; tours and tour packages; activity and event reservations; car rentals; travel insurance; and any other travel or travel related products offered or sold by venti.
                        The term “Trip” is defined as any Travel Service, or package of Travel Services, offered or sold by venti.
                        References herein to “Traveler(s)” or “you” shall apply to each and any of the following: a party participating in a Trip and/or the party who purchases a Trip for themselves and/or others, including a parent or legal guardian who accepts these Booking Terms on a participants’ behalf if the party participating is not of legal age or cannot otherwise enter into a binding legal contract on their own behalf.
                        Travel Service Supplier (“Supplier(s)”) are any third-party providers of Travel Services.
                        “Accommodations” are any lodgings in a dwelling or similar living quarters afforded to Travelers including, but not limited to, hotels, motels, and resorts.
                        2. Eligibility
                        The Services offered by venti are available for purchase by all North American residents who have the requisite power and authority to enter into and perform the obligations under these Terms and Conditions. You must be over the age of 18 to book a Trip and agree to these Terms and Conditions.
                        </p>
                        <p>
                        3. Prices and Pricing Errors
                        Only items and fees explicitly advertised as included in the price of a Travel Service will be included as part of Travel Service’s advertised price. We use commercially reasonable endeavors to publish and maintain accurate prices and information for Services. Suppliers provide the price, availability and other information related to these Services. In the event, however, that a Service is listed or provided to us at an incorrect price or with incorrect information due to typographical error or other error in pricing or information received from a Supplier, we retain the right to refuse or cancel any Reservation placed for such Service, whether or not the order has been confirmed and/or your credit card charged. If your credit card has already been charged for the purchase and your Reservation is canceled because of incorrect hotel, airline, or car provider information, we will promptly issue a credit to your credit card account in the amount of the charge. We expressly reserve the right to correct any advertised pricing errors and/or pending bookings. In such event, if available, we will offer you the opportunity to keep your pending reservation at the correct price or we will cancel your reservation without penalty.
                        </p>
                        <p>
                        No claim by Traveler relating to the price of a Trip will be considered once the reservation is effective. All prices are quoted in US dollars (US$) unless otherwise delineated. Rates for Travel Services are based on tariffs and exchange rates in effect at the time of posting and are subject to change prior to departure. Substantial changes in tariffs, exchange rates, price of fuel, services and labor sometimes increase the cost of Travel Service arrangements.
                        </p>
                        <p>
                        4. Bookings
                        To book a Trip with venti, please follow the booking procedures in the App. If you have difficulty booking through the App, please contact us directly at support@venti.co
                        </p>
                        <p>
                        5. Payment
                        Full payment is due at time of booking. We accept payment by all Major Credit Cards (Visa, MasterCard, American Express, Discover, etc)
                        </p>
                        <p>
                        6. Cancellation, Substitution, and Alteration Policies
                        i. Cancellations, Substitutions, and Alterations attributable to Traveler
                        </p>
                        <p>
                        Cancellation, substitution, and/or alteration terms vary by Supplier. Some Services are completely non-refundable after booking. Please familiarize yourself with the terms of your Supplier prior to booking as we cannot be held accountable for your unfamiliarity with those terms.
                        </p>
                        <p>
                        a. For California and Illinois Residents only:
                        </p>
                        <p>
                        Upon cancellation of the transportation or Travel Services, where the Traveler is not at fault and has not canceled in violation of any terms and conditions previously clearly and conspicuously disclosed and agreed to by the Traveler, all sums paid to the seller of travel for services not provided will be promptly paid to the Traveler, unless the Traveler advises the seller of travel in writing, after cancellation. This provision does not apply where the seller of travel has remitted the payment to another registered wholesale seller of travel or a carrier, without obtaining a refund, and where the wholesaler or provider defaults in providing the agreed-upon transportation or service. In this situation, the seller of travel must provide a California Traveler with a written statement accompanied by bank records establishing the disbursement of the payment, and if disbursed to a wholesale seller of travel, proof of current registration of that wholesaler.
                        </p>
                        <p>
                        ii. Cancellations and/or Alterations not attributable to Traveler
                        </p>
                        <p>
                        Particularly as a result of causes beyond our control (ie. “Force Majeure” as defined infra), changes and/or cancellations may need to be made to confirmed bookings. While we always endeavor to avoid changes and cancellations, venti and our Suppliers must reserve the right to do so and to substitute alternative arrangements of comparable monetary value. venti and our Suppliers reserve the right to adjust Travel Services by changing the modes of air travel, changing the category of Accommodations, or otherwise altering a Trip without prior notice. We accept no liability for loss of enjoyment as a result of these changes. venti will attempt to provide Traveler advanced notice of any changes to your Trip to the extent commercially possible.
                        </p>
                        <p>
                        7. Issuing Travel Documents
                        Travel documents will only be sent to the purchasing Traveler who places the order and personally agrees to these Terms and Conditions. Should you change your email address, phone number, or address before your departure date, you are required to advise us of the change. If a Traveler provides incorrect information to venti, we do not assume any liability if the Trip is adversely affected or made impossible by the non-receipt of travel documents.
                        </p>
                        <p>
                        8. Travelers with Special Needs or Disabilities
                        If you have special needs (e.g., wheelchair accessible room, traveling with seeing eye dog, etc.) you must contact all relevant Suppliers for your Trip ahead of time and verify that those special needs can be met. Depending on their terms and conditions, your reservation may be refunded, canceled or modified if special needs cannot be met. venti makes no guarantee as to the ability of any Travel Supplier to meet the special needs of disabled Travelers.
                        </p>
                        <p>
                        Travelers should be aware that different countries have their own standards of accessibility for persons with disabilities. Before you travel, visit travel.state.gov/destination and enter the appropriate country or area to find information for mobility-impaired Passengers in the “Special Laws & Circumstances” Section.
                        </p>
                        <p>
                        9. Insurance
                        Should you have to cancel your Trip because of illness, injury or death to you or an immediate family member, depending on the type of coverage purchased, trip cancellation insurance may protect some or all deposits and payments for Trip costs. Trip cancellation and interruption penalties are significant. Purchasing trip cancellation insurance at a much later date may limit some of the coverage as to pre-existing or other conditions. venti recommends the immediate purchase of travel cancellation insurance including emergency medical evacuation, flight delay, baggage and repatriation.
                        </p>
                        <p>
                        It is Traveler’s responsibility to understand the limitations of their insurance coverage and purchase additional insurance as needed. It is the Traveler’s sole responsibility to research, evaluate and purchase appropriate coverage. Traveler agrees that venti is not responsible for any uninsured losses.
                        </p>
                        <p>
                        10. International Travel Requirements: Passports & Visas, Vaccinations, etc.
                        It is Traveler’s sole responsibility to verify they have all the necessary visas, transit visas, passports, and vaccinations prior to travel and have paid any required reciprocity fees for their destination. A full and valid passport is required for all persons traveling to any of the destinations outside the U.S. that we feature. You must obtain and have possession of a valid passport, all visas, permits and certificates, and vaccination certificates required for your entire Trip.
                        </p>
                        <p>
                        Most international Trips require a passport valid until at least six (6) months beyond the scheduled return date. Further information on entry requirements can be obtained from the State Department, by phone (202) 647-5335 or accessed online at http://travel.state.gov/content/passports/en/passports.html, or directly from the destination country’s website. Some countries require you to be in possession of a return ticket or exit ticket and have sufficient funds, etc. Similarly, certain countries require that the Traveler produce evidence of insurance/repatriation coverage before it will issue a visa.
                        </p>
                        <p>
                        You must carefully observe all applicable formalities and ensure that the surnames and forenames used for all Travelers when making a booking and appearing in their travel documents (booking forms, travel tickets, vouchers, etc.), correspond exactly with those appearing on their passport, visas, etc.
                        </p>
                        <p>
                        Immunization requirements vary from country to country and even region to region. Up-to-date information should be obtained from your local health department and consulate. You assume complete and full responsibility for, and hereby release venti from, any duty of checking and verifying vaccination or other entry requirements of each destination, as well as all safety and security conditions of such destinations during the length of the proposed travel or extensions expected or unexpected. For State Department information about conditions abroad that may affect travel safety and security, you can contact them by phone at (202) 647-5335. For foreign health requirements and dangers, contact the U.S. Centers for Disease Control (CDC) at (404) 332-4559, use their fax information service at (404) 332-4565, or go to http://wwwnc.cdc.gov/travel/.
                        </p>
                        <p>
                        WE CANNOT ACCEPT RESPONSIBILITY IF YOU ARE REFUSED PASSAGE ON ANY AIRLINE, TRANSPORT OR ENTRY INTO ANY COUNTRY DUE TO THE FAILURE ON YOUR PART TO CARRY OR OBTAIN THE CORRECT DOCUMENTATION OR IMMUNIZATIONS. IF FAILURE TO DO SO RESULTS IN FINES, SURCHARGES, CLAIMS, FINANCIAL DEMANDS OR OTHER FINANCIAL PENALTIES BEING IMPOSED ON US, YOU WILL BE RESPONSIBLE FOR INDEMNIFYING AND REIMBURSING US ACCORDINGLY.
                        </p>
                        <p>
                        11. Accommodations
                        i. Pricing and Taxes
                        Prices of Accommodations are based on single occupancy unless described otherwise. If you prefer double occupancy, you may select that option during the booking process.
                        In some countries, there is a local tax known as “visitors’ tax”, “city tax” “tourist tax” (or similar) and other fees including (but not limited to) resort fees or service charges, which shall be paid directly by Traveler at their Accommodations and/or at the airport. Our Suppliers shall use reasonable endeavors to provide, at the time of booking, an estimation of the applicable fees and/or local tax(es) attributable to each individual booking and payable at the time of booking and/or locally on arrival (“Local Tax/Fee Estimation”). Notwithstanding the foregoing, we cannot warrant that the Local Tax/Fee Estimations shall be accurate, and Travelers acknowledge and agree that the Local Tax/Fee Estimations are provided as estimates only. Travelers further acknowledge that Local Taxes and Fee Estimations may change from time to time. As a result, we cannot be held liable for any loss, costs or damages incurred as a result of the provision of such Local Taxes/Fee Estimations.
                        </p>
                        <p>
                        ii. Categories and Living Standards
                        The categories of the Accommodations we offer have been provided by the Accommodation Suppliers themselves and are in accordance with specific regulations applicable in each country. Accommodations and living standards may vary from country to country and region to region. An Accommodation in one country, therefore, may not be similar in terms of services and quality to an Accommodation in another, despite belonging to the same category. venti makes no guarantees about Accommodation living standards.
                        </p>
                        <p>
                        iii. Accommodation Ownership and Responsibility for Operation
                        venti provides the Accommodations for Trips through third-party Suppliers and retains no ownership or management interest in those Accommodations. venti does not guarantee the location or the amenities of the Accommodations nor the performance of the third-party Suppliers. If any issues arise with your Accommodations, please contact the owner/operators of the respective Accommodations directly.
                        </p>
                        <p>
                        iv. Refurbishment / Renovations
                        We provide the information supplied by our Accommodation Suppliers regarding the existence of works of refurbishment or renovation of the Accommodations, as well as duration thereof. We shall not accept claims for works about which we have not been informed or which extend beyond the planned date of conclusion thereof.
                        </p>
                        <p>
                        v. Age Requirements for Check-in
                        In some countries the legal adult age may differ depending on the relevant local legislation. It is the sole responsibility of Traveler to ensure that they are at least of legal age in order to check into their Accommodations.
                        </p>
                        <p>
                        vi. Holding Deposits
                        Many Accommodation Suppliers require a holding deposit on credit or debit card upon check-in to cover incidental charges incurred during the Traveler’s stay, including but not limited to, long distance telephone charges, room service, resort fees, in-room movies, damage or theft of property, mini bar usage and other such amenities. Such deposit is unrelated to any payment received by us for your Accommodation booking.
                        </p>
                        <p>
                        vii. No Show Travelers
                        No shows without prior warning for reserved Accommodations shall be considered a cancellation. Charges for no show Travelers may range from the cost of one night to 100% of the cost of the booking.
                        </p>
                        <p>
                        viii. Reservation Modifications
                        Unless specifically authorized by venti, Travelers shall not be allowed to reduce the reserved period of stay, nor change the names on their booking once it has been confirmed. Any such changes shall be deemed to be a cancellation of the booking.
                        Modifications to extend the reserved period shall be subject to availability; in the event of an extension, the price shall be modified accordingly.
                        </p>
                        <p>
                        ix. Cancellations
                        In the event Traveler cancels their reservation for Accommodations, Traveler shall have the right to be returned all amounts paid, with deduction of the amounts, if any, which may have accrued in terms of cancellation charges. Cancellation costs may vary depending on the destination, dates and relevant Accommodation Supplier.
                        All cancellations should be made by emailing us directly at support@venti.co. After we receive notice of a cancellation, we will inform you of all the charges, if any, which may be applicable.
                        </p>
                        <p>
                        x. Changes to Names of Particular Accommodations
                        Throughout the year, some Accommodations may change their name or trade name. This change shall not be construed as a change of Accommodations or modification of a booking.
                        </p>
                        <p>
                        xi. Reimbursement for Early Check-Out
                        Any claims by Travelers for reimbursement for leaving their Accommodations before the reserved departure date (early check-out), must be provided to us within 15 days of the effective date of departure, together with written confirmation from the Accommodation Supplier of time and date of departure. For early check-outs, Accommodation Suppliers may charge the full amount of the original booking, in which case reimbursement shall not apply.
                        </p>
                        <p>
                        xii. Accommodation Descriptions
                        All descriptions of Accommodations and their amenities are based on information we have received and/or our interpretation of that information. We cannot be held responsible for any inaccuracies or errors in the provided descriptions, and disclaim any liability for any errors or other inaccuracies in the provided descriptions.
                        </p>
                        <p>
                        12. Air Transport
                        i. General conditions governing air transport
                        venti’s responsibilities in respect to air travel are limited by the relevant airline’s Contract of Carriage. All airline Contracts of Carriage are available for view publicly on their websites and at their office branches. If your Trip involves carriage by multiple carriers, you will be bound by the Contracts of Carriage of each carrier.
                        venti is not able to specify the type of aircraft to be used by any airline or guarantee seat assignments, even if pre-booked with the airline. In certain situations out of our control, the airline fulfilling your reservation may change from the airline displayed on our App. venti is not responsible for losses due to cancelled flights, seats, or changed flight itineraries. Airlines retain the right to adjust flight times and schedules at any time, and these changes may include a change in the airline you fly, your aircraft type or destination. Such alterations do not constitute a significant change to your Trip. If an airline cancels or delays a flight, you should work with the airline to ensure you arrive at your destination on or ahead of time.
                        venti will not provide any refund for Trips missed, in part or full, due to missed, cancelled or delayed flights, or other flight irregularities including, without limitation, denied boarding whether or not you are responsible for such denial. Airline flights may be overbooked and you may be denied boarding. A person denied boarding on a flight may be entitled to a compensatory payment or other benefits from the airline. The rules for denied boarding are available at all ticket counters in your Contract of Carriage.
                        </p>
                        <p>
                        ii. Flight Times
                        Sometimes, a flight’s scheduled departure or arrival time in the App is different from the times reflected on your e-ticket. This may occur when the airline doesn’t transmit the updated schedule to us. If the flight is a month and more away, the scheduled time should update closer to the departure date.
                        If it isn’t updated two to three weeks prior to your departure date, we would appreciate if you contact us at support@venti.co, so we can update the schedule.
                        The flight times given by venti are subject to change. Up-to-date flight times will be shown on your e-ticket. Traveler must check the tickets very carefully immediately upon receipt of the latest timings.
                        </p>
                        <p>
                        iii. Failure to Check-in
                        Failure to check-in for a flight on the outward journey (on a charter or scheduled flight) will automatically result in cancellation of the return flight by the airline. We would encourage you to contact the airline directly on the date of departure if you fail to check-in for your outward journey, but you still want the airline to keep your return flight open; this decision remains at the discretion of the airline company.
                        </p>
                        <p>
                        iv. Flight Connections
                        If any booked flight connecting with your outbound or inbound flight is cancelled or delayed, the airlines reserve the right to provide that transport by any other means (coach/bus, train, etc.). If you organize your own connecting transport with the arrangements booked with venti, we would advise that you reserve flexible or refundable tickets in order to avoid the risk of any financial loss. You are also advised not to make any important appointments for the day following your return date. venti cannot accept responsibility for the consequences of delays (such as a cancelled scheduled flight) in the context of connecting transport organized by you.
                        </p>
                        <p>
                        v. Non-Use of Flight Segments
                        You agree not to purchase a ticket or tickets containing flight segments that you will not be using, such as a “point-beyond”, “hidden-city”, or “back-to-back tickets”. You further agree not to purchase a round-trip ticket that you plan to use only for one-way travel. You acknowledge that the airlines generally prohibit all such tickets, and therefore we do not guarantee that the airline will honor your ticket or tickets. You agree to indemnify venti against any airline claims for the difference between the full fare of your actual itinerary and the value of the ticket or tickets that you purchased.
                        </p>
                        <p>
                        vi. Baggage
                        venti assumes no liability for any loss or damage to baggage or personal effects, whether in transit to or from a Trip, or during a Trip. The airline is liable to you for the baggage you entrust to it only for the compensation contemplated in the international conventions and relevant statutes. In the event of damage, late forwarding, theft or loss of luggage, you should contact your airline and declare the damage, absence or loss of your personal effects before leaving the airport, and then submit a declaration, attaching the originals of the following documents: the travel ticket, the baggage check-in slip, and the declaration. It is recommended that you take out an insurance policy covering the value of your items.
                        Additional and oversized baggage fees: Most airlines have their own policy regarding baggage. Always check with your airline before you fly for their most current baggage fees and rules.
                        </p>
                        <p>
                        If we do not have info for your flight, we recommend that you check with your airline ahead of time for any weight restrictions and additional charges relating to checked baggage. You will be responsible for paying to the airline any additional charges for checked or overweight baggage, including, but not limited to, golf bags and oversized luggage. If you exceed the weight limit set by your airline, and excess weight is permitted, you must pay a supplement directly to the airline at the airport.
                        </p>
                        <p>
                        ix. Stop-Overs
                        Direct flights may be “non-stop” or may involve one or more stop-overs (in the latter case this means the same flight by the airline, because the flight number remains the same). The same applies to connecting flights that may be subject to crew changes. When you reserve a scheduled or charter flight involving a stop-over in a town, and the second flight takes off from a different airport to the airport of arrival, ensure that you have sufficient time for reaching the second airport. The journey to the other airport is at your own expense. venti will not be able to reimburse you for these costs, nor will it be liable if you miss the second flight.
                        </p>
                        <p>
                        x. E-tickets
                        An electronic ticket (“E-ticket”) is a ticket with no physical form. When using this type of ticket, Traveler must go to the check-in desk of the airline concerned and show a valid travel document (passport, visa, identity card, etc.) in order to obtain his / her boarding card. Traveler must strictly observe the times for checking in.
                        </p>
                        <p>
                        xi. Problems related to the issuance of e-tickets
                        As of June 1st, 2008, the International Air Transport Association (IATA) imposed rules with regard to the issuing of air travel tickets. As of that date, travel agencies and airlines have an obligation to only issue travel tickets via electronic means.
                        Due to technical constraints to do with airline’s restrictions in relation to certain requirements (infants under the age of 2, inter-airline agreements, groups, etc.), it may be impossible to issue an electronic ticket. Therefore, though a flight may be shown as available, it might prove impossible for us to honor your reservation. This situation, which is outside our control, will not result in liability on our part.
                        </p>
                        <p>
                        If we cannot issue you an e-ticket, we will contact you to propose an alternative route solution. This could involve a different tariff and/or additional costs for which you would be responsible. In the event of the absence of an alternative solution, your refusal to pay any tariff difference, or if the issuance of tickets proves impossible, we would be forced to cancel your reservation at no cost to you. We will provide you with a full refund within 30 days after determining that there is no alternative solution possible.
                        </p>
                        <p>
                        13. Activities
                        Some activities we offer are physically active and interactive, so Travelers must be in good physical condition and health to participate in them. An offered activity may not be appropriate for all ages or for individuals with certain medical conditions or disabilities. venti may not be held liable in the event of an incident or accident which is due to a lack of vigilance on your part.
                        </p>
                        <p>
                        It may happen that certain activities offered are no longer provided by the local Supplier for climatic reasons, in the event of Force Majeure, outside of the local tourist season, or when the minimum number of participants required for providing a given activity is not reached. In the early or late season, some activities may not be available, some of the facilities may be closed, or maintenance work may be in progress. As a general rule, entertainment and sports activities may vary in frequency depending on how many people are participating in them at the time and based on climatic conditions. venti cannot be liable for activities unavailable due to any of the reasons listed above or for any other reason outside of our control.
                        </p>
                        <p>
                        YOU ACKNOWLEDGE THAT THE USE OR ENJOYMENT OF AN ACTIVITY MAY BE HAZARDOUS AND INHERENTLY RISKY, AND, TO THE MAXIMUM EXTENT PERMITTED BY LAW, venti SHALL HAVE NO LIABILITY FOR ANY PERSONAL INJURY OR DEATH; LOST, STOLEN, DAMAGED OR DESTROYED PROPERTY; OR OTHER LIABILITY ARISING OUT OF OR IN CONNECTION WITH THE ACTIVITY.
                        </p>
                        <p>
                        14. Local Customs, Laws, and Travel Risks
                        Travelers may be traveling to foreign countries, with different customs, standards, laws and risks than those Travelers are accustomed to. Traveler understands that they must be prepared to cope with the unexpected, with local customs and shortages, with the vagaries of weather, travel and humankind in general. As such, Traveler acknowledges and accepts the risks associated with travel in a foreign country and agrees to release and hold venti harmless for any such problems experienced while participating in their Trip.
                        </p>
                        <p>
                        Although most travel to offered destinations is completed without incident, travel to certain areas may involve greater risk than others. You assume sole responsibility for your own safety at any destination traveled to. venti does not guarantee your safety at any time, and assumes no responsibility for gathering and/or disseminating information for you relating to risks associated with your destinations. BY OFFERING OR FACILITATING TRAVEL TO CERTAIN DESTINATIONS, WE DO NOT REPRESENT OR WARRANT THAT TRAVEL TO SUCH POINTS IS ADVISABLE OR WITHOUT RISK, AND WE SHALL NOT BE LIABLE FOR DAMAGES OR LOSSES THAT MAY RESULT FROM TRAVEL TO SUCH DESTINATIONS.
                        </p>
                        <p>
                        15. Notices
                        Any notices required or permitted hereunder shall be given:
                        </p>
                        <p>
                        i. If to venti, via email to:
                        </p>
                        <p>
                        admin@venti.co
                        </p>
                        <p>
                        ii. If to Traveler, at the email or physical address provided by Traveler during the booking process.
                        </p>
                        <p>
                        iii. Such notice shall be deemed given: upon personal delivery; if sent by electronic mail, upon confirmation of receipt; or if sent by certified or registered mail, postage prepaid, three (3) days after the date of mailing.
                        </p>
                        <p>
                        17. Limitation of Liability
                        IN NO EVENT SHALL VENTI BE LIABLE FOR ANY CONSEQUENTIAL, INDIRECT, EXEMPLARY, SPECIAL, INCIDENTAL OR PUNITIVE DAMAGES OF ANY KIND, INCLUDING WITHOUT LIMITATION, DAMAGES FOR ANY LOSS OF OPPORTUNITY OR OTHER PECUNIARY LOSS, EVEN IF venti HAS BEEN ADVISED OF THE POSSIBILITY OR PROBABILITY OF SUCH DAMAGES OR LOSSES, WHETHER SUCH LIABILITY IS BASED UPON CONTRACT, TORT, NEGLIGENCE OR OTHER LEGAL THEORY. IN NO EVENT SHALL VENTI’S TOTAL AGGREGATE LIABILITY TO THE TRAVELER FOR CLAIMS ARISING UNDER THIS AGREEMENT EXCEED THE TOTAL AMOUNTS PAID BY THE TRAVELER TO venti UNDER THIS AGREEMENT.
                        venti OFFERS VARIOUS TRAVEL SERVICES PROVIDED BY THIRD PARTY SUPPLIERS. venti RETAINS NO OWNERSHIP INTEREST, MANAGEMENT, OR CONTROL OF THOSE THIRD PARTY SUPPLIERS. TO THE FULLEST EXTENT PERMITTED BY LAW, venti DOES NOT ASSUME LIABILITY FOR ANY INJURY, DAMAGE, DEATH, LOSS, ACCIDENT OR DELAY DUE TO AN ACT OR OMISSION OF ANY THIRD PARTIES (INCLUDING THIRD PARTY SUPPLIERS), GOVERNMENTAL AUTHORITY, OR ACTS ATTRIBUTABLE TO YOU YOURSELF, INCLUDING, WITHOUT LIMITATION, NEGLIGENT OR RECKLESS ACTS.
                        </p>
                        <p>
                        18. Disclaimer of Warranties
                        The inclusion or offering of any Services by venti does not constitute any endorsement or recommendation of such products or services. UNLESS OTHERWISE STATED, ALL GOODS AND SERVICES OFFERED BY venti ARE PROVIDED TO YOU ON AN “AS IS,” “AS AVAILABLE” BASIS. Certain kinds of information, such as Hotel ratings, should be treated as broad guidelines. venti does not guarantee the accuracy of this information.
                        </p>
                        <p>
                        TO THE FULLEST EXTENT PERMISSIBLE PURSUANT TO APPLICABLE LAW, venti DISCLAIMS ALL REPRESENTATIONS AND WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, SUITABILITY FOR A PARTICULAR PURPOSE, TITLE, UNINTERRUPTED SERVICE, AND ANY WARRANTIES ARISING OUT OF A COURSE OF PERFORMANCE, DEALING OR TRADE USAGE FOR ALL GOODS AND SERVICES SOLD BY/THROUGH venti. Applicable law in your jurisdiction may not allow the exclusion of implied warranties, so the above exclusions may not apply to you.
                        </p>
                        <p>
                        19. Indemnification
                        You agree to defend and indemnify venti, and/or its respective Suppliers, and any of venti’s officers, directors, employees and agents from and against any claims, causes of action, demands, recoveries, losses, damages, fines, penalties or other costs or expenses of any kind or nature including but not limited to reasonable legal and accounting fees, brought by third parties as a result of:
                        </p>
                        <p>
                        your breach of this Agreement;<br>
                        your violation of any law or the rights of a third party;<br>
                        your use of the App or Site; or<br>
                        your use of the App or Site for, or on the behalf of, third parties.<br>
                        </p>
                        <p>
                        20. Force Majeure
                        venti shall not be responsible for failure to perform any of its obligations under this Agreement during any period in which such performance is prevented or delayed due to Force Majeure. “Force Majeure” refers to any event beyond venti’s reasonable control, including but not limited to severe weather, fire, flood, mudslides, earthquakes, war, labor disputes, strikes, political unrest, natural or nuclear disaster, epidemics, World Health Organization’s advisories and/or alerts, Center for Disease Control’s advisories and/or alerts, U.S. State Department’s advisories and/or alerts, any order of any local, provincial or federal government authority, interruption of power Services, terrorism or any other causes beyond the control of venti or deemed by venti to constitute a danger to the safety and well-being of Travelers. venti and our Suppliers reserve the right to cancel any Services due to Force Majeure. Any additional charges incurred arising from the postponement, delay or extension of a Trip due to Force Majeure will be the Traveler’s responsibility.
                        </p>
                        <p>
                        21. Marketing Materials and Illustrative Photos
                        venti endeavors to illustrate the Travel Services it offers using photographs or illustrations that provide a realistic representation of the Services. However, please note that photographs and illustrations appearing in descriptions are for illustrative purposes only. They are not contractual nor are they to be construed as guarantees of the conditions of the Travel Services pictured at the time of a scheduled Trip.
                        </p>
                        <p>
                        22. Compliance
                        If you believe that venti has not adhered to this Agreement, please contact venti by emailing us at admin@venti.co. We will do our best to address your concerns. If you feel that your concerns have been addressed incompletely, please let us know.
                        </p>
                        <p>
                        23. Disputes: Mandatory Binding Arbitration, Class Action Waiver
                        You and venti shall attempt in good faith to resolve any dispute concerning, relating, or referring to a Trip, Services sold by us, venti’s website or mobile app, any literature or materials concerning venti, and these Terms and Conditions or the breach, termination, enforcement, interpretation or validity thereof, (hereinafter a “Dispute”) through preliminary negotiations. If the Dispute is not resolved through good faith negotiation, all Disputes shall be resolved exclusively by binding arbitration held in San Diego, California and presided over by one (1) arbiter. The arbitration shall be administered by JAMS or a similar ADR organization pursuant to JAMS Comprehensive Arbitration Rules and Procedures and in accordance with the Expedited Procedures in those Rules. The arbitrator’s decision shall be final and binding and judgment may be entered thereon. In the event a party fails to proceed with arbitration the other party is entitled to costs of suit including a reasonable attorney’s fee for having to compel arbitration. Nothing herein will be construed to prevent any party’s use of injunction, and/or any other prejudgment or provisional action or remedy. Any such action or remedy shall act as a waiver of the moving party’s right to compel arbitration of any dispute. YOU RECOGNIZE, BY AGREEING TO THESE TERMS AND CONDITIONS, YOU AND venti ARE EACH WAIVING THE RIGHT TO A TRIAL BY JURY OR TO PARTICIPATE IN A CLASS ACTION WITH RESPECT TO THE CLAIMS COVERED BY THIS MANDATORY BINDING ARBITRATION PROVISION.
                        </p>
                        <p>
                        24. Governing Law, Venue, and Jurisdiction
                        These Terms and Conditions and the relationship between you and venti will be governed by U.S. law, particularly the laws of the State of California, without regard to its conflict of law provisions. You and venti agree to submit to the personal jurisdiction of the federal and state courts of San Diego County with respect to any legal proceedings that may arise in connection with, or relate to, our Binding Arbitration clause and/or any other Dispute related to these Booking Terms not covered by our Binding Arbitration clause. Traveler and venti agree the exclusive venue for any and all legal proceedings that may arise in connection with, or relate to, our Binding Arbitration clause and/or a Dispute, shall be the federal and state courts of San Diego County, and to irrevocably submit to the jurisdiction of any such court in any such action, suit or proceeding and hereby agrees not to assert, by way of motion, as a defense or otherwise, in any such action, suit or proceeding, any claim that (i) he, she or it is not subject personally to the jurisdiction of such court, (ii) the venue is improper, or (iii) this agreement or the subject matter hereof may not be enforced in or by such court.
                        </p>
                        <p>
                        25. Attorney’s Fees, Costs, and Expenses of Suit
                        If any act of law or equity, including an action for declaratory relief, is brought to enforce, interpret or construe the provisions of this Agreement and/or the included Mandatory Binding Arbitration Agreement, the prevailing party shall be entitled to recover actual reasonable attorney’s fees, costs, and expenses.
                        </p>
                        <p>
                        26. Assignment
                        Traveler may not assign his rights or obligations hereunder without the prior written consent of venti.
                        </p>
                        <p>
                        27. Severability and Survivability
                        If any provision, or portion of a provision, in these Terms and Conditions shall be unlawful, void, or for any reason unenforceable, then that provision shall be deemed severable and shall not affect the validity and enforceability of any remaining provisions. Traveler and venti agree to substitute for such provision a valid provision which most closely approximates the intent and economic effect of such severed provision.
                        Notwithstanding any other provisions of this these Terms and Conditions, or any general legal principles to the contrary, any provision of these Terms and Conditions that imposes or contemplates continuing obligations on a party will survive the expiration or termination of these Terms and Conditions.
                        </p>
                        <p>
                        28. Waiver
                        No delay or failure by either party to exercise or enforce at any time any right or provision hereof will be considered a waiver thereof of such party’s rights thereafter to exercise or enforce each and every right and provision hereof. No single waiver will constitute a continuing or subsequent waiver. venti does not guarantee it will take action against all breaches of this Agreement. No waiver, modification or amendment of any provision hereof will be effective unless it is in a writing signed by both the parties.
                        </p>
                        <p>
                        29. Consumer Complaints
                        Under California Civil Code Section 1789.3, California users of the Service receive the following specific consumer rights notice: The Complaint Assistance Unit of the Division of Consumer Services of the California Department of Consumer Affairs may be contacted in writing at 1020 N Street, #501, Sacramento, California 95814, or by telephone at (800) 952-5210.
                        </p>
                        <p>
                        30. Modification of Our Booking Terms
                        Our Booking Terms may be amended or modified by us at any time, without notice, on the understanding that such changes will not apply to Trips booked prior to the amendment or modification. It is therefore essential that you consult and accept our Booking Terms at the time of making a booking, particularly in order to determine which provisions are in operation at that time in case they have changed since the last time you placed an order with venti or reviewed our Booking Terms.
                        </p>
                        <p>
                        31. Entire Agreement
                        This Agreement is the final, complete and exclusive agreement of the parties with respect to the subject matter hereof and supersedes and merges all prior discussions or agreements between the parties with respect to such subject matter.
                        </p>
                        <p>
                        32. Booking on Our Site:
                    </p>
                    <ul>
<li>comply with all applicable laws</li>
<li>cooperate with any anti-fraud/anti-money laundering checks we need to carry out</li>
<li>not use the Platform to cause a nuisance or make fake Bookings</li>
<li>use the Travel Experience and/or Platform for their intended purpose</li>
<li>not cause any nuisance or damage, and not behave inappropriately to the Service Provider’s personnel (or anyone else, for that matter).</li>
</ul>
<p>33. Prices</p>
<p>
1. When you make a Booking, you agree to pay the cost of the Travel Experience, including any taxes and charges that may apply.</p>
<p>
2. Some of the prices you see may have been rounded to the nearest whole number. The price you pay will be based on the original, “non-rounded” price (although the actual difference will be tiny anyway).</p>
<p>
3. Obvious errors and misprints are not binding. For example, if you book a premium car or a night in a luxury suite that was mistakenly offered for $1, we may simply cancel that Booking and refund anything you’ve paid.</p>
<p>
4. A crossed-out price indicates the price of a like-for-like Booking without the price reduction applied (“like-for-like” means same dates, same policies, same quality of accommodation/vehicle/class of travel, etc.).</p>
<p>34. Payment</p>
<p>
1. For some products/services, the Service Provider will require an Upfront Payment and/or a payment taken during your Travel Experience. If we organize your payment, we (or in some cases our affiliate in the country your payment originates from) will be responsible for managing your payment and ensuring the completion of your transaction with our Service Provider. In this case, your payment constitutes final settlement of the “due and payable” price.
If the Service Provider charges you, this will usually be in person at the start of your Travel Experience, but it could also be (for example) that your credit card is charged when you book, or that you pay when you check out of your Accommodation. This depends on the Upfront Payment policy of the Service Provider as communicated to you in the booking process.</p>
<p>
2. If the Service Provider requires an Upfront Payment, it may be taken or pre-authorized when you make your Booking, and it may be non-refundable. Before you book, check the Service Provider’s Upfront Payments policy (available during the booking process), which we don’t influence and aren’t responsible for.
</p>
<p>
3. If you know of or suspect any fraud or unauthorized use of your Payment Method, contact your payment provider, who may cover any resulting charges, possibly for a fee.
</p>
<p>
4. If the currency selected on the Platform isn't the same as the Service Provider's currency, we may:
<ul>
    <li>show prices in your own currency</li>
    <li>offer you the Pay In Your Own Currency option.</li>
    <li>You’ll see our Currency Conversion Rate during check-out, in the Booking details of your Account, or (if you don’t have an Account) in the email we send you. If we charge you fees in connection with any such services, you’ll find the fee expressed as a percentage over European Central Bank rates. Your card issuer may charge you a foreign transaction fee.</li>
</ul>
</p>
<p>
5. We’ll store your Payment Method details for future transactions after collecting your consent.
</p>
<p>
36. Booking Policies
</p>
<p>
1. When you make a Booking, you accept the applicable policies as displayed in the booking process. You'll find each Service Provider's cancellation policy and any other policies (e.g. age requirements, security/damage deposits, additional supplements for group Bookings, extra beds, breakfast, pets, cards accepted, etc.) on our Platform, on the Service Provider information pages, during the booking process, in the fine print, and/or in the confirmation email or ticket (if applicable).
</p>
<p>
2. If you cancel a Booking or don’t show up, any cancellation/no-show fee or refund will depend on the Service Provider’s cancellation/no-show policy.
</p>
<p>
3. Some Bookings can’t be canceled for free, while others can only be canceled for free before a deadline.
</p>
<p>
4. If you book a Travel Experience by paying in advance (including all price components and/or a damage deposit if applicable), the Service Provider may cancel the Booking without notice if they can't collect the balance on the date specified. If they do, any non-refundable payment you’ve made will only be refunded at their discretion. It's your responsibility to make sure the payment goes through on time, that your bank, debit card, or credit card details are correct, and that there's enough money available in your account.
</p>
<p>
5. If you think you won’t arrive on time, contact your Service Provider and tell them when they can expect you so they don't cancel your Booking. If you’re late, we are not liable for the consequences (e.g. the cancellation of your Booking or any fees the Service Provider may charge).
</p>
<p>
6. As the person making the Booking, you are responsible for the actions and behavior (in relation to the Travel Experience) of everyone in the group. You’re also responsible for obtaining their permission before providing us with their personal data.
</p>
<p> 37. Flights </p>
<p>
    1. Most Flights on our Platform are provided via a Third-Party Aggregator, which acts as an intermediary to the airline(s).
</p>
<p>2. When you make a Booking, it’s directly with the airline. We’re not a “contractual party” to your Booking. When booking, you enter into (i) an Intermediation Contract with the Third-Party Aggregator (for the ticket) and (ii) a Contract of Carriage with the airline (for the Flight itself).</p>
<p>
3. If you book any extras (e.g. additional baggage, insurance, etc.), you’ll enter into a direct contract with the Third-Party Aggregator or another company. We will not be involved in this contract.</p>
<p>
4. We act solely as the Platform and are not involved in the Third-Party Terms. We are not responsible for your ticket or any extras you may buy and (to the fullest extent permitted by law) have no liability to you in relation to your Booking.
</p>
<p>
    5. You’ll find the cancellation policy in the Contract of Carriage, which is available while you’re booking your Flight.
</p>
<p>6. There may be a fee for changing or canceling your Flight.</p>
<p>7. Airlines reserve the right to reschedule or cancel flights at their discretion.</p>
<p>8. Different tickets from the same airline may have different restrictions or include different services.</p>
<p>9. If you have any questions about changes, cancellations, or refunds, contact our Customer Service team.</p>
<p> 38. Bank Account </p>
<p>1. It is against our terms of use to link and utilize a bank account that does not belong to you. We reserve the right to suspend accounts for whatever reason if we suspect fraud or if our terms are actively violated. You will have to work with our resolution team by sending an email to admin@venti.co.</p>
<p>2. ACH withdraws from your bank account must be made in good faith that the funds are available to cover the transaction cost and that you have the appropriate permission to send the funds to Venti. In the instance of an ACH failure due to insufficient funds, bank initiated return, or ACH reversals, we reserve the right to impose a fee of at least $25 dollars. This fee is either taken from your next deposit or withdrawal, deducted from your cash balance, deducted from your points balance, or invoiced to you directly.</p>
<p>3. Repeated ACH failures, returns, or reversals will result in account suspension. Dwolla reserves the right to immediately deactivate your account in the instance of ACH failure.</p>
<p>4. Failure to resolve outstanding fees and negative balances with Venti will result in account suspension or permanent closure.</p>
<p>5. Failure to resolve any outstanding payments may result in Venti submitting your unpaid invoices to a collections agency. Venti will send an invoice via electronic submission, and you agree to receive invoices via electronic means, including email. You will receive at least three notifications via email if the invoice has gone unpaid. Physical mail will be sent to your address on file if the invoice remains outstanding for more than 30 days. </p>
<p>6. Any bookings made by your account may be canceled by Venti if an ACH failure results in the loss of funds needed to cover the cost of your booking or if it places your account into the negative for more than 48 hours. You will be contact via email with a 48-hour notice to make the necessary corrections.</p>
<p>7. Venti reserves the right to cancel any deposit or withdraw attempt made on our platform if we suspect fraud or for any reason. You will receive an email notification, which may not include justification. You will be directed to contact Venti via admin@venti.co for questions.</p>
<p>39. Account Suspension</p>
<p>1. During account suspension, funds are frozen, meaning you cannot deposit, withdraw, or make purchases. We reserve the right to bring in necessary forensics and law enforcement to aid with investigations at your expense.</p>
<p>40. Boarding Pass Points ("Points")</p>
<p>1. Points cannot be redeemed for cash payments on or off our platform. We reserve the right to disallow the use of points on your account for any reason, especially if we suspect fraud is involved or if our terms are being violated.</p>
<p>2. In the instance of a refund, ACH failure, or negative balance, Venti reserves the right to deduct points and suspend your account.</p>
<p>3. In the instance of insolvency or bankruptcy, Venti is not obligated and will not exchange Boarding Pass Points for cash or any equivalent value.</p>
<p>41. Interest</p>
<p>1. Annual Percentage Yield (APY) is accurate as of 01/1/2024. Select markets only. APY is compounded and credited monthly. Fees may reduce earnings. Rates are variable and subject to change before and after account opening. Interest is not credited in cash. Only Boarding Pass Points.</p>
<p>2. Venti reserves the right to adjust the interest rate paid out to account holders to any amount; at any time; and for whatever reason.</p>
<p>42. Verified Customer/Boarding Pass</p>
<p>In order to use the payment functionality of our application, you must open a “Dwolla Platform” account provided by Dwolla, Inc. and you must accept the Dwolla <a href="https://www.dwolla.com/legal/tos" target="_blank">Terms of Service</a> and <a href="https://www.dwolla.com/legal/privacy" target="_blank">Privacy Policy</a>. Any funds held in or transferred through the Dwolla Account are held or transferred by Dwolla’s financial institution partners as described in the Dwolla Terms of Service. You authorize Venti Financial to collect and share with Dwolla your personal information including full name, [date of birth, social security number, physical address,] email address and financial information, and you are responsible for the accuracy and completeness of that data. You understand that you will access and manage your Dwolla account through our application, and Dwolla account notifications will be sent by Venti Financial, not Dwolla. We will provide customer support for your Dwolla account activity, and can be reached at https://venti.co/contact, admin@venti.co and/or 888-515-4209.</p>
<p>43. Cardholder Agreement</p>
<p>By adding a credit or debit card to your Venti profile, you agree to be an authorized user to conduct online transactions with the provided card. Venti reserves the right to contact the card issuer at anytime to verify your authorization. Venti reserves the right to block, cancel, and automatically refund charges associated with a card that we believe was improperly attached to your profile or used for a transaction.</p>
<p>It is against our terms of service to conduct a chargeback or file a dispute with your card issueer for a legitimate charge on your credit card. We reserve the right to cancel any order without refund. Attempts to reverse a charge for a legitimate booking without following Venti's cancellation procedures will be labeled an act of fraud and will be forwarded to the appropriate authorities.</p>
                        <!--<p>

                        Website terms
                        Welcome to venti.co (the “Site”) operated by Venti Financial, Inc. (“venti“). Please read these Terms of Use (“TOU”) and ask us any related questions before you use the Site. This website and the information content provided herein are granted for use to you (“you” or “user”) expressly conditioned on the acceptance of the following terms and conditions. By using this website, you agree to be bound by the terms, conditions and notices that follow without modification herein, under a grant of a limited, non-transferable license to use this website in accordance with these TOU. If you do not agree to these terms and conditions, you must not use this website.
                        </p>
                        <p>
                        Be sure to return to this page periodically to review the most current version of the TOU. We reserve the right at any time, at our sole discretion, to change or otherwise modify the TOU without prior notice, and your continued access or use of this Site signifies your acceptance of the updated or modified TOU.
                        </p>
                        <p>
                        1. Description of Site Services
                        venti provides users with tools for travel and leisure discovery, planning and booking (the “Services”) which can be accessed through the links posted on the Site to the venti Mobile App. You are responsible for obtaining access to the Site, and that access may involve third-party fees (such as Internet service provider or airtime charges). In addition, you must provide and are responsible for all equipment necessary to access the Site and our Mobile App.
                        </p>
                        <p>
                        2. License and Site Access
                        venti grants you a limited license to access and make personal use of the Site and the Services. This license does not include any downloading or copying of account information for the benefit of another vendor or any other third party; caching, unauthorized hypertext links to the Site and the framing of any content available through the Site; uploading, posting, or transmitting any material that contains software viruses or any other computer code, files or programs designed to interrupt, destroy or limit the functionality of any computer software or hardware or telecommunications equipment; any action that imposes or may impose (in venti’s sole discretion) an unreasonable or disproportionately large load on venti’s infrastructure; or any use of data mining, robots, scrapers, or similar data gathering and extraction tools. You may not bypass any measures used by venti to prevent or restrict access to the Site. Any unauthorized use by you shall terminate the permission or license granted to you by venti.
                        </p>
                        <p>
                        By using the Site you agree not to: (i) use this Site or its contents for any commercial purpose; (ii) make any speculative, false, or fraudulent reservation or any reservation in anticipation of demand; (iii) access, monitor or copy any content or information of this Site using any robot, spider, scraper or other automated means or any manual process for any purpose without our express written permission; (iv) violate the restrictions in any robot exclusion headers on this Site or bypass or circumvent other measures employed to prevent or limit access to this Site; (v) take any action that imposes, or may impose, in our discretion, an unreasonable or disproportionately large load on our infrastructure; (vi) deep-link to any portion of this Site (including, without limitation, the purchase path for any travel services) for any purpose without our express written permission; or (vii) “frame”, “mirror” or otherwise incorporate any part of this Site into any other website without our prior written authorization.
                        </p>
                        <p>
                        In consideration of your use of the Site, you agree to provide true, accurate, current and complete information about yourself as prompted by the Site. If you provide any information that is untrue, inaccurate, not current or incomplete (or becomes untrue, inaccurate, not current or incomplete), or venti has reasonable grounds to suspect that such information is untrue, inaccurate, not current or incomplete, venti has the right to suspend or terminate any and all current or future use of the Site. venti reserves the right to refuse service, terminate accounts, or remove or edit content in its sole discretion. Notwithstanding the above, we retain the right at our sole discretion to deny access to anyone to the Site and the Services we offer, at any time and for any reason, including, but not limited to, for violation of these TOU or our Privacy Policy.
                        </p>
                        <p>
                        3. Disclaimer of Liability and Warranty
                        THE CONTENT, PRODUCTS, AND SERVICES PUBLISHED ON THIS SITE MAY INCLUDE INACCURACIES OR ERRORS, INCLUDING PRICING ERRORS. WE DO NOT GUARANTEE THE ACCURACY OF AND DISCLAIM ALL LIABILITY FOR ANY ERRORS OR OTHER INACCURACIES RELATING TO THE INFORMATION AND DESCRIPTION OF THE CONTENT, PRODUCTS, AND SERVICES. WE EXPRESSLY RESERVE THE RIGHT TO CORRECT ANY PRICING ERRORS ON THE SITE AND/OR ON PENDING RESERVATIONS MADE UNDER AN INCORRECT PRICE.
                        </p>
                        <p>
                        venti MAKES NO REPRESENTATIONS ABOUT THE SUITABILITY OF THE INFORMATION, SOFTWARE, PRODUCTS, AND SERVICES CONTAINED, OR LINKED TO, ON THIS SITE FOR ANY PURPOSE, AND THE INCLUSION OR OFFERING OF ANY PRODUCTS OR SERVICES ON THIS SITE DOES NOT CONSTITUTE ANY ENDORSEMENT OR RECOMMENDATION OF SUCH PRODUCTS OR SERVICES. ALL SUCH INFORMATION, SOFTWARE, PRODUCTS, AND SERVICES ARE PROVIDED “AS IS” WITHOUT WARRANTY OF ANY KIND. venti AND THEIR RESPECTIVE SUPPLIERS DISCLAIM ALL WARRANTIES AND CONDITIONS THAT THIS SITE, ITS SERVERS OR ANY EMAIL SENT FROM venti, ITS AFFILIATES, AND/OR THEIR RESPECTIVE SUPPLIERS ARE FREE OF VIRUSES OR OTHER HARMFUL COMPONENTS. venti, ITS AFFILIATES, AND THEIR RESPECTIVE SUPPLIERS HEREBY DISCLAIM ALL WARRANTIES AND CONDITIONS WITH REGARD TO THIS INFORMATION, SOFTWARE, PRODUCTS, AND SERVICES, INCLUDING ALL IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, TITLE, AND NONINFRINGEMENT.
                        </p>
                        <p>
                        THE SUPPLIERS PROVIDING TRAVEL OR OTHER SERVICES SOLD OR OFFERED THROUGH THIS SITE ARE INDEPENDENT CONTRACTORS AND NOT AGENTS OR EMPLOYEES OF venti OR ITS AFFILIATES. venti AND ITS AFFILIATES ARE NOT LIABLE FOR THE ACTS, ERRORS, OMISSIONS, REPRESENTATIONS, WARRANTIES, BREACHES OR NEGLIGENCE OF ANY SUCH SUPPLIERS OR FOR ANY PERSONAL INJURIES, DEATH, PROPERTY DAMAGE, OR OTHER DAMAGES OR EXPENSES RESULTING THEREFROM venti AND ITS AFFILIATES HAVE NO LIABILITY AND WILL MAKE NO REFUND IN THE EVENT OF ANY DELAY, CANCELLATION, OVERBOOKING, STRIKE, FORCE MAJEURE OR OTHER CAUSES BEYOND THEIR DIRECT CONTROL, AND THEY HAVE NO RESPONSIBILITY FOR ANY ADDITIONAL EXPENSE, OMISSIONS, DELAYS, RE-ROUTING OR ACTS OF ANY GOVERNMENT OR AUTHORITY.
                        </p>
                        <p>
                        IN NO EVENT SHALL venti, ITS AFFILIATES, AND/OR THEIR RESPECTIVE SUPPLIERS BE LIABLE FOR ANY DIRECT, INDIRECT, PUNITIVE, INCIDENTAL, SPECIAL, OR CONSEQUENTIAL DAMAGES ARISING OUT OF, OR IN ANY WAY CONNECTED WITH, YOUR ACCESS TO, DISPLAY OF OR USE OF THIS SITE OR WITH THE DELAY OR INABILITY TO ACCESS, DISPLAY OR USE THIS SITE (INCLUDING, BUT NOT LIMITED TO, YOUR RELIANCE UPON OPINIONS APPEARING ON THIS SITE; ANY COMPUTER VIRUSES, INFORMATION, SOFTWARE, LINKED SITES, PRODUCTS, AND SERVICES OBTAINED THROUGH THIS SITE; OR OTHERWISE ARISING OUT OF THE ACCESS TO, DISPLAY OF OR USE OF THIS SITE) WHETHER BASED ON A THEORY OF NEGLIGENCE, CONTRACT, TORT, STRICT LIABILITY, OR OTHERWISE, AND EVEN IF venti, ITS AFFILIATES AND/OR THEIR RESPECTIVE SUPPLIERS HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
                        </p>
                        <p>
                        4. Limitation of Liability
                        YOU EXPRESSLY UNDERSTAND AND AGREE THAT venti AND ITS, AFFILIATES, OFFICERS, EMPLOYEES, AGENTS, PARTNERS AND LICENSORS SHALL NOT BE LIABLE TO YOU FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, CONSEQUENTIAL OR EXEMPLARY DAMAGES, INCLUDING, BUT NOT LIMITED TO, DAMAGES FOR LOSS OF PROFITS, GOODWILL, USE, DATA OR OTHER INTANGIBLE LOSSES (EVEN IF venti HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES), RESULTING FROM USE OF THE SITE, CONTENT OR ANY RELATED SERVICES.
                        </p>
                        <p>
                        If, despite the limitation above, venti or their respective suppliers are found liable for any loss or damage which arises out of or in any way connected with any of the occurrences described above, then the liability of venti and/or their respective suppliers will in no event exceed, in the aggregate, the greater of (a) the service fees you paid to venti in connection with such transaction(s) on this Site, or (b) One-Hundred Dollars (US$100.00) or the equivalent in local currency.
                        </p>
                        <p>
                        The limitation of liability reflects the allocation of risk between the parties. The limitations specified in this section will survive and apply even if any limited remedy specified in these terms is found to have failed its essential purpose. The limitations of liability provided in these terms inure to the benefit of venti and/or its respective suppliers.
                        </p>
                        <p>
                        5. Indemnity
                        You agree to indemnify and hold venti (and its officers, directors, agents, subsidiaries, joint ventures, and employees) harmless from any and against any claims, causes of action, demands, recoveries, losses, damages, fines, penalties or other costs or expenses of any kind or nature, including reasonable attorneys’ fees, or arising out of or related to your breach of this TOU, your violation of any law or the rights of a third party, or your use of the Site.
                        </p>
                        <p>
                        6. Electronic Communication
                        When you use the Site or send emails to venti, you are communicating with venti electronically. You consent to receive communications from venti electronically. venti will communicate with you by email or by posting notices on the Site. You agree that all agreements, notices, disclosures and other communications that we provide to you electronically satisfy any legal requirement that such communications be in writing.
                        </p>
                        <p>
                        7. Links
                        The Site and/or third parties may provide links to other World Wide Web sites or resources. Because venti has no control over such sites and resources, you acknowledge and agree that venti is not responsible for the availability of such external sites or resources, and does not endorse and is not responsible or liable for any content, advertising, products or other materials on or available from such sites or resources. You further acknowledge and agree that venti shall not be responsible or liable, directly or indirectly, for any damage or loss caused or alleged to be caused by or in connection with use of or reliance on any such content, goods or services available on or through any such site or resource.
                        </p>
                        <p>
                        8. Modification to Terms of Use
                        venti reserves the right to make changes to the Site, related policies and agreements, our Terms and Conditions of Sale, and this TOU and the Privacy Policy at any time. Always check the Site for the most updated version before usage.
                        </p>
                        <p>
                        9. Trademarks
                        venti’s trademarks are important corporate assets and venti requires they are used properly. To preserve its reputation and protect its trademarks, venti diligently guards against any violation of its trademarks. venti acknowledges the desire of third parties to show affiliation with venti. Without written permission from venti, you should not use venti’s trademarks, service marks, or names in a manner suggesting any affiliation or association with venti. Only parties with written permission from venti are allowed to use venti’s trademarks in accordance with applicable terms.
                        </p>
                        <p>
                        venti trademarks take on various forms and may include letters, words, logos, designs, images, slogans, and colors associated with our company, website, and service offerings.
                        </p>
                        <p>
                        Certain activities may constitute infringement or dilution of venti’s trademarks, and are not permitted. Please review the following list of ways to avoid unauthorized use of venti’s trademark(s):
                        </p>
                        <p>
                        Do not use an venti trademark or name in a manner likely to cause confusion about the origin of any product, service, material, course, technology, program or other offerings.
                        Unless you have prior permission from venti, do not use an venti trademark or name in a manner likely to give the impression or otherwise imply an affiliation or association between you, your products or services, and venti, or any of its products, services, programs, materials, or other offerings.
                        Do not use the venti logo in any materials without the written permission of venti.
                        Do not use any venti trademarks as or as part of a company, product, service, solution, technology, or program name.
                        Do not use a venti trademark in a manner likely to dilute, defame, disparage, or harm the reputation of venti.
                        Do not use any trademark or designation confusingly similar to the venti name or any venti trademarks.
                        Do not copy or imitate any venti trade dress, type style, logo, product packaging, or the look, design, or overall commercial impression of any venti website, blog, or other materials.
                        Do not register or use any domain name which incorporates any venti mark or name.
                        Do not register, or seek to register, an venti trademark or any mark or name confusingly similar to an venti mark.
                        </p>
                        <p>

                        10. Procedure for Claims of Intellectual Property Infringement
                        venti respects the intellectual property of others, and we ask our users to do the same. If you believe that your work has been copied in a way that constitutes copyright infringement, or your intellectual property rights have been otherwise violated, please provide venti’s Copyright Agent the following information:
                        </p>
                        <p>
                        (i) an electronic or physical signature of the person authorized to act on behalf of the owner of the copyright or other intellectual property interest; (ii) a description of the copyrighted work or other intellectual property that you claim has been infringed; (iii) a description of where the material that you claim is infringing is located on the Site; (iv) your address, telephone number, and email address; (v) a statement by you that you have a good faith belief that the disputed use is not authorized by the copyright owner, its agent, or the law; or (vi) a statement by you, made under penalty of perjury, that the above information in your notice is accurate and that you are the copyright or intellectual property owner or authorized to act on the copyright or intellectual property owner’s behalf.
                        </p>
                        <p>
                        venti’s agent for notice of claims of copyright or other intellectual property infringement can be reached as follows:
                        </p>
                        <p>
                        By email:
                        </p>
                        <p>
                        admin@venti.co
                        </p>
                        <p>
                        11. Severability and Survivability
                        If any provision, or portion of a provision, in these TOU shall be unlawful, void, or for any reason unenforceable, then that provision shall be deemed severable and shall not affect the validity and enforceability of any remaining provisions. The parties agree to substitute for such provision a valid provision which most closely approximates the intent and economic effect of such severed provision. Notwithstanding any other provisions of these TOU, or any general legal principles to the contrary, any provision of these TOU that imposes or contemplates continuing obligations on a party will survive the expiration or termination of these TOU.
                        </p>
                        <p>
                        12. Disputes: Binding Arbitration, Governing Law, Jurisdiction, etc.
                        These TOU and the relationship between you and venti will be governed by the laws of the State of California without regard to its conflict of law provisions.
                        </p>
                        <p>
                        User and venti shall attempt in good faith to resolve any dispute concerning, relating, or referring to our Privacy Policy, the Site, any literature or materials concerning venti, venti’s Trademark Policy, and these TOU, or the breach, termination, enforcement, interpretation or validity thereof, (hereinafter a “Dispute”) through good faith preliminary negotiations. If the Dispute is not resolved through good faith negotiation, all Disputes shall be resolved exclusively by binding arbitration held in Wilmington, Delaware, and presided over by one (1) arbiter. The arbitration shall be administered by JAMS pursuant to its Comprehensive Arbitration Rules and Procedures and in accordance with the Expedited Procedures in those Rules. The arbitrator’s decision shall be final and binding and judgment may be entered thereon. In the event a party fails to proceed with arbitration the other party is entitled to costs of suit including a reasonable attorney’s fee for having to compel arbitration. Nothing herein will be construed to prevent any party’s use of injunction, and/or any other prejudgment or provisional action or remedy. Any such action or remedy shall act as a waiver of the moving party’s right to compel arbitration of any dispute.
                        </p>
                        <p>
                        User and venti agree to submit to the personal jurisdiction of the federal and state courts located in Wilmington, Delaware with respect to any legal proceedings that may arise in connection with, or relate to, our Binding Arbitration clause and/or a Dispute. User and venti agree the exclusive venue for any and all legal proceedings that may arise in connection with, or relate to, our Binding Arbitration clause and/or a Dispute, shall be the federal and state courts located in Wilmington, Delaware and to irrevocably submit to the jurisdiction of any such court in any such action, suit or proceeding and hereby agrees not to assert, by way of motion, as a defense or otherwise, in any such action, suit or proceeding, any claim that (i) he, she or it is not subject personally to the jurisdiction of such court, (ii) the venue is improper, or (iii) this agreement or the subject matter hereof may not be enforced in or by such court. YOU RECOGNIZE, BY AGREEING TO THESE TERMS AND CONDITIONS, YOU AND venti ARE EACH WAIVING THE RIGHT TO A TRIAL BY JURY OR TO PARTICIPATE IN A CLASS ACTION WITH RESPECT TO THE CLAIMS COVERED BY THIS MANDATORY BINDING ARBITRATION PROVISION.
                        </p>
                        <p>
                        13. Attorney’s Fees, Costs, and Expenses of Suit
                        If any act of law or equity, including an action for declaratory relief or any arbitration proceeding, is brought to enforce, interpret or construe the provisions of these Terms of Use, our Privacy Policy, the Site, or any literature or materials concerning venti, the prevailing party shall be entitled to recover actual attorney’s fees, costs, and expenses.
                        </p>
                        <p>
                        14.Waiver, Etc.
                        No delay or failure by either party to exercise or enforce at any time any right or provision hereof will be considered a waiver thereof of such party’s rights thereafter to exercise or enforce each and every right and provision hereof. No single waiver will constitute a continuing or subsequent waiver. venti does not guarantee it will take action against all breaches of these TOU. No waiver, modification or amendment of any provision hereof will be effective unless it is in a writing signed by both the parties.
                        </p>
                    -->
                                </div>     
                            </div>
                        </div>
                    </div>
                        <div class="card bg-white">
                            <div class="card-body">
                                    <div class="row">
                                        <div class="col-lg-12 col-md-12 faqs-container">
                                            <h3 class="mb-3 mt-3">Venti's Privacy Policy</h3>
                                            <br>
                                            <p>
                                PLEASE READ THIS PRIVACY POLICY CAREFULLY. THIS POLICY DESCRIBES THE WAYS Venti Financial, INC. ("Venti", "we", "us", "our") OR OUR VENDORS COLLECT, PROTECT, USE AND STORE YOUR PERSONAL INFORMATION. YOU ACCEPT THIS PRIVACY POLICY BY USING OUR PRODUCTS AND SERVICES ON OUR WEBSITE OR THROUGH ANY OTHER MEANS (COLLECTIVELY THE "SERVICES"). We may amend this Privacy Policy at any time by posting a revised version on our website. We will attempt to give a reasonable notice period upon making any changes; however, unless otherwise stated, the revised version will be effective at the time we post it.

                                </p>
                                <p>1. How Venti collects your information</p>
                                <p>
                                1.1. Information you give Venti <br>
                                If you open an account or use the Venti Services, either directly or through a merchant partner or a third-party platform, we may collect the following types of information:

                                </p>
                                <p>Personal information - your name, date of birth, Social Security number, address, phone, email, third-party application IDs, and other similar information.
                                Financial information - bank account online login information, bank account numbers, bank account details including transaction history, routing numbers and/or debit card numbers and credit card numbers that you link to your Venti account or you give us when you use the Services. Before permitting you to use the Services, we may require you to provide certain information, including but not limited to your date of birth, social security number. We may use this information or other information you provide to verify your identity, which may require use to utilize third-party services.
                                </p>
                                <p>
                                1.2. Information Venti learns from your Use <br>
                                When you visit the Venti website or use the Services, we may collect information sent to us by your computer, mobile phone or any other device. This information may include your IP address, device information including but not limited to identifier, name and type, operating system, location, mobile network information and standard web log information, such as your browser type, traffic to and from our site, the pages you accessed on our website, and any other available information. We may also collect information about your use and interaction with our website, application or the Services. For example, we may evaluate your computer, mobile phone or other access device to identify any malicious software or activity that may affect the availability of the Services. When you use the Services, we may also store information based on your usage history. This includes, but is not limited to, details of your purchases, content you viewed, event information, click stream information, and cookies that may uniquely identify your browser or your account. We may also collect information about you from any contact you have with any of our services or employees, such as, with our customer support team, in surveys, or through interactions with our affiliates.

                                </p>
                                <p>1.3. Cookies and other tracking technologies <br>
                                We use various technologies to collect and store information when you use the Services, and this may include sending one or more cookies or device identifiers. We also use these tracking technologies when you interact with the services we offer to our partners, such as advertising services or Venti features that may appear on other sites and in any other manner that we deem necessary for our business purposes, such as:

                                </p>
                                <p>Site operations: Enabling features that are necessary for providing you the Services on our site, such as identifying you as being signed in, tracking content views, remembering your preferences, and the number of times you have been shown an advertisement.
                                Analytics: Allowing us to understand how our Services are being used, track site performance, and make improvements.
                                Personalized advertising: Delivering tailored advertising based on your preferences or interests across services and devices, and measuring the effectiveness of advertisements.
                                You can learn more about ad-serving companies and the options available to limit their collection and use of your information by visiting the websites for the Network Advertising Initiative, the Digital Advertising Alliance, and the European Interactive Digital Advertising Initiative. Similarly, you can learn about your options to opt out of mobile app tracking by certain advertising networks through your device settings and by resetting the advertiser ID on your Apple or Android device.

                                </p>
                                <p>Please note that opting out of advertising networks services does not mean that you will not receive advertising while using our Services or on other websites, nor will it prevent the receipt of interest- based advertising from other companies that do not participate in these programs. It will, however, exclude you from interest-based advertising conducted through participating networks, as provided by their policies and choice mechanisms. Note that if you delete your cookies, you may also delete your opt-out preferences

                                </p>
                                <p>1.4. Information obtained from third parties <br>
                                We may also obtain information about you from third parties such as credit bureaus and identity verification services. If you apply for a loan or line of credit with Venti, we will obtain consumer reports from consumer reporting agencies related to you at the time of your collection and periodically throughout the term of your loan or line of credit, as well as in connection with the use of any other services that we offer or that you may obtain from us (including financial management services, credit profile tools, and the Venti marketplace).

                                </p>
                                <p>You may choose to provide us with access to certain personal information stored by third parties such as social media sites (such as Facebook and Twitter). The information we have access to varies by site and is controlled by your privacy settings on that site and your authorization. By associating an account managed by a third party with your Venti account and authorizing Venti to have access to this information, you agree that Venti may collect, store and use this information in accordance with this Privacy Policy.

                                </p>
                                <p>2. How Venti protects your information</p>
                                <p>
                                2.1. Safety policies <br>
                                Venti stores and processes your information maintaining physical, electronic and procedural safeguards. We maintain physical security measures to guard against unauthorized access to systems and use safeguards such as firewalls and data encryption. We enforce physical access controls to our buildings, and we authorize access to personal information only for those employees or agents who require it to fulfill the responsibilities of their jobs.

                                </p>
                                <p>3. How Venti uses your information</p>
                                <p>
                                3.1. To improve our service <br>
                                Venti uses information to perform and improve our services, contact you, conduct research, and provide anonymous reporting for clients. For example, we may use information to provide customer service and support, process transactions, resolve disputes, collect payments, prevent illegal activities, customize the Services, reduce risk to all parties involved in our transactions, and verify the accuracy of information.

                                </p>
                                <p>3.2. To serve relevant marketing to you <br>
                                We may use information to deliver targeted marketing, service update notices, and promotional offers based on your communication preferences. We may combine your information with information we collect from other companies and use it to improve and personalize the Services, content, and advertising.

                                </p>
                                <p>4. How Venti shares your information.</p>
                                <p>
                                4.1. For our everyday business purposes <br>
                                We share your personal information with employees, affiliates, vendors, partners, merchant partners, marketing providers, and third parties as required to offer the Venti Services. This includes, but is not limited to, processing transactions, maintaining your account, offering or servicing loans or lines of credit, offering other financial services, responding to court orders and legal investigations, litigation purposes, complying with audits or other investigations, and reporting to credit bureaus. We also engage the following types of service providers to perform functions on our behalf: marketing providers, billing and collection providers, auditing and accounting firms, professional services consultants, providers of analytics services, security vendors, and IT vendors. Occasionally, these service providers may also collect data directly from you and their privacy policies may also apply. Venti will not share your financial information collected under Section 1.1 of this Privacy Policy with any third party unless required by law or valid court order.

                                </p>
                                <p>4.2. When required by law <br>
                                We will share your information with any party when required by law or by a government request to do so or to combat fraud or criminal activity.

                                </p>
                                <p>4.3. With our merchant partners to provide personalized offers <br>
                                We may share your information with merchants you interact with using Venti for marketing, if you consent or as permitted by law. For accounts created on or after 11/30/17: By creating an Venti account, you consent to Venti sharing your information with merchants you use the Venti Services with, for those merchants to market to you. We do this to help merchants personalize services and offers, so you can have a better experience. You're able to revoke your consent at any time by following the steps below. If you choose to do so, we will stop sharing your information with merchants from that point onward, except as necessary to complete transactions you initiate. To revoke your consent to information sharing with merchants for marketing, you can log into your Venti account at www.Venti.com/u/ and edit your Personalized Services preferences.

                                </p>
                                <p>5. What are your options?</p>
                                <p>
                                5.1 Notifications <br>
                                If you no longer wish to receive notifications about our Services, you may change your notification preferences by emailing admin@Venti.com. Alternatively, you may be able to indicate your preference by logging into your account and adjusting your preferences or by following the directions provided with the communication. Venti reserves the right to close or limit access to your account should you opt out of the crucial notices that are required to perform the Venti Service. You are still responsible for any amounts due to Venti even if we close or limit access to your account.

                                </p>
                                <p>5.2 SMS messaging <br>
                                You can opt out of receiving SMS messages by emailing admin@Venti.com. Alternatively, you may be able to indicate your preference by logging into your account and adjusting your preferences or by following the directions provided with the communication. Again, Venti reserves the right to close or limit access to your account should you opt out of the crucial notices that are required to perform the Venti Service. You are still responsible for any amounts due to Venti even if we close or limit access to your account.

                                </p>
                                <p>5.3 Access your information <br>
                                You can review and edit your personal information at any time by logging in to your account or by contacting us at admin@Venti.com. You can also request to close your account if you do not have an active loan by contacting us at admin@Venti.com. If you close your Venti account, we will mark your account in our database as "Closed," but will keep your account information in our database to comply with our legal obligations. This is necessary in order to deter fraud, by ensuring that persons who try to commit fraud will not be able to avoid detection simply by closing their account and opening a new account. If you close your account, your personally identifiable information will not be used by us for any further purposes, nor sold or shared with third parties, except as necessary to prevent fraud and assist law enforcement, as required by law or in accordance with this Privacy Policy.

                                </p>
                                <p>6. California consumers</p>
                                <p>
                                6.1 Your California privacy rights <br>
                                Persons with disabilities may obtain this notice in alternative format upon request by contacting us at admin@venti.co.
                                California Shine the Light: Residents of the State of California have the right to request information from Venti regarding other companies to whom the company has disclosed certain categories of information during the preceding year for the other companies' direct marketing purposes. If you are a California resident and would like to make such a request, please click HERE or email admin@venti.co.
                                California Consumer Privacy Act: The California Consumer Privacy Act ("CCPA") provides California residents with the right to receive certain disclosures regarding the collection, use, and sharing of "Personal Information," as well as the right to know/access, delete, and limit sharing of Personal Information. The CCPA defines "Personal Information" to mean "information that identifies, relates to, describes, is reasonably capable of being associated with, or could reasonably be linked, directly or indirectly, with a particular consumer or household." Certain information we collect may be exempt from the CCPA because it is considered public information (e.g., it is made available by a government entity) or covered by a specific federal privacy law, such as the Gramm–Leach–Bliley Act, the Health Insurance Portability and Accountability Act, or the Fair Credit Reporting Act.

                                </p>
                                <p>To the extent that we collect Personal Information that is subject to the CCPA, that information, our practices, and your rights are described below.

                                </p>
                                <p>Right to notice at collection regarding the categories of personal information collected

                                </p>
                                <p>You have the right to receive notice of the categories of Personal Information we collect, and the purposes for which those categories of Personal Information will be used. This notice should be provided at or before the time of collection. The categories we use to describe the information are those enumerated in the CCPA.

                                </p>
                                <p>Personal identifiers: <br>
                                We collect your name, phone number, and email address and contact address when you create an account or complete a transaction. If you choose to create an account, you will also be asked to create a username, and we will assign one or more unique identifiers to your profile. We use this information to provide the Services, respond to your requests, and send information and advertisements to you.
                                We collect payment information when you provide it to us, which may be your credit card number or a bank account, when you complete a transaction. You have the option to store this information to your account or set up a recurring transaction. We use this information to streamline and facilitate payments and transactions.
                                </p>
                                <p>
                                We may collect your Social Security number or Driver's License number. We use this information to identify you, authenticate and collate information about you, prevent fraud, and conduct background checks or other screening activities.
                                We collect your IP address automatically when you use our Services. We use this information to identify you, gauge online activity on our website, measure the effectiveness of online services, applications, and tools, and to serve targeted advertisements based on your online activities.
                            </p>
                            <p>
                                We collect your Device ID automatically when you use our Services. We use this information to monitor your use, and the effectiveness of, our Services, to identify you, and to provide you with targeted information and offers.
                            </p>
                            <p>
                                Protected classifications: <br>
                                We collect your age in order to comply with laws that restrict collection and disclosure of personal information belonging to minors.
                            </p>
                            <p>
                                Commercial information: <br>
                                When you engage in transactions with us, we create records of goods or services purchased or considered, as well as purchasing or consuming histories or tendencies. We use this information to measure the effectiveness of our Services and to provide you with targeted information, advertisements, and offers.
                            </p>
                            <p>
                                Biometric information: <br>
                                We collect information about your physiological, biological, and behavioral characteristics. We use this information to verify your identity.
                            </p>
                            <p>
                                Internet or other electronic network activity information: <br>
                                We collect information about your browsing history, search history, interaction with websites, and applications or advertisements automatically when you utilize our Services. We use this information to gauge online activity on our website, measure the effectiveness of online services, applications, and tools, and to serve targeted advertisements based on your online activities.
                            </p>
                            <p>
                                Geolocation data: <br>
                                As described above, we collect your IP address automatically when you use our Services. We may be able to determine your general location based on your device's IP address. When you use the Services for the first time, we may ask for your permission to collect your precise location (i.e., your GPS coordinates). If you allow your device to provide us with this information, we use it to make improvements to our products and services, and to provide recommendations and deliver relevant advertising.
                                Audio, electronic, visual, or similar information: If you contact us via phone, we may record the call. We will notify you if a call is being recorded at the beginning of the call. We may collect your photographic or video image, or similar information. We use this information to monitor our customer service, maintain the security of our systems and physical locations, and train employees.
                                Professional or employment-related information: We collect information about your current employer and your employment history. We use this information to conduct background and other screening activities, and to promote our services to others.
                                Inferences drawn to create a profile about a consumer reflecting the consumer's preferences or characteristics: We may analyze your actual or likely preferences through a series of computer processes. On some occasions, we may add our observations to your internal profile. We use this information to gauge and develop our marketing activities, measure the appeal and effectiveness of our Services, applications, and tools, and to provide you with targeted information, advertisements, and offers.
                                We may use any of the categories of information listed above for other business or operational purposes compatible with the context in which the Personal Information was collected.
                                </p>

                                <p>We may share any of the above-listed information with Service Providers, which are companies that we engage for business purposes to conduct activities on our behalf. Service Providers are restricted from using Personal Information for any purpose that is not related to our engagement. The categories of Service Providers with whom we share information and the services they provide are described in this Privacy Policy.</p>

                                <p>Right to know about personal information collected, disclosed, or sold</p>

                                <p>You have the right to request that we disclose to you the Personal Information we collect, use, disclose, or sell. In order to process your request to know/access your Personal Information or delete your Personal Information we may ask you to take additional steps to verify your request or identity.</p>

                                <p>Verification procedures</p>

                                <p>In order to process your request to know about or delete personal information we collect, disclose, or sell, we must verify your request. We do this by:</p>
                                
                                <p>Providing personal identifiers we can match against information we may have collected from you previously, and
                                Asking you to confirm your request using the email address or telephone account stated in the request.
                                If you have authorized someone else to make requests on your behalf, we will require that you provide notarized statements confirming the identity and authority of that person. Such notarized statements can be obtained by emailing admin@venti.co</p>

                                <p>Right to know/access information</p>

                                <p>You have the right to request access to Personal Information collected about you and information regarding the source of that information, the purposes for which we collect it, and the third parties and service providers with whom we share it. To protect our customers' Personal Information, we are required to verify your identify before we can act on your request.</p>

                                <p>Right to request deletion of information</p>

                                <p>You have the right to request in certain circumstances that we delete any Personal Information that we have collected directly from you. To protect our customers' Personal Information, we are required to verify your identity before we can act on your request. We may have a reason under the law why we do not have to comply with your request, or why we may comply with it in a more limited way than you anticipated. If we do, we will explain that to you in our response.</p>

                                <p>Right to information regarding participation in data sharing for financial incentives</p>

                                <p>You have the right to be free from discrimination based on your exercise of your CCPA rights. We may run promotions from time to time wherein we incentivize a consumer to share certain pieces of information with us. Participation in these incentives is voluntary, and you may opt out of the data sharing at any time.</p>

                                <p>Right to opt out of sale of personal information to third parties</p>

                                <p>Though Venti does not sell Personal Information to third parties, California law requires that we maintain a separate webpage that allows you to opt out of the sale of your Personal Information in the future, which can be accessed by visiting our <a href="/privacy/do-not-sell">Do not sell my info"</a> webpage or by emailing us at admin@venti.co</p>

                                <p>Please note that your right to opt out does not apply to our sharing of Personal Information with service providers, who are parties we engage to perform a function on our behalf and are contractually obligated to use the Personal Information only for that function.</p>

                                <p>We may also disclose information to other entities when required by law or to protect Venti or other persons, as described in our Privacy Policy.</p>

                                <p>How to submit a request</p>

                                <p>You may submit a request to exercise your rights through the following means:</p>

                                <p>By visiting www.venti.com/privacy, where you can request and download specific pieces of information we have collected. By signing in to your account to submit the request, you will be able to automatically verify your identity, which will result in faster processing of your request.
                                By calling us at (202) 683-1017.
                            </p>
                                <p>Authorized agent</p>

                                <p>You may authorize another individual or a business registered with the California Secretary of State, called an authorized agent, to make requests on your behalf. We require that you and the individual complete notarized affidavits in order to verify the identity of the authorized agent and confirm that you have authorized them to act on your behalf.</p>

                                <p>7. Nevada residents</p>
                                
                                <p>
                                7.1 Special information for Nevada residents
                                Residents of the State of Nevada have the right to opt out of the sale of certain pieces of their information to other companies who will sell or license their information to others. Venti does not sell the Personal Information of its customers. However, if you are a Nevada resident and would like to make such a request, please email admin@venti.co.
                                </p>

                                <p>8. Contact Venti</p>
                                <p>8.1 Contact Venti
                                If you have questions or concerns regarding this Privacy Policy, you should contact us at adming@venti.com.</p>

                                <br>
                                <p>Revised. 1/2022</p>

                                <p>Please note:
                                If you are a new customer, we can begin sharing your information 30 days from the date we sent this notice, or earlier if you consent or as permitted by law. When you are no longer our customer, we continue to share your information as described in this notice.
                                However, you can contact us at any time to limit our sharing.
                                </p>

                                <p>For California Residents. We will not share personal information with affiliates or nonaffiliates except as permitted by California law, such as to process your transaction or with your consent.

                                </p>
                                <p>For Vermont Residents. We will not share personal information with affiliates or nonaffiliates except as permitted by Vermont law, such as to process your transaction or with your consent.

                                </p>
                                <p>
                                    <strong>Special Notice For State Residents</strong>
                                </p>
                                <p>For Alaska, Illinois, Maryland and North Dakota Customers. We will not share personal information with nonaffiliates either for them to market to you or for joint marketing-without your authorization.
                                </p>
                                <p>For California Customers. We will not share personal information with nonaffiliates either for them to market to you or for joint marketing-without your authorization. We will also limit our sharing of personal information about you with our affiliates to comply with all California privacy laws that apply to us.
                                </p>
                                <p>For Massachusetts, Mississippi and New Jersey Customers. We will not share personal information from deposit or share relationships with nonaffiliates either for them to market to you or for joint marketing-without your authorization.
                                </p>
                                <p>For Vermont Customers. We will not disclose information about your creditworthiness to our affiliates and will not disclose your personal information, financial information, credit report, or health information to nonaffiliated third parties to market to you, other than as permitted by Vermont law, unless you authorize us to make those disclosures.
                                </p>           
                    </div>
                </div>
            </div> 
        </div>
    </div>
</section>
@endsection