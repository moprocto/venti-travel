@extends('layouts.curator-master')

@php
	$hotel["room_count"] = $hotel["rooms"];
	$photos = $hotel["accommodation"]["photos"];
	$rooms = $hotel["accommodation"]["rooms"];

	$coordinates = $hotel["accommodation"]["location"]["geographic_coordinates"];
	
	$address = $hotel["accommodation"]["location"]["address"];

	$addressString = "";

	foreach($address as $key => $value){
		$addressString .= $value . " ";
	}

	$amenities = $hotel["accommodation"]["amenities"] ?? [];

	$numChildren = 0;

	$guests = $hotel["guests"];

	foreach($guests as $guest){
		if($guest["type"] == "child"){
			$numChildren++;
		}
	}
@endphp

@section('css')
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper@11/swiper-bundle.min.css" />
	<style type="text/css">
	.form-small{
		padding: 0;
		font-size: 12px;
	}
	.no-bg{
		background-color: transparent;
		border: none;
	}
	.datepicker{
		max-width: 170px;
		font-size:12px;
		height:38px;
	}
	.btn-primary{
		border-radius: 4px;
	}
	.swiper {
		width: 100%;
		height: 300px;
		margin-left: auto;
		margin-right: auto;
	}
    .swiper-slide {
		background-size: contain;
		background-position: center;
    }

    .mySwiper2 {
      height: 80%;
      width: 100%;
      max-height: 500px;
    }

    .mySwiper {
      height: 20%;
      box-sizing: border-box;
      padding: 10px 0;
    }

    .mySwiper .swiper-slide {
      width: 25%;
      height: 100%;
      max-height: 100px;
      opacity: 0.4;
    }

    .mySwiper .swiper-slide-thumb-active {
      opacity: 1;
    }

    .swiper-slide img {
      display: block;
      width: 100%;
      height: 100%;
      object-fit: cover;
    }
	td.vertical-align{
		vertical-align: middle;
	}
	tr.borderless td{
		border-bottom: none !important;
	}
	.table,.card-body{
		padding-bottom: 0;
		margin: 0;
	}
	ul.nolist{
		list-style: none;
	}
	.text-small{
		font-size: 12px;
	}
	.results-table .text-left{
		font-size: 13px;
		font-weight: 700;
	}
	.results-table .text-right{
		font-size: 13px;
	}
	table h5, table h2, table h3{
		padding: 0;
		margin: 0 !important;
	}
	.table tr td{
		vertical-align: middle;
	}
	.card{
		background: transparent;
	}
	.bg-white{
		background-color: transparent;
		background-image: linear-gradient(112.1deg, rgba(248, 248, 248,0) 11.4%, rgba(255, 255, 255, 0.7) 70.2%);
	}
	.row_total td{
		border-bottom: none;
	}
	.bg-success > *, .bg-success .card-title{
		color: white;
	}
	.bg-white{
		cursor: pointer;
	}
	li strong{
		font-size: 13px;
	}
	.card-title .fa-check, .hide{
		display: none;
	}
	h5.card-title.selected .fa-check{
		display: inline-block;
		float: right;
	}
	
	.text-bold{
		font-weight: 500;
	}
	#pac-input{
		top:8px !important;
		max-width:400px !important;
	}

	@media(max-width: 768px){
		.fa-star{
			font-size: 13px;
		}
		.mwidth{
			display: block !important;
		}
	}
	.rate-details{
		list-style: none;
		padding: 0;
	}
	.rate-details li{
		padding: 5px 10px;
		border-bottom: 1px solid rgba(0,0,0,0.1);
	}
	.bg-super{
		    background-image: linear-gradient(112.1deg, rgba(242, 242, 246,0) 11.4%, rgba(255, 255, 255, 0.4) 70.2%);
    background-color: rgba(50, 50, 20, 0.1);

	}
	</style>
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/lightgallery.js/dist/css/lightgallery.min.css" />

@endsection

@section('content')
<div class="gradient-wrap" style="background: transparent; width:100%;">
    <!--  HERO -->
    <div class="section-wrapper black position-relative">
    	<div class="row">
    		<div class="col-md-9">
    			<div class="row mb-4">
    				<div class="col-md-12">
		    			<div class="card bg-white">
		    				<div class="card-body">
								<div style="--swiper-navigation-color: #fff; --swiper-pagination-color: #fff" class="swiper mySwiper2">
									<div class="swiper-wrapper" id="lg-swipper">
										@foreach($photos as $photo)
											<a data-lg-size="1600-1200" href="{{ $photo['url'] }}" class="swiper-slide"><img src="{{ $photo['url'] }}" /></a>
										@endforeach
									</div>
									<div class="swiper-button-next"></div>
									<div class="swiper-button-prev"></div>
								</div>
								<div thumbsSlider="" class="swiper mySwiper">
									<div class="swiper-wrapper">
										@foreach($photos as $photo)
											<div class="swiper-slide">
												<img src="{{ $photo['url'] }}" />
											</div>
										@endforeach
									</div>
								</div>
								<p></p>
		    				</div>
		    			</div>
	    			</div>
	    		</div>
	    		<div class="row mb-4">
	    			<div class="col-md-12">
		    			<div class="card bg-white">
		    				<div class="card-body">
		    					<div class="d-flex mwidth" style="flex-direction: row; justify-content:space-between;">
		    							<h2 style="font-size:23px">{{ $hotel["accommodation"]["name"] ?? "" }}
		    								<span class="d-block mt-2" style="font-size:16px;">
		    									Check In: {{ \Carbon\Carbon::parse($hotel["check_in_date"])->format("F j, Y") }}
		    									<br>
		    									Check Out: {{ \Carbon\Carbon::parse($hotel["check_out_date"])->format("F j, Y") }}
		    									<br>
		    									Adults: {{ $hotel["adults"] }}
		    									@if($numChildren > 0)
		    										<br>
		    										Children: {{ $numChildren }}
		    									@endif
		    									<br>
		    									Rooms: {{ $hotel["rooms"] }}
		    								</span>
		    							</h2>
			    						<h3 class="text-center mwidth">@for($i = 1; $i <= $hotel["accommodation"]["rating"]; $i++)
											<i class="fa fa-star text-warning"></i>
										@endfor<span class="d-block" style="font-size: 12px;margin-top: 0px; font-weight:bold;">Average Rating: {{ $hotel["accommodation"]["review_score"] ?? "" }}</span></h3>
		    					</div>
								<p>{{ $hotel["accommodation"]["description"] ?? "" }}</p>
								@if($amenities)
									<h5>Amenities</h5>
									<div style="display:flex; flex-wrap: wrap;">
										@foreach($amenities as $amenity)
											<div class="d-flex" style="margin: 5px; align-items:center; background-color:whitesmoke; padding: 10px 20px; border-radius: 20px;">
												<img src="/assets/img/icons/hotel/{{ $amenity['type'] }}.png" style="width:30px; margin-right:10px;">
												<span class="text-bold">{{ $amenity["description"] }}</span>
											</div>
										@endforeach
									</div>
									<br>
								@endif
		    				</div>
		    			</div>
	    			</div>
	    		</div>
    			<h4>Hotel Location</h4>
    			<div class="d-flex mb-4" style="flex-direction:row; justify-content:space-between; align-items:center;">
    				<span class="d-block mb-1" style="font-size:18px;"><i class="fa fa-location-dot"></i> {{ $addressString }}</span>
    				<a href="https://maps.google.com/?q={{ $addressString }}" target="_blank" class="btn btn-black">OPEN MAP</a>
    			</div>
    			
    			<div class="row mb-4">
    				<div class="card bg-white" style="width:100%;">
    					<div class="card-body" style="width:100%;">
    						<input id="pac-input" class="controls form-control" type="text" placeholder="Search Box" >

    						<div id="map" style="height:400px; width:100%; border-radius: 10px; margin-bottom:15px;"></div>
    						<h5 id="distance" class="mt-4 mb-4"></h5>
    					</div>
    				</div>
    			</div>
    			<h4>Available Offers</h4>
    			<p>Prices may differ from the search results page as we receive updated inventory from the hotel reservation system.</p>
    			<div class="row">
    				<!--
    				<div class="col-md-3">

    					<select class="form-control text-bold mb-3">
    						<option>SORT BY</option>
    						<option>PRICE</option>
    						<option>TOTAL SAVINGS</option>
    					</select>

    					<div class="card bg-white">
    						<div class="card-body">
    							<h5 style="font-size: 18px;">BED TYPES</h5>
    							<ul style="list-style:none; padding: 0; margin-bottom:10px">
    								<li><input type="checkbox"> KING</li>
    								<li><input type="checkbox"> QUEEN</li>
    								<li><input type="checkbox"> SINGLE</li>
    							</ul>
    						</div>
    					</div>

    				</div>
    			-->
    				<div class="col-md-12">
    					@foreach($rooms as $room)

    						@php $room["pricing"] = true; @endphp
		    				@include('hotels.components.room', [
		    				"room" => $room,
		    				"selectable" => true
		    				])
		    			@endforeach
    				</div>
	    		</div>
    		</div>
    		<div class="col-md-3">
    		</div>
    	</div>
	</div>
</div>

@include('hotels.components.resort-fee-modal')
@endsection

@section('js')
	<script async defer src="https://maps.googleapis.com/maps/api/js?key={{ env('GOOGLE_MAPS_KEY') }}&libraries=places&callback=initMap"></script>
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
	<script src="https://cdn.jsdelivr.net/npm/swiper@11/swiper-bundle.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/lightgallery.js/dist/js/lightgallery.min.js"></script>
	<script>

		setTimeout(initMap, 1000);

		function initMap() {
		    var initialLocation = {lat: {{ $coordinates['latitude'] }}, lng: {{ $coordinates['longitude'] }}}; // Your initial location // Replace with your lat/long
				var map = new google.maps.Map(document.getElementById('map'), {
		        zoom: 15,
		        center: initialLocation
		    });

		    // Directions service and renderer for displaying the route
		    var directionsService = new google.maps.DirectionsService();
		    var directionsRenderer = new google.maps.DirectionsRenderer();
		    directionsRenderer.setMap(map);

		    const icon = {
			    url: "https://venti.co/assets/img/map-marker.png", // url
			    scaledSize: new google.maps.Size(40, 40), // scaled size
			    origin: new google.maps.Point(0,0), // origin
			    anchor: new google.maps.Point(0, 0) // anchor
				};

		    // Red marker for the initial location
		    var redMarker = new google.maps.Marker({
		        position: initialLocation,
		        map: map,
		        icon: icon
		    });

		    // Create the search box and link it to the UI element
		    var input = document.getElementById('pac-input');
		    map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
		    var searchBox = new google.maps.places.SearchBox(input);

		    // Bias the search box results towards current map's viewport
		    map.addListener('bounds_changed', function() {
		        searchBox.setBounds(map.getBounds());
		    });

		    // Listen for the event fired when the user selects a prediction from the search box
		    searchBox.addListener('places_changed', function() {
		        var places = searchBox.getPlaces();

		        if (places.length == 0) {
		            return;
		        }

		        var place = places[0];
		        if (!place.geometry) {
		            console.log("Selected place contains no geometry");
		            return;
		        }

		        // Update the search box to show only the place name
		        input.value = place.name;

		        // Specify the start and end locations for the directions
		        var start = place.geometry.location;
		        var end = initialLocation;

		        // Request for the directions
		        var request = {
		            origin: start,
		            destination: end,
		            travelMode: 'DRIVING'
		        };

		        // Retrieve the directions and estimate driving distance
		        directionsService.route(request, function(response, status) {
		            if (status === 'OK') {
		                // Display the route on the map
		                directionsRenderer.setDirections(response);

		                // Get the driving distance and time estimate and display them
		                var distance = response.routes[0].legs[0].distance.text;
		                var duration = response.routes[0].legs[0].duration.text;
		                $("#distance").text('Estimated driving distance: ' + distance + '. Estimated driving time: ' + duration + '.')

		            } else {
		                window.alert('Directions request failed due to ' + status);
		            }
		        });
		    });
		}

		// Make sure the global variable markers is defined outside of the initMap function
		var markers = [];

				
		let $lgSwiper = document.getElementById('lg-swipper');

		var swiper = new Swiper(".mySwiper", {
		  spaceBetween: 10,
		  slidesPerView: 4,
		  freeMode: true,
		  watchSlidesProgress: true,
		});
		var swiper2 = new Swiper(".mySwiper2", {
		  spaceBetween: 10,
		  navigation: {
		    nextEl: ".swiper-button-next",
		    prevEl: ".swiper-button-prev",
		  },
		  thumbs: {
		    swiper: swiper,
		  },
		  on: {
		        init: function () {
		            const lg = lightGallery($lgSwiper);
		            // Before closing lightGallery, make sure swiper slide
		            // is aligned with the lightGallery active slide
		            $lgSwiper.addEventListener('lgBeforeClose', () => {
		                swiper.slideTo(lg.index, 0)
		            });
		        },
		    }
		});

		document.addEventListener('DOMContentLoaded', function() {
		    var lightboxGallery = document.querySelector('.mySwiper2'); // Your main gallery container
		    lightGallery(lightboxGallery, {
		        selector: 'swiper-slide:not(.swiper-slide-duplicate) span', // Target only non-duplicate slides with <a> tags
		        download: false // Disable downloading by default
		    });
		});

		$(".resort-fee").click(function(){
			$("#resortFeeModal").modal("show");
		});


		document.addEventListener('DOMContentLoaded', function() {
  let galleries = document.querySelectorAll('.lightgallery');
  galleries.forEach(function(gallery) {
    lightGallery(gallery);
  });
});
  </script>
@endsection

