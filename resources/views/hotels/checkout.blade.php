@extends('layouts.curator-master')

@php
	$address = $hotel["location"]["address"];
	$conditions = [];

	$addressString = "";
	
	foreach($address as $key => $value){
		$addressString .= $value . " ";
	}

	$hotel["room_count"] = $quote["rooms"];

	$total = convertCurrency($quote["total_amount"], $quote["total_currency"]);

	$orderUrl = "/hotels/search/result/" . $orderID . "/checkout/" . $quote["id"];

	$subscription = $user->customClaims["subscription"] ?? "buddy";

	$numChildren = 0;

	$guests = $quote["guests"];

	foreach($guests as $guest){
		if($guest["type"] == "child"){
			$numChildren++;
		}
	}

@endphp
@section('css')
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/switchery/0.8.2/switchery.min.css" integrity="sha512-uyGg6dZr3cE1PxtKOCGqKGTiZybe5iSq3LsqOolABqAWlIRLo/HKyrMMD8drX+gls3twJdpYX0gDKEdtf2dpmw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
	<link rel="stylesheet" type="text/css" href="/assets/css/steps.css">
	<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
	<link rel="stylesheet" href="/assets/css/intlTelInput.min.css">
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/lightgallery.js/dist/css/lightgallery.min.css" />
	<style type="text/css">
		.form-label{
			font-weight: 500;
		}
		.wizard>.content>.body{
            height: 100%;
            position: relative;
            padding: 0;
        }
        .wizard>.content{
            min-height: 120px;
            background: none;
        }
		.form-small{
			padding: 0;
			font-size: 12px;
		}
		.no-bg{
			background-color: transparent;
			border: none;
		}
		.datepicker{
			max-width: 170px;
			font-size:12px;
			height:38px;
		}
		.btn-primary{
			border-radius: 4px;
		}
		.swiper {
			width: 100%;
			height: 300px;
			margin-left: auto;
			margin-right: auto;
		}
	    .swiper-slide {
			background-size: contain;
			background-position: center;
	    }

	    .mySwiper2 {
	      height: 80%;
	      width: 100%;
	      max-height: 500px;
	    }

	    .mySwiper {
	      height: 20%;
	      box-sizing: border-box;
	      padding: 10px 0;
	    }

	    .mySwiper .swiper-slide {
	      width: 25%;
	      height: 100%;
	      max-height: 100px;
	      opacity: 0.4;
	    }

	    .mySwiper .swiper-slide-thumb-active {
	      opacity: 1;
	    }

	    .swiper-slide img {
	      display: block;
	      width: 100%;
	      height: 100%;
	      object-fit: cover;
	    }
		td.vertical-align{
			vertical-align: middle;
		}
		tr.borderless td{
			border-bottom: none !important;
		}
		.table,.card-body{
			padding-bottom: 0;
			margin: 0;
		}
		ul.nolist{
			list-style: none;
		}
		.text-small{
			font-size: 12px;
		}
		.results-table .text-left{
			font-size: 13px;
			font-weight: 700;
		}
		.results-table .text-right{
			font-size: 13px;
		}
		table h5, table h2, table h3{
			padding: 0;
			margin: 0 !important;
		}
		.table tr td{
			vertical-align: middle;
		}
		.card{
			background: transparent;
		}
		.bg-white{
			background-color: transparent;
			background-image: linear-gradient(112.1deg, rgba(248, 248, 248,0) 11.4%, rgba(255, 255, 255, 0.7) 70.2%);
		}
		.row_total td{
			border-bottom: none;
		}
		.bg-success > *, .bg-success .card-title{
			color: white;
		}
		.bg-white{
			cursor: pointer;
		}
		li strong{
			font-size: 13px;
		}
		.card-title .fa-check, .hide, #ccFeeDisclosure{
			display: none;
		}
		h5.card-title.selected .fa-check{
			display: inline-block;
			float: right;
		}

		.text-bold{
			font-weight: 500;
		}
		#pac-input{
			top:8px !important;
			max-width:400px !important;
		}

		@media(max-width: 768px){
			.fa-star{
				font-size: 13px;
			}
			.mwidth{
				display: block !important;
			}
		}
		.reservation-info strong{
			font-size: 16px;
		}
		h6{
			font-weight: 600;
		}
		.wizard>.content>.body ul.rate-details{
			list-style: none !important;
			padding: 0 !important;
		}
		.wizard>.content>.body ul.rate-details li{
			padding: 5px 10px;
			border-bottom: 1px solid rgba(0,0,0,0.1);
		}
		.wizard>.content>.body .invalid-feedback{
		  	border: 1px solid #dc3545 !important;
		}
		.bg-super{
		    background-image: linear-gradient(112.1deg, rgba(242, 242, 246,0) 11.4%, rgba(255, 255, 255, 0.4) 70.2%);
    		background-color: rgba(50, 50, 20, 0.1);
		}
	</style>
@endsection

@section('content')
<div class="gradient-wrap" style="background: transparent; width:100%;">
    <!--  HERO -->
    <div class="section-wrapper black position-relative">
    	<div class="col-md-10 offset-md-1">
    		<div class="row">
    		<div class="col-md-12 col-md-offset-1 justify-center">
    			<div class="d-flex" style="justify-content:space-between; flex-direction:row">
    				<h4>Hotel Offer <span class="text-small" id="offer-expiry"></span></h4>
    			</div>
    		</div>
    		<div class="col-md-12">
				<div id="checkout-wizard">
    				<h3>Booking Summary</h3>
    				@if(env('DUFFEL_BOOKABLE_LIMIT') >= $total)
	    				<section>
	    					<h5>Reservation Information</h5>
	    					<div class="card bg-white mb-4">
								<div class="card-body">
									<ul class="reservation-info" style="padding-left:12px !important; list-style:none !important; padding-bottom:10px;">
										<li><strong>Hotel:</strong> {{ $hotel["name"] }}</li>
										<li><strong>Address:</strong> {{ $addressString }}</li>
										<li><strong>Check In:</strong> {{ \Carbon\Carbon::parse($quote["check_in_date"])->format("F j, Y") }}</li>
										<li><strong>Check Out:</strong> {{ \Carbon\Carbon::parse($quote["check_out_date"])->format("F j, Y") }}</li>
										<li><strong>Adults:</strong> {{ $quote["adults"] }}</li>
			    									@if($numChildren > 0)
			    										<li><strong>Children:</strong> {{ $numChildren }} </li>
			    									@endif
										<li><strong>Rooms:</strong> {{ $quote["rooms"] }}</li>
										@if($hotel["check_in_information"] != null)
											<li><strong>Check in available after:</strong> {{ $hotel["check_in_information"]["check_in_after_time"] }}</li>
											<li><strong>Check out before:</strong> {{ $hotel["check_in_information"]["check_out_before_time"] }}</li>
										@else
											<li>Check in/Check out information not provided by hotel at this time. Check in is typically after 3 p.m. local time and check out is before 11:30 a.m. local time. Always contact the hotel to confirm.</li>
										@endif
									</ul>
	    						</div>
	    					</div>
	    					<h5>Your Room(s)</h5>
	    					@foreach($hotel["rooms"] as $room)
	    						@php
	    							$room["pricing"] = false;
	    							$rates = $room["rates"];
	    							$conditionsString = "";
	    							$cancelationTimelines = [];
	    							foreach($rates as $rate){
	    								
	    								$cancelationTimelines = array_merge($cancelationTimelines, $rate["cancellation_timeline"]);

	    								foreach($rate["conditions"] as $condition){
	    									$conditionsString .= "<h6>" . $condition["title"] . "</h6><p>" . $condition["description"] . "</p>";
	    								}
	    							}
	    						@endphp
	    						<div class="row">
	    							<div class="col-md-12">
			    						@include('hotels.components.room', [
			    				"room" => $room,
			    				"selectable" => false
			    				])
			    					</div>
			    				</div>
	    					@endforeach
	    					@if($user)
	    					<div class="card bg-white mb-3">
	    						<div class="card-body" id="guestList">
	    							<h4>Contact Info</h4>
	    							<span class="d-block text-small mb-3">The following information will be shared with the accommodation to complete your booking.</span>
	    							<div class="row mb-3">
		    							<div class="form-group col-md-6">
		    								<label class="form-label">Email:</label>
		    								<input type="text" class="form-control disabled" disabled="" value="{{ $user->email }}">
		    							</div>
		    							<div class="form-group col-md-6">
		    								<label class="form-label">Phone Number:</label>
		    								<input type="text" class="form-control" value="{{ $user->customClaims['phoneNumber'] ?? '' }}">
		    							</div>
	    							</div>

	    							<h4>Guest List</h4>
	    							<p>To reserve this hotel, you must provide the names of each guest that will be traveling.</p>


	    							@for($zz = 0; $zz < sizeof($quote["guests"]); $zz++)
	    								@php $p = $zz + 1; @endphp
	    								<h5>Guest {{ $p }} | {{ ucfirst($quote["guests"][$zz]["type"]) }}</h5>
	    								@if($p == 1 && $subscription != "first")
											<div class="row mb-3">
												<div class="col-md-12">
													<div class="alert alert-info">
														<span>Your subscription requires your name to be on each booking. To book on behalf of others, please upgrade to a paid subscription by contacting admin@venti.co. Your name must match your legal documents for flying. If there is an error, please contact us via email before booking. You are responsible for making sure your name is accurate.</span>
													</div>
												</div>
											</div>
										@endif
	    								<div class="row mb-3">
	    									<div class="col-md-6">
	    										<div class="form-group">
	    											<label>First Name</label>
	    											<input type="text" id="fname_{{ $p }}"
	    											class="form-control required @if($subscription != 'first') disabled @endif" name=""
	    											@if($p == 1)
	    												value="{{ getFirstName($user->displayName) }}"
	    											@endif
	    											@if($subscription != "first")
	    												disabled="" 
	    											@endif
	    											>
	    										</div>
	    									</div>
	    									<div class="col-md-6">
	    										<div class="form-group">
	    											<label>Last Name</label>
	    											<input type="text" id="lname_{{ $p }}"
	    											class="form-control required @if($subscription != 'first') disabled @endif" name=""
	    											@if($p == 1)
	    												value="{{ getLastName($user->displayName) }}"
	    											@endif
	    											@if($subscription != "first")
	    												disabled="" 
	    											@endif
	    											>
	    										</div>
	    									</div>
	    								</div>
	    							@endfor

	    							<div class="row mb-3">
	    								<div class="col-md-12">
			    							<h4>Special Requests</h4>
			    							<span class="d-block text-small mb-3">Guest's specific requests about this booking to be forwarded to the accommodation. Example: "2:00 PM early check-in required". Venti does not guarantee the accommodation will honor special requests. We recommend you call the hotel provider in advance to booking to confirm if certain requests can be accommodated.<br><strong>This field should not be used to provide medical or otherwise sensitive information.</strong></span>
			    							<textarea class="form-control" name="requests" id="requests"></textarea>
		    							</div>
		    						</div>

		    						@if($quote["supported_loyalty_programme"])
			    						<div class="row mb-3">
		    								<div class="col-md-12">
				    							<h4>Loyalty Program #</h4>
				    							<span class="d-block text-small">Loyalty programme account number to associate with this booking. Use this only when the quote has a supported_loyalty_programme indicated. Otherwise, this will result into an error. Account number is relayed to the hotel on a best-effort basis, it is at the hotel's discretion to honour the loyalty programme.</span>
				    							<input type="text" class="form-control" id="programme" name="">
			    							</div>
			    						</div>
		    						@endif
		    						<br>
	    					</div>
	    					@else
	    						<div class="card bg-white mb-3">
	    							<div class="card-body">
	    								<h3>Must be logged in to continue</h3>
	    								<p>You need a <a href="/boardingpass" target="_blank">Boarding Pass</a> to purchase this hotel. Members can save up to 100% on airfare, hotels, and more.</p>
	    							</div>
	    						</div>
	    						<style type="text/css">
	    							#checkout-wizard .actions{
	    								display: none !important;
	    							}
	    						</style>
	    					@endif
	    				</section>
    				@else
    					@php 
    						$cancelationTimelines = []; 
    						$conditionsString = "";
    					@endphp
    					<section>
    						<h4>Thank you for your interest in booking a Venti hotel.</h4>
    						<p>Please contact support via admin@venti.co to proceed with booking.</p>
    					</section>
    				@endif
    				<h3>Terms and Conditions</h3>
    				<section>
    					<h5>Terms and Conditions</h5>
    					<div class="card bg-white mb-3">
    						<div class="card-body">
    							<h4>Cancelation</h4>
    							<ul style="padding-left:12px !important; list-style:none !important; padding-bottom:10px;">
	    							@foreach($cancelationTimelines as $cancelationTimeline)
	    								<li>${{ number_format(convertCurrency($cancelationTimeline["refund_amount"],$cancelationTimeline["currency"]),2) }} refunded if canceled before {{ Carbon\Carbon::parse($cancelationTimeline["before"])->format("F j, Y @ h:i A") }}</li>
	    							@endforeach
    							</ul>
    							{!! $conditionsString !!}
    						</div>
    					</div>
    					<div class="card bg-white mb-3">
    						<div class="card-body">
    							<p>By advancing to the next step, you acknowledge to have read and agree to the Terms and Conditions outlined with this reservation. For questions regarding fees and the availability of certain amenities/accommodations and rooms, please contact the hotel directly.</p>
    						</div>
    					</div>
    				</section>
    				<h3>Order Summary</h3>
    				<section>
    					@if(env('DUFFEL_BOOKABLE_LIMIT') >= $total)
    						@include('hotels.components.checkout-step')
    					@else
    						<p>Please contact support via admin@venti.co to proceed with booking.</p>
    					@endif
    				</section>
				</div>
    		</div>
    		</div>
    	</div>
    </div>
</div>

@include('hotels.components.resort-fee-modal')
@include('hotels.components.loading')

@endsection

@section('js')
<script type="text/javascript" src="/assets/js/jquery.steps.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/switchery/0.8.2/switchery.min.js" integrity="sha512-lC8vSUSlXWqh7A/F+EUS3l77bdlj+rGMN4NB5XFAHnTR3jQtg4ibZccWpuSSIdPoPUlUxtnGktLyrWcDhG8RvA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script type="text/javascript" src="/assets/js/flights.js"></script>
<script src="https://cdn.jsdelivr.net/npm/intl-tel-input@18.2.1/build/js/intlTelInput.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/lightgallery.js/dist/js/lightgallery.min.js"></script>
<script type="text/javascript">
	$.ajaxSetup({
		headers: {
	    	'X-CSRF-TOKEN': '{{ csrf_token() }}'
		}
    });

    var wizardCheckout = $("#checkout-wizard").steps({
		headerTag: "h3",
	    bodyTag: "section",
	    transitionEffect: "slideLeft",
	    onStepChanging: function(event, currentIndex, priorIndex){
	    	if(currentIndex == 0){
	    		var passable = checkRequiredFields("#guestList")
	    		if(passable){
	    			return true;
	    		}
	    		return false;
	    	}
	    	return true;
	    }
	});

	$(".resort-fee").click(function(){
		$("#resortFeeModal").modal("show");
	});

	@if(Session::get('user') !== null)

	updateBalanceDetails();

	@endif

	function updateBalanceDetails(){
		@if(Session::get('user') !== null)
			$.ajax({
	            url: "{{ route('home-customer-details') }}",
				type: 'POST',
				async: true,
				processData: true,
		        dataType: 'json',
	            data:{
	                wallet : "{{ $wallet }}",
	                user: "{{ $user->uid }}"
	            },
	            success: function (data) {
	            	$("#spending-power-balance").html(data.spendingPowerHTML);
            		$("#cash-balance").html(data.cashHTML);
            		$("#points-balance").html(data.pointsHTML);
            		checkMaxPoints();
	            }
	       });
		@endif
	}

	function checkMaxPoints(){
		currentInput = parseFloat($("#maxPointsInput").val());
		var maxPoints = parseFloat($(".pointsFinal:eq(1)").text());

		if($("#total-discount").val() == currentInput){
			$("#maxPointsInput").val(maxPoints);
		}
		
		$("#maxPointsInput").attr("max", maxPoints);
		calculateSuperTotal();

		$(".maxPoints").each(function(){
			$(this).text(parseFloat($(".maxPoints:eq(0)").text()).toLocaleString(undefined, {
				maximumFractionDigits: 2,
				minimumFractionDigits: 2
			}));
		});
	}

	function calculateSuperTotal(){
		var baseTotal = parseFloat($("#newTotal").text().replace(',', ''));

		var pointsApplied = parseFloat($("#maxPointsInput").val());

		if(isNaN(pointsApplied)){
			pointsApplied = 0;
		}

		var superTotal = baseTotal - pointsApplied;

		$("#pointsApplied").text(pointsApplied.toLocaleString(undefined, {minimumFractionDigits: 2,maximumFractionDigits:2}));

		var deductedAmount = superTotal;

		$(".superTotal").each(function(){
			$(this).text(superTotal.toLocaleString(undefined, {
				minimumFractionDigits: 2,
				maximumFractionDigits: 2
			}))
		});

		if($("#paymentPrefSelected").val() != "pass"){
			// cc selected
			$("#ccFeeDisclosure").show();
			var ccFee = deductedAmount * 0.03;
			$("#ccFee").text(ccFee.toLocaleString(undefined, {minimumFractionDigits: 2,maximumFractionDigits:2}));
			deductedAmount += ccFee;
			$("#finalOffer").text(deductedAmount.toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits:2}));
		} else {
			$("#ccFeeDisclosure").hide();
		}

		$("#deductedAmount").text(deductedAmount.toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits:2}));
	}

	function checkRequiredFields(target){
		var passable = true;
		$(target + " .required").each(function(){
			var field = $(this);
			var fieldValue = $(this).val();
			field.removeClass("invalid-feedback");

			if(fieldValue == "" || fieldValue == undefined){
				field.addClass("invalid-feedback");
				passable = false;
			}
		});

		if(!passable){
			swal.fire({
				icon: 'warning',
				title: 'Missing required fields',
				text: 'To continue with checkout, please complete all fields, including the list of guests.',
				confirmButtonText: 'Ok'
			});
		}

		return passable;
	}

	$("#finalizePurchase").click(function(){
		var btn = $(this);
		btn.addClass("disable-while-loading");

		var passable = true;    

		

		var adults = parseInt({{ sizeof($quote["guests"]) }});
		var quoteID = "{{ $quote['id'] }}";
		var accommodation_special_requests = $("#requests").val();
		var programme = $("#programme").val();
		var pointsApplied = $("#maxPointsInput").val();

		var guests = [];

		for(i = 1; i <= adults; i++){
			guests.push({
				"given_name" : $("#fname_" + i).val(),
				"family_name": $("#lname_" + i).val()
			});
		}

		var paymentPref = $("#paymentPrefSelected").val();

		    	

    	if(paymentPref != "pass"){
    		// if CC, calculate amount due via payment intent
    		if(paymentPref.length < 9){
    			btn.removeClass("disable-while-loading");
    			swal.fire({
					icon: 'warning',
					title: 'You missed a step',
					text: "Please select a valid payment method",
					confirmButtonText: 'Ok'
				});
				return false;
    		}
    	}

    	$("#loadingModal").modal("show");

		$.ajax({
            url: "{{ $orderUrl }}",
			type: 'POST',
			async: true,
			processData: true,
	        dataType: 'json',
            data:{
                guests: guests,
                order: "{{ $orderID }}",
                rate: "{{ $rateID }}",
                quote: quoteID,
                programme: programme,
                accommodation_special_requests: accommodation_special_requests,
                pointsApplied: pointsApplied,
                paymentPref: paymentPref
            },
            success: function (data) {
            	$("#loadingModal").modal("hide");

            	if(data.error != undefined){
            		// we found an error in the order
            		btn.removeClass("disable-while-loading");

            		swal.fire({
						icon: 'error',
						title: 'Your order could not be processed',
						text: data.message,
						confirmButtonText: 'Ok'
					});

        			return false;
            	}
            	if(data.success != undefined){
            		swal.fire({
						icon: 'success',
						title: 'Your order was processed successfully',
						text: data.message,
						confirmButtonText: 'Ok'
					});

            		setTimeout(function(){
            			window.location.href = "/trips/" + data.order_id;
            		}, 10000);
        			// redirect user to order confirmation page
            	}
            },
            error: function(e){
            	btn.removeClass("disable-while-loading");

            	swal.fire({
					icon: 'error',
					title: 'Your order could not be processed',
					text: e.message,
					confirmButtonText: 'Ok'
				});

				$("#loadingModal").modal("hide");
            }
       });
	});

	$(".maxPointsButton").click(function(){
		var maxPoints = parseFloat($(".maxPoints:eq(0)").text());
		var myPointsBalance = $("#points-balance").find("h1").text().replace(",","");
		var myPointsBalance = parseFloat(myPointsBalance);

		if(maxPoints > myPointsBalance){
			maxPoints = myPointsBalance.toLocaleString();
		}

		maxPoints = (Math.round(maxPoints * 100)/100).toFixed(2);

		$("#maxPointsInput").val(maxPoints);
		$("#maxPointsInput").attr("max", maxPoints);
		calculateSuperTotal();
	});

	$("#maxPointsInput").on("change", function(){
		calculateSuperTotal();
	});

	$("body").on("click", ".paymentPref", function(){
		var paymentPref = $(this).data("pref");
		updateCheckoutOptions(paymentPref);

	});

	function updateCheckoutOptions(pref){
		var withdrawSource = "will be withdrawn from your Cash Balance";
		var selectedCard = $('input.paymentPref:checked');
		var last4 = selectedCard.data("last4") ?? false;
		var paymentId = selectedCard.data("id");

		if(pref != "pass"){
			withdrawSource = "will be charged to your card";
			if(last4){
				withdrawSource = "will be charged to your card ending in " + last4;
			}	
			$("#paymentPrefSelected").val(paymentId);
		} else {
			$("#paymentPrefSelected").val("pass");
		}

		$("#withdrawSource").text(withdrawSource);
		calculateSuperTotal();
	}

	document.addEventListener('DOMContentLoaded', function() {
	  let galleries = document.querySelectorAll('.lightgallery');
	  galleries.forEach(function(gallery) {
	    lightGallery(gallery);
	  });
	});
</script>
@endsection

