@extends('layouts.curator-master')

@section('css')
	<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

	<style type="text/css">
		.form-small{
			padding: 0;
			font-size: 12px;
		}
		.no-bg{
			background-color: transparent;
			border: none;
		}
		.select2-container .select2-selection--single,.select2-container--default .select2-selection--single .select2-selection__clear,.select2-container--default .select2-selection--single .select2-selection__arrow{
			height: 38px;
			font-size: 12px;
		}
		.select2-selection__rendered{
			padding-top: 5px;
		}
		.select2-container--default .select2-selection--single{
			border: 1px solid #ced4da;
		}
		.datepicker{
			max-width: 170px;
			font-size:12px;
			height:38px;
		}
		.btn-primary{
			border-radius: 4px;
		}
		.value-button{
			display: inline-block;
		    border: 1px solid #ddd;
		    margin: 0px;
		    width: 40px;
		    height: 40px;
		    text-align: center;
		    vertical-align: middle;
		    padding: 7px 0;
		    background: #eee;
		    -webkit-touch-callout: none;
		    -webkit-user-select: none;
		    -khtml-user-select: none;
		    -moz-user-select: none;
		    -ms-user-select: none;
		    user-select: none;
		}
		.decrease{
			margin-right: -4px;
    		border-radius: 8px 0 0 8px;
		}
		.increase{
			margin-left: 0;
    		border-radius: 0 8px 8px 0;
		}
		.steppable{
			text-align: center;
		    border: none;
		    border-top: 1px solid #ddd;
		    border-bottom: 1px solid #ddd;
		    margin: 0px;
		    width: 40px;
		    height: 40px;
		}
		.applyBtn{
			width: 100%;
			padding: 10px 5px !important;
		}
		.card{
			background: transparent;
			border: none;
			box-shadow:
		      0 1px 1px hsl(0deg 0% 0% / 0.015),
		      0 2px 2px hsl(0deg 0% 0% / 0.015),
		      0 4px 4px hsl(0deg 0% 0% / 0.015),
		      0 8px 8px hsl(0deg 0% 0% / 0.015),
		      0 16px 16px hsl(0deg 0% 0% / 0.015);
		}
		.bg-white{
			background-color: transparent;
			background-image: linear-gradient(112.1deg, rgba(255, 255, 248,0.2) 11.4%, rgba(255, 255, 255, 0.7) 70.2%);
		}
		
		.flight-search-dates .form-label, .btn-text-sm, .hide{
			display: none;
		}
		.datepicker{
			max-width: 300px !important;
		}
		.flight-search-dates{

		}

		@media(max-width:785px){
			.section-wrapper.black{
				margin-top: 0 !important;
			}
			.col-xs-6{
				width: 48% !important;
				margin: 2px !important;
				padding: 10px;
				border: 1px solid #ced4da;
				border-radius: 7px;
			}
			.flight-search-details-row{
				padding: 8px;
				margin-right: 0;
			}
			.swap-destinations{
				margin: 10px auto;
				width: 100% !important;
				max-width: 200px !important;
			}
			.swap-destinations .btn{
				width: 100%;
				max-width: inherit;
			}

			.flight-search-dates .form-label{
				display: block;
				font-size: 12px;
			}

			.flight-search-dates{
				width: 100%;
				margin: 10px 0;
			}
			.datepicker{
				max-width: 700px !important;
			}
			.flight-search-submit{
				margin: 0 auto;
				width: 100% !important;
				max-width: 200px !important;
			}
			.flight-search-submit .btn{
				width: 100%;
				max-width: 175px;
			}
			.btn-text-sm{
				display: inline-block;
			}
				.modal-content{
					min-height:700px;
				}
				.daterangepicker{
					border: none !important;
					left: 50%;
	    			width: 83% !important;
	    			max-width: 350px !important;
				}
				.drp-calendar{
					width: 100% important;
					max-width: 350px !important;
				}

			
			.daterangepicker:before{
				display: none !important;
			}
		}
		@media(min-width:785px){
			.daterangepicker:before{
				display: none !important;
			}
			.modal-content{
				min-height:500px;
			}
			.modal-dialog{
				max-width: 530px !important;
			}
			.daterangepicker{
				border: none !important;

				max-width: 500px !important;
				
			}
			.daterangepicker.single{
				min-width:  500px !important;
			}
			.drp-calendar{
				width: 100% important;
				max-width: 500px !important;
				
			}
			.single .drp-calendar{
				min-width:  500px !important;
			}
		}
		
	</style>
	<script src="https://maps.googleapis.com/maps/api/js?key={{ env('GOOGLE_MAPS_KEY') }}&libraries=places"></script>
@endsection

@section('content')
<div class="gradient-wrap" style="background: transparent; width:100%;">
    <!--  HERO -->
    <div class="section-wrapper black position-relative" style="max-width: 1000px; margin-top:100px; margin-bottom: 200px;">
    	<img src="/assets/img/resort.gif" style="display: block; width:300px; margin:0 auto; margin-bottom: 40px;">

			<div class="card bg-white">
				<div class="card-body">
			    	@include('hotels.components.search-form', $priorSearch)
			    </div>
			</div>
    	<br>
		<div class="justify-content-center text-center">
			<a href="/boardingpass" class="btn btn-white mb-2 text-success" target="_blank" style="font-weight: 400;"><img src="/assets/img/icons/deposit.png" style="width:25px; margin-top:-5px;"> Boarding Pass Required to Book Hotels</a>
			<div style="margin-top:2em">
				We're in Beta
				<p style="font-size:12px; max-width:340px; margin:0 auto;">Not all hotel chains are currently supported.</p>
			</div>
		</div>
	</div>
</div>
@include('flights.components.datepicker')
@endsection

@section('js')
	<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
	<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
	<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
	<script src="https://unpkg.com/@popperjs/core@2"></script>

	@include('hotels.components.hotels-js')
@endsection