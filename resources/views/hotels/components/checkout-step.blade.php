@include("hotels.components.offer-price-table")
@if($customer)
    <h5>Step 1: Select Payment Method</h5>
    <div class="tab-content pt-3" id="pills-tabContent">
        <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
            <ul class="nav nav-pills nav-fill flex d-flex mb-3 nav-justified" style="list-style:none !important;">
              <li class="nav-item paymentPref" data-pref="pass">
                <a class="nav-link active  text-center" aria-current="page" href="#pills-all-banks" data-bs-toggle="pill" data-bs-target="#pills-all-banks" type="button" role="tab" aria-controls="pills-all-banks">Boarding Pass</a>
              </li>
              <li class="nav-item paymentPref" data-pref="card">
                <a class="nav-link text-center" aria-current="page" href="#pills-all-cards" data-bs-toggle="pill" data-bs-target="#pills-all-cards" type="button" role="tab" aria-controls="pills-all-cards">Card</a>
              </li>
            </ul>
            <div class="tab-content">
                <div id="pills-all-banks" class="tab-pane fade in active show" role="tabpanel">
                    <div class="row mb-3">
                        <div class="col-md-6">
                            <div class="card bg-white p-3 text-center" style="background-image: linear-gradient(112.1deg, rgb(126, 124, 224) 11.4%, rgb(11, 143, 251) 70.2%);">
                                <button href=";javascript" type="button" data-bs-toggle="modal" data-bs-target="#spendingExplainerModal" style="background: none; border: none;">
                                    <span id="spending-power-balance"><i style="font-size: 200%;" class="fa fa-sync fa-spin text-large text-light"></i></span></button>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="card bg-white p-3 text-center" style="background-image: linear-gradient(112.1deg, rgb(96, 224, 210) 11.4%, rgb(4, 227, 151) 70.2%);">
                                <button href=";javascript" type="button" data-bs-toggle="modal" data-bs-target="#balanceExplainerModal" style="background: none; border: none;">
                                    <span id="cash-balance"><i style="font-size: 200%;" class="fa fa-sync fa-spin text-large text-light"></i></span></button>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="pills-all-cards" class="tab-pane fade in" role="tabpanel">
                    <div class="row">
                        @if(sizeof($cards) == 0)
                            <div class='col-md-12 col-lg-6 col-xl-4 mb-4'>
                                <div class='card bg-white small-shadow'>
                                    <div class='card-body pb-2'>
                                        <h5>To checkout with a card, please add a payment method to <a href="/home" target="_blank">your profile</a>.</h5>
                                        <p>You can then refresh this page.</p>
                                    </div>
                                </div>
                            </div>
                        @endif
                        @foreach($cards as $card)
                            @php
                                $cardNumber = "<span style='font-size:7px; opacity:0.3;'>⬤⬤⬤⬤</span> - <span style='font-size:7px; opacity:0.3;'>⬤⬤⬤⬤</span> - <span style='font-size:7px; opacity:0.3;'>⬤⬤⬤⬤</span> -" . $card["cardLast4"];
                                if($card["cardBrand"] == "Amex"){
                                    $cardNumber = "<span style='font-size:7px; opacity:0.3;'>⬤⬤⬤⬤</span> - <span style='font-size:7px; opacity:0.3;'>⬤⬤⬤⬤⬤⬤</span> -" . $card["cardLast4"];
                                }
                            @endphp
                            <div class='col-md-12 col-lg-6 col-xl-4 mb-4'><div class='card bg-white small-shadow'><div class='card-body pb-2'><div style='display:flex; flex-direction:row; justify-content:space-between; align-items:center;'><span style='font-size:13px;'>{{ $card["cardNickname"] }}</span><input type='radio' name='paymentId' data-id='{{ $card["paymentId"] }}' data-last4='{{ $card["cardLast4"] }}' class="paymentPref"></div><p class='pt-4' style='align-items:center; font-size:17px; font-weight:700; letter-spacing:0.3em; display:flex;'>{!! $cardNumber !!}<p><div style='display:flex; flex-direction:row; justify-content:space-between; align-items:center;'><span class='pt-4 pb-1'>{{ $card["cardExpMonth"] }}/{{ $card["cardExpYear"] }}</span><span class='pt-4 pb-1'>{{ $card["cardBrand"] }}</span></div></div></div></div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
    <h5>Step 2: Use Your Points</h5>
    <div id="points-input-module" class="row">
        <div class="col-md-8 mb-3">
            <div class="card bg-white">
                <div class="card-body">
                    <p>Enter the amount of points you would like to use towards this transaction. <strong>Max Points allowed: <span class="maxPoints"></span></strong></p>
                    <div class="input-group">
                        <input type="number" id="maxPointsInput" inputmode="numeric" maxlength="7" minlength="1" required="" class="form-control required" placeholder="1" step="0.5" min="0">
                        <span style="padding:10px;" class="btn btn-black maxPointsButton">MAX</span>
                    </div>
                    <br>
                </div>
            </div>
        </div>
        <div class="col-md-4 mb-3">
            <div class="card bg-white p-3 text-center" style="background-image: linear-gradient(112.1deg, rgb(253, 118, 78) 11.4%, rgb(254, 174, 27) 70.2%);">
            <button href=";javascript" type="button" data-bs-toggle="modal" data-bs-target="#pointsExplainerModal" style="background: none; border: none;">
                <span id='points-balance'><i style="font-size: 200%;" class="fa fa-sync fa-spin text-large text-light"></i></span></button>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card bg-white">
                <div class="card-body">
                    <h2>You'll Pay: $<span class="superTotal" id="finalOffer">{{ number_format($total,2) }}</i></span></h2>
                    <p>$<span id="deductedAmount">0</span>
                        <span id="withdrawSource">will be withdrawn from your Cash Balance</span>.
                    </p>
                    <p id="ccFeeDisclosure">A 3% card processing fee of $<span id="ccFee">0</span> is included.</p>
                    <p><span id="pointsApplied">0</span> Points will be deducted from your Points balance.</p>
                    <br>
                    <h5>Due at Accommodation: ${{ $hotel["rooms"][0]["rates"][0]["due_at_accommodation_amount"] }}</h5>
                    <p style="font-size:12px;">
                    Mandatory fees or taxes that are due by the guest at the accommodation and typically cover items such as resort fees, wi-fi access, fitness centers, pools, energy charges or safe fees. The amount of the charge is subject to change. Depending on the accommodation, these may be payable on check in or check out. <strong>This fee cannot be collected during the booking process on Venti.</strong> A value of "N/A" means the accomodation has not shared resort fees with Venti, but we cannot guarantee it is zero. You are responsible for contacting the hotel to ensure your awareness of any additional fees associated with reserving this accommodation and what exactly is covered with resort/mandatory fees.</p>
                    <br>
                    <input type="hidden" id="paymentPrefSelected" value="pass">
                    <button id="finalizePurchase" href="#" class="btn btn-success btn-lg" style="width: 100%"><i class="fa fa-sync fa-spin"></i>
                        Book This Hotel<span style="font-size:7px;display:block;"></span></button>
                    <br><br>
                    <p class="text-small">By booking this hotel, you re-affirm your agreement to Venti Financial's Terms of Use and Privacy Policy. You also agree to {{ $quote["accommodation"]["name"] }}'s Terms and Conditions, which may be found on their website.</p> 
                </div>
            </div>
        </div>
    </div>
@endif
@if(!$customer)
    <div class="card bg-white">
        <div class="card-body">
            <h5>You need a <a href="/boardingpass" target="_blank">Boarding Pass</a> to purchase this hotel. Members can save up to 99% on airfare, hotels, and more.</h5>
        </div>
    </div>
@endif