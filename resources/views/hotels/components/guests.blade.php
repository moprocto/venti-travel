<div class="modal" tabindex="-1" id="passengerModal">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <table class="table">
        	<tbody>
        		<tr>
        			<td style="vertical-align: middle;">Adults</td>
        			<td style="vertical-align: middle;">
        				<div style="display:flex; align-items: center;">
	        				<div class="value-button decrease" id="decreaseAdult" onclick="decreaseAdultCount()" value="Decrease Value"><i class="fa fa-minus"></i></div>
	  						<input type="text" class="steppable" id="adultNumber" value="{{ $priorSearch['adults'] ?? 1 }}" />
	  						<div class="value-button increase" id="increaseAdult" onclick="increaseAdultCount()" value="Increase Value"><i class="fa fa-plus"></i></div>
  						</div>
  					</td>
        		</tr>
        		<tr>
        			<td style="vertical-align: middle;">Children (Age 3 - 11)</td>
        			<td style="vertical-align: middle;">
        				<div style="display:flex; align-items: center;">
	        				<div class="value-button decrease" id="decreaseChildren" onclick="decreaseChildrenCount()" value="Decrease Value"><i class="fa fa-minus"></i></div>
	  						<input type="text" class="steppable" id="childrenNumber" value="{{ $priorSearch['children'] ?? 0 }}" />
	  						<div class="value-button increase" id="increaseChildren" onclick="increaseChildrenCount()" value="Increase Value"><i class="fa fa-plus"></i></div>
  						</div>
        			</td>
        		</tr>
        		<tr>
        			<td style="vertical-align: middle;">Rooms</td>
        			<td style="vertical-align: middle;">
        				<div style="display:flex; align-items: center;">
	        				<div class="value-button decrease" id="decreaseRooms" onclick="decreaseRoomsCount()" value="Decrease Value"><i class="fa fa-minus"></i></div>
	  						<input type="text" class="steppable" id="roomsNumber" value="{{ $priorSearch['rooms'] ?? 1 }}" />
	  						<div class="value-button increase" id="increaseRooms" onclick="increaseRoomsCount()" value="Increase Value"><i class="fa fa-plus"></i></div>
  						</div>
        			</td>
        		</tr>
        	</tbody>
        </table>
      </div>
    </div>
  </div>
</div>