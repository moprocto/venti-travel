@php
	$beds = $room["beds"] ?? [];
	$bedType = $beds[0]["type"] ?? false;
	$photo = false;
	$photos = $room["photos"] ?? [];
	if(sizeof($photos)>0){
		$photo = $photos[0]["url"];
	}
	$rates = $room["rates"];
	$randomRateID = "r" . generateRandomString(12);

@endphp
<div class="col-md-12" id="{{ $randomRateID }}">
	<div class="card bg-white mb-3">
		<div class="card-body">
			<div class="d-flex flex-column">
			<div class="row">
				@if($photo)
				<div class="col-md-4">
				@else
				<div class="col-md-12">
				@endif
					<h5 class="text-left">@if($hotel["room_count"] > 1) {{ $hotel["room_count"]}}X @endif {{ $room["name"] }}</h5>
					<div class="d-flex flex-column">
						<div style="display:flex; flex-wrap: wrap;">
	    					@foreach($beds as $bed)
		    					<div class="d-flex" style="margin: 5px; align-items:center; background-color:whitesmoke; padding: 5px 14px; border-radius: 20px;">
		    						<div style="display:flex; justify-content: space-between; flex-direction:row">
	    								<img src="/assets/img/icons/{{ $bed['type'] }}_bed.png" style="width:100%; max-width: 20px; margin-right:3px;">
	    								<span class="text-bold">{{ $bed["count"] }} {{ ucfirst($bed["type"]) }}</span>
	    							</div>
		    					</div>
	    					@endforeach
	    				</div>
					</div>
				</div>
				@if($photo)
					<div class="col-md-8" style="overflow: hidden; overflow-x:scroll;">
						<div class="d-flex flex-row lightgallery" style="width:fit-content;">
						@foreach($photos as $photo)
						<a href="{{ $photo["url"] }}">
		    				<div style="background-image: url('{{ $photo["url"] }}'); width:100px; height:100px; background-position: center; background-size:cover; border-radius: 0.375rem; margin-right:20px">
		    				</div>
		    			</a>
		    			@endforeach
		    			</div>
					</div>
					@else
					<div class="col-md-8">
						<p>No Photos Available</p>
					</div>
				@endif
			</div>
			<div class="d-flex flex-row justify-content-start mr-3" style="margin-right: 10px;">
				@foreach($rates as $rate)
				@php
					$id = $rate["id"];
					$total_amount = convertCurrency($rate["total_amount"],$rate["total_currency"]);
					$discountedPriceMultiplier = getDiscountPercent("hotel", $total_amount, $rate["total_currency"]);
					$discountedPrice = $total_amount * $discountedPriceMultiplier;
					$pointsNeeded = number_format($total_amount - $discountedPrice,2);
					$quantity = $rate["quantity_available"] ?? "N/A";

					// Calculate the cost per night

					//$costPerNight = round($total_amount / $numberOfNights,2);
					//$discountedCostPerNight = round($discountedPrice / $nights,2);

					
				@endphp
					@if($quantity == "N/A" || $quantity > 0)
					<div class="card bg-super mb-3 mt-3">
						<div class="card-body">
							<h3 class="text-center">
								<s style="font-size:11px; display:block;">
									${{ number_format($total_amount,0) }}
								</s> 
								${{ number_format($discountedPrice,2) }}
								<span class="d-block" style="font-size:10px;">
										{{ $pointsNeeded }} Points Needed
							</span></h3>
							<ul class="rate-details">
								<li>
									@if(!$rate["cancellation_timeline"] || $rate["cancellation_timeline"] == null)
										Non-Refundable
									@else
										Refundable
									@endif
								</li>
								<li>Boarding: {{ ucfirst(str_replace("_"," ", $rate["board_type"])) }}</li>
								<li><u class="resort-fee">${{ $rate["due_at_accommodation_amount"] ?? "N/A" }} Due at Check-In<sup>[?]</sup></u></li>
								<li>{{ $quantity }} Available</li>
							</ul>
							<span class="text-left"> </span>
							@if($selectable)
							<a href="/hotels/search/result/{{ $hotel['id'] }}/checkout/{{ $rate['id'] }}" class="btn btn-success d-block" target="_blank" style="margin:0 auto; font-size:11px; padding:5px;">SELECT OFFER</a>
							<br>
							@endif
						</div>
					</div>
						
					@endif
				@endforeach
			</div>
			</div>
		</div>
	</div>
</div>