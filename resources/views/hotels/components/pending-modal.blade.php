<div class="modal fade btnClose" id="pendingHotelModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true" style="background-image: linear-gradient(112.1deg, rgba(125, 110, 112,0.5) 11.4%, rgba(152, 135, 125,0.8) 70.2%); ">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header" style="position: absolute; right: 0; top: 10px; border-bottom: transparent; z-index: 99;">
        <button type="button" class="btn-close btnClose" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <h5>Checking Availability...</h5>
        <p>We're asking the hotel to confirm availability.</p>
        <div id="continuePreview" class=" text-center"><h1 class="pendingLoader"><i class="fa fa-sync fa-spin"></i></h1></div>
      </div>
    </div>
  </div>
</div>