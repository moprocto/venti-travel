@php
	$photos = $data["accommodation"]["photos"];
	$price = $data["accommodation"]["cheapest_rate_total_amount"];

	$discountedPriceMultiplier = getDiscountPercent("hotel", $price, $data["accommodation"]["cheapest_rate_currency"]);
	$discountedPrice = $price * $discountedPriceMultiplier;
	$totalSavings = $price - $discountedPrice;
	$totalSavingsPercent = (($discountedPrice - $price) / $price) * 100;

	$ratings = $data["rating"] ?? false;
	$description = $data["accommodation"]["description"] ?? "";

	// Create Carbon instances for the check-in and check-out dates
	$checkIn = Carbon\Carbon::createFromFormat('Y-m-d', $data["check_in_date"]);
	$checkOut = Carbon\Carbon::createFromFormat('Y-m-d', $data["check_out_date"]);

	// Calculate the number of nights
	$numberOfNights = $checkIn->diffInDays($checkOut);

	// Ensure there's at least one night
	$numberOfNights = max($numberOfNights, 1);

	// Calculate the cost per night
	$costPerNight = round($price / $numberOfNights,2);

	$discountedCostPerNight = round($discountedPrice / $numberOfNights,2);

	$chain = $data["accommodation"]["chain"]["name"] ?? false;

	$name = $data["accommodation"]["name"];

	if(str_contains($name, "Hilton") || str_contains($name, "Hampton Inn")){
		$chain = "Hilton";
	}

	if(str_contains($name, "Hyatt")){
		$chain = "Hyatt";
	}

	if(str_contains($name, "Marriott")){
		$chain = "Marriott";
	}
	if(str_contains($name, "Westgate")){
		$chain = "Westgate";
	}
	if(str_contains($name, "Hotel Indigo")){
		$chain = "Hotel Indigo";
	}
	if(str_contains($name, "Comfort Inn")){
		$chain = "Comfort Inn";
	}
	if(str_contains($name, "Wyndham")){
		$chain = "Wyndham";
	}
	$numChildren = 0;

	if(array_key_exists("guests", $data)){
		foreach($data["guests"] as $guest){
			if(!array_key_exists("type", $guest)){
				continue;
			}

			if($guest["type"] == "child"){
				$numChildren++;
			}
		}	
	}

	

@endphp
<div class="card bg-white mb-4 hotel-card"
data-offer="{{ $data['id'] }}"
data-default-order="{{ $data['order'] }}"
data-nightly-price="{{ $discountedCostPerNight }}"
data-stars="{{ $data['accommodation']['rating'] }}"
data-reviews="{{ $data['accommodation']['review_score'] }}"
data-brand="{{ $chain }}"
data-price="{{ $discountedPrice }}"
data-savings="{{ $totalSavings }}"
data-savings-percent="{{ $totalSavingsPercent }}"
>
	<div class="card-body" style="padding:0px;">
		<div class="hotel-card-content">
			<div class="row">
				<div class="col-md-3">
					<div id="{{ $data['id'] }}">
						<!-- Additional required wrapper -->
						@foreach($photos as $photo)
							@if($data["imgBlock"])
							<div class="hotel-thumbnail" style="background-image: url('{{ $photo['url'] }}'); background-size: cover; background-position:center;">
							</div>
							@else
								<img src="{{ $photo['url'] }}" style="width:300px;" style="display:block;" />
							@endif
							@php break; @endphp
						@endforeach
					</div>
				</div>
				<div class="col-md-5" style="padding:20px; padding-bottom:0; padding-left:10px;">
					<div class="d-flex" style="justify-content:space-between; flex-direction:row; align-items:center;">
					<h4 style="margin: 0; font-size:18px; text-transform: uppercase;">{{ $data["accommodation"]["name"] ?? "" }}</h4>
					<div class="d-flex">
						@for($i = 1; $i <= $data["accommodation"]["rating"]; $i++)
							<i class="fa fa-star text-warning"></i>
						@endfor
					</div>
					</div>
					<ul style="list-style: none; padding-left:0px;">
						@if(!$data["showPricing"])
							<li>Check In: {{ $checkIn->format("F d, Y") }}</li>
							<li>Check Out: {{ $checkOut->format("F d, Y") }}</li>
						@endif
						<li>
							<div class="d-flex" style="flex-direction:row; justify-content:flex-start;">
								@if($data["showPricing"])<span style="width:30px; text-align: right; padding-right: 10px;"><i class="fa fa-moon"></i></span>
								@endif
								<span>{{ $numberOfNights }} Night(s)</span>
							</div>
						</li>
						
						<li>
							<div class="d-flex" style="flex-direction:row; justify-content:flex-start;">
								@if($data["showPricing"])
								<span style="width:30px; text-align: center; padding-right:10px"><i class="fa fa-location-dot"></i></span>
								@endif
								<span>{{ 
								$data["accommodation"]["location"]["address"]["city_name"] }}, {{ $data["accommodation"]["location"]["address"]["country_code"] }}</span>
							</div>
						</li>
						@if($chain)
							<li>
								<div class="d-flex" style="flex-direction:row; justify-content:flex-start;">
									@if($data["showPricing"])
									<span style="width:30px; text-align: center; padding-right:10px"><i class="fa fa-hotel"></i></span>
									@endif
									<span>{{ $chain }}</span>
								</div>
							</li>
						@endif
						<li>
							<div class="d-flex" style="flex-direction:row; justify-content:flex-start;">
								@if($data["showPricing"])
								<span style="width:30px; text-align: center; padding-right:10px"><i class="fa fa-pencil"></i></span>
								@endif
								<span>Average Review: {{ $data["accommodation"]["review_score"] }}</span>
							</div>
						</li>
						<li>
							<div class="d-flex" style="flex-direction:row; justify-content:flex-start;">
								@if($data["showPricing"])
									<span style="width:30px; text-align: center; padding-right:10px"><i class="fa fa-user"></i></span>
								@endif
								@if(!isset($embed))
									<span class="d-block">{{ $data["adults"] }} @php echo ((int) $data["adults"] > 1) ? "Adults" : "Adult"; @endphp
									@if($numChildren > 0)
										<br>
										{{ $numChildren }} @php echo ((int) $numChildren > 1) ? "Children" : "Child"; @endphp
									@endif
									</span>
								@else
									<span class="d-block">{{ $data["adults"] }} @php echo ((int) $data["adults"] > 1) ? "Guests" : "Guest"; @endphp
									</span>
								@endif
								
							</div>
						</li>
						<li>
							<div class="d-flex" style="flex-direction:row; justify-content:flex-start;">
								@if($data["showPricing"])
									<span style="width:30px; text-align: center; padding-right:10px"><i class="fa fa-bed"></i></span>
								@endif
								<span>{{ $data["rooms"] }} Rooms</span>
							</div>
						</li>
						@if(!$data["showPricing"])
						<li>
							<div class="d-flex" style="flex-direction:row; justify-content:flex-start;">
								<span class="text-bold"><strong>Address</strong>:<br> {!! $data["addressString"] !!}</span>
							</div>
						</li>
						@endif
					</ul>
				</div>
				@if($data["showPricing"])
				<div class="col-md-4 text-center d-flex" style="padding: 20px; padding-bottom: 0; flex-direction: column; justify-content:center;">
					<s style="font-size:14px;" data-price="{{ $price }}">${{ number_format($costPerNight,2) }} Per Night</s>
					<h3 class="mb-1">${{ number_format($discountedCostPerNight,2) }} Per Night <span class="d-block" style="font-size:16px;">Subtotal: ${{ number_format($discountedPrice) }}</span></h3>
					@if($data["showPricing"])

							<div class="d-flex" style="flex-direction:row; justify-content:center; padding:10px">
								<span style="width:30px; text-align: center; padding-right:10px"><img src="/assets/img/venti-financial-logo-50.png" style="width:20px;"></span>
								<span class="text-success">Total Savings: ${{ number_format($price - $discountedPrice) }}</span>
							</div>

						
						@endif
					<button href="/hotels/search/result/{{ $data['id'] }}/offer/{{ $data['accommodation']['id'] }}" 
					target="_blank"
					data-accommodation-id="{{ $data['accommodation']['id'] }}"
					data-price="{{ $price }}"
					data-msrpd="{{ $discountedPriceMultiplier }}"
					data-searchid="{{ $data['id'] }}"
					type="button" 
					class="btn btn-primary btn-availability"><i class="fa fa-sync fa-spin"></i> View Offers</button>
					<a href="javascript:void(0)" class="priceMatchPop d-block text-decoration text-center text-small mt-1">We Price Match</a>
				</div>
				@endif
			</div>
		</div>
	</div>
</div>