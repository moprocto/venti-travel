<div class="modal fade" id="loadingModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true" style="background-image: linear-gradient(112.1deg, rgba(125, 110, 112,0.5) 11.4%, rgba(152, 135, 125,0.8) 70.2%); ">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header" style="position: absolute; right: 0; top: 10px; border-bottom: transparent; z-index: 99;">
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <img src="/assets/img/resort.gif" class="d-block mt-4" style="width:100px; margin:0px auto;">
        <h4 class="mt-3 d-block text-center" style="width:100%;">Loading...<span class="d-block" style="font-size:14px;">We're processing your order.</span></h4>
        <p>This may take up to a full minute. Please do not navigate away from this page or refresh the page.</p>
      </div>
    </div>
  </div>
</div>