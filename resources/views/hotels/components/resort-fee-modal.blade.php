<div class="modal fade" id="resortFeeModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true" style="background-image: linear-gradient(112.1deg, rgba(125, 110, 112,0.5) 11.4%, rgba(152, 135, 125,0.8) 70.2%); ">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header" style="position: absolute;
    right: 0;
    top: 10px; border-bottom: transparent; z-index: 99;">
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <h5>Resort Fees</h5>
        <p>Mandatory fees or taxes that are due by the guest at the accommodation and typically cover items such as resort fees, wi-fi access, fitness centers, pools, energy charges or safe fees. The amount of the charge is subject to change. Depending on the accommodation, these may be payable on check in or check out. <strong>This fee cannot be collected during the booking process on Venti.</strong> A value of "N/A" means the accomodation has not shared resort fees with Venti, but we cannot guarantee it is zero. You are responsible for contacting the hotel to ensure your awareness of any additional fees associated with reserving this accommodation and what exactly is covered with resort/mandatory fees.
        <p><strong>Room Only:</strong> This rate does not include food, resort credits, etc.</p>
        <p><strong>Breakfast:</strong> This rate includes breakfast, but may not include other meals and resort credits.</p>
      </div>
    </div>
  </div>
</div>