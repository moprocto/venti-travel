<form method="GET" action="/hotels/search" id="hotelSearch">
	<input type="hidden" name="adults" value="{{ $priorSearch['adults'] ?? 1 }}">
	<input type="hidden" name="children" value="{{ $priorSearch['children'] ?? 0 }}">
	<input type="hidden" name="rooms" value="{{ $priorSearch['rooms'] ?? 0 }}">
	<div class="row" style="align-items:baseline;">
		<div class="col-lg-3 col-xs-3 col-sm-3 col-md-3" style=" margin-right: 10px; display:flex;">
			<a href="#" data-bs-toggle="modal" data-bs-target="#passengerModal" style="font-size:14px; color:black;">
			  <span id="passengers">{{ ($priorSearch['adults'] ?? 1) + ($priorSearch['children'] ?? 0) }}  
					@if(!isset($priorSearch))
						Adult(s)
					@else
						Traveler(s)
					@endif
			</span> &nbsp; &nbsp; <span id="rooms">{{ $priorSearch['rooms'] ?? 1 }}</span> Rooms <i class="fa fa-chevron-down"></i>
			</a>
			@include('hotels.components.guests')
		</div>

		<div class="col-md-4">
			<input id="autocomplete_search" name="autocomplete_search" type="text" class="form-control" placeholder="Destination city" style="height: 38.5px; font-size:12px;" value="{{ $priorSearch['to'] ?? '' }}" />
			<input type="hidden" name="lat" id="lat" value="{{ $priorSearch['lat'] ?? '' }}" >
            <input type="hidden" name="long" id="long" value="{{ $priorSearch['long'] ?? '' }}" >
            <input type="hidden" name="city" id="city" value="{{ $priorSearch['to'] ?? '' }}">
		</div>
		<div class="col-md-3 flight-search-dates">
			<button class="btn btn-white text-left datePick" type="button" data-bs-toggle="modal" data-bs-target="#datepickModal" id="roundTripDate"style="font-size:11px; width:100%; border:1px solid #ced4da; padding:10px;">
				@if($priorSearch)
					@if(isset($priorSearch['end']) && $priorSearch['end'] != 'undefined' && $priorSearch['end'] != '')
						{{ $priorSearch['start'] }} - {{ $priorSearch['end'] }}
					@endif
				@else
					{{ Carbon\Carbon::now()->add("30","days")->format("m/d/Y") }} - {{ Carbon\Carbon::now()->add("33","days")->format("m/d/Y") }}
				@endif
			</button>
		</div>
		<div class="col-md-1 flight-search-submit">
			<button class="btn btn-primary" type="button" id="search"><i class="fa fa-search"></i> <span class="btn-text-sm">Search</span></button>
		</div>
	</div>
</form>
	