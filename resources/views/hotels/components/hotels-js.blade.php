<script type="text/javascript">
  	google.maps.event.addDomListener(window, 'load', initialize);

    function initialize() {
		var input = document.getElementById('autocomplete_search');

		var autocomplete = new google.maps.places.Autocomplete(input);

		autocomplete.addListener('place_changed', function () {
			var place = autocomplete.getPlace();

			// place variable will have all the information you are looking for.

			$("#city").val(place.formatted_address);

			$('#lat').val(place.geometry['location'].lat());

			$('#long').val(place.geometry['location'].lng());

		});
  	}

  	$(".showMap").click(function(){
  		$("#mapModal").modal("show");
  	});

	var windowWidth = $(window).width();


	if(windowWidth < 800){
		@if(Route::current()->getName() != 'hotels')
			var accordionElement = document.getElementById('collapseOne');
		    var bsCollapse = new bootstrap.Collapse(accordionElement, {
		        toggle: true
		    });

			var accordionElement = document.getElementById('collapseTwo');
		    var bsCollapse = new bootstrap.Collapse(accordionElement, {
		        toggle: true
		    });
	    @endif
	}


	@if(!$priorSearch)
		$("#roundTripDate").show();
		$("#oneWayDate").hide();
	@else

		@if(($priorSearch["end"] ?? false) == false || $priorSearch["end"] == "undefined")
			$("#roundTripDate").hide();
		@else
			$("#oneWayDate").hide();
		@endif
	@endif
	$(".value-button").click(function(){
		// calc number of travelers
		var adults = parseInt($("#adultNumber").val());
		var children = parseInt($("#childrenNumber").val());
		var rooms = parseInt($("#roomsNumber").val());

		var passengers = parseInt(adults + children);
		if(children != 0 || rooms != 0){
			$("#passengers").text(passengers + " Travelers");
		} else {
			$("#passengers").text(passengers + " Adults");
		}

		$("#rooms").text(rooms);
		
    });

    $('#hotelSearch').submit(function(e){
	  //e.preventDefault();
	});

	$('#search').click(function(){
		var destination = $('#city').val();
		var startDate = $('.datepicker').val().split(" - ")[0];
		var returnDate = $('.datepicker').val().split(" - ")[1];
		var lat = $("#lat").val();
		var long = $("#long").val();

		var adults = $("#adultNumber").val();
		var children = $("#childrenNumber").val();
		var rooms = $("#roomsNumber").val();

		if(rooms == 0){
			rooms = 1;
		}

	  	var fieldCheck = checkMandatoryFields(destination, startDate, returnDate, adults, children, rooms);

	  	if(fieldCheck === true){
	  		window.location.href = "/hotels/search" +
	  		"?to=" + destination + 
	  		"&start=" + startDate +  
	  		"&end=" + returnDate + 
	  		"&adults=" + adults + 
	  		"&children=" + children + 
	  		"&rooms=" + rooms +
	  		"&lat=" + lat +
	  		"&long=" + long
	  	}
	});


	function checkMandatoryFields(destination, startDate, returnDate, adults, children, rooms){

		if(isBlank(destination) || destination == "Select Destination"){
			swal({
                  icon: 'error',
                  title: 'Missing required fields',
                  text: 'Please add where you looking for a hotel',
            });

            return false;
		}



		if(adults == 0 && children == 0 && rooms == 0){
			swal({
                  icon: 'error',
                  title: 'Missing required fields',
                  text: 'You need to input at least one traveler',
            });
            return false;
		}
		if(rooms >= 1 && adults == 0){
			swal({
                  icon: 'error',
                  title: 'Missing required fields',
                  text: 'You need to input at least one adult',
            });
            return false;
		}

		return true;
	}

	function isBlank(value){
		if(value == "" || value == undefined || value == null){
			return true;
		}
		return false;
	}

	function clearOptions(elementId) {
	   var courseSelect = document.querySelector("#" + elementId);

	   while (courseSelect.childNodes[0] != null) {
	       courseSelect.childNodes[0].remove();
	   }
	}

	var dayDif = 0;

	@if(!$priorSearch)
			$(".datepicker").daterangepicker({
				locale: {
			          format: 'MM/DD/YYYY',
			    },

			    singleDatePicker: false,
				showDropdowns: true,
				drops: "auto",
				opens:"auto",
				startDate: moment().add(30, 'days').format("MM/DD/YYYY"),
				endDate: moment().add(33, 'days').format("MM/DD/YYYY"),
				minDate: moment().format("MM/DD/YYYY"),
				maxDate: moment().add(10, 'M').format("MM/DD/YYYY")
			});
	@else

			@if(($priorSearch["end"] ?? null) == null || ($priorSearch["end"] ?? null) == "undefined")
				$('.datepicker').daterangepicker({
				    locale: {
				          format: 'MM/DD/YYYY',
				    },
				    singleDatePicker: true,
				    showDropdowns: true,
				    startDate: "{{ $priorSearch['start'] ?? Carbon\Carbon::now()->format('m/d/Y') }}",
				    drops: "auto",
					opens:"auto"
				});
			@else
				$('input[name="dates"]').daterangepicker({
					drops: "auto",
					opens:"auto",
					startDate: "{{ $priorSearch['start'] }}",
					endDate: "{{ $priorSearch['end'] }}",
					minDate: moment().format("MM/DD/YYYY"),
					maxDate: moment().add(10, 'M').format("MM/DD/YYYY")
				});
			@endif

	@endif

	$("body").on("click",".applyBtn", function(){
		updateDates();
	});

	$("body").on("click",".cancelBtn", function(){
		updateDates();
	});


	function updateDates(){
		$("#datepickModal").modal('hide');
		var flightStartDate = $('.datepicker').val().split(" - ")[0];
		var flightReturnDate = $('.datepicker').val().split(" - ")[1];
		var flighType = $(".flight-type[type=radio]:checked").val();

		if(flighType == "one-way"){
			$(".datePick").text(flightStartDate);
		} else {
			$(".datePick").text(flightStartDate + " - " + flightReturnDate);
		}
	}

	$(".datePick").click(function(){
		var roundTripDate = $("#roundTripDate");
		var oneWayDate = $("#oneWayDate");

		setTimeout(function(){
			$(".modal-dialog .datepicker").trigger("click");
		}, 500);

		var flighType = $(".flight-type[type=radio]:checked").val();
	});

	$(".flight-type[type=radio]").click(function(){
		var flightStartDate = $('.datepicker').val().split(" - ")[0];
		var flightReturnDate = $('.datepicker').val().split(" - ")[1];
		if(!isBlank(flightReturnDate)){
			dayDif = moment(flightReturnDate).diff(moment(flightStartDate), 'days', false);

		}

		if($(this).val() == "one-way"){

			$("#roundTripDate").hide();
			$("#oneWayDate").show();
			$(".datePick").text(flightStartDate);
			$('.datepicker').daterangepicker({
			    locale: {
			          format: 'MM/DD/YYYY',
			    },
			    singleDatePicker: true,
			    showDropdowns: true,
			    startDate: flightStartDate,
			    drops: "auto",
				opens:"auto",
				minDate: moment().format("MM/DD/YYYY"),
				maxDate: moment().add(10, 'M').format("MM/DD/YYYY")
			});
		} else {
			if(flightReturnDate == undefined){
				flightReturnDate = moment(flightStartDate).add(7, 'days').format("MM/DD/YYYY");
			}

			$(".datePick").text(flightStartDate + " - " + flightReturnDate);
			$("#oneWayDate").hide();
			$("#roundTripDate").show();
			$('.datepicker').daterangepicker({
			    locale: {
			          format: 'MM/DD/YYYY',
			    },
			    showDropdowns: true,
			    startDate: flightStartDate,
			    endDate: (dayDif > 0) ? moment(flightStartDate).add(dayDif, 'days').format("MM/DD/YYYY") : moment(flightStartDate).add(7, 'days').format("MM/DD/YYYY"),
			    drops: "auto",
				opens:"auto",
				minDate: moment().format("MM/DD/YYYY"),
				maxDate: moment(flightStartDate).add(10, 'M').format("MM/DD/YYYY")
			});
		}
	});

	function getRandomInt(max) {
	  return Math.floor(Math.random() * max);
	}

	function increaseAdultCount() {
	  var value = parseInt(document.getElementById('adultNumber').value, 10);
	  value = isNaN(value) ? 0 : value;
	  value++;
	  document.getElementById('adultNumber').value = value;
	}

	function decreaseAdultCount() {
	  var value = parseInt(document.getElementById('adultNumber').value, 10);
	  value = isNaN(value) ? 0 : value;
	  value < 1 ? value = 1 : '';
	  value--;
	  document.getElementById('adultNumber').value = value;
	}

	function increaseChildrenCount() {
	  var value = parseInt(document.getElementById('childrenNumber').value, 10);
	  value = isNaN(value) ? 0 : value;
	  value++;
	  document.getElementById('childrenNumber').value = value;
	}

	function decreaseChildrenCount() {
	  var value = parseInt(document.getElementById('childrenNumber').value, 10);
	  value = isNaN(value) ? 0 : value;
	  value < 1 ? value = 1 : '';
	  value--;
	  document.getElementById('childrenNumber').value = value;
	}

	function increaseRoomsCount() {
	  var value = parseInt(document.getElementById('roomsNumber').value, 10);
	  value = isNaN(value) ? 0 : value;
	  value++;
	  document.getElementById('roomsNumber').value = value;
	}

	function decreaseRoomsCount() {
	  var value = parseInt(document.getElementById('roomsNumber').value, 10);
	  value = isNaN(value) ? 0 : value;
	  value < 1 ? value = 1 : '';
	  value--;
	  document.getElementById('roomsNumber').value = value;
	}

	$(document).on('select2:open', function(e) {
	  document.querySelector(`[aria-controls="select2-${e.target.id}-results"]`).focus();
	});

	function getDiscountPercent(productClass, msrp, $estimatedProfitMargin = null){

	    // 
	    switch (productClass) {
	        case 'airfare':
	            // code...

	            switch (msrp) {
	                case msrp > 5000:
	                    // 5 percent off business and first class flights
	                    return 0.95;
	                    break;
	                case (msrp < 5000 && msrp > 2000 ):
	                    return 0.90;
	                    // 10 percent off premium economcy
	                    break;
	                case (msrp < 2000 && msrp > 500):
	                    return 0.85;
	                    // 15 percent off economy
	                    break;
	                case (msrp < 500 && msrp > 100):
	                    return 0.75;
	                    // 30 percent off domestic economy
	                    break;
	                case (msrp < 100 && msrp > 70):
	                	return ((msrp - 50)/msrp);
	                    // 50 percent off economy
	                    break;
	                case (msrp <= 70):
	                    return 0;
	                    // 99 percent off spirit economy lol
	                    break;
	                default:
	                    return 1;
	                    break;
	            }
	            return 1;
	            break;
	        
	        default:
	            return 1;
	            break;
	    }
	}
</script>