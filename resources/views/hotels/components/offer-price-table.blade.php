@php
	$subtotal = $quote["base_amount"] + $quote["tax_amount"] + $quote["fee_amount"];
	$total = convertCurrency($subtotal, $quote["total_currency"]);
	$discountedPriceMultiplier = getDiscountPercent("hotel", $total, $quote["total_currency"]);
	$discountedPrice = $total * $discountedPriceMultiplier;
	$maxPoints = number_format($total - $discountedPrice,2);
@endphp
<div class="card bg-white mb-3">
	<div class="card-body ">
		<div class="text-right">
			<table class="table">
				<input type="hidden" class="destination">
				<tr>
					<td class="text-left">Base Amount</td>
					<td class="text-right"><h5>${{ number_format(convertCurrency($quote["base_amount"], $quote["base_currency"]),2) }}</h5></td>
				</tr>
				<tr>
					<td class="text-left">Taxes</td>
					<td class="text-right"><h5>${{ number_format(convertCurrency($quote["tax_amount"], $quote["tax_currency"]),2) }}</h5></td>
				</tr>

				<tr>
					<td class="text-left">Additional Resort Fees</td>
					<td class="text-right"><h5>${{ number_format(convertCurrency($quote["fee_amount"], $quote["fee_currency"]),2) }}</h5></td>
				</tr>

				<tr>
					<td class="text-left">Subtotal</td>
					<td class="text-right"><h5>$<span id="newTotal">{{ number_format($total,2) }}</span></h5></td>
				</tr>
				<tr>
					<input type="hidden" id="maxPointsAllowedOG" value="{{ $maxPoints }}">
					<td class="text-left"><h3>Lowest Possible Price <span  style="font-size:14px; display:block;">You can use up to <span class="maxPoints">{{ $maxPoints }}</span> Points to achieve this price.</span></h3></td>
					<td class="text-right"><h5>$<span id="lowestPossiblePrice">{{ number_format($discountedPrice,2) }}</span></h5></td>
				</tr>
			</table>
		</div>
	</div>
</div>
    			
    			
