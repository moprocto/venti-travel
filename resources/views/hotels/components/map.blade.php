<div class="modal fade" id="mapModal" tabindex="-1" aria-labelledby="exampleModalLabel"  style="background-image: linear-gradient(112.1deg, rgba(125, 110, 112,0.5) 11.4%, rgba(152, 135, 125,0.8) 70.2%); ">
  <div class="modal-dialog modal-dialog-centered" style="width:90%; max-width:9000px !important;">
    <div class="modal-content" style="min-height:600px; !important">
      <div class="modal-header" style="position: absolute;
    right: 0;
    top: 10px; border-bottom: transparent; z-index: 99;">
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div id="map" style="height:600px; width:100%; border-radius: 10px; margin-bottom:15px;"></div>
      </div>
    </div>
  </div>
</div>