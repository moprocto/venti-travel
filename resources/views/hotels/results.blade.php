@extends("layouts.curator-master")

@section('css')
<link
  rel="stylesheet"
  href="https://cdn.jsdelivr.net/npm/swiper@11/swiper-bundle.min.css"
/>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

<link rel="stylesheet" type="text/css" href="/assets/css/steps.css">
<style type="text/css">
	input.disabled{
        pointer-events: none;
    }
    .swiper {
	  width: 380px;
	  height: 240px;
	}
    .accordion-button:not(.collapsed){
    	background: transparent;
    }
    .accordion-item{
		border: none;
	}
	td.vertical-align{
		vertical-align: middle;
	}
	tr.borderless td{
		border-bottom: none !important;
	}
	.table,.card-body{
		padding-bottom: 0;
		margin: 0;
	}
	ul.nolist{
		list-style: none;
	}
	.text-small{
		font-size: 12px;
	}
	.results-table .text-left{
		font-size: 13px;
		font-weight: 700;
	}
	.results-table .text-right{
		font-size: 13px;
	}
	.card{
		background: transparent;
		background-image: linear-gradient(112.1deg, rgba(242, 242, 246,0) 11.4%, rgba(255, 255, 255, 0.7) 70.2%);
		border: none;
		box-shadow:
	      0 1px 1px hsl(0deg 0% 0% / 0.015),
	      0 2px 2px hsl(0deg 0% 0% / 0.015),
	      0 4px 4px hsl(0deg 0% 0% / 0.015),
	      0 8px 8px hsl(0deg 0% 0% / 0.015),
	      0 16px 16px hsl(0deg 0% 0% / 0.015)
	    ;
	}
	td.borderless{
		border-bottom: none;
	}
	#sorter-row,#filters-menu,.flight-search-dates .form-label, .btn-text-sm, .hide{
		display: none;
	}
	@media(min-width:786px){
		.flight-card-content{
			display: flex;
			align-items: center;
			padding: 8px 0;
		}
		.boarding-pass-footer{
			padding: 0 10px;
			font-size: 11px;
			margin: 0;
			max-width: 170px;
		}
		.checked-bags{
			line-height: 0.9;
			display:block;
			max-width: 155px;
			margin:0 auto;
		}
		#sorter-row{
			margin-top: -22px;
		}
	}

	@media(max-width:785px){
		#filters-menu{
			margin-top: 0 !important;
		}
		.flight-card-content{
			display: block;
			align-items: center;
		}
		.flight-card .table{
			width: 100% !important;
		}
		.flight-card .table h5{
			font-size: 1rem;
		}
		.flight-card .table img{
			max-width: 40px !important;
		}
		.carrier-data{
			max-width: 80px !important;
		}
		.table .text-small{
			font-size: 9px;
		}
		.table,.card-body{
			padding: 0;
			margin: 0;
			padding-bottom: 10px;
		}

		.duration-info{
			width: 100px;
		}
		.boarding-pass-footer{
			padding: 0 10px;
			font-size: 11px;
			margin: 0;
			max-width: inherit;
		}
		.checked-bags{
			line-height: 0.9;
			display:block;
			max-width: 155px;
			margin:0 auto;
			margin-top: 20px;
		}
		.section-wrapper.black{
			margin-top: 50px;
		}
		#sorter-row{
			margin-top: 22px;
			padding-top: 5px !important;
		}
	}

	.remove{
		display: none !important;
	}

	li.completed a,li.completed a:hover{
		background-color: rgba(25, 135, 84, 0.3) !important;
	}
	

	button.accordion-button{
		line-height: 2;
		font-weight: 400 !important;
		font-family: 'sofia';
		border-top-left-radius: 0.375rem;
		border-top-right-radius: 0.375rem;
		padding: 10px 10px 5px 10px;
	}
	.accordion-item{
		border-radius: 0.375rem;
		border: none;
	}

	.accordion-item,.accordion-button:not(.collapsed){
		background-color: white;
	}
	.accordion-item{
		border: none;
	}

	.wizard>.steps a,.wizard>.steps a:hover{
		padding: 8px 7px 7px 20px;
	}
	.stepThreeContent .btn-success{
		pointer-events: none;
		opacity: 0.6;
	}
	td.vertical-align .text-small{
		line-height: 1;
	}
	.select2-container .select2-selection--single,.select2-container--default .select2-selection--single .select2-selection__clear,.select2-container--default .select2-selection--single .select2-selection__arrow{
			height: 38px;
			font-size: 12px;
		}
		.select2-selection__rendered{
			padding-top: 5px;
		}
		.select2-container--default .select2-selection--single{
			border: 1px solid #ced4da;
		}
		.select2-container--open{
			z-index: 9999;
		}
		.datepicker{
			max-width: 170px;
			font-size:12px;
			height:38px;
		}
		.btn-primary{
			border-radius: 4px;
		}
		.value-button{
			display: inline-block;
		    border: 1px solid #ddd;
		    margin: 0px;
		    width: 40px;
		    height: 40px;
		    text-align: center;
		    vertical-align: middle;
		    padding: 7px 0;
		    background: #eee;
		    -webkit-touch-callout: none;
		    -webkit-user-select: none;
		    -khtml-user-select: none;
		    -moz-user-select: none;
		    -ms-user-select: none;
		    user-select: none;
		}
		.decrease{
			margin-right: -4px;
    		border-radius: 8px 0 0 8px;
		}
		.increase{
			margin-left: 0;
    		border-radius: 0 8px 8px 0;
		}
		.steppable{
			text-align: center;
		    border: none;
		    border-top: 1px solid #ddd;
		    border-bottom: 1px solid #ddd;
		    margin: 0px;
		    width: 40px;
		    height: 40px;
		}
		.applyBtn{
			width: 100%;
			padding: 10px 5px !important;
		}

		@media(max-width:785px){
			.section-wrapper.black{
				margin-top: 0 !important;
			}
			.col-xs-6{
				width: 48% !important;
				margin: 2px !important;
				padding: 10px;
				border: 1px solid #ced4da;
				border-radius: 7px;
			}
			.flight-search-details-row{
				padding: 8px;
				margin-right: 0;
			}
			.swap-destinations{
				margin: 10px auto;
				width: 100% !important;
				max-width: 200px !important;
			}
			.swap-destinations .btn{
				width: 100%;
				max-width: inherit;
			}

			.flight-search-dates .form-label{
				display: block;
				font-size: 12px;
			}

			.flight-search-dates{
				width: 100%;
				margin: 10px 0;
			}
			.datepicker{
				max-width: 700px !important;
			}
			.flight-search-submit{
				margin: 0 auto;
				width: 100% !important;
				max-width: 200px !important;
			}
			.flight-search-submit .btn{
				width: 100%;
				max-width: 175px;
			}
			.btn-text-sm{
				display: inline-block;
			}
				.modal-content{
					min-height:700px;
				}
				.daterangepicker{
					border: none !important;
					left: 50%;
	    			width: 83% !important;
	    			max-width: 350px !important;
				}
				.drp-calendar{
					width: 100% important;
					max-width: 350px !important;
				}

			
			.daterangepicker:before{
				display: none !important;
			}
		}

		@media(min-width:785px){
			.daterangepicker:before{
				display: none !important;
			}
			.modal-content{
				min-height:500px;
			}
			.modal-dialog{
				max-width: 530px !important;
			}
			.daterangepicker{
				border: none !important;

				max-width: 500px !important;
				
			}
			.daterangepicker.single{
				min-width:  500px !important;
			}
			.drp-calendar{
				width: 100% important;
				max-width: 500px !important;
				
			}
			.single .drp-calendar{
				min-width:  500px !important;
			}
		}
		.no-bg{
			background-color: transparent;
			border: none;
		}
		.form-small {
		    padding: 0;
		    font-size: 12px;
		}

		#example-basic .actions ul li:nth-child(2){
			display: none !important;
		}
		
		
		@media(max-width: 768px){
			.hotel-thumbnail{
				border-top-left-radius: 0.375rem; border-top-right-radius: 0.375rem; 
				height: 220px; 
				width: 100%;
				background-position: center;
			}
			.hotel-card .col-md-5{
				padding: 20px 30px !important;
			}
			.hotel-card .col-md-4{
				padding-bottom: 20px !important;
			}
		}
		@media(min-width: 768px){
			.hotel-thumbnail{
				border-top-left-radius: 0.375rem; border-bottom-left-radius: 0.375rem;
				height: 220px;
				width:200px;
			}
		}
		.btn-availability{
			display: flex;
			justify-content: center;
			align-items: center;
		}
		.btn-availability .fa-spin{
			max-width: 30px;
			margin-right: 5px;
		}
		.map-price{
			box-shadow:0 .5rem 1rem rgba(0,0,0,.15)!important;
			z-index: 9999;
		}
		.map-price:hover{
			cursor: pointer;
			background: white;
			color: black;
		}

		button.gm-ui-hover-effect{
			display: none;
		}
</style>
<script src="https://maps.googleapis.com/maps/api/js?key={{ env('GOOGLE_MAPS_KEY') }}&libraries=places"></script>
@endsection

@section('content')

<div class="gradient-wrap" style="background: transparent; width:100%;">
    <!--  HERO -->
    <div class="section-wrapper black position-relative">
    	<div class="row mb-3">
    		<div class="col-md-12">
    			<div class="accordion " id="accordionExample" style="width:100%; padding:0;">
					  <div class="accordion-item bg-shadow" style="border-radius: 0.375rem">
					    <h2 class="accordion-header" id="headingOne" style="border-radius: 10px;">
					      <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne" style="border-top-left-radius:0.375rem; border-top-right-radius:0.375rem;">My Search</button>
					    </h2>
				        <div id="collapseOne" class="accordion-collapse collapse show" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
				            <div class="accordion-body">
				    			@include('hotels.components.search-form')
				            </div>
				        </div>
				   </div>
				</div>
			</div>
		</div>
    	<div class="row">
    		<div class="col-md-4">
    			<div id="show-map" class="hide">
	    			<div class="mb-2 bg-shadow d-flex" style="justify-content: center; min-height: 150px; background-size: cover; background-image: url('/assets/img/2d-map.jpg'); border-radius:0.325em">
	    				<a href="#" style="display: flex; flex-direction: column; justify-content: center;">
	    					<img src="/assets/img/map-marker.png" style="display: block; width:32px; margin:10px auto;">
	    				<button class="btn btn-success showMap" style="width:150px; display:block;">Show Map</button></a>
	    			</div>
	    		</div>
    			<div class="mb-2 mt-2" id="sorter-row" style="padding-top:23px;">
					<div style="flex-direction: row;  align-items: center; justify-content: space-between;">
						<div style="width: 100%;">
							<select class="form-control bg-shadow" id="sorter" style="border:none;">
								<option value="lowest">Sort by Subtotal</option>
								<option value="savings">Sort by Total Savings</option>
								<option value="reviews">Sort by Avg. Review</option>
							</select>
						</div>
						<div>
							<span style="color: grey; font-size: 11px; padding: 5px 20px; text-align: right;"></span>
						</div>
					</div>
				</div>
    			<div class="row">
    				<div class="col-md-12">
    					
    				</div>
    			</div>
    			<div class="row mt-1" id="filters-menu">
    				<div class="col-md-12">
    					<div class="accordion mb-3" id="accordionExample2" style="width:100%; padding:0;">
						  <div class="accordion-item bg-shadow" style="border-radius: 0.375rem">
						    <h2 class="accordion-header" id="headingTwo" style="border-radius: 10px;">
						      <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo" style="border-top-left-radius:0.375rem; border-top-right-radius:0.375rem;">Filter Results</button>
						    </h2>
						        <div id="collapseTwo" class="accordion-collapse collapse show" aria-labelledby="headingOne" data-bs-parent="#accordionExample2">
						            <div class="accordion-body">
						            	<div id="hotel-filters"></div>
						            </div>
						        </div>
						   </div>
						</div>
						<p class="text-small">Currently in Beta. Better filters will be added in due time.</p>
    				</div>
    			</div>
    		</div>
    		<div class="col-md-8">
    			<div id="example-basic">
				    
				</div>
				<div id="hotels-results"><img src="/assets/img/resort.gif"  class="d-block mt-4" style="width:300px; margin:0 auto;"><h4 class="text-center mt-4">Searching for hotels...</h4></div>
    		</div>
    	</div>
    </div>
</div>

@include('flights.components.datepicker') 
@include('hotels.components.pending-modal')
@include('hotels.components.map')
@include('flights.components.price-match-explainer', ["pm" => "hotels"])

@endsection

@section('js')
	<script src="https://cdn.jsdelivr.net/npm/swiper@11/swiper-bundle.min.js"></script>
	<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
	@include('hotels.components.hotels-js')
	<script type="text/javascript" src="/assets/js/jquery.steps.min.js"></script>
	
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
	<script type="text/javascript">
		$.ajaxSetup({
	        headers: {
	            'X-CSRF-TOKEN': '{{ csrf_token() }}'
	        }
	    });

	    var hotelCoordinates = [];
	    var map;
	    var google;


	    function initialize() {

	    	google.maps.event.addDomListener(window, 'load', initialize);

			var input = document.getElementById('autocomplete_search');

			var autocomplete = new google.maps.places.Autocomplete(input);

			autocomplete.addListener('place_changed', function () {

				var place = autocomplete.getPlace();

				// place variable will have all the information you are looking for.

				$("#city").val(place.formatted_address);

				$('#lat').val(place.geometry['location'].lat());

				$('#long').val(place.geometry['location'].lng());

			});

		}
		function initMap(){

		    var initialLocation = {lat: {{ $lat }}, lng: {{ $long }}}; // Your initial location // Replace with your lat/long

			map = new google.maps.Map(document.getElementById('map'), {
		        zoom: 14,
		        center: initialLocation
		    });

		    for(i= 0; i < hotelCoordinates.length; i++){
		    	createMarkerWithInfoWindow(hotelCoordinates[i]);
		    }

		    /*
		    var aListener = google.maps.event.addListener(map, 'click', function(event) {
			    // Try to prevent event propagation to the map
			    event.stop();
			    event.cancelBubble = true;
			    if (event.stopPropagation) {
			        event.stopPropagation();
			    }
			    if (event.preventDefault) {
			        event.preventDefault(); 
			    } else {
			        event.returnValue = false;  
			    }
			});
			*/
		}
		function createMarkerWithInfoWindow(hotelData) {
			var hotel = hotelData;

			var newMarkerLocation = new google.maps.LatLng(hotel.lat, hotel.lng);
			var textMarker = new TextMarker(newMarkerLocation, "$" + hotel.price, map, hotel);
		    var infowindow = new google.maps.InfoWindow();
		    /*
		    var aListener = google.maps.event.addListener(map, 'click', function(event) {
			    // Try to prevent event propagation to the map
			    console.log(event.className)
			    event.stop();
			    event.cancelBubble = true;
			    if (event.stopPropagation) {
			        event.stopPropagation();
			    }
			    if (event.preventDefault) {
			        event.preventDefault(); 
			    } else {
			        event.returnValue = false;  
			    }
			});
			*/

		    google.maps.event.addListener(infowindow, 'domready', function() {
			    // This is the <div> which receives the click event.	    

		        $("#" + hotel.id + "-btn").click(function(){
					var btn = $(this);
					btn.addClass("disable-while-loading");

					var searchID = $(this).data("searchid");
					var price = $(this).data("price");
					var msrpd = $(this).data("msrpd");
					var accommodationID = $(this).data("accommodation-id");

					getOfferStatus(searchID, accommodationID).then(offer => {
						if(offer == 200){
			        		btn.removeClass("disable-while-loading");
			        		updateSearchSelection(price, searchID, msrpd);
			        		return navigate('/hotels/search/result/'+ searchID +'/offer/' + accommodationID, true);
			        	} else {
			        		btn.removeClass("disable-while-loading");
			        		btn.removeClass("btn-availability");
			        		btn.removeClass("btn-primary");
			        		btn.addClass("btn-warning");
			        		btn.text("No Longer Available");
			        	}
					});

					
		        });

		        google.maps.event.addListener(infowindow, 'click', function() {
			    	console.log("click detected infowindow 607")
			    });
			});
		}



		class TextMarker extends google.maps.OverlayView {
			constructor(position, text, map, hotel) {
			    super();
			    this.position = position;
			    this.text = text;
			    this.map = map;
			    this.hotel = hotel;
			    
			    // Define a property to hold the marker's div
			    this.div = null;

			    // Explicitly call setMap on this overlay
			    this.setMap(map);

			    var htmlContent = '<div style="min-height:260px; height:260px; width:200px; display:flex; flex-direction:column; justify-content:center"><div>' +
			    		'<div style="width:200px; height:125px; background-size:cover; background-position:center; background-image:url(\'' + hotel.photo + '\')"></div>' +
			    		'<h4 style="font-size:16px; margin-top:5px;">' + hotel.name + '</h4>' +
	                    '<h5 style="font-size:14px;">Discounted rates starting at: $' + hotel.price + ' Per Night</h5>' +
	                      // Add more details you want to show in the popup
	                    '<button id="'+ hotel.id +'-btn" type="button" class="btn btn-primary btn-availability"' +
	                    'data-accommodation-id="' + hotel.accommodationID + '" data-price="' + hotel.price + '" data-msrpd="' + hotel.msrpd + '" data-searchid="'+ hotel.id +'"><i class="fa fa-spin fa-sync"></i> View Offers</button></div></div>';

	            this.infoWindow = new google.maps.InfoWindow({
	                content: htmlContent,
	            });
	            this.id = hotel.id + "-m";

	            this.invisibleMarker = new google.maps.Marker({
				    position: this.position,
				    map: this.map,
				    visible: false // Make the marker invisible
				});
	        }

			onAdd() {
			    var div = document.createElement('div');

			    this.div = div;
			    
			    div.setAttribute('id', this.id);
			    div.className = "map-price";
			    div.style.position = 'absolute';
			    div.style.padding = '5px';
			    div.style.border = '1px solid #222222';
			    div.style.color="white";
			    div.style.backgroundColor = '#222222';
			    div.style.textAlign = 'center';
			    div.style.fontWeight = 'bold';
			    div.style.borderRadius = '8px';
			    div.style.fontSize = '14px';
			    div.style.cursor = 'pointer';
			    div.style.zIndex = '999';

			    div.textContent = this.text;

			    // Add the element to the "overlayLayer" pane
			    var panes = this.getPanes();
			    panes.overlayLayer.appendChild(div);

			    var that = this;

			    this.getPanes().overlayMouseTarget.appendChild(div);

			    document.querySelector('#map').addEventListener('click', function(e) {
				    // Check if the event target (the clicked element) has the class 'gm-ui-hover-effect'

				    if (e.target.classList.contains('gm-ui-hover-effect')) {

				    	$(".gm-style-iw-c").hide();
					    window.lastInfoWindow.close();
				        
				        // Prevent the click from bubbling up

				        e.stopPropagation();
				    } else {
				    	$(".gm-style-iw-c").show();
				    }
				}, true); // Use capture phase to handle the event early
			    
	           	google.maps.event.addDomListener(div, 'click', function() {

			        // Close any previously opened InfoWindow
			        if (window.lastInfoWindow) {
			            window.lastInfoWindow.close();
			        }

			    
			         // Set the content of the InfoWindow to whatever you need
        			that.infoWindow.setContent(that.infoWindow.content); // We already set the content when creating the InfoWindow

        			// Open the InfoWindow
        			// Replace this line in the click event listener
					that.infoWindow.open(that.map, that);

					// With this line
					that.infoWindow.open(that.map, that.invisibleMarker);

					window.lastInfoWindow = that.infoWindow;

			    });
			}

			draw() {
			    // We use the south-west and north-east
			    // coordinates of the overlay to peg it to the correct position and size.
			    // To do this, we need to retrieve the projection from the overlay.
			    var overlayProjection = this.getProjection();

			    // Retrieve the coordinates of the marker
			    // in the div's position
			    var sw = overlayProjection.fromLatLngToDivPixel(this.position);

			    // Position the div to the desired position
			    var div = this.div;
			    div.style.left = sw.x + 'px';
			    div.style.top = sw.y + 'px';
			}

			onRemove() {
			    this.div.parentNode.removeChild(this.div);
			    this.div = null;
			}
		}

	    $('body #hotel-filters').on("click",'.filter-brands',function() {
			var carriers = [];
			$(".filter-brands").each(function(){
				if($(this).is(":checked")){
					carriers.push($(this).val());
				}
			});

			$(".hotel-card").each(function(){
				var flightCard  = $(this);
				if(carriers.length == 0){
					$(this).removeClass("remove");
				} else {
					var brand = $(this).data('brand');

					let isFound = carriers.includes(brand);

					if(!isFound){
						$(this).addClass("remove");
					}
					else{
						$(this).removeClass("remove");
					}
				}
			});

			filterTripsEval();
        });

        function filterTripsEval(){
        	var trips = [];

        	$(".filter-trips").each(function(){
				if($(this).is(":checked")){
					trips.push($(this).val());
				}
			});

			$(".flight-card").each(function(){
				var trip = $(this).attr("data-total-flights");
				
				if(trips.length == 0){
					$(this).show();
				} else {
					if(!trips.includes(trip)){
						$(this).hide();
					}
				}
			});
        }

        getHotels("{{ $path }}");

        function showGalleries(){
        	$(".swiper").each(function(){
	        	var slider = $(this).attr("id");

	        	const swiper = new Swiper('#' + slider, {
				  // Optional parameters
				  direction: 'horizontal',
				  loop: true,

				  // If we need pagination
				  pagination: {
				    el: '.swiper-pagination',
				  },

				  // Navigation arrows
				  navigation: {
				    nextEl: '.swiper-button-next',
				    prevEl: '.swiper-button-prev',
				  },

				  // And if we need scrollbar
				  scrollbar: {
				    el: '.swiper-scrollbar',
				  },
				});
	        });
        }

	    async function getHotels(url){
	    	url = url.replace(/&amp;/g, '&');
			$.ajax({
                url: "/hotels/search/results" + url,
                type: 'post',
                processData: true,
                async: true,
              	dataType: 'json',
                success: function (data) {
                	$("#hotels-results").html("");
                	if(data.length == 0){
                		
                		return swal.fire({
							title: 'Nothing found',
							text: 'We could not find any hotels. This usually happens when there are no accommodations at or near the entered destination. Please try changing your trip plan by starting a new search.',
							imageUrl: '/assets/img/404.png',
							imageWidth: 210,
							imageHeight: 130,
							showCancelButton: true,
							imageAlt: 'Custom image',
		  					confirmButtonText: 'New Search',
						}).then((result) => {
						  if (result.isConfirmed) {
						  	window.location.href = "/hotels"
						  }
						});
			        }

                	dismissFlightsLoad();

                	$.when(
	                    $.each(data, function(i, item) {

	                    	hotelCoordinates.push({
	                    		lat:  item.lat,
	                    		lng: item.long,
	                    		photo: item.photo,
	                    		price: item.lowestPrice,
	                    		savings: item.totalSavings,
	                    		id: item.offerID,
	                    		accommodationID: item.accommodationID,
	                    		name: item.name,
	                    		msrpd: item.msrpd
	                    	});

	                        $("#hotels-results").append(item.html);
	                    })
		            ).then(function () {
		            	initMap(hotelCoordinates)
		            	$("#show-map").show();
			            getCarriers();
			        });
            	},
            	error: function(data){
	            	return swal.fire({
						icon: 'warning',
						title: 'Something went wrong',
						text: 'There was an issue getting availabile hotels. Please refresh the page or start a new search.',
						confirmButtonText: 'Refresh',
						showCancelButton: true,
					}).then((result) => {
					  if (result.isConfirmed) {
					  	window.location.href = window.location.href
					  }
					});	
            	}
            });
		}

		function dismissFlightsLoad(){
			$("#flights-loader").remove();
		}

		$("body").on("click", ".btnClose",function(){
			if(!$(this).hasClass("gm-ui-hover-effect")){
				$("#continuePreview").html('<h1 class="pendingLoader"><i class="fa fa-sync fa-spin"></i></h1>');
				$("#pendingHotelModal").modal("hide");
			}
		});

		$("body").on("click", ".gm-ui-hover-effect",function(){

			$("#mapModal").modal("show");
		});


		

		function verifyOffers(searchID, accommodationID){
			$.ajax({
                url: "/hotels/search/result/verify",
                type: 'post',
                processData: true,
              	dataType: 'json',
              	data:{
              		searchID: searchID,
              		accommodationID: accommodationID
              	},
                success: function (data) {
                	if(data == 200){
                		// allow extended preview

                		$("#continuePreview").html('<br> Press the button below to get more details about this hotel<br><br><a href="/hotels/search/result/' + searchID + '/offer/' + accommodationID + '" id="extendedPreviewLink" class="btn btn-success d-block" target="_blank"><i class="fa fa-hotel"></i> Continue With Room Selection</a>');

                		//
                	} else {
                		$("#continuePreview").html('<br><br><br><button class="btnClose btn  btn-dark btn-wide" style="width:100%;">CLOSE</button>');
                	}
                }
            });
		}

		function getCarriers(){
			$("#sorter-row").show();
			$("#filters-menu").show();

			var carriers = [];

			$(".hotel-card").each(function(){
				var carrier = $(this).attr("data-brand");
				carriers.push(carrier);
			});

			carriers = carriers.filter(function (value, index, array) { 
			  return array.indexOf(value) === index;
			});

			$("#hotel-filters").html("");

			$("#hotel-filters").append("<h5>By Brand</h5><ul style='list-style:none !important;' id='flight-filters-carriers'>");

			for(i = 0; i < carriers.length; i++){
				if(carriers[i] != ""){
					$("#flight-filters-carriers").append("<li><input type='checkbox' class='filter-brands' value='" + carriers[i] +"'/> " + carriers[i] +" </li>");
				}
			}

			$("#hotel-filters").append("</ul>");

			getPlanes();
		}

		$("#sorter").change(function(){
			var sorter = $(this).val();
			sortResults(sorter);
		});

		function sortResults(sortType){

			if(sortType == "lowest"){
				var result = $('#hotels-results .hotel-card').sort(function (a, b) {
				  var contentA =parseInt( $(a).data('price'));
				  var contentB =parseInt( $(b).data('price'));
				  return (contentA < contentB) ? -1 : (contentA > contentB) ? 1 : 0;
				});
			}
			if(sortType == "savings"){
				var result = $('#hotels-results .hotel-card').sort(function (a, b) {
				  var contentA =parseInt( $(a).data('savings'));
				  var contentB =parseInt( $(b).data('savings'));
				  return (contentA > contentB) ? -1 : (contentA < contentB) ? 1 : 0;
				});
			}
			if(sortType == "reviews"){
				var result = $('#hotels-results .hotel-card').sort(function (a, b) {
				  var contentA =parseInt( $(a).data('reviews'));
				  var contentB =parseInt( $(b).data('reviews'));
				  return (contentA > contentB) ? -1 : (contentA < contentB) ? 1 : 0;
				});
			}
			$('#hotels-results').html(result);
			$("#hotels-results").animate({scrollTop: $("#hotels-results").offset().top});
		}

		function getPlanes(){
			// to allow filtering but number of number of stops
			$("#hotel-filters").append("<h5></h5><ul style='list-style:none !important;' id='flight-filters-trips'>");

			var stops = [];

			$(".flight-card").each(function(){
				var stop = $(this).attr("data-total-flights");
				stops.push(stop);
			});

			stops = stops.filter(function (value, index, array) { 
			  return array.indexOf(value) === index;
			});

			stops = stops.sort();

			for(i = 0; i < stops.length; i++){
				if(i == 0){
					$("#flight-filters-trips").append("<li><input type='checkbox' class='filter-trips' value='" + stops[i] +"'/> Direct</li>");
				} else {
					$("#flight-filters-trips").append("<li><input type='checkbox' class='filter-trips' value='" + stops[i] +"'/> " + (parseInt(stops[i]) - 1) + " </li>");
				}
			}

			$("#hotel-filters").append("</ul>");
		}


		$("body").on("click", ".btn-availability", function(){
			var btn = $(this);
			btn.addClass("disable-while-loading");
			var searchID = $(this).data("searchid");
			var price = $(this).data("price");
			var msrpd = $(this).data("msrpd");
			var accommodationID = $(this).data("accommodation-id");

			getOfferStatus(searchID, accommodationID).then(offer => {
				if(offer == 200){
	        		btn.removeClass("disable-while-loading");
	        		updateSearchSelection(price, searchID, msrpd);
	        		return navigate('/hotels/search/result/'+ searchID +'/offer/' + accommodationID, true);
	        	} else {
	        		btn.removeClass("disable-while-loading");
	        		btn.removeClass("btn-availability");
	        		btn.removeClass("btn-primary");
	        		btn.addClass("btn-warning");
	        		btn.text("No Longer Available");
	        	}
			});

			var offer = getOfferStatus(searchID, accommodationID);
			var verified = verifyOffers(searchID, accommodationID);
		});

		function getOfferStatus(searchID, accommodationID){
			var offerStatus = $.ajax({
                url: "/hotels/search/result",
                type: 'post',
                processData: true,
              	dataType: 'json',
              	data:{
              		id: searchID,
              		hotel: accommodationID
              	},
                success: function (data) {
                	if(data == 200){
                		return true;
                	} else {
                		return false;
                	}
                },
                error: function(){
                	console.log("error");
                	return false;
                }
            });

            return offerStatus;
		}

		function updateSearchSelection(selectedAmount, selectedOffer, msrpd){
			$.ajax({
                url: "/hotels/search/update/selection",
                type: 'post',
                processData: true,
              	dataType: 'json',
              	data:{
              		selectedAmount: selectedAmount,
              		selectedOffer: selectedOffer,
              		msrpd: msrpd
              	},
                success: function (data) {

                }
            });
		}

		setTimeout(function(){
			setExpiry();
		}, 1200000);

		function setExpiry(){
			// after 10 minutes, all deals are marked as expired.

			swal.fire({
				icon: 'warning',
				title: 'Offers expired',
				text: 'Hotel prices move fast! To ensure you see the latest prices and availability, please refresh the page',
				confirmButtonText: 'Refresh',
				showCancelButton: false,
			}).then((result) => {
			  if (result.isConfirmed) {
			  	window.location.href = window.location.href.replace("#","")
			  }
			});

			$(".btn-availability").each(function(){
				$(this).removeClass("btn-primary");
				$(this).addClass("btn-warning");
				$(this).text("Deal Expired");
				$(this).attr("href","#");
				$(this).attr("target","");
				$(this).attr("disabled","disabled");
			});

		}

		function navigate(href, newTab) {
		   var a = document.createElement('a');
		   a.href = href;
		   if (newTab) {
		      a.setAttribute('target', '_blank');
		   }
		   a.click();
		}

		$("body").on('click', '.priceMatchPop', function(){
	    	$("#MatchexplainerModal").modal("toggle");
	    });
	</script>
@endsection