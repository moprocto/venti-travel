@extends('layouts.loan-master')

@section('css')
    <style type="text/css">
        @media(max-width: 1368px ){
            .tb-full-width{
                width: 100%;
            }
        }
    
    tr.highlight{
        background: #282e2e;
        font-weight: bold;
    }
    .modal-header, .modal-body, .modal-footer{
        background: #181818;
        border: none;
    }
    .modal .about-body-icon-cnt{
        margin: 0 auto;
        padding-right: 10px;
    }
    .modal-header button{
        color: white;
    }
    #tripModal  .modal-content{
        background-size: cover;
    }
    #tripModal .modal-header, #tripModal .modal-body, #tripModal .modal-footer{
        background-color:  rgba(14, 14, 14, 0.65);
    }
    .slider-horizontal{
            width: 100% !important;
          }
          .slider-tick.round{
            margin-top: -10px;
          }
    </style>
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/11.0.2/css/bootstrap-slider.min.css">
@endsection

@section('hero')

<!-- Cover Start -->
<section id="cover" style="background-color: #fefefe;">
    <div class="container">
        <div class="row">
            <div class="col-md-7 cover-image align-self-center d-none d-md-flex d-lg-flex">
                <img src="/assets/img/ventiapp-hero.png" class="img-fluid" alt="product">
            </div>
            <div class="col-md-12 cover-image align-self-center d-xs-flex d-sm-flex d-md-none d-lg-none" style="margin-top: 50px;">
                <img src="/assets/img/venti-hero-mobile.png" class="img-fluid" alt="product">
            </div>
            <div class="col-md-5 align-self-center cover-body">
                <h1 id="apply" class="text-dark" style="color:black;"> Saving for Travel Made Easy and Fun</h1>
                <p></p>
                <h5 class="text-dark" style="color:black;">Unlock rewards and free flights as you save for your next big trip. Early birds get free membership for life.</h5>
                @include('forms.waitlist', ["dark" => true])
            </div>
        </div>
    </div>
</section>
<!-- Cover End -->

@endsection

@section('content')

 <!-- About Start -->
    <section id="about" style="padding-bottom: 0;">
        <div class="container full-width">
           <!-- <div class="row">
                <div class="col-md-6 full-width d-md-none d-lg-flex upperTile" bg-image="/assets/loan/img/maldives-bg-square.jpg">
                    <img src="/assets/loan/img/maldives-bg-square.jpeg" style="width: 100%;">
                </div>
                <div class="col-md-6 full-width d-none d-md-flex d-lg-none upperTile" bg-image="/assets/loan/img/maldives-bg-square.jpg">
                    <img src="/assets/loan/img/maldives-bg-tall.jpg" style="width: 100%;">
                </div>
                <div class="col-md-6 text-center pd-20">
                    <h2 class="section-head-title">Massive Savings. Guaranteed.</h2>
                    <p class="section-head-body">Whether it's a honeymoon in the Maldives, new year celebrations in Thailand, or trekking the hills of coastal Greece, we save you up to 60% with our fully-customizable packages. Every venti trip includes heavily discounted: </p>
                    <ul style="font-size: 20px; list-style: none; line-height: 2; margin-left:-32px;">
                        <li>Airfare</li>
                        <li>Lodging</li>
                        <li>Transportation</li>
                        <li>Food & Drinks</li>
                        <li>Tours & Activities</li>
                        <li>Trip Insurance</li>
                    </ul>

                    <br>

                    <p>The only thing that should be on your mind is having the best time of your life</p>

                    <div class="row about-body">
                        <div class="col-lg-3 col-md-12 col-sm-6 about-body-item d-flex mobile-block align-items-center">
                            <div class="tb-full-width">
                                <div class="about-body-icon-cnt" data-icon="fa-plane">
                                    <i class="material-icons-round about-body-icon fas fa-plane text-small"></i>
                                    <span class="about-body-text-head">Airfare</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-12 about-body-item d-flex mobile-block align-items-center desktop-ml-20">
                            <div class="tb-full-width">
                                <div class="about-body-icon-cnt" data-icon="fa-ship">
                                    <i class="material-icons-round about-body-icon fas fa-ship text-small"></i>
                                    <span class="about-body-text-head">Cruises</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-12 about-body-item d-flex mobile-block align-items-center desktop-ml-20">
                            <div class="tb-full-width">
                                <div class="about-body-icon-cnt" data-icon="fa-hotel">
                                    <i class="material-icons-round about-body-icon fas fa-hotel text-small"></i>
                                    <span class="about-body-text-head">Lodging</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row about-body">
                        <div class="col-lg-3 col-md-12 about-body-item d-flex mobile-block align-items-center">
                            <div class="tb-full-width">
                                <div class="about-body-icon-cnt" data-icon="fa-car">
                                    <i class="material-icons-round about-body-icon fas fa-car text-small"></i>
                                    <span class="about-body-text-head">Rentals</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-12 about-body-item d-flex mobile-block align-items-center desktop-ml-20">
                            <div class="tb-full-width">
                                <div class="about-body-icon-cnt" data-icon="fa-taco">
                                    <i class="material-icons-round about-body-icon fas fa-taco text-small"></i>
                                    <span class="about-body-text-head">Food</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-12 about-body-item d-flex mobile-block align-items-center">
                            <div class="tb-full-width">
                                <div class="about-body-icon-cnt" data-icon="fa-cocktail">
                                    <i class="material-icons-round about-body-icon fas fa-cocktail text-small"></i>
                                    <span class="about-body-text-head">Drinks</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row about-body desktop-hide">
                        <div class="col-md-12 text-center">
                            <br>
                                <h5>Shopping, activities, tours, cash, and so much more!</h5>
                        </div>
                    </div>

                    <div class="row about-body mobile-hide">
                        <div class="col-lg-3 col-md-12 about-body-item d-flex mobile-block align-items-center">
                            <div class="tb-full-width">
                                <div class="about-body-icon-cnt">
                                    <i class="material-icons-round about-body-icon fas fa-shopping-bag text-small"></i>
                                    <span class="about-body-text-head">Shopping</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-12 about-body-item d-flex mobile-block align-items-center desktop-ml-20">
                            <div class="tb-full-width">
                                <div class="about-body-icon-cnt">
                                    <i class="material-icons-round about-body-icon fas fa-shuttle-van text-small"></i>
                                    <span class="about-body-text-head">Shuttles</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-12 about-body-item d-flex mobile-block align-items-center desktop-ml-20">
                            <div class="tb-full-width">
                                <div class="about-body-icon-cnt">
                                    <i class="material-icons-round about-body-icon fas fa-skiing"></i>
                                    <span class="about-body-text-head">Activities</span>
                                </div>
                            </div>
                        </div>
                         
                    </div>
                    <div class="row about-body mobile-hide">
                        <div class="col-lg-3 col-md-12 about-body-item d-flex mobile-block align-items-center">
                            <div class="tb-full-width">
                                <div class="about-body-icon-cnt">
                                    <i class="material-icons-round about-body-icon fas fa-binoculars text-small"></i>
                                    <span class="about-body-text-head">Tours</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-12 about-body-item d-flex mobile-block align-items-center">
                            <div class="tb-full-width">
                                <div class="about-body-icon-cnt">
                                    <i class="material-icons-round about-body-icon fas fa-money-bill text-small"></i>
                                    <span class="about-body-text-head">Cash</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-12 about-body-item d-flex mobile-block align-items-center">
                            <div class="tb-full-width">
                                <div class="about-body-icon-cnt">
                                    <i class="material-icons-round about-body-icon fas fa-stethoscope text-small"></i>
                                    <span class="about-body-text-head">Insurance</span>
                                </div>
                            </div>
                        </div>
                    </div>
                -->
                </div>
            </div>
            <div class="row" style="background-color: #181818">
                <div class="col-md-12 col-lg-7 text-center pd-20" style="padding-bottom: 0; margin-bottom: -3px;" >
                    <div class="section-head-tag">The ultimate travel savings plan for everyone</div>
                    <h2 class="section-head-title" >How it Works</h2>
                    <div class="row text-dark text-left pills-item pills-item-1">
                        <div class="col-md-1 text-center">
                            <span class="badge sand-badge">1</span>
                        </div>
                        <div class="col-md-11">
                            <p class="text-dark text-large">Select one of our pre-packaged trips as a template or create your own from scratch. Start your savings plan by linking a bank account and setting up automatic withdrawals.</p>
                            <p class="text-dark"> Watch your savings grow each month while we match deposits up to 2%</p>
                        </div>
                    </div>
                    <div class="row text-dark text-left pills-item pills-item-2">
                        <div class="col-md-1 text-center">
                            <span class="badge sand-badge">2</span>
                        </div>
                        <div class="col-md-11">
                            <p class="text-dark text-large">As your balance grows, you'll unlock travel-related rewards. See what <strong>free rewards</strong> you can get after you've saved:</p>
                            @include('components.interactive')
                        </div>
                    </div>
                    <div class="row text-dark text-left pills-item pills-item-3">
                        <div class="col-md-1 text-center">
                            <span class="badge sand-badge">3</span>
                        </div>
                        <div class="col-md-11">
                            <p class="text-dark text-large">When you're ready to travel, we'll send you a secure debit card that is packed with cashback opportunities:</p>
                            <ul class="text-large" style="margin-left:-10px;">
                                <li>5% on all purchases at your destination</li>
                                <li>50% on concierge services</li>
                                <li>10% on trip and baggage insurance</li>
                                <li>Up to 40% on travel packages</li>
                            </ul>
                            <p class="text-dark">You can also decide to withdraw funds back to your bank account.</p>
                        </div>
                    </div>
                    <!--
                    <div class="row text-dark text-left pills-item pills-item-3">
                        <div class="col-md-12">
                            <p class="text-dark text-large">Put that high-interest credit card away! Members that opt to use our exclusive payment plans get:</p>
                            <ul class="text-large" style="margin-left:-10px;">
                                <li>Low-interest financing with payment plans up to 72 months</li>
                                <li>Complimentary seat and room upgrades when available</li>
                            </ul>
                        </div>
                    </div>
                -->
                </div>
                <div class="col-md-5 full-width d-md-none d-lg-flex upperTile" bg-image="/assets/loan/img/morocco-bg-square.jpg">
                    <img src="/assets/loan/img/morocco-bg.jpg" style="width: 100%; max-height: 600px;">
                </div>
                <div class="col-md-12 full-width d-none d-md-flex d-lg-none upperTile">
                    <img src="/assets/loan/img/morocco-bg-wide.jpg" style="width: 100%;">
                </div>

            </div>
            <div class="row">
                <div class="col-md-6">
                    <p class="text-dark text-large">Select one of our pre-packaged trips as a template or create your own from scratch. Start your savings plan by linking a bank account and setting up automatic withdrawals.</p>
                        <p class="text-dark"> Watch your savings grow each month while we match deposits up to 2%</p>
                    </div>
                </div>
                <div class="col-md-6">
                    <img src="/assets/loan/img/morocco-bg-wide.jpg" style="width: 100%;">
                </div>
            </div>
            <!--
            <div class="row" id="Waitlist">
                <div class="col-md-6 full-width mobile-hide">
                    <img src="/assets/loan/img/iceland-bg.jpg" style="width: 100%;">
                </div>
                <div class="col-md-12 col-lg-6 section-head text-center sign-up-box" style="align-self:center;">
                    <div class="section-head-tag">See The World</div>
                    <h2 class="section-head-title">Travel Without Limits</h2>
                    <img src="/assets/loan/img/venti-card-image.png" style="width: 100%; max-width:200px; margin:0 auto;">
                    <br><br>
                    <p class="section-head-body">We're launching in 2022. <br> Early birds may get 0% APR for life with all the perks.</p>
                    <div class="col-lg-12 subscribe-container text-center" id="subscribe">
                        <div class="clear" id="Waitlist"></div>
                        @include('forms.waitlist')
                    </div>
                </div>
            </div>
            -->
        </div>
    </section>
    <!-- About End -->

    <section id="products" style="padding-bottom:30px; background:black;">
        <div class="container">
            <div class="row">
                <div class="col-md-8 offset-md-2 section-head text-center">
                    <h2 class="section-head-title">Need Inspiration?</h2>
                    <!--
                    <p>We worked with the best of the best to design these customizable travel packages to offer out-of-this-world discounts and experiences for our members. Don't see your destination? No worries, our concierge team can create something custom for you. Need a private jet? We'll arrange that, too!</p>
                -->
                    <p>We worked with the best travel designers to come up with these incredible experiences. See how much you'll need to save each month to score these deals. Need something custom? Our elite concierge team is ready to assist! Don't need to save? Buy them outright with discounts of up to 40% off when we launch. Prices below assume a 12-month savings plan. </p>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="products-slider">
                        @include('layouts.rotator-two')
                    </div>
                </div>
                
            </div>
        </div>
    </section>
    <!-- End Products -->

    <section style="background-position: center; background-image: url('/assets/loan/img/brazil-mountians.jpg'); background-size: cover; padding: 100px 20px;">
        <div class="container full-width">
            <div class="row">
                <div class="col-12 text-center">
                    <div class="section-head-tag">Savings. Perks. Exclusivity.</div>
                    <h2 class="section-head-title">Get in Early!</h2>
                    @include('forms.waitlist')
                </div>
            </div>

        </div>
    </section>

    <!-- Start FAQs -->
    
    @include('layouts.faq-two')

    <!-- End FAQs -->

    @include('forms.feedback')


    @include('components.modals')

@endsection

@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/11.0.2/bootstrap-slider.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $(".about-body-icon-cnt").click(function(){
                var tileText = $(this).find(".about-body-text-head").text();
                var tileIcon = $(this).attr("data-icon");
                console.log(tileText);
                $("#tilesModal").find("#tilesTitle").text(tileText);
                $("#tilesModal").find(".material-icons-round").attr("class", "material-icons-round about-body-icon fas " + tileIcon);
                
                if(tileText == "Airfare" || tileText == "Cruises" || tileText == "Lodging" || tileText == "Rentals"){
                    $("#tilesModal").find("#tilesText").text("venti members can save up to 25% or more in airfare, cruises, lodging, and vehicle rentals by purchasing through one our packages.");
                    $("#tilesModal").modal("show");
                }

                if(tileText == "Food" || tileText == "Drinks"){
                    $("#tilesModal").find("#tilesText").text("venti's all-inclusive travel packages generally include unlimited food and drinks.");
                    $("#tilesModal").modal("show");
                }

                
            });

            $(".owl-item").click(function(){
                var bgImage = $(this).find(".img-fluid").attr("bg-image");
                $("#tripModal").find(".modal-content").attr("style", "background-image: url('" + bgImage + "');")
                $("#tripModal").modal("show");

            });
            $(".upperTile").click(function(){
                var bgImage = $(this).attr("bg-image");
                $("#tripModal").find(".modal-content").attr("style", "background-image: url('" + bgImage + "');")
                $("#tripModal").modal("show");
            });

            $("#submit-feedback").click(function(){
                if($("#feedback-text").val() == ""){
                    alert("Please provide some feedback");
                } else {
                    $("#feedback-form").submit();
                    $("#feedback-text").val("");
                    alert("Thank you so much! :)");
                }
            });

            $(".calculate").on("change", function(){

          var poolAmount = $("#pool-amount").val();
          var prize = "👀👉";

          if(poolAmount == 1){
            prize = "Package Raffle";
          }
          else if(poolAmount == 2){
            prize = "Domestic Plane Ticket ✈️";
          }
          else if(poolAmount == 3){
            prize = "Travel Giftbox";
          }
          else if(poolAmount == 4){
            prize = "60% Off Travel Package";
          }
          
          $("#reward").text(prize);

        });


      function calculatePayout(poolAmount, poolSize, poolRate){
        console.log("size " + poolSize);
        console.log("amount " + poolAmount);
        console.log("rate " + poolRate);

        var top = poolSize * poolAmount;
        var bottom = poolSize * poolRate;

        console.log("top " + top);
        console.log("bottom " + bottom);

        return top/bottom;
      }
        });
    </script>
@endsection
    
