@extends("layouts.curator-master")


@section("css")
   <link href="/css/lightbox/lightbox.min.css" rel="stylesheet" />
   <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
   <link href="/assets/css/store.css" rel="stylesheet" />
   <style type="text/css">
      @media screen and (max-width:  600px){
         .grid{
            margin-top: 25px;
         }
         .card-row{
         	margin: 0;
         }
         .grid .card{
         	min-width: 170px !important;
         	min-height: 170px !important;

         }
         .gutter-sizer { width: 4%; }
         .grid-sizer { width: 22%; }
      }
       @media screen and (min-width:  600px){
         .grid{
         
         }
         .gutter-sizer { width: 1%; }
         .grid-sizer { width: 50%; }
         .grid-item{
            width: 33%;
            padding: 70px 10px;
         }

      }
      .grid .card{
         border: none;
         box-shadow: 0 1px 4px rgb(0 0 0 / 15%);
      }
      .grid .card img{
         border-top-left-radius: 20px;
         border-top-right-radius: 20px;
      }
      .grid .card, .grid .card .card-body{
         border-radius: 20px;
      }
      h4.title{
         display: flex;
         justify-content: space-between;
         align-items: center;
         align-content: center;
      }
      .category{
         text-transform: uppercase;
         background: black;
         color: white;
         font-size: 12px;
         padding: 5px 10px;
         border-radius: 10px;
         margin-top: -3px;
      }
      .grid{
         
      }
      .form-group .form-label{
         text-transform: uppercase;
    font-size: 10px;
    color: black;
    font-weight: 900;
    margin-bottom: 20px;
      }
   </style>
@endsection

@section("content")

<div id="page-content">
    <div class="gradient-wrap">
	    <!--  HERO -->
	    	<div class="black position-relative hero-section mb-4" style="margin: 20px 4em;">
	        	<div class="row" style="justify-content: center;">
	            	<div class="col-md-4 text-center">
	                    <h1 class="mb-3" style="font-weight:600 !important;">
                    			Account Deletion Form
                    		</h1>
                    	<div class="card">
                    		<div class="card-body text-left">
                    			@error('success')
                					<div class="alert alert-success">
                						<span>Your message was sent! For your security, we will require you to confirm the account deletion request via email.</span>
                					</div>
                				@enderror
                    			<form method="POST" action="/contact/delete">
                    				@CSRF
                    				@php
                    					$random = ( rand(10, 100) );

                    					if($random % 2 !== 0) {
										    if($random == 100) {
										        $random = $random - 2;
										    } else {
										        $random = $random + 1;
										    }
										}

										$solution = $random / 2;
										echo "<input type='hidden' name='ran' value='VENTI$random'>";
                    				@endphp
                    				
                    				<div class="form-group">
                    					<label class="form-label">Email:</label>
                    					<input type="text" class="form-control" name="email" placeholder="your@email.com" required="" value="{{ old('email') }}">
                    				</div>
                    				<div class="form-group">
                    					<label class="form-label">Why are you deleting your account?</label>
                    					<select class="form-control" name="subject" required="">
                    						<option>-- Please Select --</option>
                    						<option value="I have no use for this platform" @if(old('subject') == 'I have no use for this platform') selected @endif>I have no use for this platform</option>
                    						<option value="I do not feel safe on Venti" @if(old('subject') == 'I do not feel safe on Venti') selected @endif>I do not feel safe on Venti</option>
                    						<option value="I plan on returning when there are more people" @if(old('subject') == 'I plan on returning when there are more people') selected @endif>I plan on returning when there are more people</option>
                    						<option value="I do not know how to use the platform" @if(old('subject') == 'I do not know how to use the platform') selected @endif>I do not know how to use the platform</option>
                    						<option value="Something else" @if(old('subject') == 'Something else') selected @endif>Something else</option>
                    					</select>
                    				</div>
                    				<div class="form-group">
                    					<label class="form-label">What should we do with your data</label>
                    					<select class="form-control" name="message" required="">
                    						<option value="Maintain my data in case I return" @if(old('subject') == 'Maintain my data in case I return') selected @endif>Maintain my data in case I return</option>
                    						<option value="Delete all data associated with my account" @if(old('subject') == 'Delete all data associated with my account') selected @endif>Delete all data associated with my account</option>
                    					</select>
                    				</div>
                    				<div class="form-group">
                    					<label>Quick Math Test: What is {{ $random }} / 2?</label>
                    					@error('incorrectAnswer')
	                    					<div class="alert alert-danger">
	                    						<span>Your answer was incorrect.</span>
	                    					</div>
	                    				@enderror
                    					<input type="number" class="form-control" required min="5" max="50"  name="answer">
                    				</div>
                    				<div class="form-group">
                    					<button type="submit" name="submit" class="btn btn-primary">Submit</button>
                    				</div>
                    				<br>
                    				<p>Need help with something else? Reach our support team via email at admin@venti.co</p>
                    			</form>
                    		</div>
                    	</div>
                    </div>
	            </div>
	        </div>
	    </div>
	</div>
</div>

@endsection