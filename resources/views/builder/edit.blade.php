@extends('layouts.curator-master')

@section('css')
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
	<script src='https://api.mapbox.com/mapbox-gl-js/v1.8.0/mapbox-gl.js'></script>
	<link href='https://api.mapbox.com/mapbox-gl-js/v1.8.0/mapbox-gl.css' rel='stylesheet' />
	<script src='https://tiles.locationiq.com/v2/js/liq-styles-ctrl-gl.js?v=0.1.6'></script>
	<link href='https://tiles.locationiq.com/v2/css/liq-styles-ctrl-gl.css?v=0.1.6' rel='stylesheet' />
    <script src="https://tiles.locationiq.com/v2/libs/gl-geocoder/4.5.1/locationiq-gl-geocoder.min.js?v=0.2.2"></script>
	<link rel="stylesheet" href="https://tiles.locationiq.com/v2/libs/gl-geocoder/4.5.1/locationiq-gl-geocoder.css?v=0.2.2" type="text/css" />
	<style type="text/css">
		.hide{
			display: none;
		}
		#plan{
			padding: 20px;
		}
		iframe h6{
			font-size: 20px !important;
		}
	</style>
	<script src="https://cdn.tiny.cloud/1/f1cdeokivvyzyohrpb09kwayr4k53ovtgo1849tqerl354st/tinymce/6/tinymce.min.js" referrerpolicy="origin"></script>
	<div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v16.0&appId=1298822020928144&autoLogAppEvents=1" nonce="jHlxbXfP"></script>
@endsection

@section('content')
	<div id="page-content">
	    <div class="gradient-wrap">
	        <div class="container hero-section mb-4 mt-4">
	            <div class="row justify-content-center">
	                <div class="col-md-8 text-center mt-4" id="stepFour">
	                	<div class="card text-left" style="border-radius:20px;">
	                		<div class="card-body">
	                			<div style="display: flex; width: 100%; justify-content:space-between; align-items:center;">
	                				<div>
	                			<div class="fb-like" data-href="https://venti.co/plans/{{ $plan['planID']}}" data-width="" data-layout="" data-action="" data-size="" data-share="true"></div>
	                			<br>
	                			Your Travel Plan Link: <a target='_blank' href='https://venti.co/plans/{{ $plan["planID"]}}'>https://venti.co/plans/{{ $plan["planID"]}}</a><br>
	                		</div>
	                			<a href="/create" class="btn btn-black">Create New Plan</a>

	                		</div>
	                		
	                		</div>
	                	</div>
	                	<div class="card text-left mt-4" style="border-radius:20px;">
	                			<div class="card-body">
	                				<button type="button" id="newImage" class="btn btn-white"><i class="fa fa-spinner"></i> New Image</button>
	                				<div id="background" style="width: 100%; border-radius: 20px; min-height: 400px; background-position:center; background-size: cover; background-image:url('{{ $plan['imageUrl'] }}')">
	                				</div>
	                				<input type="hidden" id="imageUrl" value="{{ $plan['imageUrl'] }}" />
	                				<input type="hidden" id="imageAuthor" value="{{ $plan['imageAuthor'] }}" />
	                				<input type="hidden" id="imageAuthorProfile" value="{{ $plan['imageAuthorProfile'] }}" />
	                				<div class="row" style="padding-top:15px; padding-bottom: 15px;">
		                				<div class="form-group col-md-4">
		                					<label class="form-label">Destination</label>
		                					<input type="text" name="destination" id="destination" class="form-control" required="" value="{{ $plan['destination'] }}">
		                				</div>
		                				<div class="form-group col-md-4">
		                					<label class="form-label">Country</label>
		                					<input type="text" name="country" id="country" class="form-control" required="" value="{{ $plan['country'] }}">
		                				</div>
		                				<div class="form-group col-md-4 text-right">
		                					<br>
		                					<button type="button" id="update" class="btn btn-primary">UPDATE</button>
		                				</div>
	                				</div>
	                				<textarea id="plan">{!! str_replace('h6>','h4>',$plan["answer"]) !!}</textarea> 
	                				
	                				<br><p>Venti is not responsible for any damages or injury associated with following this itinerary. You are strongly encouraged to double-check all references to ensure their accuracy.</p>
	                			</div>
	                	</div>
	                </div>
	            </div>
	        </div>
	    </div>
	</div>
@endsection

@section('js')
<script>
	tinymce.init({
      selector: 'textarea#plan',
      height: 1100,
      menubar: false,
      plugins: [
        'link'
      ],
      toolbar: 'undo redo | formatselect | ' +
      'bold italic link | ' +
      'removeformat',
      content_style: 'body { font-family:Helvetica,Arial,sans-serif; font-size:14px }'
    });

    jQuery(document).ready(function ($) {
		$.ajaxSetup({
         headers: {
            'X-CSRF-TOKEN': '{{ csrf_token() }}'
         }
      	});
		var country = $("#country").val();
      	var destination = $("#destination").val();

      	var imageCache = {};
      	var locationCache = destination + " " + country;


      	$("#newImage").click(function(){
      		var country = $("#country").val();
      		var destination = $("#destination").val() + " " + country;

      		if(destination == locationCache){
      			return swapImage();
      		}

      		setImageCache(destination, country, true);
      		
      	});

      	function swapImage(){
      		var button = $("#newImage").find("i");
      		button.addClass("fa-spin");

      		// get a random image

            var random = randomRange(0, 20);

            if(imageCache[random] == undefined){
            	console.log("hit a snag");
            	random = 0;
            	button.removeClass("fa-spin");
            	//return alert("error");
            }

            $("#imageUrl").val(imageCache[random].urls.regular);
            $("#imageAuthor").val(imageCache[random].user.links.html);
            $("#imageAuthorProfile").val(imageCache[random].user.links.name);

            button.removeClass("fa-spin");

            $("#background").animate({
			    opacity: 0
			}, 100, function() {
			    // Callback
			    $(this).css("background-image", "url(" + imageCache[random].urls.regular + ")").promise().done(function(){
			        // Callback of the callback :)
			        $(this).animate({
			            opacity: 1
			        }, 600)
			    });    
			});
      	}

      	function setImageCache(destination, country, swap){
      		destination = destination + " " + country;
      		$.ajax({
                url: "https://api.unsplash.com/search/photos?client_id={{ env('UNSPLASH_KEY') }}&per_page=30&query=view of " + destination,
                type: 'get',
                success: function (data) {
                    imageCache = data.results;

                    if(swap){
                    	locationCache = destination + " " + country;
                    	swapImage();
                    }
                }
            });
      	}

      	setImageCache("{{ $plan['destination'] }}","{{ $plan['country'] }}", false);



      	function randomRange(myMin, myMax) {
              return Math.floor(Math.random() * (myMax - myMin + 1) + myMin);
            }

      	$("#update").click(function(){
      		var country = $("#country").val();
      		var destination = $("#destination").val();
      		var answer = tinymce.get("plan").getContent();

      		$.ajax({
                url: "/plan/update/{{ $plan['planID'] }}",
                type: 'post',
                processData: true,
                data:{
                    country: country,
                    destination: destination,
                    answer: answer,
                    imageUrl: $("#imageUrl").val(),
                    imageAuthor: $("#imageAuthor").val(),
                    imageAuthorProfile: $("#imageAuthorProfile").val()
                },
                dataType: 'json',
                success: function (data) {

                	if(data == 500){
                		return alert("We could not find this trip plan.");
                	}

                	return swal({
				  icon: 'success',
				  title: 'All Set!',
				  text: 'Your changes have been saved successfully',
				})
                }
            });
      	})
    });
</script>
@endsection