@extends('layouts.curator-master')

@section('css')
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
	<script src='https://api.mapbox.com/mapbox-gl-js/v1.8.0/mapbox-gl.js'></script>
	<link href='https://api.mapbox.com/mapbox-gl-js/v1.8.0/mapbox-gl.css' rel='stylesheet' />
	<script src='https://tiles.locationiq.com/v2/js/liq-styles-ctrl-gl.js?v=0.1.6'></script>
	<link href='https://tiles.locationiq.com/v2/css/liq-styles-ctrl-gl.css?v=0.1.6' rel='stylesheet' />
    <script src="https://tiles.locationiq.com/v2/libs/gl-geocoder/4.5.1/locationiq-gl-geocoder.min.js?v=0.2.2"></script>
	<link rel="stylesheet" href="https://tiles.locationiq.com/v2/libs/gl-geocoder/4.5.1/locationiq-gl-geocoder.css?v=0.2.2" type="text/css" />
	<style type="text/css">
		.hide{
			display: none;
		}
		#plan{
			padding: 20px;
		}
		.btn-white{
			color: black !important;
		}
	</style>
	<div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v16.0&appId=1298822020928144&autoLogAppEvents=1" nonce="jHlxbXfP"></script>
@endsection

@section('content')
	<div id="page-content">
	    <div class="gradient-wrap">
	        <div class="container hero-section mb-4 mt-4">
	            <div class="row justify-content-center">
	                <div class="col-md-8 text-center mt-4">
	                	<h2>Trip Planner<sup style="font-size:10px; top:-16px;">BETA</sup></h2>
	                	<p class="text-left">Venti is a social discovery platform that makes it easy to find and create travel groups based on your travel preferences. We've integrated ChatGPT into our platform to make it easer for organizers to create travel plans. Contact <a href="mailto:admin@venti.co">admin@venti.co</a> if you would like your itinerary customized by a professional travel consultant. <br> Submit <a href="https://venti.nolt.io/" target="_blank">feedback here</a>. </p>
	                	<div class="fb-like" data-href="https://venti.co/create" data-width="" data-layout="" data-action="" data-size="" data-share="true"></div>
	                	<div class="card text-left">
	                		<div class="card-body">
	                			<h3>Step One</h3>
	                			<p>Enter the city you'd like Venti to create a travel plan for.</p>
	                			<div class="container">
					        		<div class="search-box" id="search-box" style="width: 100%" name="destination"></div>
						        	<div id="result"></div>
						        </div>
						        <input type="hidden" name="country" id="country" value="">
						        <input type="hidden" name="latitude" id="latitude" value="">
						        <input type="hidden" name="longitude" id="longitude" value="">
						        <input type="hidden" name="destination" id="destination" value="">
						        @php
                					$random = rand(10, 100);

                					if($random % 2 !== 0) {
									    if($random == 100) {
									        $random = $random - 2;
									    } else {
									        $random = $random + 1;
									    }
									}

									$solution = $random / 2;
									
                				@endphp
                				<input type='hidden' name='ran' id="ran" value='VENTI {{ $random }}'>
	                		</div>
	                	</div>
	                </div>
	                <div class="col-md-8 text-center mt-4 hide" id="stepTwo">
	                	<div class="card text-left">
	                		<div class="card-body">
	                			<h3>Step Two</h3>
	                			<p>How many days in <span class="dest"></span> do you need planned?</p>
	                			<div class="container">
					        		<select id="numDays" class="form-control" required="">
					        			<option value="">-- Please Select --</option>
					        			<option value="1">Day Trip</option>
					        			<option value="2">Weekend Trip</option>
					        		</select>
						        </div>
						        <input type="hidden" name="country" id="country" value="">
						        <input type="hidden" name="latitude" id="latitude" value="">
						        <input type="hidden" name="longitude" id="longitude" value="">
						        <input type="hidden" name="destination" id="destination" value="">
	                		</div>
	                	</div>
	                </div>
	                <div class="col-md-8 text-center mt-4 hide" id="stepThree">
	                	<div class="card text-left">
	                		<div class="card-body">
	                			<h3>Step Three</h3>
	                			<p>Just checking to see if you're human :)</p>
	                			<label>Quick Math Test: What is {{ $random }} / 2?</label>
                    			<input type="number" class="form-control" required min="5" max="50"  name="answer" id="answer">
                    			<br>
	                			<button type="button" id="generate" value="GENERATE" class="btn btn-primary"><span id="loader-spin" class="hide"><i class="fa fa-spinner fa-spin"></i></span> GENERATE</button>
	                		</div>
	                	</div>
	                </div>
	                <div class="col-md-8 text-center mt-4 hide" id="stepFour">
	                	<div class="card text-left">
	                		<div class="card-body">
	                			<div id="loader" style="width:100%; text-align:center;">
	                				<div class="loading-box">
	                					<img src="/assets/img/airplane.gif" style="width:200px;">
	                					<h3>Creating Your Travel Plan...</h3>
	                					<p style="text-align: center;">This might take up to five minutes. <br>Please do not navigate away from this page :)</p>
	                				</div>
	                			</div>
	                			<div id="plan"></div>
	                		</div>
	                	</div>
	                </div>
	            </div>
	        </div>
	    </div>
	</div>
@endsection

@section('js')
    
	<script>
		jQuery(document).ready(function ($) {
		$.ajaxSetup({
         headers: {
            'X-CSRF-TOKEN': '{{ csrf_token() }}'
         }
      	});


        locationiq.key = "{{ env("LOCATIONIQ_KEY") }} ";

        //Add Geocoder control to the map
        var geocoder = new MapboxGeocoder({
            accessToken: locationiq.key,
            limit: 5,
            dedupe: 1,
            name: "destination",
            id:"destination",
            getItemValue: function (item) {


                var name = item.place_name;
                console.log(item);
                if(item.address.city !== undefined){
                    if(item.address.country == "USA" || item.address.country == "United States" || item.address.country == "United States of America"){
                        name = item.address.name;
                    } else {
                        name = item.address.name + " " + item.address.country;
                    }
                }
                $("#destination").val(name);
                $("#country").val(item.address.country);
                $("#latitude").val(item.lat);
                $("#longitude").val(item.lon);

                $("#stepTwo").show();
                $(".dest").text(name);

                return item.place_name
            }
        });

        geocoder.addTo('#search-box');

        $("#numDays").on("change", function(){
        	if($(this).val() != ""){
        		$("#stepThree").show();
        	}
        });

        $("#generate").on("click", function(){
        	$("#stepFour").show();
        	$("#loader").show();
        	$("#loader-spin").show();
        	$("#plan").html("");

        	var destination = $("#destination").val();
        	var country = $("#country").val();
        	var lat = $("#latitude").val();
            var long = $("#longitude").val();
            var days = $("#numDays").val();
            var ran = $("#ran").val();
            var answer = $("#answer").val();
            var lodging = $("#lodging:checked").val() ?? 0;
            var food = $("#food:checked").val() ?? 0;
            var airports = $("#airports:checked").val() ?? 0;

            $.ajax({
                url: "/create",
                type: 'post',
                processData: true,
                data:{
                    lat: lat,
                    long: long,
                    country: country,
                    destination: destination,
                    random: ran,
                    days:days,
                    answer: answer,
                    lodging: lodging,
                    food: food,
                    airports: airports
                },
                dataType: 'json',
                success: function (data) {

                	
                	if(data == 500){
                		$("#stepFour").hide();
                		$("#loader-spin").hide();
                		$("#loader").hide();
                		return alert("Your math is wrong :(");
                	}
                	if(data == 501){
                		$("#stepFour").hide();
                		$("#loader-spin").hide();
                		$("#loader").hide();
                		return alert("The operation timed out due to an error. It might be a connection issue. Please try again or contact admin@venti.co");
                	}

                	$("#loader").hide();
                		$("#loader-spin").hide();
                		$("#plan").show();
                		var newHMTL = "<div style='display:flex; justify-content:space-between; align-items:center;'><div>Your Travel Plan Link: <a target='_blank' href='https://venti.co/plans/" + data.planID + "'>https://venti.co/plans/" + data.planID + "</a></div><div><a href='/plan/edit/" + data.planID + "' class='btn btn-white'><i class='fa fa-pencil'></i> Edit</a>&nbsp; <a href='/plan/pdf/" + data.planID +"' class='btn btn-black' target='_blank'><i class='fa fa-download'></i> PDF</a></div></div></div><br><br>";
                		if(data.image != null){
                			newHMTL += '<div class="" style="width: 100%; border-radius: 20px; background-position:center; min-height: 300px; background-size: cover; background-image:url(\'' + data.image +'\')"></div>'
                		}
                    	$("#plan").html(newHMTL + "<br>" + data.answer + "<br><br><p>Venti is not responsible for any damages or injury associated with following this itinerary. You are strongly encouraged to double-check all references to ensure their accuracy.</p>");

                    	return true;
            	}
            });

        });
    });
	</script>
@endsection