@extends("layouts.curator-master")

@section('css')
	<style type="text/css">
		body {
			background: lightgrey;
			font-family: sans-serif;
		}

		.form-rendered #build-wrap {
		  display: none;
		}

		.render-wrap {
		  display: none;
		}

		.form-rendered .render-wrap {
		  display: block;
		}

		#edit-form {
		  display: none;
		  float: right;
		}

		.form-rendered #edit-form {
		  display: block;
		}
		.form-label{
			margin-bottom: 0;
		}
		span.form-helper{
			display: block;
			font-size: 12px;
		}
		.preview-image{
			background-color: whitesmoke;
			min-height: 250px;
			margin-top: 20px;
			border-radius: 20px;
		}
		.city-field, .accomodation-field{
			padding-left: 5em !important;
		}

		 .activity-field, .restaurant-field{
			padding-left: 10em !important;
		}
		.form-label.description{
			display: block !important;
			margin: 10px 0;
		}
		 .imageuploadify-images-list i.fa-cloud-upload, a.formbuilder-icon-pencil, .imageuploadify-message{
			display: none !important;
		}
		.imageuploadify-message{
			font-size: 12px !important;
		}
		.imageuploadify{
			min-height: 0 !important;
		}
		@for($i = 0; $i <= 13; $i++)
			.input-control-{{$i}}{
				display: none;
			}
		@endfor
	</style>
	<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
	<link href="/assets/css/imageuploadify.min.css" rel="stylesheet">
	<script src="https://cdn.tiny.cloud/1/f1cdeokivvyzyohrpb09kwayr4k53ovtgo1849tqerl354st/tinymce/6/tinymce.min.js" referrerpolicy="origin"></script>
@endsection


@section('content')

<div id="page-content">
    <div class="gradient-wrap">
    	<div class="position-relative hero-section mb-4" style="margin: 20px 4em;">
    		<div class="col-md-12 mb-4">
    			<ul class="nav nav-tabs" id="myTab" role="tablist" style="border-bottom:none;">
				  <li class="nav-item" role="presentation">
				    <button class="nav-link active" id="home-tab" data-toggle="tab" data-target="#home" type="button" role="tab" aria-controls="home" aria-selected="true">Trip Details</button>
				  </li>
				  <li class="nav-item" role="presentation">
				    <button class="nav-link" id="builder-tab" data-toggle="tab" data-target="#builder" type="button" role="tab" aria-controls="profile" aria-selected="false">Itinerary</button>
				  </li>
				</ul>
    		</div>
    		<div class="col-md-12 tab-content" id="myTabContent">
    			<div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
    				<div class="col-md-12 card">
						<div class="card-body mb-4">
							<div class="row mb-4">
								<div class="col-md-6">
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label class="form-label">Marketing Title:</label>
												<input type="text" class="form-control" name="title" id="title">
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label class="form-label">Destination (City):</label>
												<input type="text" class="form-control" name="title" id="title">
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label class="form-label">Start Date:</label>
												<input type="date" class="form-control" name="title" id="title">
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label class="form-label">Number of Days:</label>
												<input type="number" class="form-control" name="days" id="setDays">
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-12">
											<div class="form-group">
												<label class="form-label">Marketing Description</label>
												<span class="form-helper">Keep this to less than 200 words.</span>
												<textarea class="form-control"></textarea>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<label class="form-label">Marketing Image:</label>
									<input type="file" class="form-control">
									<div class="preview-image"></div>
								</div>
							</div>
							
							<div class="row">
								<div class="col-md-4">
									<div class="form-group">
										<label class="form-label">Marketing Tags</label>
										<span class="form-helper">Customers will be able to narrow their search by tags on our platform. Visit <a href="/about/tags">this page</a> for details.</span>
										<select class="js-example-basic-multiple form-control" name="tags[]" multiple="multiple" data-minimum-selection-length="2" data-maximum-selection-length="4" required="">
                                                <option value="Plenty of Exercise">Plenty of Exercise</option>
                                                <option value="Light Activity">Light Activity</option>
                                                <option value="Nature">Nature Adventure</option>
                                                <option value="Major Savings">Major Savings</option>
                                                <option value="Ecotourism">Ecotourism</option>
                                                <option value="Educational/Cultural">Educational/Cultural</option>
                                                <option value="Animals/Ecological">Animals/Ecological</option>
                                                <option value="Foodie">Foodie</option>
                                                <option value="Relaxing">Relaxing</option>
                                                <option value="Water Fun">Water Fun</option>
                                                <option value="Backpacker">Backpacker</option>
                                                <option value="Artsy">Artsy </option>
                                                <option value="For Couples">For Couples</option>
                                                <option value="For Friends">For Friends</option>
                                                <option value="Quick Getaway">Quick Getaway</option>
                                                <option value="Multiple Cities">Multiple Cities</option>
                                                <option value="Holiday">Holiday</option>
                                                <option value="Girls Trip">Girls Trip</option>
                                                <option value="Guys Trip">Guys Trip</option>
                                            </select>
									</div>
								</div>
								<div class="col-md-4">
									<label class="form-label">Purchase Price (USD):</label>
									<span class="form-helper">We recommend $3 per night for destinations < 2 million visitors a year, else we recommend $4 per night.</span>
									<input type="number" min="1" step="1" class="form-control" name="purchasePrice" id="purchasePrice">
								</div>
								<div class="col-md-4">
									<label class="form-label">Personalization Price (USD):</label>
									<span class="form-helper">This covers the first hour of consultation with you. By default, you will be CC'd on all orders whether this is purchased or not.</span>
									<input type="number" class="form-control" name="personalizationPrice" id="personalizationPrice">
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label class="form-label">Purchase Note:</label>
										<span class="form-helper">When a customer buys your itinerary, you can include additional notes that will be included in the confirmation email. This is your opportunity to build a relationship with the customer and market your services.</span>
										<textarea id="purchaseNote"></textarea>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="form-label">Personalization Note:</label>
										<span class="form-helper">For those that request personalization, you can write a special note in their confirmation email with instructions on how to begin the process. It is recommended to include a calendly link or any other instructions.</span>
										<textarea id="personalizationNote"></textarea>
									</div>
								</div>
							</div>
						</div>
    				</div>
    			</div>
    			<div class="tab-pane fade show" id="builder" role="tabpanel" aria-labelledby="builder-tab">
    				<div class="col-md-12 card">
						<div class="card-body">
							<div id="build-wrap"></div>
							<div class="render-wrap"></div>
							<button id="setData">Edit Form</button>
						</div>
		    		</div>
    			</div>
    		</div>
    	</div>
    </div>
</div>

@endsection


@section('js')
	<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
	<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.2.0/jquery.rateyo.min.js"></script>
	<script type="text/javascript" src="/assets/js/render.js"></script>
	<script type="text/javascript" src="/assets/js/builder.js"></script>
	<script src="/assets/js/builder-scripts.js"></script>
	<script type="text/javascript" src="/assets/js/imageuploadify.min.js"></script>
@endsection