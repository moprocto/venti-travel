@extends('layouts.curator-master', ["fontless" => true])

@section('css')
	<style>
		nav, footer{
			display: none !important;
		}
		.hero-section,.card-body{
			margin: 0 !important;
			padding: 0 !important;
		}
	</style>
@endsection

@section('content')
	<div id="page-content">
	    <div class="gradient-wrap">
	        <div class="container hero-section mb-4 mt-4">
	            <div class="row justify-content-center">
	                <div class="col-md-8 text-center mt-4" id="stepFour">
	                	<div class="card text-center" style="border-radius:20px; margin-bottom: 10px;">
	                		<table class="table" width="500"> 
	                			<tr>
	                			<td width="150"></td>
	                			<td width="200" style="text-align:center;">
	                				<a href="https://venti.co"><img src="https://venti.co/assets/img/venti-trademark-logo-black.png" style="width:100px;"></a>
	                				<p>Create your own itinerary for free at <a href="https://venti.co/create" target="_blank">https://venti.co/create</a>
	                			</td>
	                			<td width="150"></td>
	                			</tr>
	                		</table>
	                		<br>
	                	</div>
	                </div>
                	<div class="card text-left mt-4" style="border-radius:20px;">
            			<div class="card-body">
            				<div id="plan">
                				<div class="" style="width: 100%; border-radius: 20px; min-height: 300px; background-position:center; background-size: cover; background-image:url('{{ $plan["imageUrl"] }}')">
                				</div>

                				<br>{!! $plan["answer"] !!}<br>

                				<br>
                				<div class="card text-center" style="border-radius:20px;">
	                		<table class="table" width="500"> 
	                			<tr>
	                			<td width="200"></td>
	                			<td width="100" style="text-align:center;">
	                				<a href="https://venti.co"><img src="https://venti.co/assets/img/venti-trademark-logo-black.png" style="width:100px;"></a>
	                			</td>
	                			<td width="200"></td>
	                			</tr>
	                		</table>
	                	</div>
	                	<div style="display: flex; text-align: left; width: 100%; justify-content:space-between; align-items:center;">
	                		<p>Create your own itinerary for free at <a href="https://venti.co/create" target="_blank">https://venti.co/create</a></p><br>
	                	</div>
                		<p>Venti is not responsible for any damages or injury associated with following this itinerary. You are strongly encouraged to double-check all references to ensure their accuracy.</p>
                			</div>
            			</div>
                	</div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')

@endsection