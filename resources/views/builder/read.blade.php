@extends('layouts.curator-master')

@section('css')
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
	<script src='https://api.mapbox.com/mapbox-gl-js/v1.8.0/mapbox-gl.js'></script>
	<link href='https://api.mapbox.com/mapbox-gl-js/v1.8.0/mapbox-gl.css' rel='stylesheet' />
	<script src='https://tiles.locationiq.com/v2/js/liq-styles-ctrl-gl.js?v=0.1.6'></script>
	<link href='https://tiles.locationiq.com/v2/css/liq-styles-ctrl-gl.css?v=0.1.6' rel='stylesheet' />
    <script src="https://tiles.locationiq.com/v2/libs/gl-geocoder/4.5.1/locationiq-gl-geocoder.min.js?v=0.2.2"></script>
	<link rel="stylesheet" href="https://tiles.locationiq.com/v2/libs/gl-geocoder/4.5.1/locationiq-gl-geocoder.css?v=0.2.2" type="text/css" />
	<style type="text/css">
		.hide{
			display: none;
		}
		#plan{
			padding: 20px;
		}
		.menu-box{
			display: flex; 
			width: 100%; 
			justify-content:space-between; 
			align-items:center;
		}
		@media (max-width: 600px){
			.menu-box{
				display: block !important;
			}
			.menu-buttons{
				margin-top: 20px !important;
			}
		}
	</style>
	<div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v16.0&appId=1298822020928144&autoLogAppEvents=1" nonce="jHlxbXfP"></script>
@endsection

@section('content')
	<div id="page-content">
	    <div class="gradient-wrap">
	        <div class="container hero-section mb-4 mt-4">
	            <div class="row justify-content-center">
	                <div class="col-md-8 text-center mt-4" id="stepFour">
	                	<div class="card text-left" style="border-radius:20px;">
	                		<div class="card-body">
	                			<div class="menu-box">
	                				<div>
			                			<div class="fb-like" data-href="https://venti.co/plans/{{ $plan['planID']}}" data-width="" data-layout="" data-action="" data-size="" data-share="true"></div>
			                			<br>
			                			Your Travel Plan Link: <a target='_blank' href='https://venti.co/plans/{{ $plan["planID"]}}'>https://venti.co/plans/{{ $plan["planID"]}}</a><br>
			                		</div>
		                			<div class="menu-buttons">
			                			<a href="/plan/edit/{{ $plan['planID']}}" class="btn btn-white" target="_blank"><i class="fa fa-pencil"></i> EDIT</a> &nbsp;
			                			<a href="/plan/pdf/{{ $plan['planID']}}" class="btn btn-black" target="_blank"><i class="fa fa-download"></i> PDF</a> &nbsp;
			                			<a href="/create" class="btn btn-success" style="font-weight:bold;"><i class="fa fa-plus"></i> NEW PLAN</a>
			                		</div>
		                		</div>
	                		</div>
	                	</div>
	                	<div class="card text-left mt-4" style="border-radius:20px;">
	                			<div class="card-body">
	                				<div id="plan">
	                				<div class="" style="width: 100%; border-radius: 20px; min-height: 400px; background-position:center; background-size: cover; background-image:url('{{ $plan["imageUrl"] }}')">
	                				</div>

	                				<br>{!! $plan["answer"] !!}<br><p>Venti is not responsible for any damages or injury associated with following this itinerary. You are strongly encouraged to double-check all references to ensure their accuracy.</p>
	                			</div>
	                			</div>
	                	</div>
	                </div>
	            </div>
	        </div>
	    </div>
	</div>
@endsection