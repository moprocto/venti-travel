<h1>{{ $data["title"] }}</h1>
<h6>{{ $data["description"] }}</h6>

@foreach($data["itinerary"] as $day)
	<h3>{{ $day["subtitle"] }}</h3>
	<p>{{ $day["description"] }}</p>
	<ul>
		@foreach($day["activities"] as $activity)
			@php
				$description = str_replace($activity["destination"],"<a href='" . $activity["url"] . "' target='_blank'>" . $activity['destination'] ."</a>",$activity["description"]);

				if($day["day"] == 0){
					// the first day requires the airport to be linked
					$description = str_replace($activity["previousDestination"],"<a href='https://venti.co/place/" . $activity["previousDestination"] ."' target='_blank'>" . $activity['previousDestination'] ."</a>",$description);
				}
			@endphp
			<li><strong>{{ $activity["start"] }} - {{ $activity["end"] }}</strong> @php echo $description; @endphp @if($activity['distanceUrl'] != null && array_key_exists("taxi", $activity["distanceToDestination"]))@if(array_key_exists("distanceUrl", $activity))<a href="{{ $activity['distanceUrl'] }}" target="_blank">{{ $activity["distanceToDestination"]["taxi"] ?? "" }} by car.</a>@endif @endif</li>
		@endforeach
	</ul>
@endforeach