@extends('layouts.curator-master')

@section('css')
    <link rel="stylesheet" type="text/css" href="/assets/css/steps.css">
    <style>
        input.disabled{
            pointer-events: none;
        }
        .wizard>.content>.body{
            height: 100%;
            position: relative;
        }
        .wizard>.content{
            min-height: 120px;
        }
        .wizard .steps{
            display: none;
        }

.profile-pic {
    width: 100%;
    height: 100%;
    cursor: pointer;
    border-radius: 50%;
}

.wizard .content .body .file-upload {
    display: none;
}
.circle {
    width: 125px;
    height: 125px;
    
    margin: 0 auto;
    display: block;
}

.p-image {
  width: 100%;
  text-align: center;
  color: #666666;
  transition: all .3s cubic-bezier(.175, .885, .32, 1.275);
}
.p-image:hover {
  transition: all .3s cubic-bezier(.175, .885, .32, 1.275);
}
.upload-button {
  font-size: 1.2em;
}

.upload-button:hover {
  transition: all .3s cubic-bezier(.175, .885, .32, 1.275);
  color: #999;
}
@media (max-width: 767px){
.black.hero-section {
margin-left: 0 !important;
margin-right: 0 !important;
}
}
</style>
@endsection

@section('content')
<div id="page-content">
    <div class="gradient-wrap">
        <div class="container black position-relative hero-section mb-4">
            <div class="row justify-content-center">
                <div class="col-md-12 text-center mt-4">
                    <h2>Register</h2>
                </div>
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-body">
                            <div class="row mt-4 mb-4 text-center">
                                <div class="col-md-12 text-center">
                                    <a class="btn btn-primary" style="font-size: 13px; border-color: #3b5998; border-radius: 20px; background-color:#3b5998 !important" href="/login/facebook/redirect"><i class="fab fa-facebook"></i> REGISTER WITH FACEBOOK</a>
                                    <br>
                                </div>
                            </div>
                            <form method="POST" action="{{ route('create-user') }}" id="registerForm" enctype="multipart/form-data">
                                @csrf
                                <div id="example-basic">
                                    <h3></h3>
                                    <section>
                                        <div class="row mb-3 justify-content-center">
                                            <div class="col-md-12">
                                                <label for="name" class="col-form-label text-left">👋 What's Your Name?</label>
                                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" autocomplete="name" placeholder="At least a first name" autofocus>
                                                <span id="nameError" class="invalid-feedback @error('name')show @enderror" role="alert">
                                                    <strong>This is required</strong>
                                                </span>
                                            </div>
                                        </div>
                                    </section>
                                    <h3></h3>
                                    <section>
                                        <div class="row mb-3 justify-content-center">
                                            <div class="col-md-12">
                                                <label for="email" class="col-form-label text-left"><span id="hello"></span>What's your email?</label>
                                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="something@email.com">
                                                <span class="invalid-feedback" role="alert" id="emailField">
                                                    <strong></strong>
                                                </span>
                                            </div>
                                        </div>
                                    </section>
                                    <h3></h3>
                                    <section>
                                        <div class="row mb-3 justify-content-center">
                                            <div class="col-md-12">
                                                <label for="username" class="col-form-label text-left">💪 Pick a cool username</label>
                                                <input id="username" type="text" class="form-control @error('username') is-invalid @enderror" name="username" value="{{ old('username') }}" placeholder="frequentflyer" required>
                                                <span class="" role="alert" id="usernameField" style="padding-top:5px;"><strong>Usernames must be</strong><ul style="padding-left:1em;"><li>longer than five characters</li><li>contain only letters, numbers, periods, and underscores;</li><li>and be shorter than 12 characters</li></ul></span>
                                            </div>
                                        </div>
                                    </section>
                                    <h3></h3>
                                    <section>
                                        <div class="row mb-3 justify-content-center">
                                            <div class="col-md-12">
                                                <label for="password" class="col-form-label text-left">🔒 Pick a secure password<br><span style="font-size:12px;">At least eight characters long for good measure</span></label>
                                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password" placeholder="At least eight characters">
                                                    <span class="invalid-feedback" role="alert" id="passError">
                                                        <strong>Try something else...</strong>
                                                    </span>
                                            </div>
                                        </div>
                                    </section>
                                    <h3></h3>
                                    <section>
                                        <div class="row mb-3 justify-content-center">
                                            <div class="col-md-12">
                                                <label for="dob" class="col-form-label text-left">How old are you?<br><span style="font-size:12px;">We require everyone to be at least 18 years of age.</span></label>
                                                <input id="dob" name="dob" type="date" class="form-control" value="{{ old('dob') }}" placeholder="frequentflyer" required>
                                                <span id="dobError" class="invalid-feedback @error('dob')show @enderror" role="alert">
                                                    <strong>This is required</strong>
                                                </span>
                                            </div>
                                        </div>
                                    </section>
                                    <h3></h3>
                                    <section>
                                        <div class="row mb-3 justify-content-center">
                                            <div class="col-md-12">
                                                <label for="photo" class="col-form-label text-left">Let's see that smile 😄<br><span style="font-size:12px;">For everyone's safety, we require all accounts to include a headshot.</label>
                                                <div class="form-group">
                                                    <div class="circle">
                                                       <img class="profile-pic" src="{{ '/assets/img/profile.jpg' }}">
                                                    </div>
                                                    <div class="p-image">
                                                        <p style="margin-bottom: 0;"><i class="fa fa-camera upload-button"></i> 200px x 200px</p>
                                                        <p>Must be a clear photo of you</p>
                                                        <input class="file-upload" type="file" name="avatar" id="photo"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                    <h3></h3>
                                    <section>
                                        <div class="row mb-3 justify-content-center">
                                            <div class="col-md-12" style="display: flex; align-items: center; justify-content: space-around;">
                                                <div class="col-xs-12" style="max-width:60px; min-width:60px;">
                                                    <input type="checkbox" id="terms" value="1" required="" class="">
                                                </div>
                                                <div style="max-width: 400px;">
                                                 By creating an account, you agree to our <a href="/terms">Terms of Conditions & Privacy Policy 🏛</a>
                                                 <span id="termsError" class="invalid-feedback" role="alert">
                                                    <strong>This is required</strong>
                                                </span>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 text-center mt-4">
                    <a href="{{ route('login') }}">Back to Login</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
    <script type="text/javascript" src="/assets/js/jquery.steps.min.js"></script>
    <script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $("#registerForm input:eq(0)").val()
        }
    });

    $("#example-basic").steps({
        headerTag: "h3",
        bodyTag: "section",
        autoFocus: true,
        onStepChanging: function(e, currentIndex, newIndex) {
            if(newIndex < currentIndex){
                return true;
            }
            var name = $("#name").val();
            var email = $("#email").val();
            var username = $("#username").val();
            var password = $("#password").val();
            var dob = $("#dob").val();
            var photo = $("#photo").val();
            

            switch (currentIndex){
                case 0:
                    // name
                    if(!checkIfEmpty(name)){
                        $("#nameError").show();
                        return false;
                    }
                break;
                case 1:
                    // email
                    if(!isEmailValid(email)){
                        $("#emailField").show();
                        $("#emailField").html("<strong>That doesn't look right...</strong>");
                        return false;
                    }
                    if($("#email").hasClass("is-invalid")){
                        return false;
                    }
                break;
                case 2:
                    // username
                    if(!isUserNameValid(username)){

                        return false;
                    }
                    if($("#username").hasClass("is-invalid")){

                        return false;
                    }
                break;
                case 3:
                    // password
                    if(!checkIfEmpty(password) || password.length < 8){
                        $("#passError").show();
                        return false;
                    } else {
                        if(password.length < 8){
                            return false;
                        }
                    }
                break;
                case 4:
                    // dob
                    if(!checkIfEmpty(dob)){
                        $("#dobError").show();
                        $("#dobError").html("<strong>This is required</strong>");
                        return false;
                    }
                    var age =  moment(dob).diff(moment(), 'years') * -1;
                    if(age < 18){
                        $("#dobError").show();
                        $("#dobError").html("<strong>Not old enough :(</strong>");
                        return false;
                    }
                break;
                case 5:
                    // photo 
                    if(!checkIfEmpty(photo)){
                        swal({
                          icon: 'warning',
                          title: '😅',
                          text: 'Sorry, but this is a required step.',
                        });

                        return false;
                    }
                break;
                default:
                    return false;
                break;
            }


            return true;
        }
    });

    $("body").on("click", 'a[href="#finish"]', function(){
            if(!$('#terms').is(':checked')){
                swal({
                  icon: 'warning',
                  title: '🏛',
                  text: 'This one is even more important.',
                });
            }
            else{
                $("#registerForm").submit();
            }
    });

    $('#example-basic').keypress(function (e) {
         var key = e.which;
         if(key == 13)  // the enter key code
          {
            $('a[href="#next"]').click();
            $(".current").find("input").focus();
            return false;  
          }
    });

    @error('fb')
        swal({
                  icon: 'error',
                  title: 'Something went wrong!',
                  text: "You either denied permission, or we're having issues connecting your facebook account. Please try using the email sign-up method. We apologize for the inconvenience",
        });
    @enderror

    $("#name").keyup(function(){
        var name = $(this).val();
        $("#hello").text("Nice to meet you, " + name + "! ");
    });

    $("#dob").on("change", function(){
        var dob = $(this).val();
        var age =  moment(dob).diff(moment(), 'years') * -1;
        if(age >= 18){
            $("#dobError").hide();
        }
    });

    $("#username").keyup(function(){
        
        $(this).val($(this).val().toLowerCase().replace(" ", "").replace(",", ""));
        var username = $(this).val().toLowerCase().replace(" ", "").replace(",", "");
        var formatError = false;
        var availableError = false;

        if(username.length > 5 && username.length < 13){
            if(isUserNameValid(username)){
                formatError = false;
                if(!$(this).hasClass("is-invalid")){
                    $(this).removeClass("is-invalid");
                }
            } else {
                formatError = true;
            }
        } else {
            formatError = true;
        }

        if(formatError === true){
            if(!$(this).hasClass("is-invalid")){
                $(this).addClass("is-invalid");
                $(this).removeClass("is-valid");
                $("#usernameField").html("Usernames must be <ul><li>longer than five characters</li><li>contain only letters, numbers, periods, and underscores;</li><li>and be shorter than 12 characters</li></ul>");
            }
        } else {
            if($(this).hasClass("is-invalid")){
                $(this).removeClass("is-invalid");
            }
            $.ajax({
                url: "{{ route('validate-username') }}",
                processData: true,
                dataType: 'json',
                data:{
                    username: username
                },
                type: 'post',
                success: function (data) {
                    if(data != 201){
                        if(!$("#username").hasClass("is-valid")){
                            $("#username").addClass("is-valid");
                            $("#username").removeClass("is-invalid");
                            $("#usernameField").html("");
                            $("#submitButton").removeClass("disabled");
                        }
                    } else {
                        if(!$("#username").hasClass("is-invalid")){
                            $("#username").addClass("is-invalid");
                            $("#username").removeClass("is-valid");
                            $("#usernameField").html("That username is not available");
                        }
                    }
                }
            });
        }

        formatError = false;

    });

    $("#email").keyup(function(){
        $(this).val($(this).val().toLowerCase());
        var email = $(this).val().toLowerCase();
        var emailFormatError = false;
        var emailAvailableError = false;

        if(email.length > 5){
            if(isEmailValid(email)){
                $.ajax({
                    url: "{{ route('validate-email') }}",
                    processData: true,
                    dataType: 'json',
                    data:{
                        email: email
                    },
                    type: 'post',
                    success: function (data) {
                        console.log(data);
                        if(data != 201){
                            if(!$("#email").hasClass("is-valid")){
                                $("#email").addClass("is-valid");
                                $("#email").removeClass("is-invalid");
                                $("#emailField").html("");
                                $("#submitButton").removeClass("disabled");
                            }
                        } else {
                            if(!$("#email").hasClass("is-invalid")){
                                $("#email").addClass("is-invalid");
                                $("#email").removeClass("is-valid");
                                $("#emailField").html("That email is not available");
                            }
                        }
                    }
                });
            } else {

            }
        }
    });

    $(".file-upload").on('change', function(){
        readURL(this);
    });
    
    $(".upload-button, .profile-pic").on('click', function() {
       $(".file-upload").click();
    });

    var readURL = function(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('.profile-pic').attr('src', e.target.result);
            }
    
            reader.readAsDataURL(input.files[0]);
        }
    }

    function isUserNameValid(username) {
      /* 
        Usernames can only have: 
        - Lowercase Letters (a-z) 
        - Numbers (0-9)
        - Dots (.)
        - Underscores (_)
      */
      const res = /^[a-z0-9_\.]+$/.exec(username);
      const valid = !!res;

      return valid;
    }

    function isEmailValid(mail) 
    {
        if(!checkIfEmpty(mail)){
            return false;
        }
        var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

        if(mail.match(mailformat)){
            return true;
        }

        return false;
    }

    

    </script> 
@endsection

@section('js')
    <script type="text/javascript">
        window.location.href = "/download";
    </script>
@endsection