@extends('layouts.curator-master')

@section('css')
    <link rel="stylesheet" type="text/css" href="/assets/css/steps.css">
    <link rel="stylesheet" href="/assets/css/intlTelInput.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/pretty-checkbox@3.0/dist/pretty-checkbox.min.css">
    <script src="/assets/js/intlTelInput.js"></script>
    <style>
        input.disabled{
            pointer-events: none;
        }
        .wizard>.content>.body{
            height: 100%;
            position: relative;
        }
        .wizard>.content{
            min-height: 120px;
        }
        .wizard .steps, .hide,#pass-strength{
            display: none;
        }
        @media (max-width: 767px){
            .black.hero-section {
            margin-left: 0 !important;
            margin-right: 0 !important;
            }
        }
        .col-form-label{
            font-weight: bold !important;
        }
        form > *{
            font-weight: 300;
        }

        .slim-p{
            margin: 0;
            padding: 0;
        }
        .pswd_info {
            list-style: none;
            margin: 5px 0px;
            padding: 0;
        }
        .pswd_info .invalid {
            padding-left: 20px;
            background:url(images/invalid.png) no-repeat 0 50%;
            color:#ec3f41;
        }
        .pswd_info .valid {
            padding-left: 20px;
            background:url(images/valid.png) no-repeat 0 50%;   
            color:#3a7d34;
        }
        #password-strength-status {
            font-size:10px;
            padding: 5px 10px;
            color: #000000;
            border-radius: 4px;
            margin-top: 5px;
        }
        .veryweak-password {
            background-color: #ec3f41;
        }
        .weak-password {
            background-color: #d35400;
        }
        .medium-password {
            background-color: #f39c12;
        }
        .average-password {
            background-color: #f1c40f;  
        }
        .strong-password {
            background-color: #229954;
        }
        #password-strength-status{
            color: white;
            font-weight: 500;
        }
</style>
@endsection

@section('content')
    @if(env('APP_DOWN') != "true")
    <div id="page-content">
        <div class="gradient-wrap">
            <div class="container black position-relative hero-section mb-4">
                <div class="row justify-content-center" >
                    <div class="col-md-12 text-center mt-4">
                        <h2 class="mt-2">Get Started</h2>
                    </div>
                    <div class="col-md-6">
                        <div class="card soft-shadow" style="border:none;">
                            <div class="card-body">
                                <h4>We are not accepting individual accounts at this time. To use Venti, you'll need to be a student or member of a partnering credit union.</h4>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 text-center mt-4">
                        <a href="{{ route('login') }}">Back to Login</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @else
    <div id="page-content">
       <div class="gradient-wrap">
          <!--  HERO -->
          <div class="black position-relative hero-section mb-4" style="margin: 20px 4em;">
             <div class="row" style="justify-content: center;">
                <div class="col-md-12 text-center">
                   <img src="/assets/img/300.png" style="width: 100%; max-width: 400px; margin-top: 40px;">
                   <h1>We're Performing Maintenance</h1>
                   <p>This is temporary, and we'll have systems flying high shortly.</p>
                </div>
                <div class="col-md-12 text-center">
                   <a class="dark-on-light nav-links btn btn-primary" href="/"><i class="fas fa-location-arrow"></i> HEAD HOME</a>
                </div>
             </div>
          </div>
       </div>
    </div>
    @endif
@endsection

@section('js')
    <script type="text/javascript" src="/assets/js/jquery.steps.min.js"></script>
    <script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $("#registerForm input:eq(0)").val()
        }
    });

    var fname = $("#fname").val();
    var lname = $("#lname").val();
    var email = $("#email").val();
    var username = $("#username").val();
    var password = $("#password").val();

    $("#example-basic").steps({
        headerTag: "h3",
        bodyTag: "section",
        autoFocus: true,
        onStepChanging: function(e, currentIndex, newIndex) {
            if(newIndex < currentIndex){
                return true;
            }
            
            switch (currentIndex){
                case 0:
                    // name
                    if(checkStepOne()){
                        return true;
                    }
                    return false;
                break;
                case 1:
                    if(checkStepTwo()){
                        return true;
                    }
                    return false;
                break;
                default:
                    return false;
                break;
            }
            return true;
        }
    });

    $("body").on("click", 'a[href="#finish"]', function(){
            var stepOne = checkStepOne();
            var stepTwo = checkStepTwo();

            if(!stepTwo){
                return swal({
                  icon: 'warning',
                  title: 'Missing required fields',
                  text: 'Please go back and check that all required steps have been completed',
                });
            }

            if(stepOne === true && stepTwo === true){
                // all required fields have been submitted. Take the user to the next screen.
                $("#registerForm").submit();
                return true;
            }
            // 

            var passwordStrenth = checkPasswordStrength($("#password").val());

            if(passwordStrenth < 3){
                return swal({
                  icon: 'warning',
                  title: 'Password is too weak',
                  text: 'Please select a new password that meets our complexity requirements. This helps secure your account.',
                });
            }

            swal({
              icon: 'warning',
              title: 'Missing required fields',
              text: 'Please go back and check that all required steps have been completed',
            });

            return false;
    });

    $('#example-basic').keypress(function (e) {
        var key = e.which;
        if(key == 13)  // the enter key code
        {
            $('a[href="#next"]').click();
            $(".current").find("input").focus();
            return false;  
        }
    });

    function checkStepOne(currentIndex){
        $(".invalid-feedback").each(function(){
            $(this).hide();
        });

        if(!$('#termsVenti').is(':checked')){

            swal({
              icon: 'warning',
              title: 'You missed a step',
              text: 'You have to accept our Terms and Conditions & Privacy policy to proceed',
            });

            $("#ventiTermsError").show();

            return false;

        }
        if(!$('#termsDwolla').is(':checked')){

            swal({
              icon: 'warning',
              title: 'You missed a step',
              text: "You have to accept Dwolla's Terms and Conditions and Privacy policies to proceed",
            });

            $("#dwollaTermsError").show();

            return false;
        }
        return true;
    }

    function checkStepTwo(){

        $(".invalid-feedback").each(function(){
            $(this).hide();
        });

        var fname = $("#fname").val();
        var lname = $("#lname").val();
        var email = $("#email").val();
        var username = $("#username").val();
        var password = $("#password").val();

        if(!checkIfEmpty(fname)){
            $("#fNameError").show();
            console.log("no first name detected")
            return false;
        }
        if(!checkIfEmpty(lname)){
            $("#lNameError").show();
            return false;
        }

        if(!isEmailValid(email)){
            $("#emailField").show();
            $("#emailField").html("<strong>That doesn't look right...</strong>");
            return false;
        }
        if($("#email").hasClass("is-invalid")){
            return false;
        }
        if(!checkIfEmpty(password) || password.length < 8){
            $("#passError").show();
            return false;
        } else {
            if(password.length < 8){
                return swal({
                  icon: 'warning',
                  title: 'Password is too weak',
                  text: 'Please select a new password that meets our complexity requirements. This helps secure your account.',
                });
            }
            var passwordStrenth = checkPasswordStrength(password);
            if(passwordStrenth < 3){

                return swal({
                  icon: 'warning',
                  title: 'Password is too weak',
                  text: 'Please select a new password that meets our complexity requirements. This helps secure your account.',
                });
            }
        }

        return true;
    }

    $("#email").keyup(function(){
        $(this).val($(this).val().toLowerCase());
        var email = $(this).val().toLowerCase();
        var emailFormatError = false;
        var emailAvailableError = false;

        if(email.length > 5){
            if(isEmailValid(email)){
                $.ajax({
                    url: "{{ route('validate-email') }}",
                    processData: true,
                    dataType: 'json',
                    data:{
                        email: email
                    },
                    type: 'post',
                    success: function (data) {
                        console.log(data);
                        if(data != 201){
                            if(!$("#email").hasClass("is-valid")){
                                $("#email").addClass("is-valid");
                                $("#email").removeClass("is-invalid");
                                $("#emailField").hide();
                                $("#submitButton").removeClass("disabled");
                            }
                        } else {
                            if(!$("#email").hasClass("is-invalid")){
                                $("#email").addClass("is-invalid");
                                $("#email").removeClass("is-valid");
                                $("#emailField").show();
                                $("#emailField").html("That email is not allowed for account creation.");
                            }
                        }
                    }
                });
            } else {

            }
        }
    });

    function isEmailValid(mail) 
    {
        if(!checkIfEmpty(mail)){
            return false;
        }
        var mailformat = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;

        if(mail.match(mailformat)){
            return true;
        }

        

        return false;
    }

    function checkPasswordStrength(password) {
    var number     = /([0-9])/;
    var upperCase  = /([A-Z])/;
    var lowerCase  = /([a-z])/;
    var specialCharacters = /([~,!,@,#,$,%,^,&,*,-,_,+,=,?,>,<])/;

    var characters     = (password.length >= 6 && password.length <= 15 );
    var capitalletters = password.match(upperCase) ? 1 : 0;
    var loweletters    = password.match(lowerCase) ? 1 : 0;
    var numbers        = password.match(number) ? 1 : 0;
    var special        = password.match(specialCharacters) ? 1 : 0;

    this.update_info('length', password.length >= 6 && password.length <= 15);
    this.update_info('capital', capitalletters);
    this.update_info('small', loweletters);
    this.update_info('number', numbers);
    this.update_info('special', special);

    var total = characters + capitalletters + loweletters + numbers + special;
    this.password_meter(total);

    return total;
}

function update_info(criterion, isValid) {
    var $passwordCriteria = $('#passwordCriterion').find('li[data-criterion="' + criterion + '"]');
    if (isValid) {
        $passwordCriteria.removeClass('invalid').addClass('valid');
    } else {
        $passwordCriteria.removeClass('valid').addClass('invalid');
    }
}

function password_meter(total) {
    var meter = $('#password-strength-status');
    meter.removeClass();
    if (total === 0) {
        meter.html('');
    } else if (total === 1) {
        meter.addClass('veryweak-password').html('Very Weak');
    } else if (total === 2) {
        meter.addClass('weak-password').html('Weak');
    } else if (total === 3) {
        meter.addClass('medium-password').html('Medium');
    } else if (total === 4) {
        meter.addClass('average-password').html('Average');
    } else {
        meter.addClass('strong-password').html('Strong');
    }
}

$('#password').keyup(function(event) {
    var password = $('#password').val();
    if(password.length > 1){
        checkPasswordStrength(password);
        $("#pass-strength").show();
    }
});

$("#subscription").on("change", function(){

    if($(this).val() == "buddy"){
        $(".buddyPass").show();
    }
    else {
        $(".buddyPass").hide();
    }
});
    

    </script> 
@endsection


