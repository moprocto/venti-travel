@extends('layouts.curator-master')

@section('css')
  <script type="text/javascript">
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '1298822020928144',
      cookie     : true,
      xfbml      : true,
      version    : 'v15.0'
    });
      
    FB.AppEvents.logPageView();   
      
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "https://connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
</script>
@endsection

@section('content')
<div id="page-content">
    <div class="gradient-wrap">
        <div class="container hero-section mb-4 mt-4">
            <div class="row justify-content-center">
                <div class="col-md-12 text-center mt-4">
                    <h2>Login</h2>
                </div>
                <div class="col-md-8">
                    <div class="card">

                        <div class="card-body">                            
                          <fb:login-button 
                            scope="public_profile,email"
                            onlogin="checkLoginState();">
                          </fb:login-button>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 mt-4 text-center">
                    <a href="{{ route('register') }}">No Account? Create One Here</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')

@endsection