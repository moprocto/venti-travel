@extends('layouts.curator-master')

@section('css')

    <link rel="stylesheet" type="text/css" href="/assets/css/steps.css">
    <link rel="stylesheet" href="/assets/css/intlTelInput.min.css">
    <style>
        input.disabled{
            pointer-events: none;
        }

        @media (max-width: 767px){
            .black.hero-section {
            margin-left: 0 !important;
            margin-right: 0 !important;
            }
        }
        .col-form-label{
            font-weight: bold !important;
        }
        form > *{
            font-weight: 300;
        }

        .slim-p{
            margin: 0;
            padding: 0;
        }
        .isVerified{
            display: none;
        }
        .fa-spinner{
            display: none;
        }
        .loading .fa-spinner{
            display: inherit !important;
        }

        @if(Session::get('smsRecipient') === null)
            .new{
                display: none;
            }
        @endif
        
        input.code-input {
            font-size: 1.8em;
            width: 1em;
            text-align: center;
            flex: 1 0 1em;
            border-color: rgb(0,0,0,0.1);
            border-radius: 7px;
            width: 55px;
            height: 75px;
          }
    </style>
@endsection

@section('content')
<div id="page-content">
    <div class="gradient-wrap">
        <div class="container black position-relative hero-section mb-4">
            <div class="row justify-content-center" >
                <div class="col-md-12 text-center mt-4">
                    <h2 class="mt-2">Security Checkpoint</h2>
                </div>
                <div class="col-md-4 text-center">
                    <div class="card soft-shadow" style="border:none;">
                        <div class="card-body">
                            <div class="row mt-4 text-center">
                                <div class="col-md-12 text-center">
                                    <img src="/assets/img/icons/mfa.png" style="width: 130px;">
                                </div>
                            </div>
                            <div id="example-basic">
                                <h3></h3>
                                <section>
                                    <div class="row mb-3 justify-content-center">
                                        <div class="col-md-12">
                                            <h5 class="text-center"><label for="name" class="col-form-label text-left text-bold"></label></h5>
                                            <p></p> 
                                            <p class="text-left">To confirm it's really you, please enter the six-digit code we sent to your phone number ending in: {{ substr(Session::get('user')->customClaims["phoneNumber"], -4); }}
                                            </p>
                                        </div>
                                        <div class="col-md-12 text-center">
                                            <p style="margin:0"></p>
                                            <span style="font-size:12px;"></span>
                                            
                                            <br><div><fieldset class='number-code'><legend>Security Code</legend><div><input name='code' class='code-input' placeholder='' required/><input name='code' class='code-input' placeholder='' required/><input name='code' class='code-input' placeholder='' required/><input name='code' class='code-input' placeholder='' required/><input name='code' class='code-input' placeholder='' required/><input name='code' class='code-input' placeholder='' required/></div></fieldset><br><button type='button' data-phoneNumber='{{ Session::get("user")->customClaims["phoneNumber"] }}'  style='margin:0 auto; font-weight:500' class='btn btn-primary' id='verifySMS'><i class='fa fa-sync fa-spin'></i> Verify</button></div>
                                        </div>
                                    </div>
                                </section>
                            </div>
                        </div>
                    </div>
                    <br><br>
                    <a href="/user/logout" class="btn btn-white text-danger"><i class="fa fa-lock"></i> Log Out</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
    <script type="text/javascript" src="/assets/js/jquery.steps.min.js"></script>
    @if(Session::get('smsRecipient') === null)
    <script src="/assets/js/intlTelInput.js"></script>
    @endif

    <script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': "{{ csrf_token() }}"
        }
    });

    initCodeInput();

    sendSMSVerification();

    function sendSMSVerification(){
        $.ajax({
                url: "{{ route('send-sms-verification') }}",
                type: 'post',
                processData: true,
                dataType: 'json',
                data:{
                    phoneNumber: "{{ Session::get('user')->customClaims['phoneNumber'] }}"
                },
                success: function (data) {

                    if(data != 500 && data != 201){
                        
                    }
                    else {
                        if(data == 201){
                            swal({
                                icon: 'success',
                                title: "We already sent a code",
                                text: "Please check your messages for a six-digit code.",
                            });

                        } else {
                            swal({
                                icon: 'error',
                                title: "Something went wrong",
                                text: "There was an error sending a text message to: {{ Session::get('user')->customClaims['phoneNumber'] }}. Please make sure the phone number is correct and the appropriate country code is applied.",
                            });
                        }
                    }
                },
               error: function(jqXhr, textStatus, errorMessage){
                  console.log("Error: ", errorMessage);
               }
            });
    }

    $("body").on("click", "#verifySMS", function(){

        var button = $(this);
        button.addClass("disable-while-loading");
        var smsCode = "";

        $(".code-input").each(function(){
            var digit = $(this).val();
            if(digit == "" || digit == null || isNaN(digit)){
                return swal({
                    icon: 'warning',
                    title: "Something's wrong",
                    text: 'Your code must be six digits',
                });
            }

            smsCode += digit;
        });

        var serviceID = $(this).attr("data-service-id");
        var phoneNumber = $(this).attr("data-phoneNumber");

        if(phoneNumber.length < 10){
            swal({
                icon: 'warning',
                title: "Something's wrong",
                text: 'The phone number seems incorrect',
            });
            button.removeClass("disable-while-loading");
        }

        else {
            $.ajax({
                url: "{{ route('verify-sms-code-mfa') }}",
                type: 'post',
                processData: true,
                dataType: 'json',
                data:{
                    phoneNumber: phoneNumber,
                    smsCode: smsCode,
                    serviceID: serviceID
                },
                success: function (data) {
                    if(data == "ok"){
                        window.location.href = "/home";
                    }
                    else {
                        button.removeClass("disable-while-loading");
                        swal({
                            icon: 'warning',
                            title: "Something's wrong",
                            text: 'That code was incorrect. Please double-check the number we sent you via txt.',
                        });
                    }
                }
            });
        }
    });

    
    function initCodeInput(){

        const inputElements = [...document.querySelectorAll('input.code-input')]

        inputElements.forEach((ele,index)=>{
          ele.addEventListener('keydown',(e)=>{
            // if the keycode is backspace & the current field is empty
            // focus the input before the current. Then the event happens
            // which will clear the "before" input box.
            if(e.keyCode === 8 && e.target.value==='') inputElements[Math.max(0,index-1)].focus()
          })
          ele.addEventListener('input',(e)=>{
            // take the first character of the input
            // this actually breaks if you input an emoji like 👨‍👩‍👧‍👦....
            // but I'm willing to overlook insane security code practices.
            const [first,...rest] = e.target.value
            e.target.value = first ?? '' // first will be undefined when backspace was entered, so set the input to ""
            const lastInputBox = index===inputElements.length-1
            const didInsertContent = first!==undefined
            if(didInsertContent && !lastInputBox) {
              // continue to input the rest of the string
              inputElements[index+1].focus()
              inputElements[index+1].value = rest.join('')
              inputElements[index+1].dispatchEvent(new Event('input'))
            }
          })
        });
    }


// mini example on how to pull the data on submit of the form
function onSubmit(e){
  e.preventDefault()
  const code = inputElements.map(({value})=>value).join('')
  console.log(code)
}
    
    </script> 
@endsection


