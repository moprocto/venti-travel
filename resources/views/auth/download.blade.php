@extends('layouts.curator-master')

@section('content')
<div id="page-content">
    <div class="gradient-wrap">
        <div class="container hero-section mb-4 mt-4">
            <div class="row justify-content-center">
                <div class="col-md-12 text-center mt-4">
                    <h2>Download</h2>
                </div>
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-body text-center">
                            <a href="https://apps.apple.com/us/app/venti-friends-and-adventure/id1620282387" style="display:block"><img style="max-width:600px; width: 100%;" src="/assets/img/900.jpg"></a>
                            <h3>To use Venti, download our mobile app!</h3>
                            <a href="https://apps.apple.com/us/app/venti-friends-and-adventure/id1620282387" class="btn btn-primary"><i class="fab fa-apple mr-1"></i> Download Venti</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
