@extends('layouts.curator-master')

@section('css')
    <style>
        #captcha-image img{
            width: 150px;
        }
    </style>
@endsection

@section('content')
    @if(env('APP_DOWN') != "true")
    <div id="page-content">
        <div class="gradient-wrap">
            <div class="container hero-section mb-4 mt-4">
                <div class="row justify-content-center">
                    <div class="col-md-12 text-center mt-4">
                        <h2>Login</h2>
                    </div>
                    <div class="col-md-8">
                        <div class="card">

                            <div class="card-body">
                                @error('general')
                                    <div class="alert alert-danger">
                                        <span>Incorrect login info!</span>
                                        @if(Session::get('loginAttempsRemaining') !== null)
                                            <span>{{ Session::get('loginAttempsRemaining') }} login attemps remaining</span>
                                        @endif
                                    </div>
                                @enderror
                                <div class="row mt-4 mb-4 text-center">
                                    
                                </div>
                                <form method="POST" action="{{ route('create-login') }}" class="mt-4">
                                    @csrf

                                    <div class="row mb-3 justify-content-center">

                                        <div class="col-md-6 col-md-offset-3">
                                            <label for="email" class="from-label">{{ __('E-Mail Address') }}</label>
                                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                                            
                                        </div>
                                    </div>

                                    <div class="row mb-3 justify-content-center">
                                        <div class="col-md-6">
                                            <label for="password" class="form-label">{{ __('Password') }}</label>
                                            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                            @error('password')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="row mb-3 justify-content-center">
                                        <div class="col-md-6 " style="display: flex; align-items: center; justify-content: space-between;">
                                            @if (Route::has('password.request'))
                                                <a class="" href="{{ route('password.request') }}">
                                                    {{ __('Forgot Your Password?') }}
                                                </a>
                                            @endif
                                        </div>
                                    </div>
                                    <!--
                                    <div class="row mb-3 justify-content-center">
                                        <div class="col-md-6">
                                          <div class="captcha" style="display: flex; flex-direction:row; justify-content:space-between;">
                                            <span id="captcha-image">{!! captcha_img() !!}</span>
                                            <button type="button" class="btn btn-black btn-refresh"><i class="fa fa-refresh"></i></button>
                                          </div>
                                        </div>
                                    </div>
                                    
                                    <div class="row mb-3 justify-content-center">
                                        <div class="col-md-6 mt-3">
                                            <label for="password" class="form-label">Enter CAPTCHA from Above Image</label>
                                            <input id="captcha" type="text" class="form-control required" placeholder="Captcha" name="captcha" required="">
                                            @if ($errors->has('captcha'))
                                              <span class="help-block">
                                                  <strong>{{ $errors->first('captcha') }}</strong>
                                              </span>
                                            @endif
                                        </div>
                                    </div>
                                    -->
                                    <div class="row mb-0 text-center">
                                        <div class="col-md-12 text-center">
                                            @if(Session::get('loginAttempsRemaining') !== null && Session::get('loginAttempsRemaining') <= 0)
                                                <h5>Log in disabled for 1 hour.</h5>
                                            @else

                                            <button type="submit" class="btn btn-primary" id="login-button" style="width:100px;"><i class="fa fa-sync fa-spin"></i> Log In</button>
                                            @endif
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 mt-4 text-center">
                        <a href="{{ route('register') }}">No Account? Create One Here</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @else
    <div id="page-content">
       <div class="gradient-wrap">
          <!--  HERO -->
          <div class="black position-relative hero-section mb-4" style="margin: 20px 4em;">
             <div class="row" style="justify-content: center;">
                <div class="col-md-12 text-center">
                   <img src="/assets/img/300.png" style="width: 100%; max-width: 400px; margin-top: 40px;">
                   <h1>We're Performing Maintenance</h1>
                   <p>This is temporary, and we'll have systems flying high shortly.</p>
                </div>
                <div class="col-md-12 text-center">
                   <a class="dark-on-light nav-links btn btn-primary" href="/"><i class="fas fa-location-arrow"></i> HEAD HOME</a>
                </div>
             </div>
          </div>
       </div>
    </div>
    @endif
@endsection

@section('js')
    <script type="text/javascript">
        $("#login-button").click(function(){
            $(this).addClass("disable-while-loading")
        });
        /*

        $(".btn-refresh").click(function(){

          $.ajax({

             type:'GET',

             url:'/refresh-captcha',

             success: function(data){

                $("#captcha-image").html(data);

             }

          });

        });
        */
    </script>
@endsection
