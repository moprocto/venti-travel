@extends('layouts.airlock-master')

@section('content')
<div id="page-content">
    <div class="gradient-wrap">
        <div class="container black position-relative hero-section mb-4">
            <div class="row justify-content-center">
                <div class="col-md-12 text-center mt-4">
                    <h2>Access Your Airlock</h2>
                </div>
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-body">
                            @error('general')
                                <div class="alert alert-danger">
                                    {{ $message }}
                                </div>
                            @enderror
                            
                            <form method="POST" action="{{ url('/airlock/login') }}">
                                @csrf
                                <div class="form-group">
                                    <label>Email Address</label>
                                    <input type="email" name="email" class="form-control @error('email') is-invalid @enderror" 
                                           value="{{ old('email') }}" required>
                                </div>

                                <div class="form-group">
                                    <label>Password</label>
                                    <input type="password" name="password" class="form-control @error('password') is-invalid @enderror" 
                                           required>
                                </div>

                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary btn-block">Login</button>
                                </div>
                            </form>

                            <div class="text-center mt-3">
                                <a href="{{ url('/airlock/register') }}">Need an account? Register here</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection