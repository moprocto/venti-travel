@extends('layouts.airlock-master')

@section('content')
<div id="page-content">
    <div class="gradient-wrap">
        <div class="container hero-section mb-4 mt-4">
            <div class="row">
                <div class="col-md-8 offset-md-2 mb-4 mt-4">
                    <span class="mt-4 mb-4"><a href="/airlock/home"><i class="fa fa-chevron-left"></i> Back to Home</a></span>
                <h4 class="mt-4 mb-4">{{ ucfirst($plan['plan']) }} Protection Plan</h4>
                    <div class="card">
                        <div class="card-body">
                            <div class="row mb-4">
                                <div class="col-md-6">
                                    <h5>Plan Details</h5>
                                    <p><strong>Status:</strong> {{ ucfirst($plan['status']) }}</p>
                                    <p><strong>Started:</strong> {{ date('M d, Y', $plan['createdAt']) }}</p>
                                    <p><strong>Coverage Level:</strong> 
                                        @if($plan['plan'] === 'standard')
                                            Up to $5,000
                                        @elseif($plan['plan'] === 'pro')
                                            Up to $10,000
                                        @else
                                            Up to $20,000
                                        @endif
                                    </p>
                                </div>
                                <div class="col-md-6">
                                    <h5>Billing</h5>
                                    <p><strong>Amount:</strong> ${{ number_format($subscription->items->data[0]->price->unit_amount / 100, 2) }}/month</p>
                                    <p><strong>Next billing date:</strong> {{ date('M d, Y', $subscription->current_period_end) }}</p>
                                    <a href="{{ url('/airlock/plan/' . $planId . '/receipt') }}" class="btn btn-primary" target="_blank">
                                        View Receipt
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection