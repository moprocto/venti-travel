@extends('layouts.airlock-master')

@section('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/switchery/0.8.2/switchery.min.css" integrity="sha512-uyGg6dZr3cE1PxtKOCGqKGTiZybe5iSq3LsqOolABqAWlIRLo/HKyrMMD8drX+gls3twJdpYX0gDKEdtf2dpmw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <style type="text/css">
        html{
            overflow-x: hidden;
        }
        @media(max-width: 1368px ){
            .tb-full-width{
                width: 100%;
            }
        }
        @media(max-width: 768px ){
            .hide-sm{
                display:none !important;
            }
        }
        .hamburger-menu-list li a{
            color: white;
        }
        .mobile-nav-open .hamburger-menu-list li a{
            color: black;
        }
        .btn-white{
            color: black !important;
        }
        .mobile-nav-open #backdrop{
            margin-top: 0 !important;
        }
        .about-body{
            justify-content: center;
        }
        html body{
            background: #F2F2F6 !important;
        }
        .cta-button {
            background: #007bff;
            color: white !important;
            padding: 15px 30px;
            border-radius: 5px;
            text-decoration: none;
            font-weight: bold;
            display: inline-block;
            margin-top: 20px;
        }
        .cta-button:hover {
            background: #0056b3;
            color: white !important;
            text-decoration: none;
        }
        .feature-icon {
            font-size: 2rem;
            margin-bottom: 1rem;
            color: #007bff;
        }
        .pills-item{
            margin-bottom:20px;
        }
        .sand-badge{
            background-color: rgba(157, 138, 124, .7);
            opacity: .8;
            color: white;
            font-size: 22px;
            border-radius: 50%;
            width: 42px;
            height: 40px;
            letter-spacing: 1;
        }
        .old-way{
            color:darkred;
            font-size: 14px;
            border-radius: 10px;
            text-align: left;
            margin-bottom: 10px;
        }
        h5.old, .old-way i{
            color:darkred;
            font-size: 14px;
            margin-bottom: 0;
            margin-top: 10px;
        }

        .new-way{
            color:#198754;
            border: 2px solid #198754;
            font-size: 14px;
            border-radius: 10px;
            text-align: left;
        }

        h5.new, .new-way i{
            color:#198754;
            font-size: 14px;
            margin-bottom: 0;
            margin-top: 10px;
        }
        .curvy-card {
            border-radius: 20px;
            border: 1px dashed rgba(255, 255, 255, 0.5) !important;
            background: transparent;
            padding: 10px;
        }

        .curvy-card .card-body {
            background: white;
            border-radius: 20px;
        }
        .pricing-monthly-as-annual {
            display: none;
        }
        .pricing-discount{
            text-decoration: line-through;
        }
        .circle{
            height: 50px;
            width: 50px;
            border-radius:50px;
            border: 1px dashed silver;
            margin: 0 5px;
            align-items: center;
            align-content:center;
            justify-content:center;
            display:flex;
            color:black;
        }
        .circle.filled{
            background-color: #a3f9d2;
            border: 1px dashed #198754;
        }
        .circle.success{
            background-color: #22ca7c;
            color:white;
        }

        .circle.filled i{
            font-size: 10px;
        }

        .text-small{
            font-size:12px;
        }
        .rewards-circles {
            display:flex;
            flex-direction:row;
            justify-content:center;
            gap: 10px; /* Adds spacing between circles */
        }

        .rewards-circles .circle {
            flex: 0 0 auto; /* Prevents circles from stretching */
        }

        /* Desktop: 10 per row */
        @media (min-width: 768px) {
            .rewards-circles {
                width: 100%;
                max-width: 500px; /* Adjust as needed */
                margin: 0 auto;
            }
            .rewards-circles .circle {
                width: 50px;
                height: 50px; /* 10 items per row, accounting for gaps */
            }
        }

        /* Mobile: 5 per row */
        @media (max-width: 767px) {
            .rewards-circles {
                flex-wrap: wrap;
            }
            .rewards-circles .circle {
                width: 40px;
                height:40px; /* 5 items per row, accounting for gaps */
            }
        }
    </style>

    <link href="https://fonts.googleapis.com/icon?family=Material+Icons+Round" rel="stylesheet">
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
@endsection

@section('content')
<div id="backdrop" style="margin-top: -130px; width: 100%; z-index: 1; display: flex; align-items: center; min-height: 800px; background-position: top center; background-size: cover; background-image:url('/assets/img/airlock-bg-2.jpg')">
    <div class="gradient-wrap" style="background: transparent; width:100%;">
        <div class="section-wrapper black position-relative">
            <div class="row" style="align-items:center;">
                <div class="col-lg-5 col-md-12 col-sm-12 col-12 top-header" style="display: flex; align-items:center;">
                    <div id="hero-text-wrapper" style="max-width:900px;">
                        <h2 class="section-subtitle mb-2" style="color: white; max-width:900px; font-size: 1.5rem;">
                            
                        </h2>
                        <h1 class="mb-2 section-title" style="line-height: 3rem; color: white; font-weight:600 !important; font-size: 3.8rem;">
                            <span style="font-size: 1.5rem;">Cancel your flight and</span><br>
                            Get Your Money Back<br>
                            <span style="font-size: 1.5rem;">With Airlock Flight Protection</span>
                        </h1>
                        <h2 class="section-subtitle mb-4" style="color: white; max-width:900px;">
                            A full refund for any reason regardless of airline policy starting at $7 per month.
                        </h2>
                        <a href="/airlock/login" class="btn btn-primary text-bold">Get Airlock</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<section class="pt-4 pb-4" style="background-color: white; ">
    <div class="container full-width" style="padding-top:50px; padding-bottom: 50px;">
        <div class="row">
            <div class="col-md-12 text-center">
                <h5>Airlines Collect</h5>
                <h2 style="font-size:3em">$1 Billion in Junk Fees Annually</h2>
                <h5>From U.S. travelers due to canceled or changed flights</h5>
                <p>Here's how they do it</p>
            </div>
        </div>
        <div class="row mb-4">
            <div class="col-md-10 offset-lg-1">
                <div class="row mt-4 mb-4">
                    <div class="col-md-4">
                        <div class="card curvy-card">
                            <div class="card-body">
                                <div class="old-way" style="align-items:center;">
                                    <h5 class="text-center old mt-4 mb-4">
                                        <img src="/assets/img/icons/airline-fee.png" style="width:75px">
                                    </h5>
                                    <div class="d-flex flex-row ">
                                        <span class="p-2">Airlines offer "changeable" tickets but stack hefty penalties to conduct a change while also adding fare difference charges. The math is often nonsensible.</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card curvy-card">
                            <div class="card-body">
                                <div class="old-way" style="align-items:center;">
                                    <h5 class="text-center old mt-4 mb-4">
                                        <img src="/assets/img/icons/fee-double.png" style="width:75px">
                                    </h5>
                                    <div class="d-flex flex-row ">
                                        <span class="p-2">Airlines sell refundable tickets that can cost double the price of a non-refundable ticket. You'll have to fight through a complicated cancellation process.</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card curvy-card">
                            <div class="card-body">
                                <div class="old-way" style="align-items:center;">
                                    <h5 class="text-center old mt-4 mb-4">
                                        <img src="/assets/img/icons/no-money.png" style="width:75px">
                                    </h5>
                                    <div class="d-flex flex-row">
                                        <span class="p-2">Some refundable tickets only give you back "credits" or miles. You may be required to pay an additional 50% penalty of the ticket price to get your money back.</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-4">
            <div class="col-md-12 text-center">
                <h5>Don't Become a Statistic</h5>
                <h2 style="font-size:3em">Get Annual Flight Protection</h2>
                <h5>And save thousands</h5>
            </div>
        </div>
    </div>
</section>
<section class="pt-4 pb-4 mt-4" style="margin-top:150px;">
    <div class="container full-width">
        <div class="row">
            <div class="col-md-10 offset-lg-1">
                <div class="row mt-4 mb-4">
                    <div class="col-md-12 text-center">
                        <img src="/assets/img/icons/airlock-active.png" style="width:150px; margin:20px auto;">
                        <h2 class="text-primary">Introducing Airlock Flight Protection</h2>
                        <h5 style="max-width:800px; margin:0 auto;">We us AI to cancel your flight and get you a full refund. Not credits. Not miles.</h5>
                    </div>
                </div>
                <div class="row" style="margin:100px 0;">
                    <div class="col-md-4 text-center">
                        <h3>Financially Smart</h3>
                        <div class="new-way bg-shadow mb-4 pb-4" style="align-items:center; text-align: center; background-color:white;">
                            <h5 class="text-center old mt-4 mb-4">
                                <img src="/assets/img/icons/innovation.png" style="width:75px">
                            </h5>
                            <div class="d-flex flex-row ">
                                <span class="p-2">Don't spend hundreds per flight for protection. Save thousands by choosing a protection plan that covers all your flights.</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 text-center">
                        <h3>Money Not Credits</h3>
                        <div class="new-way bg-shadow mb-4 pb-4" style="align-items:center; text-align: center; background-color:white;">
                            <h5 class="text-center old mt-4 mb-4">
                                <img src="/assets/img/icons/money.png" style="width:75px">
                            </h5>
                            <div class="d-flex flex-row ">
                                <span class="p-2">All refunds go back to your original form of payment within 1 to 7 business days without delay.</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 text-center">
                        <h3>No "Gotchas"</h3>
                        <div class="new-way bg-shadow mb-4 pb-4" style="align-items:center; text-align: center; background-color:white;">
                            <h5 class="text-center old mt-4 mb-4">
                                <img src="/assets/img/icons/easy.png" style="width:75px">
                            </h5>
                            <div class="d-flex flex-row ">
                                <span class="p-2">
                                Cancel your flight and get a full refund for any reason whatsover before departure. The claims process takes less than a minute.</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="about" style="padding-bottom: 0;">
    <div class="container full-width">
        <div class="row" style="background-color: #f7f9ff;">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 text-center pd-20" style="padding-bottom: 0; margin-bottom: -3px; padding:40px;">
            
                <h2 class="section-head-title text-dark mb-3 section-title" style="margin:40px; margin-top:20px">How it Works</h2>
                <h5 style="font-weight: 800; font-size: 16px; text-transform:uppercase; margin-bottom:20px">Flight Refunds Made Easy</h5>
                
                <div class="row text-dark text-left pills-item pills-item-1">
                    <div class="col-md-1 text-center">
                        <span class="badge sand-badge">1</span>
                    </div>
                    <div class="col-md-9">
                        <p class="text-dark">Select the plan that provides the right amount of annual coverage. Follow recommendations in the pricing section for how much coverage we recommend for you.</p>
                    </div>
                    <div class="col-md-2 text-center mb-pd-20 hide-sm" style="display: flex; align-items: center; justify-content:center;">
                        <i class="fas fa-shield"></i>
                    </div>
                </div>

                <div class="row text-dark text-left pills-item pills-item-2">
                    <div class="col-md-1 text-center">
                        <span class="badge sand-badge">2</span>
                    </div>
                    <div class="col-md-9">
                        <p class="text-dark">Only flights marked with <img src="/assets/img/icons/airlock-active.png" style="width:25px;"> are automatically protected by flight protection. Eligibility is based on your plan. See below terms regarding off-platform purchases.</p>
                    </div>
                    <div class="col-md-2 text-center mb-pd-20  hide-sm" style="display: flex; align-items: center; justify-content:center;">
                        <i class="fas fa-plane"></i>
                    </div>
                </div>

                <div class="row text-dark text-left pills-item pills-item-3 mb-4">
                    <div class="col-md-1 text-center">
                        <span class="badge sand-badge">3</span>
                    </div>
                    <div class="col-md-9">
                        <p class="text-dark">To cancel a flight and get refunded, simply navigate to your trips dashboard and submit a cancellation request. Refunds are processed within 1 to 7 business days and will go back to your original form of payment.</p>
                    </div>
                    <div class="col-md-2 text-center mb-pd-20  hide-sm" style="display: flex; align-items: center; justify-content:center;">
                        <i class="fas fa-money-bill-wave"></i>
                    </div>
                </div>
            </div>
            
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 full-width">
                <img src="/assets/img/airlock-easy-cancel.jpg" style="width: 100%;" class="">
            </div>
        </div>
    </div>
</section>

@include('airlock.rewards')

<!-- Pricing Section -->
<section id="pricing">
    <div class="container full-width" style="padding:50px; background-color: #f7f9ff;">
        <div class="row justify-content-center">
            <div class="col-md-12 text-center mb-4">
                <h5 class="section-head-body justify-content-center text-center" style="max-width: 800px; justify-content:center; margin: 0 auto;">All Plans Currently 50% Off</h5>
                <h2 class="section-head-title text-dark mb-3 section-title" style="font-size: 3rem; margin-top:20px;">Protection Plans</h2>
                <h5 class="section-head-body justify-content-center text-center" style="max-width: 800px; justify-content:center; margin: 0 auto;">Save 30% When You Pay Annually</h5>
                <div class="d-flex flex-row justify-content-center" style="align-items:center;">
                    <h5 style="margin-right:20px; padding-top:5px;">See Price Difference <i class="fa fa-hand-point-right"></i> </h5>
                    <h5><input type="checkbox" id="protection" class="js-switch"/></h5>
                </div>
            </div>
            <div class="col-md-11 mt-4">
                <div class="row justify-content-center">
                    <!-- Standard Tier -->
                    @include("airlock.components.standard-plan-details", ["key" => "standard"])

                    <!-- Pro Tier -->
                    @include("airlock.components.pro-plan-details", ["key" => "pro"])

                    <!-- Jetsetter Tier -->
                    @include("airlock.components.jetsetter-plan-details", ["key" => "jetsetter"])
                </div>
                <div class="row">
                    <div class="col-md-12 text-center mt-4">
                        <p class="text-muted">Above prices already include our promotional 50% discount. Looking for an enterprise, travel agent, or affiliate solution? Send us an email at <a href="mailto:admin@venti.co">admin@venti.co</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>



<!-- Terms and Conditions section -->
<section style="padding: 50px 0; background: white;">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center mb-5">
                <h2 class="section-head-title text-dark mb-3">Finer Print and Limitations</h2>
                <h5 class="section-head-body pb-4 justify-content-center text-center" style="max-width: 800px; justify-content:center; margin: 0 auto;">It's in our DNA to be as transparent as possible. Feel free to contact us at <a href="mailto:admin@venti.co">admin@venti.co</a> with any questions.</h5>
            </div>
            <div class="col-md-8 mx-auto">
                <div class="card">
                    <div class="card-body">
                        @include('airlock.terms')
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@include('airlock.faq')

@endsection

@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/switchery/0.8.2/switchery.min.js" integrity="sha512-lC8vSUSlXWqh7A/F+EUS3l77bdlj+rGMN4NB5XFAHnTR3jQtg4ibZccWpuSSIdPoPUlUxtnGktLyrWcDhG8RvA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script type="text/javascript">
    $(document).ready(function() {
        toggleLogo("black","white");

        function toggleLogo(color, replaced){
            var logoSRC = $(".logo").attr("src");
            logoSRC = logoSRC.replace(color, replaced);
            $(".logo").attr("src", logoSRC);
        }

        $("#hamburger-menu").click(function(){
            if($("#page_home__show").hasClass("mobile-nav-open")){
                toggleLogo("white","black");
            } else {
                toggleLogo("black","white");
            }
        });

        var elem = document.querySelector('.js-switch');

		var switchery = new Switchery(elem, { });

        $("body").on("click", ".switchery", function(){
            updatePricingOptions(elem.checked);
        })

        function updatePricingOptions(isYearly) {
            // Update all pricing elements
            document.querySelectorAll('[data-pricing-yearly]').forEach(element => {
                const monthlyPrice = element.getAttribute('data-pricing-monthly');
                const yearlyPrice = element.getAttribute('data-pricing-yearly');
                element.textContent = isYearly ? yearlyPrice : monthlyPrice;
            });

            // Toggle visibility of annual comparison prices
            document.querySelectorAll('.pricing-monthly-as-annual').forEach(element => {
                element.style.display = isYearly ? 'inline' : 'none';
            });

            // Update all frequency text elements
            document.querySelectorAll('.pricing-frequency').forEach(element => {
                element.textContent = isYearly ? 'year' : 'month';
                // Update hidden form inputs if they exist
                if (element.tagName.toLowerCase() === 'input') {
                    element.value = isYearly ? 'year' : 'month';
                }
            });
        }
    });
</script>
@endsection
