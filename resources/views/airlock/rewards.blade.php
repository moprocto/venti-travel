<section style="padding: 50px 0; background: white;">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center mb-2">
                <h2 class="section-head-title text-dark mb-3">Receive a Free Flight</h2>
                <h5 class="section-head-body pb-4 justify-content-center text-center" style="max-width: 800px; justify-content:center; margin: 0 auto;">For every 5 flights you complete with Venti</h5>
            </div>
            <div class="col-md-8 mx-auto mb-4">
                <img src="/assets/img/airplane.gif" style="display:block; width:150px; margin:0 auto; margin-bottom:50px;"/>
                <div class="rewards-circles">
                    @for($i = 0; $i <= 5; $i++)
                            <span class="circle {{ $i == 5 ? 'success' : 'filled' }}">
                            @if($i == 5)
                                <i class="fa fa-plane"></i>
                            @else
                                <i class="fa fa-check"></i>
                            @endif
                        </span>
                    @endfor
                </div>
            </div>
            <div class="col-md-12 text-center mt-4">
                <p class="text-small">Terms and conditions apply</p>
            </div>
        </div>
    </div>
</section>