@extends('layouts.airlock-master')

@section('css')
    <style>

    </style>
@endsection

@section('content')
<div id="page-content">
    <div class="gradient-wrap">
        <div class="container black position-relative hero-section mb-4">
            <div class="row justify-content-center">
                <div class="col-md-12 text-center mt-4">
                    <h2>Create Your Airlock</h2>
                </div>
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-body">
                            <form method="POST" action="{{ url('/airlock/register') }}">
                                @csrf
                                <div class="form-group">
                                    <label>First Name<span class="d-block text-small">As it appears on your passport or other travel documents</span></label>
                                    <input type="text" name="f_name" class="form-control @error('f_name') is-invalid @enderror" 
                                           value="{{ old('f_name') }}" required>
                                    @error('f_name')
                                        <span class="invalid-feedback">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label>Last Name<span class="d-block text-small">As it appears on your passport or other travel documents</span></label>
                                    <input type="text" name="l_name" class="form-control @error('l_name') is-invalid @enderror" 
                                           value="{{ old('l_name') }}" required>
                                    @error('l_name')
                                        <span class="invalid-feedback">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label>Email Address</label>
                                    <input type="email" name="email" class="form-control @error('email') is-invalid @enderror" 
                                           value="{{ old('email') }}" required>
                                    @error('email')
                                        <span class="invalid-feedback">{{ $message }}</span>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    <label>Password</label>
                                    <input type="password" name="password" class="form-control @error('password') is-invalid @enderror" 
                                           required minlength="8">
                                    @error('password')
                                        <span class="invalid-feedback">{{ $message }}</span>
                                    @enderror
                                    <small class="form-text text-muted">Password must be at least 8 characters long</small>
                                </div>

                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary btn-block">Create Account</button>
                                </div>
                            </form>

                            <div class="text-center mt-3">
                                <a href="{{ url('/airlock/login') }}">Already have an account? Log in here</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection