<div class="col-xs-12 col-sm-12 col-md-6 col-xl-4 mb-4">
    <div class="card curvy-card h-100">
        <div class="card-body">
            <h3 class="text-center mb-4">Jetsetter</h3>
            <div class="text-center mb-4">
            <span class="text-muted"><span class="pricing-monthly-as-annual pricing-discount">$1,956</span></span>
                <h2 class="mb-0" class="pricing pricing-{{ $key }}" data-pricing-yearly="$1,500" data-pricing-monthly="$163">$163</h2>
                <p class="text-muted">per <span class="pricing-frequency">month</span></p>
            </div>
            <div class="text-center mb-4">
                <p class="mb-0" style="font-size: 2em;">$40,000</p>
                <p class="text-muted">Max refunds per Year</p>
            </div>
            <hr>
            <ul class="list-unstyled">
                <li class="mb-3">
                    <i class="fas fa-check me-2"></i>
                    Up to $10,000 per Person per Trip
                </li>
                <li class="mb-3">
                    <i class="fas fa-check me-2"></i>
                    Up to $20,000 per Trip
                </li>
                <li class="mb-3">
                    <i class="fas fa-check me-2"></i>
                    Name on booking not required
                </li>
                <li class="mb-3">
                    <i class="fas fa-check me-2"></i>
                    Up to Economy Plus eligible
                </li>
                <li class="mb-3">
                    <i class="fas fa-check me-2"></i>
                    Domestic and international flights
                </li>
                <li class="mb-3">
                    <i class="fas fa-check me-2"></i>
                    Limited to flights booked at least 2 days in advance
                </li>
                <li class="mb-3">
                    <i class="fas fa-check me-2"></i>
                    Venti add-ons/upgrades eligible
                </li>
            </ul>
            <div class="text-center mt-4">
                @if(Route::current()->getName() == 'airlock-landing')
                    <div class="text-center mt-4">
                        <a href="/airlock/login" class="btn btn-outline-primary btn-lg w-100">Get Started</a>
                    </div>
                @else
                    <form action="/airlock/plans/process" method="POST">
                        @csrf
                        <input type="hidden" name="plan" value="{{ $key }}">
                        <input type="hidden" class="pricing-frequency" name="frequency" value="month">
                        <button type="submit" class="btn btn-{{ $key === 'pro' ? 'primary' : 'outline-primary' }} btn-lg w-100">
                            Select Pro
                        </button>
                    </form>
                @endif
            </div>
        </div>
    </div>
</div>