<div class="col-xs-12 col-sm-12 col-md-6 col-xl-4 mb-4">
    <div class="card h-100 curvy-card">
        <div class="card-body">
            <h3 class="text-center mb-4">Standard</h3>
            <div class="text-center mb-4">
                <span class="text-muted"><span class="pricing-monthly-as-annual pricing-discount">$84</span></span>
                <h2 class="mb-0" class="pricing pricing-{{ $key }}" data-pricing-yearly="$60" data-pricing-monthly="$7">$7</h2>
                <p class="text-muted">per <span class="pricing-frequency">month</span></p>
            </div>
            <div class="text-center mb-4">
                <p class="mb-0" style="font-size: 2em;">$2,500</p>
                <p class="text-muted">Max refunds per Year</p>
            </div>
            <hr>
            <ul class="list-unstyled">
                <li class="mb-3">
                    <i class="fas fa-check me-2"></i>
                    Departure must be 30+ days out
                </li>
                <li class="mb-3">
                    <i class="fas fa-check me-2"></i>
                    Up to $2,500 per Trip
                </li>
                <li class="mb-3">
                    <i class="fas fa-check me-2"></i>
                    Only valid for your ticket
                </li>
                <li class="mb-3">
                    <i class="fas fa-check me-2"></i>
                    Only Economy flights eligible
                </li>
                <li class="mb-3">
                    <i class="fas fa-check me-2"></i>
                    Only for domestic fights
                </li>
            </ul>
            <div class="text-center mt-4">
                @if(Route::current()->getName() == 'airlock-landing')
                    <div class="text-center mt-4">
                        <a href="/airlock/login" class="btn btn-outline-primary btn-lg w-100">Get Started</a>
                    </div>
                @else
                    <form action="/airlock/plans/process" method="POST">
                        @csrf
                        <input type="hidden" name="plan" value="{{ $key }}">
                        <input type="hidden" class="pricing-frequency" name="frequency" value="month">
                        <button type="submit" class="btn btn-{{ $key === 'pro' ? 'primary' : 'outline-primary' }} btn-lg w-100">
                            Select Pro
                        </button>
                    </form>
                @endif
            </div>
        </div>
    </div>
</div>