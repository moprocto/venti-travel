@extends('layouts.airlock-master')

@section('content')
<div id="page-content">
    <div class="gradient-wrap">
        <div class="container hero-section mb-4">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card soft-shadow">
                        <div class="card-body">
                            <h2 class="mb-4">File an Airlock Claim</h2>
                            
                            <form method="POST" action="/airlock/claim">
                                @csrf
                                
                                <div class="mb-4">
                                    <label class="form-label">Flight Confirmation Number</label>
                                    <input type="text" 
                                           name="confirmation_number" 
                                           class="form-control @error('confirmation_number') is-invalid @enderror"
                                           value="{{ old('confirmation_number') }}"
                                           required>
                                    @error('confirmation_number')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>

                                <div class="mb-4">
                                    <label class="form-label">Reason for Cancellation</label>
                                    <textarea name="reason" 
                                              class="form-control @error('reason') is-invalid @enderror"
                                              rows="4" 
                                              required>{{ old('reason') }}</textarea>
                                    @error('reason')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>

                                <button type="submit" class="btn btn-primary">Submit Claim</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@if(session('success'))
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script>
document.addEventListener('DOMContentLoaded', function() {
    Swal.fire({
        icon: 'success',
        title: 'Claim Submitted Successfully',
        text: 'We will review your claim and process it within 1-3 business days.',
    });
});
</script>
@endif
@endsection
