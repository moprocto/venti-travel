@extends('layouts.airlock-master')

@section('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/switchery/0.8.2/switchery.min.css" integrity="sha512-uyGg6dZr3cE1PxtKOCGqKGTiZybe5iSq3LsqOolABqAWlIRLo/HKyrMMD8drX+gls3twJdpYX0gDKEdtf2dpmw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <style>
        .curvy-card {
            border-radius: 20px;
            border: 1px dashed rgba(255, 255, 255, 0.5) !important;
            background: transparent;
            padding: 10px;
        }

        .curvy-card .card-body {
            background: white;
            border-radius: 20px;
        }
        .pricing-monthly-as-annual {
            display: none;
        }
        .pricing-discount{
            text-decoration: line-through;
        }
    </style>
@endsection

@section('content')
<div id="page-content">
    <div class="gradient-wrap">
        <div class="container hero-section mb-4 mt-4">
            <div class="row justify-content-center">
                <div class="col-md-12 text-center mb-4 mt-4">
                    <h2>Select Your Protection Plan</h2>
                    <h5 class="section-head-body pb-4 justify-content-center text-center" style="max-width: 800px; justify-content:center; margin: 0 auto;">Save 30% When You Paying Annually</h5>
                    <div class="d-flex flex-row justify-content-center" style="align-items:center;">
                        <h5 style="margin-right:20px; padding-top:5px;">See Price Difference <i class="fa fa-hand-point-right"></i> </h5>
                        <h5><input type="checkbox" id="protection" class="js-switch"/></h5>
                    </div>
                </div>
                @include("airlock.components.standard-plan-details", ["key" => "standard"])
                @include("airlock.components.pro-plan-details", ["key" => "pro"])
                @include("airlock.components.jetsetter-plan-details", ["key" => "jetsetter"])
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/switchery/0.8.2/switchery.min.js" integrity="sha512-lC8vSUSlXWqh7A/F+EUS3l77bdlj+rGMN4NB5XFAHnTR3jQtg4ibZccWpuSSIdPoPUlUxtnGktLyrWcDhG8RvA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            var elem = document.querySelector('.js-switch');

            var switchery = new Switchery(elem, {  });

            $("body").on("click", ".switchery", function(){
                updatePricingOptions(elem.checked);
            })

            function updatePricingOptions(isYearly) {
                // Update all pricing elements
                document.querySelectorAll('[data-pricing-yearly]').forEach(element => {
                    const monthlyPrice = element.getAttribute('data-pricing-monthly');
                    const yearlyPrice = element.getAttribute('data-pricing-yearly');
                    element.textContent = isYearly ? yearlyPrice : monthlyPrice;
                });

                document.querySelectorAll('.pricing-monthly-as-annual').forEach(element => {
                    element.style.display = isYearly ? 'inline' : 'none';
                });

                // Update all frequency text elements
                document.querySelectorAll('.pricing-frequency').forEach(element => {
                    element.textContent = isYearly ? 'year' : 'month';
                    // Update hidden form inputs if they exist
                    if (element.tagName.toLowerCase() === 'input') {
                        element.value = isYearly ? 'year' : 'month';
                    }
                });
            }
        });
    </script>
@endsection