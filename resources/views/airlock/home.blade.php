@extends('layouts.airlock-master')

@section('content')
<div id="page-content">
    <div class="gradient-wrap">
        <div class="container hero-section mb-4 mt-4">
            <div class="row">
                <!-- Welcome Section -->
                <div class="col-md-12 mb-4">
                    <div class="card soft-shadow">
                        <div class="card-body">
                            <div class="d-flex justify-content-between align-items-center">
                                <div>
                                    <h2 class="mb-0">Welcome, {{ getFirstName($user->displayName) }}</h2>
                                    <p class="text-muted mb-0">Your Airlock Flight Protection Dashboard</p>
                                </div>
                                <div>
                                    <a href="/" class="btn btn-primary">
                                        <i class="fas fa-plus"></i> Log Out
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Protection Plans List -->
                <div class="col-md-12">
                    <div class="card soft-shadow">
                        <div class="card-body">
                            <h3 class="mb-4">Your Protection Plans</h3>
                            
                            @if($plans->isEmpty())
                                <div class="text-center py-5">
                                    <i class="fas fa-plane-slash text-muted mb-4" style="font-size: 3em;"></i>
                                    <h4 class="text-muted">No Active Protection Plans</h4>
                                    <p class="mb-4">You haven't protected any flights yet.</p>
                                    <a href="/airlock/new-protection" class="btn btn-primary">
                                        Protect Your First Flight
                                    </a>
                                </div>
                            @else
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>Plan ID</th>
                                                <th>Flight</th>
                                                <th>Coverage Amount</th>
                                                <th>Status</th>
                                                <th>Expiry Date</th>
                                                <th>Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($plans as $plan)
                                                <tr>
                                                    <td>{{ $plan->id() }}</td>
                                                    <td>{{ $plan->data()['flightNumber'] ?? 'N/A' }}</td>
                                                    <td>${{ number_format($plan->data()['coverageAmount'] ?? 0) }}</td>
                                                    <td>
                                                        <span class="badge bg-success">Active</span>
                                                    </td>
                                                    <td>{{ $plan->data()['expiryDate'] ?? 'N/A' }}</td>
                                                    <td>
                                                        <a href="/airlock/plan/{{ $plan->id() }}" class="btn btn-sm btn-primary">
                                                            View Details
                                                        </a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('css')
<style>
    .soft-shadow {
        box-shadow: 0 2px 4px rgba(0,0,0,0.05);
        border: none;
    }
    .card {
        border-radius: 10px;
    }
    .text-primary {
        color: #0d6efd !important;
    }
    .badge {
        padding: 0.5em 1em;
        border-radius: 30px;
    }
    .table > :not(caption) > * > * {
        padding: 1rem;
    }
</style>
@endsection