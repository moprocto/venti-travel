<p>Airlock is not travel insurance. Only flights booked from your Venti account are eligible for protection. If you need to purchase travel insurance, please visit our <a href="/insurance" target="_blank">affiliate provider</a>.</p>
<p>Unused Airlock coverage does not roll over to the next year. Annual coverage and the number of flights protected will reset upon renewal.</p>
<p>Limited to one activation per month. You may include multiple flights per activation.</p>
<p>Cannot be used after departure of the first flight of the itinerary. For post-departure financial protection, we recommend <a href="/insurance" target="_blank">travel insurance</a>.</p>
<p>Cannot be used for flights booked prior to the start of your protection. Cannot be used for the same flight more than once.</p>
<p>No double-dipping. Airline credits/miles/rewards are not eligible for Airlock and will be deducted from Airlock your refund amount if provided by the airline.</p>
<p>Off-platform upgrades/enhancements are not covered. Only upgrades purchased through Venti are protected by Airlock.</p>
<p>Certain routes, airlines, and departure dates are not protected by Airlock. You will be made fully-aware before booking whether the flight is eligible.</p>
<p>Monthly subscribers have a 30-day warm-up period for new accounts.</p>
<p>Rewards are promotional in nature and subject to change at any time.</p>
<p></p>