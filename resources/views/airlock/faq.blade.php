<section id="faqs" class="pt-4 pb-4 mt-4">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 offset-lg-2 col-md-12 section-head text-center">
                <h2 class="text-lg">FAQs</h2>
                <h5 class="section-head-body mb-4">You've got questions, and we've got answers.</h5>
            </div>
        </div>
        <div class="row mt-4">
            <div class="col-md-12 col-lg-8 offset-lg-2">
                <div class="accordion" id="customAccordion">
                    <div class="accordion-item first">
                        <h2 class="accordion-header">
                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                              Why purchase this protection plan instead of travel insurance? 
                            </button>
                        </h2>
                        <div id="collapseTwo" class="accordion-collapse collapse" data-bs-parent="#customAccordion">
                            <div class="accordion-body">
                                <p>Travel insurance is more comprehensive and more expensive than flight protection when trying to cover multiple trips.
                                    In addition to higher costs, most travel insurance policies do not allow you to cancel your flight for any reason. You may be required to provide strong evidence that aligns with their claims process, which may result in days of back-and-forth with no guarantee your cancellation request will be approved.
                                    We recommend purchasing flight protection and minimal <a href="/insurance" target="_blank">travel insurance</a> to maximize peace of mind.
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="accordion-item ">
                        <h2 class="accordion-header">
                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                Why purchase this if my credit card offers insurance and protection?</button>
                        </h2>
                        <div id="collapseFour" class="accordion-collapse collapse" data-bs-parent="#customAccordion">
                            <div class="accordion-body">
                                <p>We strongly encourage you to read the fine print associated with your credit card regarding the terms and claims process. Credit card companies are notorious for unpleasant changes to their card holder benefits. In many cases, the cancellation terms are also inflexible and have very low coverage limits that often barely cover the cost of a flight.</p>
                            </div>
                        </div>
                    </div>
                    <div class="accordion-item ">
                        <h2 class="accordion-header">
                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseFiv" aria-expanded="false" aria-controls="collapseFiv">
                                What if I booked a flight outside of Venti and used points/miles?</button>
                        </h2>
                        <div id="collapseFiv" class="accordion-collapse collapse" data-bs-parent="#customAccordion">
                            <div class="accordion-body">
                                <p>At this time, we only provide protection for flights booked on Venti with a credit or debit card. Flights booked off our platform are not covered with Airlock.</p>
                            </div>
                        </div>
                    </div>
                    <div class="accordion-item ">
                        <h2 class="accordion-header">
                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                                Why is there a warm-up period for monthly subscribers?</button>
                        </h2>
                        <div id="collapseSix" class="accordion-collapse collapse" data-bs-parent="#customAccordion">
                            <div class="accordion-body">
                                <p>This measure, along with others, are meant to prevent platform abuse from those that have little to no intention to board their flights.</p>
                            </div>
                        </div>
                    </div>
                    <div class="accordion-item">
                        <h2 class="accordion-header">
                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseSev" aria-expanded="false" aria-controls="collapseSev">
                                Do reward points expire?</button>
                        </h2>
                        <div id="collapseSev" class="accordion-collapse collapse" data-bs-parent="#customAccordion">
                            <div class="accordion-body">
                                <p>You can earn up to 1% of the purchase price of a flight in reward points after you've completed your itinerary. These points do not expire and help you lower the cost of your next flight or hotel booking.</p>
                            </div>
                        </div>
                    </div>
                    <div class="accordion-item">
                        <h2 class="accordion-header">
                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
                                Do you have an affiliate program?</button>
                        </h2>
                        <div id="collapseEight" class="accordion-collapse collapse" data-bs-parent="#customAccordion">
                            <div class="accordion-body">
                                <p>Yes! You can earn cash and points each time you refer a customer to one of our subscription packages and earn up to 10% of each recurring payment if they subscribe to a monthly plan. You'll also be able to receive our Standard protection plan for free. To learn more, contact us at <a href="mailto:admin@venti.co">admin@venti.co</a>.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>