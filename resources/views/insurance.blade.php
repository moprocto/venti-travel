@extends("layouts.curator-master")

@section('css')
   <script id="visitorscoverage_quote_widget" type="text/javascript" src="https://www.visitorscoverage.com/widgets/quotes/js/main.js"></script>
@endsection

@section('content')
  <div id="page-content">
      <div class="gradient-wrap">
          <div class="container hero-section mb-4 mt-4">
              <div class="row">
                <div class="col-md-10 mt-4">
                      <h1>Travel Insurance</h1>
                      <p>Use the portal below to find insurance for your upcoming trip by <a href="https://www.visitorscoverage.com/?affid=36a20d3544aad" target="_blank">Visitors Coverage</a>. Boarding Pass members can exchange 100 Points for $100 when they purchase insurance for any trip (on or off Venti) at least three times within a calendar year. Members may submit their exchange request after the completion of their trips.</p>
                  </div>
              </div>
              <a href="https://www.visitorscoverage.com/?affid=36a20d3544aad" target="_blank">
                <img class="d-block" src="/assets/img/visitors-coverage-logo.png" style="border-radius: 20px; width:100%;">
              </a>
              <br><br>
              <a href="https://www.visitorscoverage.com/?affid=36a20d3544aad" target="_blank" class="btn btn-primary">Open Portal <i class="fa fa-arrow-right"></i></a>
          </div>
      </div>
  </div>
@endsection

@section('js')

@endsection